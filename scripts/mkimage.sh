#!/bin/bash
#
# Voice Signal Preprocess
# Copyright (C) 2001-2019 NationalChip Co., Ltd
# ALL RIGHTS RESERVED!
#
# mkimage.sh - Create an image file
#

[ $# -eq 4 ] || {
    echo "SYNTAX: $0 <total image size> <fw> <fw offset> <outputfile>"
    exit 1
}

filesize(){
    stat -c %s $1 | tr -d '\n'
}

TOTAL_IMAGE_SIZE=$1
FW="$2"
FW_OFFSET=$3
OUTPUT="$4"

FW_SIZE=$(filesize $FW)
DD_COUNT=$((($TOTAL_IMAGE_SIZE)/1024))

if [ $FW_SIZE -gt $(($TOTAL_IMAGE_SIZE-$FW_OFFSET)) ]; then
    echo "FW image too large"
    exit -1
fi

if [ ! -f "$OUTPUT" ];then
    tr '\000' '\377' < /dev/zero | dd of="$OUTPUT" bs=1024 count=$DD_COUNT
fi

dd if="$FW" of="$OUTPUT" bs=1 seek="$FW_OFFSET" conv=notrunc
