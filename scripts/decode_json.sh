#!/bin/bash

json=$(cat $1)
key=$2

if [[ -z "$3" ]]; then
num=1
else
num=$3
fi

value=$(echo "${json}" | awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'${key}'\042/){print $(i+1)}}}' | tr -d '"' | sed -n ${num}p)

echo ${value}

