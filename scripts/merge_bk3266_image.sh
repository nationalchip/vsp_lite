#!/bin/bash
#
# Voice Signal Preprocess
# Copyright (C) 2001-2019 NationalChip Co., Ltd
# ALL RIGHTS RESERVED!
#
# merge_bk3266_image.sh - Merge bk3266 and gx8008 images
#

[ $# -eq 3 ] || {
    echo "SYNTAX: $0 <BK3266 image version> <BK3266 image> <GX8008 image>"
    echo "EXAMPLE: $0 1.2.3 BK3266_flash_image_crc.bin ../output/vsp.bin"
    exit 1
}

filesize(){
    stat -c %s $1 | tr -d '\n'
}

BK3266_IMAGE_SIZE=1048576
VSP_BIN_MIN_SIZE=2097152
BK3266_IMAGE="./.fw.bin"
BK3266_IMAGEINFO="./.fw_imageinfo.bin"
TOOLS_MKIMAGEINFO="../tools/imageinfo/mkimageinfo"
BK3266_IMAGE_VERSION=`echo $1 |  awk -F '.' '{printf("0x%02x%02x%02x", $1, $2, $3)}'`
BK3266_BIN="$2"
VSP_BIN="$3"
MERGE_IMAGE="./vsp_merge.bin"

VSP_BIN_SIZE=$(filesize $VSP_BIN)
VSP_IMAGEINFO_OFFSET=$((($VSP_BIN_SIZE)/2-64*1024-1024))
BK3266_BIN_SIZE=$(filesize $BK3266_BIN)
BK3266_IMAGEINFO_OFFSET=$((($VSP_BIN_SIZE)/2-64*1024-512))
BK3266_IMAGE_OFFSET=$((($VSP_BIN_SIZE)/2-64*1024))
DD_COUNT=$((($BK3266_IMAGE_SIZE)/1024))

if [ $VSP_BIN_SIZE -lt $(($VSP_BIN_MIN_SIZE)) ]; then
    echo "$VSP_BIN too small"
    exit -1
fi

if [ $BK3266_BIN_SIZE -gt $(($BK3266_IMAGE_SIZE)) ]; then
    echo "$BK3266_BIN too large"
    exit -1
fi

if [ ! -f "$TOOLS_MKIMAGEINFO" ];then
    echo "$TOOLS_MKIMAGEINFO does not exist"
    exit -1
fi

# Generate BK3266 image
if [ -f "$BK3266_IMAGE" ];then
    rm -f $BK3266_IMAGE
fi
tr '\000' '\377' < /dev/zero | dd of="$BK3266_IMAGE" bs=1024 count=$DD_COUNT
dd if="$BK3266_BIN" of="$BK3266_IMAGE" bs=1 seek=0 conv=notrunc

# Generate BK3266 imageinfo
$TOOLS_MKIMAGEINFO -S $BK3266_IMAGE_VERSION -f $BK3266_IMAGE -o $BK3266_IMAGEINFO

# Merge BK3266 image & imageinfo to MERGE_IMAGE
cp -f $VSP_BIN $MERGE_IMAGE
dd if="$BK3266_IMAGEINFO" of="$MERGE_IMAGE" bs=1 seek="$BK3266_IMAGEINFO_OFFSET" conv=notrunc
dd if="$BK3266_IMAGE" of="$MERGE_IMAGE" bs=1 seek="$BK3266_IMAGE_OFFSET" conv=notrunc
rm -f $BK3266_IMAGE $BK3266_IMAGEINFO

# Rename MERGE_IMAGE with versions
command -v hexdump >/dev/null 2>&1 || { echo >&2 "I require hexdump to rename but it's not installed.  Ignoring."; exit -1; }
VSP_VER=`hexdump -n 3 -e '3/1 "%02x" ""' -s $(($VSP_IMAGEINFO_OFFSET+8)) $MERGE_IMAGE`
VSP_VER=`printf "v%01d.%01d.%01d" "0x"${VSP_VER:4:2} "0x"${VSP_VER:2:2} "0x"${VSP_VER:0:2}`
BK3266_VER=v"$1"
echo vsp image version: $VSP_VER
echo BK3266 image version: $BK3266_VER
mv $MERGE_IMAGE vsp_"$VSP_VER"_bk3266_"$BK3266_VER".bin

# Lzo compress
../tools/pc_simulator_demo/lzo/compress vsp_"$VSP_VER"_bk3266_"$BK3266_VER".bin vsp_"$VSP_VER"_bk3266_"$BK3266_VER".lzo 16 | grep -v -e "compressed" -e "into"
