#!/bin/bash
#
# Voice Signal Preprocess
# Copyright (C) 2001-2020 NationalChip Co., Ltd
# ALL RIGHTS RESERVED!
#
# check_fw_size.sh - check fw image size
#

[ $# -eq 3 ] || {
    echo "SYNTAX: $0 <fw> <fw offset> <next fw offset>"
    exit 1
}

filesize(){
    stat -c %s $1 | tr -d '\n'
}

FW="$1"
FW_OFFSET=$2
NEXT_FW_OFFSET=$3

FW_SIZE=$(filesize $FW)
declare -i FW_SPACES_SIZE=$NEXT_FW_OFFSET-$FW_OFFSET

if [ $FW_SIZE -gt $FW_SPACES_SIZE ]; then
    echo "$FW image too large, please check code or edit flsh_map.json"
    exit -1
fi

