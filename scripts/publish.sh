#!/bin/bash -x
#
# CleanUp all files which can't be published
#

if [ ! -x scripts/publish.sh ]; then
    echo "Please run me at TOP of VSP"
fi

make distclean

# Private Files
rm -rf mcu/drivers/snpu
rm -rf mcu/drivers/otp
rm -rf dsp/gxma
rm -rf dsp/3rd

# delete me
rm -f scripts/publish

# SCM files
rm -rf .git
rm -rf vsp.xcodeproj

echo "[Publish Done]"

# MacOSX related
find . -name "._*" | xargs rm
