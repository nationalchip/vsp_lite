/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 *
 * imageinfo.h: imageinfo utilities header
 *
 */
#ifndef __IMAGEINFO_H__
#define __IMAGEINFO_H__

#include <stdint.h>

#define IMAGEINFO_SIZE       (1*1024)
#define IMAGEINFO_MAGIC      0x494D4948 // "IMIH"
#define IMAGEINFO_VERSION    0x00000001
#define IMAGEINFO_CRC32_LEN  4
#define IMAGEINFO_PAD        0xFF

typedef struct {
    uint32_t magic;
    uint32_t imageinfo_version;
    uint32_t image_version;
    uint32_t image_crc32;
    uint32_t imageinfo_crc32;
} IMAGE_INFO;

int readimageinfo(const char *path, uint32_t offset, IMAGE_INFO *imageinfo);

#endif
