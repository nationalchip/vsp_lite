/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 *
 * mkimageinfo.c: make imageinfo bin
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

#include "../libs/crc.h"
#include "imageinfo.h"

#define MKIMAGEINFO_VERSION "0.1.0"

#define TMP_LEN        128

static const struct option longopts[] = {
    {"image-version",    required_argument, NULL, 'S'},
    {"firmwall-path",    required_argument, NULL, 'f'},
    {"output",           required_argument, NULL, 'o'},
    {"help",             no_argument,       NULL, 'h'},
    {"version",          no_argument,       NULL, 'v'},
    {NULL, 0, NULL, 0}
};

static void usage(FILE *out)
{
    fputs("\nUsage: mkimageinfo [OPTION]...\n", out);
    fputs("make imageinfo binary.\n", out);

    fputs("\nOptions:\n", out);
    fputs("  -S, --image-version <verison>  set image version\n", out);
    fputs("  -f, --firmwall <file>          firmwall bin file\n", out);
    fputs("  -o, --output <file>            place the output into <file>\n", out);
    fputs("  -h, --help                     display this help and exit\n", out);
    fputs("  -v, --version                  output version information and exit\n", out);
}

static int mkimageinfo_get_image_crc32(char *path, uint32_t *image_crc32)
{
    FILE *fp;
    char buf[1024];
    size_t readlen;
    size_t filesize;

    fp = fopen(path, "rb");
    if (!fp) {
        printf("open file %s error\n", path);
        return -1;
    }

    fseek(fp, 0L, SEEK_END);
    filesize = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    *image_crc32 = 0;
    while (filesize > 0) {
        readlen = filesize < 1024 ? filesize : 1024;
        if (fread(buf, readlen, 1, fp) != 1) {
            printf("read error!\n");
            fclose(fp);
            return -1;
        }
        *image_crc32 = crc32(*image_crc32, buf, readlen);
        filesize -= readlen;
    }

    fclose(fp);
    return 0;

}

int main(int argc, char *argv[])
{
    int i, c;
    FILE *fp;
    IMAGE_INFO *image_info;
    char imageinfo_buf[IMAGEINFO_SIZE];
    char tmp[TMP_LEN];
    int optarg_len;
    char *imageinfo_path = "imageinfo.bin";
    int image_version_flag = 0;
    int image_crc32_flag = 0;

    for (i = 0; i < IMAGEINFO_SIZE; i++)
        imageinfo_buf[i] = IMAGEINFO_PAD;

    image_info = (IMAGE_INFO *)imageinfo_buf;

    image_info->magic = IMAGEINFO_MAGIC;
    image_info->imageinfo_version = IMAGEINFO_VERSION;

    while ((c = getopt_long(argc, argv, "H:S:N:V:P:o:f:hv",
                    longopts, NULL)) != -1) {
        switch (c) {
        case 'S':
            image_info->image_version = strtoul(optarg, NULL, 0);
            image_version_flag = 1;
            break;
        case 'f':
            mkimageinfo_get_image_crc32(optarg, &image_info->image_crc32);
            image_crc32_flag = 1;
            break;
        case 'o':
            imageinfo_path = optarg;
            break;
        case 'h':
            usage(stdout);
            break;
        case 'v':
            printf("V%s\n", MKIMAGEINFO_VERSION);
            break;
        default:
            usage(stderr);
            break;
        }
    }

    if (image_version_flag != 1) {
        printf("Please specify software version by -S\n");
        usage(stderr);
        return -1;
    }

    if (image_crc32_flag != 1) {
        printf("Please specify software path by -f\n");
        usage(stderr);
        return -1;
    }

    if ((fp = fopen(imageinfo_path, "w")) == NULL) {
        printf("open failed!\n");
        return -1;
    }

    image_info->imageinfo_crc32 = crc32(0, (unsigned char *)image_info, sizeof(IMAGE_INFO)-sizeof(image_info->imageinfo_crc32));

    if (fwrite(imageinfo_buf, IMAGEINFO_SIZE, 1, fp) != 1) {
        printf("write error!\n");
        fclose(fp);
        return -1;
    }

    printf("image info:\n");
    printf("  image version: 0x%x\n", image_info->image_version);
    printf("  image crc32  : 0x%x\n", image_info->image_crc32);
    printf("  imageinfo crc32  : 0x%x\n", image_info->imageinfo_crc32);

    fclose(fp);

    return 0;
}
