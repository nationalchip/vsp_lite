/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 *
 * readimageinfo.c: read imageinfo bin
 */

#include <stdio.h>
#include <stddef.h>
#include "imageinfo.h"
#include "../libs/crc.h"

int readimageinfo(const char *path, uint32_t offset, IMAGE_INFO *imageinfo)
{
    FILE *fp;
    size_t filesize;
    uint32_t image_crc32 = 0;

    if (!path || !imageinfo)
        return -1;

    if ((fp = fopen(path, "r")) == NULL) {
        printf("open image faile!\n");
        return -1;
    }

    fseek(fp, 0L, SEEK_END);
    filesize = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    if (offset > filesize || filesize < IMAGEINFO_SIZE) {
        printf("not found image!\n");
        goto error;
    }

    fseek(fp, offset, SEEK_SET);
    if (fread(imageinfo, sizeof(imageinfo), 1, fp) != 1) {
        printf("read image info error!\n");
        goto error;
    }

    if (imageinfo->magic != IMAGEINFO_MAGIC) {
        printf("image info format error!\n");
        goto error;
    }

    image_crc32 = crc32(image_crc32, (unsigned char *)imageinfo, sizeof(imageinfo)-IMAGEINFO_CRC32_LEN);
    if (image_crc32 != imageinfo->image_crc32) {
        printf("image info is corrupted!\n");
        goto error;
    }

    fclose(fp);
    return 0;

error:
    fclose(fp);
    return -1;
}

#ifdef BUILD_BIN
int main(int argc, char *argv[])
{
    IMAGE_INFO imageinfo;

    if (argc < 2) {
        fputs("Usage: readimageinfo image\n", stderr);
        return -1;
    }

    if (readimageinfo(argv[1], 0, &imageinfo) < 0)
        return -1;

    printf("image info:\n");
    printf("  image version: 0x%x\n", imageinfo.image_version);
    printf("  image crc32  : 0x%x\n", imageinfo.image_crc32);

    return 0;
}
#endif
