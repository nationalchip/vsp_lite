# MKIMAGEINFO

## 简介
mkimageinfo是一个生成image信息分区的工具。

## IMAGEINFO分区
image info区域包含一些完整固件相关信息，固定64K大小。格式定义如下：
|内容                 |大小(Byte)|
|---------------------|----------|
|MAGIC                |4         |
|IMAGEINFO_VERSION    |4         |
|IMAGE_VERSION        |4         |
|MCU+DSP固件区CRC32值 |4         |
|IMAGEINFO区域CRC32值 |4         |
|RESV                 |-         |


* MAGIC
    IMAGE INFO头，固定为0x494D4948 ，也就是“IMIH”。
* IMAGEINFO_VERSION
    IMAGEINFO区域版本号，修改IMAGEINFO区域格式，需要更新IMAGEINFO_VERSION。
* IMAGE_VERSION
    GX8008完整固件对应的版本号
* MCU+DSP固件区CRC32值
    记录了MCU+DSP固件区的CRC32校验值，保证MCU+DSP固件区的完整性。
* IMAGEINFO区域CRC32值
    记录了IMAGEINFO区的CRC32校验值，保证IMAGEINFO区的完整性。
* RESV
    保留

## 使用方法
使用mkiamgeinfo -h可以查看使用方法

```
mkimageinfo
Please specify software version by -S

Usage: mkimageinfo [OPTION]...
make imageinfo binary.

Options:
  -S, --soft-version <verison>   set image version
  -f, --firmwall <file>          firmwall bin file
  -o, --output <file>            place the output into <file>
  -h, --help                     display this help and exit
  -v, --version                  output version information and exit

```

* -S, --image-version <verison>
    指定软件版本号，如-S 0x010301 表示版本号1.3.1
* -f, --firmwall <file>
    指定了MCU+DSP固件路径，用于计算了MCU+DSP固件CRC32校验值
* -o, --output <file>
    指定IMAGEINFO分区bin文件保存路径
* -h, --help
    显示帮助信息
* -v, --version
    显示mkimageinfo软件版本号
