# vsp.zip说明

## 一、简介

- vsp.zip是`vsp`工程针对`iot方案`的固件包。
- vsp.zip包含`vsp.bin`、`mcu_nor.bin`、`dsp.bin`、`flash_map.json`、`README.md`和 `.config`共6个文件。
- vsp.zip用于`发布生产固件`、`iot方案公版固件`、`MagicBuilder调试`等。
- 一个vsp.zip只对应一种板级，如果用于其他板级可能发生`未知错误`。

---
## 二、各文件说明

### vsp.bin
1. vsp.bin是完整固件，它的size一般与对应板级的`flash total size`相等。
2. vsp.bin是有iota方案各个部分的固件组成的。
3. vsp.bin的flash烧录地址是`0`。
4. vsp.bin的烧录工具是`bootx`或`NCdownload.exe`。
5. 只需烧录vsp.bin即可实现功能，压缩包中的其他文件都是为`二次开发`所准备的。


### mcu_nor.bin
1. mcu_nor.bin是`mcu`部分的固件，它在flash中的烧录地址是`0`，它的size上限由`flash_map.json`决定。
2. mcu_nor.bin已经包含在了`vsp.bin`中，在vsp.bin中的偏移地址即是flash烧录地址。
3. mcu_nor.bin可通过`MagicBuilder`、`vsp-lite`的make或其他工具合并到另外的vsp.bin中使用。
4. 可以通过vsp或vsp-lite工程生成另外的mcu_nor.bin，合并到当前压缩包中的vsp.bin里使用。

### dsp.fw
1. dsp.fw是`dsp`部分的固件，它在flash中的烧录地址和它的size上限由`flash_map.json`决定。
2. dsp.fw已经包含在了vsp.bin中，在vsp.bin中的偏移地址即是flash烧录地址。
3. dsp.fw可通过`vsp-lite`的make或其他工具合并到另外的vsp.bin中使用，或将另外的dsp.fw合并到当前压缩包中的vsp.bin里使用。

### flash_map.json
1. flash_map.json是iot方案各个固件在flash中烧录地址和在vsp.bin中偏移地址的配置文件。
2. flash_map.json在vsp、vsp-lite编译iot方案时和MagicBuilder中被使用到。
3. 举个例子：
    ```json
    {   "flash_total_size":4194304,
        "flash_block_size":65536,
        "mcu_nor.bin":0,
        "dsp.fw":131072,
        "imageinfo.bin":2096128,
        "mcu_main_factory.bin":2097152,
        "dsp_factory.fw":2228224,
        "user_storage":2359296,
        "proxy":2490368,
        "oem":4128768}
    ```
    |字段|含义|数值|备注|
    |:--|:-:|:-:|:-:|
    |`flash_total_size`     |4MB        |flash size     |iot方案常见的是2MB和4MB|
    |`flash_block_size`     |64KB       |block size     |一般使用nor flash，block size为64KB|
    |`mcu_nor.bin`          |0          |mcu部分的固件地址|固定为0|
    |`dsp.fw`               |128KB      |dsp部分的固件地址|一般要为上一段的mcu固件留出128KB空间，也可以根据需要压缩mcu固件空间|
    |`imageinfo.bin`        |2MB - 1KB  |imageinfo地址  |一般设为1/2×flash_total_size - 1KB，imageinfo的size固定为1KB|
    |`mcu_main_factory.bin` |2MB        |厂测mcu固件地址  |可根据需要自行设定或弃用|
    |`dsp_factory.fw`       |2MB + 128KB|厂测dsp固件地址  |可根据需要自行设定弃用|
    |`user_storage`         |2MB + 256KB|用户空间地址     |用于flash bridge时至少需要1×flash_block_size，用于EasyFlash时至少需要2×flash_block_size|
    |`proxy`                |2MB + 384KB|proxy模块占用地址|用于放置MagicBuilder生成的配置文件，可根据需要进行调整|
    |`oem`                  |4MB - 64KB |oem信息地址     |固定为flash_total_size - flash_block_size|
4. 针对不同方案，并不是所有字段都会被使用、
5. mcu_nor.bin至oem地址设置应是逐渐递增的，没有使用到的字段如果想保留可设为与下一个字段相同。
6. 数值使用十进制是因为目前vsp和vsp-lite工程中使用的json解析脚本不支持解析十六进制。

### READMEmd
   *略*

### .config
1. vsp工程的编译配置文件，可在vsp-lite工程中使用。
2. **`vsp-lite工程的.config配置文件不可在vsp工程中使用。`**
3. 应用时需要执行$make menuconfig再退出保存，才能生效。

---

## 版本信息
commit id:
