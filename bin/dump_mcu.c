/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * dump_mcu.h: Generate MCU binary file with Firmware Header
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <getopt.h>

#include <vsp_firmware.h>
#include <vsp_message.h>

#include <mcu_main.h>

static void _PrintHelp(int detail)
{
    printf("Usage: dump_mcu -o <output> [-v][-h]\n");
}

int main(int argc, char **argv)
{
    // Parse command line
    static const char *optString = "o:h?";
    char *outputPath = NULL;

    int opt;
    while((opt = getopt(argc, argv, optString)) != -1)
    {
        switch(opt)
        {
            case 'o':   // Output File
                outputPath = strdup(optarg);
                break;
            case 'h':   /* fall-through is intentional */
            case '?':
            default:
                _PrintHelp(1);
                return 1;
        }
    }

    if (!outputPath) {
        printf("No enough parameters!\n");
        _PrintHelp(0);
        return 1;
    }

    FILE *pFile = fopen(outputPath, "w");
    if (!pFile) {
        printf("Can't create file:%s!\n", outputPath);
        return 1;
    }

    VSP_FIRMWARE_MCU_HEADER header;
    memset(&header, 0, sizeof(header));
    memcpy(&header.magic, VSP_FIRMWARE_MAGIC_MCU, sizeof(header.magic));
    header.message_version = VSP_MESSAGE_VERSION_BTW_MCU_CPU;
    header.size = sizeof(mcu_main_bin);

    fwrite(&header, sizeof(header), 1, pFile);
    fwrite(mcu_main_bin, sizeof(mcu_main_bin), 1, pFile);

    fclose(pFile);

    return 0;
}
