/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * dump_dsp.c: Generate DSP binary file with Firmware Header
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <getopt.h>

#include <vsp_firmware.h>
#include <vsp_message.h>

#include <dsp_dtcm.h>
#include <dsp_ptcm.h>
#include <dsp_sram.h>
#ifdef CONFIG_DSP_ENABLE_XIP
#include <dsp_xip.h>
#endif

static void _PrintHelp(int detail)
{
    printf("Usage: dump_dsp -o <output> [-v][-h]\n");
}

int main(int argc, char **argv)
{
    // Parse command line
    static const char *optString = "o:h?";
    char *outputPath = NULL;

    int opt;
    while((opt = getopt(argc, argv, optString)) != -1)
    {
        switch(opt)
        {
            case 'o':   // Output File
                outputPath = strdup(optarg);
                break;
            case 'h':   /* fall-through is intentional */
            case '?':
            default:
                _PrintHelp(1);
                return 1;
        }
    }

    if (!outputPath) {
        printf("No enough parameters!\n");
        _PrintHelp(0);
        return 1;
    }

    FILE *pFile = fopen(outputPath, "w");
    if (!pFile) {
        printf("Can't create file:%s!\n", outputPath);
        return 1;
    }

    VSP_FIRMWARE_DSP_HEADER header;
    memset(&header, 0, sizeof(header));
    memcpy(&header.magic, VSP_FIRMWARE_MAGIC_DSP, sizeof(header.magic));
    header.message_version = VSP_MESSAGE_VERSION_BTW_MCU_DSP;
    header.ptcm_size = sizeof(dsp_ptcm);
    header.dtcm_size = sizeof(dsp_dtcm);
    header.sram_size = sizeof(dsp_sram);
#ifdef CONFIG_DSP_ENABLE_XIP
    header.xip_size  = sizeof(dsp_xip);
#endif
    header.sram_base = (CONFIG_SOC_SRAM_SIZE_KB - CONFIG_DSP_SRAM_SIZE_KB) * 1024;

    fwrite(&header, sizeof(header), 1, pFile);
#ifdef CONFIG_DSP_ENABLE_XIP
    fwrite(dsp_xip, sizeof(dsp_xip), 1, pFile);
#endif
    fwrite(dsp_ptcm, sizeof(dsp_ptcm), 1, pFile);
    fwrite(dsp_dtcm, sizeof(dsp_dtcm), 1, pFile);
    fwrite(dsp_sram, sizeof(dsp_sram), 1, pFile);

    fclose(pFile);

    return 0;
}
