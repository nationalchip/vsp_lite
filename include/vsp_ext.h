/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * All Right Reserved!
 *
 * vsp_ext.h: EXT buffer related definition
 *
 */

#ifndef __VSP_EXT_H__
#define __VSP_EXT_H__

#define VSP_EXT_VERSION 0x20190117

typedef enum {
    VSP_EXT_BUFFER_TYPE_OPUS,
    VSP_EXT_BUFFER_TYPE_SPEEX,
} VSP_EXT_BUFFER_TYPE;

typedef struct {
    // 1 Byte
    unsigned int vbr       : 1; // 0: Constant Bit Rate; 1: Variable Bit Rate
    unsigned int frame_size: 5; // The unit is ms, default is set 20 ms
    unsigned int frame_num : 3; // 1~6
    // 1 Byte
    unsigned int quality   : 4; // 0~10
    unsigned int complexity: 4; // 0~10
    // 2 Byte
    unsigned int bitrate   :15; // Bit Rate
} VSP_CODEC_DATA_HEADER;

typedef struct {
    VSP_CODEC_DATA_HEADER  codec_header;    //  4 Bytes
    unsigned int           codec_size[6];   // 24 Bytes
    void                  *data;            //
} VSP_CODEC_DATA;

typedef struct {
    unsigned int        version;
    VSP_EXT_BUFFER_TYPE type;
    union {
        VSP_CODEC_DATA  codec_data;
    } buffer;
} VSP_EXT_BUFFER;

#endif /* __VSP_EXT_H__ */
