/* Voice Signal Preprocess
 * Copyright (C) 1991-2020 NationalChip Co., Ltd
 * All Right Reserved!
 *
 * vsp_command.h: Command mcu to cpu related definition
 *
 */

#ifndef __VSP_COMMAND_H__
#define __VSP_COMMAND_H__

#define VSP_COMMAND_VERSION 0x20190813

typedef struct {
    unsigned int            version;
    void                   *cmd_buffer;
    unsigned int            cmd_num;
    unsigned int            cmd_data_size;
} VSP_COMMAND_HEADER;

enum {
    VCI_I2C_DATA = 0xfffffffd,           // used for get I2C data
    VCI_UAC_INFO = 0xfffffffe,           // used for get uac info
} VSP_COMMAND_ID;

typedef struct {
    VSP_COMMAND_HEADER     *cmd_header;
    unsigned int            cmd_index;
    unsigned int            cmd_id;
    void                   *cmd_data;
} VSP_COMMAND;

#endif /* __VSP_COMMAND_H__ */
