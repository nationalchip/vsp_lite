/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * All Right Reserved!
 *
 * vsp_led.h: LED related definition
 *
 */

#ifndef __VSP_LED_H__
#define __VSP_LED_H__

#define VSP_LED_VERSION 0x20171010

typedef union {
    unsigned int value;
    struct {
        unsigned r:8;           /* red */
        unsigned g:8;           /* green */
        unsigned b:8;           /* blue */
        unsigned a:8;           /* alpha, or level */
    } bits;
} VSP_LED_PIXEL;

typedef union {
    unsigned int value;
    struct {
        unsigned hold:16;       /* 0 - 65535 ms */
        unsigned setup:16;      /* 0 - 65535 ms */
    } bits;
} VSP_LED_FRAME_HEADER;

typedef struct {
    unsigned int version;       /* led version, filled with VSP_LED_VERSION */
    unsigned int read;          /* MCU read index */
    unsigned int write;          /* CPU write index */
    unsigned int frame_num;     /* queue length */
    unsigned int pixel_num;     /* led number */
} VSP_LED_QUEUE;

#endif /* __VSP_LED_H__ */
