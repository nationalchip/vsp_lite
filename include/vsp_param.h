/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * vsp_param.h: Parameter definition template
 *
 */

#ifndef __VSP_PARAM_H__
#define __VSP_PARAM_H__

#include <autoconf.h>

#define VSP_PARAM_VERSION 0x20180428

enum {
    KWS_TYPE_MAJOR = 1,
    KWS_TYPE_DEALY2DECODE = 0x40,   // 该唤醒词延迟唤醒
    KWS_TYPE_REF_KWS = 0x10,        // 该唤醒词只有ref通道去做唤醒
};

enum {
    KWS_LEVEL_TOP = 1<<7,   // 第一级KWS
    KWS_LEVEL_MID = 1<<8,   // 第二级KWS
    KWS_LEVEL_END = 1<<9,   // 第三级KWS
};

typedef enum {
    // TODO: Put your param type here
    VSP_PARAM_TYPE_KWS,
    VSP_PARAM_TYPE_MIC_ARRAY,
    VSP_PARAM_TYPE_AEC,
    VSP_PARAM_TYPE_OTP_KEY,
    VSP_PARAM_TYPE_OLAB,
    VSP_PARAM_TYPE_KWS_CONFIG,
    VSP_PARAM_TYPE_VMA_CONFIG,
    VSP_PARAM_TYPE_CMD,
    VSP_PARAM_TYPE_MIC_GAIN_CONTROL,
    VSP_PARAM_TYPE_REF_GAIN_CONTROL
} VSP_PARAM_TYPE;

typedef struct {
    char kws_words[40];          // Key words
    char labels[14];             // The label of key words
#ifdef CONFIG_VPA_OLAB_OP
#else
    char syllable_blank_flag[14];//
#endif
    int  label_length;           // The length of key label
#ifdef CONFIG_VPA_OLAB_OP
    int threshold;
//# ifdef CONFIG_VSP_VPA_OLAB_OP_ENABLE_ICA
    int ica_threshold;
//# endif
#else
    char threshold[8];           // The threshold of kws
#endif
    int  kws_value;              // The event id for mcu
    int  major;                  // bit 0-->major kws, bit 1,2,3-->reverse; bit 4-->>ref_kws; bit 6-->> delay2decode; bit 7,8,9,10,11 -->> kws level
} VSP_KWS_SOURCE;

typedef struct {
    unsigned char *lm;
    unsigned int   lm_size;
    unsigned char *lst;
    unsigned int   lst_size;
} LANGUAGE_MODEL_PARAM;

typedef struct {
    VSP_KWS_SOURCE *kws_source;
    unsigned int    count;
    LANGUAGE_MODEL_PARAM lm_param;
} VSP_PARAM_OLAB;

typedef struct {
    unsigned int kws_value;
} VSP_PARAM_KWS;

typedef struct {
    // TODO: Put your MIC ARRAY parameters here
    // ...
} VSP_PARAM_MIC_ARRAY;

typedef struct {
    // TODO: Put your AEC parameters here
    // ...
} VSP_PARAM_AEC;

typedef struct {
    unsigned int buff_addr;
    unsigned int buff_size;
} VSP_PARAM_OTP_KEY;

typedef struct {
    unsigned int buff_addr;
    unsigned int buff_size;
} VSP_PARAM_KWS_CONFIG;

typedef struct {
    unsigned int buff_addr;
    unsigned int buff_size;
} VSP_PARAM_VMA_CONFIG;

typedef struct {
    unsigned int buff_addr;
    unsigned int buff_size;
} VSP_PARAM_CMD;

// gain(db)
typedef struct {
    int mic_gain;
}VSP_PARAM_MIC_GAIN_CONTROL;

typedef struct {
    int ref_gain;
}VSP_PARAM_REF_GAIN_CONTROL;

typedef struct {
    unsigned int    version;
    VSP_PARAM_TYPE  type;
    union {
        VSP_PARAM_KWS           kws;
        VSP_PARAM_MIC_ARRAY     mic_array;
        VSP_PARAM_AEC           aec;
        VSP_PARAM_OTP_KEY       key;
        VSP_PARAM_OLAB          olab;
        VSP_PARAM_KWS_CONFIG    kws_config;
        VSP_PARAM_VMA_CONFIG    vma_config;
        VSP_PARAM_CMD           cmd;
        VSP_PARAM_MIC_GAIN_CONTROL   mic_gain_control;
        VSP_PARAM_REF_GAIN_CONTROL   ref_gain_control;
    } param;
} VSP_PARAM;

typedef struct {
    unsigned short label_index;
    unsigned short label_len;
    float          score_threshold;
    unsigned char *labels;
} VSP_KWS_LABEL;

typedef struct {
    unsigned int   count;
    VSP_KWS_LABEL *kws_label;
} VSP_KWS_LIST;

typedef struct {
    unsigned char kws_number;
    unsigned :8;
    unsigned short magic;   // Default: 0x1234
    unsigned int kws_list_offset;
    unsigned int vma_config_length;
    unsigned int vma_config_offset;
    unsigned int reserved[12];
} VSP_VPA_CONFIG_HEADER;

#endif /* __VSP_PARAM_H__ */
