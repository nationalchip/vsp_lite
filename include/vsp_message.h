/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_message.h: Message Definition
 *                This header file is shared between MCU, DSP and CPU,
 *                So you need to be very careful when making changes.
 */

#ifndef __VSP_MESSAGE_H__
#define __VSP_MESSAGE_H__

// Note: If you modify the message type or message structure between mcu and cpu, you must update this version.
// 20191205: VMB_CM_LOAD_DSP add ddr parmeter
#define VSP_MESSAGE_VERSION_BTW_MCU_CPU 0x20191205

// Note: If you modify the message type or message structure between mcu and dsp, you must update this version.
// 2019035: VSP_PROCESS_TYPE add VSP_PROCESS_TALKING
#define VSP_MESSAGE_VERSION_BTW_MCU_DSP 0x20180517

//=================================================================================================

typedef enum {
    // CPU to MCU Message: Request  13
    VMT_CM_HELLO = 0x0,
    VMT_CM_LOAD_MCU,                // Load MCU Firmware from DDR
    VMT_CM_LOAD_DSP,                // Load DSP Firmware from DDR
    VMT_CM_SET_BUFFER,              // Set DDR buffer (VSP, LED, ...)
    VMT_CM_SWITCH_MODE,             // Set VSP workmode
    VMT_CM_PROCESS,
    VMT_CM_SET_PARAM,
    VMT_CM_FEED_DATA,               // Feed data
    VMT_CM_GET_BATTERY_INFO,        // Get Battery INFO
    VMT_CM_PM_REQUEST,              // Power Management Request: power off, reboot, suspend
    VMT_CM_SET_WAKEUP_MASK,         // Set wakeup mask
    VMT_CM_EXIT_VSP,
    VMT_CM_ENTER_UPGRADE,

    // MCU to CPU Message: Response 13
    VMT_MC_MCU_INFO = 0x0,
    VMT_MC_LOAD_MCU_DONE,           // Load MCU done
    VMT_MC_LOAD_DSP_DONE,           // Load DSP done
    VMT_MC_SET_BUFFER_DONE,         // Set DDR done
    VMT_MC_SWITCH_MODE_DONE,        // Switch Mode done
    VMT_MC_PROCESS_DONE,            // A context is processed
    VMT_MC_SET_PARAM_DONE,
    VMT_MC_LOG,                     // Print MCU or DSP message in Kernel
    VMT_MC_SYNC_LED_QUEUE,          // Sync LED Queue
    VMT_MC_GET_BATTERY_INFO_DONE,   // Battery VOLTAGE
    VMT_MC_SET_WAKEUP_MASK_DONE,
    VMT_MC_EXIT_VSP_DONE,
    VMT_MC_ENTER_UPGRADE_DONE,
    VMT_MC_SEND_COMMAND,

    //--------------------------------------------------------------------------

    // MCU to DSP Message: Request  8
    VMT_MD_HELLO = 0x0,             // Init a DSP module
    VMT_MD_PROCESS,                 // a frame of data is available
    VMT_MD_CLEANUP,                 // Call DSP to clean up
    VMT_MD_HALT,                    // Halt DSP to waitmode (no need maybe)
    VMT_MD_SET_PARAM,               // Call DSP to set parameters
    VMT_MD_RUN_SNPU_TASK_DONE,      // Post result of SNPU task
    VMT_MD_REQUEST_AUTHORIZE_KEY_DONE,  // Post authorize for algorithm
    VMT_MD_REQUEST_FLASH_BRIDGE_DONE,            // Response VMT_DM_REQUEST_FLASH_BRIDGE

    // DSP to MCU Message: Response 9
    VMT_DM_HELLO_DONE = 0x0,        // Feedback from DSP module Echo
    VMT_DM_PROCESS_DONE,
    VMT_DM_PROCESS_ERROR,
    VMT_DM_CLEANUP_DONE,
    VMT_DM_LOG,                     // Print DSP message in MCU or CPU
    VMT_DM_SET_PARAM_DONE,
    VMT_DM_RUN_SNPU_TASK,           // Run a SNPU Task
    VMT_DM_REQUEST_AUTHORIZE_KEY,       // Request authorize for algorithm
    VMT_DM_REQUEST_FLASH_BRIDGE,            // Request mcu read/write flash

} VSP_MSG_TYPE;

//-------------------------------------------------------------------------------------------------

typedef union {
    unsigned int value;
    struct {
        VSP_MSG_TYPE type:4;        // messgae type
        unsigned size:4;            // message body size in unit of 32bit (not include header)
        unsigned magic:8;           // message magic code
        unsigned param:16;          // some message can contain a small parameter in header
    };
} VSP_MSG_HEADER;

//=================================================================================================
// Enumeration definition MCU <--> CPU

typedef enum {
    VSP_MODE_IDLE,
    VSP_MODE_BOOT,
    VSP_MODE_STANDBY,
    VSP_MODE_ACTIVE,
    VSP_MODE_MODEM,
    VSP_MODE_BYPASS,
    VSP_MODE_TEST,
    VSP_MODE_UAC,
    VSP_MODE_FACTORY,
    VSP_MODE_PLC,
    VSP_MODE_CODEC
} VSP_MODE_TYPE;

typedef enum {
    VSP_BUFFER_VSP,                 // VSP Buffer
    VSP_BUFFER_LED,                 // LED Buffer
    VSP_BUFFER_CMD,                 // LED Buffer
} VSP_BUFFER_TYPE;

typedef enum {
    VSP_CPU_WAKEUP_REASON_COLD = 0,
    VSP_CPU_WAKEUP_REASON_BUTTON,   // User press button
    VSP_CPU_WAKEUP_REASON_KEYWORD,  // User Speak a keyword
    VSP_CPU_WAKEUP_REASON_WIFI,     // WiFi Module
    VSP_CPU_WAKEUP_REASON_RTC,      // RTC
    VSP_CPU_WAKEUP_REASON_CHARGER,  // Wall plug
    VSP_CPU_WAKEUP_REASON_ILLEGAL,  // A7 illegal operation of standby
    VSP_CPU_WAKEUP_REASON_OTHER,    // Other cases
} VSP_CPU_WAKE_UP_REASON;

typedef enum {
    VSP_PM_REQUEST_REBOOT,
    VSP_PM_REQUEST_POWEROFF,
    VSP_PM_REQUEST_SUSPEND,
} VSP_PM_REQUEST_TYPE;

//=================================================================================================
// Message Body, CPU -> MCU, Request

// VMT_CM_HELLO
typedef struct {                    // 1 x 32bits
    unsigned int message_version;
} VMB_CM_HELLO;

// VMT_CM_LOAD_MCU
typedef struct {                    // 2 x 32bits
    void        *main_addr;
    unsigned int main_size;
} VMB_CM_LOAD_MCU;

// VMT_CM_LOAD_DSP
typedef struct {                    // 7 x 32bits
    void        *ptcm_addr;
    unsigned int ptcm_size;
    void        *dtcm_addr;
    unsigned int dtcm_size;
    void        *sram_addr;
    unsigned int sram_size;
    void        *sram_base;
    void        *ddr_addr;
    unsigned int ddr_size;
    void        *ddr_base;
} VMB_CM_LOAD_DSP;

// VMT_CM_SET_BUFFER
typedef struct {                    // 7 x 32bits
    void *mic_buffer;
    void *ref_buffer;
    void *ctx_buffer;
    void *ctx_header;
    void *log_buffer;
    void *tmp_buffer;
    void *cmd_buffer;
    unsigned int total_size;
} VMB_CM_SET_BUFFER_VSP;

typedef struct {                    // 2 x 32bits
    void *queue_buffer;
    void *frame_buffer;
} VMB_CM_SET_BUFFER_LED;

typedef struct {                    // 3 x 32bits
    void *cmd_buffer;
    void *cmd_header;
    unsigned int total_size;
} VMB_CM_SET_BUFFER_CMD;

// VMT_CM_SWITCH_MODE has no body

// VMT_CM_PROCESS
typedef struct {                    // 3 x 32bits
    void        *context_addr;
    unsigned int context_size;
} VMB_CM_PROCESS;

// VMT_CM_SET_PARAM
typedef struct {                    // 2 x 32bits
    void        *param_addr;
    unsigned int param_size;
} VMB_CM_SET_PARAM;

// VMT_CM_FEED_DATA
typedef struct {                    // 2 x 32bits
    unsigned int context_index;
} VMB_CM_FEED_DATA;

// VMT_CM_GET_BATTERY_INFO has no body
// VMT_CM_PM_REQUEST has no body
// VMT_CM_SET_WAKEUP_MASK has no body
typedef struct {
    unsigned int exit_status;
} VMB_CM_EXIT_VSP;

typedef struct {                    // 8 x 32bits (FULL)
    VSP_MSG_HEADER header;
    union {
        VMB_CM_HELLO hello;
        VMB_CM_LOAD_MCU load_mcu;
        VMB_CM_LOAD_DSP load_dsp;
        VMB_CM_SET_BUFFER_VSP set_buffer_vsp;
        VMB_CM_SET_BUFFER_LED set_buffer_led;
        VMB_CM_SET_BUFFER_CMD set_buffer_cmd;
        VMB_CM_PROCESS process;
        VMB_CM_SET_PARAM set_param;
        VMB_CM_FEED_DATA feed_data;
        VMB_CM_EXIT_VSP status;
    };
} VSP_MSG_CPU_MCU;

//-------------------------------------------------------------------------------------------------
// Message Body, MCU -> CPU, Response

// VMT_MC_MCU_INFO
typedef struct {                    // 7 x 32bits
    unsigned message_version:32;        // Message Version

    unsigned mic_num:4;                 // 1 -   8 channels
    unsigned ref_num:3;                 // 0 -   2 channels
    unsigned out_interlaced:1;          // 1: interlace, 0: non-interlace
    unsigned out_num:8;                 // 0 - 128 channels
    unsigned frame_length:6;            // 10ms, 16ms
    unsigned frame_num:10;              // 0 - 1023 units       (enough)

    unsigned sample_rate:16;            // 8000, 16000, 48000 Hz
    unsigned led_number:8;              // 0 - 255 LEDs
    unsigned led_queue_length:8;        // 0 - 255 Frames

    unsigned context_size:20;           // 0 - 1048575 bytes    (enough)
    unsigned context_num:12;            // 0 - 4095 contexts    (enough)

    unsigned frame_num_per_context:4;   // 0 - 10 frames        (enough)
    unsigned features_dim:12;           // 0 - 2048 dims        (enough)
    unsigned ext_buffer_size:16;        // extra buffer size in KBytes;

    unsigned log_buffer_size:16;        // bytes
    unsigned tmp_buffer_size:16;        // temparory buffer size in KBytes;

    unsigned cmd_num:4;                 // 0 - 16 command queue length
    unsigned cmd_size:16;               // 0 - 1048575 bytes
    unsigned cmd_data_size:4;           // cmd data size in 4KBytes;
    unsigned :8;

} VMB_MC_MCU_INFO;

// VMT_MC_LOAD_MCU_DONE has no body
// VMT_MC_LOAD_DSP_DONE has no body
// VMT_MC_SET_BUFFER_DONE has no body
// VMT_MC_SWITCH_MODE_DONE has no body

// VMT_MC_PROCESS_DONE
typedef struct {                    // 2 x 32bits
    void        *context_addr;
    unsigned int context_size;
} VMB_MC_PROCESS_DONE;

// VMT_MC_SET_PARAM_DONE
typedef struct {
    void        *param_addr;
    unsigned int param_size;
} VMB_MC_SET_PARAM_DONE;

// VMT_MC_LOG                       // 2 x 32bits
typedef struct {
    unsigned char *log_buffer;
    unsigned int log_size;
} VMB_MC_LOG;

// VMT_MC_SYNC_LED_QUEUE has no body

// VMT_MC_GET_BATTERY_INFO_DONE
typedef struct {
    unsigned int voltage;
} VMB_MC_GET_BATTERY_INFO_DONE;

// VMT_MC_SET_WAKEUP_MASK_DONE has no body

// VMT_MC_SEND_COMMAND
typedef struct {
    void        *command_addr;
    unsigned int command_size;
} VMB_MC_SEND_COMMAND;

typedef struct {                    // 8 x 32bits
    VSP_MSG_HEADER header;
    union {
        VMB_MC_MCU_INFO mcu_info;
        VMB_MC_PROCESS_DONE process_done;
        VMB_MC_LOG log;
        VMB_MC_GET_BATTERY_INFO_DONE get_battery_info_done;
        VMB_MC_SET_PARAM_DONE set_param_done;
        VMB_MC_SEND_COMMAND send_command;
    };
} VSP_MSG_MCU_CPU;

//=================================================================================================
// Enumeration definition MCU <--> DSP

typedef enum {
    VSP_RPOCESS_CALIBRATION,        // do micphone calibration while booting
    VSP_PROCESS_ACTIVE,
    VSP_PROCESS_STANDBY,
    VSP_PROCESS_CTC,
    VSP_PROCESS_MODEM,
    VSP_PROCESS_TALKING,
    VSP_PROCESS_TEST,              // do OPUS encoding of 2-channel MIC audio, used to testing MIC performance from wireless devices(BLE etc.)
} VSP_PROCESS_TYPE;

//-------------------------------------------------------------------------------------------------
// Message Body, MCU -> DSP, Request

// VMT_MD_HELLO
typedef struct {                    // 3 x 32bits
    unsigned int message_version;
    void        *context_header_addr;
    unsigned int context_header_size;
} VMB_MD_HELLO;

// VMT_MD_PROCESS
typedef struct {                    // 2 x 32bits
    void        *context_addr;
    unsigned int context_size;
} VMB_MD_PROCESS;

// VMT_MD_CLEANUP has no body
// VMT_MD_HALT has no body

// VMT_MD_SET_PARAM
typedef struct {
    void        *param_addr;        // 2 x 32bits
    unsigned int param_size;
} VMB_MD_SET_PARAM;

// VMT_MD_RUN_SNPU_TASK_DONE
typedef struct {                    // 3 x 32bits
    unsigned int module_id;
    unsigned int state;
    void        *private_data;
} VMB_MD_RUN_SNPU_TASK_DONE;

// VMT_MD_REQUEST_AUTHORIZE_KEY_DONE
typedef struct {
    void        *key_addr;          // 2 x 32bits
    unsigned int key_size;
} VMB_MD_REQUEST_AUTHORIZE_KEY_DONE;

// VMT_MD_REQUEST_FLASH_BRIDGE_DONE
typedef struct {
    void        *addr;          // 2 x 32bits
    unsigned int size;
} VMB_MD_RESPONSE_FLASH_BRIDGE;


typedef struct {                    // 5 x 32bits
    VSP_MSG_HEADER header;
    union {
        VMB_MD_HELLO hello;
        VMB_MD_PROCESS process;
        VMB_MD_SET_PARAM set_param;
        VMB_MD_RUN_SNPU_TASK_DONE run_snpu_task_done;
        VMB_MD_REQUEST_AUTHORIZE_KEY_DONE request_authorize_key_done;
        VMB_MD_RESPONSE_FLASH_BRIDGE response_flash_bridge;
    };
} VSP_MSG_MCU_DSP;

//-------------------------------------------------------------------------------------------------
// Message Body, DSP -> MCU, Response

// VMT_DM_HELLO
typedef struct {                    // 1 x 32bits
    unsigned int message_version;
} VMB_DM_HELLO_DONE;

// VMT_DM_PROCESS_DONE & VMT_DM_PROCESS_ERROR
typedef struct {                    // 2 x 32bits
    void        *context_addr;
    unsigned int context_size;
} VMB_DM_PROCESS_DONE;

// VMT_DM_CLEANUP_DONE has no body

// VMT_DM_LOG
typedef struct {                    // 2 x 32bits
    char        *addr;
    unsigned int size;
} VMB_DM_LOG;

// VMT_DM_SET_PARAM_DONE
typedef struct {                    // 2 x 32bits
    void        *param_addr;
    unsigned int param_size;
} VMB_DM_SET_PARAM_DONE;

// VMT_DM_RUN_SNPU_TASK
typedef struct {                    // 8 x 32bits
    unsigned int module_id;         // module_id defined by programmer
    void        *ops;               // ops_content in model.c
    void        *data;              // cpu_content in model.c
    void        *input;             // input in model.c
    void        *output;            // output in model.c
    void        *cmd;               // npu_content in model.c
    void        *tmp_mem;           // tmp_content in model.c
    void        *priv_data;
} VMB_DM_RUN_SNPU_TASK;

// VMT_DM_REQUEST_AUTHORIZE_KEY
typedef struct {                    // 4 x 32bits
    void        *input_buffer;
    unsigned int input_size;
    void        *output_buffer;
    unsigned int output_size;
} VMB_DM_REQUEST_AUTHORIZE_KEY;

// VMT_DM_REQUEST_FLASH_BRIDGE
typedef struct {                    // 3 x 32bits
    enum {
        FLASH_BRIDGE_WRITE,
        FLASH_BRIDGE_READ,
    } cmd;
    void        *addr;
    unsigned int size;
} VMB_DM_REQUEST_FLASH_BRIDGE;

typedef struct {                    // 9 x 32bits
    VSP_MSG_HEADER header;
    union {
        VMB_DM_HELLO_DONE hello_done;
        VMB_DM_PROCESS_DONE process_done;
        VMB_DM_LOG log;
        VMB_DM_SET_PARAM_DONE set_param_done;
        VMB_DM_RUN_SNPU_TASK run_snpu_task;
        VMB_DM_REQUEST_AUTHORIZE_KEY request_authorize_key;
        VMB_DM_REQUEST_FLASH_BRIDGE request_flash_bridge;
    };
} VSP_MSG_DSP_MCU;

//=================================================================================================

#endif /* __VSP_MESSAGE_H__ */
