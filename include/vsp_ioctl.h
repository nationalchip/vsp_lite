/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_ioctl.h: VSP I/O Control command between user space application and device driver
 *
 */

#ifndef __VSP_IOCTL_H__
#define __VSP_IOCTL_H__

#define VSP_DEVICE_NAME     "vsp"
#define VSP_IRQ_NAME        "vsp"
#define VSP_CLASS_NAME      "gxvsp"

#define VSP_IOC_VERSION     0x20190819

//=================================================================================================
// For VSP_IOC_SWITCH_MODE

typedef enum {
    VSP_IOC_MODE_IDLE,
    VSP_IOC_MODE_BOOT,
    VSP_IOC_MODE_STANDBY,
    VSP_IOC_MODE_ACTIVE,
    VSP_IOC_MODE_MODEM,
    VSP_IOC_MODE_BYPASS,
    VSP_IOC_MODE_TEST,
    VSP_IOC_MODE_UAC,           // For Test
    VSP_IOC_MODE_FACTORY,
    VSP_IOC_MODE_PLC,
} VSP_IOC_MODE_TYPE;

//-------------------------------------------------------------------------------------------------
// For VSP_IOC_LOAD_DSP and VSP_IOC_LOAD_MCU

typedef struct {
    void        *buffer;
    unsigned int size;
} VSP_IOC_FIRMWARE;

//-------------------------------------------------------------------------------------------------
// For VSP_IOC_GET_INFO

typedef struct {
    /* Version related */
    unsigned int    msg_version;
    unsigned int    ioc_version;
    /* Audio related */
    unsigned int    sample_rate;
    unsigned int    mic_num;
    unsigned int    ref_num;
    unsigned int    out_num;
    unsigned int    out_interlaced;
    unsigned int    frame_length;
    unsigned int    frame_num;              /* per context */
    /* Result related */
    unsigned int    features_dim;           /* per frame */
    unsigned int    ext_buffer_size;
    /* Led related */
    unsigned int    led_num;
    /* Commannd related */
    unsigned int    cmd_num;
    unsigned int    cmd_size;
    unsigned int    cmd_data_size;
} VSP_IOC_INFO;

//-------------------------------------------------------------------------------------------------
// For VSP_IOC_GET_CONTEXT and VSP_IOC_PUT_CONTEXT

typedef struct {
    void           *addr;
    unsigned int    size;
} VSP_IOC_BUFFER;

typedef struct {
    unsigned        mic_mask:16;            /* output */
    unsigned        ref_mask:16;            /* output */
    unsigned int    frame_index;            /* output */
    unsigned int    ctx_index;              /* output */
    unsigned        vad;                    /* output */
    unsigned int    kws;                    /* output */
    unsigned int    mic_gain;               /* input */
    unsigned int    ref_gain;               /* input */
    unsigned int    direction;              /* 0 - 360 degree */
    VSP_IOC_BUFFER  features;
    VSP_IOC_BUFFER  out_buffer;             /* only 1 channel */
    VSP_IOC_BUFFER  mic_buffer[8];          /* max 8 channel */
    VSP_IOC_BUFFER  ref_buffer[2];          /* max 2 channel */
    VSP_IOC_BUFFER  ext_buffer;
} VSP_IOC_CONTEXT;

//-------------------------------------------------------------------------------------------------
// For VSP_IOC_PUT_LED_FRAME

typedef union {
    unsigned int value;
    struct {
        unsigned r:8;
        unsigned g:8;
        unsigned b:8;
        unsigned a:8;
    }bits;
} VSP_IOC_PIXEL;

typedef struct {
    unsigned short  transition;             /* transition time, in ms, should <= duration */
    unsigned short  duration;               /* duration time, in ms */
    VSP_IOC_BUFFER  pixels;                 /* list of pixels */
} VSP_IOC_LED_FRAME;

//-------------------------------------------------------------------------------------------------
// For VSP_IOC_GET_MCU_COMMAND
typedef struct {
    unsigned int    cmd_id;
    unsigned int    cmd_index;
    VSP_IOC_BUFFER  cmd_data;
} VSP_IOC_COMMAND;

enum usb_device_status {
    USB_DEVICE_SUSPEND    = 0,  // not connect
    USB_DEVICE_RESET      = 1,  // connected
    USB_DEVICE_ADDRESSED  = 2,  // assigned address
    USB_DEVICE_CONFIGURED = 3,  // ready work
    USB_DEVICE_RESUME     = 4,  // reconnect, can be treated as reset
};

enum usb_func_status {
    USB_FUNC_STATUS_DISABLE,
    USB_FUNC_STATUS_ENABLE,
};

struct usb_status {
    int changed;

    union {
        enum usb_device_status status; // usb device enumeration status
        enum usb_func_status enable;   // usb functions status : uac/hid/..
    } st;
};

enum {
    HID_MSG_RING_REPORT_ID    = 3,
    HID_MSG_MUTE_REPORT_ID    = 4,
    HID_MSG_OFFHOOK_REPORT_ID = 5,
    HID_MSG_HOLD_REPORT_ID    = 6,
    HID_MSG_PRIVATE_REPORT_ID = 8,
};

#define HID_MSG_MAX_PACKET_SIZE 64
struct procotol_huachuangshixun {
    enum {
        // report id 3
        HID_MSG_RING,

        // report id 4
        HID_MSG_MUTE,

        // report id 5
        HID_MSG_OFFHOOK,

        // report id 6
        HID_MSG_HOLD,

        // report id 8
        HID_MSG_PRIVATE,

        // other
        HID_MSG_OTHER,
    } msg_type;

    union {
        int value; // for report id 3 - 6, value : 0 or 1

         // for report id 8
        struct {
            unsigned char content[HID_MSG_MAX_PACKET_SIZE];
            unsigned int  len;
        };
    };
};

struct hid_msg {
    int valid; // 1: valid, 0 : invalid

    union {
        struct procotol_huachuangshixun ph;
    };
};

// For VSP_IOC_GET_MCU_COMMAND
typedef struct {
    enum {
        TMP_BUF_OK,
        TMP_BUF_READ_OFFSET_TOO_LARGE,
        TMP_BUF_READ_SIZE_TOO_LARGE,
    } status;

    // tmp buff info
    unsigned int    total_size;
    unsigned int    read_offset;
    unsigned int    read_size;
    unsigned int    cycle_num;
    unsigned int    write_offset;

    // usb device status
    struct usb_status usb_dev_status;
    struct usb_status usb_uac_us_status; // uac upstream
    struct usb_status usb_uac_ds_status; // uac downstream
    struct usb_status usb_hid_status;

    // uac control info
    int uac_ctrl_info_valid;          /* 0 : invalid, 1 : valid  */
    enum {
        VSP_UAC_CTRL_DIR_DOWNSTREAM,
        VSP_UAC_CTRL_DIR_UPSTREAM,
    } uac_ctrl_dir;

    enum {
        VSP_UAC_CTRL_TYPE_UNKNOW,
        VSP_UAC_CTRL_TYPE_VOLUME,
        VSP_UAC_CTRL_TYPE_MUTE,
        VSP_UAC_CTRL_TYPE_AUTO_GAIN_CONTROL,
        VSP_UAC_CTRL_TYPE_BASS_BOOST,
    } uac_ctrl_type;

    union {
        int volume;
        int enable;
    } uac_ctrl_value;

    // hid control info
    struct hid_msg hid_msg;

    // bluetooth info, to be added

    VSP_IOC_BUFFER  data;
} VSP_IOC_UAC_INFO;
//-------------------------------------------------------------------------------------------------
// For VSP_IOC_GET_WAKEUP_REASON
// The Enum is clone of VSP_CPU_WAKE_UP_REASON

typedef enum {
    VSP_IOC_WAKEUP_REASON_COLD = 0,
    VSP_IOC_WAKEUP_REASON_BUTTON,           /* User press button */
    VSP_IOC_WAKEUP_REASON_KEYWORD,          /* User Speak a keyword */
    VSP_IOC_WAKEUP_REASON_WIFI,             /* WiFi Module */
    VSP_IOC_WAKEUP_REASON_RTC,              /* RTC */
    VSP_IOC_WAKEUP_REASON_CHARGER,          /* Wall plug */
} VSP_IOC_WAKE_UP_REASON;

//-------------------------------------------------------------------------------------------------
// For VSP_IOC_SET_PARAM

enum {
    UAC_MODE_MSG_HID_VOLUME_UP_REPORT_ID = 1,
    UAC_MODE_MSG_HID_VOLUME_DOWN_REPORT_ID = 1,

    UAC_MODE_MSG_HID_HOOKSWITH_REPORT_ID = 2,
    UAC_MODE_MSG_HID_PHONEMUTE_REPORT_ID = 2,

    UAC_MODE_MSG_HID_PRIVATE_REPORT_ID   = 7,
};

typedef enum uac_mode_msg_type {
    UAC_MODE_MSG_TYPE_NONE,
    UAC_MODE_MSG_TYPE_HID,
} uac_mode_msg_type_t;

// UAC_MODE_MSG_TYPE_HID msg
typedef enum {
    // report id 1
    UAC_MODE_MSG_HID_VOLUME_UP,
    UAC_MODE_MSG_HID_VOLUME_DOWN,
    // report id 2
    UAC_MODE_MSG_HID_HOOKSWITH,
    UAC_MODE_MSG_HID_PHONEMUTE,

    // report id 7
    UAC_MODE_MSG_HID_PRIVATE,
} uac_mode_hid_msg_type_t;

#define UAC_MODE_HID_MSG_MAX_CONTENT_LEN 200
typedef struct {
    uac_mode_hid_msg_type_t type;

    union {
        struct {
            unsigned char content[UAC_MODE_HID_MSG_MAX_CONTENT_LEN];
            unsigned int  len;
        };
    };
} uac_mode_hid_msg_t;

typedef struct uac_mode_msg {
    uac_mode_msg_type_t type;

    union {
        uac_mode_hid_msg_t hid;
    };
} VSP_PARAM_UAC_MODE_MSG;

typedef struct {
    void        *addr;
    unsigned int size;
} VSP_IOC_PARAM;

//=================================================================================================
// For VSP
#define VSP_IOC_SWITCH_MODE         (0x0101)    /* VSP_IOC_MODE_TYPE */
#define VSP_IOC_LOAD_DSP            (0x0102)    /* VSP_IOC_FIRMWARE */
#define VSP_IOC_LOAD_MCU            (0x0103)    /* VSP_IOC_FIRMWARE */
#define VSP_IOC_START_STREAM        (0x0104)
#define VSP_IOC_STOP_STREAM         (0x0105)
#define VSP_IOC_GET_INFO            (0x0106)    /* VSP_IOC_INFO */
#define VSP_IOC_SET_TIMEOUT         (0x0107)    /* integer, millisecond */
#define VSP_IOC_GET_CONTEXT         (0x0108)    /* VSP_IOC_CONTEXT */
#define VSP_IOC_PUT_CONTEXT         (0x0109)    /* VSP_IOC_CONTEXT */
#define VSP_IOC_SET_PARAM           (0x010A)    /* VSP_IOC_PARAM */
#define VSP_IOC_EXIT_VSP            (0x010B)    /* VSP_IOC_EXIT_VSP */
#define VSP_IOC_ENTER_UPGRADE       (0x010C)    /* VSP_IOC_ENTER_UPGRADE */
#define VSP_IOC_GET_MCU_COMMAND     (0x010D)    /* VSP_IOC_GET_MCU_COMMAND */
#define VSP_IOC_READ_TMP_BUFFER     (0x010E)    /* VSP_IOC_READ_TMP_BUFFER */

// For LED
#define VSP_IOC_PUT_LED_FRAME       (0x0201)    /* VSP_IOC_LED_FRAME */
#define VSP_IOC_FLUSH_LED_QUEUE     (0x0202)

// For Standby
#define VSP_IOC_GET_WAKEUP_REASON   (0x0301)    /* VSP_IOC_WAKE_UP_REASON */
#define VSP_IOC_SET_WAKEUP_MASK     (0x0302)    /* unsigned int */

// For PMU
#define VSP_IOC_GET_BATTERY_VOLTAGE (0x0403)    /* unsigned, 0 - 1024 */

#endif  /* __VSP_IOCTL_H__ */
