/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_context.h: Context definition
 *                Context is a data struct designed for pipeline, each phase of pipeline read
 *                  data from a context and write data back to the context.
 *                Context is common language between MCU, DSP, and CPU, so each core MUST check
 *                  the Context version before processing.
 *
 */

#ifndef __VSP_CONTEXT_H__
#define __VSP_CONTEXT_H__

#define VSP_CONTEXT_VERSION 0x20210910
#include<autoconf.h>
#ifdef CONFIG_VSP_FRAME_BIT_16
typedef short SAMPLE;
#endif

#ifdef CONFIG_VSP_FRAME_BIT_24
typedef int SAMPLE;
#endif
//=================================================================================================
// Audio Process Context shared with MCU, DSP and CPU

typedef struct {
    unsigned int        version;

    unsigned int        mic_num;                // 1 - 8
    unsigned int        ref_num;                // 0 - 2
    unsigned int        out_num;                // 1 - 15
    unsigned int        out_interlaced;         // 1: interlace or 0: non-interlace
    unsigned int        frame_num_per_context;  // the FRAME num in a CONTEXT
    unsigned int        frame_num_per_channel;  // the total FRMAE num in a CHANNEL
    unsigned int        frame_length;           // 10ms, 16ms
    unsigned int        sample_rate;            // 8000, 16000, 48000

    int                 ref_offset_to_mic;      // MIC signal offset to REF in unit of context.
                                                //   If REF is saved before MIC, the offset is > 0;
                                                // otherwise, it will be < 0.

    unsigned int        features_dim_per_frame; // Features dimension per FRAME

    // CTX buffer for GLOBAL
    void               *ctx_buffer;             // Context Buffer header point
    unsigned int        ctx_num;                // Context number
    unsigned int        ctx_size;               // Context size

    // SNPU, EXTRA and OUT buffer for CONTEXT
    unsigned int        snpu_buffer_size;       // Bytes
    unsigned int        ext_buffer_size;        // Bytes
    unsigned int        out_buffer_size;        // Bytes

    // MIC buffer for GLOBAL
    SAMPLE             *mic_buffer;             // DEVICE ADDRESS
    unsigned int        mic_buffer_size;        // Bytes
    // REF buffer for GLOBAL
    SAMPLE             *ref_buffer;             // DEVICE ADDRESS
    unsigned int        ref_buffer_size;        // Bytes
    // TMP buffer for GLOBAL
    void               *tmp_buffer;             // DEVICE ADDRESS
    unsigned int        tmp_buffer_size;        // Bytes
} VSP_CONTEXT_HEADER;

typedef struct {
    VSP_CONTEXT_HEADER *ctx_header;             // DEVICE ADDRESS
    unsigned            mic_mask:16;
    unsigned            ref_mask:16;
    unsigned int        frame_index;            // FRAME index of the first frame in CONTEXT
    unsigned int        ctx_index;              // CONTEXT index from 0 - (2^32 - 1)
    unsigned int        vad:8;
    unsigned int        kws:8;
    unsigned int        mic_gain:8;
    unsigned int        ref_gain:8;
    int                 direction;
    SAMPLE             *out_buffer;             // DEVICE ADDRESS
    void               *features;               // DEVICE ADDRESS
    void               *snpu_buffer;            // DEVICE ADDRESS
    void               *ext_buffer;             // DEVICE ADDRESS
} VSP_CONTEXT;

//=================================================================================================
#endif /* __VSP_CONTEXT_H__ */
