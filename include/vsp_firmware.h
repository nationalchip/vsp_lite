/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 Nationalchip Co., Ltd
 *
 * vsp_firmware.h: Firmware Binary Header Definition
 *                This header file is shared between MCU and CPU
 */

#ifndef __VSP_FIRMWARE_H__
#define __VSP_FIRMWARE_H__

#define VSP_FIRMWARE_MAGIC_DSP "VDSP"
#define VSP_FIRMWARE_MAGIC_MCU "VMCU"

typedef struct {
    unsigned int magic;
    unsigned int message_version;
    unsigned int ptcm_size;
    unsigned int dtcm_size;
    unsigned int sram_size;
    unsigned int sram_base;
    unsigned int ddr_size;
    unsigned int ddr_base;
    unsigned int xip_size;
    unsigned int reserved[7];
} VSP_FIRMWARE_DSP_HEADER;

typedef struct {
    unsigned int magic;
    unsigned int message_version;
    unsigned int size;
} VSP_FIRMWARE_MCU_HEADER;

#endif /* __VSP_FIRMWARE_H__ */
