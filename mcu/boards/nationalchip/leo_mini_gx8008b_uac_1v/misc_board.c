/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Misc settings for GX8008B_UAC_V1.0_20190422 board
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/misc.h>
#include <driver/padmux.h>
#include <driver/gpio.h>

//=================================================================================================
// GPIO For LED

#define UAC_LED_PORT_R 8
#define UAC_LED_PORT_G 6
#define UAC_LED_PORT_B 7

//=================================================================================================
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0   | function1   | function2   | function3  | function4 */
    { 1, 0},  // UART1RX     | PD1PORT01
    { 2, 0},  // UART0RX     | PD1PORT02
    { 3, 0},  // UART0TX     | PD1PORT03
    { 4, 0},  // UART1TX     | OTP_AVDD_EN | PD1PORT04
//  { 5, 0},  // SDBGTDI     | DDBGTDI     | UART0_RTS   | SDA0       | PD1PORT05
//  { 6, 0},  // SDBGTDO     | DDBGTDO     | UART0_CTS   | SCL0       | PD1PORT06
//  { 7, 0},  // SDBGTMS     | DDBGTMS     | SDA1        | PCM1INBCLK | PD1PORT07
//  { 8, 0},  // SDBGTCK     | DDBGTCK     | SDA1        | PCM1INLRCK | PD1PORT08
//  { 9, 0},  // SDBGTRST    | DDBGTRST    | SCL1        | PCM1INDAT0 | PD1PORT09
    {10, 1},  // PCM0OUTMCLK | DUARTTX     | SDA0        | SPI1SCK    | PD1PORT14
//  {11, 0},  // PCM0OUTAT0  | SPI1MOSI    | SCL0        | PD1PORT15
//  {12, 0},  // PCM0OUTLRCK | SPI1CSn     | PD1PORT16
//  {13, 0},  // PCM0OUTBCLK | SPI1MISO    | PD1PORT17
    {14, 0},  // SPI0WP      | SDA1        | PD1PORT28
    {15, 0},  // SPI0HOLD    | SCL1        | PD1PORT29
//  {16, 0},  // PCM0INDAT0  | IR          | PD1PORT31
//  {17, 0},  // PCM0INMCLK  | PDMDAT1     | PD1PORT32
//  {18, 0},  // PCM0INLRCK  | PDMDAT0     | PCM0OUTLRCK | PD1PORT33
//  {19, 0},  // PCM0INBCLK  | PDMCLK      | PCM0OUTBCLK | PD1PORT34
};

void BoardInit(void)
{
    int table_size = ARRAY_SIZE(_pad_configs);

    PadMuxInit(_pad_configs, table_size);

    for (int i = 0; i < table_size; i++) {
        int pad_function = PadMuxGetFunction(_pad_configs[i].pad_id);
        if (pad_function != _pad_configs[i].pad_function)
            printf("Fail to setup Pad %d, should be %d, but %d!\n",
                   _pad_configs[i].pad_id,
                   _pad_configs[i].pad_function, pad_function);
    }

    PadMuxDumpRegs();
}

void BoardDone(void)
{

}
//-------------------------------------------------------------------------------------------------
void BoardSuspend(void)
{
    // Turn off External Amplifier
    GpioSetDirection(31, GPIO_DIRECTION_INPUT);

    // Turn off LED Output Enable
    GpioSetDirection(UAC_LED_PORT_R, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_G, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_B, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_HIGH);

    // Turn Down Core 1.1 Voltage
    GpioEnablePWM(5, 1000*1000, 60);
    GpioSetDirection(5, GPIO_DIRECTION_OUTPUT);
}

void BoardResume(void)
{
    // Recover Core 1.1 Voltage
    GpioSetDirection(5, GPIO_DIRECTION_INPUT);
    GpioDisablePWM(5);

    // Turn on External Amplifier
}
//=================================================================================================
// LED
int LedInit(void)
{
    GpioSetDirection(UAC_LED_PORT_R, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_G, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_B, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_HIGH);

    return 0;
}

int LedSetPixel(unsigned int index, LED_PIXEL pixel)
{
    switch (index)
    {
        case 0:
            GpioSetLevel(UAC_LED_PORT_R, !pixel.bits.r);
            GpioSetLevel(UAC_LED_PORT_B, !pixel.bits.b);
            GpioSetLevel(UAC_LED_PORT_G, !pixel.bits.g);
            break;
        default:
            break;
    }

    return 0;
}

int LedFlush(void)
{
    return 0;
}

int LedDone(void)
{
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_HIGH);

    return 0;
}
