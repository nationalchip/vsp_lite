/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * boot_board.c: Boot Board Configurations for GX8008B_UAC_V1.0_20190422 board
 *
 */

#include <autoconf.h>
#include <boot/boot_padmux.h>
#include <boot/boot_gpio.h>
#include <board_config.h>

#define   CONFIG_BASE              0xa030a000
#define   CONFIG_DTO_BASE          0x28
#define   CONFIG_SOURCE_SEL        0x170
#define   PLL_AUDIO_CONFIG_BASE    0xc0
#define   PLL_DTO_CONFIG_BASE      0xc8
#define   PLL_ARM_CONFIG_BASE      0xcc
#define   CONFIG_CLOCK_DIV_CONFIG  0x24
#define   CONFIG_CLOCK_DIV_CONFIG2 0x178
#define   CONFIG_CLOCK_DIV_CONFIG  0x24
#define   ROM_GETCH_VARIABLE_ADDR  0xa0103adc
#define   USB_CONFIG4              0x110

// fo = fi*div/2^30
// div = fo*2^30/fi
// dto select the bus, fi is dto's pll fre, fo is bus fre (output fre), here fi is 1188000000
void DTO_Config(unsigned int dto, unsigned int div)
{
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31);
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = 0;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31);
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | div;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | (1 << 30) | div;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | (1 << 30) | div;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | div;
}

/* pll.c */
static struct param{
    unsigned int   freq;
    unsigned char  clkbp;
    unsigned char  clkod;
    unsigned char  clkn;
    unsigned char  clkm;
} param_table[] = {
    {1200000000, 0, 0, 0, 49 }, // 1.2GHz     DTO
    { 761856000, 0, 1, 0, 30 }, // 761.856MHz AUDIO PLL
};

// fvco = freq_xtal * clkm / clkn,其中freq_xtal是晶振频率12000000
// fclkout = fvco / (2 ^ clkod)
void PLL_Config(unsigned int pll, unsigned int freq)
{
    int i;
    volatile unsigned int j = 100;
    for (i=0; i < sizeof(param_table) / sizeof(struct param); i++) {
        if (freq == param_table[i].freq) {
            //unsigned int clkbp = param_table[i].clkbp;
            unsigned int clkod = param_table[i].clkod;
            unsigned int clkn = param_table[i].clkn;
            unsigned int clkm = param_table[i].clkm;

            *(volatile unsigned int*)(pll) = (1<<15)|(1<<14)|(clkod<<12)|(clkn<<7)|clkm;
            while(j--);
            *(volatile unsigned int*)(pll) = (0<<15)|(1<<14)|(clkod<<12)|(clkn<<7)|clkm;
            j=100;
            while(j--);
            *(volatile unsigned int*)(pll) = (0<<15)|(0<<14)|(clkod<<12)|(clkn<<7)|clkm;
        }
    }
    j=100;
    while(j--);
}

/* cpu_config.c */
void gx_setup_pll_mini_controller(void)
{
    *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) = 0;
    //PLL DTO
    PLL_Config(CONFIG_BASE + PLL_DTO_CONFIG_BASE,   1200000000);// DTO:     1200M
    // PLL Audio
    PLL_Config(CONFIG_BASE + PLL_AUDIO_CONFIG_BASE, 761856000); // AUDIO:   761.856M
}

static struct pll_dto {
    unsigned int dto;
    unsigned int div;
    unsigned int value;
} dto_table[] = {
    {1, 0x07FFFFFF,  1      },  // MCU            150 MHz
    /*{2  , 0x02E147AE , 1 <<  1} ,  // APB2_0       54MHz*/
    {3 , 0x0A3D70A4, 1 <<  2} ,  // APB2_uart     192MHz
#if defined(CONFIG_DSP_FREQUENCE_400M)
    {5, 0x15555555, 1 << 16},  // DSP           400 MHz
#else
    {5, 0x1B851EB8, 1 << 16},  // DSP           516 MHz
#endif
    {6 , 0x07FFFFFF, 1 <<  5} ,  // Secure        150MHZ
    {7 , 0x00A7C5AC, 1 <<  9} ,  // Audio srl     12.288MHz
    {8 , 0x014F8B59, 1 << 29} ,  // Audio in      24.576MHz
    {9 , 0x10000000, 1 << 10} ,  // sram          300MHZ
#if CONFIG_FLASH_SPI_CLK_SRC==200000000
    {10 , 0x0aaaaaaa , 1 << 17} ,  // spi_flash     200MHz
#elif CONFIG_FLASH_SPI_CLK_SRC==150000000
    {10 , 0x07FFFFFF , 1 << 17} ,  // spi_flash     150MHz
#else
#error "set not supported value to CONFIG_FLASH_SPI_CLK_SRC!"
#endif
    {11, 0x0aaaaaaa, 1 << 18} ,  // spi_master    200MHz
    {12, 0x0A3D70A4, 1 << 19} ,  // apb2_uart2    192MHz
    {13, 0x02aaaaab, 1 << 21} ,  // ref_clk_out1  50MHz
    {14, 0x02aaaaab, 1 << 20} ,  // ref_clk_out0  50MHz
    {15, 0x00a3d70a, 1 << 22} ,  // otp_clk       12MHz
};

void gx_setup_pll_full_controller(void)
{
    int i;

    for (i=0; i < sizeof(dto_table) / sizeof(struct pll_dto); i++) {
        DTO_Config(dto_table[i].dto, dto_table[i].div);
        *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) |= dto_table[i].value;
    }

    //clock audio in select MUX
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) |=  (1<<31);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) |=  (1<<26);  //选择时钟来自audio PLL

    //clock audio In DIV
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<23);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |=  (1<<23); //rst
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(0x3f<<16);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (30<<16); //761.856MHz / 31 分频 = 24.5714MHz
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<22);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<22); //load end

    //USB slave clock In DIV
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG2) &= ~(1<<11);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG2) |=  (1<<11); //rst
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG2) &= ~(0xff<<2);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG2) |= (99<<2); //1200MHz / 100 分频 = 12MHz
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG2) &= ~(1<<10);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG2) |= (1<<10); //load end

    //USB slave clock select
    *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) |= (1<<11); // select clock from usb phy
    *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) &= ~(1<<12); // usb phy from xtal
    *((volatile unsigned int *)(CONFIG_BASE + USB_CONFIG4)) |= (1 << 6);

    //clock audio clock_audio_lodec
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<31);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<31); //rst
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(0x3f<<24);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (23<<24); //div//24分频、、1200/24=50M
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<30);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<30); //load end
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) |= (1<<6);// source div lodec
}

void BootBoardInit(void)
{
    gx_setup_pll_mini_controller();
    gx_setup_pll_full_controller();
}

//=================================================================================================

#ifdef CONFIG_BOARD_SUPPORT_MULTIBOOT
int BootBoardGetImageInfo(unsigned int *image_addr, unsigned int *image_size)
{
    BootPadMuxSetFunction(28, 1);
    BootGpioSetDirection(28, GPIO_DIRECTION_INPUT);
    if (BootGpioGetLevel(28) == GPIO_LEVEL_LOW) {
        *image_addr = CONFIG_BOARD_GX8008B_UAC_V1_SECOND_IMAGE_ADDR;
        *image_size = CONFIG_BOARD_GX8008B_UAC_V1_SECOND_IMAGE_SIZE_KB * 1024;
    }
    return 0;
}
#endif
