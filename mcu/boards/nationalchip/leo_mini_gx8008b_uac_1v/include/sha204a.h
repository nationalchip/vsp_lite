/* IFLYTEK Authorization Security Chip(SHA204A) driver
 * Copyright (C) 2001-2018 IFLYTEK Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * sha204a.h: Security Chip driver
 *
 */

#ifndef __ATSHA204A_H__
#define __ATSHA204A_H__

typedef void *handle_t;
typedef unsigned char byte_t;
typedef unsigned short word_t;
typedef byte_t digest_t[0x08];
typedef byte_t cipher_t[0x20];

/**
 *  @brief      Authorization process interface
 *  @param      [i]digest   digest information, 8bytes
 *              [o]cipher   cipher information, 32bytes
 *  @return     true        authorization success
 *              false       authorization failed
**/
bool ifly_auth_proc(digest_t *digest, cipher_t *cipher);


#endif//__SECURITY_ATSHA204A_H__
