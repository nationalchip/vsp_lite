/* Voice Signal Preprocess
 * Copyright (C) 2001-2021 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Misc settings for GX8008_QFN88_NRE_UAC_V1.1_20180108 board
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/misc.h>
#include <driver/padmux.h>
#include <driver/gpio.h>

//=================================================================================================
// GPIO For LED
#if defined (CONFIG_BOARD_GX8008_NRE_UAC_1V4) || defined (CONFIG_BOARD_GX8008_NRE_UAC_1V5)

#define UAC_LED_PORT_R 5
#define UAC_LED_PORT_G 6
#define UAC_LED_PORT_B 7

#endif

#ifdef CONFIG_BOARD_GX8008_NRE_UAC_1V3

#define UAC_LED_PORT_R 6
#define UAC_LED_PORT_G 7
#define UAC_LED_PORT_B 5

#endif

#if defined (CONFIG_BOARD_GX8008_NRE_UAC_1V0) || defined (CONFIG_BOARD_GX8008_NRE_UAC_1V2)

#define UAC_LED_PORT_R 32
#define UAC_LED_PORT_G 29
#define UAC_LED_PORT_B 31

#endif

//=================================================================================================
// The table is for GX8008
#if defined (CONFIG_BOARD_GX8008_NRE_UAC_1V4) || defined (CONFIG_BOARD_GX8008_NRE_UAC_1V5)
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
  //{ 1, 1},  // POWERDOWN  |*PD1PORT01 |
    { 2, 0},  //*UART0RX    | PD1PORT02 |
    { 3, 0},  //*UART0TX    | PD1PORT03 |
    { 4, 1},  //*OTPAVDDEN  | PD1PORT04 |
    { 5, 3},  // SDBGTDI    | DDBGTDI   | SNDBGTDI    |*PD1PORT05
    { 6, 3},  // SDBGTDO    | DDBGTDO   | SNDBGTDO    |*PD1PORT06
    { 7, 4},  // SDBGTMS    | DDBGTMS   | SNDBGTMS    | PCM1INBCLK |*PD1PORT07
    { 8, 4},  //*SDBGTCK    | DDBGTCK   | SNDBGTCK    | PCM1INLRCK | PD1PORT08
    { 9, 4},  //*SDBGTRST   | DDBGTRST  | SNBGTRST    | PCM1INDAT0 | PD1PORT09
    {14, 3},  //*PCMOUTMCLK | DUARTTX   | SNUARTTX    | PD1PORT14
    {15, 2},  //*PCMOUTDAT0 | SPDIF     | PD1PORT15
    {16, 1},  //*PCMOUTLRCK | PD1PORT16
    {17, 1},  //*PCMOUTBCLK | PD1PORT17
    {28, 0},  // SDA1       |*PD1PORT28
    {29, 0},  // SCL1       |*PD1PORT29
  //{31, 2},  // PCM0INDAT0 | PDMDAT2   |*PD1PORT31
    {32, 2},  // PCM0INMCLK | PDMDAT1   |*PD1PORT32
  //{33, 3},  // PCM0INLRCK | PDMDAT0   | PCM0OUTLRCK |*PD1PORT33
  //{34, 3},  // PCM0INBCLK | PDMCLK    | PCM0OUTBCLK |*PD1PORT34
};
#endif
#ifdef CONFIG_BOARD_GX8008_NRE_UAC_1V3
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
    { 1, 0},  // POWERDOWN  |*PD1PORT01 |
    { 2, 0},  //*UART0RX    | PD1PORT02 |
    { 3, 0},  //*UART0TX    | PD1PORT03 |
  //{ 4, 1},  //*OTPAVDDEN  | PD1PORT04 |
  //{ 5, 3},  // SDBGTDI    | DDBGTDI   | SNDBGTDI    |*PD1PORT05
  //{ 6, 3},  // SDBGTDO    | DDBGTDO   | SNDBGTDO    |*PD1PORT06
  //{ 7, 4},  // SDBGTMS    | DDBGTMS   | SNDBGTMS    | PCM1INBCLK |*PD1PORT07
  //{ 8, 4},  //*SDBGTCK    | DDBGTCK   | SNDBGTCK    | PCM1INLRCK | PD1PORT08
  //{ 9, 4},  //*SDBGTRST   | DDBGTRST  | SNBGTRST    | PCM1INDAT0 | PD1PORT09
    {14, 1},  //*PCMOUTMCLK | DUARTTX   | SNUARTTX    | PD1PORT14
  //{15, 2},  //*PCMOUTDAT0 | SPDIF     | PD1PORT15
  //{16, 1},  //*PCMOUTLRCK | PD1PORT16
  //{17, 1},  //*PCMOUTBCLK | PD1PORT17
    {28, 0},  // SDA1       |*PD1PORT28
    {29, 0},  // SCL1       |*PD1PORT29
  //{31, 2},  // PCM0INDAT0 | PDMDAT2   |*PD1PORT31
  //{32, 2},  // PCM0INMCLK | PDMDAT1   |*PD1PORT32
  //{33, 3},  // PCM0INLRCK | PDMDAT0   | PCM0OUTLRCK |*PD1PORT33
  //{34, 3},  // PCM0INBCLK | PDMCLK    | PCM0OUTBCLK |*PD1PORT34
};
#endif

#if defined (CONFIG_BOARD_GX8008_NRE_UAC_1V0) || (defined CONFIG_BOARD_GX8008_NRE_UAC_1V2)
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
  //{ 1, 1},  // POWERDOWN  |*PD1PORT01 |
    { 2, 0},  //*UART0RX    | PD1PORT02 |
    { 3, 0},  //*UART0TX    | PD1PORT03 |
    { 4, 0},  //*OTPAVDDEN  | PD1PORT04 |
  //{ 5, 3},  // SDBGTDI    | DDBGTDI   | SNDBGTDI    |*PD1PORT05
  //{ 6, 3},  // SDBGTDO    | DDBGTDO   | SNDBGTDO    |*PD1PORT06
  //{ 7, 4},  // SDBGTMS    | DDBGTMS   | SNDBGTMS    | PCM1INBCLK |*PD1PORT07
    { 8, 0},  //*SDBGTCK    | DDBGTCK   | SNDBGTCK    | PCM1INLRCK | PD1PORT08
    { 9, 0},  //*SDBGTRST   | DDBGTRST  | SNBGTRST    | PCM1INDAT0 | PD1PORT09
    {14, 0},  //*PCMOUTMCLK | DUARTTX   | SNUARTTX    | PD1PORT14
    {15, 0},  //*PCMOUTDAT0 | SPDIF     | PD1PORT15
    {16, 0},  //*PCMOUTLRCK | PD1PORT16
    {17, 0},  //*PCMOUTBCLK | PD1PORT17
  //{28, 1},  // SDA1       |*PD1PORT28
  //{29, 1},  // SCL1       |*PD1PORT29
  //{31, 2},  // PCM0INDAT0 | PDMDAT2   |*PD1PORT31
  //{32, 2},  // PCM0INMCLK | PDMDAT1   |*PD1PORT32
  //{33, 1},  // PCM0INLRCK | PDMDAT0   | PCM0OUTLRCK |*PD1PORT33
  //{34, 1},  // PCM0INBCLK | PDMCLK    | PCM0OUTBCLK |*PD1PORT34
};
#endif


void BoardInit(void)
{
    int table_size = ARRAY_SIZE(_pad_configs);

    PadMuxInit(_pad_configs, table_size);

    for (int i = 0; i < table_size; i++) {
        int pad_function = PadMuxGetFunction(_pad_configs[i].pad_id);
        if (pad_function != _pad_configs[i].pad_function)
            printf("Fail to setup Pad %d, should be %d, but %d!\n",
                   _pad_configs[i].pad_id,
                   _pad_configs[i].pad_function, pad_function);
    }

    PadMuxDumpRegs();
}

void BoardDone(void)
{

}
//-------------------------------------------------------------------------------------------------
#if defined (CONFIG_BOARD_GX8008_NRE_UAC_1V4) || defined (CONFIG_BOARD_GX8008_NRE_UAC_1V5)
void BoardSuspend(void)
{
    // Turn off External Amplifier
    GpioSetDirection(32, GPIO_DIRECTION_INPUT);

    // Turn off LED Output Enable
    GpioSetDirection(UAC_LED_PORT_R, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_G, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_B, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_LOW);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_LOW);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_LOW);
}
#else
void BoardSuspend(void)
{
    // Turn off External Amplifier
    GpioSetDirection(31, GPIO_DIRECTION_INPUT);

    // Turn off LED Output Enable
    GpioSetDirection(UAC_LED_PORT_R, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_G, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_B, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_HIGH);
}
#endif

void BoardResume(void)
{
    // Turn on External Amplifier
}
//=================================================================================================
// LED
int LedInit(void)
{
    GpioSetDirection(UAC_LED_PORT_R, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_G, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(UAC_LED_PORT_B, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_HIGH);

    return 0;
}

int LedSetPixel(unsigned int index, LED_PIXEL pixel)
{
    switch (index)
    {
        case 0:
            GpioSetLevel(UAC_LED_PORT_R, !pixel.bits.r);
            GpioSetLevel(UAC_LED_PORT_B, !pixel.bits.b);
            GpioSetLevel(UAC_LED_PORT_G, !pixel.bits.g);
            break;
        default:
            break;
    }

    return 0;
}

int LedFlush(void)
{

    return 0;
}

int LedDone(void)
{
    GpioSetLevel(UAC_LED_PORT_R, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_G, GPIO_LEVEL_HIGH);
    GpioSetLevel(UAC_LED_PORT_B, GPIO_LEVEL_HIGH);

    return 0;
}
