/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * board_config.h: Board configuration for ssd board
 *
 */

#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include <soc_config.h>
#include <base_addr.h>

/*
 * Some macro used by link.ld
 */
#define CONFIG_BOARD_PATH           boards/nationalchip/leo_gx8009_bd3m

#define CONFIG_DW_UART0_CLK          27000000
#define CONFIG_DW_UART1_CLK          27000000
#define CONFIG_SPL_DESIGNWARE_UART_CLKDIV 15  /* (CONFIG_DW_UART_CLK + (8 * baudrate)) / (16 * baudrate) */
#define CONFIG_SERIAL_PORT          0
#define CONFIG_SERIAL_BAUD_RATE     115200

/* SPI FLASH */
#define CONFIG_DESIGNWARE_SPI_CLK   150000000
#define CONFIG_SPL_DESIGNWARE_SPI_CLKDIV 2  /* 必须为偶数且非0 */

#define CONFIG_SF_SAMPLE_DELAY      1
#define CONFIG_SF_DEFAULT_CS        0
#define CONFIG_SF_DEFAULT_SPEED     (CONFIG_DESIGNWARE_SPI_CLK / CONFIG_SPL_DESIGNWARE_SPI_CLKDIV)
#define CONFIG_SF_DEFAULT_MODE      0

/* BUTTON GPIO PORT */
// GPIO can't be shared between Button, WiFi, Power Charger
#define CONFIG_BOARD_WAKEUP_BUTTON_GPIO 0
#define CONFIG_BOARD_WAKEUP_WIFI_GPIO   9
#define CONFIG_BOARD_GPIO_MUTE_BTN      25
//#define CONFIG_BOARD_WAKEUP_CHARGE_GPIO

#endif  /* __BOARD_CONFIG_H__ */
