/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * All Rights Reserved!
 *
 * u-boot-spl.bin.h: U-Boot Stage 1 Binary
 *
 */

const unsigned char u_boot_spl_bin[] = {};
const unsigned int u_boot_spl_bin_len = 0;
