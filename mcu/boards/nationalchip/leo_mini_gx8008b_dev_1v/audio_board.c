/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * All Rights Reserved!
 *
 * audio_board.c: Board Configurations for GX8012&GX8008_QFN88_TEST_V1.0
 *
 */

#include <autoconf.h>

#include <driver/audio_mic.h>
#include <driver/audio_out.h>
#include <driver/irq.h>
#include <driver/delay.h>
#include <driver/gpio.h>

#include <base_addr.h>
#include <audio_in_regs.h>
#include <audio_out_regs.h>
#include <misc_regs.h>

//=================================================================================================

#ifdef CONFIG_BOARD_LEO_MINI_DEV_1V_DMIC_4

const AUDIO_IN_SOURCE_CTRL _source_ctrl = {
    .bits = {
        .source_sel = AUDIO_IN_INPUT_SOURCE_DMIC,
        .filter_mag_en = AUDIO_IN_INPUT_FILTER_DMIC,
        .magnif_sel = AUDIO_IN_INPUT_GAIN_32,
        .i2s_channel_sel = 0,
        .channel_work_en = (0xf >> (4 - CONFIG_MIC_ARRAY_MIC_NUM)) & 0xf,
        .i2sout_sys_gate_enable = 0,
        .else_rst_auto_enable = 0,
        .else_gate_auto_enable = 1,
        .cic_clk_inv_en = 0,
        .mclk_out_sel = 0, // 256*fs 即 12.288M
        .else_sys_gate_enable = 1,
        .i2sin_sys_gate_enable = 1,
        .dmic_sys_gate_enable = 1,
        .i2sout_test_mode = 0,
        .i2sout_soft_rstn = 0,
        .dmic_soft_rstn = 0,
        .i2s_soft_rstn = 1
    }
};

const AUDIO_IN_AVAD_CTRL _avad_ctrl = {
    .bits = {
        .it_param_sel = AIN_AVAD_ITV_USER,
        .zcr_bypass = 1,
        .read_itpara_sel = 0,
        .set_itpara_sel = 0,
        .ch_avad_en = 0x8,
        .echo_avad_mode = AIN_AVAD_ECHO_AVAD_MODE_INTERNAL,
        .avad_mode = AIN_AVAD_AVAD_MODE_INTERNAL,
        .avad_bypass = 0,
        .avad_soft_rstn = 1,
    }
};

const AUDIO_IN_AVAD_FRAME_INFO _avad_frame_info = {
    .bits = {
        .avad_frame_sample_num = 5*48,
        .avad_init_frame_num = 2,
    }
};

const AUDIO_IN_CHANNEL_SEL _channel_sel = {
    .bits = {
        .channel_0_sel = 0,
        .channel_1_sel = 1,
        .channel_2_sel = 2,
        .channel_3_sel = 3,
        .channel_4_sel = 0xf,
        .channel_5_sel = 0xf,
        .channel_6_sel = 0xf,
        .channel_7_sel = 0xf,
    }
};

int AudioInBoardInit()
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    regs->source_ctrl.value = _source_ctrl.value;
    regs->channel_sel.value = _channel_sel.value;
    regs->avad_ctrl.value   = _avad_ctrl.value;

    regs->avad_frame_info.value = _avad_frame_info.value;

    AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_ITL, 0x15000);
    AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_ITU, 0x15000);
    AudioInSetMicDcEnable(AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE | AUDIO_IN_MIC_POST_DC_FILTER_DISABLE);
    AudioInSetRefDcEnable(AUDIO_IN_REF_PRE_DC_FILTER_ENABLE | AUDIO_IN_REF_POST_DC_FILTER_DISABLE);
    AudioInSetMicEnable(0xff);
    AudioInSetMicGain(30);

    // Set Audio REF (Internal Loop)
    AudioInSetRefSource(AUDIO_IN_REF_SOURCE_INTERNAL_LOOP, 0, 1);
    AudioInSetRefEnable(0xff);
    AudioInSetRefGain(0);

    return 0;
}

#endif

//=================================================================================================

#ifdef CONFIG_BOARD_LEO_MINI_DEV_1V_AMIC_4

const AUDIO_IN_SOURCE_CTRL _source_ctrl = {
    .bits = {
        .source_sel = AUDIO_IN_INPUT_SOURCE_AMIC,
        .filter_mag_en = AUDIO_IN_INPUT_FILTER_AMIC,
        .magnif_sel = AUDIO_IN_INPUT_GAIN_2,
        .i2s_channel_sel = 0,
        .channel_work_en = (0xf >> (4 - CONFIG_MIC_ARRAY_MIC_NUM)) & 0xf,
        .i2sout_sys_gate_enable = 0,
        .else_rst_auto_enable = 0,
        .else_gate_auto_enable = 1,
        .cic_clk_inv_en = 0,
        .mclk_out_sel = 0, // 256*fs 即 12.288M
        .else_sys_gate_enable = 1,
        .i2sin_sys_gate_enable = 1,
        .dmic_sys_gate_enable = 1,
        .i2sout_test_mode = 0,
        .i2sout_soft_rstn = 0,
        .dmic_soft_rstn = 1,
        .i2s_soft_rstn = 1
    }
};

const AUDIO_IN_CHANNEL_SEL _channel_sel = {
    .bits = {
    #if CONFIG_MIC_ARRAY_MIC_NUM == 2
        .channel_0_sel = 3,
        .channel_1_sel = 5,
        .channel_2_sel = 0xf,
        .channel_3_sel = 0xf,
        .channel_4_sel = 0xf,
        .channel_5_sel = 0xf,
        .channel_6_sel = 0xf,
        .channel_7_sel = 0xf,
    #else
        .channel_0_sel = 3,
        .channel_1_sel = 4,
        .channel_2_sel = 5,
        .channel_3_sel = 6,
        .channel_4_sel = 0xf,
        .channel_5_sel = 0xf,
        .channel_6_sel = 0xf,
        .channel_7_sel = 0xf,
    #endif
    }
};

const AUDIO_IN_AVAD_CTRL _avad_ctrl = {
    .bits = {
        .it_param_sel = AIN_AVAD_ITV_USER,
        .zcr_bypass = 1,
        .read_itpara_sel = 0,
        .set_itpara_sel = 0,
        .ch_avad_en = 0x1,

    #ifdef CONFIG_BOARD_ANALOG_VAD
        .echo_avad_mode = AIN_AVAD_ECHO_AVAD_MODE_EXTERNAL,
        .avad_mode = AIN_AVAD_AVAD_MODE_EXTERNAL,
    #else
        .echo_avad_mode = AIN_AVAD_ECHO_AVAD_MODE_INTERNAL,
        .avad_mode = AIN_AVAD_AVAD_MODE_INTERNAL,
    #endif
        .avad_bypass = 0,
        .avad_soft_rstn = 1,
    }
};

const AUDIO_IN_AVAD_FRAME_INFO _avad_frame_info = {
    .bits = {
        .avad_frame_sample_num = 5*48,
        .avad_init_frame_num = 2,
    }
};

int AudioInBoardInit()
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    regs->source_ctrl.value     = _source_ctrl.value;
    regs->channel_sel.value     = _channel_sel.value;
    regs->avad_ctrl.value       = _avad_ctrl.value;
    regs->avad_frame_info.value = _avad_frame_info.value;

    regs->adc_ldo_pd.value      = 0x00000000;
    regs->adc_ldo_set.bits.bandgap_pd  = 0x0;
    regs->adc_ldo_set.bits.ldo_voltage = 0x4;

    regs->adc_clk_inv.value     = 0x00000000;

#ifdef CONFIG_BOARD_ANALOG_VAD
    AudioInSetAnalogVadParamUsing(AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_47, AUDIO_IN_AVAD_PREAMP_20);
#else
    AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_ITL, 0x15000);
    AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_ITU, 0x15000);
#endif
    AudioInSetMicDcEnable(AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE | AUDIO_IN_MIC_POST_DC_FILTER_DISABLE);
    AudioInSetRefDcEnable(AUDIO_IN_REF_PRE_DC_FILTER_ENABLE | AUDIO_IN_REF_POST_DC_FILTER_DISABLE);
    AudioInSetMicEnable(0xff);
    AudioInSetMicGain(24);

    // Set Audio REF (Internal Loop)
    AudioInSetRefSource(AUDIO_IN_REF_SOURCE_INTERNAL_LOOP, 0, 1);
    AudioInSetRefEnable(0xff);
    AudioInSetRefGain(0);

    return 0;
}

#endif

//=================================================================================================
const AUDIO_PLAY_DAC_LOWPOWER_PARA _dac_lowpower_para = {
    .bits = {
        .lp_value = 0x0005,
        .lp_num   = 0x10,
        .lp_channel_sel = 0x3,
        .lp_en    = 0
    }
};

const AUDIO_PLAY_DAC_HIGHPOWER_PARA _dac_highpower_para = {
    .bits = {
        .hp_value = 0x0020,
        .hp_num   = 0x10,
        .hp_channel_sel = 0x3,
        .hp_en    = 0
    }
};

int AudioOutBoardInit()
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    regs->audio_play_dac_lowpower_para.value  = _dac_lowpower_para.value;
    regs->audio_play_dac_highpower_para.value = _dac_highpower_para.value;

    // Disable DAC Auto Mute
    AudioOutAutoMuteDAC(0);
    // Enable DAC Out
    AudioOutSetupDAC();

    return 0;
}

int AudioOutExternCodecInit(void)
{
    return 0;
}

