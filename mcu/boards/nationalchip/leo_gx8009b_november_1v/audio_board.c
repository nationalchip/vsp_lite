/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * audio_board.c: Audio Driver for GX8009B_NOVEMBER_1V board
 *
 */

#include <autoconf.h>

#include <driver/audio_mic.h>
#include <driver/audio_out.h>
#include <driver/irq.h>
#include <driver/delay.h>
#include <driver/gpio.h>

#include <base_addr.h>
#include <audio_in_regs.h>
#include <misc_regs.h>

//=================================================================================================

const AUDIO_IN_SOURCE_CTRL _source_ctrl = {
    .bits = {
        .source_sel = AUDIO_IN_INPUT_SOURCE_AMIC,
        .filter_mag_en = AUDIO_IN_INPUT_FILTER_AMIC,
        .magnif_sel = AUDIO_IN_INPUT_GAIN_2,
        .i2s_channel_sel = 0,
        .channel_work_en = (0xff << (8 - CONFIG_MIC_ARRAY_MIC_NUM)) & 0xff,
        .i2sout_sys_gate_enable = 0,
        .else_rst_auto_enable = 0,
        .else_gate_auto_enable = 1,
        .cic_clk_inv_en = 0,
        .mclk_out_sel = 0, // 256*fs 即 12.288M
        .else_sys_gate_enable = 1,
        .i2sin_sys_gate_enable = 1,
        .dmic_sys_gate_enable = 1,
        .i2sout_test_mode = 0,
        .i2sout_soft_rstn = 0,
        .dmic_soft_rstn = 0,
        .i2s_soft_rstn = 1
    }
};

const AUDIO_IN_CHANNEL_SEL _channel_sel = {
    .bits = {
        .channel_0_sel = 0xf,
        .channel_1_sel = 0xf,
        .channel_2_sel = 0xf,
        .channel_3_sel = 0xf,
        .channel_4_sel = 0xf,
        .channel_5_sel = 0xf,
        .channel_6_sel = 6,
        .channel_7_sel = 5,
    }
};

const AUDIO_IN_AVAD_CTRL _avad_ctrl = {
    .bits = {
        .frame_mode = AIN_AVAD_FRAME_LENGTH_5MS,
        .it_param_sel = AIN_AVAD_ITV_USER,
        .zcr_bypass = 1,
        .read_itpara_sel = 0,
        .set_itpara_sel = 0,
        .ch_avad_en = 0x80,
        .echo_ch_avad_en = 0,
#ifdef CONFIG_BOARD_ANALOG_VAD
        .echo_avad_mode = AIN_AVAD_ECHO_AVAD_MODE_EXTERNAL,
        .avad_mode = AIN_AVAD_AVAD_MODE_EXTERNAL,
#else
        .echo_avad_mode = AIN_AVAD_ECHO_AVAD_MODE_INTERNAL,
        .avad_mode = AIN_AVAD_AVAD_MODE_INTERNAL,
#endif
        .avad_bypass = 0,
        .amic_avad_rstn_inv_en = 0,
        .avad_soft_rstn = 1,
    }
};

int AudioInBoardInit()
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    regs->source_ctrl.value = _source_ctrl.value;
    regs->channel_sel.value = _channel_sel.value;
    regs->avad_ctrl.value = _avad_ctrl.value;

    regs->adc_ldo_pd.value      = 0x00000000;
    regs->adc_ldo_set.bits.ldo_voltage = 0x4;

    regs->adc_clk_inv.value     = 0x00000000;

    regs->i2s_adcinfo.bits.second_sdata_en = 0;

#ifdef CONFIG_BOARD_ANALOG_VAD
    AudioInSetAnalogVadParamUsing(AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_97, AUDIO_IN_AVAD_PREAMP_INVALID);
#else
    AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_ITL, 0x15000);
    AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_ITU, 0x15000);
#endif
    AudioInSetMicDcEnable(AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE | AUDIO_IN_MIC_POST_DC_FILTER_ENABLE);
    AudioInSetRefDcEnable(AUDIO_IN_REF_PRE_DC_FILTER_ENABLE | AUDIO_IN_REF_POST_DC_FILTER_ENABLE);
    AudioInSetMicEnable(0xff);
    AudioInSetMicGain(34);

    // Set Audio REF (Internal Loop)
    AudioInSetRefSource(AUDIO_IN_REF_SOURCE_AMIC, 2, 3);
    AudioInSetRefEnable(0xff);
    AudioInSetRefGain(6);

    return 0;
}

//=================================================================================================
int AudioOutBoardInit()
{
    mdelay(20); // Turn on the mute circuit to prevent popping

    AudioOutSetupDAC();

    GpioSetDirection(32, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(32, GPIO_LEVEL_HIGH);
    mdelay(100);// Turn off the mute circuit

    return 0;
}

int AudioOutExternCodecInit(void)
{
    return 0;
}
