/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Misc settings
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/misc.h>
#include <driver/padmux.h>

//=================================================================================================
// The table is for GX8010
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
    { 1, 0},  //*POWERDOWN  | PD1PORT01 |
    { 2, 0},  //*UART0RX    | PD1PORT02 |
    { 3, 0},  //*UART0TX    | PD1PORT03 |
    { 4, 0},  //*OTPAVDDEN  | PD1PORT04 |
    { 5, 0},  //*SDBGTDI    | DDBGTDI   | SNDBGTDI    | PD1PORT05
    { 6, 0},  //*SDBGTDO    | DDBGTDO   | SNDBGTDO    | PD1PORT06
    { 7, 0},  //*SDBGTMS    | DDBGTMS   | SNDBGTMS    | PCM1INBCLK | PD1PORT07
    { 8, 0},  //*SDBGTCK    | DDBGTCK   | SNDBGTCK    | PCM1INLRCK | PD1PORT08
    { 9, 0},  //*SDBGTRST   | DDBGTRST  | SNBGTRST    | PCM1INDAT0 | PD1PORT09
    {11, 0},  //*PCM1INBCLK | PD1PORT11
    {12, 0},  //*PCM1INLRCK | PD1PORT12
    {13, 0},  //*PCM1INDAT0 | PD1PORT13
    {14, 0},  //*PCMOUTMCLK | DUARTTX   | SNUARTTX    | PD1PORT14
    {15, 0},  //*PCMOUTDAT0 | SPDIF     | PD1PORT15
    {16, 0},  //*PCMOUTLRCK | PD1PORT16
    {17, 0},  //*PCMOUTBCLK | PD1PORT17
    {18, 0},  //*UART1RX    | PD1PORT18
    {19, 0},  //*UART1TX    | PD1PORT19
    {20, 0},  //*DDBGTDI    | SNDBGTDI  | PD1PORT20
    {21, 0},  //*DDBGTDO    | SNDBGTDO  | PD1PORT21
    {22, 0},  //*DDBGTMS    | SNDBGTMS  | PD1PORT22
    {23, 0},  //*DDBGTCK    | SNDBGTCK  | PD1PORT23
    {24, 0},  //*DDBGTRST   | SNDBGTRST | PD1PORT24
    {25, 0},  //*DUARTTX    | SNUARTTX  | PD1PORT25
    {26, 0},  //*SDA0       | PD1PORT26
    {27, 0},  //*SCL0       | PD1PORT27
    {28, 0},  //*SDA1       | PD1PORT28
    {29, 0},  //*SCL1       | PD1PORT29
    {30, 1},  // PCM0INDAT1 |*PDMDAT3   | PD1PORT30
    {31, 1},  // PCM0INDAT0 |*PDMDAT2   | PD1PORT31
    {32, 1},  // PCM0INMCLK |*PDMDAT1   | PD1PORT32
    {33, 1},  // PCM0INLRCK |*PDMDAT0   | PCM0OUTLRCK | PD1PORT33
    {34, 1},  // PCM0INBCLK |*PDMCLK    | PCM0OUTBCLK | PD1PORT34
    {35, 0},  //*IR         | PD1PORT35
};

void BoardInit(void)
{
    int table_size = ARRAY_SIZE(_pad_configs);

    PadMuxInit(_pad_configs, table_size);

    for (int i = 0; i < table_size; i++) {
        int pad_function = PadMuxGetFunction(_pad_configs[i].pad_id);
        if (pad_function != _pad_configs[i].pad_function)
            printf("Fail to setup Pad %d, should be %d, but %d!\n",
                   _pad_configs[i].pad_id,
                   _pad_configs[i].pad_function, pad_function);
    }

    PadMuxDumpRegs();
}

void BoardDone(void)
{
    
}

//=================================================================================================

int LedInit(void)
{
    return 0;
}

int LedSetPixel(unsigned int index, LED_PIXEL color)
{
    return 0;
}

int LedFlush(void)
{
    return 0;
}

int LedDone(void)
{
    return 0;
}

