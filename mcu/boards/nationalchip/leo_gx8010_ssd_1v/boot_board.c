/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * boot_board.c: Boot Board Configurations for SSD ...
 *
 */

#include <autoconf.h>
#include <boot/boot_padmux.h>
#include <boot/boot_gpio.h>

//#define ARM_CONFIG_BY_CK
//#define MCU_OVER_CLOCK

#define   CONFIG_BASE             0xa030a000
#define   CONFIG_DTO_BASE         0x28
#define   CONFIG_SOURCE_SEL       0x170
#define   PLL_AUDIO_CONFIG_BASE   0xc0
#define   PLL_DTO_CONFIG_BASE     0xc8
#define   PLL_ARM_CONFIG_BASE     0xcc
#define   CONFIG_CLOCK_DIV_CONFIG 0x24
#define   ROM_GETCH_VARIABLE_ADDR 0xa0103adc
#define   USB_CONFIG4             0x110

//void mulpin_init(void);
//void serial_init(void);
//void serial_put(int ch);

// fo = fi*div/2^30
// div = fo*2^30/fi
// dto select the bus, fi is dto's pll fre, fo is bus fre (output fre), here fi is 1188000000
void DTO_Config(unsigned int dto, unsigned int div)
{
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31);
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = 0;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31);
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | div;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | (1 << 30) | div;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | (1 << 30) | div;
    *(volatile unsigned int *)(CONFIG_BASE + CONFIG_DTO_BASE + 4 * (dto - 1)) = (1 << 31) | div;
}

/* pll.c */
static struct param{
    unsigned int   freq;
    unsigned char  clkbp;
    unsigned char  clkod;
    unsigned char  clkn;
    unsigned char  clkm;
} param_table[] = {
    { 432000000, 0, 2, 0, 71 }, // 432MHz   DDR
    { 600000000, 0, 1, 0, 49 }, // 600MHz   ARM
    { 516000000, 0, 1, 0, 42 }, // 516MHz   AUDIO
    {1200000000, 0, 0, 0, 49 }, // 1.2GHz   DTO
};

// fvco = freq_xtal * clkm / clkn,其中freq_xtal是晶振频率12000000
// fclkout = fvco / (2 ^ clkod)
void PLL_Config(unsigned int pll, unsigned int freq)
{
    int i;
    volatile unsigned int j = 100;
    for (i=0; i < sizeof(param_table) / sizeof(struct param); i++) {
        if (freq == param_table[i].freq) {
            //unsigned int clkbp = param_table[i].clkbp;
            unsigned int clkod = param_table[i].clkod;
            unsigned int clkn = param_table[i].clkn;
            unsigned int clkm = param_table[i].clkm;

            *(volatile unsigned int*)(pll) = (1<<15)|(1<<14)|(clkod<<12)|(clkn<<7)|clkm;
            while(j--);
            *(volatile unsigned int*)(pll) = (0<<15)|(1<<14)|(clkod<<12)|(clkn<<7)|clkm;
            j=100;
            while(j--);
            *(volatile unsigned int*)(pll) = (0<<15)|(0<<14)|(clkod<<12)|(clkn<<7)|clkm;
        }
    }
    j=100;
    while(j--);
}

/* cpu_config.c */
void gx_setup_pll_mini_controller(void)
{
    *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) = 0;
    //PLL DTO
    PLL_Config(CONFIG_BASE + PLL_DTO_CONFIG_BASE,   1200000000);// DTO:     1200M
    // PLL config
    PLL_Config(CONFIG_BASE + PLL_AUDIO_CONFIG_BASE, 516000000); // AUDIO:    516M
}

static struct pll_dto {
    unsigned int dto;
    unsigned int div;
    unsigned int value;
} dto_table[] = {
    {1, 0x07FFFFFF, 1      },  // MCU           150 MHz
//  {2, 0x02E147AE, 1 <<  1},  // APB2_0      53.99 MHz
    {3, 0x0170A3D7, 1 <<  2},  // APB2_UART   26.99 MHz
    {4, 0x08888889, 1 <<  3},  // SNPU          160 MHz
#if defined(CONFIG_DSP_FREQUENCE_400M)
    {5, 0x15555555, 1 << 16},  // DSP           400 MHz
#else
    {5, 0x1B851EB8, 1 << 16},  // DSP           516 MHz
#endif
    {6, 0x07FFFFFF, 1 <<  5},  // Secure     149.99 MHz
    {7, 0x00A7C5AC, 1 <<  9},  // Audio SRL  12.288 MHz
    {8, 0x014F8B59, 1 <<  8},  // Audio IN   24.576 MHz
    {9, 0x0AAAAAAB, 1 << 10},  // SRAM          200 MHz
};

void gx_setup_pll_full_controller(void)
{
    int i;

    for (i=0; i < sizeof(dto_table) / sizeof(struct pll_dto); i++) {
        DTO_Config(dto_table[i].dto, dto_table[i].div);
        *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) |= dto_table[i].value;
    }

    // 由arm配置pll_arm 和 pll_ddr
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) |= (1<<31);

    //clock audio in select MUX
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) &= ~(1<<7);  // 选择时钟来自DTO

    //clock audio In DIV
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<23);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |=  (1<<23); //rst
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(0x3f<<16);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (20<<16); //516MHz / 21 分频 = 24.5714MHz
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<22);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<22); //load end

    //DSP select DIV
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) &= ~(1<<13);  // 选择时钟来自DTO


    //USB slave clock select
    *(volatile unsigned int*)(CONFIG_BASE + CONFIG_SOURCE_SEL) |= (1<<11);
    *((volatile unsigned int *)(CONFIG_BASE + USB_CONFIG4)) |= (1 << 6);

    //clock audio clock_audio_lodec
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<31);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<31); //rst
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(0x3f<<24);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (23<<24); //div//24分频、、1200/24=50M
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) &= ~(1<<30);
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<30); //load end
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_SOURCE_SEL) |= (1<<6);// source div lodec
#ifdef SCPU_OVER_CLOCK
    *(volatile unsigned int*)(CONFIG_BASE+CONFIG_CLOCK_DIV_CONFIG) |= (1<<4); // SCPU 150MHZ
#endif
}

void BootBoardInit(void)
{
    gx_setup_pll_mini_controller();
    gx_setup_pll_full_controller();
}

//=================================================================================================
// 

#ifdef CONFIG_BOARD_SUPPORT_MULTIBOOT
int BootBoardGetImageInfo(unsigned int *image_addr, unsigned int *image_size)
{
    BootPadMuxSetFunction(14, 3);
    BootGpioSetDirection(14, GPIO_DIRECTION_INPUT);
    if (BootGpioGetLevel(14) == GPIO_LEVEL_LOW) {
        *image_addr = CONFIG_BOARD_GX8010_NRE_SSD_V1_0_SECOND_IMAGE_ADDR;
        *image_size = CONFIG_BOARD_GX8010_NRE_SSD_V1_0_SECOND_IMAGE_SIZE_KB * 1024;
    }
    return 0;
}
#endif
