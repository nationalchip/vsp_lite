/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Misc settings
 *
 */

#include <autoconf.h>

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/misc.h>
#include <driver/padmux.h>
#include <driver/pmu.h>
#include <driver/gpio.h>
#include <driver/led_is31fl3236a.h>

#define LOG_TAG "[MISC]"

//=================================================================================================
// The table is for GX8010
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
    { 1, 0},  // POWERDOWN  | PD1PORT01 |
    { 2, 0},  // UART0RX    | PD1PORT02 |
    { 3, 0},  // UART0TX    | PD1PORT03 |
    { 4, 1},  // OTPAVDDEN  | PD1PORT04 |
    { 5, 3},  // SDBGTDI    | DDBGTDI   | SNDBGTDI    | PD1PORT05
    { 6, 3},  // SDBGTDO    | DDBGTDO   | SNDBGTDO    | PD1PORT06
    { 7, 4},  // SDBGTMS    | DDBGTMS   | SNDBGTMS    | PCM1INBCLK | PD1PORT07
    { 8, 4},  // SDBGTCK    | DDBGTCK   | SNDBGTCK    | PCM1INLRCK | PD1PORT08
    { 9, 4},  // SDBGTRST   | DDBGTRST  | SNBGTRST    | PCM1INDAT0 | PD1PORT09
    {11, 0},  // PCM1INBCLK | PD1PORT11
    {12, 0},  // PCM1INLRCK | PD1PORT12
    {13, 0},  // PCM1INDAT0 | PD1PORT13
    {14, 3},  // PCMOUTMCLK | DUARTTX   | SNUARTTX    | PD1PORT14
    {15, 2},  // PCMOUTDAT0 | SPDIF     | PD1PORT15
    {16, 1},  // PCMOUTLRCK | PD1PORT16
    {17, 1},  // PCMOUTBCLK | PD1PORT17
    {18, 1},  // UART1RX    | PD1PORT18
    {19, 1},  // UART1TX    | PD1PORT19
    {20, 2},  // DDBGTDI    | SNDBGTDI  | PD1PORT20
    {21, 2},  // DDBGTDO    | SNDBGTDO  | PD1PORT21
    {22, 2},  // DDBGTMS    | SNDBGTMS  | PD1PORT22
    {23, 2},  // DDBGTCK    | SNDBGTCK  | PD1PORT23
    {24, 2},  // DDBGTRST   | SNDBGTRST | PD1PORT24
    {25, 0},  // DUARTTX    | SNUARTTX  | PD1PORT25
    {26, 0},  // SDA0       | PD1PORT26
    {27, 0},  // SCL0       | PD1PORT27
    {28, 0},  // SDA1       | PD1PORT28
    {29, 0},  // SCL1       | PD1PORT29
    {30, 1},  // PCM0INDAT1 | PDMDAT3   | PD1PORT30
    {31, 1},  // PCM0INDAT0 | PDMDAT2   | PD1PORT31
    {32, 1},  // PCM0INMCLK | PDMDAT1   | PD1PORT32
    {33, 1},  // PCM0INLRCK | PDMDAT0   | PCM0OUTLRCK | PD1PORT33
    {34, 1},  // PCM0INBCLK | PDMCLK    | PCM0OUTBCLK | PD1PORT34
    {35, 1},  // IR         | PD1PORT35
};

void BoardInit(void)
{
    int table_size = ARRAY_SIZE(_pad_configs);

    PadMuxInit(_pad_configs, table_size);

    for (int i = 0; i < table_size; i++) {
        int pad_function = PadMuxGetFunction(_pad_configs[i].pad_id);
        if (pad_function != _pad_configs[i].pad_function)
            printf("Fail to setup Pad %d, should be %d, but %d!\n",
                   _pad_configs[i].pad_id,
                   _pad_configs[i].pad_function, pad_function);
    }

    PadMuxDumpRegs();

    // PMU is attached at the second i2c bus
    PmuInit(1);
}

void BoardDone(void)
{

}

//-------------------------------------------------------------------------------------------------

void BoardSuspend(void)
{
    // Turn off LED Output Enable
    GpioSetDirection(35, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(35, GPIO_LEVEL_LOW);

#ifdef CONFIG_MCU_POWER_OFF_CPU_WHILE_SUSPEND
    // Turn Down Core 1.1 Voltage
    GpioEnablePWM(4, 1000*1000, 60);
    GpioSetDirection(4, GPIO_DIRECTION_OUTPUT);
#endif
}

void BoardResume(void)
{
#ifdef CONFIG_MCU_POWER_OFF_CPU_WHILE_SUSPEND
    // Recover Core 1.1 Voltage
    GpioSetDirection(4, GPIO_DIRECTION_INPUT);
    GpioDisablePWM(4);
#endif

    // Turn on LED Output Enable
    GpioSetDirection(35, GPIO_DIRECTION_INPUT);
}

//=================================================================================================
// LED

static struct {
    LED_COLOR     led_color[CONFIG_BOARD_LED_NUM];
    LED_LIGHT     led_light[CONFIG_BOARD_LED_NUM];
    unsigned char phy_index[CONFIG_BOARD_LED_NUM];
} s_led_info;

//-------------------------------------------------------------------------------------------------

int LedInit(void)
{
    LedIS31FL3236AInit(0, 0x3c);

    memset(&s_led_info, 0, sizeof(s_led_info));
    for (int i = 0; i < CONFIG_BOARD_LED_NUM; i++) {
        s_led_info.phy_index[i] = CONFIG_BOARD_LED_NUM - i - 1;
        LedIS31FL3236ASetSwitch(i, LED_SWITCH_ON);
    }
    return 0;
}

int LedSetPixel(unsigned int index, LED_PIXEL pixel)
{
    s_led_info.led_color[s_led_info.phy_index[index]].r = pixel.bits.r;
    s_led_info.led_color[s_led_info.phy_index[index]].g = pixel.bits.g;
    s_led_info.led_color[s_led_info.phy_index[index]].b = pixel.bits.b;
    s_led_info.led_light[s_led_info.phy_index[index]] = (pixel.bits.a >> 5) & 0x6;
    return 0;
}

int LedFlush(void)
{
    LedIS31FL3236ASetColors(0, CONFIG_BOARD_LED_NUM, s_led_info.led_color);
    LedIS31FL3236ASetLights(0, CONFIG_BOARD_LED_NUM, s_led_info.led_light);
    return 0;
}

int LedDone(void)
{
    for (int i = 0; i < CONFIG_BOARD_LED_NUM; i++)
        LedIS31FL3236ASetSwitch(i, LED_SWITCH_OFF);
    return 0;
}

