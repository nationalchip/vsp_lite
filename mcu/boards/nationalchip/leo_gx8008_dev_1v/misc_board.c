/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Board Configurations for GX8012&GX8008_QFN88_TEST_V1.0
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/misc.h>
#include <driver/padmux.h>

//=================================================================================================
// The table is for GX8008
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
  //{ 1, 1},  // POWERDOWN  |*PD1PORT01 |
    { 2, 0},  //*UART0RX    | PD1PORT02 |
    { 3, 0},  //*UART0TX    | PD1PORT03 |
    { 4, 0},  //*OTPAVDDEN  | PD1PORT04 |
  //{ 5, 3},  // SDBGTDI    | DDBGTDI   | SNDBGTDI    |*PD1PORT05
  //{ 6, 3},  // SDBGTDO    | DDBGTDO   | SNDBGTDO    |*PD1PORT06
  //{ 7, 4},  // SDBGTMS    | DDBGTMS   | SNDBGTMS    | PCM1INBCLK |*PD1PORT07
    { 8, 0},  //*SDBGTCK    | DDBGTCK   | SNDBGTCK    | PCM1INLRCK | PD1PORT08
    { 9, 0},  //*SDBGTRST   | DDBGTRST  | SNBGTRST    | PCM1INDAT0 | PD1PORT09
    {14, 0},  //*PCMOUTMCLK | DUARTTX   | SNUARTTX    | PD1PORT14
    {15, 0},  //*PCMOUTDAT0 | SPDIF     | PD1PORT15
    {16, 0},  //*PCMOUTLRCK | PD1PORT16
    {17, 0},  //*PCMOUTBCLK | PD1PORT17
    {28, 0},  //*SDA1       | PD1PORT28
    {29, 0},  //*SCL1       | PD1PORT29
  //{31, 2},  // PCM0INDAT0 | PDMDAT2   |*PD1PORT31
  //{32, 1},  // PCM0INMCLK | PDMDAT1   |*PD1PORT32
  //{33, 1},  // PCM0INLRCK | PDMDAT0   | PCM0OUTLRCK |*PD1PORT33
  //{34, 1},  // PCM0INBCLK | PDMCLK    | PCM0OUTBCLK |*PD1PORT34
};

void BoardInit(void)
{
    int table_size = ARRAY_SIZE(_pad_configs);

    PadMuxInit(_pad_configs, table_size);

    for (int i = 0; i < table_size; i++) {
        int pad_function = PadMuxGetFunction(_pad_configs[i].pad_id);
        if (pad_function != _pad_configs[i].pad_function)
            printf("Fail to setup Pad %d, should be %d, but %d!\n",
                   _pad_configs[i].pad_id,
                   _pad_configs[i].pad_function, pad_function);
    }

    PadMuxDumpRegs();
}

void BoardDone(void)
{
    
}

//=================================================================================================

int LedInit(void)
{
    return 0;
}

int LedSetPixel(unsigned int index, LED_PIXEL color)
{
    return 0;
}

int LedFlush(void)
{
    return 0;
}

int LedDone(void)
{
    return 0;
}

