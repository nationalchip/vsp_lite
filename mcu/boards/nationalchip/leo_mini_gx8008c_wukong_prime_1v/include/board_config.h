/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * board_config.h: Board configuration for GX8008C_WUKONG_PRIME_V1.0_20190522 board
 *
 */

#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include <soc_config.h>
#include <base_addr.h>
#include <autoconf.h>

/*
 * Some macro used by link.ld
 */
#define CONFIG_BOARD_PATH           boards/nationalchip/leo_mini_gx8008c_wukong_prime_1v

#define CONFIG_DW_UART0_CLK         192000000
#define CONFIG_DW_UART1_CLK         192000000

#if defined(CONFIG_DSP_FREQUENCE_400M) || defined(CONFIG_DSP_FREQUENCE_512M)
# define CONFIG_SPL_DESIGNWARE_UART_CLKDIV 16  /* (CONFIG_DW_UART_CLK + (8 * baudrate)) / (16 * baudrate) */
#else
# define CONFIG_SPL_DESIGNWARE_UART_CLKDIV 4   /* (CONFIG_DW_UART_CLK + (8 * baudrate)) / (16 * baudrate) */
#endif

#define CONFIG_SPL_DESIGNWARE_UART_CLKDLF 0  /* (CONFIG_DW_UART_CLK % (16 * baudrate) * 1.0) / (16 * baudrate) * (1 << 4) */
#define CONFIG_SERIAL_PORT          1
#define CONFIG_SERIAL_BAUD_RATE     115200
#define CONFIG_PROXY_SERIAL_BAUD_RATE 614400

/* SPI FLASH */
#if defined(CONFIG_DSP_FREQUENCE_400M) || defined(CONFIG_DSP_FREQUENCE_512M)
#define CONFIG_FLASH_SPI_CLK_SRC        200000000    /* 200MHz */
#else
#define CONFIG_FLASH_SPI_CLK_SRC        150000000    /* 200MHz */
#endif
#define CONFIG_SF_DEFAULT_CLKDIV        2            /* 分频数必须为偶数且非0 */
#define CONFIG_SF_SAMPLE_DELAY          2
#define CONFIG_SF_DEFAULT_SPEED         (CONFIG_FLASH_SPI_CLK_SRC / CONFIG_SF_DEFAULT_CLKDIV)

#define CONFIG_SF_DEFAULT_CS            0
#define CONFIG_SF_DEFAULT_MODE          0

/*GENERAL_SPI*/
#define CONFIG_GENERAL_SPI_CLK_SRC      200000000
#define CONFIG_GENERAL_SPI_BUS_SN       0

/* GPIO Settings */
#define CONFIG_BOARD_GPIO_MUTE_BTN  0

#endif    /* __BOARD_CONFIG_H__ */
