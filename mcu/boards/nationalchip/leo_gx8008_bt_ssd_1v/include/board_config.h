/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * board_config.h: Board configuration for GX8008_BT_SSD_V1.0 board
 *
 */

#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include <soc_config.h>
#include <base_addr.h>

/*
 * Some macro used by link.ld
 */
#define CONFIG_BOARD_PATH           boards/nationalchip/leo_gx8008_bt_ssd_1v

#define CONFIG_DW_UART0_CLK          192000000
#define CONFIG_DW_UART1_CLK          192000000
#define CONFIG_SPL_DESIGNWARE_UART_CLKDIV 4  /* (CONFIG_DW_UART_CLK + (8 * baudrate)) / (16 * baudrate) */
#define CONFIG_SERIAL_PORT          0
#define CONFIG_SERIAL_BAUD_RATE     460800

/* SPI FLASH */
#define CONFIG_DESIGNWARE_SPI_CLK   150000000
#define CONFIG_SPL_DESIGNWARE_SPI_CLKDIV 2  /* 必须为偶数且非0 */

#define CONFIG_SF_SAMPLE_DELAY      1
#define CONFIG_SF_DEFAULT_CS        0
#define CONFIG_SF_DEFAULT_SPEED     (CONFIG_DESIGNWARE_SPI_CLK / CONFIG_SPL_DESIGNWARE_SPI_CLKDIV)
#define CONFIG_SF_DEFAULT_MODE      0

#define CONFIG_BOARD_GPIO_IRR       31
#define CONFIG_BOARD_GPIO_RESET_BT  28
#define CONFIG_BOARD_GPIO_LED01     5
#endif  /* __BOARD_CONFIG_H__ */
