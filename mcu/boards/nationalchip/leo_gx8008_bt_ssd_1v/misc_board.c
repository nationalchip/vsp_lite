/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Misc settings for GX8008_BT_SSD_V1.0 board
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <board_config.h>

#include <driver/misc.h>
#include <driver/padmux.h>
#include <driver/led_aw9523b.h>
#include <driver/gpio.h>


#define BT_CODE_ADDR_START 0x200000
#define BT_FLASH_LEN 253

//=================================================================================================
// The table is for GX8008
static const PADMUX_PAD_CONFIG _pad_configs[] = {
/*   id| func // function0  | function1 | function2   | function3  | function4 */
    //{ 1, 1},  // POWERDOWN  |*PD1PORT01 |
    { 2, 0},  //*UART0RX    | PD1PORT02 |
    { 3, 0},  //*UART0TX    | PD1PORT03 |
    //{ 4, 1},  //*OTPAVDDEN  | PD1PORT04 |
    { 5, 3},  // SDBGTDI    | DDBGTDI   | SNDBGTDI    |*PD1PORT05
    //{ 6, 3},  // SDBGTDO    | DDBGTDO   | SNDBGTDO    |*PD1PORT06
    //{ 7, 4},  // SDBGTMS    | DDBGTMS   | SNDBGTMS    |*PCM1INBCLK | PD1PORT07
    //{ 8, 4},  // SDBGTCK    | DDBGTCK   | SNDBGTCK    |*PCM1INLRCK | PD1PORT08
    //{ 9, 4},  // SDBGTRST   | DDBGTRST  | SNBGTRST    |*PCM1INDAT0 | PD1PORT09
    {14, 1},  // PCMOUTMCLK |*DUARTTX   | SNUARTTX    | PD1PORT14
    {15, 0},  //*PCMOUTDAT0 | SPDIF     | PD1PORT15
    {16, 0},  //*PCMOUTLRCK | PD1PORT16
    {17, 0},  //*PCMOUTBCLK | PD1PORT17
    {28, 1},  //*SDA1       | PD1PORT28
    {29, 0},  //*SCL1       | PD1PORT29
    {31, 2},  // PCM0INDAT0 | *PDMDAT2   |*PD1PORT31
    {32, 2},  // PCM0INMCLK | *PDMDAT1   |*PD1PORT32
    {33, 3},  // PCM0INLRCK | *PDMDAT0   | PCM0OUTLRCK |*PD1PORT33
    {34, 3},  // PCM0INBCLK | *PDMCLK    | PCM0OUTBCLK |*PD1PORT34
};

void BoardInit(void)
{
    int table_size = ARRAY_SIZE(_pad_configs);

    PadMuxInit(_pad_configs, table_size);

    for (int i = 0; i < table_size; i++) {
        int pad_function = PadMuxGetFunction(_pad_configs[i].pad_id);
        if (pad_function != _pad_configs[i].pad_function)
            printf("Fail to setup Pad %d, should be %d, but %d!\n",
                   _pad_configs[i].pad_id,
                   _pad_configs[i].pad_function, pad_function);
    }

    PadMuxDumpRegs();

    GpioSetDirection(CONFIG_BOARD_GPIO_RESET_BT, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(CONFIG_BOARD_GPIO_LED01, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_HIGH);
    GpioSetLevel(CONFIG_BOARD_GPIO_LED01, GPIO_LEVEL_HIGH);
}

void BoardDone(void)
{

}

//-------------------------------------------------------------------------------------------------

void BoardSuspend(void)
{

}

void BoardResume(void)
{

}

//=================================================================================================
// LED

int LedInit(void)
{
    return 0;
}

int LedSetPixel(unsigned int index, LED_PIXEL pixel)
{
    return 0;
}

int LedFlush()
{
    return 0;
}

int LedDone(void)
{
    return 0;
}

