/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * counter_delay.c: MCU Counter Driver in BOOT
 *
 */

#include <common.h>
#include <soc_config.h>
#include <base_addr.h>

#include "boot.h"

/* counter */
#define MCU_VA_COUNTER_2_STATUS     (MCU_REG_BASE_COUNTER + 0x40)
#define MCU_VA_COUNTER_2_VALUE      (MCU_REG_BASE_COUNTER + 0x44)
#define MCU_VA_COUNTER_2_ACCSNAP    (MCU_REG_BASE_COUNTER + 0x48)
#define MCU_VA_COUNTER_2_CONTROL    (MCU_REG_BASE_COUNTER + 0x50)
#define MCU_VA_COUNTER_2_CONFIG     (MCU_REG_BASE_COUNTER + 0x60)
#define MCU_VA_COUNTER_2_PRE        (MCU_REG_BASE_COUNTER + 0x64)
#define MCU_VA_COUNTER_2_INI        (MCU_REG_BASE_COUNTER + 0x68)
#define MCU_VA_COUNTER_2_ACC        (MCU_REG_BASE_COUNTER + 0x70)

#define MCU_VA_COUNTER_PSYSCLK      (CONFIG_APB2_CLK)
#define MCU_VA_COUNTER_2_RATE       (1000000)
#define MCU_VA_COUNTER_CLK(val)     ((MCU_VA_COUNTER_PSYSCLK/(val))-1)

#define CTR_INIT_VALUE  (0xFFFFFFFF - 1000)
#define MAX_REG_NUM     10

unsigned long long boot_get_time_us(void)
{
    unsigned int timebase_l = readl(MCU_VA_COUNTER_2_VALUE);
    unsigned long long timebase_h = readl(MCU_VA_COUNTER_2_ACCSNAP);
    return timebase_l | (timebase_h << 32);
}

void boot_udelay(unsigned int usec)
{
    unsigned long long due = boot_get_time_us() + usec;/* get current timestamp */
    while (boot_get_time_us() < due + 1);
}

void boot_mdelay(unsigned int msec)
{
    while (msec--)
        boot_udelay(1000);
}

//=================================================================================================

void BootDelayInit(void)
{
    writel(0x1, MCU_VA_COUNTER_2_CONTROL);
    writel(0x0, MCU_VA_COUNTER_2_CONTROL);
    writel(0x1, MCU_VA_COUNTER_2_CONFIG);
    writel(MCU_VA_COUNTER_CLK(MCU_VA_COUNTER_2_RATE), MCU_VA_COUNTER_2_PRE); //1us 1/pre_clk
    writel(0, MCU_VA_COUNTER_2_INI);
    writel(0x2, MCU_VA_COUNTER_2_CONTROL); //begin count
}

