/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot_spi_nor.c: SPI NOR Flash Driver in BOOT
 *
 */

#include <autoconf.h>

#include <common.h>
#include <base_addr.h>
#include <board_config.h>
#include <misc_regs.h>

#include "boot.h"
#include "boot_spi.h"

#define SPI_FLASH_READ_CMD       0x03
#define SPI_FLASH_FAST_READ_CMD  0x0b
#define SPIFLASH_MAGIC_NUM       0XAA55AA55
#define Tranverse32(x)  ((0x00ff & (x   >> 24)) | ((0xff0000 & (x)) >> 8 )    |\
((0xff00 & (x)) << 8 )  | ((0x0000ff & (x)) << 24))

#ifndef CONFIG_GX8008B
typedef struct{
    uint8_t  cmd_len;
    uint8_t  cmd[5];
    uint32_t xfer_len;
    uint8_t  *tx;
    uint8_t  *rx;
} xfer_cmd_t;

static int spl_cmd(xfer_cmd_t *xfer)
{
    spl_xfer(xfer->cmd, NULL, xfer->cmd_len, \
             xfer->xfer_len ? SPI_XFER_BEGIN : SPI_XFER_BEGIN | SPI_XFER_END);

    return  xfer->cmd_len + ( xfer->xfer_len ? spl_xfer(xfer->tx, \
                                                        xfer->rx, xfer->xfer_len, SPI_XFER_END) : 0 );
}

static void s_flash_read(char *buf, unsigned int addr, unsigned int len)
{
    uint32_t temp;
    xfer_cmd_t cmd = {
        .cmd_len   = 5,
        .cmd       = { SPI_FLASH_FAST_READ_CMD, 0,0,0,0 },
        .xfer_len  = 4,
        .tx        = NULL,
        .rx        = (uint8_t*)&temp,
    };

    spl_cmd(&cmd);

    temp = Tranverse32(temp);

    if (temp != SPIFLASH_MAGIC_NUM) {
        cmd.cmd_len = 5,
        cmd.cmd[1]  = 0xff & (addr >> 24);
        cmd.cmd[2]  = 0xff & (addr >> 16);
        cmd.cmd[3]  = 0xff & (addr >> 8 );
        cmd.cmd[4]  = 0xff & (addr >> 0 );
    }else{
        cmd.cmd[1]  = 0xff & (addr >> 16);
        cmd.cmd[2]  = 0xff & (addr >> 8 );
        cmd.cmd[3]  = 0xff & (addr >> 0 );
    }

    cmd.xfer_len    = len;
    cmd.tx          = NULL;
    cmd.rx          = (uint8_t*)buf;
    spl_cmd(&cmd);
}
#endif

/* NOR SPL functions */
int BootLoadSpiNorImage(void)
{
#ifndef CONFIG_GX8008B
    spl_spi_init(CONFIG_SF_SAMPLE_DELAY, 0, CONFIG_SF_DEFAULT_SPEED, 0x800);
#endif

    // Load Main
    uint32_t image_addr;
    uint32_t image_size;

#ifdef CONFIG_GX8008B
    image_addr = boot_info.info.norflash.boot_addr + CONFIG_STAGE1_DRAM_SIZE;
#else
    image_addr = CONFIG_STAGE1_DRAM_SIZE;
#endif

#ifdef CONFIG_BOARD_SUPPORT_MULTIBOOT
    image_size = CONFIG_STAGE2_DRAM_SIZE;
    BootBoardGetImageInfo(&image_addr, &image_size);
#endif

#ifdef CONFIG_GX8008B
    extern int sflash_readdata(unsigned int offset, void *to, unsigned int len);
# ifdef CONFIG_MCU_ENABLE_XIP
    extern uint32_t _stage2_xip_start_text_, _stage2_xip_end_text_;
    extern uint32_t _stage2_sram_start_text_, _stage2_sram_end_text_;
    extern uint32_t _stage2_sram_start_data_, _stage2_sram_end_data_;
    uint32_t stage2_xip_text_len  = (uint32_t)&_stage2_xip_end_text_  - (uint32_t)&_stage2_xip_start_text_;
    uint32_t stage2_sram_text_len = (uint32_t)&_stage2_sram_end_text_ - (uint32_t)&_stage2_sram_start_text_;
    uint32_t stage2_sram_data_len = (uint32_t)&_stage2_sram_end_data_ - (uint32_t)&_stage2_sram_start_data_;
    uint32_t stage2_sram_text_offset = image_addr + stage2_xip_text_len;
    uint32_t stage2_sram_data_offset = image_addr + stage2_xip_text_len + stage2_sram_text_len;
    sflash_readdata(stage2_sram_text_offset, (void *)((uint32_t)&_stage2_sram_start_text_ + (CONFIG_STAGE1_DRAM_BASE - CONFIG_STAGE1_IRAM_BASE)), stage2_sram_text_len);
    sflash_readdata(stage2_sram_data_offset, (void *)((uint32_t)&_stage2_sram_start_data_), stage2_sram_data_len);
# else /* CONFIG_MCU_ENABLE_XIP */
    image_size = CONFIG_STAGE2_DRAM_SIZE;
    sflash_readdata(image_addr, (char *)CONFIG_STAGE2_DRAM_BASE, image_size);
# endif /* CONFIG_MCU_ENABLE_XIP */
#else /* CONFIG_GX8008B */
    image_size = CONFIG_STAGE2_DRAM_SIZE;
    s_flash_read((char *)CONFIG_STAGE2_DRAM_BASE, image_addr, image_size);
#endif /* CONFIG_GX8008B */

    // Load Uboot Stage1
#ifdef CONFIG_MCU_ENABLE_BOOT_LOAD_UBOOT_STAGE1
    s_flash_read((char *)CONFIG_UBOOT_STAGE1_RAM_BASE, CONFIG_MCU_ENABLE_BOOT_LOAD_UBOOT_FROM, CONFIG_UBOOT_STAGE1_SIZE);
#endif

#ifdef CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM_NOR_FLASH
    /* load boot music */
    extern int snor_load_boot_music(void);
    snor_load_boot_music();
#endif
    return 0;
}

//=================================================================================================
#ifdef CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM_NOR_FLASH
typedef struct
{
    unsigned int riff_id;
    unsigned int riff_size;
    unsigned int riff_format;
} RIFF_HEADER;

typedef struct
{
    unsigned int    format_id;
    unsigned int    format_size;
    unsigned short  format_tag;
    unsigned short  channels;
    unsigned int    sample_rate;
    unsigned int    bytes_rate;
    unsigned short  block_align;
    unsigned short  bits;
} WAVE_FORMAT;

typedef struct
{
    unsigned int data_tag;
    unsigned int data_size;
} WAVE_DATA_INFO;

typedef struct
{
    RIFF_HEADER riff_header;
    WAVE_FORMAT wave_format;
    WAVE_DATA_INFO data_info;
} WAVE_HEADER;

int snor_load_boot_music(void)
{
    WAVE_HEADER wave_header;
    s_flash_read((char*)&wave_header,CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM, sizeof(WAVE_HEADER));

    if (wave_header.riff_header.riff_id != 0x46464952) return -1; // RIFF
    if (wave_header.wave_format.bits    != 0x10)       return -1; // 16bit
    if (wave_header.data_info.data_tag  != 0x61746164) return -1; // data
    if (wave_header.data_info.data_size  > 524288)     return -1; // size > 512K

    s_flash_read(( char *)CONFIG_DSP_SRAM_BASE,CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM, wave_header.data_info.data_size + sizeof(WAVE_HEADER));

    return 0;
}

#endif