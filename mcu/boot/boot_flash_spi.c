/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * boot_flash_spi.c: stage1 NOR Flash driver of Flash SPI
 *
 */


#include <stdio.h>
#include <string.h>

#include <util.h>
#include <stdarg.h>
#include "boot_flash_spi.h"
#include <board_config.h>

#define DMA_BASE                      0xa0800000
#define DMA_CFGREG                    (DMA_BASE + 0x10)
#define DMA_CHENREG                   (DMA_BASE + 0x18)
#define DMA_INTSTATUSREG              (DMA_BASE + 0x30)
#define DMA_RESETREG                  (DMA_BASE + 0x58)

#define DMA_CHXSAR(x)                 (DMA_BASE + 0x100 + x*0x100)
#define DMA_CHXDAR(x)                 (DMA_BASE + 0x108 + x*0x100)
#define DMA_CHXBLKTS(x)               (DMA_BASE + 0x110 + x*0x100)
#define DMA_CHXCTL(x)                 (DMA_BASE + 0x118 + x*0x100)
#define DMA_CHXCFG(x)                 (DMA_BASE + 0x120 + x*0x100)
#define DMA_CHXSTATUS(x)              (DMA_BASE + 0x130 + x*0x100)
#define DMA_CHXSWHSSRC(x)             (DMA_BASE + 0x138 + x*0x100)
#define DMA_CHXSWHSDST(x)             (DMA_BASE + 0x140 + x*0x100)
#define DMA_CHXINTSTATUSEN(x)         (DMA_BASE + 0x180 + x*0x100)
#define DMA_CHXINTSTATUS(x)           (DMA_BASE + 0x188 + x*0x100)


#define CPU_ADDR_TO_DMA(_addr) ({                                                                  \
	uint32_t __addr = (uint32_t)(_addr);                                                       \
	__addr - (__addr >= 0xa0000000 ? 0xa0000000 : 0x40000000);                                 \
})

static int spl_xip_init(unsigned int cmd,  unsigned int cmd_len,  unsigned int cmd_mode,
			unsigned int addr_len, unsigned int addr_mode,
			unsigned int mode_code, unsigned int mode_code_enable,
			unsigned int wait_cycles, unsigned int spi_mode);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
static unsigned long long dma_reg_read64(unsigned int reg)
{
	unsigned long long value;
	value = *((volatile unsigned long long *)(reg));
	return value;
}

static void dma_reg_write64(unsigned int reg, unsigned long long value)
{
	*((volatile unsigned long long *)(reg)) = value;
	return ;
}

static int select_dma_channel(void)
{
	int i = 0;
	unsigned long long  ch_enable=0;

	for(i = 0; i < 8; i++)
	{
		ch_enable = (dma_reg_read64(DMA_CHENREG)) & (0x01 << i);
		if(0 == ch_enable)
			return i;
	}

	return -1;
}

static int dma_spi2mem(void *dst, void *src, size_t len)
{
	unsigned int dma_channel;
	unsigned long long reg_value,temp;

	reg_value = dma_reg_read64(DMA_BASE);

	dma_reg_write64(DMA_RESETREG, 0x01);
	while(1)
	{
		temp = dma_reg_read64(DMA_RESETREG);
		if(0 == temp)
			break;
	}

	dma_reg_write64(DMA_CFGREG,0x03);

	dma_channel = select_dma_channel();
	if(-1 == dma_channel)
	{
		return -1;
	}

	reg_value = (unsigned long long)(CPU_ADDR_TO_DMA(src));
	dma_reg_write64(DMA_CHXSAR(dma_channel),reg_value);

	reg_value = (unsigned long long)(CPU_ADDR_TO_DMA(dst));
	dma_reg_write64(DMA_CHXDAR(dma_channel),reg_value);

	reg_value = (unsigned long long)(len-1);
	dma_reg_write64(DMA_CHXBLKTS(dma_channel),reg_value);


	reg_value = dma_reg_read64(DMA_CHXCTL(dma_channel));

	temp = 0xff;
	reg_value &= ~(temp << 48);
	temp = 0x04;
	reg_value |= temp << 48; // set awlen to 4
	temp = 0x01;
	reg_value |= temp << 47;// set awlen_en to 1

	temp =  0xff;
	reg_value &= ~(temp << 39);
	temp = 0x03;
	reg_value |= temp << 39; // set arlen to 4
	temp = 0x01;
	reg_value |= temp << 38;// set arlen_en to 1
	temp = 0xf;
	reg_value &= ~(temp << 18);
	temp = 0x02;
	reg_value |= temp << 18;//set dst_msize to 8;
	temp = 0xf;
	reg_value &= ~(temp << 14);
	temp = 0x02;
	reg_value |= temp << 14;//set src_msize to 32;
	temp = 0x7;
	reg_value &= ~(temp << 11);//set dst_tr_size to 8bit
	temp = 0x0;
	reg_value |= temp << 11;

	temp = 0x7;
	reg_value &= ~(temp << 8); //set src_tr_size to 8bit
	temp = 0x0;
	reg_value |= temp << 8;

	reg_value &= ~(0x01 << 6);//dst addr inc
	reg_value |= (0x01 << 4);//src addr fix

	reg_value &= ~0x07;
	if ((uint32_t)dst >= 0xa0100000)
		reg_value |= 0x1;
	else
		reg_value |= 0x5;
	dma_reg_write64(DMA_CHXCTL(dma_channel),reg_value);

	reg_value = dma_reg_read64(DMA_CHXCFG(dma_channel));
	temp = 0x07;
	reg_value &=  ~(temp << 32);
	temp = 0x02;
	reg_value |= temp << 32;// 0x00 mem2mem
	temp = 0x1f;
	reg_value &=  ~(temp << 4);
	temp = 0x0;
	reg_value |= temp << 4;

	temp = 0x01;
	reg_value &= ~(temp << 35);
	temp = 0x0;
	reg_value |= temp << 35;

	dma_reg_write64(DMA_CHXCFG(dma_channel),reg_value);

	//enable channel
	reg_value = dma_reg_read64(DMA_CHENREG);
	reg_value |= 0x101 << dma_channel;
	dma_reg_write64(DMA_CHENREG, reg_value);

	while(1)
	{
		reg_value = dma_reg_read64(DMA_CHXINTSTATUS(dma_channel));
		reg_value &= 0x02;
		if (2 == reg_value)
		{
			break;
		};

	}

	return 0;
}
#endif

static inline int wait_bus_ready(void)
{
	uint32_t val;

	do{
		val = readl(SPIM_SR);
	} while (val & SR_BUSY);

	return 0;
}

static inline int wait_rx_done(void)
{
	uint32_t val;

	do{
		val = readl(SPIM_RXFLR);
	} while (val != 0);

	wait_bus_ready();

	return 0;
}

static inline int wait_tx_done(void)
{
	uint32_t val;

	do{
		val = readl(SPIM_TXFLR);
	} while (val != 0);

	wait_bus_ready();

	return 0;
}

static int sflash_read_reg(uint8_t cmd, void* reg_val, uint32_t len)
{
	uint32_t i, val;
	uint8_t *p;
	p = (uint8_t*)reg_val;
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_DMACR);
	writel(EEPROM_RO_8BITS_MODE, SPIM_CTRLR0);
	writel(len - 1, SPIM_CTRLR1);
	writel(0x01, SPIM_SER);
	writel(0 << 16, SPIM_TXFTLR);
	writel(0x00, SPIM_SPI_CTRL0);
	writel(0x01, SPIM_ENR);

	writel(cmd, SPIM_TXDR);

	for (i = 0; i < len; i++) {
		do{
			val = readl(SPIM_SR);
		} while (!(val & SR_RF_NOT_EMPT));
		p[i] = readl(SPIM_RXDR);
	}

	wait_rx_done();

	return 0;
}

static int sflash_write_reg(uint8_t cmd, const void* reg_val, uint32_t len)
{
	uint32_t i, val;
	const uint8_t *p;
	p = (const uint8_t*)reg_val;
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_DMACR);
	writel(STANDARD_TO_8BITS_MODE, SPIM_CTRLR0);
	writel(len, SPIM_CTRLR1);
	writel(0x01, SPIM_SER);
	writel(len << 16, SPIM_TXFTLR);
	writel(0x00, SPIM_SPI_CTRL0);
	writel(0x01, SPIM_ENR);

	writel(cmd, SPIM_TXDR);

	for (i = 0; i < len; i++) {
		do{
			val = readl(SPIM_SR);
		} while (!(val & SR_TF_NOT_FULL));
		writel(p[i], SPIM_TXDR);
	}

	wait_tx_done();

	return 0;
}

/*
 * Read the status register, returning its value in the location
 * Return the status register value.
 * Returns negative if error occurred.
 */
static uint8_t read_sr1(void)
{
	uint8_t status;

	sflash_read_reg(GX_CMD_RDSR1, &status, sizeof(status));

	return status;
}


/*
 * Set write enable latch with Write Enable command.
 * Returns negative if error occurred.
 */
static inline int write_enable(void)
{
	sflash_write_reg(GX_CMD_WREN, NULL, 0);

	return 0;
}

/*
 * Service routine to read status register until ready, or timeout occurs.
 * Returns **** if error.
 */
static int wait_till_ready(void)
{
	uint8_t sr;
	do{
		sr = read_sr1();
	}while (sr & GX_STAT_WIP);

	return 1;
}

static int sflash_readid(int* jedec)
{
	int              tmp;
	unsigned char    id[3];

	tmp = sflash_read_reg(GX_CMD_READID, id, 3);
	if (tmp !=0) {
		return -2;
	}

	*jedec = id[0];
	*jedec = *jedec << 8;
	*jedec |= id[1];
	*jedec = *jedec << 8;
	*jedec |= id[2];

	return 0;
}

#ifdef CONFIG_MCU_FLASH_SPI_QUAD
// Write 8bit or 16bit status register
static int write_sr(uint8_t *buf, uint32_t len, uint32_t reg_order)
{
	uint8_t cmd;

	if(len > 2)
		return -1;

	switch(reg_order)
	{
		case 1:
			cmd = GX_CMD_WRSR1;
			break;
		case 2:
			cmd = GX_CMD_WRSR2;
			break;
		case 3:
			cmd = GX_CMD_WRSR3;
			break;
		default:
			return -1;
	}

	wait_till_ready();
	write_enable();
	sflash_write_reg(cmd, buf, len);

	return 0;
}

/*
 * Read the status register, returning its value in the location
 * Return the status register value.
 * Returns negative if error occurred.
 */
static uint8_t read_sr2(void)
{
	uint8_t status;

	sflash_read_reg(GX_CMD_RDSR2, &status, sizeof(status));

	return status;
}

static uint8_t read_sr3(void)
{
	uint8_t status;

	sflash_read_reg(GX_CMD_RDSR3, &status, sizeof(status));

	return status;
}

/* 将status2 寄存器QE位置1 */
static int sflash_enable_quad_0(void)
{
	uint8_t status = read_sr2();
	if (status & 0x02)
		return 0;
	status |= 0x02;
	write_sr(&status, 1, 2);
	wait_till_ready();

	return 0;
}

static int sflash_enable_quad_1(void)
{
	uint8_t status[2];
	status[0] = read_sr1();
	status[1] = read_sr2();
	if (status[1] & 0x02)
		return 0;
	status[1] |= 0x02;
	write_sr(status, 2, 1);
	wait_till_ready();

	return 0;
}

static int sflash_enable_hfm(void)
{
	uint8_t status = read_sr3();
	if (status & 0x10)
		return 0;
	status |= 0x10;
	write_sr(&status, 1, 3);
	wait_till_ready();

	return 0;
}
/* 将status2 寄存器QE位置1 */
static int sflash_enable_quad(int id)
{
	switch(id) {
	case 0x00854012: /* P25Q21L */
	case 0x00856013: /* P25Q40L */
	case 0x00c84215: /* GD25VE16C */
	case 0x000b4015: /* XT25F16B */
    case 0x00ba6015: /* zb25q16b */
		sflash_enable_quad_1();
		break;
	case 0x001c3812: /* en25s20a */
	case 0x001c3813: /* en25s40a */
	case 0x001c7017: /* en25qh64a */
		break;
	case 0x00c84015: /* gd25q16 */
	case 0x00684015: /* by25q16bs */
		sflash_enable_quad_0();
		sflash_enable_quad_1();
		break;
	default:
		sflash_enable_quad_0();
		break;
	}

	/* XM25QH32B 四倍数开启HFM*/
	if (id == 0x00204016)
		sflash_enable_hfm();

	return 0;
}

#endif

static int sflash_init(void)
{
	int  jedec = 0;

	writel(0x0, 0xa030a164);
	writel(0x0, SPIM_ENR);

	/* 读取Flash ID时，配置为较低频率 */
	writel(CONFIG_SF_DEFAULT_CLKDIV * 2, SPIM_BAUDR);
	writel(CONFIG_SF_SAMPLE_DELAY, SPIM_SAMPLE_DLY);
	writel(0x0, SPIM_IMR);
	writel(0x1f, SPIM_RXFTLR);

	if (sflash_readid(&jedec) != 0)
		return -1;

	writel(0x0, SPIM_ENR);
	writel(CONFIG_SF_DEFAULT_CLKDIV, SPIM_BAUDR);

#ifdef CONFIG_MCU_FLASH_SPI_QUAD
	sflash_enable_quad(jedec);
	spl_xip_init(0xeb, 8, 1,
			24, 4,
			0x00, 1,
			4, 4);
#else
	spl_xip_init(0xbb, 8, 1,
			24, 2,
			0x00, 1,
			0, 2);
#endif

	return 0;
}

/*
 * Read an address range from the flash chip.  The address range
 * may be any size provided it is within the physical boundaries.
 */
static int sflash_fread(unsigned int from, void *buf,unsigned int len)
{
	/* sanity checks */
	if (!len)
		return 0;

	writel(0x00, SPIM_ENR);

#ifdef CONFIG_MCU_FLASH_SPI_QUAD
	writel(QUAD_RO_8BITS_MODE, SPIM_CTRLR0);
#else
	writel(DUAL_RO_8BITS_MODE, SPIM_CTRLR0);
#endif
	writel(len-1, SPIM_CTRLR1);
	writel(0x01, SPIM_SER);
	writel(0 << 16, SPIM_TXFTLR);
	writel(STRETCH_WAITCYCLES8_INST8_ADDR24, SPIM_SPI_CTRL0);
	writel(7, SPIM_DMARDLR);
	writel(0x01, SPIM_DMACR);

	writel(0x01, SPIM_ENR);

#ifdef CONFIG_MCU_FLASH_SPI_QUAD
	writel(0x6b, SPIM_TXDR);
#else
	writel(0x3b, SPIM_TXDR);
#endif
	writel(from, SPIM_TXDR);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	dma_spi2mem(buf, (void*)SPIM_RXDR, len);
#else
	uint32_t i;
	for (i = 0; i < len; i++) {
		while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
		((uint8_t*)buf)[i] = 0xff & readl(SPIM_RXDR);
	}
#endif
	wait_rx_done();

	return 0;
}

int sflash_readdata(unsigned int offset, void *to, unsigned int len)
{
	unsigned int size;
	unsigned char* p = to;

	sflash_init();
	wait_till_ready();
	while(len != 0) {
		size = min(0x10000, len);
		sflash_fread(offset, p, size);
		len -= size;
		offset += size;
		p += size;
	}

	return 0;
}

//=======================================================================================

static int spl_xip_init(unsigned int cmd,  unsigned int cmd_len,  unsigned int cmd_mode,
			unsigned int addr_len, unsigned int addr_mode,
			unsigned int mode_code, unsigned int mode_code_enable,
			unsigned int wait_cycles, unsigned int spi_mode)
{
	unsigned int cmd_addr_mode = XIP_STANDARD;
	unsigned int reg_data      = 0;

	writel(0x00, SPIM_ENR);

	cmd_mode  = cmd_mode  >> 1;
	addr_mode = addr_mode >> 1;
	spi_mode  = spi_mode  >> 1;

	if (cmd_len == 0)
		cmd_len = XIP_INST_L0;
	else if (cmd_len == 4)
		cmd_len = XIP_INST_L4;
	else if (cmd_len == 8)
		cmd_len = XIP_INST_L8;
	else if (cmd_len == 16)
		cmd_len = XIP_INST_L16;
	else {
		printf("Spi does not support this cmd len\n");
		return -1;
	}

	if ((cmd_mode == XIP_STANDARD) && (addr_mode == XIP_STANDARD))
		cmd_addr_mode = XIP_TRANS_TYPE_TT0;
	else if ((cmd_mode == XIP_STANDARD) && (addr_mode == spi_mode))
		cmd_addr_mode = XIP_TRANS_TYPE_TT1;
	else if ((cmd_mode == spi_mode) && (addr_mode == spi_mode))
		cmd_addr_mode = XIP_TRANS_TYPE_TT2;
	else{
		printf("Spi does not support this configuration\n");
		return -1;
	}

    if (addr_len % 4) {
        printf("addr_len error\n");
        return -1;
    }

	// dsp在复杂case下开预取存在异常
	// reg_data |= 1 << XIP_PREFETCH_EN;
	reg_data |= 1 << XIP_CONT_XFER_EN;
	reg_data |= 1 << XIP_INST_EN;
	reg_data |= wait_cycles << XIP_WAIT_CYCLES;
	reg_data |= cmd_len << XIP_INST_L;
	reg_data |= (addr_len / 4) << XIP_ADDR_L;
	reg_data |= spi_mode << XIP_FRF;
	reg_data |= cmd_addr_mode << XIP_TRANS_TYPE;

	// 模式位设置
	if (mode_code_enable == 1) {
		reg_data |= MBL_8 << XIP_MBL;
		reg_data |= 1 << XIP_MD_BITS_EN;
	}

	writel(reg_data, XIP_CTRL);
	writel(1,        XIP_SER);
	writel(cmd,      XIP_INCR_INST);
	writel(cmd,      XIP_WRAP_INST);
	writel(0,        FLASH_SPI_CS_REG);
	writel(0x01,     SPIM_ENR);
	return 0;
}
