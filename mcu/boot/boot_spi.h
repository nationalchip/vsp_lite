/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot_spi.h: SPI Driver in BOOT
 *
 */

#ifndef __BOOT_SPI_H__
#define __BOOT_SPI_H__

#include <common.h>

#define assert_param(_param, _rtn) do{\
    if(!(_param)) return _rtn;\
}while(0)

#define SPI_BASE_ADDR           (0xa0303000)
#define SPI_CS_ADDR             (0xa030a168)

#define SPIM_CTRLR0             0x00
#define SPIM_CTRLR1             0x04
#define SPIM_ENR                0x08
#define SPIM_SER                0x10
#define SPIM_BAUDR              0x14
#define SPIM_TXFTLR             0x18
#define SPIM_RXFTLR             0x1c
#define SPIM_TXFLR              0x20
#define SPIM_RXFLR              0x24
#define SPIM_SR                 0x28
#define SPIM_IMR                0x2c
#define SPIM_ISR                0x30
#define SPIM_RISR               0x34
#define DW_SPI_TXOICR           0x38
#define DW_SPI_RXOICR           0x3c
#define DW_SPI_RXUICR           0x40
#define SPIM_ICR                0x48
#define SPIM_DMACR              0x4c
#define SPIM_DMATDLR            0x50
#define SPIM_DMARDLR            0x54
#define SPIM_TXDR               0x60
#define SPIM_RXDR               0x60
#define SPIM_RX_SAMPLE_DLY      0XF0

/* Bit fields in SR, 7 bits */
#define SR_MASK                 0x7f                          /* cover 7 bits */
#define SR_BUSY                 (1 << 0)
#define SR_TF_NOT_FULL          (1 << 1)
#define SR_TF_EMPT              (1 << 2)
#define SR_RF_NOT_EMPT          (1 << 3)
#define SR_RF_FULL              (1 << 4)
#define SR_TX_ERR               (1 << 5)
#define SR_DCOL                 (1 << 6)

//Bits in CTRL0
#define SPI_DFS_OFFSET           0
#define SPI_DFS_8BIT             0x07
#define SPI_FRF_OFFSET           4                            /* Frame Format */
#define SPI_MODE_OFFSET          6                            /* SCPH & SCOL  */
#define SPI_TMOD_OFFSET          8
#define SPI_TMOD_TR              0x0                          /* xmit & recv  */
#define SPI_TMOD_TO              0x1                          /* xmit only    */
#define SPI_TMOD_RO              0x2                          /* recv only    */

#define RX_FIFO_LEN              0xf

#define SPI_XFER_BEGIN      (0x01 << 0)
#define SPI_XFER_END        (0x01 << 1)
#define SPI_XFER_ONCE       (SPI_XFER_BEGIN | SPI_XFER_END)
#define SPI_XFER_MMAP       (0x01 << 2)
#define SPI_XFER_MMAP_END   (0x01 << 3)
#define SPI_XFER_U_PAGE     (0x01 << 4)

#define dw_readl( regs, off)       readl(regs + off)
#define dw_writel(regs, off, val)  writel(val, regs + off)
#define dw_readw( regs, off)       readw(regs + off)

#define CS_ENABLE(_en) do{ *spi_priv.cs_regs = _en; }while(0)


/*********************** regs for dma ***********************/
#define GXSCPU_DMA_BASE   0xa0800000
#define BLOCK_TS          0XFFC

#define DW_DMAC_MAX_CH  8
#define DW_REG(_name)   volatile unsigned int _name[2]

/* Hardware register definitions. */
typedef struct dw_dma_chan_regs {
    DW_REG(SAR);                     /* Source Address Register */
    DW_REG(DAR);                     /* Destination Address Register */
    DW_REG(LLP);                     /* Linked List Pointer */
    DW_REG(CTL);                     /* Channel Control Register */
    DW_REG(SSTAT);                   /* Channel Source Status Register */
    DW_REG(DSTAT);                   /* Channel Destination Status Register */
    DW_REG(SSTATAR);                 /* Channel Source Status Address Register */
    DW_REG(DSTATAR);                 /* Channel Destination Status Address Register */
    DW_REG(CFG);                     /* Configuration Register */
    DW_REG(SGR);                     /* Channel Source Gather Register */
    DW_REG(DSR);                     /* Channel Destination Scatter Register */
} dw_dma_ch_regs_t;

typedef struct dw_dma_irq_regs {
    DW_REG(XFER);
    DW_REG(BLOCK);
    DW_REG(SRC_TRAN);
    DW_REG(DST_TRAN);
    DW_REG(ERROR);
} dw_dma_irq_regs_t;

typedef struct dw_dma_regs {
    dw_dma_ch_regs_t   CHAN[DW_DMAC_MAX_CH];

    dw_dma_irq_regs_t  RAW;          /* Interrupt Raw Status Registers */
    dw_dma_irq_regs_t  STATUS;       /* Interrupt Status Registers */
    dw_dma_irq_regs_t  MASK;         /* Interrupt Mask Registers */
    dw_dma_irq_regs_t  CLEAR;        /* Interrupt Clear Registers */
    DW_REG(STATUS_INT);              /* Combined Interrupt Status Register */

    /* software handshaking */
    DW_REG(REQ_SRC);                 /* Source Software Transaction Request Register */
    DW_REG(REQ_DST);                 /* Destination Software Transaction Request Register */
    DW_REG(SGL_REQ_SRC);             /* Single Source Transaction Request Register */
    DW_REG(SGL_REQ_DST);             /* Single Destination Transaction Request Register */
    DW_REG(LAST_SRC);                /* Last Source Transaction Request Register */
    DW_REG(LAST_DST);                /* Last Destination Transaction Request Register */

    /* miscellaneous */
    DW_REG(CFG);                     /* DW_ahb_dmac Configuration Register */
    DW_REG(CH_EN);                   /* DW_ahb_dmac Channel Enable Register */
    DW_REG(ID);                      /* DW_ahb_dmac ID Register */
    DW_REG(TEST);                    /* DW_ahb_dmac Test Register */
    DW_REG(Reserved_1);
    DW_REG(Reserved_2);
    DW_REG(DMA_COMP_PARAMS_6);
    DW_REG(DMA_COMP_PARAMS_5);
    DW_REG(DMA_COMP_PARAMS_4);
    DW_REG(DMA_COMP_PARAMS_3);
    DW_REG(DMA_COMP_PARAMS_2);
    DW_REG(DMA_COMP_PARAMS_1);
    DW_REG(DMA_COMP_ID);
} dw_dma_regs_t;

typedef struct dw_lli {
    uint32_t  sar;
    uint32_t  dar;
    uint32_t  llp;
    uint32_t  ctl[2];
    uint32_t  sstat;
    uint32_t  dstat;
} dw_lli_t;

struct dw_dmac_priv{
    dw_dma_regs_t *dw_dmac;
    volatile dw_lli_t *lli_tx;
    volatile dw_lli_t *lli_rx;
    uint32_t       temp;
};

typedef struct spi_xfer{
    uint32_t regs;
    volatile uint32_t *cs_regs;
    uint32_t freq;
    uint32_t mode;
    int32_t  tx_fifo_len;
    int32_t  rx_fifo_len;

    uint32_t len;
    uint8_t *tx;
    uint8_t *tx_end;
    uint8_t *rx;
    uint8_t *rx_end;
}spi_xfer_t;

#define DWC_CTLL_INT_EN         (1 << 0)                 /* irqs enabled?     */
#define DWC_CTLL_DST_WIDTH(n)   ((n)<<1)                 /* bytes per element */
#define DWC_CTLL_SRC_WIDTH(n)   ((n)<<4)
#define DWC_CTLL_DST_INC        (0<<7)                   /* DAR update/not    */
#define DWC_CTLL_DST_DEC        (1<<7)
#define DWC_CTLL_DST_FIX        (2<<7)
#define DWC_CTLL_SRC_INC        (0<<7)                   /* SAR update/not    */
#define DWC_CTLL_SRC_DEC        (1<<9)
#define DWC_CTLL_SRC_FIX        (2<<9)
#define DWC_CTLL_DST_MSIZE(n)   ((n)<<11)                /* burst, #elements  */
#define DWC_CTLL_SRC_MSIZE(n)   ((n)<<14)
#define DWC_CTLL_S_GATH_EN      (1 << 17)                /* src gather, !FIX  */
#define DWC_CTLL_D_SCAT_EN      (1 << 18)                /* dst scatter, !FIX */
#define DWC_CTLL_FC_M2M         (0 << 20)                /* mem-to-mem        */
#define DWC_CTLL_FC_M2P         (1 << 20)                /* mem-to-periph     */
#define DWC_CTLL_FC_P2M         (2 << 20)                /* periph-to-mem     */
#define DWC_CTLL_FC_P2P         (3 << 20)                /* periph-to-periph  */
#define DWC_CTLL_DMS(n)         ((n)<<23)                /* dst master select */
#define DWC_CTLL_SMS(n)         ((n)<<25)                /* src master select */
#define DWC_CTLL_LLP_D_EN       (1 << 27)                /* dest block chain  */
#define DWC_CTLL_LLP_S_EN       (1 << 28)                /* src block chain   */


#define DWC_COM_CTRL0   DWC_CTLL_DST_MSIZE(2) | DWC_CTLL_SRC_MSIZE(2)|\
                        DWC_CTLL_LLP_D_EN     | DWC_CTLL_LLP_S_EN|\
                        DWC_CTLL_INT_EN
#define DWC_ME2ME_CTRL0 DWC_CTLL_DST_WIDTH(2) | DWC_CTLL_SRC_WIDTH(2)|\
                        DWC_CTLL_DMS(0)       | DWC_CTLL_SMS(0)|\
                        DWC_CTLL_FC_M2M       | DWC_COM_CTRL0
#define DWC_ME2SR_CTRL0 DWC_CTLL_DST_WIDTH(2) | DWC_CTLL_SRC_WIDTH(2)|\
                        DWC_CTLL_DMS(1)       | DWC_CTLL_SMS(0)|\
                        DWC_CTLL_FC_M2M       | DWC_COM_CTRL0
#define DWC_SR2ME_CTRL0 DWC_CTLL_DST_WIDTH(2) | DWC_CTLL_SRC_WIDTH(2)|\
                        DWC_CTLL_DMS(0)       | DWC_CTLL_SMS(1)|\
                        DWC_CTLL_FC_M2M       | DWC_COM_CTRL0
#define DWC_ME2SP_CTRL0 DWC_CTLL_DST_WIDTH(0) | DWC_CTLL_SRC_WIDTH(0)|\
                        DWC_CTLL_DMS(1)       | DWC_CTLL_SMS(0)|\
                        DWC_CTLL_FC_M2P       | DWC_COM_CTRL0
#define DWC_SP2ME_CTRL0 DWC_CTLL_DST_WIDTH(2) | DWC_CTLL_SRC_WIDTH(0)|\
                        DWC_CTLL_DMS(0)       | DWC_CTLL_SMS(1)|\
                        DWC_CTLL_FC_P2M       | DWC_COM_CTRL0
#define DWC_SR2SP_CTRL0 DWC_CTLL_DST_WIDTH(0) | DWC_CTLL_SRC_WIDTH(0)|\
                        DWC_CTLL_DMS(1)       | DWC_CTLL_SMS(1)|\
                        DWC_CTLL_FC_M2P       | DWC_COM_CTRL0
#define DWC_SP2SR_CTRL0 DWC_CTLL_DST_WIDTH(0) | DWC_CTLL_SRC_WIDTH(0)|\
                        DWC_CTLL_DMS(1)       | DWC_CTLL_SMS(1)|\
                        DWC_CTLL_FC_P2M       | DWC_COM_CTRL0


extern void spl_spi_init(int sample, int cs, int speed, int mode);
extern int  spl_xfer(uint8_t *tx, uint8_t *rx, uint32_t len, uint32_t flags);

#endif  /* __BOOT_SPI_H__ */
