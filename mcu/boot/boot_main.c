/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot.c: VSP Boot Main Function
 *
 */

#include <autoconf.h>
#include <common.h>
#include <misc_regs.h>

#include "boot.h"

#ifndef CONFIG_MCU_ENABLE_JTAG_DEBUG
BOOT_INFO boot_info;
static BOOT_DEVICE _BootDevice(void)
{
    volatile BOOT_MODE *mode = (volatile BOOT_MODE *)MISC_REG_BOOT_MODE;

    if(mode->flash_boot == BOOT_MODE_SPI_NAND)
        return BOOT_DEVICE_SPINAND;
    else if(mode->flash_boot == BOOT_MODE_SPI_NOR_3B||\
            mode->flash_boot == BOOT_MODE_SPI_NOR_4B) {
#ifdef CONFIG_GX8008B
        boot_info.info.norflash.boot_addr = mode->flash_boot_high_addr << 16;
#endif
        return BOOT_DEVICE_SPINOR;
    } else if(mode->uart_boot == BOOT_MODE_UART) {
#ifdef CONFIG_GX8008B
        boot_info.info.uart.uart_boot_port = mode->uart_boot_port;
#endif
        return BOOT_DEVICE_UART;
    } else if(mode->usb_boot == BOOT_MODE_USB_HIGH)
        return BOOT_DEVICE_USB_HIGH;
    else if(mode->usb_boot == BOOT_MODE_USB_FULL)
        return BOOT_DEVICE_USB_FULL;
    return -1;
}
#endif

void BootMain(void)
{
    extern int _start_boot_switch_;
    int tmp_value;

    // Init Board PLL and DTO
#ifdef CONFIG_MCU_BOOT_ENABLE_INIT_BOARD
    BootBoardInit();
#endif
    // Load Main Image
#ifndef CONFIG_MCU_ENABLE_JTAG_DEBUG

    boot_info.boot_device = _BootDevice();
    switch (boot_info.boot_device) {
        case BOOT_DEVICE_SPINAND:
#ifdef CONFIG_GX8008B /* Leo_mini不支持spi nand */
            return;
#else
            BootLoadSpiNandImage();
#endif
            break;
        case BOOT_DEVICE_SPINOR:
            tmp_value = *(volatile unsigned int *)&_start_boot_switch_;
            BootLoadSpiNorImage();
            *(volatile unsigned int *)&_start_boot_switch_ = tmp_value;
            break;
        case BOOT_DEVICE_UART:
            BootLoadUartImage();
            break;
        default:
            break;
    }
#endif
    // BootUartPutString(LOG_TAG"Boot MAIN...!\n");
}
