/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot.c: VSP Boot Main Function
 *
 */

#include <autoconf.h>
#include <common.h>

#include <vsp_message.h>

#include <soc_config.h>
#include <base_addr.h>
#include <cpu_regs.h>

#include "boot.h"

void BootRestart(void)
{
    volatile VSP_MSG_CPU_MCU *request = (volatile VSP_MSG_CPU_MCU *)(MCU_REG_BASE_CONFIG + VSP_A7_CK_REG_OFFSET);
    if (request->header.type == VMT_CM_LOAD_MCU) {
#ifdef CONFIG_MCU_ENABLE_PRINTF
        BootUartPutString(LOG_TAG"Reload MAIN...");
#endif
        volatile unsigned int *target_addr = (volatile unsigned int *)(CONFIG_STAGE1_DRAM_BASE + CONFIG_STAGE1_DRAM_SIZE);
        volatile unsigned int *source_addr = (volatile unsigned int *)DEV_TO_MCU(request->load_mcu.main_addr);
        for (int i = (request->load_mcu.main_size + 3) / 4; i > 0 ; i--) {
            *target_addr = *source_addr;
            target_addr++;
            source_addr++;
        }
#ifdef CONFIG_MCU_ENABLE_PRINTF
        BootUartPutString("Done!\n");
#endif
    }
}
