/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot_spi_nand.c: SPI NAND Flash Driver in BOOT
 *
 */

#include <autoconf.h>

#include <board_config.h>
#include <common.h>

#include "boot.h"
#include "boot_spi.h"

#define CMD_READ                   0x13
#define CMD_READ_RDM               0x03
#define CMD_PROG_PAGE_CLRCACHE     0x02
#define CMD_PROG_PAGE              0x84
#define CMD_PROG_PAGE_EXC          0x10
#define CMD_ERASE_BLK              0xd8
#define CMD_WR_ENABLE              0x06
#define CMD_WR_DISABLE             0x04
#define CMD_READ_ID                0x9f
#define CMD_RESET                  0xff
#define CMD_READ_REG               0x0f
#define CMD_WRITE_REG              0x1f

/* feature/ status reg */
#define REG_BLOCK_LOCK             0xa0
#define REG_OTP                    0xb0
#define REG_STATUS                 0xc0

/* status */
#define STATUS_OIP_MASK            (0x01  )
#define STATUS_READY               (0 << 0)
#define STATUS_BUSY                (1 << 0)
#define STATUS_E_FAIL_MASK         (0x04  )
#define STATUS_E_FAIL              (1 << 2)
#define STATUS_P_FAIL_MASK         (0x08  )
#define STATUS_P_FAIL              (1 << 3)
#define STATUS_COMMON_ECC_MASK     (0x30  )
#define STATUS_FORESEE_ECC_MASK    (0x70  )
#define STATUS_COMMON_ECC_ERROR    (2 << 4)
#define STATUS_FORESEE_ECC_ERROR   (7 << 4)
#define STATUS_ECC_1BIT_CORRECTED  (1 << 4)
#define STATUS_ECC_ERROR           (2 << 4)
#define STATUS_ECC_RESERVED        (3 << 4)


/*ECC enable defines*/
#define REG_ECC_MASK               0x10
#define REG_ECC_OFF                0x00
#define REG_ECC_ON                 0x01
#define ECC_DISABLED
#define ECC_IN_NAND
#define ECC_SOFT

/*BUF enable defines*/
#define REG_BUF_MASK               0x08
#define REG_BUF_OFF                0x00
#define REG_BUF_ON                 0x01

/* block lock */
#define BL_ALL_LOCKED              0x38
#define BL_1_2_LOCKED              0x30
#define BL_1_4_LOCKED              0x28
#define BL_1_8_LOCKED              0x20
#define BL_1_16_LOCKED             0x18
#define BL_1_32_LOCKED             0x10
#define BL_1_64_LOCKED             0x08
#define BL_ALL_UNLOCKED            0x00

#define SFLASH_READY_TIMEOUT_CNT   1000000

#define PAGES_PER_BLOCK            64

typedef struct{
    uint8_t  cmd_len;
    uint8_t  cmd[5];
    uint32_t xfer_len;
    uint8_t  *tx;
    uint8_t  *rx;
} nand_cmd_t;

struct mtd_oob_ops {
    uint32_t  mode;
    size_t    len;
    size_t    retlen;
    size_t    ooblen;
    size_t    oobretlen;
    uint32_t  ooboffs;
    uint8_t  *datbuf;
    uint8_t  *oobbuf;
};

struct spi_nand_info{
    uint32_t    nand_id;
    const char *name;
    uint32_t    nand_size;
    uint32_t    usable_size;
    uint32_t    block_size;
    uint32_t    block_main_size;
    uint32_t    block_num_per_chip;
    uint16_t    page_size;
    uint16_t    page_main_size;
    uint16_t    page_spare_size;
    uint16_t    page_num_per_block;
    uint16_t    block_shift;
    uint16_t    page_shift;
    uint32_t    block_mask;
    uint32_t    page_mask;
    struct nand_ecclayout *ecclayout;
};

struct nand_id_index{
    uint32_t id;
    const char *name;
    struct spi_nand_info *info;
};

struct spi_nand_priv {
    uint32_t              id;
    struct udevice       *dev;
    struct spi_slave     *slave;
    struct spi_nand_info *info;
};

//=================================================================================================

static struct spi_nand_info nand_info[] = {
    /**
     *  0x112c 0x122c 0x132c 0xc8f1 0xf1c8 0xc8d1
     *  0xd1c8 0xaaef 0x21C8 0xc298 0x12c2 0xe1a1
     */
    [0] = {
        .nand_size          = 1024 * 64 * 2112,
        .usable_size        = 1024 * 64 * 2048,
        .block_size         = 2112*64,
        .block_main_size    = 2048*64,
        .block_num_per_chip = 1024,
        .page_size          = 2112,
        .page_main_size     = 2048,
        .page_spare_size    = 64,
        .page_num_per_block = 64,
        .block_shift        = 17,
        .block_mask         = 0x1ffff,
        .page_shift         = 11,
        .page_mask          = 0x7ff,
        .ecclayout          = NULL,
    },
    [1] = { // 0xc8f2
        .nand_size          = (2048 * 64 * 2112),
        .usable_size        = (2048 * 64 * 2048),
        .block_size         = (2112*64),
        .block_main_size    = (2048*64),
        .block_num_per_chip = 2048,
        .page_size          = 2112,
        .page_main_size     = 2048,
        .page_spare_size    = 64,
        .page_num_per_block = 64,
        .block_shift        = 17,
        .block_mask         = 0x1ffff,
        .page_shift         = 11,
        .page_mask          = 0x7ff,
        .ecclayout          = NULL,
    },
};

static struct nand_id_index id_table[] = {
    /* GD spi nand flash */
    {.id = 0xF1C8, .info = nand_info,   .name = "GD5F1GQ4UAYIG"   },
    {.id = 0xD1C8, .info = nand_info,   .name = "GD5F1GQ4UB"      },
    {.id = 0xC1C8, .info = nand_info,   .name = "GD5F1GQ4RB"      },
    {.id = 0xD2C8, .info = nand_info+1, .name = "GD5F2GQ4UB"      },
    {.id = 0xC2C8, .info = nand_info+1, .name = "GD5F2GQ4RB"      },
    {.id = 0xF2C8, .info = nand_info+1, .name = "GD5F2GQ4RAYIG"   },
    {.id = 0x51C8, .info = nand_info,   .name = "GD5F1GQ5UEYIG"   },
    {.id = 0x52C8, .info = nand_info+1, .name = "GD5F2GQ5UEYIG"   },
    /* TOSHIBA spi nand flash */
    {.id = 0xc298, .info = nand_info,   .name = "TC58CVG053HRA1G" },
    /* Micron spi nand flash */
    {.id = 0x122C, .info = nand_info,   .name = "MT29F1G01ZAC"    },
    {.id = 0x112C, .info = nand_info,   .name = "MT29F1G01ZAC"    },
    {.id = 0x132C, .info = nand_info,   .name = "MT29F1G01ZAC"    },
    /* 芯天下 spi nand flash */
    {.id = 0xe1a1, .info = nand_info,   .name = "PN26G01AWS1UG"   },
    {.id = 0xe10b, .info = nand_info,   .name = "XT26G01AWS1UG"   },
    {.id = 0xe20b, .info = nand_info+1, .name = "XT26G02AWSEGA"   },
    /* Zetta Confidentia spi nand flash */
    {.id = 0x71ba, .info = nand_info,   .name = "ZD35X1GA"        },
    {.id = 0x21ba, .info = nand_info,   .name = "ZD35X1GA"        },
    /* ESMT spi nand flash */
    {.id = 0x21C8, .info = nand_info,   .name = "F50L1G41A "      },
    {.id = 0x01C8, .info = nand_info,   .name = "F50L1G41LB"      },
    /* winbond spi nand flash */
    {.id = 0x21AAEF, .info = nand_info,   .name = "W25N01GV"      },
    {.id = 0x22AAEF, .info = nand_info+1, .name = "W25N02KV"      },
    /* Mxic spi nand flash */
    {.id = 0x12c2, .info = nand_info,   .name = "MX35LF1GE4AB"    },
    /* foresee spi nand flash */
    {.id = 0xa1cd, .info = nand_info,   .name = "FS35ND01G-D1F1"},
    {.id = 0xb1cd, .info = nand_info,   .name = "FS35ND01G-S1F1"},
    {.id = 0xeacd, .info = nand_info,   .name = "FS35ND01G-S1Y2"},
    /* EtronTech spi nand flash */
    {.id = 0x1cd5, .info = nand_info,   .name = "EM73C044VCD"     },
    {.id = 0x1fd5, .info = nand_info+1, .name = "EM73D044VCG"     },
    /* dosilicon spi nand flash */
    {.id = 0x71e5, .info = nand_info,   .name = "DS35Q1GA"        },
    {.id = 0x72e5, .info = nand_info+1, .name = "DS35Q2GA"        },
    /* HeYangTek spi nand flash */
    {.id = 0x21c9, .info = nand_info,   .name = "HYF1GQ4UDACAE"   },
    {.id = 0x5ac9, .info = nand_info,   .name = "HYF2GQ4UHCCAE"   },
    {.id = 0x52c9, .info = nand_info,   .name = "HYF2GQ4UAACAE"   },
    /* Fudan Microelectronics */
    {.id = 0xa1a1, .info = nand_info,   .name = "FM25S01"         },
    {.id = 0xe4a1, .info = nand_info,   .name = "FM25S01A"        },
    {.id = 0xf2a1, .info = nand_info+1, .name = "FM25G02"         },
    {.id = ~0,     .info = NULL,        .name = "NULL"            }
};

static struct spi_nand_priv nand_priv = { .id = -1, };

static int nand_cmd_send(nand_cmd_t *nand)
{
    uint32_t flag = SPI_XFER_BEGIN | (nand->xfer_len ? 0 : SPI_XFER_END);

    spl_xfer(nand->cmd, NULL, nand->cmd_len, flag);
    if(nand->xfer_len)
        spl_xfer(nand->tx, nand->rx, nand->xfer_len, SPI_XFER_END);

    return nand->cmd_len + nand->xfer_len;
}


void snand_reset(void)
{
    nand_cmd_t cmd = {
        .cmd_len = 1,
        .cmd = { CMD_RESET,0,0,0,0 },
        .xfer_len = 0,
        .tx = NULL,
        .rx = NULL,
    };

    nand_cmd_send(&cmd);
}

static void snand_read_page_to_cache(u32 id)
{
    nand_cmd_t cmd = {
        .cmd_len = 4,
        .cmd = {
            CMD_READ,
            0xFF & (id >> 16),
            0xFF & (id >> 8 ),
            0xFF & (id >> 0 ),
            0,
        },
        .xfer_len = 0,
        .tx = NULL,
        .rx = NULL,
    };

    nand_cmd_send(&cmd);
}

static void snand_read_from_cache(u32 id, u32 len, void *rbuf)
{
    nand_cmd_t cmd = {
        .cmd_len = 4,
        .cmd = {
            CMD_READ_RDM,
            0xFF & (id >> 8),
            0xFF & (id >> 0),
            ~0, 0,
        },
        .xfer_len = len,
        .rx = rbuf,
        .tx = NULL,
    };

    nand_cmd_send(&cmd);
}

static void snand_get_status(u8 reg, void *stat)
{
    nand_cmd_t cmd = {
        .cmd_len  = 2,
        .cmd = { CMD_READ_REG, reg, 0, 0, 0},
        .xfer_len = 1,
        .rx = stat,
        .tx = NULL,
    };

    nand_cmd_send(&cmd);
}

static int snand_wait_ready(int timeout)
{
    u8 status = 0xff;

    while(1){

        snand_get_status(REG_STATUS, &status);

        if((status & STATUS_OIP_MASK) != STATUS_BUSY)
            break;

        if(timeout < 0)
            continue;

        if(--timeout == 0)
            break;
    }

    return -(status & STATUS_OIP_MASK);
}


static void snand_read_id(void *id)
{
    nand_cmd_t cmd = {
        .cmd_len = 2,
        .cmd = { CMD_READ_ID, 0, 0, 0, 0},
        .xfer_len = 3,
        .rx = id,
        .tx = NULL,
    };

    snand_wait_ready(SFLASH_READY_TIMEOUT_CNT);
    nand_cmd_send(&cmd);
}

static struct spi_nand_info *snand_get_info(uint32_t id)
{
    uint32_t tmp;
    struct nand_id_index *tb = id_table;
    while(tb->info != NULL){
        if ((tb->id >> 16) == 0)
            tmp = id & 0x0000FFFF;
        else
            tmp = id & 0x00FFFFFF;
        if (tmp == tb->id) {
            tb->info->nand_id = tb->id;
            tb->info->name    = tb->name;
            return tb->info;
        }
        ++tb;
    }
    return NULL;
}

static inline int snand_read_status(uint8_t *status)
{
    snand_get_status(REG_STATUS, status);
    return 0;
}

static int32_t  snand_page_read(uint32_t page_id,                              \
                                uint16_t offset, uint16_t len, uint8_t* rbuf)
{
    uint8_t  status = 0;

    //针对Dosilicon的DS35Q2GA做特殊处理,DS35Q2GA只有一个die,每个die有两个plane.
    //当block number为奇数,选择另外一个plane.
    if(((&nand_priv)->id == 0x72e5) && ((page_id / PAGES_PER_BLOCK) % 2)){
        offset |= (0x1 << 12);
    }

    snand_wait_ready(SFLASH_READY_TIMEOUT_CNT);

    snand_read_page_to_cache(page_id);

    while (1){
        snand_read_status(&status);

        if((status & STATUS_OIP_MASK) == STATUS_READY){
            /* FS35ND01G-D1F1/FS35ND01G-S1F1 ECC错误标志不一样 */
            if ((nand_priv.id & 0x1ff) != 0x1cd) {
                if((status & STATUS_COMMON_ECC_MASK) == STATUS_COMMON_ECC_ERROR)
                    return -1;
            }else{ //处理 foresee spi nand ecc 错误标志位特异性
                if((status & STATUS_FORESEE_ECC_MASK) == STATUS_FORESEE_ECC_ERROR)
                    return -1;
            }
            break;
        }
    }
    snand_read_from_cache(offset, len, rbuf);

    return len;
}

static int snand_block_isbad(uint32_t ofs)
{
    struct   spi_nand_info *info = nand_priv.info;
    uint16_t block_id = ofs >> info->block_shift;
    uint8_t  is_bad   = 0;
    snand_page_read(block_id*info->page_num_per_block, \
                    info->page_main_size, 1, &is_bad);
    return is_bad == 0XFF ? 0 : 1;
}

static int snand_read_ops(ssize_t from, struct mtd_oob_ops *ops)
{
    assert_param(ops, -1);
    struct spi_nand_info *info = nand_priv.info;
    int32_t page_id  = from >> info->page_shift;
    int32_t page_num = (ops->len + info->page_main_size - 1) >> info->page_shift;
    int32_t count    = 0, main_left = ops->len;
    uint8_t *pbuf    = ops->datbuf;

    while (count < page_num){
        int size = main_left < info->page_main_size ?\
        main_left : info->page_main_size;

        if(snand_page_read(page_id + count, 0, size, pbuf) < 0)
            return -1;

        main_left -= size;
        pbuf      += size;
        count++;
    }

    ops->retlen = pbuf - ops->datbuf;

    return ops->retlen;
}

static int snand_read_skip_bad(ssize_t offset, void *buf, uint32_t len)
{
    struct   spi_nand_info *info = nand_priv.info;
    uint32_t blk_sz    = info->block_main_size;
    uint32_t page_sz   = info->page_main_size;
    ssize_t  read_left = len;

    if(len == 0)
        return 0;

    assert_param(buf, -1);
    assert_param(offset + len <= info->usable_size, -1);
    assert_param(!(offset & (page_sz - 1)), -1);        // 必须按页对齐,否则返回-1

    uint8_t *pbuf = buf;
    struct   mtd_oob_ops ops;

    while(read_left > 0){
        ssize_t blk_ofs = offset & info->block_mask;
        /* skip bad block */
        if(snand_block_isbad(offset & ~info->block_mask)){
            offset += blk_sz - blk_ofs;
            continue;
        }

        ops.datbuf = pbuf;
        ops.len    = (read_left < (blk_sz-blk_ofs)) ? \
        read_left : (blk_sz-blk_ofs);

        if(snand_read_ops(offset, &ops) < 0)
            return -1;

        pbuf      += ops.retlen;
        offset    += ops.retlen;
        read_left -= ops.retlen;
    }

    return pbuf-(uint8_t*)buf;
}

static void my_memcpy(uint8_t *dest, uint8_t *src, int32_t len)
{
    assert_param(dest&&src&&len > 0,;);
    while(len--)    // 没有考虑内存重叠, 实际拷贝中不会出现内存重叠
        *dest++ = *src++;
}

static int snand_read(uint32_t offset, uint8_t *dest, uint32_t len)
{
    struct spi_nand_info *info = nand_priv.info;
    uint32_t block_sz = info->block_main_size;
    uint32_t page_sz  = info->page_main_size;
    uint32_t page_off = offset & (page_sz - 1);
    uint32_t rsize    = 0;
    int32_t  rtn;

    if(page_off){  // 如果地址没有按页对齐
        while(snand_block_isbad(offset & ~info->block_mask))
            offset += block_sz;

        uint8_t  *frist_page = (uint8_t*)0x400a0000;
        uint32_t tmp_len = page_sz-page_off < len ? page_sz-page_off : len;

        rtn = snand_read_skip_bad(offset - page_off, frist_page, page_sz);
        assert_param(rtn > 0, rtn);

        my_memcpy(dest, frist_page + page_off, tmp_len);

        dest   += tmp_len;
        offset += tmp_len;
        len    -= tmp_len;
        rsize  += tmp_len;
    }

    if(len > 0){
        rtn = snand_read_skip_bad(offset, dest, len);
        if(rtn < 0) return rtn;
        rsize += rtn;
    }

    return rsize;
}

static int snand_init(void)
{
    spl_spi_init(CONFIG_SF_SAMPLE_DELAY, 0, CONFIG_SF_DEFAULT_SPEED, 0x800);

    nand_priv.id = 0;
    snand_read_id(&(nand_priv.id));

    nand_priv.info = snand_get_info(nand_priv.id);
    nand_priv.id   = nand_priv.info->nand_id;

    assert_param(nand_priv.info, -1);

    return 0;
}

//=================================================================================================

int BootLoadSpiNandImage(void)
{
    if(snand_init() < 0)
        return -1;

    // Load Main
    uint32_t image_addr = CONFIG_STAGE1_DRAM_SIZE * 4;
    uint32_t image_size = CONFIG_STAGE2_DRAM_SIZE;
#ifdef CONFIG_BOARD_SUPPORT_MULTIBOOT
    BootBoardGetImageInfo(&image_addr, &image_size);
#endif
    snand_read(image_addr, (uint8_t *)CONFIG_STAGE2_DRAM_BASE, image_size);

    // Load Uboot Stage1
#ifdef CONFIG_MCU_ENABLE_BOOT_LOAD_UBOOT_STAGE1
    /* load uboot stage1 */
    snand_read(CONFIG_MCU_ENABLE_BOOT_LOAD_UBOOT_FROM,
               (void*)CONFIG_UBOOT_STAGE1_RAM_BASE, CONFIG_UBOOT_STAGE1_SIZE);
#endif

#ifdef CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM_FLASH
    /* load boot music */
    extern int snand_load_boot_music(void);
    snand_load_boot_music();
#endif
    return 0;
}

//=================================================================================================
#ifdef CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM_FLASH
typedef struct
{
    unsigned int riff_id;
    unsigned int riff_size;
    unsigned int riff_format;
} RIFF_HEADER;

typedef struct
{
    unsigned int    format_id;
    unsigned int    format_size;
    unsigned short  format_tag;
    unsigned short  channels;
    unsigned int    sample_rate;
    unsigned int    bytes_rate;
    unsigned short  block_align;
    unsigned short  bits;
} WAVE_FORMAT;

typedef struct
{
    unsigned int data_tag;
    unsigned int data_size;
} WAVE_DATA_INFO;

typedef struct
{
    RIFF_HEADER riff_header;
    WAVE_FORMAT wave_format;
    WAVE_DATA_INFO data_info;
} WAVE_HEADER;

int snand_load_boot_music(void)
{
    WAVE_HEADER wave_header;
    if (sizeof(WAVE_HEADER) != snand_read(CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM, (unsigned char*)&wave_header, sizeof(WAVE_HEADER))) {
        return -1;
    }
    if (wave_header.riff_header.riff_id != 0x46464952) return -1; // RIFF
    if (wave_header.wave_format.bits    != 0x10)       return -1; // 16bit
    if (wave_header.data_info.data_tag  != 0x61746164) return -1; // data
    if (wave_header.data_info.data_size  > 524288)     return -1; // size > 512K

    int rsize = snand_read(CONFIG_VSP_BOOT_LOAD_BOOT_MUSIC_FROM, (unsigned char *)CONFIG_DSP_SRAM_BASE, wave_header.data_info.data_size + sizeof(WAVE_HEADER));
    if (rsize != wave_header.data_info.data_size + sizeof(WAVE_HEADER)) {
        return -1;
    }

    return 0;
}

#endif

