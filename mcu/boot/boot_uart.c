/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * boot_uart.c: UART Driver in BOOT
 *
 */

#include <autoconf.h>
#include <base_addr.h>
#include <board_config.h>
#include <common.h>

#include <misc_regs.h>

#include "boot.h"

#define  DSW_RBR  0x0
#define  DSW_DLL  0x0
#define  DSW_THR  0x0
#define  DSW_DLH  0x4
#define  DSW_IER  0x4
#define  DSW_FCR  0x8
#define  DSW_LSR  0x14
#define  DSW_MCR  0x10
#define  DSW_USR  0x7c
#define  DSW_LCR  0xc
#define  DSW_DLF  0xc0

#define  DSW_USR_BUSY 0x0
#define  DSW_LSR_TEMT 0x6
#define  DSW_LSR_DR   0x0

#define DW_WAIT_IDLE(base, offset, state, bit) do {\
    state = readl(((base) + (offset)));\
} while( ((state) & (1 << (bit))) )

static unsigned int dw_serialbase = MCU_REG_BASE_UART0;

static void _UartInit(int baudrate)
{
    int clkdiv = 0;
    unsigned int _usr = 0;

#ifdef CONFIG_GX8008B
    unsigned int dw_uart_port = boot_info.info.uart.uart_boot_port;

    if(0 == dw_uart_port) {
        dw_serialbase = MCU_REG_BASE_UART0;
    } else if(1 == dw_uart_port) {
        dw_serialbase = MCU_REG_BASE_UART1;
    }
#endif

    clkdiv = CONFIG_SPL_DESIGNWARE_UART_CLKDIV;

    writel(0x0, (dw_serialbase + DSW_IER));
    writel(0x3, (dw_serialbase + DSW_MCR));

    DW_WAIT_IDLE(dw_serialbase, DSW_USR, _usr, DSW_USR_BUSY);
    writel(0x80, (dw_serialbase + DSW_LCR));
    writel(clkdiv & 0xff, (dw_serialbase + DSW_DLL));
    writel((clkdiv >> 8) & 0xff, (dw_serialbase + DSW_DLH));
#ifdef CONFIG_GX8008B
    writel(CONFIG_SPL_DESIGNWARE_UART_CLKDLF & 0xff, (dw_serialbase + DSW_DLF));
#endif

    DW_WAIT_IDLE(dw_serialbase, DSW_USR, _usr, DSW_USR_BUSY);
    writel(0x03, (dw_serialbase + DSW_LCR));
    writel(0x01, (dw_serialbase + DSW_FCR));
}

static void _UartPut(int ch)
{
    unsigned int state;
    do {
        state = readl(dw_serialbase + DSW_LSR);
    } while (!(state & (1 << DSW_LSR_TEMT)));

    writel((unsigned int)ch, (dw_serialbase + DSW_THR));
}

void BootUartPut(int ch) {
    if (ch == '\n')
        _UartPut('\r');
    _UartPut(ch);
}

void BootUartPutString(const char *string)
{
    while (*string) {
        BootUartPut(*string);
        string++;
    }
}

void BootUartInit(void)
{
    _UartInit(CONFIG_SERIAL_BAUD_RATE);
}

int _UartGet(void)
{
    unsigned int state;
    unsigned int ch;

    do {
        state = readl(dw_serialbase + DSW_LSR);
    } while (!(state & (1 << DSW_LSR_DR)));

    ch = readl(dw_serialbase + DSW_RBR);

    return (ch & 0xff);
}

int BootLoadUartImage(void)
{
    int i;
    unsigned char *p;
    unsigned int image_size, size, check_sum, new_check_sum;
    char *stage2_base = (char *)CONFIG_STAGE2_DRAM_BASE;

    BootUartInit();
    BootDelayInit();

    boot_mdelay(300);

    BootUartPut('G');
    BootUartPut('E');
    BootUartPut('T');

    p = (unsigned char *)&check_sum;
    for(i = 0; i < sizeof(check_sum); i++){
        *p++ = _UartGet();
    }

    p = (unsigned char *)&image_size;
    for(i = 0; i < sizeof(image_size); i++){
        *p++ = _UartGet();
    }

    while (1) {
        new_check_sum = 0;
        size = image_size;

        for(i = 0; i < size; i++) {
            stage2_base[i] = _UartGet();
            new_check_sum += stage2_base[i];
        }

        if (new_check_sum != check_sum) {
            BootUartPut('E');
        } else {
            BootUartPut('O');
            break;
        }
    }

    return 0;
}

