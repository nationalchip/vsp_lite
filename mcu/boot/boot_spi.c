/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot_spi.c: SPI Driver in BOOT
 *
 */

#include <board_config.h>
#include "boot_spi.h"

//=================================================================================================

static spi_xfer_t spi_priv = {
    .regs     = SPI_BASE_ADDR,
    .cs_regs  = (volatile uint32_t *)SPI_CS_ADDR,
    .tx_fifo_len = -1,
    .rx_fifo_len = -1,
};

#define cpu_addr_to_dma(_addr) ({\
    uint32_t __addr = (uint32_t)(_addr);\
    __addr - (__addr > 0xa0000000 ? 0xa0000000 : 0x40000000);\
})

#define SPI_DMA_CHANNEL  0

static struct dw_dmac_priv spl_priv = { .temp = ~0, };
static uint32_t cfg_ctrl[] ={ /*  MEM 00   SR 01   SPI 10 */
    [0x00] = DWC_ME2ME_CTRL0,     // MEM  -> MEM
    [0x01] = DWC_ME2SR_CTRL0,     // MEM  -> SRAM
    [0x02] = DWC_ME2SP_CTRL0,     // MEM  -> SPI
    [0x04] = DWC_SR2ME_CTRL0,     // SRAM -> MEM
    [0x05] = 0,
    [0x06] = DWC_SR2SP_CTRL0,     // SRAM -> SPI
    [0x07] = 0,
    [0x08] = DWC_SP2ME_CTRL0,     // SPI  -> MEM
    [0x09] = DWC_SP2SR_CTRL0,     // SPI  -> SRAM
};

static inline void dw_chan_clean(int ch)
{
    spl_priv.dw_dmac->CLEAR.XFER[0] |=   0x01  << ch;
    spl_priv.dw_dmac->RAW.XFER[0]   &= ~(0x01  << ch);
    spl_priv.dw_dmac->MASK.XFER[0]  |=   0x101 << ch;
}

static uint32_t get_dma_ctrl(void *src, void *dest)
{
    uint32_t _SRC = (uint32_t)src;
    uint32_t _DST = (uint32_t)dest;
    int32_t  index;

    index  = (_SRC < 0xa0000000 ? 0 : _SRC < 0xa0110000 ? 1 : 2) << 2;
    index |= (_DST < 0xa0000000 ? 0 : _DST < 0xa0110000 ? 1 : 2);

    return cfg_ctrl[index];
}

static uint32_t uint32_div(uint32_t dividend, uint32_t divisor, uint32_t *remainder)
{
    uint32_t k, c, res=0;
    if(divisor == 0) return -1;

    if(dividend <= divisor)
        return  (dividend < divisor) ? \
                (*remainder = dividend, 0) : (*remainder = 0, 1);

    while (dividend > divisor) {
        k = 0, c = divisor;
        while(dividend >= c) {
            if (dividend - c < divisor) {
                res += 1<<k;
                break;
            }
            c <<= 1, k++;
        }
        if (dividend - c < divisor)
            break;

        res += 1<<(k - 1);
        dividend -= c>>1;
    }

    *remainder = dividend - c;
    return res;
}

static void dw_dmac_cfg(void *dst, void *src, uint32_t len, bool is_rx)
{
    dw_dma_regs_t  *dma = spl_priv.dw_dmac;
    u32 tx_ctl, rx_ctl;

    spl_priv.dw_dmac->CFG[0] = 0;
    dw_chan_clean(SPI_DMA_CHANNEL);

    if(is_rx){
        rx_ctl = DWC_CTLL_DST_INC | DWC_CTLL_SRC_FIX;

        /* config for rx */
        dma->CHAN[SPI_DMA_CHANNEL].SAR[0] = cpu_addr_to_dma(src);
        dma->CHAN[SPI_DMA_CHANNEL].DAR[0] = cpu_addr_to_dma(dst);
        dma->CHAN[SPI_DMA_CHANNEL].CTL[0] = get_dma_ctrl(src, dst) | rx_ctl;
        dma->CHAN[SPI_DMA_CHANNEL].CTL[1] = len;
        dma->CHAN[SPI_DMA_CHANNEL].LLP[0] = 0;
    } else {
        tx_ctl = DWC_CTLL_DST_FIX | DWC_CTLL_SRC_INC;

        /* config for tx */
        dma->CHAN[SPI_DMA_CHANNEL].SAR[0] = cpu_addr_to_dma(src);
        dma->CHAN[SPI_DMA_CHANNEL].DAR[0] = cpu_addr_to_dma(dst);
        dma->CHAN[SPI_DMA_CHANNEL].CTL[0] = get_dma_ctrl(src, dst) | tx_ctl;
        dma->CHAN[SPI_DMA_CHANNEL].CTL[1] = len >> 2;
        dma->CHAN[SPI_DMA_CHANNEL].LLP[0] = 0;
    }
}

/* 发送最多BLOCK_TS个字节数据 */
static inline int simple_dma_send_block(uint8_t *dest, uint8_t *src, uint32_t len)
{
    dw_writel(spi_priv.regs, SPIM_ENR, 0);
    dw_writel(spi_priv.regs, SPIM_CTRLR1,  len-1);

    dw_dmac_cfg(dest, src, len, 0);

    dw_writel(spi_priv.regs, SPIM_ENR, 1);

    spl_priv.dw_dmac->CFG[0]   = 1;
    spl_priv.dw_dmac->CH_EN[0] = 0x101;

    while((spl_priv.dw_dmac->RAW.XFER[0] & (0X01 << SPI_DMA_CHANNEL)) == 0);

    spl_priv.dw_dmac->CFG[0]   = 0;
    spl_priv.dw_dmac->CH_EN[0] = 0;

    while(dw_readl(spi_priv.regs, SPIM_TXFLR) !=  0);
    while(dw_readl(spi_priv.regs, SPIM_SR) & 0x01);

    return len;
}

static int simple_dma_send(uint8_t *dest, uint8_t *src, uint32_t len)
{
    uint32_t left = 0;
    uint32_t size;
    uint8_t *src_pos;

    src_pos = src;
    left = len;
    dw_writel(spi_priv.regs, SPIM_ENR, 0);
    dw_writel(spi_priv.regs, SPIM_DMACR,   0x02);
    dw_writel(spi_priv.regs, SPIM_SER,     1);
    dw_writel(spi_priv.regs, SPIM_CTRLR0,
        (SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_TO << SPI_TMOD_OFFSET));

    spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[1] = 0x02;
    spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[0] = 0;
    while (0 != left) {
        size = min(BLOCK_TS, left);
        simple_dma_send_block(dest, src_pos, size);
        left -= size;
        src_pos += size;
    }

    return len;
}

/* 接收最多BLOCK_TS个字节数据 */
static inline int simple_dma_recv_block(uint8_t *dest, uint8_t *src, uint32_t len)
{
    dw_writel(spi_priv.regs, SPIM_ENR, 0);
    dw_writel(spi_priv.regs, SPIM_CTRLR1,  len-1);

    dw_dmac_cfg(dest, src, len, 1);

    dw_writel(spi_priv.regs, SPIM_ENR, 1);

    spl_priv.dw_dmac->CFG[0]   = 1;
    spl_priv.dw_dmac->CH_EN[0] = 0x101;

    dw_writel(spi_priv.regs, SPIM_TXDR, 0);

    while((spl_priv.dw_dmac->RAW.XFER[0] & (0X01 << SPI_DMA_CHANNEL)) == 0);

    spl_priv.dw_dmac->CFG[0]   = 0;
    spl_priv.dw_dmac->CH_EN[0] = 0;

    while(dw_readl(spi_priv.regs, SPIM_RXFLR) !=  0);
    while(dw_readl(spi_priv.regs, SPIM_SR) & 0x01);

    return 0;
}

static int simple_dma_recv(unsigned char *dest, unsigned char *src, unsigned len)
{
    uint32_t left = 0;
    uint32_t size;
    uint8_t *dst_pos;

    dst_pos = dest;
    left = len;
    dw_writel(spi_priv.regs, SPIM_ENR, 0);
    dw_writel(spi_priv.regs, SPIM_DMACR,   0x01);
    dw_writel(spi_priv.regs, SPIM_SER,     1);
    dw_writel(spi_priv.regs, SPIM_CTRLR0,
        (SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_RO << SPI_TMOD_OFFSET));

    spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[1] = 0x882;
    spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[0] = 0;
    while (0 != left) {
        size = min(BLOCK_TS, left);
        simple_dma_recv_block(dst_pos, src, size);
        left -= size;
        dst_pos += size;
    }
    return len;
}

static inline int poll_xfer(uint8_t *tx, uint8_t *rx, uint32_t len)
{
    uint32_t i, size;
    dw_writel(spi_priv.regs, SPIM_ENR,   0);
    dw_writel(spi_priv.regs, SPIM_SER,     1);
    dw_writel(spi_priv.regs, SPIM_DMACR, 0);

    if (tx) {
        dw_writel(spi_priv.regs, SPIM_CTRLR0,
            (SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_TO  << SPI_TMOD_OFFSET));
        dw_writel(spi_priv.regs, SPIM_ENR,   1);

        for (i = 0; i < len; i++) {
            while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_TF_NOT_FULL) == 0);
            dw_writel(spi_priv.regs, SPIM_TXDR, tx[i]);
        }

        while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_TF_EMPT) == 0); // 等待发送fifo 为空
        while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_BUSY) != 0);    // 等待发送busy位置0
    } else {
        dw_writel(spi_priv.regs, SPIM_CTRLR0,
            (SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_RO  << SPI_TMOD_OFFSET));

        while (len != 0) {
            dw_writel(spi_priv.regs, SPIM_ENR,   0);
            size = min_t(uint32_t, spi_priv.rx_fifo_len, len);
            dw_writel(spi_priv.regs, SPIM_CTRLR1,   size-1);
            dw_writel(spi_priv.regs, SPIM_ENR,   1);
            dw_writel(spi_priv.regs, SPIM_TXDR, 0);

            for (i = 0; i < size; i++) {
                while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_RF_NOT_EMPT) == 0);
                rx[i] = dw_readl(spi_priv.regs, SPIM_TXDR);
            }
            while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_RF_NOT_EMPT) != 0); // 等待接收fifo 为空
            rx += size;
            len -= size;
        }
    }

    return len;
}

int spl_xfer(uint8_t *tx, uint8_t *rx, uint32_t len, uint32_t flags)
{
    int ret = 0;

    if(flags & SPI_XFER_BEGIN)
        CS_ENABLE(0);

    u32 tmp_len = 0;
    uint32_t offset;

    if((spi_priv.mode >> 11 == 0x1) && len >= 64){
        if(rx){
            offset = ((uint32_t)rx) & 0x03;
            if (offset) {
                tmp_len = 4 - offset;
                poll_xfer(tx, rx, tmp_len);
                rx += tmp_len;
                len -= tmp_len;
            }
            tmp_len = len & ~0x03;
            ret = simple_dma_recv(rx, (u8*)(spi_priv.regs + SPIM_RXDR), tmp_len);
        }else if(tx) {
            offset = ((uint32_t)tx) & 0x03;
            if (offset) {
                tmp_len = 4 - offset;
                poll_xfer(tx, rx, tmp_len);
                tx += tmp_len;
                len -= tmp_len;
            }
            tmp_len = len & ~0x03;
            ret = simple_dma_send((u8*)(spi_priv.regs + SPIM_TXDR), tx, tmp_len);
        }

        dw_writel(spi_priv.regs, SPIM_DMACR, 0);

        if(len != tmp_len && ret >= 0){
            len -= tmp_len;
            tx += tx ? tmp_len : 0;
            rx += rx ? tmp_len : 0;
        }else{
            goto out;
        }
    }

    ret += poll_xfer(tx, rx, len);

out:

    if (flags & SPI_XFER_END)
        CS_ENABLE(1);

    return ret;

}

void spl_spi_init(int sample, int cs, int speed, int mode)
{
    uint32_t remainder;
    uint32_t div = uint32_div(CONFIG_DESIGNWARE_SPI_CLK, speed, &remainder);
    spi_priv.mode = mode;

    div = div < 2 ? 2 : div;
    spi_priv.freq = uint32_div(CONFIG_DESIGNWARE_SPI_CLK, div, &remainder);

    dw_writel(spi_priv.regs, SPIM_ENR, 0);

    dw_writel(spi_priv.regs, SPIM_IMR, 0x00);
    dw_writel(spi_priv.regs, SPIM_BAUDR, div);
    dw_writel(spi_priv.regs, SPIM_CTRLR0,
        (SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_TR  << SPI_TMOD_OFFSET));
    dw_writel(spi_priv.regs, SPIM_CTRLR1,  0);

    if (spi_priv.tx_fifo_len < 0) {
        uint32_t fifo;
        for (fifo = 1; fifo < 256; fifo++) {
            dw_writel(spi_priv.regs, SPIM_TXFTLR, fifo);
            if (fifo != dw_readl(spi_priv.regs, SPIM_TXFTLR))
                break;
        }
        spi_priv.tx_fifo_len = (fifo == 1) ? 0 : fifo;
        dw_writel(spi_priv.regs, SPIM_TXFTLR, 0);
    }

    if (spi_priv.rx_fifo_len < 0) {
        uint32_t fifo;
        for (fifo = 1; fifo < 256; fifo++) {
            dw_writel(spi_priv.regs, SPIM_RXFTLR, fifo);
            if (fifo != dw_readl(spi_priv.regs, SPIM_RXFTLR))
                break;
        }
        spi_priv.rx_fifo_len = (fifo == 1) ? 0 : fifo;
        dw_writel(spi_priv.regs, SPIM_RXFTLR, spi_priv.rx_fifo_len-1);
    }

    dw_writel(spi_priv.regs, SPIM_RX_SAMPLE_DLY, sample);
    dw_writel(spi_priv.regs, SPIM_SER,     1);

    spl_priv.dw_dmac = (dw_dma_regs_t*)GXSCPU_DMA_BASE;

    dw_writel(spi_priv.regs, SPIM_DMATDLR, 0x04);
    dw_writel(spi_priv.regs, SPIM_DMARDLR, 0x07);

    dw_writel(spi_priv.regs, SPIM_ENR, 0);
}
