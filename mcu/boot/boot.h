/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * boot.h: Definition of boot function
 *
 */

#ifndef __BOOT_H__
#define __BOOT_H__

#define LOG_TAG "<BOOT>"

#ifndef CONFIG_MCU_ENABLE_JTAG_DEBUG
typedef enum {
    BOOT_DEVICE_SPINOR,
    BOOT_DEVICE_SPINAND,
    BOOT_DEVICE_UART,
    BOOT_DEVICE_RAM,
    BOOT_DEVICE_USB_HIGH,
    BOOT_DEVICE_USB_FULL,
    BOOT_DEVICE_NONE
} BOOT_DEVICE;

typedef struct {
    BOOT_DEVICE boot_device;
    union {
        unsigned int value;

        struct {
            int uart_boot_port;
        } uart;

        struct {
            unsigned int boot_addr;
        } norflash;
    } info;
} BOOT_INFO;

extern BOOT_INFO boot_info;
#endif

/* Boot board init */
void BootBoardInit(void);
int BootBoardGetImageInfo(unsigned int *image_addr, unsigned int *image_size);

/* Boot Uart */
void BootUartInit(void);
void BootUartPut(int ch);
void BootUartPutString(const char *string);

/* Boot Delay */
void BootDelayInit(void);
void boot_mdelay(unsigned int msec);

/* Load Images */
int BootLoadUartImage(void);
int BootLoadRamImage(void);
int BootLoadSpiNorImage(void);
int BootLoadSpiNandImage(void);

#endif  /* __BOOT_H__ */
