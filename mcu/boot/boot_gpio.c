/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * boot_gpio.c: MCU GPIO Driver in BOOT
 *
 */

#include <types.h>

#include <base_addr.h>
#include <soc_config.h>

#include <boot/boot_gpio.h>

typedef struct {
    unsigned long  EPDDR;
    unsigned long  EPDDR_SET;
    unsigned long  EPDDR_CLR;
    unsigned long  reserve1;
    unsigned long  EPB_OUT;
    unsigned long  EPBSET;
    unsigned long  EPBCLR;
    unsigned long  EPDR;
    unsigned long  PWM_CHANNEL_SEL[4];
    unsigned long  PWM_MUX;
    unsigned long  DSEN;
    unsigned long  DSCNT;
    unsigned long  EPODR;
    unsigned long  INTC_EN;
    unsigned long  INTC_STA;
    unsigned long  INTC_MASK;
    unsigned long  reserve2;
    unsigned long  INTC_HIGHT;
    unsigned long  INTC_LOW;
    unsigned long  INTC_RISING;
    unsigned long  INTC_FALLING;
    unsigned long  reserve3[8];

    unsigned long  PWM_DACIN[8];
    unsigned long  reserve4[8];
    unsigned long  PWM_CYCLE[8];
    unsigned long  PWM_UPDATE;
    unsigned long  PWM_DACEN;
    unsigned long  reserve5[2];
    unsigned long  PWM_CLK_SEL;
}GPIO_REGS;

#define PWM_CHANNEL_SHIFT               0x8
#define PWM_CHANNEL_NUM                 0x8
#define PWM_CHANNEL_MASK                0xf
#define PWM_CHANNEL_WIDTH               0x4

#define PWM_SEL_CHANGE(data, shift) (((data >> 3 * shift) & 0x7) << (shift * 4))
#define PWM_SEL_READ(data) (PWM_SEL_CHANGE(data,0) | PWM_SEL_CHANGE(data,1) | PWM_SEL_CHANGE(data,2) | PWM_SEL_CHANGE(data,3) | PWM_SEL_CHANGE(data,4) | PWM_SEL_CHANGE(data,5) | PWM_SEL_CHANGE(data,6) | PWM_SEL_CHANGE(data,7))

#define ARRAY_MAX_COUNT 1000
#define NSEC_PER_SEC    1000000000L
#define CLOCK_PWM       CONFIG_GPIO_CLK

//=================================================================================================

static GPIO_REGS * _GpioGetRegsBase(unsigned int port)
{
    if (port < 32) {
        return (GPIO_REGS *)MCU_REG_BASE_GPIO1;
    } else if (port < 64) {
        return (GPIO_REGS *)MCU_REG_BASE_GPIO2;
    } else if (port < 96) {
        return (GPIO_REGS *)MCU_REG_BASE_GPIO3;
    } else
        return (GPIO_REGS *)NULL;
}

//=================================================================================================

GPIO_DIRECTION BootGpioGetDirection(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs)
        return (regs->EPDDR & (1 << (port % 32))) ? GPIO_DIRECTION_OUTPUT : GPIO_DIRECTION_INPUT;

    return -1;
}

int BootGpioSetDirection(unsigned int port, GPIO_DIRECTION direction)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs) {
        unsigned int offset = port % 32;
        if (direction)
            regs->EPDDR |= 1 << offset;
        else
            regs->EPDDR &= ~(1 << offset);
        return 0;
    }
    return -1;
}

//=================================================================================================

GPIO_LEVEL BootGpioGetLevel(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs)
        return (regs->EPDR & (1 << (port % 32))) ? GPIO_LEVEL_HIGH : GPIO_LEVEL_LOW;

    return -1;
}

int BootGpioSetLevel(unsigned int port, GPIO_LEVEL level)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs) {
        unsigned int offset = port % 32;
        if (level == GPIO_LEVEL_HIGH)
            regs->EPBSET = 1 << offset;
        else
            regs->EPBCLR = 1 << offset;
        return 0;
    }
    return -1;
}
