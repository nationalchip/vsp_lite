/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * watchdog.c: watchdog driver
 *
 */
#include <common.h>
#include <base_addr.h>
#include <board_config.h>
#include <driver/irq.h>
#include <driver/watchdog.h>

#include "watchdog_private.h"

#define GX_WDT_RATE 1000 /* 1ms */

#if defined(CONFIG_GX8010NRE) || defined(CONFIG_GX8008B)
#define GX_WDT_IRQ_NUM MCU_IRQ_WDT
#endif

static GX_WATCHDOG wdt_dev = {
    .regs = (void *)MCU_REG_BASE_WDT,
    .pclk = CONFIG_WDT_SYS_CLK,
};

static inline uint32_t GxWatchdogReadl(GX_WATCHDOG *dev,
        uint32_t offset)
{
    return readl((uint32_t)dev->regs + offset);
}

static inline void GxWatchdogWritel(GX_WATCHDOG *dev,
        uint32_t offset, uint32_t value)
{
    writel(value, (uint32_t)dev->regs + offset);
}

static void GxWatchdogStart(GX_WATCHDOG *dev)
{
    uint32_t value;

    value = GxWatchdogReadl(dev, GX_WDT_CTRL);
    value |= WDT_ENABLE_MASK;
    GxWatchdogWritel(dev, GX_WDT_CTRL, value);
}

static void GxWatchdogStop(GX_WATCHDOG *dev)
{
    uint32_t value;

    value = GxWatchdogReadl(dev, GX_WDT_CTRL);
    value &= ~WDT_ENABLE_MASK;
    GxWatchdogWritel(dev, GX_WDT_CTRL, value);
}

static void GxWatchdogSetResetTimeout(GX_WATCHDOG *dev)
{
    uint32_t value;

    value = GxWatchdogReadl(dev, GX_WDT_MATCH) & ~WDT_COUNT_INI;
    value |= WDT_COUNT_OVERFLOW_VAL - dev->reset_timeout_ms;
    GxWatchdogWritel(dev, GX_WDT_MATCH, value);
}

static void GxWatchdogSetLevelTimeout(GX_WATCHDOG *dev)
{
    uint32_t value;
    uint32_t count;

    value = GxWatchdogReadl(dev, GX_WDT_WSR) & ~WDT_COUNT_LEVEL;
    count = GxWatchdogReadl(dev, GX_WDT_COUNT);
    value |= (count + dev->level_timeout_ms) << WDT_COUNT_LEVEL_POS;
    GxWatchdogWritel(dev, GX_WDT_WSR, value);
}

static void gx_wdt_set_clock(GX_WATCHDOG *dev, uint32_t pclk)
{
    dev->pclk = pclk;
    dev->div = pclk / GX_WDT_RATE - 1;

    GxWatchdogWritel(dev, GX_WDT_MATCH, dev->div << WDT_COUNT_DIV_POS);
}

static void GxWatchdogPing(GX_WATCHDOG *dev)
{
    uint32_t value;

    value = GxWatchdogReadl(dev, GX_WDT_WSR) & ~WDT_WSR;
    GxWatchdogWritel(dev, GX_WDT_WSR, value | 0x5555);
    GxWatchdogWritel(dev, GX_WDT_WSR, value | 0xAAAA);
}

static int GxWatchdogIrqHandler(int irq, void *pdata)
{
    uint32_t value;

    ((irq_handler_t)wdt_dev.irq_handler)(irq, pdata);

    /* Clear level interrupt flag */
    value = GxWatchdogReadl(&wdt_dev, GX_WDT_CTRL) | WDT_LEVEL_INT_FLAG;
    GxWatchdogWritel(&wdt_dev, GX_WDT_CTRL, value);

    return 0;
}

static void GxWatchdogInit(GX_WATCHDOG *dev)
{
    GxWatchdogStop(dev);
    gx_wdt_set_clock(dev, CONFIG_WDT_SYS_CLK);
    GxWatchdogSetResetTimeout(dev);
    if (dev->irq_handler && dev->level_timeout_ms < dev->reset_timeout_ms) {
        GxWatchdogSetLevelTimeout(dev);
        gx_request_irq(GX_WDT_IRQ_NUM, GxWatchdogIrqHandler, NULL);
    }
    GxWatchdogStart(dev);

#ifdef DEBUG
    printf("WDT_CTRL  0x%x\n", GxWatchdogReadl(&wdt_dev, GX_WDT_CTRL));
    printf("WDT_MATCH 0x%x\n", GxWatchdogReadl(&wdt_dev, GX_WDT_MATCH));
    printf("WDT_COUNT 0x%x\n", GxWatchdogReadl(&wdt_dev, GX_WDT_COUNT));
    printf("WDT_WSR   0x%x\n", GxWatchdogReadl(&wdt_dev, GX_WDT_WSR));
#endif
}

void WatchdogPing(void)
{
    GxWatchdogPing(&wdt_dev);
}

void WatchdogInit(uint16_t reset_timeout_ms,
        uint16_t level_timeout_ms, irq_handler_t irq_handler)
{
    wdt_dev.reset_timeout_ms = reset_timeout_ms;
    wdt_dev.level_timeout_ms = level_timeout_ms;
    wdt_dev.irq_handler = irq_handler;
    GxWatchdogInit(&wdt_dev);
}

void WatchdogDone(void)
{
    GxWatchdogStop(&wdt_dev);
}
