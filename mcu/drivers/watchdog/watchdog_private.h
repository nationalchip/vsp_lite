/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * watchdog_private.h: watchdog private header
 *
 */

#ifndef __GX_WDT_PRIV_H__
#define __GX_WDT_PRIV_H__

#include <common.h>

/* Watchdog register offset */
#define GX_WDT_CTRL  0x00 /* Control Register */
#define GX_WDT_MATCH 0x04 /* Match Register */
#define GX_WDT_COUNT 0x08 /* Count Register */
#define GX_WDT_WSR   0x0C /* WSR Register */

/* Definition for the Watchdog Control Register */
#define WDT_EN             (1UL << 0) /* The module enable of Watchdog */
#define WDT_SYS_RESET_EN   (1UL << 1) /* The system reset enable control signal */
#define WDT_SYS_RESET_FLAG (1UL << 2) /* The system reset flag singal */
#define WDT_LEVEL_INT_EN   (1UL << 3) /* The level interrupt enable control singal */
#define WDT_LEVEL_INT_FLAG (1UL << 4) /* The level interrupt flag signal */

#define WDT_ENABLE_MASK (WDT_EN | WDT_SYS_RESET_EN | WDT_LEVEL_INT_EN)

/* Definition for the MATCH Register */
/* The value of starting count */
#define WDT_COUNT_INI_POS 0
#define WDT_COUNT_INI (0xFFFFUL << WDT_COUNT_INI_POS)

#define WDT_COUNT_OVERFLOW_VAL 0x10000

/* The divide value of count clock, count_clock = sys_clk / (count_div + 1)*/
#define WDT_COUNT_DIV_POS 16
#define WDT_COUNT_DIV (0xFFFFUL << WDT_COUNT_DIV_POS)

/* Definition for the COUNT Register */
#define WDT_COUNT (0xFFFFUL << 0) /* The value of current count */

/* Definition for the WSR Register */

/* Write 0x5555 and 0xAAAA in turn to service the Watchdog to
 * relaod the count with initialization count configured */
#define WDT_WSR_POS 0
#define WDT_WSR (0xFFFFUL << WDT_WSR_POS)

 /* The level value of interrupt to need service the Watchdog */
#define WDT_COUNT_LEVEL_POS 16
#define WDT_COUNT_LEVEL (0xFFFFUL << WDT_COUNT_LEVEL_POS)

typedef struct gx_wdt {
    void *regs;
    uint32_t pclk;
    uint32_t div;
    uint16_t reset_timeout_ms;
    uint16_t level_timeout_ms;
    void *irq_handler;
} GX_WATCHDOG;

#endif
