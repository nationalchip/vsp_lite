/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * dw_i2c: I2C Driver
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <base_addr.h>
#include <soc_config.h>

#include <driver/i2c.h>
#include <driver/delay.h>
#include "driver/irq.h"

//#define CONFIG_I2C_DEBUG
#ifdef CONFIG_I2C_DEBUG
#define I2C_DBG(...)   printf(__VA_ARGS__)
#else
#define I2C_DBG(fmt, args...)   ((void)0)
#endif

#define MAX_I2C_DEVICES (2)
#define STD_SPEED       (100000)
#define FAST_SPEED      (400000)
#define SLV_ACTIVITY    (1 << 6)
#define SLV_NOT_CLOSE   (0)
#define SLV_NEED_CLOSE  (1)
#define SLV_CLOSE_OK    (2)

static unsigned char slave_close_flag = SLV_NOT_CLOSE;
struct i2c_rdwr_ioctl_data {
	struct i2c_msg msgs[2];
	int nmsgs;
};

struct i2c_info {
	unsigned int  regs;
	unsigned int  i2c_pclk;
	unsigned int  i2c_bus_freq;
	unsigned int  hight_time;
	unsigned int  low_time;
	unsigned int  chip_address;
	unsigned int  irq;
};

struct gx_i2c_device {
	int           i2c_devid; /* the id is i2c device id */
	int           used;
	unsigned int  chip_address;
	unsigned int  address_width; /* 1 or 2 bytes, often */
	int           send_noack;
	int           send_stop;
	int           retries;
	struct i2c_info      *dev;
	struct gx_i2c_device *next;
};

enum i2c_mode {
	I2C_MASTER = 0,
	I2C_SLAVE,
	I2C_NONE
};


#define I2C0_CHIP_ADDR    (0x11)
#define I2C1_CHIP_ADDR    (0x22)
#define I2C_OK            (0)
#define I2C_ERR           (-1)

static struct i2c_info g_i2c_info[] = {
	{(MCU_REG_BASE_I2C0), CONFIG_APB2_CLK, 400000UL, 1250, 1250, I2C0_CHIP_ADDR, MCU_IRQ_DW_I2C1},
	{(MCU_REG_BASE_I2C1), CONFIG_APB2_CLK, 400000UL, 1250, 1250, I2C1_CHIP_ADDR, MCU_IRQ_DW_I2C0},
};

#define ___constant_swab32(x) \
    ((__u32)( \
        (((__u32)(x) & (__u32)0x000000ffUL) << 24) | \
        (((__u32)(x) & (__u32)0x0000ff00UL) <<  8) | \
        (((__u32)(x) & (__u32)0x00ff0000UL) >>  8) | \
        (((__u32)(x) & (__u32)0xff000000UL) >> 24) ))

#define BIT(n)        (1 << n)
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define readl(addr) ({ unsigned int __v = (*(volatile unsigned int *) (addr)); __v; })
#define writel(b,addr) (void)((*(volatile unsigned int *) (addr)) = (b))

static inline int __ffs(unsigned int word)
{
	int num = 0;
#if BITS_PER_LONG == 64
	if ((word & 0xffffffff) == 0) {
		num += 32;
		word >>= 32;
	}
#endif
	if ((word & 0xffff) == 0) {
		num += 16;
		word >>= 16;
	}
	if ((word & 0xff) == 0) {
		num += 8;
		word >>= 8;
	}
	if ((word & 0xf) == 0) {
		num += 4;
		word >>= 4;
	}
	if ((word & 0x3) == 0) {
		num += 2;
		word >>= 2;
	}
	if ((word & 0x1) == 0)
		num += 1;
	return num;
}

/*
 * FIND_FIRST_BIT - find the first set bit in a memory region
 * @addr: The address to start the search at
 * @size: The maximum size to search
 *
 * Returns the bit-number of the first set bit, not the number of the byte
 * containing a bit.
 */
static inline int FIND_FIRST_BIT (const unsigned long * addr, int size)
{
	const unsigned long *p = addr;
	int result = 0;
	int tmp;

	while (size & ~31UL) {
		if ((tmp = *(p++)))
			goto found;
		result += 32;
		size -= 32;
	}
	if (!size)
		return result;

	tmp = (*p) & (~0UL >> (32 - size));
	if (tmp == 0UL)         /* Are any bits set? */
		return result + size;   /* Nope. */
found:
	return result + __ffs(tmp);
}

/*
 * FIND_NEXT_BIT - find the next set bit in a memory region
 * @addr: The address to base the search on
 * @offset: The bitnumber to start searching at
 * @size: The maximum size to search
 */
static inline int FIND_NEXT_BIT (const unsigned long * addr,
		int size, int offset)
{
	const unsigned long *p = addr + (offset >> 5);
	int result = offset & ~31UL;
	int tmp;

	if (offset >= size)
		return size;
	size -= result;
	offset &= 31UL;
	if (offset) {
		tmp = *(p++);
		tmp &= (~0UL << offset);
		if (size < 32)
			goto found_first;
		if (tmp)
			goto found_middle;
		size -= 32;
		result += 32;
	}
	while (size & ~31UL) {
		if ((tmp = *(p++)))
			goto found_middle;
		result += 32;
		size -= 32;
	}
	if (!size)
		return result;
	tmp = *p;

found_first:
	tmp &= (~0UL >> (32 - size));
	if (tmp == 0UL)         /* Are any bits set? */
		return result + size;   /* Nope. */
found_middle:
	return result + __ffs(tmp);
}

/*
 * Registers offset
 */
#define DW_IC_CON           0x0
#define DW_IC_TAR           0x4
#define DW_IC_SAR           0x8
#define DW_IC_DATA_CMD      0x10
#define DW_IC_SS_SCL_HCNT   0x14
#define DW_IC_SS_SCL_LCNT   0x18
#define DW_IC_FS_SCL_HCNT   0x1c
#define DW_IC_FS_SCL_LCNT   0x20
#define DW_IC_INTR_STAT     0x2c
#define DW_IC_INTR_MASK     0x30
#define DW_IC_RAW_INTR_STAT 0x34
#define DW_IC_RX_TL         0x38
#define DW_IC_TX_TL         0x3c
#define DW_IC_CLR_INTR      0x40
#define DW_IC_CLR_RX_UNDER  0x44
#define DW_IC_CLR_RX_OVER   0x48
#define DW_IC_CLR_TX_OVER   0x4c
#define DW_IC_CLR_RD_REQ    0x50
#define DW_IC_CLR_TX_ABRT   0x54
#define DW_IC_CLR_RX_DONE   0x58
#define DW_IC_CLR_ACTIVITY  0x5c
#define DW_IC_CLR_STOP_DET  0x60
#define DW_IC_CLR_START_DET 0x64
#define DW_IC_CLR_GEN_CALL  0x68
#define DW_IC_ENABLE        0x6c
#define DW_IC_STATUS        0x70
#define DW_IC_TXFLR         0x74
#define DW_IC_RXFLR         0x78
#define DW_IC_SDA_SETUP     0x94
#define DW_IC_ENABLE_STATUS 0x9c
#define DW_IC_COMP_PARAM_1  0xf4
#define DW_IC_COMP_TYPE     0xfc
#define DW_IC_COMP_TYPE_VALUE       0x44570140 //add by huangjb.   copy from the newest driver.
#define DW_IC_TX_ABRT_SOURCE        0x80

#define DW_IC_CON_MASTER            0x1
#define DW_IC_CON_SPEED_STD         0x2
#define DW_IC_CON_SPEED_FAST        0x4
#define DW_IC_CON_10BITADDR_MASTER  0x10
#define DW_IC_CON_RESTART_EN        0x20
#define DW_IC_CON_SLAVE_DISABLE     0x40

#define DW_IC_INTR_RX_UNDER  0x001
#define DW_IC_INTR_RX_OVER   0x002
#define DW_IC_INTR_RX_FULL   0x004
#define DW_IC_INTR_TX_OVER   0x008
#define DW_IC_INTR_TX_EMPTY  0x010
#define DW_IC_INTR_RD_REQ    0x020
#define DW_IC_INTR_TX_ABRT   0x040
#define DW_IC_INTR_RX_DONE   0x080
#define DW_IC_INTR_ACTIVITY  0x100
#define DW_IC_INTR_STOP_DET  0x200
#define DW_IC_INTR_START_DET 0x400
#define DW_IC_INTR_GEN_CALL  0x800

#define DW_IC_INTR_DEFAULT_MASK     (DW_IC_INTR_RX_FULL | \
        DW_IC_INTR_TX_EMPTY | \
        DW_IC_INTR_TX_ABRT | \
        DW_IC_INTR_STOP_DET)

#define DW_IC_INTR_SLAVE_MASK       (DW_IC_INTR_RX_FULL | \
        DW_IC_INTR_TX_ABRT | \
        DW_IC_INTR_STOP_DET | \
        DW_IC_INTR_RD_REQ)

#define DW_IC_STATUS_ACTIVITY 0x1

#define DW_IC_ERR_TX_ABRT     0x1

/*
 * status codes
 */
#define STATUS_IDLE                 0x0
#define STATUS_WRITE_IN_PROGRESS    0x1
#define STATUS_READ_IN_PROGRESS     0x2

#define TIMEOUT         (20) /* ms */
#define SDA_SETUP_TIME  (3470) /* ns */
#define SDA_SETUP_DIV   (1000)
#define SDA_SETUP       (CONFIG_APB2_CLK / 1000000 * SDA_SETUP_TIME / SDA_SETUP_DIV + 1)
#define MAX_POLL_COUNT  (1000)

/*
 * hardware abort codes from the DW_IC_TX_ABRT_SOURCE register
 *
 * only expected abort codes are listed here
 * refer to the datasheet for the full list
 */
#define ABRT_7B_ADDR_NOACK  0
#define ABRT_10ADDR1_NOACK  1
#define ABRT_10ADDR2_NOACK  2
#define ABRT_TXDATA_NOACK   3
#define ABRT_GCALL_NOACK    4
#define ABRT_GCALL_READ     5
#define ABRT_SBYTE_ACKDET   7
#define ABRT_SBYTE_NORSTRT  9
#define ABRT_10B_RD_NORSTRT 10
#define ABRT_MASTER_DIS     11
#define ARB_LOST            12

#define DW_IC_TX_ABRT_7B_ADDR_NOACK    (1UL << ABRT_7B_ADDR_NOACK)
#define DW_IC_TX_ABRT_10ADDR1_NOACK    (1UL << ABRT_10ADDR1_NOACK)
#define DW_IC_TX_ABRT_10ADDR2_NOACK    (1UL << ABRT_10ADDR2_NOACK)
#define DW_IC_TX_ABRT_TXDATA_NOACK     (1UL << ABRT_TXDATA_NOACK)
#define DW_IC_TX_ABRT_GCALL_NOACK      (1UL << ABRT_GCALL_NOACK)
#define DW_IC_TX_ABRT_GCALL_READ       (1UL << ABRT_GCALL_READ)
#define DW_IC_TX_ABRT_SBYTE_ACKDET     (1UL << ABRT_SBYTE_ACKDET)
#define DW_IC_TX_ABRT_SBYTE_NORSTRT    (1UL << ABRT_SBYTE_NORSTRT)
#define DW_IC_TX_ABRT_10B_RD_NORSTRT   (1UL << ABRT_10B_RD_NORSTRT)
#define DW_IC_TX_ABRT_MASTER_DIS       (1UL << ABRT_MASTER_DIS)
#define DW_IC_TX_ARB_LOST              (1UL << ARB_LOST)

#define DW_IC_TX_ABRT_NOACK            (DW_IC_TX_ABRT_7B_ADDR_NOACK | \
        DW_IC_TX_ABRT_10ADDR1_NOACK | \
        DW_IC_TX_ABRT_10ADDR2_NOACK | \
        DW_IC_TX_ABRT_TXDATA_NOACK | \
        DW_IC_TX_ABRT_GCALL_NOACK)

#ifdef CONFIG_I2C_DEBUG
static char *abort_sources[] = {
	[ABRT_7B_ADDR_NOACK] =
		"slave address not acknowledged (7bit mode)",
	[ABRT_10ADDR1_NOACK] =
		"first address byte not acknowledged (10bit mode)",
	[ABRT_10ADDR2_NOACK] =
		"second address byte not acknowledged (10bit mode)",
	[ABRT_TXDATA_NOACK] =
		"data not acknowledged",
	[ABRT_GCALL_NOACK] =
		"no acknowledgement for a general call",
	[ABRT_GCALL_READ] =
		"read after general call",
	[ABRT_SBYTE_ACKDET] =
		"start byte acknowledged",
	[ABRT_SBYTE_NORSTRT] =
		"trying to send start byte when restart is disabled",
	[ABRT_10B_RD_NORSTRT] =
		"trying to read when restart is disabled (10bit mode)",
	[ABRT_MASTER_DIS] =
		"trying to use disabled adapter",
	[ARB_LOST] =
		"lost arbitration",
};
#endif
/**
 * struct dw_i2c_dev - private i2c-designware data
 * @base: IO registers pointer
 * @cmd_err: run time hadware error code
 * @msgs: points to an array of messages currently being transferred
 * @msgs_num: the number of elements in msgs
 * @msg_write_idx: the element index of the current tx message in the msgs
 *	array
 * @tx_buf_len: the length of the current tx buffer
 * @tx_buf: the current tx buffer
 * @msg_read_idx: the element index of the current rx message in the msgs
 *	array
 * @rx_buf_len: the length of the current rx buffer
 * @rx_buf: the current rx buffer
 * @msg_err: error status of the current transfer
 * @status: i2c master status, one of STATUS_*
 * @abort_source: copy of the TX_ABRT_SOURCE register
 * @irq: interrupt number for the i2c master
 * @swab: true if the instantiated IP is of different endianess
 * @adapter: i2c subsystem adapter node
 * @tx_fifo_depth: depth of the hardware tx fifo
 * @rx_fifo_depth: depth of the hardware rx fifo
 */
struct dw_i2c_dev {
	unsigned int        src_clk;
	unsigned int        scl_clk;
	unsigned int        high_time;
	unsigned int        low_time;
	unsigned int        base;
	int                 cmd_err;
	struct i2c_msg     *msgs;
	int                 msgs_num;
	int                 msg_write_idx;
	u32                 tx_buf_len;
	u8                  *tx_buf;
	int                 msg_read_idx;
	u32                 rx_buf_len;
	u8                  *rx_buf;
	int                 msg_err;
	unsigned int        status;
	u32                 abort_source;
	int                 irq;
	int                 swab;
	int                 rx_outstanding;
	unsigned int        tx_fifo_depth;
	unsigned int        rx_fifo_depth;

	struct dw_i2c_dev  *next;
	unsigned int        i2c_devid;
	unsigned char       used;
	unsigned int        chip_address;
	unsigned int        address_width;
	int                 retries;
	int                 finish_flag;
	unsigned int        rx_index;
	unsigned int        tx_index;
	i2c_slave_cb_t      slave_cb;
	enum i2c_mode       role;
};

static struct dw_i2c_dev g_gx_i2c_device[MAX_I2C_DEVICES] = {{0}};
struct dw_i2c_dev *g_i2c_list = NULL;
#define I2C_WAIT_TIMES  200000

static u32 dw_readl(struct dw_i2c_dev *dev, int offset)
{
	u32 value = readl(dev->base + offset);

	if (dev->swab)
		return swab32(value);
	else
		return value;
}

static void dw_writel(struct dw_i2c_dev *dev, u32 b, int offset)
{
	if (dev->swab)
		b = swab32(b);

	writel(b, dev->base + offset);
}

static u32 i2c_dw_scl_hcnt(u32 ic_clk, u32 tSYMBOL, u32 tf, int cond, int offset)
{
	/*
	 * DesignWare I2C core doesn't seem to have solid strategy to meet
	 * the tHD;STA timing spec.  Configuring _HCNT based on tHIGH spec
	 * will result in violation of the tHD;STA spec.
	 */
	if (cond)
		/*
		 * Conditional expression:
		 *
		 *   IC_[FS]S_SCL_HCNT + (1+4+3) >= IC_CLK * tHIGH
		 *
		 * This is based on the DW manuals, and represents an ideal
		 * configuration.  The resulting I2C bus speed will be
		 * faster than any of the others.
		 *
		 * If your hardware is free from tHD;STA issue, try this one.
		 */
		return (ic_clk * tSYMBOL + 500000) / 1000000 - 8 + offset;
	else
		/*
		 * Conditional expression:
		 *
		 *   IC_[FS]S_SCL_HCNT + 3 >= IC_CLK * (tHD;STA + tf)
		 *
		 * This is just experimental rule; the tHD;STA period turned
		 * out to be proportinal to (_HCNT + 3).  With this setting,
		 * we could meet both tHIGH and tHD;STA timing specs.
		 *
		 * If unsure, you'd better to take this alternative.
		 *
		 * The reason why we need to take into account "tf" here,
		 * is the same as described in i2c_dw_scl_lcnt().
		 */
		return (ic_clk * (tSYMBOL + tf) + 500000) / 1000000 - 3 + offset;
}

static u32 i2c_dw_scl_lcnt(u32 ic_clk, u32 tLOW, u32 tf, int offset)
{
	/*
	 * Conditional expression:
	 *
	 *   IC_[FS]S_SCL_LCNT + 1 >= IC_CLK * (tLOW + tf)
	 *
	 * DW I2C core starts counting the SCL CNTs for the LOW period
	 * of the SCL clock (tLOW) as soon as it pulls the SCL line.
	 * In order to meet the tLOW timing spec, we need to take into
	 * account the fall time of SCL signal (tf).  Default tf value
	 * should be 0.3 us, for safety.
	 */
	return ((ic_clk * (tLOW + tf) + 500000) / 1000000) - 1 + offset;
}

/**
 * i2c_dw_init() - initialize the designware i2c master hardware
 * @dev: device private data
 *
 * This functions configures and enables the I2C master.
 * This function is called during I2C init function, and in case of timeout at
 * run time.
 */
static int i2c_dw_init(struct dw_i2c_dev *dev)
{
	u32 input_clock_khz = dev->src_clk / 1000;
	u32 ic_con, hcnt, lcnt;
	u32 reg;

	/* Configure register endianess access */
	reg = dw_readl(dev, DW_IC_COMP_TYPE);
	if (reg == ___constant_swab32(DW_IC_COMP_TYPE_VALUE)) {
		dev->swab = 1;
		reg = DW_IC_COMP_TYPE_VALUE;
	}

	if (reg != DW_IC_COMP_TYPE_VALUE) {
		I2C_DBG("Unknown Synopsys component type: "
				"0x%08x\n", reg);
		return -ENODEV;
	}

	/* Disable the adapter */
	dw_writel(dev, 0, DW_IC_ENABLE);

	/* set standard and fast speed deviders for high/low periods */

	/* Standard-mode */
	hcnt = i2c_dw_scl_hcnt(input_clock_khz,
			dev->high_time,	/* tHD;STA = tHIGH = 4.0 us */
			0,	/* tf = 0 us */
			0,	/* 0: DW default, 1: Ideal */
			0);	/* No offset */
	lcnt = i2c_dw_scl_lcnt(input_clock_khz,
			dev->low_time,	/* tLOW = 4.7 us */
			0,	/* tf = 0 us */
			0);	/* No offset */
	dw_writel(dev, hcnt, DW_IC_SS_SCL_HCNT);
	dw_writel(dev, lcnt, DW_IC_SS_SCL_LCNT);
	I2C_DBG("Standard-mode HCNT:LCNT = %d:%d\n", hcnt, lcnt);

	/* Fast-mode */
	hcnt = i2c_dw_scl_hcnt(input_clock_khz,
			dev->high_time,	/* tHD;STA = tHIGH = 0.6 us */
			0,	/* tf = 0 us */
			0,	/* 0: DW default, 1: Ideal */
			0);	/* No offset */
	lcnt = i2c_dw_scl_lcnt(input_clock_khz,
			dev->low_time,	/* tLOW = 1.3 us */
			0,	/* tf = 0 us */
			0);	/* No offset */
	dw_writel(dev, hcnt, DW_IC_FS_SCL_HCNT);
	dw_writel(dev, lcnt, DW_IC_FS_SCL_LCNT);
	I2C_DBG("Fast-mode HCNT:LCNT = %d:%d\n", hcnt, lcnt);

	/* Configure Tx/Rx FIFO threshold levels */
	dw_writel(dev, dev->tx_fifo_depth - 1, DW_IC_TX_TL);
	dw_writel(dev, 0, DW_IC_RX_TL);

	/* configure the i2c master */
	ic_con = DW_IC_CON_MASTER | DW_IC_CON_SLAVE_DISABLE |
		DW_IC_CON_RESTART_EN | DW_IC_CON_SPEED_FAST;
	if (dev->scl_clk <= STD_SPEED)
		ic_con = DW_IC_CON_MASTER | DW_IC_CON_SLAVE_DISABLE |
			DW_IC_CON_RESTART_EN | DW_IC_CON_SPEED_STD;
	else if (dev->scl_clk <= FAST_SPEED)
		ic_con = DW_IC_CON_MASTER | DW_IC_CON_SLAVE_DISABLE |
			DW_IC_CON_RESTART_EN | DW_IC_CON_SPEED_FAST;
	dw_writel(dev, ic_con, DW_IC_CON);
	return 0;
}

static int i2c_slave_callback(struct i2c_slave_callback_param *callback_param)
{
	switch (callback_param->event) {
		case I2C_SLAVE_STOP:
			break;

		case I2C_SLAVE_RECEIVE_DATA:
			break;

		case I2C_SLAVE_REQUESTED_DATA:
			callback_param->data = 0x55;
			break;

		default:
			break;
	}
	return 0;
}

static int i2c_dw_init_slave(struct dw_i2c_dev *dev)
{
	u32 reg;

	/* Configure register endianess access */
	reg = dw_readl(dev, DW_IC_COMP_TYPE);
	if (reg == ___constant_swab32(DW_IC_COMP_TYPE_VALUE)) {
		dev->swab = 1;
		reg = DW_IC_COMP_TYPE_VALUE;
	}

	if (reg != DW_IC_COMP_TYPE_VALUE) {
		printf("Unknown Synopsys component type: "
				"0x%08x\n", reg);
		return -ENODEV;
	}

	/* Disable the adapter */
	dw_writel(dev, 0, DW_IC_ENABLE);

	/* Config SDA_SETUP */
	dw_writel(dev, SDA_SETUP, DW_IC_SDA_SETUP);

	/* Configure Tx/Rx FIFO threshold levels */
	dw_writel(dev, 0, DW_IC_TX_TL);
	dw_writel(dev, 0, DW_IC_RX_TL);

	/* configure the i2c slave */
	dw_writel(dev, 0, DW_IC_CON);

	/* Config slave adress */
	dw_writel(dev, dev->chip_address, DW_IC_SAR);

	/* Config irq */
	dw_writel(dev, DW_IC_INTR_SLAVE_MASK, DW_IC_INTR_MASK);

	/* Clear all interrupts */
	dw_readl(dev, DW_IC_CLR_INTR);

	/* Enable the adapter */
	dw_writel(dev, 1, DW_IC_ENABLE);

	return 0;
}
/*
 * Waiting for bus not busy
 */
extern void mdelay(unsigned int msec);
static int i2c_dw_wait_bus_not_busy(struct dw_i2c_dev *dev)
{
	int timeout = TIMEOUT;

	while (dw_readl(dev, DW_IC_STATUS) & DW_IC_STATUS_ACTIVITY) {
		if (timeout <= 0) {
			I2C_DBG("timeout waiting for bus ready\n");
			return -ETIMEDOUT;
		}
		timeout--;
		mdelay(1);
	}

	return 0;
}

static void i2c_dw_xfer_init(struct dw_i2c_dev *dev)
{
	struct i2c_msg *msgs = dev->msgs;
	u32 ic_con;

	/* Disable the adapter */
	dw_writel(dev, 0, DW_IC_ENABLE);

	/* set the slave (target) address */
	dw_writel(dev, msgs[dev->msg_write_idx].addr, DW_IC_TAR);

	/* if the slave address is ten bit address, enable 10BITADDR */
	ic_con = dw_readl(dev, DW_IC_CON);
	if (msgs[dev->msg_write_idx].flags & I2C_M_TEN)
		ic_con |= DW_IC_CON_10BITADDR_MASTER;
	else
		ic_con &= ~DW_IC_CON_10BITADDR_MASTER;
	dw_writel(dev, ic_con, DW_IC_CON);

	/* Enable the adapter */
	dw_writel(dev, 1, DW_IC_ENABLE);

	/* Enable interrupts */
	dw_writel(dev, DW_IC_INTR_DEFAULT_MASK, DW_IC_INTR_MASK);
}

/*
 * Initiate (and continue) low level master read/write transaction.
 * This function is only called from i2c_dw_isr, and pumping i2c_msg
 * messages into the tx buffer.  Even if the size of i2c_msg data is
 * longer than the size of the tx buffer, it handles everything.
 */
static void i2c_dw_xfer_msg(struct dw_i2c_dev *dev)
{
	struct i2c_msg *msgs = dev->msgs;
	u32 intr_mask;
	int tx_limit, rx_limit;
	u32 addr = msgs[dev->msg_write_idx].addr;
	u32 buf_len = dev->tx_buf_len;
	u8 *buf = dev->tx_buf;

	intr_mask = DW_IC_INTR_DEFAULT_MASK;

	for (; dev->msg_write_idx < dev->msgs_num; dev->msg_write_idx++) {
		/*
		 * if target address has changed, we need to
		 * reprogram the target address in the i2c
		 * adapter when we are done with this transfer
		 */
		if (msgs[dev->msg_write_idx].addr != addr) {
			I2C_DBG("%s: invalid target address\n", __func__);
			dev->msg_err = -EINVAL;
			break;
		}

		if (msgs[dev->msg_write_idx].len == 0) {
			I2C_DBG("%s: invalid message length\n", __func__);
			dev->msg_err = -EINVAL;
			break;
		}

		if (!(dev->status & STATUS_WRITE_IN_PROGRESS)) {
			/* new i2c_msg */
			buf = msgs[dev->msg_write_idx].buf;
			buf_len = msgs[dev->msg_write_idx].len;
		}

		tx_limit = dev->tx_fifo_depth - dw_readl(dev, DW_IC_TXFLR);
		rx_limit = dev->rx_fifo_depth - dw_readl(dev, DW_IC_RXFLR);

		while (buf_len > 0 && tx_limit > 0 && rx_limit > 0) {
			u32 cmd = 0;

			if (dev->msg_write_idx == dev->msgs_num - 1 && buf_len == 1)
				cmd |= BIT(9);

			if (msgs[dev->msg_write_idx].flags & I2C_M_RD) {
				//dw_writel(dev, 0x100, DW_IC_DATA_CMD);
				if (rx_limit - dev->rx_outstanding <= 0)
					break;
				dw_writel(dev, cmd | 0x100, DW_IC_DATA_CMD);//huangjb:the newest one
				rx_limit--;
				dev->rx_outstanding++;
			} else
				//dw_writel(dev, *buf++, DW_IC_DATA_CMD);
				dw_writel(dev, cmd | *buf++, DW_IC_DATA_CMD);//huangjb:the newest one
			tx_limit--; buf_len--;
		}

		dev->tx_buf = buf;
		dev->tx_buf_len = buf_len;

		if (buf_len > 0) {
			/* more bytes to be written */
			dev->status |= STATUS_WRITE_IN_PROGRESS;
			break;
		} else
			dev->status &= ~STATUS_WRITE_IN_PROGRESS;
	}

	/*
	 * If i2c_msg index search is completed, we don't need TX_EMPTY
	 * interrupt any more.
	 */
	if (dev->msg_write_idx == dev->msgs_num)
		intr_mask &= ~DW_IC_INTR_TX_EMPTY;

	if (dev->msg_err)
		intr_mask = 0;

	dw_writel(dev, intr_mask, DW_IC_INTR_MASK);
}

static void i2c_dw_read(struct dw_i2c_dev *dev)
{
	struct i2c_msg *msgs = dev->msgs;
	int rx_valid;

	for (; dev->msg_read_idx < dev->msgs_num; dev->msg_read_idx++) {
		u32 len;
		u8 *buf;

		if (!(msgs[dev->msg_read_idx].flags & I2C_M_RD))
			continue;

		if (!(dev->status & STATUS_READ_IN_PROGRESS)) {
			len = msgs[dev->msg_read_idx].len;
			buf = msgs[dev->msg_read_idx].buf;
		} else {
			len = dev->rx_buf_len;
			buf = dev->rx_buf;
		}

		rx_valid = dw_readl(dev, DW_IC_RXFLR);

		for (; len > 0 && rx_valid > 0; len--, rx_valid--)
		{
			*buf++ = dw_readl(dev, DW_IC_DATA_CMD);
			dev->rx_outstanding--;
		}

		if (len > 0) {
			dev->status |= STATUS_READ_IN_PROGRESS;
			dev->rx_buf_len = len;
			dev->rx_buf = buf;
			return;
		} else
			dev->status &= ~STATUS_READ_IN_PROGRESS;
	}
}

#define FOR_EACH_SET_BIT(bit, addr, size) \
    for ((bit) = FIND_FIRST_BIT((addr), (size)); \
            (bit) < (size); \
            (bit) = FIND_NEXT_BIT((addr), (size), (bit) + 1))

static int i2c_dw_handle_tx_abort(struct dw_i2c_dev *dev)
{
#ifdef CONFIG_I2C_DEBUG
	unsigned long abort_source = dev->abort_source;
	int i;

	if (abort_source & DW_IC_TX_ABRT_NOACK) {
		FOR_EACH_SET_BIT(i, &abort_source, ARRAY_SIZE(abort_sources))
			I2C_DBG("%s: %s\n", __func__, abort_sources[i]);
		return -EREMOTEIO;
	}

	FOR_EACH_SET_BIT(i, &abort_source, ARRAY_SIZE(abort_sources))
		I2C_DBG("%s: %s\n", __func__, abort_sources[i]);

	if (abort_source & DW_IC_TX_ARB_LOST)
		return -EAGAIN;
	else if (abort_source & DW_IC_TX_ABRT_GCALL_READ)
		return -EINVAL; /* wrong msgs[] data */
	else
		return -EIO;
#else
	return -EIO;
#endif
}

static u32 i2c_dw_read_clear_intrbits(struct dw_i2c_dev *dev)
{
	u32 stat;

	/*
	 * The IC_INTR_STAT register just indicates "enabled" interrupts.
	 * Ths unmasked raw version of interrupt status bits are available
	 * in the IC_RAW_INTR_STAT register.
	 *
	 * That is,
	 *   stat = readl(IC_INTR_STAT);
	 * equals to,
	 *   stat = readl(IC_RAW_INTR_STAT) & readl(IC_INTR_MASK);
	 *
	 * The raw version might be useful for debugging purposes.
	 */
	stat = dw_readl(dev, DW_IC_INTR_STAT);

	/*
	 * Do not use the IC_CLR_INTR register to clear interrupts, or
	 * you'll miss some interrupts, triggered during the period from
	 * readl(IC_INTR_STAT) to readl(IC_CLR_INTR).
	 *
	 * Instead, use the separately-prepared IC_CLR_* registers.
	 */
	if (stat & DW_IC_INTR_RX_UNDER)
		dw_readl(dev, DW_IC_CLR_RX_UNDER);
	if (stat & DW_IC_INTR_RX_OVER)
		dw_readl(dev, DW_IC_CLR_RX_OVER);
	if (stat & DW_IC_INTR_TX_OVER)
		dw_readl(dev, DW_IC_CLR_TX_OVER);
	if (stat & DW_IC_INTR_RD_REQ)
		dw_readl(dev, DW_IC_CLR_RD_REQ);
	if (stat & DW_IC_INTR_TX_ABRT) {
		/*
		 * The IC_TX_ABRT_SOURCE register is cleared whenever
		 * the IC_CLR_TX_ABRT is read.  Preserve it beforehand.
		 */
		dev->abort_source = dw_readl(dev, DW_IC_TX_ABRT_SOURCE);
		dw_readl(dev, DW_IC_CLR_TX_ABRT);
	}
	if (stat & DW_IC_INTR_RX_DONE)
		dw_readl(dev, DW_IC_CLR_RX_DONE);
	if (stat & DW_IC_INTR_ACTIVITY)
		dw_readl(dev, DW_IC_CLR_ACTIVITY);
	if (stat & DW_IC_INTR_STOP_DET)
		dw_readl(dev, DW_IC_CLR_STOP_DET);
	if (stat & DW_IC_INTR_START_DET)
		dw_readl(dev, DW_IC_CLR_START_DET);
	if (stat & DW_IC_INTR_GEN_CALL)
		dw_readl(dev, DW_IC_CLR_GEN_CALL);

	return stat;
}
/*
 * Prepare controller for a transaction and call i2c_dw_xfer_msg
 */
static int i2c_dw_xfer(struct dw_i2c_dev *device, struct i2c_msg msgs[], int num)
{
	struct dw_i2c_dev *dev = (struct dw_i2c_dev *)device;
	int ret;

	I2C_DBG("%s: msgs: %d\n", __func__, num);

	dev->msgs = msgs;
	dev->msgs_num = num;
	dev->cmd_err = 0;
	dev->msg_write_idx = 0;
	dev->msg_read_idx = 0;
	dev->msg_err = 0;
	dev->status = STATUS_IDLE;
	dev->rx_outstanding = 0;
	dev->abort_source = 0;

	ret = i2c_dw_wait_bus_not_busy(dev);
	if (ret < 0)
		goto done;

	/* start the transfers */
	i2c_dw_xfer_init(dev);

	int try_times = I2C_WAIT_TIMES;
	u32 stat;
	while (try_times--) {
		stat = i2c_dw_read_clear_intrbits(dev);

		if (stat & DW_IC_INTR_TX_ABRT) {
			dev->cmd_err |= DW_IC_ERR_TX_ABRT;
			dev->status = STATUS_IDLE;
			/*
			 * Anytime TX_ABRT is set, the contents of the tx/rx
			 * buffers are flushed.  Make sure to skip them.
			 */
			dw_writel(dev, 0, DW_IC_INTR_MASK);
			goto tx_aborted;
		}

		if (stat & DW_IC_INTR_RX_FULL) {
			i2c_dw_read(dev);
			try_times = I2C_WAIT_TIMES;
		}

		if (stat & DW_IC_INTR_TX_EMPTY) {
			i2c_dw_xfer_msg(dev);
			try_times = I2C_WAIT_TIMES;
		}
tx_aborted:
		if ((stat & (DW_IC_INTR_TX_ABRT | DW_IC_INTR_STOP_DET)) || dev->msg_err)
			break;
	}

	if (try_times < 0) {
		I2C_DBG("dw_i2c timeout\n");
		i2c_dw_init(dev);
		ret = -ETIMEDOUT;
		goto done;
	}

	if (dev->msg_err) {
		ret = dev->msg_err;
		goto done;
	}

	/* no error */
	if (likely(!dev->cmd_err)) {
		/* Disable the adapter */
		dw_writel(dev, 0, DW_IC_ENABLE);
		//change to cooperate the eeprom code in gxloader
		ret = 0;
		goto done;
	}

	/* We have an error */
	if (dev->cmd_err == DW_IC_ERR_TX_ABRT) {
		ret = i2c_dw_handle_tx_abort(dev);
		goto done;
	}
	ret = -EIO;

done:
	return ret;
}

static int i2c_dw_irq_handler_slave(struct dw_i2c_dev *dev);
static int i2c_interrupt_isr(int vector, void* pdata)
{
	gx_mask_irq(vector);

	if (i2c_dw_irq_handler_slave(pdata) == I2C_ERR) {
		printf("i2c slave transaction error\n");
	}

	gx_unmask_irq(vector);
	return 0;
}

static int dw_i2c_probe(struct dw_i2c_dev *device, enum i2c_mode mode)
{
	struct dw_i2c_dev *dev = device;
	int r;
	u32 reg;

	if (mode == I2C_MASTER) {
		r = i2c_dw_init(dev);
		dw_writel(dev, 0, DW_IC_INTR_MASK); /* disable IRQ */
	} else {
		r = i2c_dw_init_slave(dev);
		gx_request_irq(dev->irq, i2c_interrupt_isr, dev);
	}

	if (r)
		return r;

	reg = dw_readl(dev, DW_IC_COMP_PARAM_1);
	dev->tx_fifo_depth = ((reg >> 16) & 0xff) + 1;
	dev->rx_fifo_depth = ((reg >> 8)  & 0xff) + 1;

	return 0;
}

static int dw_i2c_remove(void *device)
{
	struct dw_i2c_dev *dev = (struct dw_i2c_dev *)device;

	dw_writel(dev, 0, DW_IC_ENABLE);
	return 0;
}

#ifdef CONFIG_PM
static int dw_i2c_suspend(struct dw_i2c_dev *device, pm_message_t state)
{
	struct dw_i2c_dev *dev = device;
	dw_writel(dev, 0, DW_IC_ENABLE);
	/* Disable all interupts */
	dw_writel(dev, 0, DW_IC_INTR_MASK);
	dw_readl(dev, DW_IC_CLR_INTR);
	return 0;
}
static int dw_i2c_resume(struct dw_i2c_dev *device)
{
	struct dw_i2c_dev *dev = device;
	i2c_dw_init(dev);
	return 0;
}
#else
#define dw_i2c_suspend NULL
#define dw_i2c_resume  NULL
#endif


int gx_i2c_transfer(void *dev, struct i2c_msg *msgs, int num)
{
	struct dw_i2c_dev *i2c = (struct dw_i2c_dev *)dev;
	i2c->retries  = 2;
	return i2c_dw_xfer(dev, msgs, num);
}

static struct dw_i2c_dev* gx_i2c_dev_register(int id, enum i2c_mode mode)
{
	struct dw_i2c_dev* i2c = NULL;
	struct dw_i2c_dev* last_node = g_i2c_list;

	for (i2c = &g_gx_i2c_device[0]; i2c != &g_gx_i2c_device[MAX_I2C_DEVICES - 1]; i2c++) {
		if (i2c->used == 0)
			break;
	}

	if (i2c == &g_gx_i2c_device[MAX_I2C_DEVICES - 1]) {
		if (i2c->used != 0) {
			printf("%s(): no free g_gx_i2c_device found\n", __func__);
			return NULL;
		}
	}

	/* init the device node */
	i2c->i2c_devid          = id;
	i2c->used               = 1;
	i2c->next               = NULL;

	if (mode == I2C_MASTER) {
		i2c->base               = g_i2c_info[id].regs;
		i2c->src_clk            = g_i2c_info[id].i2c_pclk;
		i2c->high_time          = g_i2c_info[id].hight_time;
		i2c->low_time           = g_i2c_info[id].low_time;
		i2c->scl_clk            = g_i2c_info[id].i2c_bus_freq;
		i2c->role               = I2C_MASTER;
	} else {
		i2c->base               = g_i2c_info[id].regs;
		i2c->src_clk            = g_i2c_info[id].i2c_pclk;
		i2c->chip_address       = g_i2c_info[id].chip_address;
		i2c->irq                = g_i2c_info[id].irq;
		i2c->rx_index           = 0;
		i2c->tx_index           = 0;
		i2c->slave_cb           = NULL;
		i2c->role               = I2C_SLAVE;
	}

	/* add this device node at list tail */
	if (!g_i2c_list) {

		g_i2c_list = i2c;
	} else {
		while (last_node->next)
			last_node = last_node->next;

		last_node->next = i2c;
	}

	return i2c;
}

static int dw_i2c_unregister(struct dw_i2c_dev *device)
{
	struct dw_i2c_dev *previous_i2c = g_i2c_list;
	struct dw_i2c_dev *current_i2c  = g_i2c_list;

	if (!device)
		return -1;

	while (current_i2c) {
		/* delete the device node if found */
		if (device->i2c_devid == current_i2c->i2c_devid) {

			if (previous_i2c==current_i2c) {

				/* first node */
				g_i2c_list = current_i2c->next;
			} else {

				previous_i2c->next = current_i2c->next;
			}

			device->i2c_devid     = 0;
			device->used          = 0;
			device->chip_address  = 0;
			device->address_width = 0;
			device->retries       = 0;
			device->slave_cb      = NULL;
			device->role          = I2C_NONE;

			return 0;
		}

		previous_i2c    = current_i2c;
		current_i2c     = current_i2c->next;
	}

	/* no the specified device found */
	return -2;
}

int gx_i2c_set_speed(unsigned int id, unsigned int speed)
{
	struct i2c_info *i2c = &g_i2c_info[id];
	unsigned int time = 1000000 / speed;  //scl一个周期的时间，单位us
	if (speed <= (STD_SPEED / 1000)){
		if (time < 4700){
			printf("Don't support the speed\n");
			return -1;
		}
	}else if (speed <= (FAST_SPEED / 1000)){
		if (time < 1300){
			printf("Don't support the speed\n");
			return -1;
		}
	}else
		printf("Don't support the speed\n");
	i2c->i2c_bus_freq = speed * 1000;
	i2c->hight_time   = time / 2;
	i2c->low_time     = time / 2;
	return 0 ;
}

void *gx_i2c_open(unsigned int id)
{
	struct dw_i2c_dev *i2c = g_i2c_list;

	/* find the i2c device node */
	while (i2c) {
		if (i2c->i2c_devid == id) {
			if (i2c->role == I2C_SLAVE) {
				if (gx_i2c_slave_close(i2c)) {
					printf("The device id is using by i2c slave, please change a id\n");
					return NULL;
				}
			}
			else
				return i2c;
		}
		i2c = i2c->next;
	}

	i2c = gx_i2c_dev_register(id, I2C_MASTER);
	if (i2c == NULL)
		return NULL;

	if ( 0 != dw_i2c_probe(i2c, I2C_MASTER)) {
		I2C_DBG("i2c probe faild.\n");
		dw_i2c_unregister(i2c);
		return NULL;
	}
	return i2c;
}

void *gx_i2c_slave_open(unsigned int id, unsigned int devid, i2c_slave_cb_t callback)
{
	struct dw_i2c_dev *i2c = g_i2c_list;

	/* find the i2c device node */
	while (i2c) {
		if (i2c->i2c_devid == id) {
			if (i2c->role == I2C_MASTER) {
				if (gx_i2c_close(i2c)) {
					printf("The device id is using by i2c master, please change a id\n");
					return NULL;
				}
			}
			else
				return i2c;
		}
		i2c = i2c->next;
	}

	g_i2c_info[id].chip_address = devid;
	i2c = gx_i2c_dev_register(id, I2C_SLAVE);
	if (i2c == NULL)
		return NULL;

	if (callback)
		i2c->slave_cb = callback;
	else
		i2c->slave_cb = i2c_slave_callback;

	if ( 0 != dw_i2c_probe(i2c, I2C_SLAVE)) {
		I2C_DBG("i2c slave probe faild.\n");
		dw_i2c_unregister(i2c);
		return NULL;
	}

	return i2c;
}

int gx_i2c_close(void *dev)
{
	dw_i2c_unregister((struct dw_i2c_dev *)dev);
	dw_i2c_remove(dev);
	return 0;
}

int gx_i2c_slave_close(void *dev)
{
	u32 state;
	u32 times = MAX_POLL_COUNT;
	struct dw_i2c_dev *device = dev;

	state = dw_readl(dev, DW_IC_STATUS);
	if (!(state & SLV_ACTIVITY)) {
		gx_free_irq(device->irq);
		dw_writel(device, 0, DW_IC_ENABLE);
		if ((dw_readl(device, DW_IC_ENABLE_STATUS) & 1) == 0) {
			dw_i2c_unregister((struct dw_i2c_dev *)dev);
			return 0;
		}
		else {
			goto err;
		}
	}
	slave_close_flag = SLV_NEED_CLOSE;

	do {
		udelay(25);
		if (slave_close_flag == SLV_CLOSE_OK) {
			if ((dw_readl(device, DW_IC_ENABLE_STATUS) & 1) == 0) {
				dw_i2c_unregister((struct dw_i2c_dev *)dev);
				return 0;
			}
		}
	} while(times--);

	slave_close_flag = SLV_NOT_CLOSE;
	if ((dw_readl(device, DW_IC_ENABLE_STATUS) & 1) == 0) {
		dw_i2c_unregister((struct dw_i2c_dev *)dev);
		return 0;
	}

err:
	dw_writel(device, 1, DW_IC_ENABLE);
	gx_request_irq(device->irq, i2c_interrupt_isr, device);
	printf("warning: i2c slave is busy and can not close\n");
	return -1;
}

static int gx_i2c_transaction(void *dev, uint32_t reg_address, uint8_t *rx_data, uint32_t count, int flag)
{
	int ret = 0;
	uint8_t buf[4] = {0};
	struct i2c_msg msgs[2];
	struct dw_i2c_dev *i2c = (struct dw_i2c_dev *)dev;

	if (!i2c || !rx_data || !count)
		return -1;

	switch (i2c->address_width)
	{
	case 0:
		break;
	case 1:
		buf[0] = (uint8_t)(reg_address);
		break;

	case 2:
		buf[0] = (uint8_t)(reg_address >> 8);
		buf[1] = (uint8_t)(reg_address);
		break;

	case 3:
		buf[0] = (uint8_t)(reg_address >> 16);
		buf[1] = (uint8_t)(reg_address >> 8);
		buf[2] = (uint8_t)(reg_address);
		break;

	case 4:
		buf[0] = (uint8_t)(reg_address >> 24);
		buf[1] = (uint8_t)(reg_address >> 16);
		buf[2] = (uint8_t)(reg_address >> 8);
		buf[3] = (uint8_t)(reg_address);
		break;

		/* invalid address width */
	default:
		return -2;
	}

	/* Send chip address. */
	msgs[0].len  = i2c->address_width;
	msgs[0].addr = i2c->chip_address;
	msgs[0].buf  = &buf[0];
	msgs[0].flags = 0;

	/* data. */
	msgs[1].len  = count;
	msgs[1].addr = i2c->chip_address;
	msgs[1].buf  = (uint8_t *)rx_data;
	msgs[1].flags = 0;

	if (flag == 1){
		msgs[0].flags = I2C_M_REV_DIR_ADDR;
		msgs[1].flags = I2C_M_RD | I2C_M_NO_RD_ACK;
	}

	ret = gx_i2c_transfer(i2c, msgs, 2);

	return ret != 0 ? -1 : 0;
}

int gx_i2c_tx(int bus_id, unsigned int devid, unsigned int reg_address, unsigned char *tx_data, unsigned int count)
{
	ssize_t status;
	struct dw_i2c_dev *i2c_dev = NULL;

	i2c_dev = gx_i2c_open(bus_id);
	if (!i2c_dev) {
		printf("open i2c dev failed.\n");
		return -1;
	}

	i2c_dev->address_width = 1;
	i2c_dev->chip_address = devid;
	status = gx_i2c_transaction(i2c_dev, reg_address, tx_data, count, 0);

	gx_i2c_close(i2c_dev);
	return status;
}

int gx_i2c_rx(int bus_id, unsigned int devid, unsigned int reg_address, unsigned char *rx_data, unsigned int count)
{
	ssize_t status;
	struct dw_i2c_dev *i2c_dev = NULL;

	i2c_dev = gx_i2c_open(bus_id);
	if (!i2c_dev) {
		printf("open i2c dev failed.\n");
		return -1;
	}

	i2c_dev->address_width = 1;
	i2c_dev->chip_address = devid;
	status = gx_i2c_transaction(i2c_dev, reg_address, rx_data, count, 1);

	gx_i2c_close(i2c_dev);
	return status;
}

static int i2c_dw_irq_handler_slave(struct dw_i2c_dev *dev)
{
	u32 stat;
	u8 val;
	struct i2c_slave_callback_param cb_param;

	stat = dw_readl(dev, DW_IC_INTR_STAT);

	if (stat & DW_IC_INTR_RD_REQ) {
		cb_param.rx_index = dev->rx_index;
		cb_param.tx_index = dev->tx_index;
		cb_param.event = I2C_SLAVE_REQUESTED_DATA;

		if (!dev->slave_cb(&cb_param)) {
			val = cb_param.data;
			dw_writel(dev, val, DW_IC_DATA_CMD);
			dev->tx_index++;
		} else {
			printf("can not get data from callback\n");
			return I2C_ERR;
		}
		dw_readl(dev, DW_IC_CLR_RD_REQ);
	}

	if (stat & DW_IC_INTR_RX_FULL) {
		val = dw_readl(dev, DW_IC_DATA_CMD);
		cb_param.data = val;
		cb_param.rx_index = dev->rx_index;
		cb_param.tx_index = dev->tx_index;
		cb_param.event = I2C_SLAVE_RECEIVE_DATA;
		dev->slave_cb(&cb_param);
		dev->rx_index++;
	}

	if (!(stat & DW_IC_INTR_RX_FULL) && (stat & DW_IC_INTR_STOP_DET)) {
		cb_param.rx_index = dev->rx_index;
		cb_param.tx_index = dev->tx_index;
		cb_param.event = I2C_SLAVE_STOP;
		dev->slave_cb(&cb_param);
		dev->rx_index = 0;
		dev->tx_index = 0;
		dw_readl(dev, DW_IC_CLR_STOP_DET);
		if (slave_close_flag == SLV_NEED_CLOSE) {
			gx_free_irq(dev->irq);
			dw_writel(dev, 0, DW_IC_ENABLE);
			if ((dw_readl(dev, DW_IC_ENABLE_STATUS) & 1) == 0)
				slave_close_flag = SLV_CLOSE_OK;
		}
	}

	if (stat & DW_IC_INTR_TX_ABRT) {
		dev->abort_source = dw_readl(dev, DW_IC_TX_ABRT_SOURCE);
		dw_readl(dev, DW_IC_CLR_TX_ABRT);
		i2c_dw_handle_tx_abort(dev);
		printf("i2c tx_abrt\n");
		return I2C_ERR;
	}

	return I2C_OK;
}
