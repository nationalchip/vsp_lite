/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * padmux.c: pad multiplexing configure
 *
 */

#include <common.h>
#include <misc_regs.h>
#include <driver/padmux.h>

#define PMUX_INV 0xff

#define REG_READ_BIT(reg, bit) ((unsigned int)((readl(reg) >> (bit)) & 1))
#define REG_WRITE_BIT(reg, bit, value)  \
    do {                                \
        unsigned int tmp = readl(reg);  \
        tmp &= ~(1UL << (bit));         \
        tmp |= ((value) << (bit));      \
        writel(tmp, reg);               \
    } while(0)

struct padmux_config {
    unsigned char pad_id;
    unsigned char sel0;
    unsigned char sel1;
    unsigned char sel2;
    unsigned char function;
};

/* DON'T EDIT! Default setup all pad to GPIO */
static const struct padmux_config _padmux_table[] = {
/*   id | bit0 | bit1   | bit2   func    function0    | function1   | function2   | function3  | function4 */
    { 1,  1,  PMUX_INV, PMUX_INV, 1},  // UART1RX     | PD1PORT01
    { 2,  2,  PMUX_INV, PMUX_INV, 1},  // UART0RX     | PD1PORT02
    { 3,  4,  PMUX_INV, PMUX_INV, 1},  // UART0TX     | PD1PORT03
    { 4,  5,        75, PMUX_INV, 2},  // UART1TX     | OTP_AVDD_EN | PD1PORT04
    { 5,  3,        64,       96, 4},  // SDBGTDI     | DDBGTDI     | UART0_RTS   | SDA0       | PD1PORT05
    { 6,  6,        65,       97, 4},  // SDBGTDO     | DDBGTDO     | UART0_CTS   | SCL0       | PD1PORT06
    { 7,  7,        66,       98, 4},  // SDBGTMS     | DDBGTMS     | SDA1        | PCM1INBCLK | PD1PORT07
    { 8,  8,        67,       99, 4},  // SDBGTCK     | DDBGTCK     | SDA1        | PCM1INLRCK | PD1PORT08
    { 9,  9,        68,      100, 4},  // SDBGTRST    | DDBGTRST    | SCL1        | PCM1INDAT0 | PD1PORT09
    {10, 13,        69,      101, 4},  // PCM0OUTMCLK | DUARTTX     | SDA0        | SPI1SCK    | PD1PORT14
    {11, 14,        70, PMUX_INV, 3},  // PCM0OUTAT0  | SPI1MOSI    | SCL0        | PD1PORT15
    {12, 15,        71, PMUX_INV, 2},  // PCM0OUTLRCK | SPI1CSn     | PD1PORT16
    {13, 16,        72, PMUX_INV, 2},  // PCM0OUTBCLK | SPI1MISO    | PD1PORT17
    {14, 27,        76, PMUX_INV, 2},  // SPI0WP      | SDA1        | PD1PORT28
    {15, 28,        77, PMUX_INV, 2},  // SPI0HOLD    | SCL1        | PD1PORT29
    {16, 30,        78, PMUX_INV, 2},  // PCM0INDAT0  | IR          | PD1PORT31
    {17, 31,        79, PMUX_INV, 2},  // PCM0INMCLK  | PDMDAT1     | PD1PORT32
    {18, 32,        80, PMUX_INV, 3},  // PCM0INLRCK  | PDMDAT0     | PCM0OUTLRCK | PD1PORT33
    {19, 33,        81, PMUX_INV, 3},  // PCM0INBCLK  | PDMCLK      | PCM0OUTBCLK | PD1PORT34

};

static void _PadMuxWriteBit(int bit, int value)
{
    if (bit < 32) {
        REG_WRITE_BIT(MISC_REG_PIN_FUNCTION_SEL_0, bit, value);
    } else if (bit < 64) {
        REG_WRITE_BIT(MISC_REG_PIN_FUNCTION_SEL_1, bit - 32, value);
    } else if (bit < 96) {
        REG_WRITE_BIT(MISC_REG_PIN_FUNCTION_SEL_2, bit - 64, value);
    } else if (bit < 128) {
        REG_WRITE_BIT(MISC_REG_PIN_FUNCTION_SEL_3, bit - 96, value);
    }
}

static unsigned char _PadMuxReadBit(int bit)
{
    unsigned char b = 0;

    if (bit < 32){
        b = REG_READ_BIT(MISC_REG_PIN_FUNCTION_SEL_0, bit);
    } else if (bit < 64){
        b = REG_READ_BIT(MISC_REG_PIN_FUNCTION_SEL_1, bit - 32);
    } else if (bit < 96){
        b = REG_READ_BIT(MISC_REG_PIN_FUNCTION_SEL_2, bit - 64);
    } else if (bit < 128){
        b = REG_READ_BIT(MISC_REG_PIN_FUNCTION_SEL_3, bit - 96);
    }

    return b;
}

//=================================================================================================

static int _PadMuxGetUserFunc(int pad_id,
                              const PADMUX_PAD_CONFIG *pad_table,
                              int table_size,
                              unsigned char *pad_function)
{
    for (int i = 0; i < table_size; i++) {
        if (pad_id == pad_table[i].pad_id) {
            *pad_function = pad_table[i].pad_function;
            return 0;
        }
    }
    return -1;
}

int PadMuxInit(const PADMUX_PAD_CONFIG *config_table, int table_size)
{
    if (!config_table || table_size < 0)
        return -1;

    for (int i = 0; i < ARRAY_SIZE(_padmux_table); i++) {
        /* Check user config, overwrite defconfig if different */
        unsigned char pad_function = 0;
        if (_PadMuxGetUserFunc(_padmux_table[i].pad_id, config_table, table_size, &pad_function))
            PadMuxSetFunction(_padmux_table[i].pad_id, _padmux_table[i].function);
        else
            PadMuxSetFunction(_padmux_table[i].pad_id, pad_function);
    }
    return 0;
}

//=================================================================================================

int PadMuxGetFunction(int pad_id)
{
    if (pad_id < 0)
        return -1;

    for (int i = 0; i < ARRAY_SIZE(_padmux_table); i++) {
        if (_padmux_table[i].pad_id != pad_id)
            continue;

        unsigned char bit0 = _PadMuxReadBit(_padmux_table[i].sel0);
        unsigned char bit1 = _PadMuxReadBit(_padmux_table[i].sel1);
        unsigned char bit2 = _PadMuxReadBit(_padmux_table[i].sel2);

        if((_padmux_table[i].sel0 == PMUX_INV) && (bit1 == 1))
            bit0 = 1;

        if(_padmux_table[i].sel1 == PMUX_INV)
            bit1 = 0;

        if(_padmux_table[i].sel2 == PMUX_INV)
            bit2 = 0;

        return (bit2 << 2) + (bit1 << 1) + bit0;
    }

    return -1;
}

int PadMuxSetFunction(int pad_id, int function)
{
    if (pad_id < 0 || function < 0)
        return -1;

    unsigned char bit0 = function & 0x1;
    unsigned char bit1 = (function & 0x2) >> 1;
    unsigned char bit2 = (function & 0x4) >> 2;

    for (int i = 0; i < ARRAY_SIZE(_padmux_table); i++) {
        if (_padmux_table[i].pad_id != pad_id)
            continue;

        _PadMuxWriteBit(_padmux_table[i].sel0, bit0);
        _PadMuxWriteBit(_padmux_table[i].sel1, bit1);
        _PadMuxWriteBit(_padmux_table[i].sel2, bit2);

        return PadMuxGetFunction(pad_id) == function;
    }
    return -1;
}

#ifdef CONFIG_MCU_ENABLE_DEBUG

void PadMuxDumpRegs(void)
{
    printf("PIN_FUNCTION_SEL_0=0x%x.\n", *(volatile unsigned int*)MISC_REG_PIN_FUNCTION_SEL_0);
    printf("PIN_FUNCTION_SEL_1=0x%x.\n", *(volatile unsigned int*)MISC_REG_PIN_FUNCTION_SEL_1);
    printf("PIN_FUNCTION_SEL_2=0x%x.\n", *(volatile unsigned int*)MISC_REG_PIN_FUNCTION_SEL_2);
    printf("PIN_FUNCTION_SEL_3=0x%x.\n", *(volatile unsigned int*)MISC_REG_PIN_FUNCTION_SEL_3);
}

#endif
