#include <common.h>
#include <base_addr.h>
#include <cpu_regs.h>
#include <driver/multicore_lock.h>

#define MULTICORE_UNLOCK_ALL 0

static int initialized = 0;

int MultiCoreTryLock(MULTICORE_LOCK lock)
{
    unsigned int tmp = 0;

    if (!initialized) {
        writel(MULTICORE_UNLOCK_ALL, MULTICORE_LOCK_MCU_LOCK);
        initialized = 1;
    }

    tmp = readl(MULTICORE_LOCK_MCU_LOCK) | lock;
    writel(tmp, MULTICORE_LOCK_MCU_LOCK);

    if (readl(MULTICORE_LOCK_CPU_LOCK) & lock) {
        tmp = readl(MULTICORE_LOCK_MCU_LOCK) & ~lock;
        writel(tmp, MULTICORE_LOCK_MCU_LOCK);
        return -1;
    }

    return 0;
}

int MultiCoreLock(MULTICORE_LOCK lock)
{
    unsigned int tmp = 0;

    if (!initialized) {
        writel(MULTICORE_UNLOCK_ALL, MULTICORE_LOCK_MCU_LOCK);
        initialized = 1;
    }

    tmp = readl(MULTICORE_LOCK_MCU_LOCK) | lock;
    writel(tmp, MULTICORE_LOCK_MCU_LOCK);

    while (readl(MULTICORE_LOCK_CPU_LOCK) & lock) {
        tmp = readl(MULTICORE_LOCK_MCU_LOCK) & ~lock;
        writel(tmp, MULTICORE_LOCK_MCU_LOCK);

        tmp = readl(MULTICORE_LOCK_MCU_LOCK) | lock;
        writel(tmp, MULTICORE_LOCK_MCU_LOCK);
    }

    return 0;
}

int MultiCoreUnlock(MULTICORE_LOCK lock)
{
    unsigned int tmp = 0;

    tmp = readl(MULTICORE_LOCK_MCU_LOCK) & ~lock;
    writel(tmp, MULTICORE_LOCK_MCU_LOCK);

    return 0;
}
