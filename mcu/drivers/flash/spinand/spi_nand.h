/* Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * spi_nand.h: SPI NAND driver header
 *
 */

#ifndef __SPI_NAND_H__
#define __SPI_NAND_H__
#include <common.h>

#define assert_param(_param, _rtn) do{                                         \
	if(!(_param)){                                                             \
		printf("ERROR: [%s] - %d...\n",__func__, __LINE__);                    \
		return _rtn;                                                           \
	}                                                                          \
}while(0)

#define MTD_MAX_OOBFREE_ENTRIES_LARGE   5
#define MTD_MAX_ECCPOS_ENTRIES_LARGE    48

#define CMD_READ                   0x13
#define CMD_READ_RDM               0x03
#define CMD_PROG_PAGE_CLRCACHE     0x02
#define CMD_PROG_PAGE              0x84
#define CMD_PROG_PAGE_EXC          0x10
#define CMD_ERASE_BLK              0xd8
#define CMD_WR_ENABLE              0x06
#define CMD_WR_DISABLE             0x04
#define CMD_READ_ID                0x9f
#define CMD_RESET                  0xff
#define CMD_READ_REG               0x0f
#define CMD_WRITE_REG              0x1f

/* feature/ status reg */
#define REG_BLOCK_LOCK             0xa0
#define REG_OTP                    0xb0
#define REG_STATUS                 0xc0

/* status */
#define STATUS_OIP_MASK            (0x01  )
#define STATUS_READY               (0 << 0)
#define STATUS_BUSY                (1 << 0)
#define STATUS_E_FAIL_MASK         (0x04  )
#define STATUS_E_FAIL              (1 << 2)
#define STATUS_P_FAIL_MASK         (0x08  )
#define STATUS_P_FAIL              (1 << 3)
#define STATUS_COMMON_ECC_MASK     (0x30  )
#define STATUS_FORESEE_ECC_MASK    (0x70  )
#define STATUS_COMMON_ECC_ERROR    (2 << 4)
#define STATUS_FORESEE_ECC_ERROR   (7 << 4)
#define STATUS_ECC_1BIT_CORRECTED  (1 << 4)
#define STATUS_ECC_RESERVED        (3 << 4)
#define	SFLASH_READY_TIMEOUT       200000         //0.2s

/*ECC enable defines*/
#define REG_ECC_MASK               0x10
#define REG_ECC_OFF                0x00
#define REG_ECC_ON                 0x01
#define ECC_DISABLED
#define ECC_IN_NAND
#define ECC_SOFT

/*BUF enable defines*/
#define REG_BUF_MASK               0x08
#define REG_BUF_OFF                0x00
#define REG_BUF_ON                 0x01

/* block lock */
#define BL_ALL_LOCKED              0x38
#define BL_1_2_LOCKED              0x30
#define BL_1_4_LOCKED              0x28
#define BL_1_8_LOCKED              0x20
#define BL_1_16_LOCKED             0x18
#define BL_1_32_LOCKED             0x10
#define BL_1_64_LOCKED             0x08
#define BL_ALL_UNLOCKED            0x00

typedef struct{
	uint8_t  cmd_len;
	uint8_t  cmd[5];
	uint32_t xfer_len;
	uint8_t  *tx;
	uint8_t  *rx;
} nand_cmd_t;

struct mtd_oob_ops {
	uint32_t  mode;
	size_t    len;
	size_t    retlen;
	size_t    ooblen;
	size_t    oobretlen;
	uint32_t  ooboffs;
	uint8_t  *datbuf;
	uint8_t  *oobbuf;
};

struct spi_nand_info{
	uint32_t    nand_id;
	const char *name;
	uint32_t    nand_size;
	uint32_t    usable_size;
	uint32_t    block_size;
	uint32_t    block_main_size;
	uint32_t    block_num_per_chip;
	uint16_t    page_size;
	uint16_t    page_main_size;
	uint16_t    page_spare_size;
	uint16_t    page_num_per_block;
	uint16_t    block_shift;
	uint16_t    page_shift;
	uint32_t    block_mask;
	uint32_t    page_mask;
	uint32_t    bbt_size;
	uint8_t    *bbt;
	struct nand_ecclayout *ecclayout;
};

struct nand_id_index{
	uint32_t id;
	const char *name;
	struct spi_nand_info *info;
};

struct spi_nand_priv {
	uint32_t              id;
	struct udevice       *dev;
	struct spi_slave     *slave;
	struct spi_nand_info *info;
	struct spi_device    *spi_device;
};

#endif
