/*****************************************
  Copyright (c) 2003-2019
  Nationalchip Science & Technology Co., Ltd. All Rights Reserved
  Proprietary and Confidential
 *****************************************/

#include <common.h>
#include <driver/delay.h>
#include <string.h>
#include <errno.h>
#include <driver/spi_device.h>
#include <driver/flash.h>
#include "spi_nand.h"


#define PAGES_PER_BLOCK            64
// #define SPINAND_DBG
#ifdef  SPINAND_DBG
#define SND_DBG_PHY    1
#define snd_dbg(_fg, _fmt,args...) do{ if(_fg) printf(_fmt,##args); }while(0)
#else
#define snd_dbg(_fg, _fmt,args...)
#endif

#define snd_err(_fmt,args...) do{\
	printf("ERROR: %s - %d ", __func__, __LINE__);\
	printf(_fmt, ##args);\
}while(0)

static struct spi_nand_info nand_info[] = {
	/**
	 *  0x112c 0x122c 0x132c 0xc8f1 0xf1c8 0xc8d1
	 *  0xd1c8 0xaaef 0x21C8 0xc298 0x12c2 0xe1a1
	 */
	[0] = {
		.nand_size          = 1024 * 64 * 2112,
		.usable_size        = 1024 * 64 * 2048,
		.block_size         = 2112*64,
		.block_main_size    = 2048*64,
		.block_num_per_chip = 1024,
		.page_size          = 2112,
		.page_main_size     = 2048,
		.page_spare_size    = 64,
		.page_num_per_block = 64,
		.block_shift        = 17,
		.block_mask         = 0x1ffff,
		.page_shift         = 11,
		.page_mask          = 0x7ff,
		.ecclayout          = NULL,
	},
	[1] = { // 0xc8f2
		.nand_size          = (2048 * 64 * 2112),
		.usable_size        = (2048 * 64 * 2048),
		.block_size         = (2112*64),
		.block_main_size    = (2048*64),
		.block_num_per_chip = 2048,
		.page_size          = 2112,
		.page_main_size     = 2048,
		.page_spare_size    = 64,
		.page_num_per_block = 64,
		.block_shift        = 17,
		.block_mask         = 0x1ffff,
		.page_shift         = 11,
		.page_mask          = 0x7ff,
		.ecclayout          = NULL,
	},
};

static struct nand_id_index id_table[] = {
	/* GD spi nand flash */
	{.id = 0xF1C8, .info = nand_info,   .name = "GD5F1GQ4UAYIG"   },
	{.id = 0xD1C8, .info = nand_info,   .name = "GD5F1GQ4UB"      },
	{.id = 0xC1C8, .info = nand_info,   .name = "GD5F1GQ4RB"      },
	{.id = 0xD2C8, .info = nand_info+1, .name = "GD5F2GQ4UB"      },
	{.id = 0xC2C8, .info = nand_info+1, .name = "GD5F2GQ4RB"      },
	{.id = 0xF2C8, .info = nand_info+1, .name = "GD5F2GQ4RAYIG"   },
	{.id = 0x51C8, .info = nand_info,   .name = "GD5F1GQ5UEYIG"   },
	{.id = 0x52C8, .info = nand_info+1, .name = "GD5F2GQ5UEYIG"   },
	/* TOSHIBA spi nand flash */
	{.id = 0xc298, .info = nand_info,   .name = "TC58CVG053HRA1G" },
	/* Micron spi nand flash */
	{.id = 0x122C, .info = nand_info,   .name = "MT29F1G01ZAC"    },
	{.id = 0x112C, .info = nand_info,   .name = "MT29F1G01ZAC"    },
	{.id = 0x132C, .info = nand_info,   .name = "MT29F1G01ZAC"    },
	/* 芯天下 spi nand flash */
	{.id = 0xe1a1, .info = nand_info,   .name = "PN26G01AWS1UG"   },
	{.id = 0xe10b, .info = nand_info,   .name = "XT26G01AWS1UG"   },
	{.id = 0xe20b, .info = nand_info+1, .name = "XT26G02AWSEGA"   },
	/* Zetta Confidentia spi nand flash */
	{.id = 0x71ba, .info = nand_info,   .name = "ZD35X1GA"        },
	{.id = 0x21ba, .info = nand_info,   .name = "ZD35X1GA"        },
	/* ESMT spi nand flash */
	{.id = 0x21C8, .info = nand_info,   .name = "F50L1G41A "      },
	{.id = 0x01C8, .info = nand_info,   .name = "F50L1G41LB"      },
	/* winbond spi nand flash */
	{.id = 0x21AAEF, .info = nand_info,   .name = "W25N01GV"      },
	{.id = 0x22AAEF, .info = nand_info+1, .name = "W25N02KV"      },
	/* Mxic spi nand flash */
	{.id = 0x12c2, .info = nand_info,   .name = "MX35LF1GE4AB"    },
	/* foresee spi nand flash */
	{.id = 0xa1cd, .info = nand_info,   .name = "FS35ND01G-D1F1"},
	{.id = 0xb1cd, .info = nand_info,   .name = "FS35ND01G-S1F1"},
	{.id = 0xeacd, .info = nand_info,   .name = "FS35ND01G-S1Y2"},
	/* EtronTech spi nand flash */
	{.id = 0x1cd5, .info = nand_info,   .name = "EM73C044VCD"     },
	{.id = 0x1fd5, .info = nand_info+1, .name = "EM73D044VCG"     },
	/* dosilicon spi nand flash */
	{.id = 0x71e5, .info = nand_info,   .name = "DS35Q1GA"        },
	{.id = 0x72e5, .info = nand_info+1, .name = "DS35Q2GA"        },
	/* HeYangTek spi nand flash */
	{.id = 0x21c9, .info = nand_info,   .name = "HYF1GQ4UDACAE"   },
	{.id = 0x5ac9, .info = nand_info,   .name = "HYF2GQ4UHCCAE"   },
	{.id = 0x52c9, .info = nand_info,   .name = "HYF2GQ4UAACAE"   },
	/* Fudan Microelectronics */
	{.id = 0xa1a1, .info = nand_info,   .name = "FM25S01"         },
	{.id = 0xe4a1, .info = nand_info,   .name = "FM25S01A"        },
	{.id = 0xf2a1, .info = nand_info+1, .name = "FM25G02"         },
	{.id = ~0,     .info = NULL,        .name = "NULL"            }
};

static struct spi_nand_priv *priv = NULL;
// bad block table buff
static unsigned char bbt_buff[512];

#define snand_reset() do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len = 1,\
		.cmd     = { CMD_RESET },\
		.xfer_len = 0,\
	});\
}while(0)

#define snand_read_page_to_cache(_page_id) do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 4,\
		.cmd      = {\
			CMD_READ,\
			0xFF & (_page_id >> 16),\
			0xFF & (_page_id >> 8 ),\
			0xFF & (_page_id >> 0 ),\
		},\
		.xfer_len = 0,\
	});\
}while(0)

#define snand_read_from_cache(_id, _len, _rbuf) do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 4,\
		.cmd      = {\
			CMD_READ_RDM,\
			0xFF & (_id >> 8),\
			0xFF & (_id >> 0),\
			~0, 0,\
		},\
		.xfer_len = _len,\
		.rx       = _rbuf,\
	});\
}while(0)

#define snand_write_enable() do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 1,\
		.cmd      = { CMD_WR_ENABLE },\
		.xfer_len = 0,\
	});\
}while(0)

#define snand_write_disable() do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 1,\
		.cmd      = { CMD_WR_DISABLE },\
		.xfer_len = 0,\
	});\
}while(0)

#define snand_erase_block_erase(_blkid) do{\
	uint32_t _row = _blkid << 6;\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 4,\
		.cmd      = {\
			CMD_ERASE_BLK,\
			0xFF & (_row >> 16),\
			0xFF & (_row >> 8 ),\
			0xFF & (_row >> 0 ),\
		},\
		.xfer_len = 0,\
	});\
}while(0)

#define snand_program_data_to_cache(_id, _len, _wbuf) do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 3,\
		.cmd      = {\
			CMD_PROG_PAGE_CLRCACHE,\
			0XFF & (_id >> 8),\
			0XFF & (_id >> 0),\
		},\
		.xfer_len = len,\
		.tx       = wbuf,\
	});\
}while(0)

#define snand_program_execute(_id) do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 4,\
		.cmd      = {\
			CMD_PROG_PAGE_EXC,\
			0XFF & (_id >> 16),\
			0XFF & (_id >> 8 ),\
			0XFF & (_id >> 0 ),\
		},\
		.xfer_len = 0,\
	});\
}while(0)

#define snand_get_status(_REG_STAT, _STATUS) do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 2,\
		.cmd      = { CMD_READ_REG, _REG_STAT },\
		.xfer_len = 1,\
		.rx       = _STATUS,\
	});\
}while(0)

#define snand_set_status(_reg, _val) do{\
	nand_cmd_send(&(nand_cmd_t){\
		.cmd_len  = 3,\
		.cmd      = { CMD_WRITE_REG, _reg, _val },\
		.xfer_len = 0,\
	});\
}while(0)

static int nand_cmd_send(nand_cmd_t *nand)
{
	priv->spi_device->set_cs(CS_ENABLE);

	spi_xfer(priv->spi_device, nand->cmd, NULL, nand->cmd_len);
	if(nand->xfer_len)
		spi_xfer(priv->spi_device, nand->tx, nand->rx, nand->xfer_len);

	priv->spi_device->set_cs(CS_DISABLE);

	return nand->cmd_len + nand->xfer_len;
}

static int snand_wait_ready(int timeout_us)
{
	u64 time_start = get_time_us(), tmp;
	u8 status = 0xff;

	while(1){

		snand_get_status(REG_STATUS, &status);

		if((status & STATUS_OIP_MASK) != STATUS_BUSY)
			break;

		if(timeout_us < 0)
			continue;

		tmp = get_time_us() - time_start;
		if(tmp >= timeout_us)
			break;
	}

	return -(status & STATUS_OIP_MASK);
}

static void snand_read_id(void *id)
{
	nand_cmd_t c;

	c.cmd_len = 2;
	c.cmd[0] = CMD_READ_ID;
	c.cmd[1] = 0;
	c.xfer_len = 3;
	c.rx = id;
	c.tx = NULL;

	snand_wait_ready(SFLASH_READY_TIMEOUT);

	nand_cmd_send(&c);
}

static int snand_unlock(void)
{
	return nand_cmd_send(&(nand_cmd_t){
		.cmd_len  = 3,
		.cmd      = { CMD_WRITE_REG, 0xa0, 0 },
		.xfer_len = 0,
	});
}

static struct spi_nand_info *snand_get_info(uint32_t id)
{
	uint32_t tmp;
	struct nand_id_index *tb = id_table;
	while(tb->info != NULL){
		if ((tb->id >> 16) == 0)
			tmp = id & 0x0000FFFF;
		else
			tmp = id & 0x00FFFFFF;
		if (tmp == tb->id){
			tb->info->nand_id = tb->id;
			tb->info->name    = tb->name;
			return tb->info;
		}
		++tb;
	}

	return NULL;
}

static inline int snand_read_status(u8 *status)
{
	snand_get_status(REG_STATUS, status);

	return 0;
}

static int32_t  snand_page_read(uint32_t page_id,                              \
	   uint16_t offset, uint16_t len, uint8_t* rbuf)
{
	uint8_t  status = 0;

	//针对Dosilicon的DS35Q2GA做特殊处理,DS35Q2GA只有一个die,每个die有两个plane.
	//当block number为奇数,选择另外一个plane.
	if((priv->id == 0x72e5) && ((page_id / PAGES_PER_BLOCK) % 2)){
		offset |= (0x1 << 12);
	}

	snand_wait_ready(SFLASH_READY_TIMEOUT);

	snand_read_page_to_cache(page_id);

	while (1){
		snand_read_status(&status);
		if((status & STATUS_OIP_MASK) == STATUS_READY){
			/* FS35ND01G-D1F1/FS35ND01G-S1F1 ECC错误标志不一样 */
			if ((priv->id & 0x1ff) != 0x1cd) {
				if((status & STATUS_COMMON_ECC_MASK) == STATUS_COMMON_ECC_ERROR)
					return -1;
			}else{ //处理 foresee spi nand ecc 错误标志位特异性
				if((status & STATUS_FORESEE_ECC_MASK) == STATUS_FORESEE_ECC_ERROR)
					return -1;
			}
			break;
		}
	}
	snand_read_from_cache(offset, len, rbuf);

	return len;
}

static int32_t  snand_page_program(uint32_t page_id,                           \
	   uint16_t offset, uint16_t len, uint8_t* wbuf)
{
	uint8_t status;
	int i;

	/* 跳过数据全为0xff页 */
	for (i = 0; i < len; i++){
		if (wbuf[i] != 0xff)
			break;
	}
	if (i == len)
		return 0;

	//针对Dosilicon的DS35Q2GA做特殊处理,DS35Q2GA只有一个die,每个die有两个plane.
	//当block number为奇数,选择另外一个plane.
	if((priv->id == 0x72e5) && ((page_id / PAGES_PER_BLOCK) % 2)){
		offset |= (0x1 << 12);
	}

	snand_wait_ready(SFLASH_READY_TIMEOUT);

	snand_write_enable();
	snand_program_data_to_cache(offset, len, wbuf);
	snand_program_execute(page_id);

	while (1){
		if(snand_read_status(&status) < 0)
			return -1;
		if(STATUS_READY == (status & STATUS_OIP_MASK)){
			if(STATUS_P_FAIL == (status & STATUS_P_FAIL_MASK))
				goto out;
			break;
		}
	}

	return 0;
out:
	return -1;
}

static void snand_hwecc(bool enable)
{
	uint8_t status;

	snand_get_status(REG_OTP, &status);
	status &= ~REG_ECC_MASK;
	status |= (!!enable) << 4;
	snand_set_status(REG_OTP, status);
}

static inline void bbt_mask_bad(struct spi_nand_info *info, u32 blk_id)
{
	u8 *bbt;

	if(blk_id > info->bbt_size * 8)
		return ;

	bbt = info->bbt + (blk_id >> 3);
	*bbt |= 0x01 << (blk_id & 0x07);
}

static inline bool bbt_block_isbad(struct spi_nand_info *info, u32 blk_id)
{
	u8 *bbt;

	if(blk_id > info->bbt_size * 8)
		return 1;

	bbt = info->bbt + (blk_id >> 3);

	return (*bbt >> (blk_id & 0x07)) & 0x01;
}

static int snand_block_isbad(uint32_t ofs)
{
	struct   spi_nand_info *info = priv->info;
	uint16_t block_id = ofs >> info->block_shift;
	uint8_t  is_bad   = bbt_block_isbad(info, block_id);

	return is_bad;
}

static int snand_scan_bad_blocks(struct spi_nand_info *info)
{
	int i, ret, block_count;
	u8 is_bad;

	if(!info)
		return -1;

	block_count = info->usable_size >> info->block_shift;

	if(info->bbt)
		return 0;

	info->bbt_size = block_count >> 3;
	if (sizeof(bbt_buff) < info->bbt_size) {
		printf ("%s, bbt buff size is not enough, buffsize : %u, need_size : %u\n", \
				__func__, sizeof(bbt_buff), info->bbt_size);
		return -1;
	}
	info->bbt = bbt_buff;
	memset(info->bbt, 0, block_count >> 3);

	snand_hwecc(0);

	for(i = 0; i < block_count; ++i){
		int page_id = i << (info->block_shift - info->page_shift);
		ret = snand_page_read(page_id, info->page_main_size, 1, &is_bad);
		if(ret < 0 || is_bad != 0xff)
			bbt_mask_bad(info, i);

	}

	snand_hwecc(1);

	return 0;
}

static int snand_read_ops(ssize_t from, struct mtd_oob_ops *ops)
{
	assert_param(ops, -1);
	struct spi_nand_info *info = priv->info;
	int page_id, page_offset = 0;
	int count = 0;
	int main_left, main_ok = 0, main_offset = 0;
	int size, retval;

	page_id = from >> info->page_shift;
	page_offset = from & info->page_mask;
	main_left = ops->len;
	main_offset = page_offset;

	while (main_left){
		size = min(main_left, info->page_main_size - main_offset);
		retval = snand_page_read(page_id + count, main_offset, \
					size, ops->datbuf + main_ok);

		if (retval < 0) {
			printf("snand_read_ops: fail, page=%d!\n", page_id);
			return -EIO;
		}

		main_ok += size;
		main_left -= size;
		main_offset = 0;
		count++;
	}

	ops->retlen = main_ok;

	return main_ok;
}

static int snand_read_skip_bad(ssize_t offset, void *buf, uint32_t len)
{
	struct   spi_nand_info *info = priv->info;
	uint32_t blk_sz    = info->block_main_size;
	ssize_t  read_left = len;

	if(len == 0)
		return 0;

	assert_param(buf, -EFAULT);
	assert_param(offset + len <= info->usable_size, -EINVAL);

	uint8_t *pbuf = buf;
	struct   mtd_oob_ops ops;

	while(read_left > 0){
		ssize_t blk_ofs = offset & info->block_mask;
		/* skip bad block */
		if(snand_block_isbad(offset & ~info->block_mask)){
			offset += blk_sz - blk_ofs;
			continue;
		}

		ops.datbuf = pbuf;
		ops.len    = min(read_left, blk_sz-blk_ofs);

		if(snand_read_ops(offset, &ops) < 0)
			return -EIO;

		pbuf      += ops.retlen;
		offset    += ops.retlen;
		read_left -= ops.retlen;
	}

	return pbuf-(uint8_t*)buf;
}

static int snand_read(uint32_t offset, uint8_t *dest, uint32_t len)
{
	int ret;

	assert_param(priv, -1);
	ret = snand_read_skip_bad(offset, dest, len);
	if (ret < 0)
		return -1;

	return 0;
}

static int snand_write_ops(ssize_t offset, struct mtd_oob_ops *ops)
{
	assert_param(ops, -1);
	struct spi_nand_info *info = priv->info;
	int page_id, page_offset = 0;
	int count = 0;
	int main_left, main_ok = 0, main_offset = 0;
	int size, retval;

	page_id = offset >> info->page_shift;
	page_offset = offset & info->page_mask;
	main_left = ops->len;
	main_offset = page_offset;

	while (main_left){
		size = min(main_left, info->page_main_size - main_offset);
		retval = snand_page_program(page_id + count, main_offset, \
					size, ops->datbuf + main_ok);

		if (retval < 0) {
			printf("snand_page_program: fail, page=%d!\n", page_id);
			return -1;
		}

		main_ok += size;
		main_left -= size;
		main_offset = 0;
		count++;
	}

	ops->retlen = main_ok;

	return main_ok;
}

static int snand_write_skip_bad(ssize_t offset, void *buf, uint32_t len)
{
	struct   spi_nand_info *info = priv->info;
	uint32_t blk_sz    = info->block_main_size;
	ssize_t  read_left = len;

	if(len == 0)
		return 0;

	assert_param(buf, -EFAULT);
	assert_param(offset + len <= info->usable_size, -EINVAL);

	uint8_t *pbuf = buf;
	struct   mtd_oob_ops ops;

	while(read_left > 0){
		ssize_t blk_ofs = offset & info->block_mask;
		/* skip bad block */
		if(snand_block_isbad(offset & ~info->block_mask)){
			offset += blk_sz - blk_ofs;
			continue;
		}

		ops.datbuf = pbuf;
		ops.len    = min(read_left, blk_sz-blk_ofs);

		if(snand_write_ops(offset, &ops) < 0)
			return -EIO;

		pbuf      += ops.retlen;
		offset    += ops.retlen;
		read_left -= ops.retlen;
	}

	return pbuf-(uint8_t*)buf;
}

static int snand_write(uint32_t offset, uint8_t *dest, uint32_t len)
{
	int ret;

	assert_param(priv, -1);
	ret = snand_write_skip_bad(offset, dest, len);
	if (ret < 0)
		return -1;

	return 0;
}

static int snand_erase_block(ssize_t block_id)
{
	uint8_t status= 0;

	snand_wait_ready(SFLASH_READY_TIMEOUT);

	snand_write_enable();
	snand_erase_block_erase(block_id);

	while (1){
		snand_read_status(&status);

		if ((status & STATUS_OIP_MASK) == STATUS_READY){
			if ((status & STATUS_E_FAIL_MASK) == STATUS_E_FAIL){
				printf("erase error, block=%d\n", block_id);
				return -EIO;
			}
			break;
		}
	}

	return 0;
}

static int snand_erase(uint32_t offset, uint32_t len)
{
	struct   spi_nand_info *info = priv->info;
	uint32_t block_sz = info->block_main_size;
	ssize_t  erase_cnt;
	int ret = 0;

	if(offset >= info->usable_size || offset & info->block_mask)
		return -EINVAL;

	if(len + offset >= info->usable_size)
		len = info->usable_size - offset;

	erase_cnt = ((offset & info->block_mask) + len + block_sz - 1) / block_sz;

	snand_unlock();

	while(erase_cnt > 0){

		if(offset >= info->usable_size)
			break;

		/* skip bad block */
		if(snand_block_isbad(offset & ~info->block_mask)){
			printf ("%s, find bad block, block id : %d\n",
				 __func__, offset >> info->block_shift);
			offset += block_sz;
			continue;
		}

		ret = snand_erase_block(offset >> info->block_shift);
		if(ret < 0)
			return ret;

		offset += block_sz;
		--erase_cnt;
	}

	return 0;
}

static int snand_chiperase(void)
{
	struct spi_nand_info *info = priv->info;

	return snand_erase(0, info->usable_size);
}

static int snand_badinfo(void)
{
	struct spi_nand_info *info = priv->info;
	u32 block_sz   = info->block_main_size;
	u32 blk_cnt = 0;

	while(blk_cnt*block_sz < info->usable_size){

		if(snand_block_isbad(blk_cnt*block_sz))
			printf("block %d id bad.\n", blk_cnt);
		blk_cnt++;
	}

	return 0;
}

static int snand_calc_phy_offset(u32 phy_start, u32 logic_offs, u32 *phy_offset)
{
	struct spi_nand_info *info;
	u32 block_id_start, block_id_end;
	int block_cnt, i, badblock = 0;

	info = priv->info;

	if(phy_start & info->block_mask){
		snd_err("phy_start not aligned by block\n");
		return -EINVAL;
	}

	if(!phy_offset){
		snd_err("phy_offset is null\n");
		return -EINVAL;
	}

	block_id_start = phy_start >> info->block_shift;
	block_id_end   = (phy_start + logic_offs) >> info->block_shift;

	block_cnt = block_id_end - block_id_start;

	for(i = 0; block_cnt; ++i){
		u32 offset = (i + block_id_start) << info->block_shift;
		if(snand_block_isbad(offset)){
			badblock++;
			continue;
		}

		block_cnt--;
	}

	snd_dbg(SND_DBG_PHY, "block start %d, end %d, bad block %d, "
		"phy_offs %d, log_offs %d\n",block_id_start, block_id_end,
				badblock, i, logic_offs>>info->block_shift);

	*phy_offset = logic_offs + ((i + block_id_start - block_id_end) << info->block_shift);

	if(*phy_offset & info->page_mask){
		snd_dbg(SND_DBG_PHY, "WARNING: phy_offs not aligned by page.\n");
		return 1;
	}

	return 0;
}

static int snand_getsize(enum spi_flash_info flash_info)
{
	struct spi_nand_info *info = priv->info;

	switch(flash_info){
		case SPI_FLASH_SIZE_CHIP:
			return info->usable_size;

		case SPI_FLASH_SIZE_BLOCK:
		case SPI_FLASH_SIZE_SECTOR:
			return info->block_main_size;

		case SPI_FLASH_NUM_BLOCK:
		case SPI_FLASH_NUM_SECTOR:
			return info->block_num_per_chip;

		case SPI_FLASH_SIZE_PAGE:
			return info->page_main_size;

		case SPI_FLASH_NUM_PAGE:
			return info->page_num_per_block * info->block_num_per_chip;

		default: break;
	}

	return -EINVAL;
}

struct flash_dev spi_nand_flash_dev;

static struct flash_dev *snand_init(struct spi_device *spi_device)
{
	static struct spi_nand_priv local_priv;
	priv = &local_priv;
	memset(priv, 0, sizeof(*priv));

	priv->spi_device = spi_device;
	snand_read_id(&(priv->id));

	priv->info = snand_get_info(priv->id);
	priv->id   = priv->info->nand_id;
	if(!priv->info){
		snd_err("unsupport spi nand flash ID: 0x%x\n", priv->id);
		return NULL;
	}

	if(snand_scan_bad_blocks(priv->info) < 0){
		snd_err("create bbt error!\n");
		return NULL;
	}

	return &spi_nand_flash_dev;
}

struct flash_dev spi_nand_flash_dev = {
	.init                 = snand_init,
	.readdata             = snand_read,
	.erasedata            = snand_erase,
	.pageprogram          = snand_write,
	.write_protect_unlock = snand_unlock,
	.block_isbad          = snand_block_isbad,
	.chiperase            = snand_chiperase,
	.badinfo              = snand_badinfo,
	.calc_phy_offset      = snand_calc_phy_offset,
	.getsize              = snand_getsize,
};
