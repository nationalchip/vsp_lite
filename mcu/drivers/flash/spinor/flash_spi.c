/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * flash_spi.c: NOR Flash driver of Flash SPI
 *
 */

#include <autoconf.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <util.h>
#include <stdarg.h>
#include "flash_spi.h"
#include <driver/dma_axi.h>
#include <driver/flash.h>
#include <base_addr.h>
#include <driver/irq.h>
#include <board_config.h>

#ifndef FLASH_BLOCK_SIZE
#define FLASH_BLOCK_SIZE 0x10000
#endif
#define FLASH_SECTOR_SIZE 0x1000
#define FLASH_PAGE_SIZE 0x100

#ifdef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
#define FLASH_ERASE_SIZE FLASH_SECTOR_SIZE
#else
#define FLASH_ERASE_SIZE FLASH_BLOCK_SIZE
#endif

#define CMD_SIZE        6

#define SPI_CS_HIGH()  do {writel(0x03, FLASH_SPI_CS_REG);} while(0);
#define SPI_CS_LOW()   do {writel(0x02, FLASH_SPI_CS_REG);} while(0);
#define SPI_CS_HW()    do {writel(0x01, FLASH_SPI_CS_REG);} while(0);

struct spi_flash
{
	int  index;
	unsigned int  size;
	unsigned int  addr_width;
	sflash_info   *info;
	unsigned char command[CMD_SIZE];
	int (*program_page)(unsigned int addr, const void *buf, unsigned int len);
	int (*read_data)(unsigned int from, unsigned char *buf,unsigned int len);
};
static struct spi_flash g_flash_info = { -1, 0, 0, NULL};
struct flash_dev flash_spi_dev;

SECTION_SRAM_RODATA
static sflash_info sflash_data[] = {
	/*--- en25qxx spi flash, eon silicon solution ---*/
	/* P25Q16SH 16 MBit devices. */
	{"P25Q16SH",    0x00856015,   0x200000,  DUAL_READ | QUAD_READ},
	/* en25f16-100HIP 16 MBit devices. */
	{"en25f16",    0x001c3115,   0x200000,   DUAL_READ },
	/* en25q16 16 MBit devices. */
	{"en25q16",    0x001c3015,   0x200000,   DUAL_READ },
	/* en25qh16-104HIP 16 MBit devices. */
	{"en25qh16",   0x001c7015,   0x200000,   DUAL_READ },
	/* en25f32 32 MBit devices. */
	{"en25f32",    0x001c3116,   0x400000,   DUAL_READ },
	/* en25q32b-104HIP 32 MBit devices. */
	{"en25q32",    0x001c3016,   0x400000,   DUAL_READ },
	/* en25qh32b-104HIP 32 MBit devices. */
	{"en25qh32b",  0x001c7016,   0x400000,   DUAL_READ | QUAD_READ },
	/* en25qh64a-104HIP 64 MBit devices. */
	{"en25qh64a",  0x001c7017,   0x800000,   DUAL_READ | QUAD_READ },
	/* en25q64-104HIP 64 MBit devices. */
	{"en25q64",    0x001c3017,   0x800000,  DUAL_READ },
	/* en25q128_d 128 MBit devices.*/
	{"en25q128",   0x001c3018,   0x1000000,  DUAL_READ },
	/* en25qh128_d 128 MBit devices. */
	{"en25qh128",  0x001c7018,   0x1000000,  DUAL_READ },

	/*--- f25lxx spi flash, elite semiconductor ---*/
	/* f25l16pa  16 MBit devices. */
	{"f25l016",    0x008c2115,   0x200000,   DUAL_READ  },
	/* f25l032qa 32 MBit devices.*/
	{"f25l032",    0x008c2116,   0x400000,   DUAL_READ  },

	/*--- gd25xx spi flash, GigaDevice ---*/
	/* gd25q16 16 MBit devices. */
	{"gd25q16",    0x00c84015,   0x200000,   DUAL_READ | QUAD_READ },
	{"gd25ve16c",  0x00c84215,   0x200000,   DUAL_READ | QUAD_READ },
	/* gd25q32 32 MBit devices.*/
	{"gd25q32",    0x00c84016,   0x400000,   DUAL_READ | QUAD_READ },
	/* gd25q64 64 MBit devices. */
	{"gd25q64",    0x00c84017,   0x800000,  DUAL_READ | QUAD_READ },
	/* gd25q128 128 MBit devices.*/
	{"gd25q128",   0x00c84018,   0x1000000,  DUAL_READ | QUAD_READ },

	/*--- m25pxx spi flash, micron(numonyx) ---*/
#ifndef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
	/* flash m25p16 16MBit devices.*/
	{"m25p016",    0x00202015,   0x200000,   DUAL_READ },
	/* flash m25p128 128MBit devices.*/
	{"m25p128",    0x00202018,   0x1000000,  DUAL_READ },
#endif
	/* flash n25q032pa 32MBit devices.*/
	{"n25q032",    0x0020ba16,   0x400000,   DUAL_READ },
	/* flash n25q064 64MBit devices.*/
	{"n25q064",    0x0020ba17,   0x800000,  DUAL_READ },
	/* flash n25q128 128MBit devices.*/
	{"n25q128",    0x0020ba18,   0x1000000,  DUAL_READ },

	/*--- MXIC ---*/
	/* mx25l1606E 16 MBit devices.*/
	{"mx25l16",    0x00c22015,   0x200000,   DUAL_READ  },
	/* mx25l3206E 32 MBit devices.*/
	{"mx25l32",    0x00c22016,   0x400000,   DUAL_READ  },
	/* mx25l6406E 64 MBit devices.*/
	{"mx25l64",    0x00c22017,   0x800000,  DUAL_READ  },
	/* mx25l12835E 128 MBit devices.*/
	{"mx25l128",   0x00c22018,   0x1000000,  DUAL_READ  },
	/* mx25l25635F 256 MBit devices.*/
	{"mx25l256",   0x00c22019,   0x2000000,  DUAL_READ  },

	/*--- winbond ---*/
	/* w25q16bv /W25Q16 16 MBit devices. */
	{"w25q16",     0x00ef4015,   0x200000,   DUAL_READ | QUAD_READ },
	/* w25q32bv /W25Q32 32 MBit devices. */
	{"w25q32",     0x00ef4016,   0x400000,   DUAL_READ | QUAD_READ },
	/* w25q32dw 32 MBit devices. */
	{"w25q32dw",   0x00ef6016,   0x400000,   DUAL_READ | QUAD_READ },
	/* w25q64bv /W25Q64 64 MBit devices. */
	{"w25q64",     0x00ef4017,   0x800000,  DUAL_READ | QUAD_READ },
	/* w25q128bv /W25Q128 128 MBit devices. */
	{"w25q128",    0x00ef4018,   0x1000000,  DUAL_READ | QUAD_READ },
	/* w25q256bv /W25Q256 256 MBit devices. */
	{"w25q256",    0x00ef4019,   0x2000000,  DUAL_READ | QUAD_READ },

#ifndef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
	/*--- spansion ---*/
	/* s25fl032p 32 MBit devices.*/
	{"s25fl032p",   0x00010215,   0x400000,   DUAL_READ },
	/* s25fl064p 64 MBit devices.*/
	{"s25fl064p",   0x00010216,   0x800000,   DUAL_READ },
	/* s25fl128p 128 MBit devices.*/
	{"s25fl128p",   0x00012018,   0x1000000,  DUAL_READ },
	/* s25fl256s 128 MBit devices.*/
	{"s25fl256s",   0x00010219,   0x2000000,  DUAL_READ },
#endif

	/*--- spi flash, pmc ---*/
	/* pm25lq032  32 MBit devices.*/
	{"pm25lq032",  0x007f9d46,   0x400000,   DUAL_READ },

	/*--- paragon ---*/
	/*pm25f32  32 MBit devices.*/
	{"pn25f32",    0x00e04016,   0x400000,   DUAL_READ },

	/*--- issi ---*/
	/*ic25lp064  64 MBit devices.*/
	{"ic25lp064",  0x009d6017,   0x800000,  DUAL_READ },

	/*--- 芯天下 ---*/
	/*xt25f04b  4 MBit devices.*/
	{"xt25f04b",   0x000b4013,   0x80000,   DUAL_READ | QUAD_READ },
	/*xt25f08b  8 MBit devices.*/
	{"xt25f08b",   0x000b4014,   0x100000,   DUAL_READ | QUAD_READ },
	/*xt25f16b  16 MBit devices.*/
	{"xt25f16b",   0x000b4015,   0x200000,   DUAL_READ | QUAD_READ },
	/*xt25f32b  32 MBit devices.*/
	{"xt25f32b",   0x000b4016,   0x400000,   DUAL_READ | QUAD_READ },

	/*--- Boya Microelectronics ---*/
	/*by25q16bs 16 Mbit devices.*/
	{"by25q16bs",  0x00684015,   0x200000,   DUAL_READ | QUAD_READ },
	/*by25q32bs 32 Mbit devices.*/
	{"by25q32bs",  0x00684016,   0x400000,   DUAL_READ },
	/*by25q64as 64 Mbit devices.*/
	{"by25q64as",  0x00684017,   0x800000,  DUAL_READ },

	/*--- yichu technology ---*/
	/*YC25Q16W  16 Mbit devices.*/
	{"YC25Q16W",   0x005e4015,   0x200000,   DUAL_READ },

	/*--- Wuhan Xinxin---*/
	/* XM25QH16C 16Mbit devices.*/
	{"xm25qh16c",  0x00204015,   0x200000,   DUAL_READ | QUAD_READ },
	/* XM25QH32B 32Mbit devices.*/
	{"xm25qh32b",  0x00204016,   0x400000,   DUAL_READ | QUAD_READ },

	/*--- Zbit ---*/
	/* ZB25VQ32B 16Mbit devices.*/
	{"zb25vq16a",  0x005e6015,   0x200000,   DUAL_READ | QUAD_READ },
	/* ZB25VQ32B 32Mbit devices.*/
	{"zb25vq32b",  0x005e4016,   0x400000,   DUAL_READ | QUAD_READ },


    /*--- Zetta ---*/
    {"zb25q16b",   0x00ba6015,   0x200000,   DUAL_READ | QUAD_READ },

	{NULL,         0x00,         0x00,       0 },            //end
};

SECTION_SRAM_TEXT
static inline int wait_bus_ready(void)
{
	uint32_t val;

	do{
		val = readl(SPIM_SR);
	} while (val & SR_BUSY);

	return 0;
}

SECTION_SRAM_TEXT
static inline int wait_rx_done(void)
{
	uint32_t val;

	do{
		val = readl(SPIM_RXFLR);
	} while (val != 0);

	wait_bus_ready();

	return 0;
}

SECTION_SRAM_TEXT
static inline int wait_tx_done(void)
{
	uint32_t val;

	do{
		val = readl(SPIM_TXFLR);
	} while (val != 0);

	wait_bus_ready();

	return 0;
}

SECTION_SRAM_TEXT
static inline int reset_spi(void)
{
	/* SPI FIFOs are reset when SSIC_EN=0 */
	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_ENR);
	writel(0x01, SPIM_ENR);
	writel(0x01, SPIM_SER);

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_read_reg(uint8_t cmd, void* reg_val, uint32_t len)
{
	uint32_t i, val;
	uint8_t *p;

	p = (uint8_t*)reg_val;
	wait_bus_ready();
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_DMACR);
	writel(EEPROM_8BITS_MODE, SPIM_CTRLR0);
	writel(len - 1, SPIM_CTRLR1);
	writel(0x01, SPIM_SER);
	writel(0 << 16, SPIM_TXFTLR);
	writel(0x00, SPIM_SPI_CTRL0);
	writel(0x01, SPIM_ENR);

	writel(cmd, SPIM_TXDR_LE);

	for (i = 0; i < len; i++) {
		do{
			val = readl(SPIM_SR);
		} while (!(val & SR_RF_NOT_EMPT));
		p[i] = readl(SPIM_RXDR_LE);
	}

	wait_rx_done();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_write_reg(uint8_t cmd, const void* reg_val, uint32_t len)
{
	uint32_t i, val;
	const uint8_t *p;

	p = (const uint8_t*)reg_val;
	wait_bus_ready();
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_DMACR);
	writel(STANDARD_TO_8BITS_MODE, SPIM_CTRLR0);
	writel(len, SPIM_CTRLR1);
	writel(0x01, SPIM_SER);
	writel(len << 16, SPIM_TXFTLR);
	writel(0x00, SPIM_SPI_CTRL0);
	writel(0x01, SPIM_ENR);

	writel(cmd, SPIM_TXDR_LE);

	for (i = 0; i < len; i++) {
		do{
			val = readl(SPIM_SR);
		} while (!(val & SR_TF_NOT_FULL));
		writel(p[i], SPIM_TXDR_LE);
	}

	wait_tx_done();

	return 0;
}

/*
 * Read the status register, returning its value in the location
 * Return the status register value.
 * Returns negative if error occurred.
 */
SECTION_SRAM_TEXT
static uint8_t read_sr1(void)
{
	uint8_t status;

	sflash_read_reg(GX_CMD_RDSR1, &status, sizeof(status));

	return status;
}

/*
 * Set write enable latch with Write Enable command.
 * Returns negative if error occurred.
 */
SECTION_SRAM_TEXT
static inline int write_enable(void)
{
	sflash_write_reg(GX_CMD_WREN, NULL, 0);

	return 0;
}

/*
 * Service routine to read status register until ready, or timeout occurs.
 * Returns **** if error.
 */
SECTION_SRAM_TEXT
static int wait_till_ready(void)
{
	uint8_t sr;

	do{
		sr = read_sr1();
	}while (sr & GX_STAT_WIP);

	return 1;
}

// Write 8bit or 16bit status register
SECTION_SRAM_TEXT
static int write_sr(uint8_t *buf, uint32_t len, uint32_t reg_order)
{
	uint8_t cmd;

	if(len > 2)
		return -1;

	switch(reg_order)
	{
		case 1:
			cmd = GX_CMD_WRSR1;
			break;
		case 2:
			cmd = GX_CMD_WRSR2;
			break;
		case 3:
			cmd = GX_CMD_WRSR3;
			break;
		default:
			return -1;
	}

	wait_till_ready();
	write_enable();
	sflash_write_reg(cmd, buf, len);

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_readid(int * jedec)
{
	int              tmp;
	unsigned char    id[3];

	tmp = sflash_read_reg(GX_CMD_READID, id, sizeof(id));
	if (tmp < 0) {
		printf( "spi flash bus: error %d reading JEDEC ID\n", tmp);
		return -2;
	}
	*jedec = id[0];
	*jedec = *jedec << 8;
	*jedec |= id[1];
	*jedec = *jedec << 8;
	*jedec |= id[2];

	return 0;
}

#if defined(CONFIG_MCU_FLASH_SPI_QUAD) || defined(CONFIG_MCU_FLASH_SPI_AUTO)
/*
 * Read the status register, returning its value in the location
 * Return the status register value.
 * Returns negative if error occurred.
 */
SECTION_SRAM_TEXT
static uint8_t read_sr2(void)
{
	uint8_t status;

	sflash_read_reg(GX_CMD_RDSR2, &status, sizeof(status));

	return status;
}

SECTION_SRAM_TEXT
static uint8_t read_sr3(void)
{
	uint8_t status;

	sflash_read_reg(GX_CMD_RDSR3, &status, sizeof(status));

	return status;
}

/* 将status2 寄存器QE位置1 */
SECTION_SRAM_TEXT
static int sflash_enable_quad_0(void)
{
	uint8_t status = read_sr2();
	if (status & 0x02)
		return 0;
	status |= 0x02;
	write_sr(&status, 1, 2);
	wait_till_ready();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_enable_quad_1(void)
{
	uint8_t status[2];
	status[0] = read_sr1();
	status[1] = read_sr2();
	if (status[1] & 0x02)
		return 0;
	status[1] |= 0x02;
	write_sr(status, 2, 1);
	wait_till_ready();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_enable_hfm(void)
{
	uint8_t status = read_sr3();
	if (status & 0x10)
		return 0;
	status |= 0x10;
	write_sr(&status, 1, 3);
	wait_till_ready();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_enable_quad(int id)
{
	switch(id) {
	case 0x00854012: /* P25Q21L */
	case 0x00856013: /* P25Q40L */
	case 0x00c84215: /* GD25VE16C */
	case 0x000b4015: /* XT25F16B */
		sflash_enable_quad_1();
		break;
	case 0x001c3812: /* en25s20a */
	case 0x001c3813: /* en25s40a */
	case 0x001c7017: /* en25qh64a */
		break;
	case 0x00c84015: /* gd25q16 */
	case 0x00684015: /* by25q16bs */
		sflash_enable_quad_0();
		sflash_enable_quad_1();
		break;
	default:
		sflash_enable_quad_0();
		break;
	}

	/* XM25QH32B 四倍数开启HFM*/
	if (id == 0x00204016)
		sflash_enable_hfm();

	return 0;
}

#endif

SECTION_SRAM_TEXT
static void sflash_addr2cmd(struct spi_flash *flash, unsigned int addr, unsigned char *cmd)
{
	/* opcode is in cmd[0] */
	cmd[1] = addr >> (flash->addr_width * 8 -  8);
	cmd[2] = addr >> (flash->addr_width * 8 - 16);
	cmd[3] = addr >> (flash->addr_width * 8 - 24);
	cmd[4] = addr >> (flash->addr_width * 8 - 32);
}

SECTION_SRAM_TEXT
static inline int sflash_cmdsz(struct spi_flash *flash)
{
	return 1 + flash->addr_width;
}

SECTION_SRAM_TEXT
static int sflash_detect(struct spi_flash *flash)
{
	int  i        = 0;
	int  jedec    = 0;
	flash->index  = -1;

	if(sflash_readid(&jedec) != 0)
		return -1;

	printf("Flash JEDEC = %#x", jedec);
	for(i = 0; sflash_data[i].jedec_id; i++){
		if (jedec == sflash_data[i].jedec_id) {
			flash->index = i;
			flash->size  = sflash_data[i].size;
			flash->addr_width = 3;
			flash->info  = &sflash_data[i];
			printf(", model = %s.\n", sflash_data[i].name);
			break;
		}
	}

	if (flash->index < 0){
		for (i = 0; sflash_data[i].jedec_id; i++) {
			//if the spi flash not supported/exit, we assume it is SFLASH_MX_25L3205 to test
			if(0x00c22016 == sflash_data[i].jedec_id){
				flash->index = i;
				flash->size  = sflash_data[i].size;
				flash->addr_width = 3;
				flash->info= &sflash_data[i];
				break;
			}
		}
#ifndef CONFIG_BOOT_TOOL
		printf("\r\n Warning Spi Flash unsupported, please info nationalchip\r\n");
		printf("\r\n JEDEC id = %x\r\n", jedec);
#endif
		return -2;
	}

	return 0;
}


SECTION_SRAM_TEXT
static int sflash_chiperase(void)
{
	wait_till_ready();
	write_enable();

	sflash_write_reg(GX_CMD_CE, NULL, 0);
	wait_till_ready();

	return 0;
}

#ifdef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
SECTION_SRAM_TEXT
static void sflash_sector_erase(struct spi_flash *flash, unsigned int addr)
{
	wait_till_ready();
	write_enable();

	sflash_addr2cmd(flash,addr,flash->command);
	sflash_write_reg(GX_CMD_SE, &(flash->command[1]), 3);
	wait_till_ready();
}
#endif

SECTION_SRAM_TEXT
static void sflash_block_erase(struct spi_flash *flash, unsigned int addr)
{
	wait_till_ready();
	write_enable();

	sflash_addr2cmd(flash,addr,flash->command);
	sflash_write_reg(GX_CMD_BE, &(flash->command[1]), 3);
	wait_till_ready();
}

#if defined(CONFIG_MCU_FLASH_SPI_STANDARD) || defined(CONFIG_MCU_FLASH_SPI_DUAL) || defined(CONFIG_MCU_FLASH_SPI_AUTO)
#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
SECTION_SRAM_TEXT
static int sflash_standard_1pageprogram(unsigned int addr, const void *buf, unsigned int len)
{
	int dma_channel;
	DW_DMAC_CH_CONFIG dma_config;

	dma_channel = dw_dma_select_channel();
	if (dma_channel < 0)
		return -1;

	wait_bus_ready();

	SPI_CS_LOW();
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_SER);

	writel(STANDARD_TO_8BITS_MODE, SPIM_CTRLR0);
	writel(len-1, SPIM_CTRLR1);
	writel(4 << 16, SPIM_TXFTLR);
	writel(0, SPIM_SPI_CTRL0);
	writel(8, SPIM_DMATDLR);

	writel(0x01, SPIM_ENR);

	writel(0x02, SPIM_TXDR_LE);
	writel(addr>>16, SPIM_TXDR_LE);
	writel(addr>>8, SPIM_TXDR_LE);
	writel(addr, SPIM_TXDR_LE);

	/* dma source config */
	dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_INC;
	dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;

	if ((uint32_t)buf >= 0xa0100000)
		dma_config.src_master_select = AXI_MASTER_1;
	else
		dma_config.src_master_select = AXI_MASTER_2;

	dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
	dma_config.src_per = 0;
	dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	/* dma destination config */
	dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
	dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;
	dma_config.dst_master_select = AXI_MASTER_2;
	dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_8;
	dma_config.dst_per = 0x01;
	dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	dma_config.flow_ctrl = DWAXIDMAC_TT_FC_MEM_TO_PER_DMAC;

	dw_dma_channel_config(dma_channel, dma_config);
	writel(0x02, SPIM_DMACR);
	writel(0x01, SPIM_SER);

	dw_dma_xfer((void*)SPIM_TXDR_LE, (void*)buf, len, dma_channel);
	dw_dma_wait_complete(dma_channel);
	writel(0x00, SPIM_DMACR);

	wait_tx_done();

	SPI_CS_HIGH();
	SPI_CS_HW();
	wait_till_ready();

	return 0;
}

#else /* CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH */

SECTION_SRAM_TEXT
static int sflash_standard_1pageprogram(unsigned int addr, const void *buf, unsigned int len)
{
	uint32_t i;
	uint8_t val;

	wait_bus_ready();

	SPI_CS_LOW();

	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_SER);

	writel(STANDARD_TO_8BITS_MODE, SPIM_CTRLR0);
	writel(len-1, SPIM_CTRLR1);
	writel(0 << 16, SPIM_TXFTLR);
	writel(0, SPIM_SPI_CTRL0);
	writel(8, SPIM_DMATDLR);

	writel(0x01, SPIM_ENR);

	writel(0x02, SPIM_TXDR_LE);
	writel(addr>>16, SPIM_TXDR_LE);
	writel(addr>>8, SPIM_TXDR_LE);
	writel(addr, SPIM_TXDR_LE);

	writel(0x01, SPIM_SER);

	for (i = 0; i < len; i++) {
		while(!(readl(SPIM_SR) & SR_TF_NOT_FULL));
		val = ((uint8_t*)buf)[i];
		writel(val, SPIM_TXDR_LE);
	}

	wait_tx_done();
	SPI_CS_HIGH();
	SPI_CS_HW();

	wait_till_ready();

	return 0;
}
#endif /* CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH */
#endif /* defined(CONFIG_MCU_FLASH_SPI_STANDARD) || defined(CONFIG_MCU_FLASH_SPI_DUAL) || defined(CONFIG_MCU_FLASH_SPI_AUTO) */

#if defined(CONFIG_MCU_FLASH_SPI_QUAD) || defined(CONFIG_MCU_FLASH_SPI_AUTO)
SECTION_SRAM_TEXT
static int sflash_quad_1pageprogram_raw(unsigned int addr, const void *buf, unsigned int len, int is_32bit)
{
	wait_bus_ready();

	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_SER);

	if (is_32bit) {
		writel(QUAD_TO_32BITS_MODE, SPIM_CTRLR0);
		writel(len/4-1, SPIM_CTRLR1);
	} else {
		writel(QUAD_TO_8BITS_MODE, SPIM_CTRLR0);
		writel(len-1, SPIM_CTRLR1);
	}

	writel(0x01, SPIM_SER);
	writel(2 << 16, SPIM_TXFTLR);
	writel(STRETCH_INST8_ADDR24, SPIM_SPI_CTRL0);
	writel(8, SPIM_DMATDLR);

	writel(0x01, SPIM_ENR);

	writel(0x32, SPIM_TXDR_BE);
	writel(addr, SPIM_TXDR_BE);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	int dma_channel;
	DW_DMAC_CH_CONFIG dma_config;

	dma_channel = dw_dma_select_channel();
	if (dma_channel < 0) {
		reset_spi();
		return -1;
	}

	/* dma source config */
	dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_INC;
	dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;

	if ((uint32_t)buf >= 0xa0100000)
		dma_config.src_master_select = AXI_MASTER_1;
	else
		dma_config.src_master_select = AXI_MASTER_2;

	dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
	dma_config.src_per = 0;

	if (is_32bit)
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	/* dma destination config */
	dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
	dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;
	dma_config.dst_master_select = AXI_MASTER_2;
	dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_8;
	dma_config.dst_per = 0x01;

	if (is_32bit)
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	dma_config.flow_ctrl = DWAXIDMAC_TT_FC_MEM_TO_PER_DMAC;

	dw_dma_channel_config(dma_channel, dma_config);

	if (is_32bit)
		dw_dma_xfer((void*)SPIM_TXDR_LE, (void*)buf, len/4, dma_channel);
	else
		dw_dma_xfer((void*)SPIM_TXDR_LE, (void*)buf, len, dma_channel);

	writel(0x02, SPIM_DMACR);
	dw_dma_wait_complete(dma_channel);

	writel(0x00, SPIM_DMACR);
#else /* CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH */
	uint32_t val;
	uint32_t i;
	if (is_32bit) {
		for (i = 0; i < len/4; i++) {
			while(!(readl(SPIM_SR) & SR_TF_NOT_FULL));
			val = ((uint32_t*)buf)[i];
			writel(val, SPIM_TXDR_LE);
		}
	} else {
		for (i = 0; i < len; i++) {
			while(!(readl(SPIM_SR) & SR_TF_NOT_FULL));
			val = ((uint8_t*)buf)[i];
			writel(val, SPIM_TXDR_LE);
		}
	}
#endif
	wait_tx_done();
	wait_till_ready();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_quad_1pageprogram(unsigned int addr, const void *buf, unsigned int len)
{
	uint32_t buf_addr = (uint32_t)buf;

	if ((buf_addr % 4) || (len % 4))
		return sflash_quad_1pageprogram_raw(addr, buf, len, 0);
	else
		return sflash_quad_1pageprogram_raw(addr, buf, len, 1);
}
#endif /* defined(CONFIG_MCU_FLASH_SPI_QUAD) || defined(CONFIG_MCU_FLASH_SPI_AUTO) */

/*
 * Write an address range to the flash chip.  Data must be written in
 * GX_PAGESIZE chunks.  The address range may be any size provided
 * it is within the physical boundaries.
 */
SECTION_SRAM_TEXT
static int sflash_pageprogram(unsigned int to,unsigned char *buf,unsigned int len)
{
	unsigned int page_offset, page_size;

	struct spi_flash *flash;

	flash = &g_flash_info;


	/* sanity checks */
	if (!len)
		return(0);

	if (to + len > flash->size)
		return -EINVAL;

	/* Wait until finished previous write command. */
	wait_till_ready();

	write_enable();

	/* what page do we start with? */
	page_offset = to % GX_PAGESIZE;

	/* do all the bytes fit onto one page? */
	if (page_offset + len <= GX_PAGESIZE) {
		g_flash_info.program_page(to, buf, len);
	} else {
		unsigned int i;

		/* the size of data remaining on the first page */
		page_size = GX_PAGESIZE - page_offset;

		g_flash_info.program_page(to, buf, page_size);

		/* write everything in PAGESIZE chunks */
		for (i = page_size; i < len; i += page_size) {
			page_size = len - i;
			if (page_size > GX_PAGESIZE)
				page_size = GX_PAGESIZE;

			write_enable();
			g_flash_info.program_page(to+i, buf+i, page_size);
		}
	}

	return 0;
}

#if defined(CONFIG_MCU_FLASH_SPI_STANDARD) || defined(CONFIG_MCU_FLASH_SPI_AUTO)
#if 0
/* 单倍速读flash */
SECTION_SRAM_TEXT
static int sflash_standard_read(unsigned int from, unsigned char *buf,unsigned int len)
{
	struct spi_flash *flash;

	flash = &g_flash_info;

	/* sanity checks */
	if (!len)
		return 0;

	if (from + len > flash->size)
		return -EINVAL;

	wait_bus_ready();
	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_SER);

	writel(EEPROM_8BITS_MODE, SPIM_CTRLR0);
	writel(len-1, SPIM_CTRLR1);
	writel(0 << 16, SPIM_TXFTLR);
	writel(0x0, SPIM_SPI_CTRL0);
	writel(7, SPIM_DMARDLR);
	writel(0x01, SPIM_ENR);

	writel(0x0b, SPIM_TXDR_LE);
	writel(from>>16, SPIM_TXDR_LE);
	writel(from>>8, SPIM_TXDR_LE);
	writel(from, SPIM_TXDR_LE);
	writel(0x00, SPIM_TXDR_LE);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	int dma_channel;
	DW_DMAC_CH_CONFIG dma_config;

	dma_channel = dw_dma_select_channel();
	if (dma_channel < 0) {
		reset_spi();
		return -1;
	}

	/* dma source config */
	dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
	dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;
	dma_config.src_master_select = AXI_MASTER_2;
	dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_8;
	dma_config.src_per = 0;
	dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	/* dma destination config */
	dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_INC;
	dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;

	if ((uint32_t)buf >= 0xa0100000)
		dma_config.dst_master_select = AXI_MASTER_1;
	else
		dma_config.dst_master_select = AXI_MASTER_2;

	dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
	dma_config.dst_per = 0;
	dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;
	dma_config.flow_ctrl = DWAXIDMAC_TT_FC_PER_TO_MEM_DMAC;
	dw_dma_channel_config(dma_channel, dma_config);

	dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len, dma_channel);
	writel(0x01, SPIM_DMACR);
	writel(0x01, SPIM_SER);
	dw_dma_wait_complete(dma_channel);
	writel(0x00, SPIM_DMACR);
#else
	writel(0x01, SPIM_SER);
	uint32_t i;
	for (i = 0; i < len; i++) {
		while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
		((uint8_t*)buf)[i] = 0xff & readl(SPIM_RXDR_LE);
	}
#endif
	wait_rx_done();

	return 0;
}
#endif

/* 单倍速读flash，软件cs */
SECTION_SRAM_TEXT
static int sflash_standard_sw_read_raw(unsigned int from, unsigned char *buf, unsigned int len, int is_32bit)
{
	struct spi_flash *flash;

	flash = &g_flash_info;

	/* sanity checks */
	if (!len)
		return 0;

	if (from + len > flash->size)
		return -EINVAL;

	wait_bus_ready();

	SPI_CS_LOW();

	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_SER);

	writel(STANDARD_TO_8BITS_MODE, SPIM_CTRLR0);
	writel(4, SPIM_CTRLR1);
	writel(0 << 16, SPIM_TXFTLR);
	writel(0x0, SPIM_SPI_CTRL0);
	writel(0x01, SPIM_ENR);

	writel(0x0b, SPIM_TXDR_LE);
	writel(from>>16, SPIM_TXDR_LE);
	writel(from>>8, SPIM_TXDR_LE);
	writel(from, SPIM_TXDR_LE);
	writel(0x00, SPIM_TXDR_LE);
	writel(0x01, SPIM_SER);

	wait_tx_done();
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_SER);

	if (is_32bit) {
		writel(STANDARD_RO_32BITS_MODE, SPIM_CTRLR0);
		writel(len/4-1, SPIM_CTRLR1);
	} else {
		writel(STANDARD_RO_8BITS_MODE, SPIM_CTRLR0);
		writel(len-1, SPIM_CTRLR1);
	}

	writel(0 << 16, SPIM_TXFTLR);
	writel(7, SPIM_DMARDLR);
	writel(0x01, SPIM_ENR);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	int dma_channel;
	DW_DMAC_CH_CONFIG dma_config;

	dma_channel = dw_dma_select_channel();
	if (dma_channel < 0) {
		reset_spi();
		return -1;
	}

	/* dma source config */
	dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
	dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;
	dma_config.src_master_select = AXI_MASTER_2;
	dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_8;
	dma_config.src_per = 0;

	if (is_32bit)
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	/* dma destination config */
	dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_INC;
	dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;

	if ((uint32_t)buf >= 0xa0100000)
		dma_config.dst_master_select = AXI_MASTER_1;
	else
		dma_config.dst_master_select = AXI_MASTER_2;

	dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
	dma_config.dst_per = 0;

	if (is_32bit)
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	dma_config.flow_ctrl = DWAXIDMAC_TT_FC_PER_TO_MEM_DMAC;
	dw_dma_channel_config(dma_channel, dma_config);

	if (is_32bit)
		dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len/4, dma_channel);
	else
		dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len, dma_channel);

	writel(0x01, SPIM_DMACR);
	writel(0x01, SPIM_SER);
	writel(0x00, SPIM_TXDR_LE);
	dw_dma_wait_complete(dma_channel);
	writel(0x00, SPIM_DMACR);
#else
	writel(0x01, SPIM_SER);
	writel(0x00, SPIM_TXDR_LE);
	uint32_t i;
	if (is_32bit) {
		for (i = 0; i < len/4; i++) {
			while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
			((uint32_t*)buf)[i] = readl(SPIM_RXDR_LE);
		}
	} else {
		for (i = 0; i < len; i++) {
			while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
			((uint8_t*)buf)[i] = 0xff & readl(SPIM_RXDR_LE);
		}
	}
#endif
	wait_rx_done();
	SPI_CS_HIGH();
	SPI_CS_HW();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_standard_sw_read(unsigned int from, unsigned char *buf,unsigned int len)
{
	uint32_t buf_addr = (uint32_t)buf;

	if ((buf_addr % 4) || (len % 4))
		return sflash_standard_sw_read_raw(from, buf, len, 0);
	else
		return sflash_standard_sw_read_raw(from, buf, len, 1);
}

#endif /* defined(CONFIG_MCU_FLASH_SPI_STANDARD) || defined(CONFIG_MCU_FLASH_SPI_AUTO) */

#if defined(CONFIG_MCU_FLASH_SPI_DUAL) || defined(CONFIG_MCU_FLASH_SPI_AUTO)
/* 双倍速读flash  */
SECTION_SRAM_TEXT
static int sflash_dual_read_raw(unsigned int from, unsigned char *buf, unsigned int len, int is_32bit)
{
	struct spi_flash *flash;

	flash = &g_flash_info;

	/* sanity checks */
	if (!len)
		return 0;

	if (from + len > flash->size)
		return -EINVAL;

	wait_bus_ready();

	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_SER);

	if (is_32bit) {
		writel(DUAL_RO_32BIT_MODE, SPIM_CTRLR0);
		writel(len/4-1, SPIM_CTRLR1);
	} else {
		writel(DUAL_RO_8BIT_MODE, SPIM_CTRLR0);
		writel(len-1, SPIM_CTRLR1);
	}

	writel(0 << 16, SPIM_TXFTLR);
	writel(STRETCH_WAIT8_INST8_ADDR24, SPIM_SPI_CTRL0);
	writel(7, SPIM_DMARDLR);

	writel(0x01, SPIM_ENR);

	writel(0x3b, SPIM_TXDR_BE);
	writel(from, SPIM_TXDR_BE);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	int dma_channel;
	DW_DMAC_CH_CONFIG dma_config;

	dma_channel = dw_dma_select_channel();
	if (dma_channel < 0) {
		reset_spi();
		return -1;
	}

	/* dma source config */
	dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
	dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;
	dma_config.src_master_select = AXI_MASTER_2;
	dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_8;
	dma_config.src_per = 0;

	if (is_32bit)
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;


	/* dma destination config */
	dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_INC;
	dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;

	if ((uint32_t)buf >= 0xa0100000)
		dma_config.dst_master_select = AXI_MASTER_1;
	else
		dma_config.dst_master_select = AXI_MASTER_2;

	dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
	dma_config.dst_per = 0;

	if (is_32bit)
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	dma_config.flow_ctrl = DWAXIDMAC_TT_FC_PER_TO_MEM_DMAC;
	dw_dma_channel_config(dma_channel, dma_config);

	if (is_32bit)
		dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len/4, dma_channel);
	else
		dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len, dma_channel);

	writel(0x01, SPIM_SER);
	writel(0x01, SPIM_DMACR);
	dw_dma_wait_complete(dma_channel);
	writel(0x00, SPIM_DMACR);
#else
	uint32_t i;
	writel(0x01, SPIM_SER);
	if (is_32bit) {
		for (i = 0; i < len/4; i++) {
			while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
			((uint32_t*)buf)[i] = readl(SPIM_RXDR_LE);
		}
	} else {
		for (i = 0; i < len; i++) {
			while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
			((uint8_t*)buf)[i] = readl(SPIM_RXDR_LE);
		}
	}
#endif
	wait_rx_done();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_dual_read(unsigned int from, unsigned char *buf,unsigned int len)
{
	uint32_t buf_addr = (uint32_t)buf;

	if ((buf_addr % 4) || (len % 4))
		return sflash_dual_read_raw(from, buf, len, 0);
	else
		return sflash_dual_read_raw(from, buf, len, 1);
}

#endif /* defined(CONFIG_MCU_FLASH_SPI_DUAL) || defined(CONFIG_MCU_FLASH_SPI_AUTO) */

#if defined(CONFIG_MCU_FLASH_SPI_QUAD) || defined(CONFIG_MCU_FLASH_SPI_AUTO)
/* 四倍速读flash */
SECTION_SRAM_TEXT
static int sflash_quad_read_raw(unsigned int from, unsigned char *buf, unsigned int len, int is_32bit)
{
	struct spi_flash *flash;

	flash = &g_flash_info;

	/* sanity checks */
	if (!len)
		return 0;

	if (from + len > flash->size)
		return -EINVAL;

	wait_bus_ready();

	writel(0x00, SPIM_DMACR);
	writel(0x00, SPIM_ENR);
	writel(0x00, SPIM_SER);

	if (is_32bit) {
		writel(QUAD_RO_32BITS_MODE, SPIM_CTRLR0);
		writel(len/4-1, SPIM_CTRLR1);
	} else {
		writel(QUAD_RO_8BITS_MODE, SPIM_CTRLR0);
		writel(len-1, SPIM_CTRLR1);
	}

	writel(0 << 16, SPIM_TXFTLR);
	writel(STRETCH_WAIT8_INST8_ADDR24, SPIM_SPI_CTRL0);
	writel(7, SPIM_DMARDLR);

	writel(0x01, SPIM_ENR);

	writel(0x6b, SPIM_TXDR_BE);
	writel(from, SPIM_TXDR_BE);

#ifdef CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	int dma_channel;
	DW_DMAC_CH_CONFIG dma_config;

	dma_channel = dw_dma_select_channel();
	if (dma_channel < 0) {
		reset_spi();
		return -1;
	}

	/* dma source config */
	dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
	dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;
	dma_config.src_master_select = AXI_MASTER_2;
	dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_8;
	dma_config.src_per = 0;

	if (is_32bit)
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	/* dma destination config */
	dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_INC;
	dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;

	if ((uint32_t)buf >= 0xa0100000)
		dma_config.dst_master_select = AXI_MASTER_1;
	else
		dma_config.dst_master_select = AXI_MASTER_2;

	dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
	dma_config.dst_per = 0;

	if (is_32bit)
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_32;
	else
		dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

	dma_config.flow_ctrl = DWAXIDMAC_TT_FC_PER_TO_MEM_DMAC;
	dw_dma_channel_config(dma_channel, dma_config);

	if (is_32bit)
		dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len/4, dma_channel);
	else
		dw_dma_xfer((void*)buf, (void*)SPIM_RXDR_LE, len, dma_channel);

	writel(0x01, SPIM_SER);
	writel(0x01, SPIM_DMACR);
	dw_dma_wait_complete(dma_channel);
	writel(0x00, SPIM_DMACR);

#else /* not define CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH */
	uint32_t i;
	writel(0x01, SPIM_SER);
	if (is_32bit) {
		for (i = 0; i < len/4; i++) {
			while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
			((uint32_t*)buf)[i] = readl(SPIM_RXDR_LE);
		}
	} else {
		for (i = 0; i < len; i++) {
			while(!(readl(SPIM_SR) & SR_RF_NOT_EMPT));
			((uint8_t*)buf)[i] = readl(SPIM_RXDR_LE);
		}
	}
#endif /* CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH */
	wait_rx_done();

	return 0;
}

SECTION_SRAM_TEXT
static int sflash_quad_read(unsigned int from, unsigned char *buf,unsigned int len)
{
	uint32_t buf_addr = (uint32_t)buf;

	if ((buf_addr % 4) || (len % 4))
		return sflash_quad_read_raw(from, buf, len, 0);
	else
		return sflash_quad_read_raw(from, buf, len, 1);
}
#endif /* defined(CONFIG_MCU_FLASH_SPI_QUAD) || defined(CONFIG_MCU_FLASH_SPI_AUTO) */

SECTION_SRAM_TEXT
static int sflash_readdata(unsigned int addr, unsigned char *to, unsigned int len)
{
	unsigned int size;

	wait_till_ready();

	while(len != 0) {
		size = min(0x10000, len);
		g_flash_info.read_data(addr, to, size);
		len -= size;
		addr += size;
		to += size;
	}

	return 0;
}


SECTION_SRAM_TEXT
static char* sflash_gettype(void)
{
	return g_flash_info.info->name;
}

static int sflash_getsize(enum spi_flash_info flash_info)
{
	switch(flash_info){
	case SPI_FLASH_SIZE_CHIP:
		return g_flash_info.size;

	case SPI_FLASH_SIZE_BLOCK:
	case SPI_FLASH_SIZE_SECTOR:
		return FLASH_ERASE_SIZE;

	case SPI_FLASH_SIZE_PAGE:
		return FLASH_PAGE_SIZE;

	case SPI_FLASH_NUM_BLOCK:
	case SPI_FLASH_NUM_SECTOR:
		return g_flash_info.size/FLASH_ERASE_SIZE;

	default: break;
	}

	return -1;
}

SECTION_SRAM_TEXT
static int sflash_calcblock(unsigned int addr, unsigned int *pstart, unsigned int *pend)
{
	int i;
	unsigned int start = 0, end = 0;
	struct spi_flash *flash;

	flash = &g_flash_info;
	*pend = 0;
	*pstart = 0;

	if (flash->index >= 0) {
		for (i = 0; i < flash->size/FLASH_ERASE_SIZE; ++i) {
			end += FLASH_ERASE_SIZE;
			if (addr >= start && addr < end) {
				*pstart = start;
				*pend = end;
				return 0;
			}
			start += FLASH_ERASE_SIZE;
		}
	}

	return -1;
}

SECTION_SRAM_TEXT
static int sflash_erasedata(unsigned int addr, unsigned int len)
{
	unsigned int erase_start, erase_count, erase_end;
	struct spi_flash *flash;

	flash = &g_flash_info;

	if(addr >= flash->size)
		return -EINVAL;

	erase_end = addr + len;
	if(erase_end > flash->size)
		erase_end = flash->size;

	erase_start = (addr / FLASH_ERASE_SIZE) * FLASH_ERASE_SIZE;
	erase_count = erase_end - erase_start;

	while(erase_count > 0) {
		if (erase_count < FLASH_ERASE_SIZE)
			erase_count = FLASH_ERASE_SIZE;
#ifdef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
		if (((erase_start % FLASH_BLOCK_SIZE)==0) && (erase_count >= FLASH_BLOCK_SIZE)) { // block 对齐
			sflash_block_erase(flash, erase_start);
			erase_start += FLASH_BLOCK_SIZE;
			erase_count -= FLASH_BLOCK_SIZE;
		}
		else {
			sflash_sector_erase(flash, erase_start);
			erase_start += FLASH_SECTOR_SIZE;
			erase_count -= FLASH_SECTOR_SIZE;
		}
#else
		sflash_block_erase(flash, erase_start);
		erase_start += FLASH_BLOCK_SIZE;
		erase_count -= FLASH_BLOCK_SIZE;
#endif
	}

	return 0;
}


// lock the flash start from address of 0 to protect_addr.
SECTION_SRAM_TEXT
static int sflash_write_protect_lock(unsigned int protect_addr)
{
	unsigned char dev_status, tb = 0, bp3 = 0, bp2_bp0 = 0x0;

	if(0x0 == protect_addr){
		bp3          = 0x0;
		bp2_bp0      = 0x0;
		tb           = 0x0;
	}
	else
	{
		printf("Not support write protect\n");
		return -1;
	}

	dev_status  = read_sr1();

	dev_status &=~0x7c;
	dev_status |=((bp2_bp0<<2) | (tb << 5 ) | ( bp3 <<6) | (1<<7));

	write_sr(&dev_status, 1, 1);

	return 0;
}

// unlock the flash start from address of 0 to flashsize-1.
SECTION_SRAM_TEXT
static int sflash_write_protect_unlock(void)
{
	return sflash_write_protect_lock(0);
}

SECTION_SRAM_TEXT
static void sflash_calcblockrange(unsigned int addr, unsigned int len, unsigned int *pstart, unsigned int *pend)
{
	unsigned int dummy;

	if (sflash_calcblock(addr, pstart, &dummy) == 0)
		if (sflash_calcblock(addr + len - 1, &dummy, pend) == 0)
			return ;
}

SECTION_SRAM_TEXT
static void sflash_sync(void)
{
	wait_till_ready();
}

SECTION_SRAM_TEXT
static int flash_spi_irq_handler(int irq, void *pdata)
{
	uint32_t int_status = readl(SPIM_ISR);
	if (int_status & SPI_INT_TXEI)
		printf("Transmit FIFO empty interrupt.\n");
	if (int_status & SPI_INT_TXOI) {
		printf("Transmit FIFO overflow interrupt.\n");
		readl(DW_SPI_TXOICR);
	}
	if (int_status & SPI_INT_RXUI)
		printf("Receive FIFO underflow interrupt.\n");
	if (int_status & SPI_INT_RXOI) {
		printf("Receive FIFO overflow interrupt.\n");
		readl(DW_SPI_RXOICR);
	}
	if (int_status & SPI_INT_RXFI)
		printf("Receive FIFO full interrupt.\n");
	if (int_status & SPI_INT_MSTI)
		printf("multi-master contention interrupt.\n");
	if (int_status & SPI_INT_XRXOI)
		printf("xip receive fifo overflow interrupt.\n");

	return 0;
}

SECTION_SRAM_TEXT
static struct flash_dev * sflash_init(struct spi_device *spi_device)
{
	uint32_t div = CONFIG_FLASH_SPI_CLK_SRC / spi_device->freq;

	div = div < 2 ? 2 : div;
	spi_device->priv.baudr= roundup(div, 2);

	wait_bus_ready();

	/* FLASH SPI 使用硬件CS */
	SPI_CS_HW();

	writel(0x0, SPIM_ENR);
	writel(SPI_INT_TXOI | SPI_INT_RXOI | SPI_INT_RXUI | SPI_INT_XRXOI, SPIM_IMR);
	writel(spi_device->sample_dly, SPIM_SAMPLE_DLY);

	/* 读取Flash ID时，配置为较低频率 */
	writel(spi_device->priv.baudr * 2, SPIM_BAUDR);
	writel(0x1f, SPIM_RXFTLR);
	writel(0x1, SPIM_ENR);

	gx_request_irq(MCU_IRQ_DW_SPI, flash_spi_irq_handler, NULL);

	if (sflash_detect(&g_flash_info) != 0)
		return NULL;

	writel(0x0, SPIM_ENR);
	writel(spi_device->priv.baudr, SPIM_BAUDR);

#if defined(CONFIG_MCU_FLASH_SPI_STANDARD)
	g_flash_info.program_page = sflash_standard_1pageprogram;
	g_flash_info.read_data = sflash_standard_sw_read;
#elif defined(CONFIG_MCU_FLASH_SPI_DUAL)
	g_flash_info.program_page = sflash_standard_1pageprogram;
	g_flash_info.read_data = sflash_dual_read;
	xip_init(0xbb, 8, 1,
			24, 2,
			0x00, 1,
			0, 2);
#elif defined(CONFIG_MCU_FLASH_SPI_QUAD)
	sflash_enable_quad(g_flash_info.info->jedec_id);

	g_flash_info.program_page = sflash_quad_1pageprogram;
	g_flash_info.read_data = sflash_quad_read;
	xip_init(0xeb, 8, 1,
			24, 4,
			0x00, 1,
			4, 4);
#else
	if (g_flash_info.info->flag & QUAD_READ) {
		sflash_enable_quad(g_flash_info.info->jedec_id);

		g_flash_info.program_page = sflash_quad_1pageprogram;
		g_flash_info.read_data = sflash_quad_read;
		xip_init(0xeb, 8, 1,
				24, 4,
				0x00, 1,
				4, 4);
	}
	else if (g_flash_info.info->flag & DUAL_READ) {
		g_flash_info.program_page = sflash_standard_1pageprogram;
		g_flash_info.read_data = sflash_dual_read;
		xip_init(0xbb, 8, 1,
				24, 2,
				0x00, 1,
				0, 2);
	}
	else {
		g_flash_info.program_page = sflash_standard_1pageprogram;
		g_flash_info.read_data = sflash_standard_sw_read;
	}
#endif

	return &flash_spi_dev;
}

//=======================================================================================
SECTION_SRAM_TEXT
int xip_init(unsigned int cmd,  unsigned int cmd_len,  unsigned int cmd_mode,
			unsigned int addr_len, unsigned int addr_mode,
			unsigned int mode_code, unsigned int mode_code_enable,
			unsigned int wait_cycles, unsigned int spi_mode)
{
	unsigned int cmd_addr_mode = XIP_STANDARD;
	unsigned int reg_data      = 0;

	writel(0x00, SPIM_ENR);

	cmd_mode  = cmd_mode  >> 1;
	addr_mode = addr_mode >> 1;
	spi_mode  = spi_mode  >> 1;

	if (cmd_len == 0)
		cmd_len = XIP_INST_L0;
	else if (cmd_len == 4)
		cmd_len = XIP_INST_L4;
	else if (cmd_len == 8)
		cmd_len = XIP_INST_L8;
	else if (cmd_len == 16)
		cmd_len = XIP_INST_L16;
	else {
		printf("Spi does not support this cmd len\n");
		return -1;
	}

	if ((cmd_mode == XIP_STANDARD) && (addr_mode == XIP_STANDARD))
		cmd_addr_mode = XIP_TRANS_TYPE_TT0;
	else if ((cmd_mode == XIP_STANDARD) && (addr_mode == spi_mode))
		cmd_addr_mode = XIP_TRANS_TYPE_TT1;
	else if ((cmd_mode == spi_mode) && (addr_mode == spi_mode))
		cmd_addr_mode = XIP_TRANS_TYPE_TT2;
	else{
		printf("Spi does not support this configuration\n");
		return -1;
	}

	if (addr_len % 4) {
		printf("addr_len error\n");
		return -1;
	}

	// dsp在复杂case下预取存在异常
	// reg_data |= 1 << XIP_PREFETCH_EN;
	reg_data |= 1 << XIP_CONT_XFER_EN;
	reg_data |= 1 << XIP_INST_EN;
	reg_data |= wait_cycles << XIP_WAIT_CYCLES;
	reg_data |= cmd_len << XIP_INST_L;
	reg_data |= (addr_len / 4) << XIP_ADDR_L;
	reg_data |= spi_mode << XIP_FRF;
	reg_data |= cmd_addr_mode << XIP_TRANS_TYPE;

	// 模式位设置
	if (mode_code_enable == 1) {
		reg_data |= MBL_8 << XIP_MBL;
		reg_data |= 1 << XIP_MD_BITS_EN;
	}

	writel(reg_data, XIP_CTRL);
	writel(1,        XIP_SER);
	writel(cmd,      XIP_INCR_INST);
	writel(cmd,      XIP_WRAP_INST);
	writel(0,        FLASH_SPI_CS_REG);
	writel(0x01,     SPIM_ENR);

	return 0;
}

SECTION_SRAM_RODATA
struct flash_dev flash_spi_dev = {
	.init                = sflash_init,
	.readdata            = sflash_readdata,
	.gettype             = sflash_gettype,
	.getsize             = sflash_getsize,
	.write_protect_mode  = NULL,
	.write_protect_status= NULL,
	.write_protect_lock  = sflash_write_protect_lock,
	.write_protect_unlock= sflash_write_protect_unlock,
	.chiperase           = sflash_chiperase,
	.erasedata           = sflash_erasedata,
	.pageprogram         = sflash_pageprogram,
	.sync                = sflash_sync,
	.test                = NULL,
	.calcblockrange      = sflash_calcblockrange,
	.badinfo             = NULL,
	.pageprogram_yaffs2  = NULL,
	.readoob             = NULL,
	.writeoob            = NULL,
	.otp_lock            = NULL,
	.otp_status          = NULL,
	.otp_erase           = NULL,
	.otp_read            = NULL,
	.otp_write           = NULL,
	.otp_get_region      = NULL,
	.otp_set_region      = NULL
};

