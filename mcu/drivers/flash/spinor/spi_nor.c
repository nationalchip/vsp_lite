/*****************************************
  Copyright (c) 2003-2019
  Nationalchip Science & Technology Co., Ltd. All Rights Reserved
  Proprietary and Confidential
 *****************************************/

#include <autoconf.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <util.h>
#include <stdarg.h>
#include <driver/delay.h>
#include <driver/spi_device.h>
#include <driver/flash.h>
#include "spi_nor.h"

#ifdef CONFIG_MTD_DEBUG
#define SFLASH_DBG(...)                printf( __VA_ARGS__)
#else
#define SFLASH_DBG(...)
#endif

#ifndef FLASH_BLOCK_SIZE
#define FLASH_BLOCK_SIZE 0x10000
#endif
#define FLASH_SECTOR_SIZE 0x1000
#define FLASH_PAGE_SIZE 0x100

#ifdef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
#define FLASH_ERASE_SIZE FLASH_SECTOR_SIZE
#else
#define FLASH_ERASE_SIZE FLASH_BLOCK_SIZE
#endif

#define CMD_SIZE        6
typedef struct sflash_otp_info{
	unsigned int    start_addr;
	unsigned int    skip_addr;
	unsigned int    region_size;
	unsigned int    region_num;
	unsigned int    flags;
}sflash_otp_info;

struct spi_flash
{
	int             index;
	unsigned int    size;
	unsigned int    addr_width;
	sflash_info     *info;
	sflash_otp_info otp;
	unsigned char   command[CMD_SIZE];
	struct spi_device* spi_device;
};
static struct spi_flash g_flash_info = { -1,0,0,NULL};

static sflash_info sflash_data[] = {
	//en25qxx spi flash, eon silicon solution
	{"en25f16",    0x001c3115,   0x200000,   0 },            // en25f16-100HIP 16 MBit devices.
	{"en25q16",    0x001c3015,   0x200000,   0 },            // en25q16 16 MBit devices.
	{"en25qh16",   0x001c7015,   0x200000,   DUAL_READ },    // en25qh16-104HIP 16 MBit devices.
	{"en25f32",    0x001c3116,   0x400000,   0 },            // en25f32 32 MBit devices.
	{"en25q32",    0x001c3016,   0x400000,   DUAL_READ },    // en25q32b-104HIP 32 MBit devices.
	{"en25qh32b",  0x001c7016,   0x400000,   DUAL_READ | QUAD_READ }, // en25qh32b-104HIP 32 MBit devices.
	{"en25q64",    0x001c3017,   0x800000,   DUAL_READ },    // en25q64-104HIP 64 MBit devices.
	{"en25q128",   0x001c3018,   0x1000000,  0 },            // en25q128_d 128 MBit devices.
	{"en25qh128",  0x001c7018,   0x1000000,  DUAL_READ  },   // en25qh128_d 128 MBit devices.

	//f25lxx spi flash, elite semiconductor
	{"f25l016",    0x008c2115,   0x200000,   0 },            // f25l16pa  16 MBit devices.
	{"f25l032",    0x008c2116,   0x400000,   DUAL_READ  },   // f25l032qa 32 MBit devices.

	//gd25xx spi flash, elite semiconductor
	{"gd25q16",    0x00c84015,   0x200000,   DUAL_READ  },   // gd25q16 16 MBit devices.
	{"gd25ve16c",  0x00c84215,   0x200000,   DUAL_READ | QUAD_READ },
	{"gd25q32",    0x00c84016,   0x400000,   DUAL_READ  },   // gd25q32 32 MBit devices.
	{"gd25q64",    0x00c84017,   0x800000,   DUAL_READ  },   // gd25q64 64 MBit devices.
	{"gd25q128",   0x00c84018,   0x1000000,  0 },            // gd25q128 128 MBit devices.

	//m25pxx spi flash, micron(numonyx)
#ifndef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
	{"m25p016",    0x00202015,   0x200000,   0 },            // flash m25p16 16MBit devices.
	{"m25p128",    0x00202018,   0x1000000,  0 },            // flash m25p128 128MBit devices.
#endif
	{"n25q032",    0x0020ba16,   0x400000,   DUAL_READ  },   // flash n25q032pa 32MBit devices.
	{"n25q064",    0x0020ba17,   0x800000,   DUAL_READ  },   // flash n25q064 64MBit devices.
	{"n25q128",    0x0020ba18,   0x1000000,  DUAL_READ },    // flash n25q128 128MBit devices.

	//mx25lxx spi flash,MXIC
	{"mx25l16",    0x00c22015,   0x200000,   0 },            // mx25l1606E 16 MBit devices.
	{"mx25l32",    0x00c22016,   0x400000,   DUAL_READ  },   // mx25l3206E 32 MBit devices.
	{"mx25l64",    0x00c22017,   0x800000,   DUAL_READ  },   // mx25l6406E 64 MBit devices.
	{"mx25l128",   0x00c22018,   0x1000000,  DUAL_READ  },   // mx25l12835E 128 MBit devices.
	{"mx25l256",   0x00c22019,   0x2000000,  DUAL_READ  },   // mx25l25635F 256 MBit devices.

	//w25qxx spi flash, winbond
	{"w25q16",     0x00ef4015,   0x200000,   DUAL_READ  },   // w25q16bv /W25Q16 16 MBit devices.
	{"w25q32",     0x00ef4016,   0x400000,   DUAL_READ  },   // w25q32bv /W25Q32 32 MBit devices.
	{"w25q32dw",   0x00ef6016,   0x400000,   DUAL_READ  },   // w25q32dw 32 MBit devices.
	{"w25q64",     0x00ef4017,   0x800000,   DUAL_READ  },   // w25q64bv /W25Q64 64 MBit devices.
	{"w25q128",    0x00ef4018,   0x1000000,  DUAL_READ  },   // w25q128bv /W25Q128 128 MBit devices.
	{"w25q256",    0x00ef4019,   0x2000000,  DUAL_READ  },   // w25q256bv /W25Q256 256 MBit devices.

	//s25flxx spi flash, spansion
#ifndef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
	{"s25fl032p",   0x00010215,   0x400000,   0 },            // s25fl032p 32 MBit devices.
	{"s25fl064p",   0x00010216,   0x800000,   0 },            // s25fl064p 64 MBit devices.
	{"s25fl128p",   0x00012018,   0x1000000,  0 },            // s25fl128p 128 MBit devices.
	{"s25fl256s",   0x00010219,   0x2000000,  0 },            // s25fl256s 128 MBit devices.
#endif

	//pm25lqxx spi flash, pmc
	{"pm25lq032",  0x007f9d46,   0x400000,   DUAL_READ  },   // pm25lq032  32 MBit devices.

	/* paragon */
	{"pn25f32",    0x00e04016,   0x400000,   DUAL_READ  },   // pm25f32  32 MBit devices.

	/* issi */
	{"ic25lp064",  0x009d6017,   0x800000,   DUAL_READ  },   // ic25lp064  64 MBit devices.

	/* 芯天下 */
	{"xt25f04b",   0x000b4013,   0x80000,    0 },            // xt25f04b  4 MBit devices.
	{"xt25f08b",   0x000b4014,   0x100000,   0 },            // xt25f08b  8 MBit devices.
	{"xt25f16b",   0x000b4015,   0x200000,   0 },            // xt25f16b  16 MBit devices.
	{"xt25f32b",   0x000b4016,   0x400000,   0 },            // xt25f32b  32 MBit devices.
	{"xt25f64b",   0x000b4017,   0x800000,   0 },            // xt25f64b  64 MBit devices.

	/* Boya Microelectronics */
	{"by25q32bs",  0x00684016,   0x400000,   0 },            // by25q32bs 32 Mbit devices.
	{"by25q64as",  0x00684017,   0x800000,   0 },            // by25q64as 64 Mbit devices.

	/* yichu technology */
	{"YC25Q16W",   0x005e4015,   0x200000,   0 },            // YC25Q16W  16 Mbit devices.
	{"YC25Q16P",   0x00856015,   0x200000,   0 },            // YC25Q16P  16 Mbit devices.

	/* Wuhan Xinxin */
	{"xm25qh16c",  0x00204015,   0x200000,   0 },            // XM25QH16C 16Mbit devices.
	{"xm25qh32b",  0x00204016,   0x400000,   0 },            // XM25QH32B 32Mbit devices.

	/*--- Zbit ---*/
	{"zb25vq16a",  0x005e6015,   0x200000,   0 },            // ZB25VQ16A 16Mbit devices.
	{"zb25vq32b",  0x005e4016,   0x400000,   0 },            // ZB25VQ32B 32Mbit devices.

	{NULL,         0x00,         0x00,       0 },            // end
};

static int xfer_cmd(xfer_cmd_t *xfer)
{
	struct spi_flash *flash;
	flash = &g_flash_info;

	flash->spi_device->set_cs(CS_ENABLE);

	spi_xfer(flash->spi_device, xfer->cmd, NULL, xfer->cmd_len);

	if(xfer->xfer_len)
		spi_xfer(flash->spi_device, xfer->tx, xfer->rx, xfer->xfer_len);

	flash->spi_device->set_cs(CS_DISABLE);

	return 0;
}

static int xfer_data(uint8_t *data, int len)
{
	struct spi_flash *flash;
	flash = &g_flash_info;

	flash->spi_device->set_cs(CS_ENABLE);

	spi_xfer(flash->spi_device, data, NULL, len);

	flash->spi_device->set_cs(CS_DISABLE);

	return 0;
}

/*
 * Read the status register 1, returning its value in the location
 * Return the status register value.
 * Returns negative if error occurred.
 */
static int read_sr1(uint8_t* status)
{
	xfer_cmd(&(xfer_cmd_t){
			.cmd_len = 1,
			.cmd = {GX_CMD_RDSR1},
			.xfer_len = 1,
			.rx = status
			});

	return 0;
}

/*
 * Read the status register 2, returning its value in the location
 * Return the status register value.
 * Returns negative if error occurred.
 */
static int read_sr2(uint8_t* status)
{
	xfer_cmd(&(xfer_cmd_t){
			.cmd_len = 1,
			.cmd = {GX_CMD_RDSR2},
			.xfer_len = 1,
			.rx = status
			});

	return 0;
}

/*
 * Set write enable latch with Write Enable command.
 * Returns negative if error occurred.
 */
static int write_enable(void)
{
	xfer_cmd(&(xfer_cmd_t){
			.cmd_len = 1,
			.cmd = {GX_CMD_WREN},
			.xfer_len = 0
			});

	return 0;
}

/*
 * Service routine to read status register until ready, or timeout occurs.
 * Returns non-zero if error.
 */
static int wait_till_ready(void)
{
	uint8_t sr;

	do{
		read_sr1(&sr);
	}while (sr & GX_STAT_WIP);

	return 1;
}

// Write 8bit or 16bit status register
static int write_sr(u8 *buf, int len, u8 reg_order)
{
	uint8_t tx_buf[3] = {0 ,0 ,0};

	if(len > 2)
		return -1;

	switch(reg_order)
	{
		case 1:
			tx_buf[0] = GX_CMD_WRSR1;
			break;
		case 2:
			tx_buf[0] = GX_CMD_WRSR2;
			break;
		case 3:
			tx_buf[0] = GX_CMD_WRSR3;
			break;
		default:
			break;
	}

	tx_buf[1] = (u8)(*buf++);
	if(len == 2)
		tx_buf[2] = (u8)(*buf++);

	wait_till_ready();
	write_enable();
	xfer_data(tx_buf, len + 1);

	return 0;
}

static int sflash_readid(int * jedec)
{
	uint8_t sr;
	uint8_t id[3];
	unsigned long long time_start;

	time_start = get_time_us();

	while(1){
		read_sr1(&sr);
		if(!(sr & GX_STAT_WIP))
			break;

		if((get_time_us() - time_start) >= SFLASH_READY_TIMEOUT){
			printf("waitting timeout in sflash_readid()\n");
			return -1;
		}
	}

	xfer_cmd(&(xfer_cmd_t){
			.cmd_len = 1,
			.cmd = {GX_CMD_READID},
			.xfer_len = 3,
			.rx = id
			});

	*jedec = id[0];
	*jedec = *jedec << 8;
	*jedec |= id[1];
	*jedec = *jedec << 8;
	*jedec |= id[2];

	return 0;
}

/*
 * Enable/disable 4-byte addressing mode.
 */
static inline int set_4byte(struct spi_flash *flash, int enable)
{
	uint32_t jedec_id=flash->info->jedec_id;

	switch (jedec_id>>16) {
		case 0xC2 /* MXIC */:
		case 0xEF /* winbond */:
			flash->command[0] = enable ? GX_CMD_EN4B : GX_CMD_EX4B;
			if(enable)
			{
				return xfer_data(flash->command, 1);
			}
			else
			{
				xfer_data(flash->command, 1);
				flash->command[0] = GX_CMD_WREN;
				xfer_data(flash->command, 1);
				flash->command[0] = GX_CMD_WREAR;
				flash->command[1] = 0x00;
				xfer_data(flash->command, 2);
				return 0;
			}
		default:
			/* Spansion style */
			flash->command[0] = GX_CMD_BRWR;
			flash->command[1] = enable << 7;
			return xfer_data(flash->command, 2);
	}
}

//switch from 4-Byte to 3-Byte Address Mode
void sflash_exit_4byte(void)
{
	struct spi_flash *flash;

	flash = &g_flash_info;

	if(flash->size > 0x1000000)
	{
		set_4byte(flash, 0);
	}
}

static void sflash_addr2cmd(struct spi_flash *flash, unsigned int addr, unsigned char *cmd)
{
	/* opcode is in cmd[0] */
	cmd[1] = addr >> (flash->addr_width * 8 -  8);
	cmd[2] = addr >> (flash->addr_width * 8 - 16);
	cmd[3] = addr >> (flash->addr_width * 8 - 24);
	cmd[4] = addr >> (flash->addr_width * 8 - 32);
}

static inline int sflash_cmdsz(struct spi_flash *flash)
{
	return 1 + flash->addr_width;
}

static int sflash_detect(struct spi_flash *flash)
{
	int  i        = 0;
	int  jedec    = 0;

	flash->index  = -1;

	if(sflash_readid(&jedec) != 0)
		return -1;

	for(i = 0; sflash_data[i].jedec_id; i++){
		if(jedec == sflash_data[i].jedec_id){
			flash->index = i;
			flash->info  = &sflash_data[i];
			flash->size  = sflash_data[i].size;
			flash->addr_width = (flash->size > 0x1000000) ? (set_4byte(flash, 1), 4) : 3;
			break;
		}
	}

	if (flash->index < 0){
		for(i = 0; sflash_data[i].jedec_id; i++){
			//if the spi flash not supported/exit, we assume it is SFLASH_MX_25L3205 to test
			if(0x00c22016 == sflash_data[i].jedec_id){
				flash->index = i;
				flash->size = sflash_data[i].size;
				flash->info= &sflash_data[i];
				flash->addr_width = 3;
				break;
			}
		}
#ifndef CONFIG_BOOT_TOOL
		printf("\r\n Warning Spi Flash unsupported, please info nationalchip\r\n");
		printf("\r\n jedec_id = %x\r\n", jedec);
#endif
		return -2;
	}

	return 0;
}


static int sflash_chiperase(void)
{
	uint8_t command[1];

	wait_till_ready();
	write_enable();

	command[0] = GX_CMD_CE;
	xfer_data(command, 1);

	wait_till_ready();
	command[0] = GX_CMD_WRDI;
	xfer_data(command, 1);

	return 0;
}

#ifdef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
static void sflash_sector_erase(struct spi_flash *flash, unsigned int addr)
{
	wait_till_ready();
	write_enable();

	flash->command[0]=GX_CMD_SE;
	sflash_addr2cmd(flash,addr,flash->command);
	xfer_data(flash->command, sflash_cmdsz(flash));
}
#endif

static void sflash_block_erase(struct spi_flash *flash, unsigned int addr)
{
	wait_till_ready();
	write_enable();

	flash->command[0]=GX_CMD_BE;
	sflash_addr2cmd(flash,addr,flash->command);
	xfer_data(flash->command, sflash_cmdsz(flash));
}

/*
 * Write an address range to the flash chip.  Data must be written in
 * GX_PAGESIZE chunks.  The address range may be any size provided
 * it is within the physical boundaries.
 */
static int sflash_pageprogram(uint32_t to, uint8_t *buf, uint32_t len)
{
	uint32_t page_offset, page_size;
	struct spi_flash *flash;
	xfer_cmd_t  xfer;

	flash = &g_flash_info;

	/* sanity checks */
	if (!len)
		return(0);

	if (to + len > flash->size)
		return -EINVAL;

	/* Wait until finished previous write command. */
	wait_till_ready();

	write_enable();

	/* Set up the opcode in the write buffer. */

	// 如果是16Mbit 则用4字节地址命令
	if (to + len <= 0x1000000) {
		xfer.cmd_len = 4;
		xfer.cmd[0] = GX_CMD_PP;
	}else{
		xfer.cmd_len = 5;
		xfer.cmd[0] = GX_CMD_PP4;
	}
	sflash_addr2cmd(flash, to, xfer.cmd);
	xfer.xfer_len = len;
	xfer.tx = buf;
	xfer.rx = NULL;

	/* what page do we start with? */
	page_offset = to % GX_PAGESIZE;

	/* do all the bytes fit onto one page? */
	if (page_offset + len <= GX_PAGESIZE) {

		xfer_cmd(&xfer);

	} else {
		uint32_t i;

		/* the size of data remaining on the first page */
		page_size = GX_PAGESIZE - page_offset;

		xfer.xfer_len = page_size;
		xfer_cmd(&xfer);

		/* write everything in PAGESIZE chunks */
		for (i = page_size; i < len; i += page_size) {
			page_size = len - i;
			if (page_size > GX_PAGESIZE)
				page_size = GX_PAGESIZE;

			/* write the next page to flash */
			sflash_addr2cmd(flash, to + i, xfer.cmd);
			xfer.xfer_len = page_size;
			xfer.tx = buf + i;

			wait_till_ready();
			write_enable();
			xfer_cmd(&xfer);
		}
	}

	return 0;
}

/*
 * Read an address range from the flash chip.  The address range
 * may be any size provided it is within the physical boundaries.
 */
static int sflash_fread(uint32_t from, uint8_t *buf, uint32_t len)
{
	struct spi_flash *flash;
	xfer_cmd_t xfer;

	flash = &g_flash_info;

	/* sanity checks */
	if (!len)
		return 0;

	if (from + len > flash->size)
		return -EINVAL;

	if (flash->addr_width == 3) {
		xfer.cmd_len = 5,
		xfer.cmd[0]  = GX_CMD_FAST_READ;
		xfer.cmd[1]  = 0xff & (from >> 16);
		xfer.cmd[2]  = 0xff & (from >> 8 );
		xfer.cmd[3]  = 0xff & (from >> 0 );
		xfer.cmd[4]  = 0xff;	//dummy
	} else {
		xfer.cmd_len = 6,
		xfer.cmd[0]  = GX_CMD_FAST_READ;
		xfer.cmd[1]  = 0xff & (from >> 24);
		xfer.cmd[2]  = 0xff & (from >> 16);
		xfer.cmd[3]  = 0xff & (from >> 8 );
		xfer.cmd[4]  = 0xff & (from >> 0 );
		xfer.cmd[5]  = 0xff;	//dummy
	}

	xfer.xfer_len    = len;
	xfer.tx          = NULL;
	xfer.rx          = (uint8_t*)buf;

	wait_till_ready();
	xfer_cmd(&xfer);

	return 0;
}

static int sflash_readdata( unsigned int addr,unsigned char *to, unsigned int len)
{
	sflash_fread(addr, to, len);
	return 0;
}


static char * sflash_gettype(void)
{
	return g_flash_info.info->name;
}

static int sflash_getsize(enum spi_flash_info flash_info)
{
	switch(flash_info){
	case SPI_FLASH_SIZE_CHIP:
		return g_flash_info.size;

	case SPI_FLASH_SIZE_BLOCK:
	case SPI_FLASH_SIZE_SECTOR:
		return FLASH_ERASE_SIZE;

	case SPI_FLASH_SIZE_PAGE:
		return FLASH_PAGE_SIZE;

	case SPI_FLASH_NUM_BLOCK:
	case SPI_FLASH_NUM_SECTOR:
		return g_flash_info.size/FLASH_ERASE_SIZE;

	default: break;
	}
	return -1;
}

static int sflash_calcblock(unsigned int addr, unsigned int *pstart, unsigned int *pend)
{
	int i;
	unsigned int start = 0, end = 0;
	struct spi_flash *flash;

	flash = &g_flash_info;
	*pend = 0;
	*pstart = 0;

	if (flash->index >= 0) {
		for (i = 0; i < flash->size/FLASH_ERASE_SIZE; ++i) {
			end += FLASH_ERASE_SIZE;
			if (addr >= start && addr < end) {
				*pstart = start;
				*pend = end;
				return 0;
			}
			start += FLASH_ERASE_SIZE;
		}
	}

	return -1;
}

static int sflash_erasedata(unsigned int addr, unsigned int len)
{
	unsigned int erase_start, erase_count, erase_end;
	struct spi_flash *flash;

	flash = &g_flash_info;

	if(addr >= flash->size)
		return -EINVAL;

	erase_end = addr + len;
	if(erase_end > flash->size)
		erase_end = flash->size;

	erase_start = (addr / FLASH_ERASE_SIZE) * FLASH_ERASE_SIZE;
	erase_count = erase_end - erase_start;

	while(erase_count > 0) {
		if (erase_count < FLASH_ERASE_SIZE)
			erase_count = FLASH_ERASE_SIZE;
#ifdef CONFIG_MCU_ENABLE_SPI_NOR_USE_4K_SECTORS
		if (((erase_start % FLASH_BLOCK_SIZE)==0) && (erase_count >= FLASH_BLOCK_SIZE)) { // block 对齐
			sflash_block_erase(flash, erase_start);
			erase_start += FLASH_BLOCK_SIZE;
			erase_count -= FLASH_BLOCK_SIZE;
		}
		else {
			sflash_sector_erase(flash, erase_start);
			erase_start += FLASH_SECTOR_SIZE;
			erase_count -= FLASH_SECTOR_SIZE;
		}
#else
		sflash_block_erase(flash, erase_start);
		erase_start += FLASH_BLOCK_SIZE;
		erase_count -= FLASH_BLOCK_SIZE;
#endif
	}
	return 0;
}

// lock the flash start from address of 0 to protect_addr.
static int sflash_write_protect_lock(unsigned int protect_addr)
{
	unsigned char dev_status, tb = 0, bp3 = 0, bp2_bp0 = 0x0;

	if(0x0 == protect_addr){
		bp3          = 0x0;
		bp2_bp0      = 0x0;
		tb           = 0x0;
	}
	else
	{
		printf("Not support write protect\n");
		return -1;
	}

	SFLASH_DBG("bp2_bp0=0x%x\n",bp2_bp0);

	read_sr1(&dev_status);
	SFLASH_DBG("dev_status=0x%x\n", dev_status);

	dev_status &=~0x7c;
	dev_status |=((bp2_bp0<<2) | (tb << 5 ) | ( bp3 <<6) | (1<<7));
	SFLASH_DBG("the lock value that will be write is 0x%x\n",dev_status);

	write_sr(&dev_status, 1, 1);

	return 0;
}

// unlock the flash start from address of 0 to flashsize-1.
static int sflash_write_protect_unlock(void)
{
	return sflash_write_protect_lock(0);
}

static void sflash_calcblockrange(unsigned int addr, unsigned int len, unsigned int *pstart, unsigned int *pend)
{
	unsigned int dummy;

	if (sflash_calcblock(addr, pstart, &dummy) == 0)
		if (sflash_calcblock(addr + len - 1, &dummy, pend) == 0)
			return ;
}

static void sflash_sync(void)
{
	wait_till_ready();
}

static int sflash_otp_probe(struct spi_flash *flash,unsigned int jedec_id);
struct flash_dev spi_nor_flash_dev;
static struct flash_dev * sflash_init(struct spi_device *spi_device)
{
	struct spi_flash *flash;
	flash = &g_flash_info;
	flash->spi_device = spi_device;

	if (sflash_detect(&g_flash_info) != 0)
		return NULL;
	sflash_otp_probe(&g_flash_info, g_flash_info.info->jedec_id);

	return &spi_nor_flash_dev;
}

/***************************************** FLASH OTP *********************************************/
static sflash_otp sflash_otp_data[] = {
	/* winbond */
	{"w25q32",     0xef4016, 0x001000, 0x001000, 256,  3, },
	{"w25q64",     0xef4017, 0x001000, 0x001000, 256,  3, },
	{"w25q128",    0xef4018, 0x001000, 0x001000, 256,  3, },
	{"w25q256",    0xef4019, 0x001000, 0x001000, 256,  3, },
	/* gd */
	{"gd25q32c",   0xc84016, 0x001000, 0x001000, 1024, 3, },//support gd25q32c,not support gd25q32b
	{"gd25q64c",   0xc84017, 0x001000, 0x001000, 1024, 3, },//support gd25q64c,not support gd25q64b
	{"gd25q128c",  0xc84018, 0x001000, 0x001000, 256,  3, },//support gd25q128c,not support gd25q128b
	/* mxic */
	//mxic flash OTP hardware does not support the erase operation
	{"MX25L3206",    0xc22016, 0x000000, 0x000000, 64,   1, },
	{"MX25L12835F",  0xc22018, 0x000000, 0x000000, 512,  1, },
	/* eon */
	{"en25q32",    0x1c3016, 0x3FF000, 0x000000, 512,  1, },
	/* pmc */
	//pmc flash OTP hardware does not support the erase operation
	{"pm25lq032c", 0x7f9d46, 0x000000, 0x000000, 64,   1, },

	/* paragon */
	{"pn25f32",    0xe04016, 0x000100, 0x000100, 256,  3, },

	/* issi */
	{"ic25lp064",  0x9d6017, 0x000000, 0x001000, 256,  4, },

};

#define SET_REGION_FLAGS(flags,num) ((flags)=((flags)&(~0x07))|(num))
#define GET_REGION_FLAGS(flags)     ((flags)&0x07)
#define JEDEC_MFR(_jedec_id)    ((_jedec_id) >> 16)
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

static int sflash_otp_probe(struct spi_flash *flash,unsigned int jedec_id)
{
	struct sflash_otp *otp_ptr;
	int count = 0;

	for(count = 0, otp_ptr = sflash_otp_data; count < ARRAY_SIZE(sflash_otp_data); count++, otp_ptr++){
		if(jedec_id == otp_ptr->jedec_id)
			break;
	}

	if(count == ARRAY_SIZE(sflash_otp_data))
		return 0;

	flash->otp.start_addr  = otp_ptr->start_addr;
	flash->otp.skip_addr   = otp_ptr->skip_addr;
	flash->otp.region_size = otp_ptr->region_size;
	flash->otp.region_num  = otp_ptr->region_num;
	flash->otp.flags       = otp_ptr->flags;

	return 0;
}

static int sflash_otp_entry(unsigned int on)
{
	struct spi_flash  *flash;
	unsigned char cmd;

	flash = &g_flash_info;

	switch(JEDEC_MFR(flash->info->jedec_id))
	{
		case 0xc2 /* mxic */:
		case 0xef /* winbond */:
			cmd = on ? 0xB1 : 0xC1;
			return xfer_data(&cmd, 1);
			break;
		case 0x1c /* eon */:
			cmd = on ? 0x3A : 0x04;
			return xfer_data(&cmd, 1);
			break;
		default /* gd pm paragon issi */:
			return 0;
	}

	return 0;
}

static int sflash_otp_write(unsigned int addr, unsigned char *buf, unsigned int len);
static int sflash_otp_lock(void)
{
	struct spi_flash *flash;
	unsigned char status[3];
	int region;
	xfer_cmd_t  xfer;

	flash = &g_flash_info;
	region = GET_REGION_FLAGS(flash->otp.flags);

	wait_till_ready();

	switch(JEDEC_MFR(flash->info->jedec_id))
	{
		case 0xef /* winbond */:
		case 0xe0 /* paragon */:
			read_sr1(&status[0]);
			read_sr2(&status[1]);
			status[1] |= 1<<(3 + region);
			write_sr(status, 2, 1);
			break;
		case 0xc8 /* gd */:
			read_sr2(&status[1]);
			status[1] |= 1<<(3 + region);
			write_sr(&status[1], 1, 2);
			break;
		case 0xc2 /* mxic */:
			status[0] = 0x2F; //lock otp
			write_enable();
			xfer_data(status, 1);
			break;
		case 0x1c /* eon */:
			sflash_otp_entry(1);
			read_sr1(&status[0]);
			status[0] |= 1<<7;
			write_sr(&status[0], 1, 1);
			wait_till_ready();
			sflash_otp_entry(0);
			break;
		case 0x7f /* pmc */:
			status[0] = 0;
			if(flash->info->jedec_id == 0x7f9d46){  /* pm25lq032c */
				flash->otp.region_size = 65;
				sflash_otp_write(64, &status[0], 1);
				flash->otp.region_size = 64;
			}
			break;
		case 0x9d /* issi */:
			status[0] = GX_CMD_WRFR;
			status[2] = GX_CMD_RDFR;
			write_enable();

			xfer.cmd_len = 1;
			xfer.cmd[0] = status[2];
			xfer.xfer_len  = 1;
			xfer.rx = &status[1];
			xfer_cmd(&xfer);
			status[1] |= 1 << (4 + region);
			xfer_data(status, 2);
			break;
		default:
			return -1;
	}

	return 0;
}

static int sflash_otp_read(unsigned int addr, unsigned char *buf, unsigned int len);
static int sflash_otp_status(unsigned char *buf)
{
	struct spi_flash *flash;
	unsigned char status[3];
	unsigned int region;
	unsigned char otp_lock = 0;
	xfer_cmd_t  xfer;

	flash = &g_flash_info;
	region = GET_REGION_FLAGS(flash->otp.flags);

	wait_till_ready();

	switch(JEDEC_MFR(flash->info->jedec_id))
	{
		case 0xef /* winbond */:
		case 0xe0 /* paragon */:
			read_sr2(&status[0]);
			otp_lock = (status[0] >> (3 + region)) & 0x01;
			break;
		case 0xc8 /* gd */:
			read_sr2(&status[0]);
			otp_lock = (status[0] >> (3 + region)) & 0x01;
			break;
		case 0xc2 /* mxic */:
			xfer.cmd_len = 1;
			xfer.cmd[0] = 0x2B; /* read otp status operation */
			xfer.xfer_len  = 1;
			xfer.rx = &status[1];
			xfer_cmd(&xfer);
			otp_lock = (status[1] >> 1) & 0x01;
			break;
		case 0x1c /* eon */:
			sflash_otp_entry(1);
			read_sr1(&status[0]);
			otp_lock = (status[0] >> 7) & 0x01;
			wait_till_ready();
			sflash_otp_entry(0);
			break;
		case 0x7f /* pmc */:
			if(flash->info->jedec_id == 0x7f9d46){  /* pm25lq032c */
				flash->otp.region_size = 65;
				sflash_otp_read(64, &status[0], 1);
				flash->otp.region_size = 64;
				otp_lock = (~(status[0] & 0x01)) & 0x1 ;
			}
			break;
		case 0x9d /* issi */:
			xfer.cmd_len = 1;
			xfer.cmd[0] = GX_CMD_RDFR;
			xfer.xfer_len  = 1;
			xfer.rx = &status[1];
			xfer_cmd(&xfer);
			otp_lock = (status[1] >> (4 + region)) & 0x01;
			break;
		default:
			return -1;
	}
	*buf = otp_lock;

	return 0;
}

static int sflash_otp_erase(void)
{
	struct spi_flash *flash;
	unsigned int addr;
	unsigned int region;

	flash = &g_flash_info;
	region = GET_REGION_FLAGS(flash->otp.flags);
	addr = flash->otp.start_addr + (region * flash->otp.skip_addr);

	switch(JEDEC_MFR(flash->info->jedec_id))
	{
		case 0xef /* winbond */:
		case 0xe0 /* paragon */:
		case 0xc8 /* gd */:
			flash->command[0] = 0x44;
			break;
		case 0x1c /* eon */:
			flash->command[0] = 0x20;
			break;
		case 0x9d /* issi */:
			flash->command[0] = 0x64;
			break;
		case 0xc2 /* mxic */: /* not support erase operation */
		case 0x7f /* pmc  */:
			return 0;
		default:
			return 0;
	}

	wait_till_ready();

	/* set write enable ,the send erase cmd and addr*/
	write_enable();
	sflash_otp_entry(1);
	sflash_addr2cmd(flash, addr, flash->command);
	xfer_data(flash->command, sflash_cmdsz(flash));
	wait_till_ready();
	sflash_otp_entry(0);

	return 0;
}

static int sflash_otp_write(unsigned int addr, unsigned char *buf, unsigned int len)
{
	struct spi_flash *flash;
	unsigned int retlen;
	unsigned int page_offset, page_size;
	unsigned int region;
	xfer_cmd_t  xfer;

	flash = &g_flash_info;
	region = GET_REGION_FLAGS(flash->otp.flags);

	if(!len)
		return -1;

	if((addr + len) > flash->otp.region_size)
		return -1;

	addr += flash->otp.start_addr + (region * flash->otp.skip_addr);

	switch(JEDEC_MFR(flash->info->jedec_id))
	{
		case 0xef /* winbond */:
		case 0xe0 /* paragon */:
		case 0xc8 /* gd */:
			xfer.cmd[0] = 0x42;
			break;
		case 0xc2 /* mxic */:
		case 0x1c /* eon */:
			xfer.cmd[0] = 0x02;
			break;
		case 0x7f /* pmc */:
			xfer.cmd[0] = 0xb1;
			break;
		case 0x9d /* issi */:
			xfer.cmd[0] = 0x62;
			break;
		default:
			return -1;
	}

	xfer.cmd_len = sflash_cmdsz(flash);
	xfer.tx = buf;

	/* Wait till previous write/erase is done. */
	wait_till_ready();
	sflash_otp_entry(1);
	write_enable();

	sflash_addr2cmd(flash, addr, xfer.cmd);

	/*what page do we start write*/
	page_offset = addr % GX_PAGESIZE;

	/* all the write data in one page*/
	if(page_offset + len <= GX_PAGESIZE){
		xfer.xfer_len = len;
		xfer_cmd(&xfer);
		retlen = len;
	}
	else{
		unsigned int i;
		/*write the remainder data on the first page*/
		page_size = GX_PAGESIZE - page_offset;
		xfer.xfer_len = page_size;
		xfer_cmd(&xfer);

		retlen = page_size;

		/* write everything in pagesize chunks*/
		for(i = page_size; i < len; i += page_size){
			page_size = len - i;
			if(page_size > GX_PAGESIZE)
				page_size = GX_PAGESIZE;

			/* write next page to flash*/
			sflash_addr2cmd(flash, addr + i, flash->command);

			xfer.xfer_len = page_size;
			xfer.tx = buf + i;

			wait_till_ready();
			write_enable();
			xfer_cmd(&xfer);
			retlen += page_size;
		}
	}
	wait_till_ready();
	sflash_otp_entry(0);

	return retlen;
}

static int sflash_otp_read(unsigned int addr, unsigned char *buf, unsigned int len)
{
	struct spi_flash *flash;
	unsigned int retlen;
	unsigned int region;
	xfer_cmd_t  xfer;
	unsigned char dummy;

	flash = &g_flash_info;
	region = GET_REGION_FLAGS(flash->otp.flags);

	if(!len)
		return -1;

	if((addr + len) > flash->otp.region_size)
		return -1;

	addr += flash->otp.start_addr + (region * flash->otp.skip_addr);


	switch(JEDEC_MFR(flash->info->jedec_id))
	{
		case 0xef /* winbond */:
		case 0xe0 /* paragon */:
		case 0xc8 /* gd */:
			xfer.cmd[0] = 0x48;
			dummy = 1;
			break;
		case 0xc2 /* mxic */:
		case 0x1c /* eon */:
			xfer.cmd[0] = 0x03;
			dummy = 0;
			break;
		case 0x7f /* pmc */:
			xfer.cmd[0] = 0x4b;
			dummy = 0;
			break;
		case 0x9d /* issi */:
			xfer.cmd[0] = 0x68;
			dummy = 1;
			break;
		default :
			return -1;
	}

	xfer.cmd_len = sflash_cmdsz(flash) + dummy;
	xfer.rx = buf;
	xfer.xfer_len = len;

	/* Wait till previous write/erase is done. */
	wait_till_ready();
	sflash_otp_entry(1);
	sflash_addr2cmd(flash, addr, xfer.cmd);
	xfer_cmd(&xfer);
	wait_till_ready();
	sflash_otp_entry(0);
	retlen = len;

	return retlen;
}

static int sflash_otp_get_region(unsigned int *region)
{
	struct spi_flash *flash = &g_flash_info;
	*region = flash->otp.region_num;

	return 0;
}

static int sflash_otp_set_region(unsigned int region)
{
	struct spi_flash *flash = &g_flash_info;
	if(region >= 0 && region < flash->otp.region_num)
		SET_REGION_FLAGS(flash->otp.flags, region);
	else
		return -1;

	return 0;
}

struct flash_dev spi_nor_flash_dev = {
	.init                = sflash_init,
	.readdata            = sflash_readdata,
	.gettype             = sflash_gettype,
	.getsize             = sflash_getsize,
	.write_protect_mode  = NULL,
	.write_protect_status= NULL,
	.write_protect_lock  = sflash_write_protect_lock,
	.write_protect_unlock= sflash_write_protect_unlock,
	.chiperase           = sflash_chiperase,
	.erasedata           = sflash_erasedata,
	.pageprogram         = sflash_pageprogram,
	.sync                = sflash_sync,
	.test                = NULL,
	.calcblockrange      = sflash_calcblockrange,
	.badinfo             = NULL,
	.pageprogram_yaffs2  = NULL,
	.readoob             = NULL,
	.writeoob            = NULL,
	.otp_lock            = sflash_otp_lock,
	.otp_status          = sflash_otp_status,
	.otp_erase           = sflash_otp_erase,
	.otp_read            = sflash_otp_read,
	.otp_write           = sflash_otp_write,
	.otp_get_region      = sflash_otp_get_region,
	.otp_set_region      = sflash_otp_set_region
};

