/*****************************************
  Copyright (c) 2003-2007
  Nationalchip Science & Technology Co., Ltd. All Rights Reserved
  Proprietary and Confidential
 *****************************************/

#ifndef __BOOTLOADER_SFLASH_H
#define __BOOTLOADER_SFLASH_H

#include "types.h"

#ifndef NULL
#define NULL 0
#endif

// GX series serial flash
#define GX_CMD_WREN           0x06      // write enable
#define GX_CMD_WRDI           0x04      // write disable
#define GX_CMD_RDSR1          0x05      // read status1 register
#define GX_CMD_RDSR2          0x35      // read status2/configure register
#define GX_CMD_RDSR3          0x15      // read status3/configure register
#define GX_CMD_WRSR1          0x01      // write status/configure register
#define GX_CMD_WRSR2          0X31      // write status/configure register
#define GX_CMD_WRSR3          0X11      // write status/configure register
#define GX_CMD_READ           0x03      // read data bytes
#define GX_CMD_FAST_READ      0x0b      // read data bytes at higher speed
#define GX_CMD_DREAD          0x3B      // read data dual output
#define GX_CMD_QREAD          0x6B      // read data quad output
#define GX_CMD_PP             0x02      // page program
#define GX_CMD_PP4            0x12      // page program with 4-Byte Address
#define GX_CMD_SE             0x20      // sector erase
#define GX_CMD_BE             0xd8      // block erase
#define GX_CMD_CE             0xc7      // chip erase
#define GX_CMD_DP             0xb9      // deep power down
#define GX_CMD_RES            0xab      // release from deep power down
#define GX_CMD_READID         0x9F      // read ID
#define GX_CMD_READID_BAK     0x90      // read ID for WinBond
#define GX_CMD_WREAR          0xC5      //Write Extended Address Register

// dummy clock
#define FAST_READ_DUMMY_CLOCK  8         //fast read dummy clock size

/* Used for Macronix and Winbond flashes. */
#define GX_CMD_EN4B     0xb7    /* Enter 4-byte mode */
#define GX_CMD_EX4B     0xe9    /* Exit 4-byte mode */
/* Used for Spansion flashes only. */
#define GX_CMD_BRWR     0x17    /* Bank register write */
/* Usec for ISSI flashes only. */
#define GX_CMD_RDFR     0x48    /* Read Function Register */
#define GX_CMD_WRFR     0x42    /* Write Function Register */


// GX25Lxx series status regsiter
#define GX_STAT_WIP           0x01      // write in progress bit
#define GX_STAT_WEL           0x02      // write enable latch
#define GX_STAT_BP0           0x04      // block protect bit 0
#define GX_STAT_BP1           0x08      // block protect bit 1
#define GX_STAT_BP2           0x10      // block protect bit 2
#define GX_STAT_SRWD          0x80      // status register write protect

// page size of pageprogram command
#define GX_PAGESIZE            256      // GX25L : 256 bytes

/*
 * spi read/write/erase  mode Macro
 */
#define FAST_READ             0X10
#define DUAL_READ             0X20
#define QUAD_READ             0X40

#define FAST_PP               0X100
#define DUAL_PP               0X200

#define FAST_ERASE            0X1000
#define DUAL_ERASE            0X2000

#define SFLASH_READY_TIMEOUT  5000000    // 5S

typedef struct {
	char               *name;
	int                jedec_id;
	unsigned int       size;
	unsigned int       flag;
} sflash_info;

typedef struct sflash_otp{
	char           *name;
	unsigned int   jedec_id;
	unsigned int   start_addr;
	unsigned int   skip_addr;
	unsigned int   region_size;
	unsigned int   region_num;
	unsigned int   flags;
} sflash_otp;

typedef struct{
	uint8_t  cmd_len;
	uint8_t  cmd[6];
	uint32_t xfer_len;
	uint8_t  *tx;
	uint8_t  *rx;
} xfer_cmd_t;

#endif // __BOOTLOADER_SFLASH_H
