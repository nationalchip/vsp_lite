/*****************************************
  Copyright (c) 2003-2019
  Nationalchip Science & Technology Co., Ltd. All Rights Reserved
  Proprietary and Confidential
 *****************************************/

#include <stdio.h>
#include <string.h>
#include <timer.h>
#include <driver/delay.h>
#include "spi_flash.h"

#define DRAM_BASE	0X40000000
#define MALLOC_MAX_LEN	(768*1024)
#define RAND_CNT 	1000

struct flash_test_info {
	struct flash_dev *dev;
	u32 test_dram_base;
	u32 malloc_max_len;
	int flash_type;
	u32 flash_size;
	u32 flash_block_size;
	u32 flash_page_size;
	u32 rand_cnt;
};

typedef struct sflash_test_struct_s{
	u32 addr;
	u32 len;
}sflash_test_struct_t;

sflash_test_struct_t sflash_test_array[] = {
	{0x14000, 112350}, {0x15000, 112350}, {0x15000, 131072},
	{0x1a0c0, 112350}, {0x1a000, 112352}, {0x1a0c0, 112352},
	{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}, 		/*测试少数字节的读写*/
	{1, 1}, {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7},
	{1, 8}, {1, 9}, {1, 10}, 						/*测试未对齐的读写*/
	{0, 100}, {0, 256}, {0, 512},						/*测试稍多字节的读写*/
	{100, 256}, {100,100}, {200, 100}, {300, 300}, {100, 500},		/*测试跨PAGE读写*/
	{0, 0x1000}, {0, 0x1000+1}, {0, 0x1000+2}, {0, 0x1000+3}, {0, 0x1000+4},/*测试多字节读写*/
	{0, 0x100000}, {0, 0x200000}, {0, 0x400000}				/*测试M级别字节读写*/
	/*此外还将产生随机地址和随机大小进行测试*/
};

sflash_test_struct_t spinandflash_test_array[] = {
	{0x20000, 0x1000}, {0x0, 0x10000}, {0x40000, 0x10000},
	{0x80000, 0x10000}, {0x100000, 0x30000}, {0x60000, 0x20000},
	{0x60000, 0x10000}, {0xa0000, 0x10000}, {0x200000, 0x10000},
	{0x400000, 0x30000}
};

extern struct flash_dev spi_nor_flash_dev;
extern struct flash_dev spi_nand_flash_dev;
static struct flash_test_info flash_test;
static u64 holdrand_test = 1;
static u32 inline rand_test(void)
{
	return (((holdrand_test = holdrand_test * 214013L + 2531011L) >> 16 ) & 0x7fff);
}

int sflash_test_single(uint32_t addr, uint32_t len)
{
	struct flash_dev *flash = flash_test.dev;
	u8 *p, *p2, *p_src, *p_dst, *p_tmp;
	u32 erase_addr, erase_len;
	int i;

	erase_addr = addr & ~(flash_test.flash_block_size - 1);
	erase_len  = len;

	printf(">>>>>>>>>addr = 0x%x, erase addr = 0x%x\n", erase_addr, addr);
	p = malloc(len);
	if(p == NULL) {
		printf("LINE%d:malloc failed.malloc len=0x%x\n", __LINE__, len);
		while(1);
	}
	p2 = malloc(len);
	if(p2 == NULL) {
		printf("LINE%d:malloc failed.malloc len=0x%x\n", __LINE__, len);
		while(1);
	}

	spi_flash_readdata(flash, addr, p, len);
	printf("sflash_gx_readl finish.\n");

	spi_flash_erasedata(flash, erase_addr, erase_len);
	printf("sflash erase finish.\n");

	printf("read addr=0x%x, len=%d\n", addr, len);
	spi_flash_readdata(flash, addr, p, len);
	printf("sflash_gx_readl finish.\n");
	for(i = 0, p_tmp = p; i < len; i++) {
		if(*p_tmp++ != 0xff) { /*确认擦除后数据是否为全1*/
			printf("erase error, i = %d\n", i);
			while(1);		//擦除错误则停止，便于打印观看
		}
	}

	memcpy(p2, (void*)flash_test.test_dram_base, len);
	memset(p, 0, len);

	spi_flash_pageprogram(flash, addr, p2, len);
	printf("sflash page program finish.\n");

	spi_flash_readdata(flash, addr, p, len);
	printf("sflash_gx_readl finish.\n");

	p_src = p;
	p_dst = p2;
	for(i = 0; i < len; i++){
		if((*p_src++) != (*p_dst++)) {
			printf("cmp error. i = %d. read=0x%x, orig=0x%x\n", i, *(p_src-1), *(p_dst-1));
			free(p);
			free(p2);
			return -1;
		}
	}

	printf("cmp finish. src equal dst.\n");

	free(p);
	free(p2);
	return 0;
}

static int flash_test_init(void)
{
	flash_test.dev = spi_flash_probe(0,0,85000000,0x800);
	if(flash_test.dev == &spi_nor_flash_dev)
		flash_test.flash_type = 0;
	else if(flash_test.dev == &spi_nand_flash_dev)
		flash_test.flash_type = 1;
	else{
		flash_test.flash_type = -1;
		return -1;
	}

	flash_test.flash_size = spi_flash_getsize(flash_test.dev, SPI_FLASH_SIZE_CHIP);
	flash_test.flash_block_size = spi_flash_getsize(flash_test.dev, SPI_FLASH_SIZE_BLOCK);
	flash_test.flash_page_size = spi_flash_getsize(flash_test.dev, SPI_FLASH_SIZE_PAGE);
	flash_test.test_dram_base = DRAM_BASE;
	flash_test.malloc_max_len = MALLOC_MAX_LEN;
	flash_test.rand_cnt = RAND_CNT;

	return 0;
}

static int flash_get_rand(u32 *addr, u32 *len)
{
	u32 tmp_addr, tmp_len;

	tmp_addr = rand_test() % flash_test.flash_size;
	tmp_len  = rand_test() % flash_test.malloc_max_len;


	if(flash_test.flash_type)
		tmp_addr &= ~(flash_test.flash_page_size - 1);

	if((tmp_addr + tmp_len) > flash_test.flash_size)
		tmp_len = flash_test.flash_size - tmp_addr;

	*addr = tmp_addr;
	*len  = tmp_len;

	return 0;
}

static void flash_speed_test(void)
{
	struct flash_dev *flash = flash_test.dev;
	u32 test_len = flash_test.flash_block_size * 2;
	u8 *p;
	unsigned long long speed, time, time1;

	printf("\n\n********** FLASH SPEED TEST *********\n");

	p = malloc(test_len);
	if(p == NULL) {
		printf("LINE%d:malloc failed.malloc len=0x%x\n", __LINE__, test_len);
		while(1);
	}

	time1 = get_time_us();
	spi_flash_readdata(flash, 0, p, test_len);
	time = get_time_us() - time1;

	speed = ((test_len * 1000 ) >> 10) * 1000u / time;
	printf(">>>>>read\t%d kb from flash speed: %lld KB/s\n",test_len/1024, speed);

	spi_flash_erasedata(flash, 0, test_len);

	memset(p, 0, test_len);

	time1 = get_time_us();
	spi_flash_pageprogram(flash, 0, p, test_len);
	time = get_time_us() - time1;

	speed = ((test_len * 1000 ) >> 10) * 1000u / time;
	printf(">>>>>write\t%d kb from flash speed: %lld KB/s\n",test_len/1024, speed);

	free(p);
}

void flash_complete_test(void)
{
	struct flash_dev *dev;
	sflash_test_struct_t *p_array;
	int i, num, ret;
	u32 addr_tmp, len_tmp;

	ret = flash_test_init();
	if(ret < 0){
		printf("error: flash_test_init.\n");
		return;
	}

	dev = flash_test.dev;

	switch(flash_test.flash_type){
		case 0:		//spi nor
			printf("flash type is nor.\n");
			num = sizeof(sflash_test_array) / sizeof(sflash_test_struct_t);
			p_array = sflash_test_array;
			break;
		case 1:		//spi nand
			printf("flash type is nand.\n");
			num = sizeof(spinandflash_test_array) / sizeof(sflash_test_struct_t);
			p_array = spinandflash_test_array;
			break;
		default:
			printf("error: cann't find flash type.\n");
			return;
	}


	spi_flash_write_protect_unlock(flash_test.dev);



	for(i = 0; i < num; i++){
		printf(">>>>>sflash_test:addr=0x%x, len = %d\n", p_array[i].addr, p_array[i].len);
		if(sflash_test_single(p_array[i].addr, p_array[i].len)) {
			printf("*****sflash_test_failed:addr=0x%x, len = %d\n\n\n", \
				p_array[i].addr, p_array[i].len);
			while(1);
		}
		printf(">>>>>sflash_test_success.\n\n\n");
	}


	printf("\n\n********** RAND TEST *********\n\n");

	/*随机数测试*/
	while(flash_test.rand_cnt--) {

		flash_get_rand(&addr_tmp, &len_tmp);

		printf(">>>>>sflash_test:addr=0x%x, len = %d\n", addr_tmp, len_tmp);
		if(sflash_test_single(addr_tmp, len_tmp)){
			printf("*****sflash_test_failed:addr=0x%x, len = %d\n\n\n", \
				addr_tmp, len_tmp);
			while(1);
		}
		printf(">>>>>sflash_test_success.\n\n\n");
	}

	flash_speed_test();

	printf("flash comtest finished.\n");
	while(1);
}


#if 0
#define BLOCK_SIZE 0x20000
static void imitate_block(int bad_block)
{
	unsigned char oob_write_buf[2];
	unsigned char oob_read_buf[2];
	int bad_addr = bad_block * BLOCK_SIZE;

	oob_write_buf[0] = 0x0;
	oob_write_buf[1] = 0xff;
	gxflash_writeoob(bad_addr, oob_write_buf, 2);
	gxflash_readoob(bad_addr, oob_read_buf, 2);
	printf(">>>>>sflash_test:oob[0]=0x%x, oob[1]=0x%x\n", \
			oob_read_buf[0], oob_read_buf[1]);

	//update bbt
	gxflash_badinfo();
}

static int flash_bbt_block_test(void)
{
#define TEST_BLOCK_NUM 20
#define TEST_BYTES_PER_BLOCK 0x2000
	uint8_t *src[TEST_BLOCK_NUM];
	uint8_t *dst[TEST_BLOCK_NUM];
	int i;
	int test_block_num = TEST_BLOCK_NUM;
	int test_len = TEST_BYTES_PER_BLOCK;
	unsigned int bad_block, bad_addr;

	if (gxflash_badinfo() != 0) {
		printf("this type of flash doesn't support oob!\n");
		return -1;
	}

	//imitate block 5 with bad one
	bad_block = 5;
	bad_addr = bad_block * BLOCK_SIZE;

	for (i=0; i<test_block_num; i++) {
		src[i] = malloc(test_len);
		dst[i] = malloc(test_len);
		if(src[i] == NULL || dst[i] == NULL) {
			printf("LINE%d:malloc failed.\n", __LINE__);
			while(1);
		}
	}

	for (i=0; i<test_block_num; i++) {
		memset(src[i], i, test_len);
	}

	/* write & read with skip */
	for (i=0; i<test_block_num; i++) {
		memset(dst[i], 0x5a, test_len);
	}
	gxflash_erasedata_noskip(0, (test_block_num+1)*BLOCK_SIZE);
	imitate_block(bad_block);

	for (i=0; i<bad_block+1; i++) {
		gxflash_pageprogram(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata(i*BLOCK_SIZE, dst[i], test_len);
	}
	gxflash_erasedata_noskip((bad_block+1)*BLOCK_SIZE, BLOCK_SIZE);//must be erased first
	for (i=bad_block+1; i<test_block_num; i++) {
		gxflash_pageprogram(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata(i*BLOCK_SIZE, dst[i], test_len);
	}
	for (i=0; i<test_block_num; i++) {
		if (memcmp(src[i], dst[i], test_len) != 0) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, i);
			while(1);
		}
	}
	printf(">>>>>sflash write read with skip ok.\n\n\n");

	/* write & read with no skip */
	for (i=0; i<test_block_num; i++) {
		memset(dst[i], 0x5a, test_len);
	}
	gxflash_erasedata_noskip(0, (test_block_num+1)*BLOCK_SIZE);
	imitate_block(bad_block);

	for (i=0; i<test_block_num; i++) {
		gxflash_pageprogram_noskip(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata_noskip(i*BLOCK_SIZE, dst[i], test_len);
	}
	for (i=0; i<test_block_num; i++) {
		if (memcmp(src[i], dst[i], test_len) != 0) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, i);
			while(1);
		}
	}
	printf(">>>>>sflash write read with no skip ok.\n\n\n");

	/* write with skip, read with no skip */
	for (i=0; i<test_block_num; i++) {
		memset(dst[i], 0x5a, test_len);
	}
	gxflash_erasedata_noskip(0, (test_block_num+1)*BLOCK_SIZE);
	imitate_block(bad_block);

	for (i=0; i<bad_block+1; i++) {
		gxflash_pageprogram(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata_noskip(i*BLOCK_SIZE, dst[i], test_len);
	}
	gxflash_erasedata_noskip((bad_block+1)*BLOCK_SIZE, BLOCK_SIZE);//must be erased first
	for (i=bad_block+1; i<test_block_num; i++) {
		gxflash_pageprogram(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata_noskip(i*BLOCK_SIZE, dst[i], test_len);
	}

	for (i=0; i<bad_block; i++) {
		if (memcmp(src[i], dst[i], test_len) != 0) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, i);
			while(1);
		}
	}
	for (i=0; i<test_len; i++) {
		if (dst[bad_block][i] != 0xff) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, bad_block);
			while(1);
		}
	}
	for (i=bad_block+1; i<test_block_num; i++) {
		if (memcmp(src[i], dst[i], test_len) != 0) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, i);
			while(1);
		}
	}
	printf(">>>>>sflash write with skip, read with no skip ok.\n\n\n");

	/* write with no skip, read with skip */
	for (i=0; i<test_block_num; i++) {
		memset(dst[i], 0x5a, test_len);
	}
	gxflash_erasedata_noskip(0, (test_block_num+1)*BLOCK_SIZE);
	imitate_block(bad_block);

	for (i=0; i<bad_block+1; i++) {
		gxflash_pageprogram_noskip(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata(i*BLOCK_SIZE, dst[i], test_len);
	}
	gxflash_erasedata_noskip((bad_block+1)*BLOCK_SIZE, BLOCK_SIZE);//must be erased first
	for (i=bad_block+1; i<test_block_num; i++) {
		gxflash_pageprogram_noskip(i*BLOCK_SIZE, src[i], test_len);
		gxflash_readdata(i*BLOCK_SIZE, dst[i], test_len);
	}
	for (i=0; i<bad_block; i++) {
		if (memcmp(src[i], dst[i], test_len) != 0) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, i);
			while(1);
		}
	}
	for (i=0; i<test_len; i++) {
		if (dst[bad_block][i] != 0xff) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, bad_block);
			while(1);
		}
	}
	for (i=bad_block+1; i<test_block_num; i++) {
		if (memcmp(src[i], dst[i], test_len) != 0) {
			printf("LINE%d:write failed. Block %d\n", __LINE__, i);
			while(1);
		}
	}
	printf(">>>>>sflash write with no skip, read with skip ok.\n\n\n");

	//recover bad block
	gxflash_erasedata_noskip(bad_addr, BLOCK_SIZE);

	for (i=0; i<test_block_num; i++) {
		free(src[i]);
		free(dst[i]);
	}

	return 0;
}

static int flash_bbt_partition_test(void)
{
	uint8_t *src, *dst;
	unsigned int addr, len;
	unsigned int bad_block, bad_addr;

	if (gxflash_badinfo() != 0) {
		printf("this type of flash doesn't support oob!\n");
		return -1;
	}

	//imitate block 5 with bad one
	bad_block = 5;
	bad_addr = bad_block * BLOCK_SIZE;

	//block 4, 5, 6
	addr = 4*BLOCK_SIZE;
	len = 3*BLOCK_SIZE;

	src = malloc(len);
	dst = malloc(len);
	if(src == NULL || dst == NULL)
	{
		printf("LINE%d:malloc failed.malloc len=0x%x\n", __LINE__, len);
		while(1);
	}

	memset(src, 0, BLOCK_SIZE);
	memset(src+BLOCK_SIZE, 1, BLOCK_SIZE);
	memset(src+2*BLOCK_SIZE, 2, BLOCK_SIZE);

	gxflash_erasedata_noskip(addr, len+BLOCK_SIZE);
	imitate_block(bad_block);
	gxflash_pageprogram_noskip(addr, src, len);
	gxflash_readdata(addr, dst, len);
	if (memcmp(src, dst, BLOCK_SIZE) != 0 || memcmp(src+2*BLOCK_SIZE, dst+BLOCK_SIZE, BLOCK_SIZE) != 0) {
		printf("LINE%d:write failed.\n", __LINE__);
		while(1);
	}

	gxflash_erasedata_noskip(addr, len+BLOCK_SIZE);
	imitate_block(bad_block);
	gxflash_pageprogram(addr, src, len);
	gxflash_readdata_noskip(addr, dst, len);
	if (memcmp(src, dst, BLOCK_SIZE) != 0 || memcmp(src+BLOCK_SIZE, dst+2*BLOCK_SIZE, BLOCK_SIZE) != 0) {
		printf("LINE%d:write failed.\n", __LINE__);
		while(1);
	}

	gxflash_erasedata_noskip(addr, len+BLOCK_SIZE);
	imitate_block(bad_block);
	gxflash_pageprogram_noskip(addr, src, len);
	gxflash_readdata_noskip(addr, dst, len);
	if (memcmp(src, dst, 0x60000) != 0) {
		printf("LINE%d:write failed.\n", __LINE__);
		while(1);
	}

	gxflash_erasedata_noskip(addr, len+BLOCK_SIZE);
	imitate_block(bad_block);
	gxflash_pageprogram(addr, src, len);
	gxflash_readdata(addr, dst, len);
	if (memcmp(src, dst, 0x60000) != 0) {
		printf("LINE%d:write failed.\n", __LINE__);
		while(1);
	}

	//recover bad block
	gxflash_erasedata_noskip(bad_addr, BLOCK_SIZE);

	free(src);
	free(dst);

	return 0;
}

void flash_oob_test(void)
{
	gxflash_write_protect_unlock();

	printf(">>>>>nand flash bbt block test start.\n");
	if (flash_bbt_block_test() != 0)
		return;
	printf(">>>>>nand flash bbt block test successfully.\n\n\n");

	printf(">>>>>nand flash bbt partition test start.\n");
	if (flash_bbt_partition_test() != 0)
		return;
	printf(">>>>>nand flash bbt partition test successfully.\n\n\n");
}
#endif

// -------------------------------------------------------------------------------

#define display_mem(_addr, _len, _msg) do{\
    printf("%s\n\t", _msg);\
    int _i = 0;\
    while(_i++ < _len)\
        printf("%02x%s", *((uint8_t*)_addr + _i -1), ((_i&0x0f) == 0) ? "\n\t":" ");\
    printf("\n");\
}while(0)

static void test_code (void)
{
	struct flash_dev *fd;
	unsigned char buf[1024];
	int ret, i;

	fd = spi_flash_probe(0, 0, 85000000, 0x800);
	if (!fd) {
		printf ("%s, probe failed\n", __func__);
		return ;
	}

	printf ("Probe Success\n");

	// read
	if (0) {
		memset (buf, 0, sizeof(buf));
		ret = spi_flash_readdata(fd, 0, buf, sizeof(buf));
		printf ("%s, read ret : %d\n", __func__, ret);
		display_mem (buf, sizeof(buf), "readbuf");
	}

	// write
	if (0) {
		unsigned int wr_addr = 0x100000;

		memset (buf, 0, sizeof(buf));
		ret = spi_flash_readdata(fd, wr_addr, buf, sizeof(buf));
		printf ("%s, read ret : %d\n", __func__, ret);
		display_mem (buf, sizeof(buf), "readbuf");

		for (i = 0; i < sizeof(buf); i++)
			buf[i] = i & 0xff;
		ret = spi_flash_pageprogram(fd, wr_addr, buf, sizeof(buf));
		printf ("%s, write ret : %d\n", __func__, ret);

		memset (buf, 0, sizeof(buf));
		ret = spi_flash_readdata(fd, wr_addr, buf, sizeof(buf));
		printf ("%s, read ret : %d\n", __func__, ret);
		display_mem (buf, sizeof(buf), "readbuf");
	}

	// erase
	if (0) {
		unsigned int wr_addr = 0x100000;

		memset (buf, 0, sizeof(buf));
		ret = spi_flash_readdata(fd, wr_addr, buf, sizeof(buf));
		printf ("%s, read ret : %d\n", __func__, ret);
		display_mem (buf, sizeof(buf), "readbuf");

		ret = spi_flash_erasedata(fd, wr_addr, sizeof(buf));
		printf ("%s, erase ret : %d\n", __func__, ret);

		memset (buf, 0, sizeof(buf));
		ret = spi_flash_readdata(fd, wr_addr, buf, sizeof(buf));
		printf ("%s, read ret : %d\n", __func__, ret);
		display_mem (buf, sizeof(buf), "readbuf");
	}

	// bad info
	if (0) {
		ret = spi_flash_badinfo(fd);
		printf ("get bad block info, ret : %d\n", ret);
	}

	return ;
}

static void flash_api_test(void)
{
	printf ("------------ Start  Test ------------\n");
	test_code();
	printf ("------------ Finish Test ------------\n");

	return ;
}
