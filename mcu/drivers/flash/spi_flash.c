/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * spi_flash.c: public spi nor/nand flash driver
 *
 */

#include <stdio.h>
#include <string.h>
#include <board_config.h>
#include <driver/spi_device.h>
#include <driver/spi_flash.h>
#include <string.h>
#include "driver/flash.h"

static struct flash_dev *fdev = NULL;
int SpiFlashInitPure(void *dma_buffer, int dma_bufsize)
{
    fdev = spi_flash_probe(CONFIG_SF_SAMPLE_DELAY, 0, CONFIG_SF_DEFAULT_SPEED, 0x800);

    if (fdev == NULL)
        return -1;
    else
        return 0;
}

int SpiFlashReadPure(unsigned int addr, unsigned char *data, unsigned int len)
{
    return spi_flash_readdata(fdev, addr, data, len);
}

int SpiFlashInit(void *dma_buffer, int dma_bufsize)
{
    fdev = spi_flash_probe(CONFIG_SF_SAMPLE_DELAY, 0, CONFIG_SF_DEFAULT_SPEED, 0x800);

    if (fdev == NULL)
        return -1;
    else
        return 0;
}

int SpiFlashRead(unsigned int addr, unsigned char *data, unsigned int len)
{
#ifdef CONFIG_MCU_ENABLE_XIP
    memcpy(data, (const void *)(MCU_FLASH_XIP_BASE + addr), len);
    return 0;
#else
    return spi_flash_readdata(fdev, addr, data, len);
#endif
}

int SpiFlashWrite(unsigned int addr, unsigned char *data, unsigned int len)
{
    return spi_flash_pageprogram(fdev, addr, data, len);
}

int SpiFlashProgram(unsigned int addr, unsigned char *data, unsigned int len)
{
    return spi_flash_pageprogram(fdev, addr, data, len);
}

int SpiFlashLock(void)
{
    return spi_flash_write_protect_lock(fdev, 0);
}

int SpiFlashUnlock(void)
{
    return spi_flash_write_protect_unlock(fdev);
}

int SpiFlashErase(unsigned int addr, unsigned int len)
{
    return spi_flash_erasedata(fdev, addr, len);
}

int SpiFlashChiperase(void)
{
    return spi_flash_chiperase(fdev);
}

int SpiFlashDone(void)
{
    return 0;
}
