/*****************************************
  Copyright (c) 2003-2019
  Nationalchip Science & Technology Co., Ltd. All Rights Reserved
  Proprietary and Confidential
 *****************************************/

#include <stdio.h>
#include "autoconf.h"
#include <driver/spi_device.h>
#include <driver/spi_flash.h>
#include "driver/flash.h"
#include "driver/delay.h"

#ifdef CONFIG_GX8010NRE
#include <misc_regs.h>
#endif

#define GXSCPU_COMMON_BOOT_MODE (0xa030a000 + 0x01f8)

#ifdef CONFIG_GX8010NRE
#define SPI_CS_ADDR             (0xa030a168)
#elif defined CONFIG_GX8008B
#define SPI_CS_ADDR             (0xa030a164)
#endif

extern struct flash_dev spi_nor_flash_dev;
extern struct flash_dev spi_nand_flash_dev;
static struct spi_device  spi_flash_device;

static int set_flash_cs(int enable)
{
	if (enable == CS_ENABLE)
		writel(0, SPI_CS_ADDR);
	else
		writel(1, SPI_CS_ADDR);

	return 0;
}

static inline int is_timeout(unsigned long long time_start)
{
	return (get_time_us() - time_start) >= 5000000;
}

struct flash_dev *spi_flash_probe(unsigned int sample, unsigned int cs,
		unsigned int max_hz, unsigned int spi_mode)
{
	struct flash_dev *spi_flash_dev = NULL;

	spi_flash_device.freq = max_hz;
	spi_flash_device.sample_dly = sample;
	spi_flash_device.set_cs = set_flash_cs;
#ifdef  CONFIG_MCU_ENABLE_SPI_DMA_FOR_FLASH
	spi_flash_device.mode = SPI_DMA;
#else
	spi_flash_device.mode = 0;
#endif

#ifndef CONFIG_GX8008B
	spi_device_init(&spi_flash_device);
#endif

#ifdef CONFIG_GX8010NRE
	volatile BOOT_MODE *mode;
	mode = (volatile BOOT_MODE *)GXSCPU_COMMON_BOOT_MODE;
	if(mode->flash_boot == BOOT_MODE_SPI_NAND){
		return spi_nand_flash_dev.init(&spi_flash_device);
	}else if(mode->flash_boot == BOOT_MODE_SPI_NOR_3B ||
		mode->flash_boot == BOOT_MODE_SPI_NOR_4B){
		return spi_nor_flash_dev.init(&spi_flash_device);
	}
#endif

#ifdef CONFIG_GX8008B
	extern struct flash_dev flash_spi_dev;
	unsigned long long time_start;

	time_start = get_time_us();

	while(1) {
		spi_flash_dev = flash_spi_dev.init(&spi_flash_device);

		if (spi_flash_dev != NULL)
			return spi_flash_dev;

		if (is_timeout(time_start)) {
			printf("flash probe waitting timeout.\n");
			return NULL;
		}
	}
#else
	spi_flash_dev = spi_nor_flash_dev.init(&spi_flash_device);
	if (spi_flash_dev != NULL)
		return spi_flash_dev;
	spi_flash_dev = spi_nand_flash_dev.init(&spi_flash_device);
	if (spi_flash_dev != NULL)
		return spi_flash_dev;
#endif

	return spi_flash_dev;
}

inline int spi_flash_readdata(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	return (devp->readdata != NULL) ? devp->readdata(addr, data, len) : -1;
}

inline int spi_flash_chiperase (struct flash_dev *devp)
{
	return devp->chiperase ? devp->chiperase() : -1;
}

inline int spi_flash_erasedata(struct flash_dev *devp, unsigned int addr, unsigned int len)
{
	return devp->erasedata ? devp->erasedata(addr, len) : -1;
}

inline int spi_flash_pageprogram(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->pageprogram != NULL)
		return devp->pageprogram(addr, data, len);
	else
		return -1;
}

inline void spi_flash_sync(struct flash_dev *devp)
{
	if(devp->sync != NULL)
		devp->sync();
}

#ifdef CONFIG_MTD_TESTS
inline int spi_flash_readdata_noskip(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->readdata_noskip != NULL)
		return devp->readdata_noskip(addr, data, len);
	else
		return -1;
}

inline void spi_flash_erasedata_noskip(struct flash_dev *devp, unsigned int addr, unsigned int len)
{
	if(devp->erasedata_noskip != NULL)
		devp->erasedata_noskip(addr, len);
}

inline int spi_flash_pageprogram_noskip(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->pageprogram_noskip != NULL)
		return devp->pageprogram_noskip(addr, data, len);
	else
		return -1;
}
#endif

inline void spi_flash_test(struct flash_dev *devp, int argc, char *argv[])
{
	if(devp->test != NULL)
		devp->test(argc, argv);
}

inline void spi_flash_calcblockrange(struct flash_dev *devp, unsigned int addr, unsigned int len, unsigned int *pstart, unsigned int *pend)
{
	if(devp->calcblockrange != NULL)
		devp->calcblockrange(addr, len, pstart, pend);
}

inline int spi_flash_badinfo(struct flash_dev *devp)
{
	if(devp->badinfo != NULL)
		return devp->badinfo();
	else
		return -1;
}

inline int spi_flash_pageprogram_yaffs2(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->pageprogram_yaffs2 != NULL)
		return devp->pageprogram_yaffs2(addr, data, len);
	else {
		printf("func(%s) is NULL, please check.\n", __func__);
		return -1;
	}
}

inline int spi_flash_readoob(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->readoob != NULL)
		return devp->readoob(addr, data, len);
	else
		return -1;
}

inline int spi_flash_writeoob(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->writeoob != NULL)
		return devp->writeoob(addr, data, len);
	else
		return -1;
}

char * spi_flash_gettype(struct flash_dev *devp)
{
	if(devp->gettype != NULL)
		return devp->gettype();
	else
		return NULL;
}
int spi_flash_getsize(struct flash_dev *devp, enum spi_flash_info flash_info)
{
	if(devp->getsize != NULL)
		return devp->getsize(flash_info);
	else
		return -1;
}

int spi_flash_write_protect_mode(struct flash_dev *devp)
{
	if(devp->write_protect_mode != NULL)
		return devp->write_protect_mode();
	else
		return -1;
}

int spi_flash_write_protect_status(struct flash_dev *devp)
{
	if(devp->write_protect_status != NULL)
		return devp->write_protect_status();
	else
		return -1;
}

int spi_flash_write_protect_lock(struct flash_dev *devp, unsigned long addr)
{
	if(devp->write_protect_lock != NULL)
		return devp->write_protect_lock(addr);
	else
		return -1;
}

int spi_flash_write_protect_unlock(struct flash_dev *devp)
{
	if(devp->write_protect_unlock != NULL)
		return devp->write_protect_unlock();
	else
		return -1;
}

int spi_flash_block_isbad(struct flash_dev *devp, unsigned int addr)
{
	return devp->block_isbad ? devp->block_isbad(addr) : 0;
}

int spi_flash_block_markbad(struct flash_dev *devp, unsigned int addr)
{
	if (devp->block_markbad != NULL)
		return devp->block_markbad(addr);
	else
		return -1;
}


int spi_flash_otp_lock(struct flash_dev *devp)
{
	if(devp->otp_lock != NULL)
		return devp->otp_lock();
	else
		return -1;
}

int spi_flash_otp_status(struct flash_dev *devp, unsigned char *data)
{
	if(devp->otp_status != NULL)
		return devp->otp_status(data);
	else
		return -1;
}

int spi_flash_otp_erase(struct flash_dev *devp)
{
	if(devp->otp_erase != NULL)
		return devp->otp_erase();
	else
		return -1;
}

int spi_flash_otp_write(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len)
{
	if(devp->otp_write != NULL)
		return devp->otp_write(addr, data, len);
	else
		return -1;
}

int spi_flash_otp_read(struct flash_dev *devp, unsigned int addr, unsigned char* data, unsigned int len)
{
	if(devp->otp_read != NULL)
		return devp->otp_read(addr, data, len);
	return -1;
}

int spi_flash_otp_get_region(struct flash_dev *devp, unsigned int *region)
{
	if(devp->otp_get_region != NULL)
		return devp->otp_get_region(region);
	else
		return -1;
}

int spi_flash_otp_set_region(struct flash_dev *devp, unsigned int region)
{
	if(devp->otp_set_region != NULL)
		return devp->otp_set_region(region);
	else
		return -1;
}

/**
 * [spi_flash_calc_phy_offset 从start开始，计算逻辑偏移对应flash中的实际物理偏移]
 * @devp         [an flash device]
 * @start        [flash 起始地址]
 * @logic_offset [相对于起始地址的逻辑偏移]
 * @phy_offset   [计算出想对于起始地址的物理偏移]
 * @return       [成功 0, 失败 -1, 偏移地址为按page对齐返回 1]
 */
int spi_flash_calc_phy_offset(struct flash_dev *devp,
	unsigned int phy_start, unsigned int logic_offset, unsigned int *phy_offset)
{
	if(devp->calc_phy_offset != NULL)
		return devp->calc_phy_offset(phy_start, logic_offset, phy_offset);
	else
		*phy_offset = logic_offset;
	return 0;
}

/**
 * [spi_flash_logic_read description]
 * @devp       [an flash device]
 * @phy_start  [flash 起始地址]
 * @logic_offs [相对于起始地址的逻辑偏移]
 * @buf        [读取缓存]
 * @len        [读取长度]
 * @return     [成功 0, 失败 -1]
 */
int spi_flash_logic_read(struct flash_dev *devp,
	unsigned int phy_start, unsigned int logic_offs, void *buf, unsigned int len)
{
	unsigned int offset = 0;
	int ret;

	ret = spi_flash_calc_phy_offset(devp, phy_start, logic_offs, &offset);

	if(ret < 0)
		return ret;

	ret = spi_flash_readdata(devp, phy_start + offset, buf, len);

	return ret >= 0 ? 0 : -1;
}

/**
 * [spi_flash_logic_program description]
 * @devp       [an flash device]
 * @phy_start  [flash 起始地址]
 * @logic_offs [相对于起始地址的逻辑偏移]
 * @buf        [写缓存]
 * @len        [写缓存长度]
 * @return     [成功 0, 失败 -1]
 */
int spi_flash_logic_program(struct flash_dev *devp,
	unsigned int phy_start, unsigned int logic_offs, void *buf, unsigned int len)
{
	unsigned int offset = 0;
	int ret;

	ret = spi_flash_calc_phy_offset(devp, phy_start, logic_offs, &offset);

	if(ret != 0)
		return -1;
	ret = spi_flash_pageprogram(devp, phy_start + offset, buf, len);

	return ret >= 0 ? 0 : -1;
}

int spi_flash_area_erase(struct flash_dev *devp,
	unsigned int phy_start, unsigned int phy_end)
{
	unsigned int blk_size = 0, erase_start;

	if(!devp->getsize || !devp->erasedata)
		return -1;

	blk_size = devp->getsize(SPI_FLASH_SIZE_BLOCK);

	if((phy_start + phy_end) & (blk_size -1)){
		printf("%s: phy_start or phy_end address is not aligned by block!\n");
		return -1;
	}

	erase_start = phy_start;
	while(erase_start < phy_end){
		if(!devp->block_isbad(erase_start))
			devp->erasedata(erase_start, blk_size);

		erase_start += blk_size;
	}

	return 0;
}
