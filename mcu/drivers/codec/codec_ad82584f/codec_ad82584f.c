/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * codec_ad82584f.c: CODEC Driver for Ad82584f
 *
 */
#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/gpio.h>
#include <driver/delay.h>

#include <autoconf.h>

#include <stdio.h>

#include <base_addr.h>
#include <audio_in_regs.h>
#include <misc_regs.h>

#include <driver/audio_mic.h>
#include <driver/irq.h>
#include <driver/delay.h>
#include <driver/clock.h>

static const int _i2c_dev_addr = 0x31;

static unsigned int _i2c_bus;
static unsigned int _gpio_dsp_rst;

#define STATE_CTL_3                      0x02
#define MVOL                             0x03
#define C1VOL                            0x04
#define C2VOL                            0x05
#define CFADDR                           0x1d
#define A1CF1                            0x1e
#define A1CF2                            0x1f
#define A1CF3                            0x20
#define CFUD                             0x2d
#define DEVICE_ID                        0x37

#define AD82584F_REGISTER_COUNT          134
#define AD82584F_RAM_TABLE_COUNT         128

struct reg_default {
    unsigned int reg;
    unsigned int def;
};

static int m_reg_tab[AD82584F_REGISTER_COUNT][2] = {
    {0x00, 0x04},//##State_Control_1
    {0x01, 0x81},//##State_Control_2
    {0x02, 0x00},//##State_Control_3
    {0x03, 0x26},//##Master_volume_control
    {0x04, 0x00},//##Channel_1_volume_control
    {0x05, 0x00},//##Channel_2_volume_control
    {0x06, 0x18},//##Channel_3_volume_control
    {0x07, 0x18},//##Channel_4_volume_control
    {0x08, 0x18},//##Channel_5_volume_control
    {0x09, 0x18},//##Channel_6_volume_control
    {0x0a, 0x10},//##Bass_Tone_Boost_and_Cut
    {0x0b, 0x10},//##treble_Tone_Boost_and_Cut
    {0x0c, 0x98},//##State_Control_4
    {0x0d, 0x00},//##Channel_1_configuration_registers
    {0x0e, 0x00},//##Channel_2_configuration_registers
    {0x0f, 0x00},//##Channel_3_configuration_registers
    {0x10, 0x00},//##Channel_4_configuration_registers
    {0x11, 0x00},//##Channel_5_configuration_registers
    {0x12, 0x00},//##Channel_6_configuration_registers
    {0x13, 0x00},//##Channel_7_configuration_registers
    {0x14, 0x00},//##Channel_8_configuration_registers
    {0x15, 0x69},//##DRC1_limiter_attack/release_rate
    {0x16, 0x6a},//##DRC2_limiter_attack/release_rate
    {0x17, 0x6a},//##DRC3_limiter_attack/release_rate
    {0x18, 0x6a},//##DRC4_limiter_attack/release_rate
    {0x19, 0x06},//##Error_Delay
    {0x1a, 0x72},//##State_Control_5
    {0x1b, 0x01},//##HVUV_selection
    {0x1c, 0x00},//##State_Control_6
    {0x1d, 0x7f},//##Coefficient_RAM_Base_Address
    {0x1e, 0x00},//##Top_8-bits_of_coefficients_A1
    {0x1f, 0x00},//##Middle_8-bits_of_coefficients_A1
    {0x20, 0x00},//##Bottom_8-bits_of_coefficients_A1
    {0x21, 0x00},//##Top_8-bits_of_coefficients_A2
    {0x22, 0x00},//##Middle_8-bits_of_coefficients_A2
    {0x23, 0x00},//##Bottom_8-bits_of_coefficients_A2
    {0x24, 0x00},//##Top_8-bits_of_coefficients_B1
    {0x25, 0x00},//##Middle_8-bits_of_coefficients_B1
    {0x26, 0x00},//##Bottom_8-bits_of_coefficients_B1
    {0x27, 0x00},//##Top_8-bits_of_coefficients_B2
    {0x28, 0x00},//##Middle_8-bits_of_coefficients_B2
    {0x29, 0x00},//##Bottom_8-bits_of_coefficients_B2
    {0x2a, 0x20},//##Top_8-bits_of_coefficients_A0
    {0x2b, 0x00},//##Middle_8-bits_of_coefficients_A0
    {0x2c, 0x00},//##Bottom_8-bits_of_coefficients_A0
    {0x2d, 0x40},//##Coefficient_R/W_control
    {0x2e, 0x00},//##Protection_Enable/Disable
    {0x2f, 0x00},//##Memory_BIST_status
    {0x30, 0x00},//##Power_Stage_Status(Read_only)
    {0x31, 0x00},//##PWM_Output_Control
    {0x32, 0x00},//##Test_Mode_Control_Reg.
    {0x33, 0x6d},//##Qua-Ternary/Ternary_Switch_Level
    {0x34, 0x00},//##Volume_Fine_tune
    {0x35, 0x00},//##Volume_Fine_tune
    {0x36, 0x60},//##OC_bypass_&_GVDD_selection
    {0x37, 0x52},//##Device_ID_register
    {0x38, 0x00},//##RAM1_test_register_address
    {0x39, 0x00},//##Top_8-bits_of_RAM1_Data
    {0x3a, 0x00},//##Middle_8-bits_of_RAM1_Data
    {0x3b, 0x00},//##Bottom_8-bits_of_RAM1_Data
    {0x3c, 0x00},//##RAM1_test_r/w_control
    {0x3d, 0x00},//##RAM2_test_register_address
    {0x3e, 0x00},//##Top_8-bits_of_RAM2_Data
    {0x3f, 0x00},//##Middle_8-bits_of_RAM2_Data
    {0x40, 0x00},//##Bottom_8-bits_of_RAM2_Data
    {0x41, 0x00},//##RAM2_test_r/w_control
    {0x42, 0x00},//##Level_Meter_Clear
    {0x43, 0x00},//##Power_Meter_Clear
    {0x44, 0x7f},//##TOP_of_C1_Level_Meter
    {0x45, 0xff},//##Middle_of_C1_Level_Meter
    {0x46, 0xff},//##Bottom_of_C1_Level_Meter
    {0x47, 0x7f},//##TOP_of_C2_Level_Meter
    {0x48, 0xff},//##Middle_of_C2_Level_Meter
    {0x49, 0xff},//##Bottom_of_C2_Level_Meter
    {0x4a, 0x00},//##TOP_of_C3_Level_Meter
    {0x4b, 0x00},//##Middle_of_C3_Level_Meter
    {0x4c, 0x00},//##Bottom_of_C3_Level_Meter
    {0x4d, 0x00},//##TOP_of_C4_Level_Meter
    {0x4e, 0x00},//##Middle_of_C4_Level_Meter
    {0x4f, 0x00},//##Bottom_of_C4_Level_Meter
    {0x50, 0x00},//##TOP_of_C5_Level_Meter
    {0x51, 0x00},//##Middle_of_C5_Level_Meter
    {0x52, 0x00},//##Bottom_of_C5_Level_Meter
    {0x53, 0x00},//##TOP_of_C6_Level_Meter
    {0x54, 0x00},//##Middle_of_C6_Level_Meter
    {0x55, 0x00},//##Bottom_of_C6_Level_Meter
    {0x56, 0x00},//##TOP_of_C7_Level_Meter
    {0x57, 0x00},//##Middle_of_C7_Level_Meter
    {0x58, 0x00},//##Bottom_of_C7_Level_Meter
    {0x59, 0x00},//##TOP_of_C8_Level_Meter
    {0x5a, 0x00},//##Middle_of_C8_Level_Meter
    {0x5b, 0x00},//##Bottom_of_C8_Level_Meter
    {0x5c, 0x06},//##I2S_Data_Output_Selection_Register
    {0x5d, 0x00},//##Reserve
    {0x5e, 0x00},//##Reserve
    {0x5f, 0x00},//##Reserve
    {0x60, 0x00},//##Reserve
    {0x61, 0x00},//##Reserve
    {0x62, 0x00},//##Reserve
    {0x63, 0x00},//##Reserve
    {0x64, 0x00},//##Reserve
    {0x65, 0x00},//##Reserve
    {0x66, 0x00},//##Reserve
    {0x67, 0x00},//##Reserve
    {0x68, 0x00},//##Reserve
    {0x69, 0x00},//##Reserve
    {0x6a, 0x00},//##Reserve
    {0x6b, 0x00},//##Reserve
    {0x6c, 0x00},//##Reserve
    {0x6d, 0x00},//##Reserve
    {0x6e, 0x00},//##Reserve
    {0x6f, 0x00},//##Reserve
    {0x70, 0x00},//##Reserve
    {0x71, 0x00},//##Reserve
    {0x72, 0x00},//##Reserve
    {0x73, 0x00},//##Reserve
    {0x74, 0x30},//##Mono_Key_High_Byte
    {0x75, 0x06},//##Mono_Key_Low_Byte
    {0x76, 0x00},//##Boost_Control
    {0x77, 0x07},//##Hi-res_Item
    {0x78, 0x40},//##Test_Mode_register
    {0x79, 0x62},//##Boost_Strap_OV/UV_Selection
    {0x7a, 0x88},//##OC_Selection_2
    {0x7b, 0x55},//##MBIST_User_Program_Top_Byte_Even
    {0x7c, 0x55},//##MBIST_User_Program_Middle_Byte_Even
    {0x7d, 0x55},//##MBIST_User_Program_Bottom_Byte_Even
    {0x7e, 0x55},//##MBIST_User_Program_Top_Byte_Odd
    {0x7f, 0x55},//##MBIST_User_Program_Middle_Byte_Odd
    {0x80, 0x55},//##MBIST_User_Program_Bottom_Byte_Odd
    {0x81, 0x00},//##ERROR_clear_register
    {0x82, 0x0c},//##Minimum_duty_test
    {0x83, 0x06},//##Reserve
    {0x84, 0xfe},//##Reserve
    {0x85, 0x4a},//##Reserve

};

static int _ReadCmd(uint8_t *data, unsigned int len)
{
    void  *i2c_dev;
    int status = -1;
    struct i2c_msg msg[2];

    msg[0].addr  = _i2c_dev_addr;
    msg[0].buf   = &data[0];
    msg[0].len   = len;
    msg[0].flags = 0;

    msg[1].addr  = _i2c_dev_addr;
    msg[1].buf   = &data[len];
    msg[1].len   = len;
    msg[1].flags = 1;

    i2c_dev = gx_i2c_open(_i2c_bus);
    if (!i2c_dev) {
        printf("Failed to open I2C Chip 0x%x at Bus %d!\n", _i2c_dev_addr,  _i2c_bus);
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);

    return status != 0 ? -1 : 0;
}

static int _WriteCmd(uint8_t *data, uint16_t len)
{
    void  *i2c_dev;
    struct i2c_msg msg;
    int    status;

    msg.addr  = _i2c_dev_addr;
    msg.len   = len;
    msg.buf   = data;
    msg.flags = 0;

    i2c_dev = gx_i2c_open(_i2c_bus);
    if (!i2c_dev) {
        printf("Failed to open I2C Chip 0x%x at Bus %d!\n", _i2c_dev_addr,  _i2c_bus);
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, &msg, 1);
    gx_i2c_close(i2c_dev);

    return status != 0 ? -1 : 0;
}

#if 0
static int _PowerOn(void)
{
    return 0;
}
#endif

static int _PowerOff(void)
{
    return 0;
}

static int _Init(void)
{
    int ret = 0;
    int i = 0;

    unsigned char device_id[2] = {DEVICE_ID, 0x00};
    unsigned char data[2];

    printf("### %s,%d,ad82584f init\n", __func__, __LINE__);

    GpioSetDirection(_gpio_dsp_rst, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(_gpio_dsp_rst, GPIO_LEVEL_HIGH);

    ret = _ReadCmd(device_id, 1);
    if (ret < 0) {
        printf("Failed to read device id from the ad82584f: %d\n", ret);
        return ret;
    }
    if (device_id[1] != 0x52) {
        printf("Not a ad82584f chip =0x%x\n", device_id[1]);
        return -1;
    }

    for (i = 0; i < AD82584F_REGISTER_COUNT; i ++) {
        data[0] = m_reg_tab[i][0];
        data[1] = m_reg_tab[i][1];
        ret = _WriteCmd(data, 2);
        if (ret < 0) {
            printf("### %s,%d,Fail to write ad82584f_reg,ret =%d,index=%d\n\n", __func__, __LINE__, ret, i);
            return -1;
        }
    }

    data[0] = STATE_CTL_3;//--unmute amp
    data[1] = 0x00;
    ret = _WriteCmd(data, 2);
    if (ret < 0) {
        printf("### %s,%d,Fail to write ad82584f_reg,ret =%d\n", __func__, __LINE__, ret);
        return -1;
    }
    return ret;
}

static int _Reset(void)
{
    _PowerOff();

    return 0;
}

int CodecAd82584fInit(unsigned int i2c_bus,unsigned int gpio_dsp_reset)
{
    _i2c_bus = i2c_bus;
    _gpio_dsp_rst = gpio_dsp_reset;

    printf("Ad82584f init start\n");
    if (_Init() < 0)
        return -1;
    printf("Ad82584f init success\n");
    return 0;
}

int CodecAd82584fDone(void)
{
    _Reset();

    return 0;
}
