#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/delay.h>
#include <driver/codec_nau8822.h>

/*
 * Register values.
 */
#define NAU8822_REG_RESET                       0x00
#define NAU8822_REG_POWER_MANAGEMENT_1          0x01
#define NAU8822_REG_POWER_MANAGEMENT_2          0x02
#define NAU8822_REG_POWER_MANAGEMENT_3          0x03
#define NAU8822_REG_AUDIO_INTERFACE             0x04
#define NAU8822_REG_COMPANDING_CONTROL          0x05
#define NAU8822_REG_CLOCKING                    0x06
#define NAU8822_REG_ADDITIONAL_CONTROL          0x07
#define NAU8822_REG_GPIO_CONTROL                0x08
#define NAU8822_REG_JACK_DETECT_CONTROL_1       0x09
#define NAU8822_REG_DAC_CONTROL                 0x0A
#define NAU8822_REG_LEFT_DAC_DIGITAL_VOLUME	    0x0B
#define NAU8822_REG_RIGHT_DAC_DIGITAL_VOLUME    0x0C
#define NAU8822_REG_JACK_DETECT_CONTROL_2       0x0D
#define NAU8822_REG_ADC_CONTROL                 0x0E
#define NAU8822_REG_LEFT_ADC_DIGITAL_VOLUME     0x0F
#define NAU8822_REG_RIGHT_ADC_DIGITAL_VOLUME    0x10
#define NAU8822_REG_EQ1                         0x12
#define NAU8822_REG_EQ2                         0x13
#define NAU8822_REG_EQ3                         0x14
#define NAU8822_REG_EQ4                         0x15
#define NAU8822_REG_EQ5                         0x16
#define NAU8822_REG_DAC_LIMITER_1               0x18
#define NAU8822_REG_DAC_LIMITER_2               0x19
#define NAU8822_REG_NOTCH_FILTER_1              0x1B
#define NAU8822_REG_NOTCH_FILTER_2              0x1C
#define NAU8822_REG_NOTCH_FILTER_3              0x1D
#define NAU8822_REG_NOTCH_FILTER_4              0x1E
#define NAU8822_REG_ALC_CONTROL_1               0x20
#define NAU8822_REG_ALC_CONTROL_2               0x21
#define NAU8822_REG_ALC_CONTROL_3               0x22
#define NAU8822_REG_NOISE_GATE                  0x23
#define NAU8822_REG_PLL_N                       0x24
#define NAU8822_REG_PLL_K1                      0x25
#define NAU8822_REG_PLL_K2                      0x26
#define NAU8822_REG_PLL_K3                      0x27
#define NAU8822_REG_3D_CONTROL                  0x29
#define NAU8822_REG_R_SPEAKER_CONTROL           0x2B
#define NAU8822_REG_INPUT_CONTROL               0x2C
#define NAU8822_REG_LEFT_INP_PGA_CONTROL        0x2D
#define NAU8822_REG_RIGHT_INP_PGA_CONTROL       0x2E
#define NAU8822_REG_LEFT_ADC_BOOST_CONTROL      0x2F
#define NAU8822_REG_RIGHT_ADC_BOOST_CONTROL     0x30
#define NAU8822_REG_OUTPUT_CONTROL              0x31
#define NAU8822_REG_LEFT_MIXER_CONTROL          0x32
#define NAU8822_REG_RIGHT_MIXER_CONTROL         0x33
#define NAU8822_REG_LHP_VOLUME                  0x34
#define NAU8822_REG_RHP_VOLUME                  0x35
#define NAU8822_REG_LSPKOUT_VOLUME              0x36
#define NAU8822_REG_RSPKOUT_VOLUME              0x37
#define NAU8822_REG_AUX2_MIXER                  0x38
#define NAU8822_REG_AUX1_MIXER                  0x39
#define NAU8822_REG_POWER_MANAGEMENT_4          0x3A
#define NAU8822_REG_LEFT_TIME_SLOT              0x3B
#define NAU8822_REG_MISC                        0x3C
#define NAU8822_REG_RIGHT_TIME_SLOT             0x3D
#define NAU8822_REG_DEVICE_REVISION             0x3E
#define NAU8822_REG_DEVICE_ID                   0x3F
#define NAU8822_REG_DAC_DITHER                  0x41
#define NAU8822_REG_ALC_ENHANCE_1               0x46
#define NAU8822_REG_ALC_ENHANCE_2               0x47
#define NAU8822_REG_192KHZ_SAMPLING             0x48
#define NAU8822_REG_MISC_CONTROL                0x49
#define NAU8822_REG_INPUT_TIEOFF                0x4A
#define NAU8822_REG_POWER_REDUCTION             0x4B
#define NAU8822_REG_AGC_PEAK2PEAK               0x4C
#define NAU8822_REG_AGC_PEAK_DETECT             0x4D
#define NAU8822_REG_AUTOMUTE_CONTROL            0x4E
#define NAU8822_REG_OUTPUT_TIEOFF               0x4F
#define NAU8822_REG_MAX_REGISTER                NAU8822_REG_OUTPUT_TIEOFF

#define NAU8822_REFIMP_80K                      0x01
#define NAU8822_REFIMP_MASK                     0x03

typedef struct {
    unsigned int addr;
    unsigned int data;
} CODEC_NAU8822_REG;

/* codec private data */
typedef enum {
    CODEC_STANDBY = 0x00,
    CODEC_ON      = 0x01,
    CODEC_OFF     = 0x02,
    CODEC_PREPARE = 0x03,
} CODEC_NAU8822_STATUS;

static const int _i2c_dev_addr = 0x1a;
static unsigned int _i2c_bus;

static CODEC_NAU8822_STATUS _status;
//48K 32BIT MCLK 12.288M
static const CODEC_NAU8822_REG _RegInit[] = {
    { NAU8822_REG_POWER_MANAGEMENT_1,       0x0000 },
    { NAU8822_REG_POWER_MANAGEMENT_2,       0x0000 },
    { NAU8822_REG_POWER_MANAGEMENT_3,       0x0000 },
    { NAU8822_REG_AUDIO_INTERFACE,          0x0070 },
    { NAU8822_REG_COMPANDING_CONTROL,       0x0000 },
    { NAU8822_REG_CLOCKING,                 0x000C },
    { NAU8822_REG_ADDITIONAL_CONTROL,       0x0000 },
    { NAU8822_REG_GPIO_CONTROL,             0x0000 },
    { NAU8822_REG_JACK_DETECT_CONTROL_1,    0x0000 },
    { NAU8822_REG_DAC_CONTROL,              0x0048 },
    { NAU8822_REG_LEFT_DAC_DIGITAL_VOLUME,  0x01ff },
    { NAU8822_REG_RIGHT_DAC_DIGITAL_VOLUME, 0x01ff },
    { NAU8822_REG_JACK_DETECT_CONTROL_2,    0x0000 },
    { NAU8822_REG_ADC_CONTROL,              0x0100 },
    { NAU8822_REG_LEFT_ADC_DIGITAL_VOLUME,  0x01ff },
    { NAU8822_REG_RIGHT_ADC_DIGITAL_VOLUME, 0x01ff },
    { 17,                                   0x0000 },
    { NAU8822_REG_EQ1,                      0x012c },
    { NAU8822_REG_EQ2,                      0x002c },
    { NAU8822_REG_EQ3,                      0x002c },
    { NAU8822_REG_EQ4,                      0x002c },
    { NAU8822_REG_EQ5,                      0x002c },
    { 23,                                   0x0000 },
    { NAU8822_REG_DAC_LIMITER_1,            0x0032 },
    { NAU8822_REG_DAC_LIMITER_2,            0x0000 },
    { 26,                                   0x0000 },
    { NAU8822_REG_NOTCH_FILTER_1,           0x0000 },
    { NAU8822_REG_NOTCH_FILTER_2,           0x0000 },
    { NAU8822_REG_NOTCH_FILTER_3,           0x0000 },
    { NAU8822_REG_NOTCH_FILTER_4,           0x0000 },
    { 31,                                   0x0000 },
    { NAU8822_REG_ALC_CONTROL_1,            0x0038 },
    { NAU8822_REG_ALC_CONTROL_2,            0x000b },
    { NAU8822_REG_ALC_CONTROL_3,            0x0032 },
    { NAU8822_REG_NOISE_GATE,               0x0010 },
    { NAU8822_REG_PLL_N,                    0x0008 },
    { NAU8822_REG_PLL_K1,                   0x000c },
    { NAU8822_REG_PLL_K2,                   0x0093 },
    { NAU8822_REG_PLL_K3,                   0x00e9 },
    { 40,                                   0x0000 },
    { NAU8822_REG_3D_CONTROL,               0x0000 },
    { 42,                                   0x0000 },
    { NAU8822_REG_R_SPEAKER_CONTROL,        0x0000 },
    { NAU8822_REG_INPUT_CONTROL,            0x0000 },
    { NAU8822_REG_LEFT_INP_PGA_CONTROL,     0x0110 },
    { NAU8822_REG_RIGHT_INP_PGA_CONTROL,    0x0110 },
    { NAU8822_REG_LEFT_ADC_BOOST_CONTROL,   0x0100 },
    { NAU8822_REG_RIGHT_ADC_BOOST_CONTROL,  0x0100 },
    { NAU8822_REG_OUTPUT_CONTROL,           0x0002 },
    { NAU8822_REG_LEFT_MIXER_CONTROL,       0x0001 },
    { NAU8822_REG_RIGHT_MIXER_CONTROL,      0x0001 },
    { NAU8822_REG_LHP_VOLUME,               0x0139 },
    { NAU8822_REG_RHP_VOLUME,               0x0139 },
    { NAU8822_REG_LSPKOUT_VOLUME,           0x0139 },
    { NAU8822_REG_RSPKOUT_VOLUME,           0x0139 },
    { NAU8822_REG_AUX2_MIXER,               0x0000 },
    { NAU8822_REG_AUX1_MIXER,               0x0000 },
    { NAU8822_REG_POWER_MANAGEMENT_4,       0x0000 },
    { NAU8822_REG_LEFT_TIME_SLOT,           0x0000 },
    { NAU8822_REG_MISC,                     0x0020 },
    { NAU8822_REG_RIGHT_TIME_SLOT,          0x0000 },
    { NAU8822_REG_DEVICE_REVISION,          0x007f },
    { NAU8822_REG_DEVICE_ID,                0x001a },
    { 64,                                   0x0000 },
    { NAU8822_REG_DAC_DITHER,               0x0114 },
    { 66,                                   0x0000 },
    { 67,                                   0x0000 },
    { 68,                                   0x0000 },
    { 69,                                   0x0000 },
    { NAU8822_REG_ALC_ENHANCE_1,            0x0000 },
    { NAU8822_REG_ALC_ENHANCE_2,            0x0000 },
    { NAU8822_REG_192KHZ_SAMPLING,          0x0008 },
    { NAU8822_REG_MISC_CONTROL,             0x0000 },
    { NAU8822_REG_INPUT_TIEOFF,             0x0000 },
    { NAU8822_REG_POWER_REDUCTION,          0x0000 },
    { NAU8822_REG_AGC_PEAK2PEAK,            0x0000 },
    { NAU8822_REG_AGC_PEAK_DETECT,          0x0000 },
    { NAU8822_REG_AUTOMUTE_CONTROL,         0x0000 },
    { NAU8822_REG_OUTPUT_TIEOFF,            0x0000 },
};

static int _Read(unsigned int addr, unsigned int *data)
{
    int     status;
    uint8_t data_buf[2];

    status = gx_i2c_rx(_i2c_bus, _i2c_dev_addr, addr << 1,  data_buf, 2);

    *data |= (data_buf[0] & 0xff) << 8;
    *data |=  data_buf[1] & 0xff;

    return status != 0 ? -1 : 0;
}


static int _Write(unsigned int addr, unsigned int data)
{
    int     status;
    uint8_t addr_buf;
    uint8_t data_buf[2];

    addr_buf    = (uint8_t)(addr & 0x000000ff) << 1;
    addr_buf   |= data_buf[0];

    data_buf[0] = (uint8_t)((data & 0x0000ff00) >> 8);
    data_buf[1] = (uint8_t) (data & 0x000000ff);

    status = gx_i2c_tx(_i2c_bus, _i2c_dev_addr, addr_buf, (unsigned char*)&data_buf[1], 1);

    return status != 0 ? -1 : 0;
}


static int _UpdateBits(unsigned int addr, unsigned int mask, unsigned int value)
{
    unsigned int temp = 0;
    int ret = 0;

    ret = _Read(addr, &temp);
    if (ret != 0)
        return ret;
    temp &=(~mask);
    temp |=(value & mask);
    ret = _Write(addr, temp);

    return ret;
}

static int _SetMute(int mute)
{
    if (mute)
        _UpdateBits(NAU8822_REG_DAC_CONTROL, 0x40, 0x40);
    else
        _UpdateBits(NAU8822_REG_DAC_CONTROL, 0x40, 0);

    return 0;
}

static int _SetStatus(int status)
{
    switch (status) {
    case CODEC_ON:
    case CODEC_PREPARE:
        if (_status != CODEC_ON) {
            _UpdateBits(NAU8822_REG_POWER_MANAGEMENT_1, NAU8822_REFIMP_MASK, NAU8822_REFIMP_80K);
            _Write(NAU8822_REG_POWER_MANAGEMENT_2,       0x0180);
            _Write(NAU8822_REG_POWER_MANAGEMENT_3,       0x000F);
            _Write(NAU8822_REG_LEFT_DAC_DIGITAL_VOLUME,  0x01FF);
            _Write(NAU8822_REG_RIGHT_DAC_DIGITAL_VOLUME, 0x01FF);
            _status = CODEC_ON;
        }
        break;
    case CODEC_STANDBY:
        if (_status != CODEC_STANDBY) {
            _Write(NAU8822_REG_POWER_MANAGEMENT_1, 0x010C);
            _Write(NAU8822_REG_POWER_MANAGEMENT_2, 0x0000);
            _Write(NAU8822_REG_POWER_MANAGEMENT_3, 0x0000);
            _status = CODEC_STANDBY;
        }
        break;
    case CODEC_OFF:
        if (_status != CODEC_OFF) {
            _Write(NAU8822_REG_POWER_MANAGEMENT_1, 0x0000);
            _Write(NAU8822_REG_POWER_MANAGEMENT_2, 0x0000);
            _Write(NAU8822_REG_POWER_MANAGEMENT_3, 0x0000);
            _status = CODEC_OFF;
        }
        break;
    }

    return 0;
}

static int _Reset(void)
{
    int ret = 0;

    _SetMute(1);
    _UpdateBits(NAU8822_REG_DAC_CONTROL, 0x40, 0x40);
    ret = _Write(NAU8822_REG_RESET, 0x0000);
    if (ret != 0)
        printf("Failed to issue reset: %d\n", ret);

    return ret;
}

static int _Init(void)
{
    int i = 0;
    int ret = 0;

    ret = _Reset();
    if (ret != 0)
        return ret;

    for (i = 0; i < ARRAY_SIZE(_RegInit); i++)
        ret = _Write(_RegInit[i].addr, _RegInit[i].data);

    _SetStatus(CODEC_STANDBY);
    _SetStatus(CODEC_ON);
    _SetMute(0);

    return ret;
}

int CodecNau8822Init(unsigned int i2c_bus)
{
    _status  = -1;
    _i2c_bus = i2c_bus;

    if (_Init() < 0)
        return -1;

    return 0;
}

int CodecNau8822Done(void)
{
    _SetStatus(CODEC_OFF);

    return 0;
}
