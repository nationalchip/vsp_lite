/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * counter_timer.c: MCU Counter Driver for timer functions
 *
 */

#include <common.h>
#include <soc_config.h>
#include <base_addr.h>

#include <driver/timer.h>
#include <driver/irq.h>

#define MCU_VA_COUNTER_1_STATUS     (MCU_REG_BASE_COUNTER + 0x00)
#define MCU_VA_COUNTER_1_CONTROL    (MCU_REG_BASE_COUNTER + 0x10)
#define MCU_VA_COUNTER_1_CONFIG     (MCU_REG_BASE_COUNTER + 0x20)
#define MCU_VA_COUNTER_1_PRE        (MCU_REG_BASE_COUNTER + 0x24)
#define MCU_VA_COUNTER_1_INI        (MCU_REG_BASE_COUNTER + 0x28)

#define MCU_VA_COUNTER_PSYSCLK      (CONFIG_APB2_CLK)
#define MCU_VA_COUNTER_CLK(val)     ((MCU_VA_COUNTER_PSYSCLK / (val)) - 1)
#define CTR_INIT_VALUE              (0x0 - 1000)

#define MCU_MAX_TIMER_NUM           (3)


static struct {
    void *private_data;
    TIMER_CALLBACK callback;
    unsigned long long interval_us;
    unsigned long long due_us;
} s_timers[MCU_MAX_TIMER_NUM];

static unsigned long long s_period_us = 0;

static int _CounterTimerISR(int irq, void *pdata)
{
    // Seek the value
    for (int i = 0; i < MCU_MAX_TIMER_NUM; i++) {
        if (!s_timers[i].callback)
            continue;
        
        s_timers[i].due_us = s_timers[i].due_us >= s_period_us ? s_timers[i].due_us - s_period_us : 0;
        if (s_timers[i].due_us == 0) {
            s_timers[i].callback(s_timers[i].private_data);
            s_timers[i].due_us = s_timers[i].interval_us;
        }
    }
    
    // Clear Interrupt Status
    volatile unsigned int reg = readl(MCU_VA_COUNTER_1_STATUS);
    reg |= 0x1;
    writel(reg, MCU_VA_COUNTER_1_STATUS);
    
    return 0;
}

int TimerInit(unsigned long long period_us)
{
    writel(0x1, MCU_VA_COUNTER_1_CONTROL);
    writel(0x0, MCU_VA_COUNTER_1_CONTROL);
    writel(0x3, MCU_VA_COUNTER_1_CONFIG);
    writel(MCU_VA_COUNTER_CLK(1000000), MCU_VA_COUNTER_1_PRE);
    writel((0x0 - period_us), MCU_VA_COUNTER_1_INI);
    writel(0x2, MCU_VA_COUNTER_1_CONTROL);
    
    s_period_us = period_us;
    
    for (int i = 0; i < MCU_MAX_TIMER_NUM; i++) {
        s_timers[i].callback = NULL;
        s_timers[i].private_data = NULL;
        s_timers[i].interval_us = 0;
        s_timers[i].due_us = 0;
    }
    
    return 0;
}

void TimerDone(void)
{
    gx_free_irq(MCU_IRQ_COUNTER0);
}

int TimerRegisterCallback_us(TIMER_CALLBACK callback, void *private_data, unsigned long long interval_us)
{
    for (int i = 0; i < MCU_MAX_TIMER_NUM; i++) {
        if (!s_timers[i].callback) {    // blank slot
            s_timers[i].callback = callback;
            s_timers[i].interval_us = interval_us;
            s_timers[i].private_data = private_data;

            gx_request_irq(MCU_IRQ_COUNTER0, _CounterTimerISR, NULL);
            return i;
        }
    }

    return -1;
}

int TimerRegisterCallback(TIMER_CALLBACK callback, void *private_data, unsigned int interval_ms)
{
    unsigned long long interval_us = interval_ms * 1000;

    return TimerRegisterCallback_us(callback, private_data, interval_us);
}

int TimerUnregisterCallback(TIMER_CALLBACK callback)
{
    int timer_num = 0;
    for (int i = 0; i < MCU_MAX_TIMER_NUM; i++) {
        if (s_timers[i].callback) {
            if (s_timers[i].callback == callback) {
                s_timers[i].callback = NULL;
                s_timers[i].private_data = NULL;
                s_timers[i].interval_us = 0;
                s_timers[i].due_us = 0;
            } else {
                timer_num ++;
            }
        }
    }

#if 0
    if (timer_num == 0)
        gx_free_irq(MCU_IRQ_COUNTER0);
#endif
    return 0;
}

