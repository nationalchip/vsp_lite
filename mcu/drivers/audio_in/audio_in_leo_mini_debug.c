/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * drv_audio_in_debug.c: MCU AudioIn Driver Debug
 *
 */

#include <audio_in_regs.h>
#include <base_addr.h>

#include <stdio.h>

#include <driver/audio_mic.h>

void AudioInDumpRegs(int detail)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    printf("==========================================================\n");

    // AUDIO_IN_SOURCE_CTRL
    printf("[%08X]AUDIO_IN_SOURCE_CTRL:                   %08X\n", (uint32_t)&regs->source_ctrl.value, regs->source_ctrl.value);
    if (detail) {
        printf("    source_sel:             %d\n", regs->source_ctrl.bits.source_sel);
        printf("    filter_mag_en:          %d\n", regs->source_ctrl.bits.filter_mag_en);
        printf("    magnif_sel:             %d\n", regs->source_ctrl.bits.magnif_sel);
        printf("    i2s_channel_sel:        %d\n", regs->source_ctrl.bits.i2s_channel_sel);
        printf("    channel_work_en:        %d\n", regs->source_ctrl.bits.channel_work_en);
        printf("    i2sout_sys_gate_enable: %d\n", regs->source_ctrl.bits.i2sout_sys_gate_enable);
        printf("    else_rst_auto_enable:   %d\n", regs->source_ctrl.bits.else_rst_auto_enable);
        printf("    else_gate_auto_enable:  %d\n", regs->source_ctrl.bits.else_gate_auto_enable);
        printf("    cic_clk_inv_en:         %d\n", regs->source_ctrl.bits.cic_clk_inv_en);
        printf("    mclk_out_sel:           %d\n", regs->source_ctrl.bits.mclk_out_sel);
        printf("    else_sys_gate_enable:   %d\n", regs->source_ctrl.bits.else_sys_gate_enable);
        printf("    i2sin_sys_gate_enable:  %d\n", regs->source_ctrl.bits.i2sin_sys_gate_enable);
        printf("    dmic_sys_gate_enable:   %d\n", regs->source_ctrl.bits.dmic_sys_gate_enable);
        printf("    i2sout_test_mode:       %d\n", regs->source_ctrl.bits.i2sout_test_mode);
        printf("    i2sout_soft_rstn:       %d\n", regs->source_ctrl.bits.i2sout_soft_rstn);
        printf("    dmic_soft_rstn:         %d\n", regs->source_ctrl.bits.dmic_soft_rstn);
        printf("    i2s_soft_rstn:          %d\n", regs->source_ctrl.bits.i2s_soft_rstn);
    }

    // AUDIO_IN_CHANNEL_SEL
    printf("[%08X]AUDIO_IN_CHANNEL_SEL:                   %08X\n", (uint32_t)&regs->channel_sel.value, regs->channel_sel.value);

    // AUDIO_IN_AVAD_CTRL
    printf("[%08X]AUDIO_IN_AVAD_CTRL:                     %08X\n", (uint32_t)&regs->avad_ctrl.value, regs->avad_ctrl.value);
    if (detail) {
        printf("    frame_mode:             %d\n", regs->avad_ctrl.bits.frame_mode);
        printf("    itpara_sel:             %d\n", regs->avad_ctrl.bits.it_param_sel);
        printf("    zcr_bypass:             %d\n", regs->avad_ctrl.bits.zcr_bypass);
        printf("    read_itpara_sel:        %d\n", regs->avad_ctrl.bits.read_itpara_sel);
        printf("    set_itpara_sel:         %d\n", regs->avad_ctrl.bits.set_itpara_sel);
        printf("    ch_avad_en:             %d\n", regs->avad_ctrl.bits.ch_avad_en);
        printf("    echo_ch_avad_en:        %d\n", regs->avad_ctrl.bits.echo_ch_avad_en);
        printf("    avad_mode:              %d\n", regs->avad_ctrl.bits.avad_mode);
        printf("    avad_bypass:            %d\n", regs->avad_ctrl.bits.avad_bypass);
        printf("    amic_avad_rstn_inv_en:  %d\n", regs->avad_ctrl.bits.amic_avad_rstn_inv_en);
        printf("    avad_soft_rstn:         %d\n", regs->avad_ctrl.bits.avad_soft_rstn);
    }

    // AUDIO_IN_READ_STATE
    printf("[%08X]AUDIO_IN_READ_STATE:                    %08X\n", (uint32_t)&regs->read_state.value, regs->read_state.value);
    if (detail) {
        printf("    avad_state:             %d\n", regs->read_state.bits.avad_state);
        printf("    frame_full_0_1:         %d\n", regs->read_state.bits.frame_full_0_1);
        printf("    echo_avad_state:        %d\n", regs->read_state.bits.echo_avad_state);
        printf("    channel_valid_state:    %d\n", regs->read_state.bits.channel_data_valid_state);
        printf("    ref_channel_valid_state %d\n", regs->read_state.bits.ref_channel_data_valid_state);
    }

    // AUDIO_IN_ITL_REG
    printf("[%08X]AUDIO_IN_ITL_REG:                       %08X\n", (uint32_t)&regs->itl_reg.value, regs->itl_reg.value);

    // AUDIO_IN_ITU_REG
    printf("[%08X]AUDIO_IN_ITU_REG:                       %08X\n", (uint32_t)&regs->itu_reg.value, regs->itu_reg.value);

    // AUDIO_IN_ITL_USING
    printf("[%08X]AUDIO_IN_ITL_USING:                     %08X\n", (uint32_t)&regs->itl_using.value, regs->itl_using.value);

    // AUDIO_IN_ITU_USING
    printf("[%08X]AUDIO_IN_ITU_USING:                     %08X\n", (uint32_t)&regs->itl_using.value, regs->itl_using.value);

    // AUDIO_IN_I2S_GENCLK
    printf("[%08X]AUDIO_IN_I2S_GENCLK:                    %08X\n", (uint32_t)&regs->i2s_genclk.value, regs->i2s_genclk.value);
    if (detail) {
        printf("    adc_clk_mode:           %d\n", regs->i2s_genclk.bits.adc_clk_mode);
        printf("    bclk_sel:               %d\n", regs->i2s_genclk.bits.bit_clk_sel);
        printf("    lrclk_sel:              %d\n", regs->i2s_genclk.bits.lr_clk_sel);
        printf("    bclk_o_inv_en:          %d\n", regs->i2s_genclk.bits.bit_clk_o_inv_en);
    }

    // AUDIO_IN_I2S_ADCINFO
    printf("[%08X]AUDIO_IN_I2S_ADCINFO:                   %08X\n", (uint32_t)&regs->i2s_adcinfo.value, regs->i2s_adcinfo.value);
    if (detail) {
        printf("    data_format:            %d\n", regs->i2s_adcinfo.bits.data_format);
        printf("    bit_clk_i_inv_en:       %d\n", regs->i2s_adcinfo.bits.bit_clk_i_inv_en);
        printf("    second_sdata_en:        %d\n", regs->i2s_adcinfo.bits.second_sdata_en);
        printf("    fsync_mode:             %d\n", regs->i2s_adcinfo.bits.fsync_mode);
        printf("    lrclk_i_inv_en:         %d\n", regs->i2s_adcinfo.bits.lrclk_i_inv_en);
        printf("    pcm_length:             %d\n", regs->i2s_adcinfo.bits.pcm_length);
        printf("    i2s_source_sel:         %d\n", regs->i2s_adcinfo.bits.i2s_source_sel);
    }

    // AUDIO_IN_W_PCM_CTRL
    printf("[%08X]AUDIO_IN_W_PCM_CTRL:                    %08X\n", (uint32_t)&regs->w_pcm_ctrl.value, regs->w_pcm_ctrl.value);
    if (detail) {
        printf("    write_mode:             %d\n",   regs->w_pcm_ctrl.bits.write_mode);
        printf("    pcm_endian:             %d\n",   regs->w_pcm_ctrl.bits.pcm_endian);
        printf("    w_echo_channel_max_sel: %d\n",   regs->w_pcm_ctrl.bits.w_echo_channel_max_sel);
        printf("    write_channel_max_sel:  %d\n",   regs->w_pcm_ctrl.bits.write_channel_max_sel);
        printf("    pcm_fs_sel:             %d\n",   regs->w_pcm_ctrl.bits.pcm_fs_sel);
        printf("    write_soft_rstn:        %d\n",   regs->w_pcm_ctrl.bits.write_soft_rstn);
    }

    // AUDIO_IN_PCM_NUM
    printf("[%08X]AUDIO_IN_PCM_NUM:                       %08X\n", (uint32_t)&regs->pcm_num.value, regs->pcm_num.value);

    // AUDIO_IN_PCM_FRAME0_SADDR
    printf("[%08X]AUDIO_IN_PCM_FRAME0_SADDR:              %08X\n", (uint32_t)&regs->pcm_frame0_saddr.value, regs->pcm_frame0_saddr.value);

    // AUDIO_IN_PCM_FRAME1_SADDR
    printf("[%08X]AUDIO_IN_PCM_FRAME1_SADDR:              %08X\n", (uint32_t)&regs->pcm_frame1_saddr.value, regs->pcm_frame1_saddr.value);

    // AUDIO_IN_PCM_FRAME_SIZE
    printf("[%08X]AUDIO_IN_PCM_FRAME_SIZE:                %08X\n", (uint32_t)&regs->pcm_frame_size.value, regs->pcm_frame_size.value);

    // AUDIO_IN_PCM_BUFFER_SADDR
    printf("[%08X]AUDIO_IN_PCM_BUFFER_SADDR:              %08X\n", (uint32_t)&regs->pcm_buffer_saddr.value, regs->pcm_buffer_saddr.value);

    // AUDIO_IN_PCM_BUFFER_SIZE
    printf("[%08X]AUDIO_IN_PCM_BUFFER_SIZE:               %08X\n", (uint32_t)&regs->pcm_buffer_size.value, regs->pcm_buffer_size.value);

    // AUDIO_IN_PCM_SDC_ADDR
    printf("[%08X]AUDIO_IN_PCM_SDC_ADDR:                  %08X\n", (uint32_t)&regs->pcm_sdc_addr.value, regs->pcm_sdc_addr.value);

    // AUDIO_IN_INTEN
    printf("[%08X]AUDIO_IN_INTEN:                         %08X\n", (uint32_t)&regs->int_en.value, regs->int_en.value);
    if (detail) {
        printf("    avad_done_int:          %d\n", regs->int_en.bits.avad_done_int);
        printf("    pcm_done_int:           %d\n", regs->int_en.bits.pcm_done_int);
        printf("    axi_idle_int:           %d\n", regs->int_en.bits.axi_idle_int);
    }

    // AUDIO_IN_INT
    printf("[%08X]AUDIO_IN_INT:                           %08X\n", (uint32_t)&regs->int_state.value, regs->int_state.value);
    if (detail) {
        printf("    avad_done_int:          %d\n", regs->int_state.bits.avad_done_int);
        printf("    pcm_done_int:           %d\n", regs->int_state.bits.pcm_done_int);
        printf("    axi_idle_int:           %d\n", regs->int_state.bits.axi_idle_int);
        printf("    reg_info_fresh:         %d\n", regs->int_state.bits.reg_info_fresh);
        printf("    cpu_stop_ain_req:       %d\n", regs->int_state.bits.cpu_stop_ain_req);
        printf("    cpu_rerun_ain_req:      %d\n", regs->int_state.bits.cpu_rerun_ain_req);
    }

    // AUDIO_IN_ECHO_SOURCE_CTRL
    printf("[%08X]AUDIO_IN_ECHO_SOURCE_CTRL:              %08X\n", (uint32_t)&regs->echo_source_ctrl.value, regs->echo_source_ctrl.value);
    if (detail) {
        printf("    echo_source_sel:        %d\n", regs->echo_source_ctrl.bits.echo_source_sel);
        printf("    echo_filter_mag_en:     %d\n", regs->echo_source_ctrl.bits.echo_filter_mag_en);
        printf("    echo_magnif_sel:        %d\n", regs->echo_source_ctrl.bits.echo_magnif_sel);
        printf("    echo_work_en:           %d\n", regs->echo_source_ctrl.bits.echo_work_en);
        printf("    echo_channel_0_sel:     %d\n", regs->echo_source_ctrl.bits.echo_channel_0_sel);
        printf("    echo_channel_1_sel:     %d\n", regs->echo_source_ctrl.bits.echo_channel_1_sel);
    }

    // AUDIO_IN_IMX_USING
    printf("[%08X]AUDIO_IN_IMX_USING:                     %08X\n", (uint32_t)&regs->imx_using.value, regs->imx_using.value);

    // AUDIO_IN_IMN_USING
    printf("[%08X]AUDIO_IN_IMN_USING:                     %08X\n", (uint32_t)&regs->imn_using.value, regs->imn_using.value);

    // AUDIO_IN_ENG_USING
    printf("[%08X]AUDIO_IN_ENG_USING:                     %08X\n", (uint32_t)&regs->eng_using.value, regs->eng_using.value);

    // AUDIO_IN_ZCR0_USING
    printf("[%08X]AUDIO_IN_ZCR0_USING:                    %08X\n", (uint32_t)&regs->zcr0_using.value, regs->zcr0_using.value);

    // AUDIO_IN_ZCR_USING
    printf("[%08X]AUDIO_IN_ZCR_USING:                     %08X\n", (uint32_t)&regs->zcr_using.value, regs->zcr_using.value);

    // AUDIO_IN_IZCT_REG
    printf("[%08X]AUDIO_IN_IZCT_REG:                      %08X\n", (uint32_t)&regs->izct_reg.value, regs->izct_reg.value);

    // AUDIO_IN_DC_CTRL
    printf("[%08X]AUDIO_IN_DC_CTRL:                       %08X\n", (uint32_t)&regs->dc_ctrl.value, regs->dc_ctrl.value);
    if (detail) {
        printf("    dc_filter_sel:          %d\n", regs->dc_ctrl.bits.dc_filter_sel);
        printf("    echo_dc_filter_sel:     %d\n", regs->dc_ctrl.bits.echo_dc_filter_sel);
        printf("    set_dc_offset_sel:      %d\n", regs->dc_ctrl.bits.set_dc_offset_sel);
        printf("    post_dc_filter_en:      %d\n", regs->dc_ctrl.bits.post_dc_filter_en);
        printf("    echo_post_dc_filter_en: %d\n", regs->dc_ctrl.bits.echo_post_dc_filter_en);
    }

    // AUDIO_IN_DC_OFFSET
    printf("[%08X]AUDIO_IN_DC_OFFSET:                     %08X\n", (uint32_t)&regs->dc_offset.value, regs->dc_offset.value);

    // AUDIO_IN_PCM_VOL_CTRL0
    printf("[%08X]AUDIO_IN_PCM_VOL_CTRL0:                 %08X\n", (uint32_t)&regs->pcm_vol_ctrl[0].value, regs->pcm_vol_ctrl[0].value);
    if (detail) {
        printf("    ch0_vol_level:          %d\n", regs->pcm_vol_ctrl[0].bits.ch0_vol_level);
        printf("    ch1_vol_level:          %d\n", regs->pcm_vol_ctrl[0].bits.ch1_vol_level);
    }

    // AUDIO_IN_PCM_VOL_CTRL1
    printf("[%08X]AUDIO_IN_PCM_VOL_CTRL1:                 %08X\n", (uint32_t)&regs->pcm_vol_ctrl[1].value, regs->pcm_vol_ctrl[1].value);
    if (detail) {
        printf("    ch2_vol_level:          %d\n", regs->pcm_vol_ctrl[1].bits.ch0_vol_level);
        printf("    ch3_vol_level:          %d\n", regs->pcm_vol_ctrl[1].bits.ch1_vol_level);
    }

    // AUDIO_IN_PCM_VOL_CTRL2
    printf("[%08X]AUDIO_IN_PCM_VOL_CTRL2:                 %08X\n", (uint32_t)&regs->pcm_vol_ctrl[2].value, regs->pcm_vol_ctrl[2].value);
    if (detail) {
        printf("    ch4_vol_level:          %d\n", regs->pcm_vol_ctrl[2].bits.ch0_vol_level);
        printf("    ch5_vol_level:          %d\n", regs->pcm_vol_ctrl[2].bits.ch1_vol_level);
    }

    // AUDIO_IN_PCM_VOL_CTRL3
    printf("[%08X]AUDIO_IN_PCM_VOL_CTRL3:                 %08X\n", (uint32_t)&regs->pcm_vol_ctrl[3].value, regs->pcm_vol_ctrl[3].value);
    if (detail) {
        printf("    ch6_vol_level:          %d\n", regs->pcm_vol_ctrl[3].bits.ch0_vol_level);
        printf("    ch7_vol_level:          %d\n", regs->pcm_vol_ctrl[3].bits.ch1_vol_level);
    }

    // AUDIO_IN_PCM_VOL_CTRL4
    printf("[%08X]AUDIO_IN_PCM_VOL_CTRL4:                 %08X\n", (uint32_t)&regs->pcm_vol_ctrl[4].value, regs->pcm_vol_ctrl[4].value);
    if (detail) {
        printf("    ch8_vol_level:          %d\n", regs->pcm_vol_ctrl[4].bits.ch0_vol_level);
        printf("    ch9_vol_level:          %d\n", regs->pcm_vol_ctrl[4].bits.ch1_vol_level);
    }

    // AUDIO_IN_ITL_INTER
    printf("[%08X]AUDIO_IN_ITL_INTER:                     %08X\n", (uint32_t)&regs->itl_inter.value, regs->itl_inter.value);

    // AUDIO_IN_ITU_INTER
    printf("[%08X]AUDIO_IN_ITU_INTER:                     %08X\n", (uint32_t)&regs->itu_inter.value, regs->itu_inter.value);

    // AUDIO_IN_AGC_CTRL
    printf("[%08X]AUDIO_IN_AGC_CTRL:                      %08X\n", (uint32_t)&regs->agc_ctrl.value, regs->agc_ctrl.value);
    if (detail) {
        printf("    agc_ch_sel:             %d\n", regs->agc_ctrl.bits.agc_ch_sel);
        printf("    agc_updata_mode:        %d\n", regs->agc_ctrl.bits.agc_updata_mode);
        printf("    min_agc:                %d\n", regs->agc_ctrl.bits.min_agc);
        printf("    max_agc:                %d\n", regs->agc_ctrl.bits.max_agc);
        printf("    amic_agc_step:          %d\n", regs->agc_ctrl.bits.amic_agc_step);
        printf("    agc_sys_gate_rst_auto:  %d\n", regs->agc_ctrl.bits.agc_sys_gate_rst_auto);
        printf("    agc_sys_gate_enable:    %d\n", regs->agc_ctrl.bits.agc_sys_gate_enable);
        printf("    agc_soft_rstn:          %d\n", regs->agc_ctrl.bits.agc_soft_rstn);
    }

    // AUDIO_IN_AGC_FILTER_PARA
    printf("[%08X]AUDIO_IN_AGC_FILTER_PARA:               %08X\n", (uint32_t)&regs->agc_filter_para.value, regs->agc_filter_para.value);
    if (detail) {
        printf("    agc_para_a:             %d\n", regs->agc_filter_para.bits.agc_para_a);
        printf("    agc_para_n:             %d\n", regs->agc_filter_para.bits.agc_para_n);
    }

    // AUDIO_IN_AGC_SAMPLE_NUM
    printf("[%08X]AUDIO_IN_AGC_SAMPLE_NUM:                %08X\n", (uint32_t)&regs->agc_sample_num.value, regs->agc_sample_num.value);

    // AUDIO_IN_AGC_ENG_HIGH
    printf("[%08X]AUDIO_IN_AGC_ENG_HIGH:                  %08X\n", (uint32_t)&regs->agc_eng_high.value, regs->agc_eng_high.value);

    // AUDIO_IN_AGC_ENG_LOW
    printf("[%08X]AUDIO_IN_AGC_ENG_LOW:                   %08X\n", (uint32_t)&regs->agc_eng_low.value, regs->agc_eng_low.value);

    // AUDIO_IN_AGC_ENG
    printf("[%08X]AUDIO_IN_AGC_ENG:                       %08X\n", (uint32_t)&regs->agc_eng.value, regs->agc_eng.value);

    // AUDIO_IN_AGC_UPDATE_CTRL
    printf("[%08X]AUDIO_IN_AGC_UPDATE_CTRL:               %08X\n", (uint32_t)&regs->agc_update_ctrl.value, regs->agc_update_ctrl.value);
    if (detail) {
        printf("    agc_updata_amic_en:     %d\n", regs->agc_update_ctrl.bits.agc_updata_amic_en);
        printf("    agc_updata_group_en:    %d\n", regs->agc_update_ctrl.bits.agc_updata_group_en);
    }

    // AUDIO_IN_AGC_STATE
    printf("[%08X]AUDIO_IN_AGC_STATE:                     %08X\n", (uint32_t)&regs->agc_state.value, regs->agc_state.value);
    if (detail) {
        printf("    agc_pga_gain:           %d\n", regs->agc_state.bits.agc_pga_gain);
        printf("    agc_boost_gain:         %d\n", regs->agc_state.bits.agc_boost_gain);
        printf("    agc_boost_gain_by_pass: %d\n", regs->agc_state.bits.agc_boost_gain_by_pass);
        printf("    agc_boost_gain_pd:      %d\n", regs->agc_state.bits.agc_boost_gain_pd);
        printf("    agc_magnif_sel:         %d\n", regs->agc_state.bits.agc_magnif_sel);
        printf("    agc_echo_magnif_sel:    %d\n", regs->agc_state.bits.agc_echo_magnif_sel);
        printf("    filter_overflow:        %d\n", regs->agc_state.bits.filter_overflow);
    }

    // AUDIO_IN_GXADC_CTRL
    printf("[%08X]AUDIO_IN_GXADC_CTRL:                    %08X\n", (uint32_t)&regs->gxadc_ctrl.value, regs->gxadc_ctrl.value);
    if (detail) {
        printf("    gxadc_mic_en_after_avad:%d\n", regs->gxadc_ctrl.bits.gxadc_mic_en_after_avad);
        printf("    pga_gain_after_avad:    %d\n", regs->gxadc_ctrl.bits.pga_gain_after_avad);
        printf("    boost_gain_after_avad:  %d\n", regs->gxadc_ctrl.bits.boost_gain_after_avad);
        printf("    gxadc_open_auto:        %d\n", regs->gxadc_ctrl.bits.gxadc_open_auto);
    }

    // AUDIO_IN_AVAD_PARA
    printf("[%08X]AUDIO_IN_AVAD_PARA:                     %08X\n", (uint32_t)&regs->avad_para.value, regs->avad_para.value);

//==============================GXADC==============================//

    // AUDIO_IN_ADC_BIAS_GAIN
    printf("[%08X]AUDIO_IN_ADC_BIAS_GAIN:                 %08X\n", (uint32_t)&regs->adc_bias_gain.value, regs->adc_bias_gain.value);
    if (detail) {
        printf("    adc_cm_bias_pd:         %d\n", regs->adc_bias_gain.bits.adc_cm_bias_pd);
        printf("    adc_micbias_pd:         %d\n", regs->adc_bias_gain.bits.adc_micbias_pd);
        printf("    adc_boost_gain_pd:      %d\n", regs->adc_bias_gain.bits.adc_boost_gain_pd);
        printf("    adc_boost_gain_bypass:  %d\n", regs->adc_bias_gain.bits.adc_boost_gain_bypass);
    }

    // AUDIO_IN_ADC_PGA_PD
    printf("[%08X]AUDIO_IN_ADC_PGA_PD:                    %08X\n", (uint32_t)&regs->adc_pga_pd.value, regs->adc_pga_pd.value);
    if (detail) {
        printf("    adc_pga_pd:             %d\n", regs->adc_pga_pd.bits.adc_pga_pd);
        printf("    adc_pga_all_bypass:     %d\n", regs->adc_pga_pd.bits.adc_pga_all_bypass);
        printf("    adc_pd:                 %d\n", regs->adc_pga_pd.bits.adc_pd);
        printf("    adc_vad_pd:             %d\n", regs->adc_pga_pd.bits.adc_vad_pd);
    }

    // AUDIO_IN_ADC_LDO_PD
    printf("[%08X]AUDIO_IN_ADC_LDO_PD:                    %08X\n", (uint32_t)&regs->adc_ldo_pd.value, regs->adc_ldo_pd.value);

    // AUDIO_IN_ADC_BOOST_GAIN
    printf("[%08X]AUDIO_IN_ADC_BOOST_GAIN:                %08X\n", (uint32_t)&regs->adc_boost_gain.value, regs->adc_boost_gain.value);

    // AUDIO_IN_ADC_PGA_GAIN
    printf("[%08X]AUDIO_IN_ADC_PGA_GAIN:                  %08X\n", (uint32_t)&regs->adc_pga_gain.value, regs->adc_pga_gain.value);

    // AUDIO_IN_ADC_SET
    printf("[%08X]AUDIO_IN_ADC_SET:                       %08X\n", (uint32_t)&regs->adc_set.value, regs->adc_set.value);

    // AUDIO_IN_ADC_OUT_INV
    printf("[%08X]AUDIO_IN_ADC_OUT_INV:                   %08X\n", (uint32_t)&regs->adc_out_inv.value, regs->adc_out_inv.value);

    // AUDIO_IN_ADC_CLK_INV
    printf("[%08X]AUDIO_IN_ADC_CLK_INV:                   %08X\n", (uint32_t)&regs->adc_clk_inv.value, regs->adc_clk_inv.value);

    // AUDIO_IN_ADC_SEL_BIAS
    printf("[%08X]AUDIO_IN_ADC_SEL_BIAS:                  %08X\n", (uint32_t)&regs->adc_sel_bias.value, regs->adc_sel_bias.value);

    // AUDIO_IN_ADC_SEL_EXCOM
    printf("[%08X]AUDIO_IN_ADC_SEL_EXCOM:                 %08X\n", (uint32_t)&regs->adc_sel_excom.value, regs->adc_sel_excom.value);

    // AUDIO_IN_ADC_SEL_EXVBG
    printf("[%08X]AUDIO_IN_ADC_SEL_EXVBG:                 %08X\n", (uint32_t)&regs->adc_sel_exvbg.value, regs->adc_sel_exvbg.value);

    // AUDIO_IN_ADC_VAD_SH
    printf("[%08X]AUDIO_IN_ADC_VAD_SH:                    %08X\n", (uint32_t)&regs->adc_vad_sh.value, regs->adc_vad_sh.value);

    // AUDIO_IN_ADC_LDO_SET
    printf("[%08X]AUDIO_IN_ADC_LDO_SET:                   %08X\n", (uint32_t)&regs->adc_ldo_set.value, regs->adc_ldo_set.value);

    // AUDIO_IN_ADC_RSTN
    printf("[%08X]AUDIO_IN_ADC_RSTN:                      %08X\n", (uint32_t)&regs->adc_rstn.value, regs->adc_rstn.value);

    // AUDIO_IN_ADC_AVAD_STATUS
    printf("[%08X]AUDIO_IN_ADC_AVAD_STATUS:               %08X\n", (uint32_t)&regs->adc_avad_status.value, regs->adc_avad_status.value);

    // AUDIO_IN_ADC_VAD_RESTORE
    printf("[%08X]AUDIO_IN_ADC_VAD_RESTORE:               %08X\n", (uint32_t)&regs->adc_vad_restore.value, regs->adc_vad_restore.value);

    // AUDIO_IN_ADC_ANA_BIAS_PD
    printf("[%08X]AUDIO_IN_ADC_ANA_BIAS_PD:               %08X\n", (uint32_t)&regs->adc_ana_bias_pd.value, regs->adc_ana_bias_pd.value);

}
