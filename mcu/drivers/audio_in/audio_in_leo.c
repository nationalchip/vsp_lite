/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * audio_in.c: MCU AudioIn Driver Common
 *
 */

#include <autoconf.h>

#include <stdio.h>

#include <base_addr.h>
#include <audio_in_regs.h>
#include <misc_regs.h>

#include <driver/audio_mic.h>
#include <driver/irq.h>
#include <driver/delay.h>
#include <driver/clock.h>

#define AUDIO_IN_DEBUG(fmt, args...) \
    do { \
        printf("\n[AIN][%s():%d]: ", __func__, __LINE__); \
        printf(fmt, ##args); \
    } while(0)

typedef struct {
    unsigned int  frame_size;
    unsigned int  channel_size;
    unsigned int  sample_rate;
    unsigned int  frame_index;
    unsigned int  last_index;
    unsigned char mic_mask;
    unsigned char ref_mask;
    unsigned int  mic_gain;
    unsigned int  ref_gain;
    unsigned int  mic_chopper_state;
    unsigned int  mic_chopper_clock;
    unsigned int  ref_chopper_state;
    unsigned int  ref_chopper_clock;

    AUDIO_IN_AGC_CB      agc_callback;
    AUDIO_IN_AVAD_CB     avad_callback;
    AUDIO_IN_RECORDER_CB record_callback;

    void *priv;
} AUDIO_IN_STATE;

static AUDIO_IN_STATE s_audio_in_state;

//=================================================================================================

static void _AudioInReset(void) {
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    regs->int_en.bits.avad_done_int = 0;
    regs->int_en.bits.pcm_done_int = 0;
    regs->int_en.bits.agc_down_done = 0;
    regs->int_en.bits.agc_up_done = 0;

    // Reset PCM Record
    regs->int_state.value = (1 << 9); //cpu request ain to stop
    //while(!regs->int_state.bits.axi_idle_int)
    //    udelay(1);

    // Reset Audio Module
    regs->source_ctrl.bits.else_sys_gate_enable = 1;
    regs->source_ctrl.bits.i2sin_sys_gate_enable = 1;
    regs->source_ctrl.bits.dmic_sys_gate_enable = 1;
    regs->source_ctrl.bits.i2s_soft_rstn = 1;
    regs->source_ctrl.bits.dmic_soft_rstn = 1;
    regs->source_ctrl.bits.i2sout_soft_rstn = 1;
    regs->source_ctrl.bits.i2sout_sys_gate_enable = 0;
    regs->source_ctrl.bits.else_rst_auto_enable   = 0;
    regs->source_ctrl.bits.else_gate_auto_enable  = 1;

    // Reset AGC
    regs->agc_ctrl.bits.agc_sys_gate_rst_auto = 0;
    regs->agc_ctrl.bits.agc_sys_gate_enable = 1;
    regs->agc_ctrl.bits.agc_soft_rstn = 1;

    regs->w_pcm_ctrl.bits.write_soft_rstn = 1;
    regs->avad_ctrl.bits.avad_soft_rstn = 1;
}

//-------------------------------------------------------------------------------------------------

static int _AudioInISR(int irq, void *pdata)
{
    if (irq != MCU_IRQ_AUDIO_IN) {
        AUDIO_IN_DEBUG("AudioIn IRQ number is wrong!\n");
        return -1;
    }

    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    // Only consider loop back mode
    if (regs->int_state.bits.pcm_done_int){
        if (regs->int_en.bits.pcm_done_int && s_audio_in_state.record_callback) {
            int index = regs->pcm_sdc_addr.value / s_audio_in_state.frame_size;
            if (index != s_audio_in_state.last_index) {
                s_audio_in_state.last_index = index;
                s_audio_in_state.record_callback(s_audio_in_state.frame_index, s_audio_in_state.priv);
                s_audio_in_state.frame_index ++;
            }
        }

        regs->int_state.value = (1 << 1); // clear pcm_done_int
    }

    if (regs->int_state.bits.avad_done_int){
        AudioInSetMicEnable(0xff);
        AudioInSetRefEnable(0xff);

        if (regs->int_en.bits.avad_done_int && s_audio_in_state.avad_callback)
            s_audio_in_state.avad_callback(regs->read_state.bits.avad_state, s_audio_in_state.priv);

#ifdef CONFIG_BOARD_ANALOG_VAD
        regs->adc_set.value                = 0x44444444;
        regs->adc_ldo_set.bits.chopper_en  = (s_audio_in_state.mic_chopper_state | s_audio_in_state.ref_chopper_state);
#endif

        regs->int_state.value = (1 << 0); // clear avad_done_int
    }

    if (regs->int_state.bits.agc_down_done){
        if (s_audio_in_state.agc_callback)
            s_audio_in_state.agc_callback(AUDIO_IN_AGC_SOFT_STATE_DONE, regs->agc_eng.value);

        regs->int_state.value = (1 << 3); // clear agc_down_done
    }

    if (regs->int_state.bits.agc_up_done){
        if (s_audio_in_state.agc_callback)
            s_audio_in_state.agc_callback(AUDIO_IN_AGC_SOFT_STATE_UP, regs->agc_eng.value);

        regs->int_state.value = (1 << 4); // clear agc_up_done
    }

    return 0;
}

static int _AudioInUpdateADC(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    unsigned char adc_po = 0, adc_pd = 0;

    adc_po |= s_audio_in_state.mic_mask;
    adc_po |= s_audio_in_state.ref_mask;

    adc_pd = ~adc_po;

    regs->adc_bias_gain.value = 0x00000000;
    regs->adc_bias_gain.bits.adc_boost_gain_bypass |= adc_pd;
    regs->adc_bias_gain.bits.adc_boost_gain_pd     |= adc_pd;
    regs->adc_bias_gain.bits.adc_micbias_pd        |= adc_pd;
    regs->adc_bias_gain.bits.adc_cm_bias_pd        |= adc_pd;

    regs->adc_pga_pd.value = 0x00000000;
    regs->adc_pga_pd.bits.adc_pd                   |= adc_pd;
    regs->adc_pga_pd.bits.adc_vad_pd                = 0xff;
    regs->adc_pga_pd.bits.adc_pga_all_bypass       |= adc_pd;
    regs->adc_pga_pd.bits.adc_pga_pd               |= adc_pd;

    regs->adc_vad_restore.value                    &= adc_pd;

    regs->adc_ana_bias_pd.value = 0x00000000;
    regs->adc_ana_bias_pd.bits.ana_bias_pd         |= adc_pd;

    if (s_audio_in_state.mic_gain <= 16) {
        if (s_audio_in_state.mic_gain == 0) {
            regs->adc_pga_pd.bits.adc_pga_all_bypass   |= s_audio_in_state.mic_mask;
            regs->adc_pga_pd.bits.adc_pga_pd           |= s_audio_in_state.mic_mask;
        }
        regs->adc_bias_gain.bits.adc_boost_gain_bypass |= s_audio_in_state.mic_mask;
        regs->adc_bias_gain.bits.adc_boost_gain_pd     |= s_audio_in_state.mic_mask;
    }

    if (s_audio_in_state.ref_gain <= 16) {
        if (s_audio_in_state.ref_gain == 0) {
            regs->adc_pga_pd.bits.adc_pga_all_bypass   |= s_audio_in_state.ref_mask;
            regs->adc_pga_pd.bits.adc_pga_pd           |= s_audio_in_state.ref_mask;
        }
        regs->adc_bias_gain.bits.adc_boost_gain_bypass |= s_audio_in_state.ref_mask;
        regs->adc_bias_gain.bits.adc_boost_gain_pd     |= s_audio_in_state.ref_mask;
    }

    regs->adc_ldo_set.bits.chopper_en = 0x1;
    if (!s_audio_in_state.mic_chopper_state && !s_audio_in_state.ref_chopper_state) {
        regs->adc_ldo_set.bits.chopper_en  = 0x0;
        regs->adc_ldo_set.bits.chopper_clk = 0x0;

    } else if (s_audio_in_state.mic_chopper_clock >= s_audio_in_state.ref_chopper_clock) {
        regs->adc_ldo_set.bits.chopper_clk = s_audio_in_state.mic_chopper_clock;
        s_audio_in_state.ref_chopper_clock = s_audio_in_state.mic_chopper_clock;

    } else {
        regs->adc_ldo_set.bits.chopper_clk = s_audio_in_state.ref_chopper_clock;
        s_audio_in_state.mic_chopper_clock = s_audio_in_state.ref_chopper_clock;
    }

    return 0;
}

//-------------------------------------------------------------------------------------------------
// Initialize and Done

int AudioInInit(const AUDIO_IN_CONFIG *config, int enable_avad)
{
    ClockEnable(CLOCK_AUDIO_IN);
    _AudioInReset();

    // Set Mic And Ref Mask
    s_audio_in_state.mic_mask = 0;
    s_audio_in_state.ref_mask = 0;

    // Set Mic And Ref Gain
    s_audio_in_state.mic_gain = 0;
    s_audio_in_state.ref_gain = 0;

    // Set Mic And Ref Chopper
    s_audio_in_state.ref_chopper_state = 0;
    s_audio_in_state.ref_chopper_clock = 0;
    s_audio_in_state.mic_chopper_state = 0;
    s_audio_in_state.mic_chopper_clock = 0;

    // Set Source AVAD and Pin Mux
    AudioInBoardInit();

    // Set Frame Index
    s_audio_in_state.frame_index = 0;
    s_audio_in_state.last_index = 0;

    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    // Setup PCM Capture
    regs->w_pcm_ctrl.bits.pcm_endian = AIN_PCM_ENDIAN_LITTLE;
    regs->w_pcm_ctrl.bits.write_mode = AIN_PCM_WRITE_MODE_LOOP;

    // PCM Num
    s_audio_in_state.frame_size = config->frame_size;
    regs->pcm_num.value = config->frame_size / 128 * 32;

    regs->w_pcm_ctrl.bits.w_echo_channel_max_sel = 2;
    regs->w_pcm_ctrl.bits.write_channel_max_sel  = 7;

    s_audio_in_state.sample_rate = config->sample_rate;
    if (config->sample_rate == 8000)
        regs->w_pcm_ctrl.bits.pcm_fs_sel = AIN_PCM_SAMPLE_RATE_08KHZ;
    else if (config->sample_rate == 16000)
        regs->w_pcm_ctrl.bits.pcm_fs_sel = AIN_PCM_SAMPLE_RATE_16KHZ;
    else if (config->sample_rate == 48000)
        regs->w_pcm_ctrl.bits.pcm_fs_sel = AIN_PCM_SAMPLE_RATE_48KHZ;
    else
        return -1;

    // Set Buffer Size
    regs->pcm_buffer_saddr.value = (uint32_t)(config->buffer_addr - (config->channel_size / sizeof(short)) * (8 - config->mic_channel_num));
    regs->pcm_buffer_size.value = config->channel_size;
    s_audio_in_state.channel_size = config->channel_size;

    // Set Audio Reference Channel
    regs->source_ctrl.bits.i2s_channel_sel = config->ref_channel_num ? config->ref_channel_num - 1 : 0;

    // Refresh Audio In Device
    regs->int_state.value = (1 << 8);

    regs->avad_ctrl.bits.avad_bypass = (!enable_avad) ? 1 : 0;

    if (enable_avad) {

        AudioInSetMicEnable(regs->avad_ctrl.bits.ch_avad_en >> (8 - CONFIG_MIC_ARRAY_MIC_NUM));
        AudioInSetRefEnable(0);
#ifdef CONFIG_BOARD_ANALOG_VAD
        unsigned int vad_ch = regs->channel_sel.bits.channel_7_sel;

        regs->adc_set.value               = 0x44444444;
        regs->adc_pga_pd.bits.adc_pd      = 0xff;
        regs->adc_ldo_set.bits.chopper_en = 0;
        regs->adc_pga_pd.bits.adc_vad_pd  = ~(1 << vad_ch) & 0xff;
        regs->adc_vad_restore.value       = 0xff;
        mdelay(20);
        regs->adc_vad_restore.value       = ~(1 << vad_ch) & 0xff;
        regs->adc_set.value               =  (1 << (vad_ch * 4)) | 0x44444444;
#endif
    }

    // Setup ISR
    s_audio_in_state.record_callback = config->record_callback;
    s_audio_in_state.avad_callback = config->avad_callback;
    s_audio_in_state.agc_callback = NULL;
    gx_request_irq(MCU_IRQ_AUDIO_IN, _AudioInISR, NULL);
    regs->int_state.value = 0x3f;
    regs->int_en.bits.pcm_done_int = 1;
    regs->int_en.bits.avad_done_int = 1;

    // Run it
    regs->source_ctrl.bits.i2s_soft_rstn = 0;
    regs->avad_ctrl.bits.avad_soft_rstn = 0;
    regs->w_pcm_ctrl.bits.write_soft_rstn = 0;
    regs->int_state.value = (1 << 10); //cpu request ain to run

    return 0;
}
//-------------------------------------------------------------------------------------------------

void AudioInDone(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    // Turn Off Interrupt
    regs->int_en.bits.avad_done_int = 0;
    regs->int_en.bits.pcm_done_int = 0;

    // Turn Off LDO
    regs->adc_ldo_pd.bits.adc_ldo_pd = 1;

    // Turn Off VAD
    regs->adc_set.value = 0x44444444;

    // Disable Audio PCM Capture
    _AudioInReset();
    ClockDisable(CLOCK_AUDIO_IN);
}

//=================================================================================================
// Agc

int AudioInAgcGetEnable(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    return regs->agc_ctrl.bits.agc_sys_gate_enable;
}

int AudioInAgcConfig(const AUDIO_IN_AGC_CONFIG *config)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (config->mode & AUDIO_IN_AGC_MODE_DISABLE) {
        regs->agc_ctrl.bits.agc_sys_gate_enable = 0;
        return 0;

    } else if (config->mode & AUDIO_IN_AGC_MODE_ENABLE) {
        int para_a, para_n;
        int a = 6, n = 6; //recommend value

        para_a = ((1 << 17) - (1 << 18) / (n + 1) + 1) >> 1; // prime formula: (int)(((n - 1) * 1.0 / (n + 1)) * (1 << 16) + 0.5), n = 6
        regs->agc_filter_para.bits.agc_para_a = para_a;
        para_n = (a * (1 << 16) + 5) / 10; // prime formula: (int)(a * (1 << 16) + 0.5), a = 0.6
        regs->agc_filter_para.bits.agc_para_n = para_n;

        AUDIO_IN_AGC_CTRL agc_ctrl = regs->agc_ctrl;

        agc_ctrl.bits.agc_ch_sel = config->channel_sel;
        if ((config->mode & AUDIO_IN_AGC_MODE_SOFTWARE) == AUDIO_IN_AGC_MODE_SOFTWARE) {
            agc_ctrl.bits.agc_updata_mode   = AIN_AGC_SOFT;
            regs->int_en.bits.agc_up_done   = config->int_en_up;
            regs->int_en.bits.agc_down_done = config->int_en_down;
            s_audio_in_state.agc_callback   = config->agc_callback;
        } else {
            agc_ctrl.bits.agc_updata_mode   = AIN_AGC_HARD;
            s_audio_in_state.agc_callback   = NULL;

            regs->agc_update_ctrl.bits.agc_updata_group_en = 0x3;

            if ((regs->source_ctrl.bits.source_sel == AUDIO_IN_INPUT_SOURCE_AMIC) ||
                    (regs->echo_source_ctrl.bits.echo_source_sel == AIN_ECHO_SOURCE_SEL_AMIC)) {
                agc_ctrl.bits.amic_agc_step = AIN_AGC_AMIC_AGC_STEP_6DB;
                agc_ctrl.bits.min_agc = 2;
                agc_ctrl.bits.max_agc = 46;
                regs->agc_update_ctrl.bits.agc_updata_amic_en = 0xff;
            } else {
                agc_ctrl.bits.min_agc = 0;
                agc_ctrl.bits.max_agc = 54;
                regs->agc_update_ctrl.bits.agc_updata_amic_en = 0x00;
            }
        }

        regs->agc_eng_low.bits.agc_eng_low       = config->eng_low;
        regs->agc_eng_high.bits.agc_eng_high     = config->eng_high;
        regs->agc_sample_num.bits.agc_sample_num = config->sample_num;

        agc_ctrl.bits.agc_soft_rstn = 0;
        regs->agc_ctrl.value = agc_ctrl.value; // Run it
    }

    return 0;
}

int AudioInSetAgcEnergyThreshold(AUDIO_IN_AGC_ENERGY_THRESHOLD_TYPE type, unsigned int value)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (type == AUDIO_IN_AGC_ENERGY_THRESHOLD_LOW)
        regs->agc_eng_low.value = value;

    else if (type == AUDIO_IN_AGC_ENERGY_THRESHOLD_HIGH)
        regs->agc_eng_high.value = value;

    return 0;
}

unsigned int AudioInGetAgcEnergyThreshold(AUDIO_IN_AGC_ENERGY_THRESHOLD_TYPE type)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (type == AUDIO_IN_AGC_ENERGY_THRESHOLD_LOW)
        return regs->agc_eng_low.value;

    else if (type == AUDIO_IN_AGC_ENERGY_THRESHOLD_HIGH)
        return regs->agc_eng_high.value;

    return -1;
}

int AudioInSetAgcSampleNum(unsigned int sample_num)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    regs->agc_sample_num.value = sample_num;

    return 0;
}

unsigned int AudioInGetAgcSampleNum(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    return regs->agc_sample_num.value;
}

unsigned int AudioInGetAgcEnergy(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    return regs->agc_eng.value;
}

//=================================================================================================
// Other Information Query

unsigned char AudioInGetMicEnable(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    return regs->source_ctrl.bits.channel_work_en >> (8 - CONFIG_MIC_ARRAY_MIC_NUM);
}

int AudioInSetMicEnable(unsigned char mask)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (regs->source_ctrl.bits.source_sel == AUDIO_IN_INPUT_SOURCE_AMIC)
    {
        unsigned int channel_sel = regs->channel_sel.value;
        unsigned char channel_work_en = (mask << (8 - CONFIG_MIC_ARRAY_MIC_NUM)) & 0xff;
        unsigned int adc_on = 0;

        for (int i = 0; i < 8; ++i) {
            // check each actived mic in channel_sel
            if ((channel_work_en >> i) & 0x1) {
                unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;
                adc_on |= 1 << channel_index;
            }
        }

        s_audio_in_state.mic_mask = adc_on;

        _AudioInUpdateADC();
    }

    regs->source_ctrl.bits.channel_work_en = mask << (8 - CONFIG_MIC_ARRAY_MIC_NUM);
    return 0;
}

unsigned char AudioInGetMicGain(void)
{
    return s_audio_in_state.mic_gain;
}

int AudioInSetMicGain(unsigned char mic_gain)
{
    volatile AUDIO_IN_REGS *ain_regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    unsigned int audio_in_gain = 0;

    s_audio_in_state.mic_gain = mic_gain;

    if (ain_regs->source_ctrl.bits.source_sel == AUDIO_IN_INPUT_SOURCE_AMIC)
    {
        unsigned char adc_on  = 0;
        unsigned int channel_sel = ain_regs->channel_sel.value;

        audio_in_gain = AUDIO_IN_INPUT_GAIN_2;

        for (int i = 0; i < 8; ++i) {
            // check each actived mic in channel_sel, and tune their gains
            unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;
            if (channel_index < 0x8)
                adc_on |= 1 << i;
        }

        if (mic_gain == 0) {
            mic_gain = 2;
            s_audio_in_state.mic_gain = mic_gain;
        }

        unsigned int channel_boost_gain = 0;
        unsigned int channel_pga_gain   = 0;
        unsigned int boost_gain_enable  = 0;

        if (mic_gain <= 16) {
            channel_boost_gain = 0x00;
            channel_pga_gain   = 6 + (mic_gain - 1) / 2;
            boost_gain_enable = 0;

        } else if (mic_gain <= 30) {
            mic_gain -= 16;
            channel_boost_gain = 0x00;
            channel_pga_gain   = 6 + (mic_gain - 1) / 2;
            boost_gain_enable = 1;

        } else if (mic_gain <= 50) {
            mic_gain -= 30;
            channel_boost_gain = 0x03;
            channel_pga_gain   = 6 + (mic_gain - 1) / 2;
            boost_gain_enable = 1;

        } else {
            mic_gain -= 50;
            channel_boost_gain = 0x03;
            channel_pga_gain   = 0x0f;
            boost_gain_enable = 1;

            audio_in_gain += (mic_gain + 3) / 6;
            if (audio_in_gain > 9)
                audio_in_gain = 9;
        }

        for (int i = 0; i < 8; ++i) {
            // check each actived mic in channel_sel, and tune their gains.
            unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;
            if (channel_index < 0x8) {
                ain_regs->adc_boost_gain.value &= ~(0x3 << (channel_index * 2));
                ain_regs->adc_boost_gain.value |= (channel_boost_gain << (channel_index * 2));
                ain_regs->adc_pga_gain.value   &= ~(0xf << (channel_index * 4));
                ain_regs->adc_pga_gain.value   |= (channel_pga_gain   << (channel_index * 4));
            }
        }

        s_audio_in_state.mic_chopper_state = boost_gain_enable;
        s_audio_in_state.mic_chopper_clock = channel_boost_gain ? 0x6 : 0x5;

        _AudioInUpdateADC();

    } else if (ain_regs->source_ctrl.bits.source_sel == AUDIO_IN_INPUT_SOURCE_DMIC) {
        audio_in_gain = AUDIO_IN_INPUT_GAIN_2;
        audio_in_gain += (mic_gain + 3) / 6;
        if (audio_in_gain > 9)
            audio_in_gain = 9;

    } else {
        audio_in_gain = (mic_gain + 3) / 6;
        if (audio_in_gain > 9)
            audio_in_gain = 9;
    }

    ain_regs->source_ctrl.bits.magnif_sel = audio_in_gain;

    return 0;
}

uint32_t AudioInGetMicChannelGain(int channel)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    for (int i = 0, channel_seq = 0; i < 8; i++) {
        unsigned int  channel_sel   = regs->channel_sel.value;
        unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;

        if (channel_index < 0x08) {
            if (channel_seq == channel)
                return ((i % 2 == 0) ? regs->pcm_vol_ctrl[i/2].value : regs->pcm_vol_ctrl[i/2].value >> 16) & 0xffff;
            channel_seq++;
        }
    }

    return -1;
}

int AudioInSetMicChannelGain(int channel, unsigned int gain)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    for (int i = 0, channel_seq = 0; i < 8; i++) {
        unsigned int  channel_sel   = regs->channel_sel.value;
        unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;

        if (channel_index < 0x08) {
            if (channel_seq == channel) {
                regs->pcm_vol_ctrl[i/2].value &= (i % 2 == 0) ? 0xffff0000 : 0x0000ffff;
                regs->pcm_vol_ctrl[i/2].value |= (i % 2 == 0) ? gain << 0  : gain << 16;
                return 0;
            }
            channel_seq++;
        }
    }

    return -1;
}

int AudioInSetRefSource(AUDIO_IN_REF_SOURCE source, int left_channel, int right_channel)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (source == AUDIO_IN_REF_SOURCE_AMIC) {
        regs->echo_source_ctrl.bits.echo_source_sel    = AIN_ECHO_SOURCE_SEL_AMIC;
        regs->echo_source_ctrl.bits.echo_filter_mag_en = AIN_ECHO_FILTER_AMIC;
    } else if (source == AUDIO_IN_REF_SOURCE_DMIC) {
        regs->echo_source_ctrl.bits.echo_source_sel    = AIN_ECHO_SOURCE_SEL_DMIC;
        regs->echo_source_ctrl.bits.echo_filter_mag_en = AIN_ECHO_FILTER_DMIC;
    } else if (source == AUDIO_IN_REF_SOURCE_INTERNAL_LOOP) {
        regs->echo_source_ctrl.bits.echo_source_sel   = AIN_ECHO_SOURCE_SEL_I2S;
        regs->echo_source_ctrl.bits.echo_filter_mag_en = AIN_ECHO_FILTER_I2S;

        regs->i2s_genclk.bits.adc_clk_mode     = AIN_I2S_ADC_CLK_MODE_MASTER;
        regs->i2s_adcinfo.bits.data_format     = AIN_I2S_DATA_FORMAT_I2S;
        regs->i2s_adcinfo.bits.pcm_length      = AIN_I2S_PCM_LENGTH_24BIT;
        regs->i2s_adcinfo.bits.i2s_source_sel  = AIN_I2S_SOURCE_SEL_AUDIO_PLAY;
    } else if (source == AUDIO_IN_REF_SOURCE_I2S_SLAVE) {
        //PINMUX: PCM0INDAT0, PCM0INBCLK, PCM0INLRCLK, PCM0INMCLK
        regs->echo_source_ctrl.bits.echo_source_sel    = AIN_ECHO_SOURCE_SEL_I2S;
        regs->echo_source_ctrl.bits.echo_filter_mag_en = AIN_ECHO_FILTER_I2S;

        regs->i2s_genclk.bits.adc_clk_mode     = AIN_I2S_ADC_CLK_MODE_SLAVE;
        regs->i2s_genclk.bits.bit_clk_sel      = AIN_I2S_BIT_CLK_MODE_256FS;
        regs->i2s_genclk.bits.lr_clk_sel       = AIN_I2S_LR_CLK_MODE_256FS;
        regs->i2s_adcinfo.bits.data_format     = AIN_I2S_DATA_FORMAT_TDM;
        regs->i2s_adcinfo.bits.pcm_length      = AIN_I2S_PCM_LENGTH_32BIT_ALL;
        regs->i2s_adcinfo.bits.fsync_mode      = AIN_I2S_FSYNC_MODE_SHORT;
        regs->i2s_adcinfo.bits.i2s_source_sel  = AIN_I2S_SOURCE_SEL_OUT_ADC;

    } else if (source == AUDIO_IN_REF_SOURCE_I2S_MASTER) {
        //PINMUX: PCM1INDAT0, PCM1INBCLK, PCM1INLRCLK
        regs->echo_source_ctrl.bits.echo_source_sel    = AIN_ECHO_SOURCE_SEL_I2S;
        regs->echo_source_ctrl.bits.echo_filter_mag_en = AIN_ECHO_FILTER_I2S;

        regs->i2s_genclk.bits.adc_clk_mode     = AIN_I2S_ADC_CLK_MODE_MASTER;
        regs->i2s_adcinfo.bits.data_format     = AIN_I2S_DATA_FORMAT_I2S;
        regs->i2s_adcinfo.bits.pcm_length      = AIN_I2S_PCM_LENGTH_24BIT;
        regs->i2s_adcinfo.bits.i2s_source_sel  = AIN_I2S_SOURCE_SEL_OUT_I2S;
    }

    regs->echo_source_ctrl.bits.echo_magnif_sel    = 0;
    regs->echo_source_ctrl.bits.echo_channel_0_sel = left_channel;
    regs->echo_source_ctrl.bits.echo_channel_1_sel = right_channel;

    return 0;
}

unsigned char AudioInGetRefEnable(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    return regs->echo_source_ctrl.bits.echo_work_en;
}

int AudioInSetRefEnable(unsigned char mask)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (regs->echo_source_ctrl.bits.echo_source_sel ==  AIN_ECHO_SOURCE_SEL_AMIC) {
        unsigned char adc_on = 0;
        unsigned char channel_work_en    = mask & ((1 << CONFIG_VSP_REF_NUM) - 1) & 0x03;
        unsigned char echo_channel_0_sel = regs->echo_source_ctrl.bits.echo_channel_0_sel;
        unsigned char echo_channel_1_sel = regs->echo_source_ctrl.bits.echo_channel_1_sel;

        if ((channel_work_en >> 0) & 0x01)
            adc_on |= (1 << echo_channel_0_sel) & 0xff;

        if ((channel_work_en >> 1) & 0x01)
            adc_on |= (1 << echo_channel_1_sel) & 0xff;

        s_audio_in_state.ref_mask = adc_on;

        _AudioInUpdateADC();
    }

    regs->echo_source_ctrl.bits.echo_work_en = mask & ((1 << CONFIG_VSP_REF_NUM) - 1);

    return 0;
}

unsigned char AudioInGetRefGain(void)
{
    return s_audio_in_state.ref_gain;
}

int AudioInSetRefGain(unsigned char ref_gain)
{
    volatile AUDIO_IN_REGS *ain_regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    unsigned int audio_in_gain = 0;

    s_audio_in_state.ref_gain = ref_gain;

    if (ain_regs->echo_source_ctrl.bits.echo_source_sel == AIN_ECHO_SOURCE_SEL_AMIC)
    {
        unsigned char adc_on = 0;
        unsigned char echo_channel_0_sel = ain_regs->echo_source_ctrl.bits.echo_channel_0_sel;
        unsigned char echo_channel_1_sel = ain_regs->echo_source_ctrl.bits.echo_channel_1_sel;

        audio_in_gain = AUDIO_IN_INPUT_GAIN_2;

        if (echo_channel_0_sel < 0x8)
            adc_on |= 1 << echo_channel_0_sel;

        if (echo_channel_1_sel < 0x8)
            adc_on |= 1 << echo_channel_1_sel;

        if (ref_gain == 0) {
            ref_gain = 2;
            s_audio_in_state.ref_gain = ref_gain;
        }

        unsigned int channel_boost_gain = 0;
        unsigned int channel_pga_gain   = 0;
        unsigned int boost_gain_enable  = 0;

        if (ref_gain <= 16) {
            channel_boost_gain = 0x00;
            channel_pga_gain   = 6 + (ref_gain - 1) / 2;
            boost_gain_enable  = 0;

        } else if (ref_gain <= 30) {
            ref_gain -= 16;
            channel_boost_gain = 0x00;
            channel_pga_gain   = 6 + (ref_gain - 1) / 2;
            boost_gain_enable = 1;

        } else if (ref_gain <= 50) {
            ref_gain -= 30;
            channel_boost_gain = 0x03;
            channel_pga_gain   = 6 + (ref_gain - 1) / 2;
            boost_gain_enable = 1;

        } else {
            ref_gain -= 50;
            channel_boost_gain = 0x03;
            channel_pga_gain   = 0x0f;
            boost_gain_enable = 1;

            audio_in_gain += (ref_gain + 3) / 6;
            if (audio_in_gain > 9)
                audio_in_gain = 9;
        }

        if (echo_channel_0_sel < 0x8) {
            ain_regs->adc_boost_gain.value &= ~(0x3 << (echo_channel_0_sel * 2));
            ain_regs->adc_boost_gain.value |= (channel_boost_gain << (echo_channel_0_sel * 2));
            ain_regs->adc_pga_gain.value   &= ~(0xf << (echo_channel_0_sel * 4));
            ain_regs->adc_pga_gain.value   |= (channel_pga_gain   << (echo_channel_0_sel * 4));
        }

        if (echo_channel_1_sel < 0x8) {
            ain_regs->adc_boost_gain.value &= ~(0x3 << (echo_channel_1_sel * 2));
            ain_regs->adc_boost_gain.value |= (channel_boost_gain << (echo_channel_1_sel * 2));
            ain_regs->adc_pga_gain.value   &= ~(0xf << (echo_channel_1_sel * 4));
            ain_regs->adc_pga_gain.value   |= (channel_pga_gain   << (echo_channel_1_sel * 4));
        }

        s_audio_in_state.ref_chopper_state = boost_gain_enable;
        s_audio_in_state.ref_chopper_clock = channel_boost_gain ? 0x6 : 0x5;

        _AudioInUpdateADC();

    } else if (ain_regs->echo_source_ctrl.bits.echo_source_sel == AIN_ECHO_SOURCE_SEL_DMIC) {
        audio_in_gain = AUDIO_IN_INPUT_GAIN_2;
        audio_in_gain += (ref_gain + 3) / 6;
        if (audio_in_gain > 9)
            audio_in_gain = 9;

    } else {
        audio_in_gain = (ref_gain + 3) / 6;
        if (audio_in_gain > 9)
            audio_in_gain = 9;
    }

    ain_regs->echo_source_ctrl.bits.echo_magnif_sel = audio_in_gain;

    return 0;
}

uint32_t AudioInGetRefChannelGain(int channel)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    for (int i = 0, channel_seq = 0; i < 2; i++) {
        unsigned int  channel_sel   = (regs->echo_source_ctrl.value >> 16) & 0xff;
        unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;

        if (channel_index < 0x08) {
            if (channel_seq == channel)
                return ((i % 2 == 0) ? regs->pcm_vol_ctrl[(i+8)/2].value : regs->pcm_vol_ctrl[(i+8)/2].value >> 16) & 0xffff;
            channel_seq++;
        }
    }

    return -1;
}

int AudioInSetRefChannelGain(int channel, unsigned int gain)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    for (int i = 0, channel_seq = 0; i < 2; i++) {
        unsigned int  channel_sel   = (regs->echo_source_ctrl.value >> 16) & 0xff;
        unsigned char channel_index = (channel_sel >> (i * 4)) & 0xf;

        if (channel_index < 0x08)  {
            if (channel == channel_seq) {
                regs->pcm_vol_ctrl[(i+8)/2].value &= (i % 2 == 0) ? 0xffff0000 : 0x0000ffff;
                regs->pcm_vol_ctrl[(i+8)/2].value |= (i % 2 == 0) ? gain << 0  : gain << 16;
                return 0;
            }
            channel_seq++;
        }
    }

    return -1;
}

int AudioInSetMicDcEnable(AUDIO_IN_MIC_DC_FILTER_ENABLE enable)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if ((enable & AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE) == AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE)
        regs->dc_ctrl.bits.dc_filter_sel = AIN_DC_FILTER_SEL_INTERNAL;
    else if ((enable & AUDIO_IN_MIC_PRE_DC_FILTER_DISABLE) == AUDIO_IN_MIC_PRE_DC_FILTER_DISABLE)
        regs->dc_ctrl.bits.dc_filter_sel = AIN_DC_FILTER_SEL_BYPASS;

    if ((enable & AUDIO_IN_MIC_POST_DC_FILTER_ENABLE) == AUDIO_IN_MIC_POST_DC_FILTER_ENABLE)
        regs->dc_ctrl.bits.post_dc_filter_en = 1;
    else if ((enable & AUDIO_IN_MIC_POST_DC_FILTER_DISABLE) == AUDIO_IN_MIC_POST_DC_FILTER_DISABLE)
        regs->dc_ctrl.bits.post_dc_filter_en = 0;

    return 0;
}

AUDIO_IN_MIC_DC_FILTER_ENABLE AudioInGetMicDcEnable(void)
{
    volatile AUDIO_IN_REGS *regs         = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    AUDIO_IN_MIC_DC_FILTER_ENABLE enable = 0;

    enable |= (regs->dc_ctrl.bits.dc_filter_sel != AIN_DC_FILTER_SEL_BYPASS) ? AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE : AUDIO_IN_MIC_PRE_DC_FILTER_DISABLE;
    enable |= regs->dc_ctrl.bits.post_dc_filter_en ? AUDIO_IN_MIC_POST_DC_FILTER_ENABLE : AUDIO_IN_MIC_POST_DC_FILTER_DISABLE;

    return enable;
}

int AudioInSetRefDcEnable(AUDIO_IN_REF_DC_FILTER_ENABLE enable)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if ((enable & AUDIO_IN_REF_PRE_DC_FILTER_ENABLE) == AUDIO_IN_REF_PRE_DC_FILTER_ENABLE)
        regs->dc_ctrl.bits.echo_dc_filter_sel = AIN_DC_ECHO_FILTER_SEL_INTERNAL;
    else if ((enable & AUDIO_IN_REF_PRE_DC_FILTER_DISABLE) == AUDIO_IN_REF_PRE_DC_FILTER_DISABLE)
        regs->dc_ctrl.bits.echo_dc_filter_sel = AIN_DC_ECHO_FILTER_SEL_BYPASS;

    if ((enable & AUDIO_IN_REF_POST_DC_FILTER_ENABLE) == AUDIO_IN_REF_POST_DC_FILTER_ENABLE)
        regs->dc_ctrl.bits.echo_post_dc_filter_en = 1;
    else if ((enable & AUDIO_IN_REF_POST_DC_FILTER_DISABLE) == AUDIO_IN_REF_POST_DC_FILTER_DISABLE)
        regs->dc_ctrl.bits.echo_post_dc_filter_en = 0;

    return 0;
}

AUDIO_IN_REF_DC_FILTER_ENABLE AudioInGetRefDcEnable(void)
{
    volatile AUDIO_IN_REGS *regs         = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    AUDIO_IN_REF_DC_FILTER_ENABLE enable = 0;

    enable |= (regs->dc_ctrl.bits.echo_dc_filter_sel != AIN_DC_ECHO_FILTER_SEL_BYPASS) ? AUDIO_IN_REF_PRE_DC_FILTER_ENABLE : AUDIO_IN_REF_PRE_DC_FILTER_DISABLE;
    enable |= regs->dc_ctrl.bits.echo_post_dc_filter_en ? AUDIO_IN_REF_POST_DC_FILTER_ENABLE : AUDIO_IN_REF_POST_DC_FILTER_DISABLE;

    return enable;
}

int AudioInSetAvadParamUsing(AUDIO_IN_READ_ITPARAM_TYPE type, uint32_t value)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (AUDIO_IN_READ_ITPARAM_ITL == type)
        regs->itl_reg.value = value;
    else
        regs->itu_reg.value = value;

    return 0;
}

uint32_t AudioInGetAvadParamUsing(AUDIO_IN_READ_ITPARAM_SEL sel, AUDIO_IN_READ_ITPARAM_TYPE type)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    regs->avad_ctrl.bits.read_itpara_sel = (AIN_AVAD_READ_ITPARAM_SEL)sel;
    return AUDIO_IN_READ_ITPARAM_ITL == type ? regs->itl_using.value : regs->itu_using.value;
}

int AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_TYPE type, uint32_t value)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (AUDIO_IN_READ_ITPARAM_ITL == type)
        regs->itl_reg.value = value;
    else
        regs->itu_reg.value = value;

    return 0;
}

uint32_t AudioInGetDigitalvadParamUsing(AUDIO_IN_READ_ITPARAM_SEL sel, AUDIO_IN_READ_ITPARAM_TYPE type)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    regs->avad_ctrl.bits.read_itpara_sel = (AIN_AVAD_READ_ITPARAM_SEL)sel;
    return AUDIO_IN_READ_ITPARAM_ITL == type ? regs->itl_using.value : regs->itu_using.value;
}

int AudioInSetAnalogVadParamUsing(AUDIO_IN_AVAD_THRESHOLD_VOLTAGE voltage, AUDIO_IN_AVAD_PREAMP preamp)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    unsigned int vad_sh    = 0;
    unsigned int ch_vad_en = regs->avad_ctrl.bits.ch_avad_en & 0xff;

    regs->adc_vad_sh.value = 0;

    switch(voltage) {
        case AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_11:
            vad_sh = 0x3;
            break;
        case AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_22:
            vad_sh = 0x2;
            break;
        case AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_47:
            vad_sh = 0x1;
            break;
        case AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_97:
            vad_sh = 0x0;
            break;
        default:
            vad_sh = 0x0;
            break;
    }

    for (int i = 0; i < 8; i++) {
        if (ch_vad_en >> i & 0x1) {
            unsigned int ch_sel = regs->channel_sel.value >> (i*4) & 0xf;
            if (ch_sel < 0x8)
                regs->adc_vad_sh.value |= vad_sh << (ch_sel*2);
        }
    }

    return 0;
}

int AudioInGetAnalogVadParamUsing(AUDIO_IN_AVAD_THRESHOLD_VOLTAGE *voltage, AUDIO_IN_AVAD_PREAMP *preamp)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    unsigned int vad_sh = 0;
    unsigned int ch_vad_en = regs->avad_ctrl.bits.ch_avad_en & 0xf;

    *voltage = AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_INVALID;
    *preamp  = AUDIO_IN_AVAD_PREAMP_INVALID;

    for (int i = 0; i < 8; i++) {
        if (ch_vad_en >> i & 0x1) {
            unsigned int ch_sel = regs->channel_sel.value >> (i*4) & 0xf;
            if (ch_sel < 0x8) {
                vad_sh = regs->adc_vad_sh.value >> (ch_sel*2) & 0x3;
                switch (vad_sh) {
                    case 0:
                        *voltage = AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_97;
                        break;
                    case 1:
                        *voltage = AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_47;
                        break;
                    case 2:
                        *voltage = AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_22;
                        break;
                    case 3:
                        *voltage = AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_11;
                        break;
                    default:
                        *voltage = AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_INVALID;
                }

                return 0;
            }
        }
    }

    return 0;
}


uint8_t AudioInGetAvadState(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    return regs->read_state.bits.avad_state;
}

int AudioInSetZcrLevel(uint32_t level)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    if (level == 0) {
        regs->avad_ctrl.bits.zcr_bypass = 1;
        regs->izct_reg.value = 0;
    } else {
        regs->avad_ctrl.bits.zcr_bypass = 0;
        regs->izct_reg.value = level;
    }

    return 0;
}

uint32_t AudioInGetZcrLevel(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);

    return regs->izct_reg.value;
}

int AudioInGetMicOverflow(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    return regs->agc_state.bits.filter_overflow;
}
