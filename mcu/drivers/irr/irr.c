/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * irr.c: Infra-Red Remote
 *
 */

#include <common.h>
#include <stdio.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/delay.h>

static struct {
    unsigned int port;
    unsigned int carrier_frequency;
    unsigned int duty;
} s_irr_state = {0,};

void IrrSendInit(unsigned int port, unsigned int carrier_frequency)
{
    s_irr_state.port = port;
    s_irr_state.carrier_frequency = carrier_frequency;

    if(s_irr_state.carrier_frequency){
        s_irr_state.duty = 33;
        GpioSetDirection(s_irr_state.port, GPIO_DIRECTION_OUTPUT);
        GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
    }else{
        GpioSetDirection(s_irr_state.port, GPIO_DIRECTION_OUTPUT);
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_HIGH);
    }
}

static void _IrrSendStart(void)
{
    if(s_irr_state.carrier_frequency){
        GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
        udelay(9000);
        GpioDisablePWM(s_irr_state.port);
        udelay(4500);
    }else{
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_LOW);
        udelay(9000);
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_HIGH);
        udelay(4500);
    }
}

static void _IrrSendEnd(void)
{
    if(s_irr_state.carrier_frequency){
        GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
        udelay(40580);
    }else{
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_LOW);
        udelay(40580);
    }
}

void IrrSendRepeat(void)
{
    if(s_irr_state.carrier_frequency){
        GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
        udelay(9000);
        GpioDisablePWM(s_irr_state.port);
        udelay(2250);
        GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
        udelay(560);
        GpioDisablePWM(s_irr_state.port);
        udelay(115310);
    }else{
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_LOW);
        udelay(9000);
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_HIGH);
        udelay(2250);
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_LOW);
        udelay(560);
        GpioSetLevel(s_irr_state.port, GPIO_LEVEL_HIGH);
        udelay(115310);
    }
}

static void _IrrSendBit(unsigned char data)
{
    if(s_irr_state.carrier_frequency){
        if (data){
            GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
            udelay(560);
            GpioDisablePWM(s_irr_state.port);
            udelay(1690);
        }else{
            GpioEnablePWM(s_irr_state.port, s_irr_state.carrier_frequency, s_irr_state.duty);
            udelay(560);
            GpioDisablePWM(s_irr_state.port);
            udelay(560);
        }
    }else{
        if (data){
            GpioSetLevel(s_irr_state.port, GPIO_LEVEL_LOW);
            udelay(560);
            GpioSetLevel(s_irr_state.port, GPIO_LEVEL_HIGH);
            udelay(1690);
        }else{
            GpioSetLevel(s_irr_state.port, GPIO_LEVEL_LOW);
            udelay(560);
            GpioSetLevel(s_irr_state.port, GPIO_LEVEL_HIGH);
            udelay(560);
        }
    }
}

static void _IrrSendByte(unsigned char data)
{
    int i = 0;
    for (; i < 8; i++){
        _IrrSendBit(data & (1 << i));
    }
}

void IrrSendCode(unsigned char addr, unsigned char code)
{
    unsigned char addr_anti = ~addr;
    unsigned char code_anti = ~code;
    _IrrSendStart();
    _IrrSendByte(addr);
    _IrrSendByte(addr_anti);
    _IrrSendByte(code);
    _IrrSendByte(code_anti);
    _IrrSendEnd();
    IrrSendRepeat();
}
