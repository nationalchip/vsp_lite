/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * sw_fifo.c: MCU Soft Fifo Driver
 *
 */
#include <string.h>

#include <driver/sw_fifo.h>

int SwFifoInit(SW_FIFO *pfifo, void *buffer, unsigned int size)
{
    if(size == 0)
        return -1;

    memset(pfifo, 0, sizeof(SW_FIFO));
    pfifo->data = buffer;
    pfifo->size = size;

    return 0;
}

void SwFifoFree(SW_FIFO *pfifo)
{
    if(pfifo->data)
        memset(pfifo, 0, sizeof(SW_FIFO));
}

unsigned int SwFifoUserGet(SW_FIFO *pfifo, void *buf, unsigned int len)
{
    unsigned int todo, size;
    unsigned int split = 0;

    if (pfifo->data) {
        size = SwFifoLen(pfifo);
        todo = len > size  ? size : len;

        split = (pfifo->pread + todo > pfifo->size) ? pfifo->size - pfifo->pread : 0;
        if (split > 0) {
            memcpy(buf, pfifo->data+pfifo->pread, split);

            buf = (void*)((unsigned int)buf + split);
            todo -= split;
            pfifo->pread = 0;
        }
        memcpy(buf, pfifo->data+pfifo->pread, todo);

        pfifo->pread = (pfifo->pread + todo) % pfifo->size;
    } else
        todo = 0;

    return (todo + split);
}

unsigned int SwFifoUserPut(SW_FIFO *pfifo, void *buf, unsigned int len)
{
    unsigned int todo, size;
    unsigned int split = 0;

    if (pfifo->data) {
        size = SwFifoFreeLen(pfifo);
        todo = len > size ? size : len;

        split = (pfifo->pwrite + todo > pfifo->size) ? pfifo->size - pfifo->pwrite : 0;
        if (split > 0) {
            memcpy(pfifo->data+pfifo->pwrite, buf, split);

            buf = (void*)((unsigned int)buf + split);
            todo -= split;
            pfifo->pwrite = 0;
        }
        memcpy(pfifo->data+pfifo->pwrite, buf, todo);

        pfifo->pwrite = (pfifo->pwrite + todo) % pfifo->size;
    } else
        todo = 0;

    return (todo + split);
}

unsigned int SwFifoLen(SW_FIFO *pfifo)
{
    if(pfifo->data) {
        return (pfifo->pwrite - pfifo->pread + pfifo->size) % pfifo->size;
    } else {
        return -1;
    }
}

unsigned int SwFifoFreeLen(SW_FIFO *pfifo)
{
    if(pfifo->data) {
        return pfifo->size - SwFifoLen(pfifo) - 1;
    } else {
        return -1;
    }
}

unsigned int SwFifoProduce(SW_FIFO *pfifo, unsigned int len)
{
    if(pfifo->data) {
        pfifo->pwrite = (pfifo->pwrite + len) % pfifo->size;
        return len;
    }

    return -1;
}

unsigned int SwFifoConsume(SW_FIFO *pfifo, unsigned int len)
{
    if(pfifo->data) {
        pfifo->pread = (pfifo->pread + len) % pfifo->size;
        return len;
    }

    return -1;
}

unsigned int SwFifoGetRptr(SW_FIFO *pfifo)
{
    if(pfifo->data) {
        return pfifo->pread;
    }

    return 0;
}

unsigned int SwFifoGetWptr(SW_FIFO *pfifo)
{
    if(pfifo->data) {
        return pfifo->pwrite;
    }

    return 0;
}

