/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * All rights reserved!
 *
 * vsp_dsp.h: Helper for DSP modules
 *
 */

#ifndef __GX_DSP_H__
#define __GX_DSP_H__

#include <driver/dsp.h>

//#include "drv_dsp_reg.h"

#include <stdio.h>

#define DSP_PRINTF(fmt, args...) \
    do { \
        printf(fmt, ##args); \
    } while(0)

#endif
