/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_dsp.c: DSP Driver
 *
 */

#include <autoconf.h>
#include <board_config.h>

#include <string.h>
#include <base_addr.h>
#include <dsp_regs.h>
#include <misc_regs.h>

#include <driver/delay.h>
#include <driver/irq.h>
#include <driver/dsp.h>
#include <driver/clock.h>

#include "dsp.h"

static struct {
    DSP_VSP_CALLBACK callback;
} s_dsp_state;

//=================================================================================================

static int _DspISR(int irq, void *pdata)
{
    volatile DSP_REGS *dsp_regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);
    if (dsp_regs->dsp_mcu_en.vsp_flag & dsp_regs->dsp_mcu_int.vsp_flag) {
        if (s_dsp_state.callback) {
            s_dsp_state.callback((volatile VSP_MSG_DSP_MCU *)dsp_regs->dsp_reg, pdata);
        }
        dsp_regs->dsp_mcu_clear.vsp_flag = 1;
    }
    
    if (dsp_regs->dsp_mcu_en.p_wait_mode & dsp_regs->dsp_mcu_int.p_wait_mode) {
        printf("DSP entered Wait Mode!\n");
        dsp_regs->dsp_mcu_clear.p_wait_mode = 1;
    }
    
    return 0;
}

void DspSetVspCallback(DSP_VSP_CALLBACK callback, void *priv)
{
    s_dsp_state.callback = callback;
    if (callback) {
        gx_request_irq(MCU_IRQ_DSP, _DspISR, priv);
    } else {
        gx_free_irq(MCU_IRQ_DSP);
    }
}


int DspPostVspMessage(const uint32_t *message, const uint32_t size)
{
    volatile DSP_REGS *dsp_regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);
    // Check DSP is running
    
    // Check last interrupt is clear
    if (dsp_regs->mcu_dsp_int.vsp_flag)
        return -1;
    
    // Copy message body to mcu register
    for (int i = 0; i < size; i++)
        dsp_regs->mcu_reg[i].value = message[i];
    
    // Invoke DSP interrupt
    dsp_regs->mcu_dsp_en.vsp_flag = 1;
    dsp_regs->mcu_dsp_int.vsp_flag = 1;
    
    return 0;
}


int DspSendVspMessage(const uint32_t *request, const uint32_t size,
                      DSP_VSP_CALLBACK callback, void *priv, unsigned int timeout_ms)
{
    if (DspPostVspMessage(request, size))
        return -1;

    volatile DSP_REGS *dsp_regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);
    unsigned int due_time = get_time_ms() + timeout_ms;
    while (get_time_ms() < due_time) {
        if (dsp_regs->dsp_mcu_int.vsp_flag) {
            int result = -1;
            if (callback)
                result = callback((volatile VSP_MSG_DSP_MCU *)dsp_regs->dsp_reg, priv);
            dsp_regs->dsp_mcu_clear.vsp_flag = 1;
            if (!result)
                return 0;
        }
    }
    // timeout return
    return -2;
}

//=================================================================================================

int DspInit(void)
{
    volatile DSP_REGS *dsp_regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);
    volatile RESET_REGS *rst_regs = (volatile RESET_REGS *)(MCU_REG_BASE_RESET);

    ClockEnable(CLOCK_DSP);

    // Check DSP is not run

    // RunStall DSP and reset DSP
    dsp_regs->dsp_runstall.runstall = 1;

    rst_regs->mepg_cld_rst_norm.bits.dsp_debug_cold_rst_n = 1;
    rst_regs->mepg_hot_rst_norm.bits.dsp_debug_hot_rst_n = 1;
    rst_regs->mepg_cld_rst_norm.bits.dsp_cold_rst_n = 1;
    rst_regs->mepg_hot_rst_norm.bits.dsp_hot_rst_n = 1;

    rst_regs->mepg_cld_rst_norm.bits.dsp_debug_cold_rst_n = 0;
    rst_regs->mepg_hot_rst_norm.bits.dsp_debug_hot_rst_n = 0;
    mdelay(100);
    rst_regs->mepg_cld_rst_norm.bits.dsp_cold_rst_n = 0;
    rst_regs->mepg_hot_rst_norm.bits.dsp_hot_rst_n = 0;

    return 0;
}

int DspLoadPTCMSection(const uint8_t *src, uint32_t length)
{
    volatile uint32_t *src_buffer = (uint32_t *)src;
    volatile uint32_t *dst_buffer = (volatile uint32_t *)(CONFIG_DSP_PTCM_BASE + 0x40000000);

    for (int i = 0; i < length; i += 4)
        *dst_buffer++ = *src_buffer++;

    return 0;
}

int DspLoadDTCMSection(const uint8_t *src, uint32_t length)
{
    volatile uint32_t *src_buffer = (uint32_t *)src;
    volatile uint32_t *dst_buffer = (volatile uint32_t *)(CONFIG_DSP_DTCM_BASE + 0x40000000);

    for (int i = 0; i < length; i += 4)
        *dst_buffer++ = *src_buffer++;

    return 0;
}

int DspLoadSRAMSection(uint8_t *dst, const uint8_t *src, uint32_t length)
{
    if (dst == src)
        return 0;

    memcpy(dst, src, length);

    return 0;
}

int DspRun(void)
{
    volatile DSP_REGS *dsp_regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);

    dsp_regs->dsp_runstall.runstall = 0;

    return 0;
}

int DspStop(void)
{
     volatile DSP_REGS *dsp_regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);

    dsp_regs->dsp_runstall.runstall = 1;

    return 0;
}

int DspDone(void)
{
    // TODO: Turn Off DSP
    gx_free_irq(39);
    ClockDisable(CLOCK_DSP);
    return 0;
}
