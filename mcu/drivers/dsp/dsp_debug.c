/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * All rights reserved!
 *
 * drv_dsp_debug.c: MCU DSP Debug
 *
 */

#include <base_addr.h>
#include <dsp_regs.h>

#include <driver/dsp.h>
#include "dsp.h"

void DspDumpRegs(int detail)
{
    volatile DSP_REGS *regs = (volatile DSP_REGS *)(MCU_REG_BASE_DSP);

    DSP_PRINTF("==========================================================\n");
    // DSP_INT_REASON
    DSP_PRINTF("[%08X]MCU_DSP_INT(RW):                        %08X\n", (uint32_t)&regs->mcu_dsp_int, regs->mcu_dsp_int.value);
    if (detail) {
        DSP_PRINTF("    vsp_flag:                  %d\n", regs->mcu_dsp_int.vsp_flag);
    }
    
    // DSP_INT_EN
    DSP_PRINTF("[%08X]MCU_DSP_EN(RW):                         %08X\n", (uint32_t)&regs->mcu_dsp_en, regs->mcu_dsp_en.value);
    if (detail) {
        DSP_PRINTF("    vsp_flag:                  %d\n", regs->mcu_dsp_en.vsp_flag);
    }
    
    // DSP_INT_CLEAR
    DSP_PRINTF("[%08X]MCU_DSP_CLEAR(R):                       %08X\n", (uint32_t)&regs->mcu_dsp_clear, regs->mcu_dsp_clear.value);
    if (detail) {
        DSP_PRINTF("    vsp_flag:                  %d\n", regs->mcu_dsp_clear.vsp_flag);
    }
    
    DSP_PRINTF("----------------------------------------------------------\n");
    
    // MCU_INT_REASON
    DSP_PRINTF("[%08X]DSP_MCU_INT(R):                         %08X\n", (uint32_t)&regs->dsp_mcu_int, regs->dsp_mcu_int.value);
    if (detail) {
        DSP_PRINTF("    vsp_flag:                  %d\n", regs->dsp_mcu_int.vsp_flag);
        DSP_PRINTF("    p_wait_mode:               %d\n", regs->dsp_mcu_int.p_wait_mode);
    }
    
    // MCU_INT_EN
    DSP_PRINTF("[%08X]DSP_MCU_EN(R):                          %08X\n", (uint32_t)&regs->dsp_mcu_en, regs->dsp_mcu_en.value);
    if (detail) {
        DSP_PRINTF("    vsp_flag:                  %d\n", regs->dsp_mcu_en.vsp_flag);
        DSP_PRINTF("    p_wait_mode:               %d\n", regs->dsp_mcu_en.p_wait_mode);
    }
    
    // MCU_INT_CLEAR
    DSP_PRINTF("[%08X]DSP_MCU_CLEAR(RW):                      %08X\n", (uint32_t)&regs->dsp_mcu_clear, regs->dsp_mcu_clear.value);
    if (detail) {
        DSP_PRINTF("    vsp_flag:                  %d\n", regs->dsp_mcu_clear.vsp_flag);
        DSP_PRINTF("    p_wait_mode:               %d\n", regs->dsp_mcu_clear.p_wait_mode);
    }
    
    DSP_PRINTF("----------------------------------------------------------\n");
    
    // DSP_RUNSTALL
    DSP_PRINTF("[%08X]DSP_RUNSTALL(RW):                       %08X\n", (uint32_t)&regs->dsp_runstall, regs->dsp_runstall.value);
    if (detail) {
        DSP_PRINTF("    runstall:                  %d\n", regs->dsp_runstall.runstall);
    }
    
    // DSP_GPIO_IN
    DSP_PRINTF("[%08X]DSP_GPIO_IN(RW):                        %08X\n", (uint32_t)&regs->dsp_gpio_in, regs->dsp_gpio_in.value);
    
    // PWAIT_MODE
    DSP_PRINTF("[%08X]PWAIT_MODE(R):                          %08X\n", (uint32_t)&regs->pwrite_mode, regs->pwrite_mode.value);
    if (detail) {
        DSP_PRINTF("    is_dsp_sleep:              %d\n", regs->pwrite_mode.is_dsp_sleep);
    }
    
    DSP_PRINTF("----------------------------------------------------------\n");
    int i;
    for (i = 0; i < 10; i++) {
        // DSP_REGX
        DSP_PRINTF("[%08X]DSP_REG%d(R):                            %08X\n", (uint32_t)&regs->dsp_reg[i], i, regs->dsp_reg[i].value);
    }
    
    DSP_PRINTF("----------------------------------------------------------\n");
    for (i = 0; i < 10; i++) {
        // MCU_REGX
        DSP_PRINTF("[%08X]MCU_REG%d(RW):                           %08X\n", (uint32_t)&regs->mcu_reg[i], i, regs->mcu_reg[i].value);
    }
    
    DSP_PRINTF("----------------------------------------------------------\n");
    // DSP_OCDHALT
    DSP_PRINTF("[%08X]DSP_OCDHALT(R):                         %08X\n", (uint32_t)&regs->dsp_ocdhalt, regs->dsp_ocdhalt.value);
    if (detail) {
        DSP_PRINTF("    ocd_halt_on_reset:         %d\n", regs->dsp_ocdhalt.ocd_halt_on_reset);
    }
    
    // DSP_XOCDMODE
    DSP_PRINTF("[%08X]DSP_XOCDMODE(R):                        %08X\n", (uint32_t)&regs->dsp_xocdmode, regs->dsp_xocdmode.value);
    if (detail) {
        DSP_PRINTF("    xocdmode:                  %d\n", regs->dsp_xocdmode.xocdmode);
    }

}
