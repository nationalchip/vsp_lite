/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * cpu.c: MCU's CPU Driver
 *
 */

#include <stdio.h>

#include <common.h>
#include <cpu_regs.h>

#include <driver/cpu.h>
#include <driver/delay.h>
#include <driver/irq.h>
#include <driver/gpio.h>
#include <driver/misc.h>
#include <driver/watchdog.h>
#include <driver/otp.h>

#ifndef CONFIG_MCU_ENABLE_BOOT_LOAD_UBOOT_STAGE1
#include <u-boot-spl.bin.h>
#endif

#define LOG_TAG "[CPU]"

static CPU_VSP_CALLBACK s_cpu_vsp_callback = NULL;

//=================================================================================================

static int _CpuISR(int irq, void *priv)
{
    volatile CPU_INT_REGS *cpu_int_regs = (volatile CPU_INT_REGS *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_CK_INT);

    if (cpu_int_regs->a7_ck_int.bits.vsp_flag) {
        if (s_cpu_vsp_callback) {
            volatile unsigned int *a7_ck_regs = (volatile unsigned int *)(MCU_REG_BASE_CONFIG + VSP_A7_CK_REG_OFFSET);
            if (s_cpu_vsp_callback)
                s_cpu_vsp_callback(a7_ck_regs, priv);
        }
        cpu_int_regs->a7_ck_clear.bits.vsp_flag = 1;
        return 0;
    }
    if (cpu_int_regs->a7_ck_int.bits.cpu_halt) {
        /* disable halt channel */
        cpu_int_regs->a7_ck_enable.bits.cpu_halt = 0;
        cpu_int_regs->a7_ck_clear.bits.cpu_halt = 1;
        if(s_cpu_vsp_callback) {
            VSP_MSG_HEADER vsp_message;
            vsp_message.magic = 0;
            vsp_message.param = *(volatile VSP_PM_REQUEST_TYPE *)VSP_A7_CK_PM_REQUEST_TYPE;
            vsp_message.size = 0;
            vsp_message.type = VMT_CM_PM_REQUEST;
            printf(LOG_TAG"Process PM Request[%d]!\n", vsp_message.param);
            s_cpu_vsp_callback(&vsp_message.value, priv);
        }
        return 0;
    }

    printf(LOG_TAG"Unknown Request\n");
    return -1;
}

int CpuSetVspCallback(CPU_VSP_CALLBACK callback, void *priv)
{
    volatile CPU_INT_REGS *cpu_int_regs = (volatile CPU_INT_REGS *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_CK_INT);
    if (callback) {
        s_cpu_vsp_callback = callback;
        gx_request_irq(MCU_IRQ_A7_TO_MCU, _CpuISR, priv);
        if (cpu_int_regs->a7_ck_int.bits.vsp_flag) {
            cpu_int_regs->a7_ck_clear.bits.vsp_flag = 1;
            cpu_int_regs->a7_ck_clear.bits.vsp_flag = 0;
        }
        if (cpu_int_regs->a7_ck_int.bits.cpu_halt) {
            cpu_int_regs->a7_ck_clear.bits.cpu_halt = 1;
            cpu_int_regs->a7_ck_clear.bits.cpu_halt = 0;
        }

        cpu_int_regs->a7_ck_enable.bits.vsp_flag = 1;
        //cpu_int_regs->a7_ck_enable.bits.cpu_halt = 1;
    }
    else {
        s_cpu_vsp_callback = NULL;
        cpu_int_regs->a7_ck_enable.bits.vsp_flag = 0;
        gx_free_irq(MCU_IRQ_A7_TO_MCU);
    }

    return 0;
}

int CpuPostVspMessage(const uint32_t *message, const uint32_t size)
{
    unsigned long long us = get_time_us();
    volatile CPU_INT_REGS *cpu_int_regs = (volatile CPU_INT_REGS *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_CK_INT);
    while (cpu_int_regs->ck_a7_int.bits.vsp_flag) {
        if (get_time_us() - us > 5000) return -1;
    }

    volatile unsigned int * ck_a7_regs = (volatile unsigned int *)(MCU_REG_BASE_CONFIG + VSP_CK_A7_REG_OFFSET);
    for (int i = 0; i < size; i++)
        ck_a7_regs[i] = message[i];

    cpu_int_regs->ck_a7_int.bits.vsp_flag = 1;

    return size;
}

int CpuGetVspMessage(uint32_t *message, const uint32_t size)
{
    volatile unsigned int *a7_ck_regs = (volatile unsigned int *)(MCU_REG_BASE_CONFIG + VSP_A7_CK_REG_OFFSET);
    for (int i = 0; i < size; i++)
        message[i] = a7_ck_regs[i];

    return size;
}

//-------------------------------------------------------------------------------------------------

#ifdef CONFIG_MCU_RESET_DDR_WHILE_CPU_START
static void _CpuResetDDR(void)
{
    // This code is only for NRE
#define CPU_DDR_RESET_GPIO 10
    GpioSetDirection(CPU_DDR_RESET_GPIO, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(CPU_DDR_RESET_GPIO, GPIO_LEVEL_LOW);
    mdelay(1);
    GpioSetLevel(CPU_DDR_RESET_GPIO, GPIO_LEVEL_HIGH);
}
#endif

int CpuReboot(void)
{
#ifdef CONFIG_MCU_RESET_SYSTEM_WHILE_CPU_REBOOT
    // Start Watch Dog Timer without Feed
    WatchdogInit(1, 0, NULL);
    while (1);
#else
    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);
    a7_enable->bits.a7_enable = 0;
    mdelay(1);
#ifdef CONFIG_MCU_RESET_DDR_WHILE_CPU_START
    /* DDR reset after a7 halt */
    _CpuResetDDR();
    mdelay(1);
#endif
    a7_enable->bits.a7_enable = 1;
#endif
    return 0;
}

int CpuSuspend(void)
{
    volatile A7_POWER_CTRL *a7_power_ctl = (volatile A7_POWER_CTRL *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_POWER_CTRL);
    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);
    mdelay(1);
    a7_enable->bits.a7_enable = 0;
    udelay(1);
    a7_power_ctl->bits.pd2_power_ctrl = 1;

    if (CHIP_VERSION_C <= OtpGetChipVersion()){
        // check chip version, and decice whether we could turn off A7 power.
        udelay(1);
        volatile LOW_POWER *low_power = (volatile LOW_POWER *)(MCU_REG_BASE_CONFIG + REG_OFFSET_LOW_POWER);
        low_power->bits.arm_power_config = 1;
        udelay(200);    //硬件给出的时间是 60us
    }
#ifdef CONFIG_BOARD_SUPPORT_STANDBY
    BoardSuspend();
#endif

    return 0;
}

int CpuResume(VSP_CPU_WAKE_UP_REASON reason)
{
    volatile VSP_CPU_WAKE_UP_REASON *wakeup_reason = (volatile VSP_CPU_WAKE_UP_REASON *)(VSP_CK_A7_WAKEUP_REASON);
    *wakeup_reason = reason;

#ifdef CONFIG_BOARD_SUPPORT_STANDBY
    BoardResume();
#endif
    if (CHIP_VERSION_C <= OtpGetChipVersion()){
        volatile LOW_POWER *low_power = (volatile LOW_POWER *)(MCU_REG_BASE_CONFIG + REG_OFFSET_LOW_POWER);
        low_power->bits.arm_power_config = 0;
        udelay(2000);
    }

    volatile A7_POWER_CTRL *a7_power_ctl = (volatile A7_POWER_CTRL *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_POWER_CTRL);
    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);

    a7_power_ctl->bits.pd2_power_ctrl = 0;
    udelay(1);
    a7_enable->bits.a7_enable = 1;

    return 0;
}

static int _CpuWakeup(VSP_CPU_WAKE_UP_REASON reason)
{
    volatile VSP_CPU_WAKE_UP_REASON *wakeup_reason = (volatile VSP_CPU_WAKE_UP_REASON *)(VSP_CK_A7_WAKEUP_REASON);
    *wakeup_reason = reason;

    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);

    // TODO: Check whether CPU is alive
    if (a7_enable->bits.a7_enable) {
        volatile unsigned int *a7_ck_regs = (volatile unsigned int *)(MCU_REG_BASE_CONFIG + REG_OFFSET_COMMON_REG_DATA00);
        *a7_ck_regs = 0;
        a7_enable->bits.a7_enable = 0;
        mdelay(1);
        //return -1;
    }

#ifndef CONFIG_MCU_ENABLE_BOOT_LOAD_UBOOT_STAGE1
    // Copy u-boot-spl.bin data into A7 SRAM
    volatile unsigned int *dst = (volatile unsigned int *)MCU_BUS_BASE_A7_SRAM;
    unsigned int *src = (unsigned int *)u_boot_spl_bin;
    for (int i = 0; i < (u_boot_spl_bin_len + 3) / 4; i++) {
        *dst = *src;
        dst++;
        src++;
    }
    mdelay(1);
#endif

    // Remove Isolate
    *(volatile unsigned int*)MCU_REG_BASE_CPU_ISOLATE = 0x0;
#ifdef CONFIG_MCU_RESET_DDR_WHILE_CPU_START
    // Reset DDR
    _CpuResetDDR();
#endif
    // Enable ALL Interrupt to A7
    // TODO: shouldn't enable some interrupts
    *(volatile unsigned int *)MCU_REG_BASE_INTC_SHARED = 0xFFFFFFFF;

    // Release a7 Reset
    a7_enable->bits.a7_enable = 1;
    mdelay(1);

    return 0;
}

int CpuWakeup(void)
{
    return _CpuWakeup(VSP_CPU_WAKEUP_REASON_COLD);
}

//-------------------------------------------------------------------------------------------------

#define DDR_INIT_REQ         0xFFFFFFFF
#define DDR_SELFREFRESH_REQ  0xFFFFFFFE
#define DDR_INIT_DONE        0x0000FFFF
#define DDR_SELFREFRESH_DONE 0x0000FFFE

int CpuInitializeDDR(void)
{
    volatile CK_GATE2 *ck_gate2 = (volatile CK_GATE2 *)(MCU_REG_BASE_CONFIG + REG_OFFSET_CK_GATE2);
    ck_gate2->bits.dto_pll_clk_gate_en = 1; // Disable ck to a7 1.2G DTO

    _CpuWakeup(DDR_INIT_REQ); // Only initialize DDR, and sleep A7
    while (readl(VSP_A7_DDR_RSP_REG) != DDR_INIT_DONE)
        continue;

    return 0;
}

int CpuSuspendDDR(void)
{
    volatile VSP_CPU_WAKE_UP_REASON *wakeup_reason = (volatile VSP_CPU_WAKE_UP_REASON *)(VSP_CK_A7_WAKEUP_REASON);
    volatile A7_POWER_CTRL *a7_power_ctl = (volatile A7_POWER_CTRL *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_POWER_CTRL);
    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);

    *wakeup_reason = DDR_SELFREFRESH_REQ;
    while (1) {
        if (a7_enable->bits.a7_enable == 0 && a7_power_ctl->bits.pd2_power_ctrl == 1)
            break;
    }

    return 0;
}

int CpuResumeDDR(void)
{
    CpuResume(DDR_INIT_REQ);
    while (readl(VSP_A7_DDR_RSP_REG) != DDR_INIT_DONE)
        continue;

    return 0;
}

//=================================================================================================

static int _CpuWatchdogIsr(int irq, void *pdata)
{
    WatchdogInit(1, 0, NULL);
    while (1);
    return 0;
}

int CpuWatchdogInit(void)
{
    gx_request_irq(MCU_IRQ_A7_WDT, _CpuWatchdogIsr, NULL);
    return 0;
}

//=================================================================================================

#ifdef CONFIG_MCU_ENABLE_DEBUG
static void _CpuDumpCKA7IRQFlags(CK_A7_INT flags)
{
    printf("    ck_dw_uart1_int:        %d\n", flags.bits.ck_dw_uart1_int);
    printf("    ck_dw_uart2_int:        %d\n", flags.bits.ck_dw_uart2_int);
    printf("    ck_dw_i2c1_int:         %d\n", flags.bits.ck_dw_i2c1_int);
    printf("    ck_dw_i2c2_int:         %d\n", flags.bits.ck_dw_i2c2_int);
    printf("    ck_dw_spi_int:          %d\n", flags.bits.ck_dw_spi_int);
    printf("    ck_count1_int:          %d\n", flags.bits.ck_count1_int);
    printf("    ck_eport1_int:          %d\n", flags.bits.ck_eport1_int);
    printf("    ck_dma_int:             %d\n", flags.bits.ck_dma_int);
    printf("    snpu_int:               %d\n", flags.bits.snpu_int);
    printf("    audio_int:              %d\n", flags.bits.audio_int);
    printf("    mtc_int:                %d\n", flags.bits.mtc_int);
    printf("    dsp_int:                %d\n", flags.bits.dsp_int);
    printf("    audio_play_int:         %d\n", flags.bits.audio_play_int);
    printf("    usb_slave_wuintreq:     %d\n", flags.bits.audio_int);
    printf("    usb_slave_int_req:      %d\n", flags.bits.mtc_int);
    printf("    usb_dma_int_req:        %d\n", flags.bits.dsp_int);
    printf("    vsp_flag:               %d\n", flags.bits.vsp_flag);
}

void CpuDumpRegs(int detail)
{
    printf("==========================================================\n");

    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);
    printf("[%08X]A7_ENABLE:                              %08X\n", (uint32_t)a7_enable, a7_enable->value);
    if (detail)
        printf("    a7_enable:              %d\n", a7_enable->bits.a7_enable);

    volatile A7_POWER_CTRL *a7_power_ctrl = (volatile A7_POWER_CTRL *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_POWER_CTRL);
    printf("[%08X]A7_POWER_CTRL:                          %08X\n", (uint32_t)a7_power_ctrl, a7_power_ctrl->value);
    if (detail)
        printf("    pd2_power_ctrl:         %d\n", a7_power_ctrl->bits.pd2_power_ctrl);

    // CPU_INT_REGS
    printf("----------------------------------------------------------\n");
    volatile CPU_INT_REGS *cpu_int_regs = (volatile CPU_INT_REGS *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_CK_INT);
    printf("[%08X]A7_CK_INT:                              %08X\n", (uint32_t)&cpu_int_regs->a7_ck_int, cpu_int_regs->a7_ck_int.value);
    printf("[%08X]A7_CK_CLEAR:                            %08X\n", (uint32_t)&cpu_int_regs->a7_ck_clear, cpu_int_regs->a7_ck_clear.value);
    printf("[%08X]A7_CK_ENABLE:                           %08X\n", (uint32_t)&cpu_int_regs->a7_ck_enable, cpu_int_regs->a7_ck_enable.value);
    printf("----------------------------------------------------------\n");
    printf("[%08X]CK_A7_INT:                              %08X\n", (uint32_t)&cpu_int_regs->ck_a7_int, cpu_int_regs->ck_a7_int.value);
    if (detail) _CpuDumpCKA7IRQFlags(cpu_int_regs->ck_a7_int);
    printf("[%08X]CK_A7_CLEAR:                            %08X\n", (uint32_t)&cpu_int_regs->ck_a7_clear, cpu_int_regs->ck_a7_clear.value);
    //if (detail) _CpuDumpCKA7IRQFlags(cpu_int_regs->ck_a7_clear);
    printf("[%08X]CK_A7_ENABLE:                           %08X\n", (uint32_t)&cpu_int_regs->ck_a7_enable, cpu_int_regs->ck_a7_enable.value);
    if (detail) _CpuDumpCKA7IRQFlags(cpu_int_regs->ck_a7_enable);

    printf("----------------------------------------------------------\n");
    volatile unsigned int *a7_ck_regs = (volatile unsigned int *)(MCU_REG_BASE_CONFIG + VSP_A7_CK_REG_OFFSET);
    for (int i = 0; i < 8; i++)
        printf("[%08X]A7_CK_REG%02d:                            %08X\n", (uint32_t)&a7_ck_regs[i], i, a7_ck_regs[i]);
    printf("----------------------------------------------------------\n");
#if 1
    volatile unsigned int *ck_a7_regs = (volatile unsigned int *)(MCU_REG_BASE_CONFIG + VSP_CK_A7_REG_OFFSET);
    for (int i = 0; i < 8; i++)
        printf("[%08X]CK_A7_REG%02d:                            %08X\n", (uint32_t)&ck_a7_regs[i], i, ck_a7_regs[i]);
#endif
}
#endif
