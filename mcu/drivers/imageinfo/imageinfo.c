/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * imageinfo.c: MCU IMAGEINFO partition read
 *
 */

#include <common.h>
#include <stdio.h>
#include <string.h>

#include <driver/spi_flash.h>
#include <driver/imageinfo.h>

#define LOG_TAG "[IMAGEINFO]"

static IMAGE_INFO imageinfo;

int ImageInfoGetImageVersion(unsigned int *image_version)
{
    *image_version = imageinfo.image_version;
    return 0;
}

int ImageInfoGetImageCRC32(unsigned int *crc32)
{
    *crc32 = imageinfo.image_crc32;
    return 0;
}

int ImageInfoInit(unsigned int offset)
{
    unsigned int imageinfo_crc32 = 0;
    unsigned int image_crc32 = 0;
    unsigned char buf[256] = {0};
    unsigned int index = 0;

    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Failed to initialize SPI Flash Driver!\n");
        return -1;
    }

    SpiFlashRead(offset, (unsigned char *)&imageinfo, sizeof(imageinfo));
    if (imageinfo.magic != IMAGEINFO_MAGIC) {
        printf(LOG_TAG"image info partition magic error! image info magic 0x%x, read 0x%x\n",
                IMAGEINFO_MAGIC, imageinfo.magic);
        return -1;
    }

    image_crc32 = 0;
    // Firmware locate before imageinfo
    for (index = 0; index < offset; index += sizeof(buf)) {
        SpiFlashRead(index, buf, sizeof(buf));
        image_crc32 = crc32(image_crc32, (const unsigned char *)buf, sizeof(buf));
    }
    if (image_crc32 != imageinfo.image_crc32) {
        printf(LOG_TAG"image partition damaged! read crc 0x%x, calc crc 0x%x\n",
                imageinfo.image_crc32, image_crc32);
        return -1;
    }

    imageinfo_crc32 = crc32(0, (unsigned char *)&imageinfo, sizeof(imageinfo)-IMAGEINFO_CRC32_LEN);
    if (imageinfo_crc32 != imageinfo.imageinfo_crc32) {
        printf(LOG_TAG"image info partition damaged! read crc 0x%x, calc crc 0x%x\n",
                imageinfo.imageinfo_crc32, imageinfo_crc32);
        return -1;
    }

    printf(LOG_TAG"image version    0x%x\n", imageinfo.image_version);
    printf(LOG_TAG"image crc32      0x%x\n", imageinfo.image_crc32);
    printf(LOG_TAG"imageinfo crc32  0x%x\n", imageinfo.imageinfo_crc32);

    return 0;
}

void ImageInfoDone(void)
{
    return;
}
