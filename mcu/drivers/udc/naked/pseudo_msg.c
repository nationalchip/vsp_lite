/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * pseudo_msg.c: usb transfer without protocol
 *
 */
#include <stdio.h>
#include <common.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include "../include/storage.h"
#include <driver/delay.h>

#include "f_mass_storage.h"

#include <soc_config.h>
#include "crc32.h"

#if 0
#define DBG(fmt, ...) printf (fmt, ##__VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif

#define FSG_DRIVER_DESC        "Private Mass Storage Function"
#define FSG_DRIVER_VERSION    "2009/09/11"

#define DRIVER_DESC        "Nationalchip Storage"
#define DRIVER_VERSION        "2009/09/11"

struct fsg_dev;
struct fsg_common;

struct fsg_common {
    struct usb_gadget           *gadget;
    struct usb_composite_dev    *cdev;
    struct fsg_dev              *fsg, *new_fsg;

    struct usb_ep               *ep0;        /* Copy of gadget->ep0 */
    struct usb_request          *ep0req;    /* Copy of cdev->req */
    unsigned int                 ep0_req_tag;

    struct fsg_buffhd           *next_buffhd_to_fill;
    struct fsg_buffhd           *next_buffhd_to_drain;
    struct fsg_buffhd           *buffhds;
    unsigned int                 fsg_num_buffers;

    int                          cmnd_size;
    u8                           cmnd[MAX_COMMAND_SIZE];

    unsigned int                 bulk_out_maxpacket;
    enum fsg_state               state;        /* For exception handling */
    unsigned int                 exception_req_tag;

    enum data_direction          data_dir;
    u32                          data_size;
    u32                          data_size_from_cmnd;
    u32                          tag;
    u32                          residue;
    u32                          usb_amount_left;

    unsigned int                 can_stall:1;
    unsigned int                 free_storage_on_release:1;
    unsigned int                 phase_error:1;
    unsigned int                 short_packet_received:1;
    unsigned int                 bad_lun_okay:1;
    unsigned int                 running:1;
    unsigned int                 sysfs:1;

    /* Callback functions. */
    const struct fsg_operations *ops;
    /* Gadget's private data. */
    void                        *private_data;

    /*
     * Vendor (8 chars), product (16 chars), release (4
     * hexadecimal digits) and NUL byte
     */
    //char inquiry_string[8 + 16 + 4 + 1];
    loff_t                       num_sectors;
    unsigned int                 blkbits; /* Bits of logical block size of bound block device */
    unsigned int                 blksize; /* logical block size of bound block device */

};

struct fsg_dev {
    struct usb_function    function;
    struct usb_gadget     *gadget;    /* Copy of cdev->gadget */
    struct fsg_common     *common;

    u16                    interface_number;

    unsigned int           bulk_in_enabled:1;
    unsigned int           bulk_out_enabled:1;

    unsigned long          atomic_bitflags;
#define IGNORE_BULK_OUT        0

    struct usb_ep         *bulk_in;
    struct usb_ep         *bulk_out;
};

/*
 * USB 2.0 devices need to expose both high speed and full speed
 * descriptors, unless they only run at full speed.
 * That means alternate endpoint descriptors (bigger packets)
 * and a "device qualifier" ... plus more construction options
 * for the configuration descriptor.
 */

/*
 * Three full-speed endpoint descriptors: bulk-in, bulk-out, and
 * interrupt-in.
 */

static struct usb_interface_descriptor fsg_intf_desc = {
    .bLength            =  sizeof fsg_intf_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bNumEndpoints      =  2,        /* Adjusted during fsg_bind() */
    .bInterfaceClass    =  USB_CLASS_MASS_STORAGE,
    .bInterfaceSubClass =  0x09,    /* Adjusted during fsg_bind() */
    .bInterfaceProtocol =  0x03,    /* Adjusted during fsg_bind() */
    .iInterface         =  FSG_STRING_INTERFACE,
};

static struct usb_endpoint_descriptor fsg_fs_bulk_in_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_IN,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
    /* wMaxPacketSize set by autoconfiguration */
};

static struct usb_endpoint_descriptor fsg_fs_bulk_out_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_OUT,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
    /* wMaxPacketSize set by autoconfiguration */
};

static struct usb_descriptor_header *fsg_fs_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_fs_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_fs_bulk_out_desc,
    NULL,
};

static struct usb_endpoint_descriptor fsg_hs_bulk_in_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
    .bmAttributes    =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize  =  cpu_to_le16(512),
};

static struct usb_endpoint_descriptor fsg_hs_bulk_out_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
    .bmAttributes    =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize  =  cpu_to_le16(512),
    .bInterval       =  1,    /* NAK every 1 uframe */
};

static struct usb_descriptor_header *fsg_hs_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_hs_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_hs_bulk_out_desc,
    NULL,
};


#if 0
static struct usb_device_descriptor msg_device_desc = {
    .bLength =        sizeof msg_device_desc,
    .bDescriptorType =    USB_DT_DEVICE,

    .bcdUSB =        cpu_to_le16(0x0200),
    .bDeviceClass =        USB_CLASS_PER_INTERFACE,

#if 0
    /* Vendor and product id can be overridden by module parameters.  */
    .idVendor =        cpu_to_le16(vendor_id),
    .idProduct =        cpu_to_le16(product_id),
#endif
    .bNumConfigurations =    1,
};

static struct usb_string strings_dev[] = {
    [USB_GADGET_MANUFACTURER_IDX].s = "",
    [USB_GADGET_PRODUCT_IDX].s = DRIVER_DESC,
    [USB_GADGET_SERIAL_IDX].s = "",
    {  } /* end of list */
};

static struct usb_gadget_strings stringtab_dev = {
    .language       = 0x0409,       /* en-us */
    .strings        = strings_dev,
};

static struct usb_gadget_strings *dev_strings[] = {
    &stringtab_dev,
    NULL,
};

static const char fsg_string_interface[] = "Mass Storage";

/* Static strings, in UTF-8 (for simplicity we use only ASCII characters) */
static struct usb_string        fsg_strings[] = {
    {FSG_STRING_INTERFACE,        fsg_string_interface},
    {}
};

static struct usb_gadget_strings    fsg_stringtab = {
    .language    = 0x0409,        /* en-us */
    .strings    = fsg_strings,
};


static struct usb_gadget_strings *fsg_strings_array[] = {
    &fsg_stringtab,
    NULL,
};

static struct usb_configuration msg_config_driver = {
    .label            = "Linux File-Backed Storage",
    .bConfigurationValue    = 1,
    //    .bmAttributes           = USB_CONFIG_ATT_SELFPOWER,
    .bmAttributes           = 0,
    .bMaxPower          = 250,
};
#endif

static struct fsg_module_parameters mod_data = {
    .file[0]       =  "fuke_dev",
    .removable[0]  =  1,
    .luns          =  1,
    .stall         =  1
};

/* structure for GET_INFO CMD */
struct system_info {
    __le32 info_version;
    __le32 len;
};

typedef void (*buff_handle_func)(unsigned char *buff, unsigned int len);

#define __round_mask(x, y) ((__typeof__(x))((y)-1))
#define round_up(x, y) ((((x)-1) | __round_mask(x, y))+1)
#define round_down(x, y) ((x) & ~__round_mask(x, y))

/*                 global data structure start                 */

static struct usb_function_instance *fi_msg;
static struct usb_function *f_msg;
static struct fsg_common *g_fsg_common;
static struct fsg_common g_common;

#define G_BUFFHD_NUM 2
static struct fsg_buffhd g_buffhds[G_BUFFHD_NUM];
#if 0
static u8 g_buffhd_buf0[FSG_BUFLEN] __attribute__ ((section (".usb_device_dma"))) __attribute__((aligned(64)));
static u8 g_buffhd_buf1[FSG_BUFLEN] __attribute__ ((section (".usb_device_dma"))) __attribute__((aligned(64)));
#else
static unsigned int fsg_buff_len = 0;
static u8 *g_buffhd_buf0 = NULL;
static u8 *g_buffhd_buf1 = NULL;
#endif

static struct fsg_opts g_opts;

static struct fsg_dev g_fsg;

#define STAGE1_SIZE (32 * 1024) - 4 // including CRC
static unsigned int sram_addr = CONFIG_STAGE1_DRAM_BASE;
static unsigned int sram_offset = 0;

static unsigned int do_crc = 0;
static   signed int sram_crc_result = 0; // 0 crc unknown, 1 crc correct, -1 crc error

static int do_return = 0;

#define SYSINFO_VERSION 0x00000001
static struct system_info sys_info = {
    .info_version = cpu_to_le32(SYSINFO_VERSION),
    .len          = cpu_to_le32(sizeof(struct system_info)),
};

/*                 global data structure end                 */


//////////////////////funciton msg driver///////////////////////////////////
static inline int __fsg_is_set(struct fsg_common *common,
        const char *func, unsigned line)
{
    if (common->fsg)
        return 1;
    DBG("common->fsg is NULL in %s at %u\n", func, line);

    return 0;
}

#define fsg_is_set(common) likely(__fsg_is_set(common, __func__, __LINE__))

static inline struct fsg_dev *fsg_from_func(struct usb_function *f)
{
    return container_of(f, struct fsg_dev, function);
}

/* check if fsg_num_buffers is within a valid range */
static inline int fsg_num_buffers_validate(unsigned int fsg_num_buffers)
{
#define FSG_MAX_NUM_BUFFERS    32

    if (fsg_num_buffers >= 2 && fsg_num_buffers <= FSG_MAX_NUM_BUFFERS)
        return 0;
    DBG("fsg_num_buffers %u is out of range (%d to %d)\n",
            fsg_num_buffers, 2, FSG_MAX_NUM_BUFFERS);
    return -EINVAL;
}

static void _fsg_common_free_buffers(struct fsg_buffhd *buffhds, unsigned n)
{
#if 0
    if (buffhds) {
        struct fsg_buffhd *bh = buffhds;
        while (n--) {
            free(bh->buf);
            ++bh;
        }
        free(buffhds);
    }
#endif

    return ;
}

static void fsg_common_free_buffers(struct fsg_common *common)
{
    _fsg_common_free_buffers(common->buffhds, common->fsg_num_buffers);
    common->buffhds = NULL;

    return ;
}

static int fsg_common_set_num_buffers(struct fsg_common *common, unsigned int n)
{
    struct fsg_buffhd *bh, *buffhds;
    int rc;

    rc = fsg_num_buffers_validate(n);
    if (rc != 0)
        return rc;

    buffhds = g_buffhds;
    bh = buffhds;
    bh->buf = g_buffhd_buf0;
    bh->next = bh + 1;
    ++bh;
    bh->buf = g_buffhd_buf1;
    bh->next = buffhds;

    _fsg_common_free_buffers(common->buffhds, common->fsg_num_buffers);
    common->fsg_num_buffers = n;
    common->buffhds = buffhds;

    return 0;
}

static void set_bulk_out_req_length(struct fsg_common *common,
        struct fsg_buffhd *bh, unsigned int length)
{
    unsigned int    rem = 0;

    bh->bulk_out_intended_length = length;

    rem = length % common->bulk_out_maxpacket;

    if (rem > 0)
        length += common->bulk_out_maxpacket - rem;
    bh->outreq->length = length;

    return ;
}

static int wedge_bulk_in_endpoint(struct fsg_dev *fsg)
{
    int    rc;

    DBG("bulk-in set wedge\n");
    rc = usb_ep_set_wedge(fsg->bulk_in);
    if (rc == -EAGAIN)
        DBG("delayed bulk-in endpoint wedge\n");
    while (rc != 0) {
        if (rc != -EAGAIN) {
            DBG("usb_ep_set_wedge -> %d\n", rc);
            rc = 0;
            break;
        }

        /* Wait for a short time and then try again */
        udelay(100);
        rc = usb_ep_set_wedge(fsg->bulk_in);
    }
    return rc;
}


#if 0
static int ep0_queue(struct fsg_common *common)
{
    int    rc;

    rc = usb_ep_queue(common->ep0, common->ep0req);
    common->ep0->driver_data = common;
    if (rc != 0 && rc != -ESHUTDOWN) {
        /* We can't do much more than wait for a reset */
        DBG( "error in submission: %s --> %d\n",
                common->ep0->name, rc);
    }
    return rc;
}
#endif

static void bulk_in_complete(struct usb_ep *ep, struct usb_request *req)
{
    //struct fsg_common    *common = ep->driver_data;
    struct fsg_buffhd    *bh = req->context;

    if (req->status || req->actual != req->length)
        DBG("%s --> %d, %u/%u\n", __func__, req->status, req->actual, req->length);
    if (req->status == -ECONNRESET)        /* Request was cancelled */
        usb_ep_fifo_flush(ep);

    /* Hold the lock while we update the request and buffer states */
    bh->inreq_busy = 0;
    bh->state = BUF_STATE_EMPTY;

    return ;
}

static void bulk_out_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct fsg_buffhd    *bh = req->context;

    if (req->status || req->actual != bh->bulk_out_intended_length)
        DBG("%s --> %d, %u/%u\n", __func__,req->status, req->actual, bh->bulk_out_intended_length);
    if (req->status == -ECONNRESET)        /* Request was cancelled */
        usb_ep_fifo_flush(ep);

    /* Hold the lock while we update the request and buffer states */
    bh->outreq_busy = 0;
    bh->state = BUF_STATE_FULL;

    return ;
}

static void start_transfer(struct fsg_dev *fsg, struct usb_ep *ep,
        struct usb_request *req, int *pbusy,
        enum fsg_buffer_state *state)
{
    int    rc;

    if (ep == fsg->bulk_in)
        DBG("bulk-in buf(%p),len(0x%x)\n", req->buf, req->length);

    *pbusy = 1;
    *state = BUF_STATE_BUSY;


    rc = usb_ep_queue(ep, req);
    if (rc == 0)
        return;  /* All good, we're done */

    *pbusy = 0;
    *state = BUF_STATE_EMPTY;

    /* We can't do much more than wait for a reset */

    /*
     * Note: currently the net2280 driver fails zero-length
     * submissions if DMA is enabled.
     */
    if (rc != -ESHUTDOWN && !(rc == -EOPNOTSUPP && req->length == 0))
        DBG("error in submission: %s --> %d\n", ep->name, rc);

    return ;
}

static bool start_in_transfer(struct fsg_common *common, struct fsg_buffhd *bh)
{
    if (!fsg_is_set(common) || !bh->inreq->buf) {
        DBG("%s, empty buffer point\n", __func__);
        return false;
    }

    start_transfer(common->fsg, common->fsg->bulk_in,
            bh->inreq, &bh->inreq_busy, &bh->state);
    return true;
}

static bool start_out_transfer(struct fsg_common *common, struct fsg_buffhd *bh)
{
    if (!fsg_is_set(common) || !bh->outreq->buf) {
        DBG("%s, empty buffer point\n", __func__);
        return false;
    }

    start_transfer(common->fsg, common->fsg->bulk_out,
            bh->outreq, &bh->outreq_busy, &bh->state);
    return true;
}

// copy from fsg_read_basic
int fsg_read_basic_for_rom(int length)
{
    struct fsg_common    *common = g_fsg_common;
    struct fsg_buffhd    *bh;
    int            get_some_more = 1;
    u32                     amount, offset = 0;
    u32                     left;
    u32                     act_write;

    if (unlikely(length <= 0))
        return -1;

    left = length;
    common->next_buffhd_to_drain = common->next_buffhd_to_fill;

    while (1) {
        bh = common->next_buffhd_to_fill;
        /* Queue a request for more data from the host */
        if (bh->state == BUF_STATE_EMPTY && get_some_more) {

            amount = min(left, fsg_buff_len);
            left -= amount;

            if (left == 0)
                get_some_more = 0;

            set_bulk_out_req_length(common, bh, amount);
            if (!start_out_transfer(common, bh))
                return -EIO;

            common->next_buffhd_to_fill = bh->next;
            continue;
        }

        /* Write the received data to the backing file */
        bh = common->next_buffhd_to_drain;
        if (bh->state == BUF_STATE_EMPTY && bh->next->state == BUF_STATE_EMPTY && !get_some_more)
            break;
        if (bh->state == BUF_STATE_FULL) {
            common->next_buffhd_to_drain = bh->next;
            bh->state = BUF_STATE_EMPTY;

            /* Did something go wrong with the transfer? */
            if (bh->outreq->status != 0) {
                DBG("%s outreq(%p): it is impossible to occur send_status in the future 1\n", __func__, bh->outreq);
                break;
            }

            amount = bh->outreq->actual;
            /* Don't accept excess data.  The spec doesn't say
             * what to do in this case.  We'll ignore the error.
             */
            amount = min(amount, bh->bulk_out_intended_length);

            if (amount == 0)
                goto empty_write;

            /* Perform the write */
            {
                if (sram_offset + amount > STAGE1_SIZE)
                    act_write = STAGE1_SIZE - sram_offset;
                else
                    act_write = amount;

                memcpy((void *)(sram_addr + sram_offset), bh->outreq->buf, act_write);
                sram_offset += act_write;

                if (sram_offset == STAGE1_SIZE && act_write != 0)
                    do_crc = 1;
            }

empty_write:
            /* Did the host decide to stop early? */
            if (bh->outreq->actual < bh->bulk_out_intended_length) {
                common->short_packet_received = 1;
                break;
            }
            continue;
        }
    }

    return offset;
}

int fsg_read_basic(unsigned char *buff, int length, buff_handle_func func)
{
    struct fsg_common    *common = g_fsg_common;
    struct fsg_buffhd    *bh;
    int            get_some_more = 1;
    u32                     amount, offset = 0;
    u32                     left;

    if (unlikely(length <= 0))
        return -1;

    left = length;
    common->next_buffhd_to_drain = common->next_buffhd_to_fill;

    while (1) {
        bh = common->next_buffhd_to_fill;
        /* Queue a request for more data from the host */
        if (bh->state == BUF_STATE_EMPTY && get_some_more) {

            amount = min(left, fsg_buff_len);
            left -= amount;

            if (left == 0)
                get_some_more = 0;

            set_bulk_out_req_length(common, bh, amount);
            if (!start_out_transfer(common, bh))
                return -EIO;

            common->next_buffhd_to_fill = bh->next;
            continue;
        }

        /* Write the received data to the backing file */
        bh = common->next_buffhd_to_drain;
        if (bh->state == BUF_STATE_EMPTY && bh->next->state == BUF_STATE_EMPTY && !get_some_more)
            break;
        if (bh->state == BUF_STATE_FULL) {
            common->next_buffhd_to_drain = bh->next;
            bh->state = BUF_STATE_EMPTY;

            /* Did something go wrong with the transfer? */
            if (bh->outreq->status != 0) {
                DBG("%s outreq(%p): it is impossible to occur send_status in the future 1\n", __func__, bh->outreq);
                break;
            }

            amount = bh->outreq->actual;
            /* Don't accept excess data.  The spec doesn't say
             * what to do in this case.  We'll ignore the error.
             */
            amount = min(amount, bh->bulk_out_intended_length);

            if (amount == 0)
                goto empty_write;

            /* Perform the write */
            {
                if (buff) {
                    memcpy(buff + offset, bh->outreq->buf, amount);
                    if (func)
                        func(buff + offset, amount);
                }
                offset += amount;
            }

empty_write:
            /* Did the host decide to stop early? */
            if (bh->outreq->actual < bh->bulk_out_intended_length) {
                common->short_packet_received = 1;
                break;
            }
            continue;
        }
    }

    return offset;
}

static int already_send = 0;
static int fsg_read_noblock(unsigned char *buf, int len)
{
    struct fsg_common    *common = g_fsg_common;
    struct fsg_buffhd   *bh  = NULL;
    struct usb_request  *req = NULL;
    int size;

    /* Wait for the next buffer to become available */
    bh = common->next_buffhd_to_fill;

    if (already_send == 0) {

        if (bh->state != BUF_STATE_EMPTY)
            return 0;

        /* Queue a request to read a Bulk-only CBW */
        set_bulk_out_req_length(common, bh, common->bulk_out_maxpacket);
        if (!start_out_transfer(common, bh)) {
            printf ("%s, start out transfer failed\n", __func__);
            return -EIO;
        }

        already_send = 1;

        return 0;
    } else {
        /*
         * We will drain the buffer in software, which means we
         * can reuse it for the next filling.  No need to advance
         * next_buffhd_to_fill.
         */

        /* Wait for the CBW to arrive */
        if (bh->state != BUF_STATE_FULL)
            return 0;

        req = bh->outreq;
        size = min(req->actual, len);
        if (req->actual > len)
            printf ("%s, recv size(%d) is big than buff size(%d)\n", __func__, req->actual, len);
        memcpy(buf, req->buf, size);

        bh->state = BUF_STATE_EMPTY;
        already_send = 0;

        return size;
    }
}

int fsg_read(unsigned char *buff, int len)
{
    return fsg_read_basic(buff, len, NULL);
}

int fsg_write(unsigned char *buff, int length)
{
    struct fsg_buffhd       *bh;
    struct fsg_common    *common = g_fsg_common;
    int                      amount, amount_left;
    int                      offset = 0;

    if (unlikely(length <= 0))
        return -1;

    amount_left = length;

    for (;;) {
        amount = min(amount_left, fsg_buff_len);

        bh = common->next_buffhd_to_fill;
        while (bh->state != BUF_STATE_EMPTY)
            udelay(1);

        {
            if (buff)
                memcpy (bh->buf, buff + offset, amount);
            offset += amount;
        }

        amount_left      -= amount;
        bh->inreq->length = amount;
        bh->state         = BUF_STATE_FULL;
        bh->inreq->zero   = 0;
        DBG ("%s, do in transfer, bh : %p, length : %d\n", __func__, bh, bh->inreq->length);
        if (!start_in_transfer(common, bh))
            return -EIO;

        if (amount_left == 0)
            break;

        common->next_buffhd_to_fill = bh->next;
    }

    while (bh->state != BUF_STATE_EMPTY || bh->next->state != BUF_STATE_EMPTY)
        udelay(10);

    return offset;        /* No default reply */
}

#if 0
static int fsg_set_halt(struct fsg_dev *fsg, struct usb_ep *ep)
{
    const char    *name;

    if (ep == fsg->bulk_in)
        name = "bulk-in";
    else if (ep == fsg->bulk_out)
        name = "bulk-out";
    else
        name = ep->name;
    DBG("%s set halt\n", name);
    return usb_ep_set_halt(ep);
}


static int halt_bulk_in_endpoint(struct fsg_dev *fsg)
{
    int    rc;
    rc = fsg_set_halt(fsg, fsg->bulk_in);
    return rc;
}
#endif

static int send_status(struct fsg_common *common)
{
    struct fsg_buffhd    *bh;
    struct bulk_cs_wrap    *csw;
    u8            status = US_BULK_STAT_OK;

    /* Wait for the next buffer to become available */
    bh = common->next_buffhd_to_fill;
    while (bh->state != BUF_STATE_EMPTY) {
        udelay(100);
    }

    /* Store and send the Bulk-only CSW */
    csw = (void *)bh->buf;

    csw->Signature = cpu_to_le32(US_BULK_CS_SIGN);
    csw->Tag = common->tag;
    csw->Residue = cpu_to_le32(common->residue);
    csw->Status = status;

    DBG ("csw : sig : %x, Tag : %x, Residue : %d, status : %d\n", csw->Signature, csw->Tag, csw->Residue, csw->Status);

    bh->inreq->length = US_BULK_CS_WRAP_LEN;
    bh->inreq->zero = 0;
    if (!start_in_transfer(common, bh))
        /* Don't know what to do if common->fsg is NULL */
        return -EIO;

    while (bh->state != BUF_STATE_EMPTY) {
        udelay(10);
    }

    common->next_buffhd_to_fill = bh->next;
    return 0;
}

#if 0
static int finish_reply(struct fsg_common *common)
{
    struct fsg_buffhd    *bh = common->next_buffhd_to_fill;
    int            rc = 0;

    switch (common->data_dir) {
    case DATA_DIR_NONE:
        break;            /* Nothing to send */

        /*
         * If we don't know whether the host wants to read or write,
         * this must be CB or CBI with an unknown command.  We mustn't
         * try to send or receive any data.  So stall both bulk pipes
         * if we can and wait for a reset.
         */
    case DATA_DIR_UNKNOWN:
        if (!common->can_stall) {
            /* Nothing */
        } else if (fsg_is_set(common)) {
            fsg_set_halt(common->fsg, common->fsg->bulk_out);
            rc = halt_bulk_in_endpoint(common->fsg);
        } else {
            /* Don't know what to do if common->fsg is NULL */
            rc = -EIO;
        }
        break;
        /* All but the last buffer of data must have already been sent */
    case DATA_DIR_TO_HOST:
        if (common->data_size == 0) {
            /* Nothing to send */

            /* Don't know what to do if common->fsg is NULL */
        } else if (!fsg_is_set(common)) {
            rc = -EIO;

            /* If there's no residue, simply send the last buffer */
        } else if (common->residue == 0) {
            bh->inreq->zero = 0;
            if (!start_in_transfer(common, bh))
                return -EIO;
            common->next_buffhd_to_fill = bh->next;

            /*
             * For Bulk-only, mark the end of the data with a short
             * packet.  If we are allowed to stall, halt the bulk-in
             * endpoint.  (Note: This violates the Bulk-Only Transport
             * specification, which requires us to pad the data if we
             * don't halt the endpoint.  Presumably nobody will mind.)
             */
        } else {
            bh->inreq->zero = 1;
            if (!start_in_transfer(common, bh))
                rc = -EIO;
            common->next_buffhd_to_fill = bh->next;
            if (common->can_stall)
                rc = halt_bulk_in_endpoint(common->fsg);
        }
        break;

        /*
         * We have processed all we want from the data the host has sent.
         * There may still be outstanding bulk-out requests.
         */
    case DATA_DIR_FROM_HOST:
        if (common->residue == 0) {
            /* Nothing to receive */

            /* Did the host stop sending unexpectedly early? */
        } else if (common->short_packet_received) {
            //raise_exception(common, FSG_STATE_ABORT_BULK_OUT);
            send_status(common);
            common->state = FSG_STATE_IDLE;
            rc = -EINTR;

            /*
             * We can't stall.  Read in the excess data and throw it
             * all away.
             */
        } else {
            //rc = throw_away_data(common);
        }
        break;
    }
    return rc;
}

static void stage2_crc_func(unsigned char *buff, unsigned int len)
{
    int i;
    for (i = 0; i < len; i++)
        stage2_crc_result += buff[i];
}
#endif

static int do_get_crc_result(struct fsg_common *common, struct fsg_buffhd *bh)
{
    u8 *buf = (u8 *)bh->buf;

    *(unsigned int *)buf = cpu_to_le32(sram_crc_result);

    // prepare for the next data transfer for stage1 data
    if (sram_crc_result == -1) {
        sram_crc_result = 0;
        sram_offset = 0;
    }

    return 4;
}

static int do_get_info(struct fsg_common *common, struct fsg_buffhd *bh)
{
    u8   *buf = (u8 *)bh->buf;
    u32  len = common->data_size_from_cmnd;

    if (len > sizeof(struct system_info))
        len = sizeof(struct system_info);

    memcpy (buf, &sys_info, len);

    return len;
}

static int do_priv_command(struct fsg_common *common)
{
    struct fsg_buffhd    *bh;
    int len;

    /* Wait for the next buffer to become available for data or status */
    bh = common->next_buffhd_to_fill;
    common->next_buffhd_to_drain = bh;

    while (bh->state != BUF_STATE_EMPTY);

    common->phase_error = 0;
    common->short_packet_received = 0;

    switch (common->cmnd[0]) {
    case WRITE_10:
        common->data_size_from_cmnd = common->data_size;
        fsg_read_basic_for_rom(common->data_size_from_cmnd);
        break;

    case GET_STATUS:
        common->data_size_from_cmnd = common->data_size;
        len = do_get_crc_result(common, bh);
        fsg_write(bh->buf, len);
        break;

    case GET_INFO:
        common->data_size_from_cmnd = common->data_size;
        len = do_get_info(common, bh);
        fsg_write(bh->buf, len);
        break;

    case DO_RUN:
    case DO_EXIT:
        do_return = 1;
        break;

    default:
        common->data_size_from_cmnd = 0;
        DBG("%s recieve unknow cmd\n", __func__);

        break;
    }

    return 0;
}

static int received_cbw(struct fsg_dev *fsg, struct fsg_buffhd *bh)
{
    struct usb_request    *req = bh->outreq;
    struct bulk_cb_wrap    *cbw = req->buf;
    struct fsg_common    *common = fsg->common;

    /* Was this a real packet?  Should it be ignored? */
#if 0
    if (req->status || test_bit(IGNORE_BULK_OUT, &fsg->atomic_bitflags))
        return -EINVAL;
#else
    if (req->status || ((1 << IGNORE_BULK_OUT) & fsg->atomic_bitflags) )
        return -EINVAL;
#endif

    /* Is the CBW valid? */
    if (req->actual != US_BULK_CB_WRAP_LEN ||
            cbw->Signature != cpu_to_le32(
                US_BULK_CB_SIGN)) {
        DBG("invalid CBW: len %u sig 0x%x\n",
                req->actual,
                le32_to_cpu(cbw->Signature));

        /*
         * The Bulk-only spec says we MUST stall the IN endpoint
         * (6.6.1), so it's unavoidable.  It also says we must
         * retain this state until the next reset, but there's
         * no way to tell the controller driver it should ignore
         * Clear-Feature(HALT) requests.
         *
         * We aren't required to halt the OUT endpoint; instead
         * we can simply accept and discard any data received
         * until the next reset.
         */
        wedge_bulk_in_endpoint(fsg);
#if 0
        set_bit(IGNORE_BULK_OUT, &fsg->atomic_bitflags);
#else
        fsg->atomic_bitflags |= (1 << IGNORE_BULK_OUT);
#endif
        return -EINVAL;
    }

    /* Save the command for later */
    common->cmnd_size = cbw->Length;
    memcpy(common->cmnd, cbw->CDB, common->cmnd_size);
    if (cbw->Flags & US_BULK_FLAG_IN)
        common->data_dir = DATA_DIR_TO_HOST;
    else
        common->data_dir = DATA_DIR_FROM_HOST;
    common->data_size = le32_to_cpu(cbw->DataTransferLength);
    if (common->data_size == 0)
        common->data_dir = DATA_DIR_NONE;

    common->usb_amount_left = common->data_size;
    common->residue = common->data_size;

    common->tag = cbw->Tag;
    return 0;
}

static int get_next_command(struct fsg_common *common)
{
    struct fsg_buffhd    *bh;
    int            rc = 0;
    static int send_out_transfer = 0;

    /* Wait for the next buffer to become available */
    bh = common->next_buffhd_to_fill;

    if (send_out_transfer == 0) {

        if (bh->state != BUF_STATE_EMPTY)
            return -EBUSY;

        /* Queue a request to read a Bulk-only CBW */
        set_bulk_out_req_length(common, bh, US_BULK_CB_WRAP_LEN);
        if (!start_out_transfer(common, bh)) {
            printf ("%s, start out transfer failed\n", __func__);
            return -EIO;
        }

        send_out_transfer = 1;
        rc = -EBUSY;
    } else {
        /*
         * We will drain the buffer in software, which means we
         * can reuse it for the next filling.  No need to advance
         * next_buffhd_to_fill.
         */

        /* Wait for the CBW to arrive */
        if (bh->state != BUF_STATE_FULL)
            return -EBUSY;

        send_out_transfer = 0;
        rc = fsg_is_set(common) ? received_cbw(common->fsg, bh) : -EIO;
        bh->state = BUF_STATE_EMPTY;
    }

    return rc;
}

static int alloc_request(struct fsg_common *common, struct usb_ep *ep,
        struct usb_request **preq)
{
    *preq = usb_ep_alloc_request(ep);
    if (*preq)
        return 0;
    DBG("can't allocate request for %s\n", ep->name);
    return -ENOMEM;
}

static struct fsg_common *fsg_common_setup(struct fsg_common *common)
{
    if (!common) {
        common = &g_common;
        memset(common, 0, sizeof(*common));
        common->free_storage_on_release = 1;
    } else {
        common->free_storage_on_release = 0;
    }

    common->state = FSG_STATE_IDLE;

    return common;
}

/* Reset interface setting and re-init endpoint state (toggle etc). */
static int do_set_interface(struct fsg_common *common, struct fsg_dev *new_fsg)
{
    struct fsg_dev *fsg;
    int i, rc = 0;

    if (common->running)
        DBG("reset interface\n");

reset:
    /* Deallocate the requests */
    if (common->fsg) {
        fsg = common->fsg;

        for (i = 0; i < common->fsg_num_buffers; ++i) {
            struct fsg_buffhd *bh = &common->buffhds[i];

            if (bh->inreq) {
                usb_ep_free_request(fsg->bulk_in, bh->inreq);
                bh->inreq = NULL;
            }
            if (bh->outreq) {
                usb_ep_free_request(fsg->bulk_out, bh->outreq);
                bh->outreq = NULL;
            }
        }

        /* Disable the endpoints */
        if (fsg->bulk_in_enabled) {
            usb_ep_disable(fsg->bulk_in);
            fsg->bulk_in_enabled = 0;
        }
        if (fsg->bulk_out_enabled) {
            usb_ep_disable(fsg->bulk_out);
            fsg->bulk_out_enabled = 0;
        }

        common->fsg = NULL;
        //wake_up(&common->fsg_wait);
    }

    common->running = 0;
    if (!new_fsg || rc)
        return rc;

    common->fsg = new_fsg;
    fsg = common->fsg;

    /* Enable the endpoints */
    rc = config_ep_by_speed(common->gadget, &(fsg->function), fsg->bulk_in);
    if (rc)
        goto reset;
    rc = usb_ep_enable(fsg->bulk_in);
    if (rc)
        goto reset;
    fsg->bulk_in->driver_data = common;
    fsg->bulk_in_enabled = 1;

    rc = config_ep_by_speed(common->gadget, &(fsg->function),
            fsg->bulk_out);
    if (rc)
        goto reset;
    rc = usb_ep_enable(fsg->bulk_out);
    if (rc)
        goto reset;
    fsg->bulk_out->driver_data = common;
    fsg->bulk_out_enabled = 1;
    common->bulk_out_maxpacket = usb_endpoint_maxp(fsg->bulk_out->desc);

#if 0
    clear_bit(IGNORE_BULK_OUT, &fsg->atomic_bitflags);
#else
    fsg->atomic_bitflags &= ~(1 << IGNORE_BULK_OUT);
#endif

    /* Allocate the requests */
    for (i = 0; i < common->fsg_num_buffers; ++i) {
        struct fsg_buffhd    *bh = &common->buffhds[i];

        rc = alloc_request(common, fsg->bulk_in, &bh->inreq);
        if (rc)
            goto reset;
        rc = alloc_request(common, fsg->bulk_out, &bh->outreq);
        if (rc)
            goto reset;
        bh->inreq->buf = bh->outreq->buf = bh->buf;
        bh->inreq->context = bh->outreq->context = bh;
        bh->inreq->complete = bulk_in_complete;
        bh->outreq->complete = bulk_out_complete;
    }

    common->running = 1;
    return rc;
}

static void basic_env_init(void)
{
    already_send = 0;
}

static int fsg_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
    struct fsg_dev *fsg = fsg_from_func(f);
    struct fsg_common    *common = fsg->common;
    struct fsg_buffhd *bh;
    int i;

    fsg->common->new_fsg = fsg;

    for (i = 0; i < common->fsg_num_buffers; ++i) {
        bh = &common->buffhds[i];
        bh->state = BUF_STATE_EMPTY;
    }

    common->next_buffhd_to_fill = &common->buffhds[0];
    common->next_buffhd_to_drain = &common->buffhds[0];
    //exception_req_tag = common->exception_req_tag;

    do_set_interface(common, common->new_fsg);
    if (common->new_fsg)
        usb_composite_setup_continue(common->cdev);

    // 在当前功能正常开始前，需要保证部分环境的干净。而 set_alt 就是当前功能的开始
    basic_env_init();

    return 0;

}

static void fsg_disable(struct usb_function *f)
{
    struct fsg_dev *fsg = fsg_from_func(f);
    struct fsg_common    *common = fsg->common;

    common->new_fsg = NULL;
    common->running = 0;

    return ;
}

static int fsg_setup(struct usb_function *f,
        const struct usb_ctrlrequest *ctrl)
{
    struct fsg_dev        *fsg = fsg_from_func(f);
    struct usb_request    *req = fsg->common->ep0req;

    if (!fsg_is_set(fsg->common))
        return -EOPNOTSUPP;

    ++fsg->common->ep0_req_tag;    /* Record arrival of a new request */
    req->context = NULL;
    req->length = 0;

    switch (ctrl->bRequest) {
    case US_BULK_RESET_REQUEST:
        DBG("fsg_setup can't handle US_BULK_RESET_REQUEST\n");
        break;

    case US_BULK_GET_MAX_LUN:
        DBG("fsg_setup can't handle US_BULK_GET_MAX_LUN\n");
        break;

    }
    return -EOPNOTSUPP;
}

static int fsg_common_set_cdev(struct fsg_common *common,
        struct usb_composite_dev *cdev, bool can_stall)
{
#if 0
    struct usb_string *us;
#endif

    common->gadget = cdev->gadget;
    common->ep0 = cdev->gadget->ep0;
    common->ep0req = cdev->req;
    common->cdev = cdev;

#if 0
    us = usb_gstrings_attach(cdev, fsg_strings_array,
            ARRAY_SIZE(fsg_strings));
    if (!us)
        return -1;

    fsg_intf_desc.iInterface = us[FSG_STRING_INTERFACE].id;
#endif

    /*
     * Some peripheral controllers are known not to be able to
     * halt bulk endpoints correctly.  If one of them is present,
     * disable stalls.
     */
    common->can_stall = can_stall;

    return 0;
}

static int fsg_bind(struct usb_configuration *c, struct usb_function *f)
{
    struct fsg_dev        *fsg = fsg_from_func(f);
    struct usb_gadget    *gadget = c->cdev->gadget;
    int            i;
    struct usb_ep        *ep;
    int            ret;
    struct fsg_opts        *opts;

    opts = fsg_opts_from_func_inst(f->fi);
    if (!opts->no_configfs) {
        ret = fsg_common_set_cdev(fsg->common, c->cdev,
                fsg->common->can_stall);
        if (ret)
            return ret;
        //fsg_common_set_inquiry_string(fsg->common, NULL, NULL);
    }

    fsg->gadget = gadget;

    /* New interface */
    i = usb_interface_id(c, f);
    if (i < 0)
        goto fail;
    fsg_intf_desc.bInterfaceNumber = i;
    fsg->interface_number = i;

    /* Find all the endpoints we will use */
    ep = usb_ep_autoconfig(gadget, &fsg_fs_bulk_in_desc);
    if (!ep)
        goto autoconf_fail;
    fsg->bulk_in = ep;

    ep = usb_ep_autoconfig(gadget, &fsg_fs_bulk_out_desc);
    if (!ep)
        goto autoconf_fail;
    fsg->bulk_out = ep;

    /* Assume endpoint addresses are the same for both speeds */
    fsg_hs_bulk_in_desc.bEndpointAddress =
        fsg_fs_bulk_in_desc.bEndpointAddress;
    fsg_hs_bulk_out_desc.bEndpointAddress =
        fsg_fs_bulk_out_desc.bEndpointAddress;

#if 0
    /* Calculate bMaxBurst, we know packet size is 1024 */
    max_burst = min_t(unsigned, fsg_buff_len / 1024, 15);

    fsg_ss_bulk_in_desc.bEndpointAddress =
        fsg_fs_bulk_in_desc.bEndpointAddress;
    fsg_ss_bulk_in_comp_desc.bMaxBurst = max_burst;

    fsg_ss_bulk_out_desc.bEndpointAddress =
        fsg_fs_bulk_out_desc.bEndpointAddress;
    fsg_ss_bulk_out_comp_desc.bMaxBurst = max_burst;
#endif

    ret = usb_assign_descriptors(f, fsg_fs_function, fsg_hs_function,
            NULL);
    if (ret)
        goto autoconf_fail;

    return 0;

autoconf_fail:
    //ERROR(fsg, "unable to autoconfigure all endpoints\n");
    i = -ENOTSUPP;
fail:
    /* terminate the thread */
    if (fsg->common->state != FSG_STATE_TERMINATED) {
        ;
    }
    return i;
}

static void fsg_unbind(struct usb_configuration *c, struct usb_function *f)
{
#if 0
    struct fsg_dev        *fsg = fsg_from_func(f);
    struct fsg_common    *common = fsg->common;

    DBG(fsg, "unbind\n");
    if (fsg->common->fsg == fsg) {
        fsg->common->new_fsg = NULL;
        //raise_exception(fsg->common, FSG_STATE_CONFIG_CHANGE);
        /* FIXME: make interruptible or killable somehow? */
        //wait_event(common->fsg_wait, common->fsg != fsg);
    }

    usb_free_all_descriptors(&fsg->function);
#endif

    return ;
}

static void fsg_free_inst(struct usb_function_instance *fi)
{
#if 0
    struct fsg_opts *opts;

    opts = fsg_opts_from_func_inst(fi);
    fsg_common_put(opts->common);
    free(opts);
#endif

    return ;
}

static void fsg_free(struct usb_function *f)
{
#if 0
    struct fsg_dev *fsg;
    struct fsg_opts *opts;

    fsg = container_of(f, struct fsg_dev, function);
    opts = container_of(f->fi, struct fsg_opts, func_inst);

    free(fsg);
#endif

    return ;
}

static struct usb_function_instance *fsg_alloc_inst(void)
{
    struct fsg_opts *opts;
    int rc;

    opts = &g_opts;

    memset(opts, 0, sizeof(*opts));
    opts->func_inst.free_func_inst = fsg_free_inst;
    opts->common = fsg_common_setup(opts->common);
    if (!opts->common) {
        goto release_opts;
    }
    g_fsg_common = opts->common;

    rc = fsg_common_set_num_buffers(opts->common,2);
    if (rc)
        goto release_opts;

    return &opts->func_inst;

release_opts:
    return NULL;
}

static void fsg_suspend(struct usb_function *func)
{
    struct fsg_dev *fsg = fsg_from_func(func);

    fsg->common->running = 0;

    return ;
}

static void fsg_resume(struct usb_function *func)
{
    func = func;

    return ;
}

static struct usb_function *fsg_alloc(struct usb_function_instance *fi)
{
    struct fsg_opts *opts = fsg_opts_from_func_inst(fi);
    struct fsg_common *common = opts->common;
    struct fsg_dev *fsg;

    fsg = &g_fsg;

    memset(fsg, 0, sizeof(*fsg));
    fsg->function.name    = FSG_DRIVER_DESC;
    fsg->function.bind    = fsg_bind;
    fsg->function.unbind    = fsg_unbind;
    fsg->function.setup    = fsg_setup;
    fsg->function.set_alt    = fsg_set_alt;
    fsg->function.disable    = fsg_disable;
    fsg->function.free_func    = fsg_free;

    fsg->function.suspend    = fsg_suspend;
    fsg->function.resume    = fsg_resume;

    fsg->common               = common;

    return &fsg->function;
}

#define MSG_FUNC_STR "private mass_storage"
static struct usb_function_driver msg_usb_func = {
    .name = MSG_FUNC_STR,
    .alloc_inst = fsg_alloc_inst,
    .alloc_func = fsg_alloc,
};

int  mass_storage_mod_init(void)
{
    return usb_function_register(&msg_usb_func);
}

int  mass_storage_mod_done(void)
{
    usb_function_unregister(&msg_usb_func);

    return 0;
}

// ---------------------------------- Export Symbol ----------------------------------

int NakedDoConfig(struct usb_configuration *c)
{
    int ret;

    f_msg = usb_get_function(fi_msg);
    if (!f_msg){
        printf ("%s, get function failed\n", __func__);
        return 0;
    }
    ret = usb_add_function(c, f_msg);
    if (ret)
        goto put_func;

    return 0;

put_func:
    usb_put_function(f_msg);
    return ret;
}

int NakedBind(struct usb_composite_dev *cdev)
{
    struct fsg_opts *opts;
    int can_stall;
    int status;

    fi_msg = usb_get_function_instance(MSG_FUNC_STR); // fsg_alloc_inst

    opts = fsg_opts_from_func_inst(fi_msg);

    opts->no_configfs = true;

    can_stall = mod_data.stall;
    status = fsg_common_set_cdev(opts->common, cdev, can_stall);
    if (status)
        goto fail_set_cdev;

    //usb_composite_overwrite_options(cdev, &coverwrite);
    DBG(DRIVER_DESC ", version: " DRIVER_VERSION "\n");
    return 0;

fail_set_cdev:
    fsg_common_free_buffers(opts->common);
    usb_put_function_instance(fi_msg);
    return status;
}

int NakedUnbind(struct usb_composite_dev *cdev)
{
    usb_put_function_instance(fi_msg);

    return 0;
}

int NakedInit(void)
{
    return mass_storage_mod_init();
}

int NakedDone(void)
{
    return mass_storage_mod_done();
}

/*  ---------------   api   ------------------- */

/*
 * 使用注意：当前接口会阻塞(除了由于错误原因返回)，直至 host 端发送数据
 */
int UsbslaveRead(void *buff, unsigned int len)
{
    struct fsg_common    *common = g_fsg_common;

    if (common->state == FSG_STATE_TERMINATED)
        return -1;

    if (!common->running)
        return -1;

    return fsg_read(buff, len);
}

int UsbslaveReadNoblock(void *buff, unsigned int len)
{
    struct fsg_common    *common = g_fsg_common;

    if (common->state == FSG_STATE_TERMINATED)
        return -1;

    if (!common->running)
        return -1;

    return fsg_read_noblock(buff, len);
}

/*
 * 使用注意：当前接口会阻塞(除了由于错误原因返回)，直至 host 端准备接收该数据
 */
int UsbslaveWrite(void *buff, unsigned int len)
{
    struct fsg_common    *common = g_fsg_common;

    if (common->state == FSG_STATE_TERMINATED)
        return -1;

    if (!common->running)
        return -1;

    return fsg_write(buff, len);
}

#define USBSLAVE_DMA_BUFF_SIZE 512
static u8 usbslave_dma_buff1[USBSLAVE_DMA_BUFF_SIZE] __attribute__((aligned(4)));
static u8 usbslave_dma_buff2[USBSLAVE_DMA_BUFF_SIZE] __attribute__((aligned(4)));

/*
 * 配置用于 dma 的 buff 地址和 size, 这个地址必须是可以用来做 dma 的地址，要求至少 4 字节对齐
 */
int UsbslaveInit(void)
{
    struct fsg_buffhd *bh;
    //struct fsg_common *common = g_fsg_common;

    fsg_buff_len  = USBSLAVE_DMA_BUFF_SIZE;

    g_buffhd_buf0 = usbslave_dma_buff1;
    g_buffhd_buf1 = usbslave_dma_buff2;

    bh = g_buffhds;
    bh->inreq->buf = bh->outreq->buf = bh->buf = g_buffhd_buf0;
    ++bh;
    bh->inreq->buf = bh->outreq->buf = bh->buf = g_buffhd_buf1;

    //usb_gadget_connect(common->gadget);

    return 0;
}

/*
 * 清除之前关于 fsg buff 的配置
 */
int UsbslaveDestroy(void)
{
    struct fsg_buffhd *bh;
    //struct fsg_common *common = g_fsg_common;

    fsg_buff_len = 0;
    g_buffhd_buf0 = NULL;
    g_buffhd_buf1 = NULL;

    bh = g_buffhds;
    bh->inreq->buf = bh->outreq->buf = bh->buf = g_buffhd_buf0;
    ++bh;
    bh->inreq->buf = bh->outreq->buf = bh->buf = g_buffhd_buf1;

    //usb_gadget_disconnect(common->gadget);

    return 0;
}

int UsbslaveEnterDownloadMode(void)
{
    struct fsg_common    *common = g_fsg_common;
    unsigned int crc;

    while (1) {

        if (!common->running) {
            udelay(10);
            continue;
        }

        DBG ("[priv msg] : go to get cmd\n");

        if (get_next_command(common))
            continue;

        DBG ("[priv msg] : get cmd\n");

        if (do_priv_command(common)) {
            printf ("%s, handle cmd failed\n", __func__);
            continue;
        }

        DBG ("[priv msg] : handle cmd fin\n");

        if (send_status(common)) {
            printf ("%s, send status failed\n", __func__);
            continue;
        }

        DBG ("[priv msg] : send status fin\n");

        if (do_crc)
        {
            crc = CRC32((unsigned char *)(CONFIG_STAGE1_DRAM_BASE), STAGE1_SIZE - 4);
            if(crc == *(unsigned int *)(CONFIG_STAGE1_DRAM_BASE + STAGE1_SIZE - 4))
                sram_crc_result = 1;
            else
                sram_crc_result = -1;

            do_crc = 0;
        }

        if (do_return)
            break;
    }

    return 0;
}
