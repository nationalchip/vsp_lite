/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * u_serial.c - utilities for USB gadget "serial port"/TTY support
 *
 */

#include <stdio.h>
#include <common.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include <driver/delay.h>
#include <driver/irq.h>
#include <driver/usb_gadget.h>

#include "u_serial.h"

/*
 * This component encapsulates the TTY layer glue needed to provide basic
 * "serial port" functionality through the USB gadget stack.  Each such
 * port is exposed through a /dev/ttyGS* node.
 *
 * After this module has been loaded, the individual TTY port can be requested
 * (gserial_alloc_line()) and it will stay available until they are removed
 * (gserial_free_line()). Each one may be connected to a USB function
 * (gserial_connect), or disconnected (with gserial_disconnect) when the USB
 * host issues a config change event. Data can only flow when the port is
 * connected to the host.
 *
 * A given TTY port can be made available in multiple configurations.
 * For example, each one might expose a ttyGS0 node which provides a
 * login application.  In one case that might use CDC ACM interface 0,
 * while another configuration might use interface 3 for that.  The
 * work to handle that (including descriptor management) is not part
 * of this component.
 *
 * Configurations may expose more than one TTY port.  For example, if
 * ttyGS0 provides login service, then ttyGS1 might provide dialer access
 * for a telephone or fax link.  And ttyGS2 might be something that just
 * needs a simple byte stream interface for some messaging protocol that
 * is managed in userspace ... OBEX, PTP, and MTP have been mentioned.
 *
 *
 * gserial is the lifecycle interface, used by USB functions
 * gs_port is the I/O nexus, used by the tty driver
 * tty_struct links to the tty/filesystem framework
 *
 * gserial <---> gs_port ... links will be null when the USB link is
 * inactive; managed by gserial_{connect,disconnect}().  each gserial
 * instance can wrap its own USB control protocol.
 *    gserial->ioport == usb_ep->driver_data ... gs_port
 *    gs_port->port_usb ... gserial
 *
 * gs_port <---> tty_struct ... links will be null when the TTY file
 * isn't opened; managed by gs_open()/gs_close()
 *    gserial->port_tty ... tty_struct
 *    tty_struct->driver_data ... gserial
 */

/* RX and TX queues can buffer QUEUE_SIZE packets before they hit the
 * next layer of buffering.  For TX that's a circular buffer; for RX
 * consider it a NOP.  A third layer is provided by the TTY code.
 */

enum req_state {
    WRITE_STATE_NONE = 0,
    WRITE_STATE_BUSY,
    WRITE_STATE_FINISH
};

/* circular buffer */
struct gs_buf {
    unsigned        buf_size;
    char            *buf_buf;
    char            *buf_get;
    char            *buf_put;
};

/*
 * The port structure holds info for each port, one for each minor number
 * (and thus for each /dev/ node).
 */
struct gs_port {
    struct gserial     *port_usb;

    bool                openclose;    /* open/close in progress */
    u8                  port_num;

    struct list_head    read_pool;
    int                 read_started;
    int                 read_allocated;
    unsigned            n_read;

    struct list_head    write_pool;
    int                 write_started;
    int                 write_allocated;
    struct gs_buf       port_write_buf;
    struct gs_buf       port_read_buf;

    /* REVISIT this state ... */
    struct usb_cdc_line_coding port_line_coding;    /* 8-N-1 etc */

    // add by douzhk
    int                 count;
    enum req_state      write_state;
};

struct gs_port gport;

#define GS_CLOSE_TIMEOUT       15        /* seconds */
#define QUEUE_SIZE              1
#define WRITE_BUF_SIZE       2048        /* TX only */
#define READ_BUF_SIZE        2048        /* RX only */
static unsigned char port_wr_buf[WRITE_BUF_SIZE];
static unsigned char port_rd_buf[READ_BUF_SIZE];

#define EP_MAX_PACKET_SIZE   512
static unsigned char read_buf[EP_MAX_PACKET_SIZE];
static unsigned char write_buf[EP_MAX_PACKET_SIZE];
/*-------------------------------------------------------------------------*/

/* Circular Buffer */

/*
 * gs_buf_alloc
 *
 * Allocate a circular buffer and all associated memory.
 */
static int gs_buf_alloc(struct gs_buf *gb, void *buf, unsigned size)
{
    if (!buf)
        return -1;

    gb->buf_buf = (char *)buf;
    gb->buf_size = size;
    gb->buf_put = gb->buf_buf;
    gb->buf_get = gb->buf_buf;

    pr_debug ("gb : %p buf : %p, size : %d\n", gb, buf, size);

    return 0;
}

/*
 * gs_buf_free
 *
 * Free the buffer and all associated memory.
 */
static void gs_buf_free(struct gs_buf *gb)
{
    gb->buf_buf  = NULL;
    gb->buf_size = 0;
}

/*
 * gs_buf_clear
 *
 * Clear out all data in the circular buffer.
 */
static void gs_buf_clear(struct gs_buf *gb)
{
    gb->buf_get = gb->buf_put;
    /* equivalent to a get of all data available */
}

/*
 * gs_buf_data_avail
 *
 * Return the number of bytes of data written into the circular
 * buffer.
 */
static unsigned gs_buf_data_avail(struct gs_buf *gb)
{
    return (gb->buf_size + gb->buf_put - gb->buf_get) % gb->buf_size;
}

/*
 * gs_buf_space_avail
 *
 * Return the number of bytes of space available in the circular
 * buffer.
 */
static unsigned gs_buf_space_avail(struct gs_buf *gb)
{
    return (gb->buf_size + gb->buf_get - gb->buf_put - 1) % gb->buf_size;
}

/*
 * gs_buf_put
 *
 * Copy data data from a user buffer and put it into the circular buffer.
 * Restrict to the amount of space available.
 *
 * Return the number of bytes copied.
 */
static unsigned gs_buf_put(struct gs_buf *gb, const unsigned char *buf, unsigned count)
{
    unsigned len;

    len  = gs_buf_space_avail(gb);
    if (count > len)
        count = len;

    if (count == 0)
        return 0;

    len = gb->buf_buf + gb->buf_size - gb->buf_put;
    if (count > len) {
        memcpy(gb->buf_put, buf, len);
        memcpy(gb->buf_buf, buf+len, count - len);
        gb->buf_put = gb->buf_buf + count - len;
    } else {
        memcpy(gb->buf_put, buf, count);
        if (count < len)
            gb->buf_put += count;
        else /* count == len */
            gb->buf_put = gb->buf_buf;
    }

    return count;
}

/*
 * gs_buf_get
 *
 * Get data from the circular buffer and copy to the given buffer.
 * Restrict to the amount of data available.
 *
 * Return the number of bytes copied.
 */
static unsigned gs_buf_get(struct gs_buf *gb, char *buf, unsigned count)
{
    unsigned len;

    len = gs_buf_data_avail(gb);
    if (count > len)
        count = len;

    if (count == 0)
        return 0;

    len = gb->buf_buf + gb->buf_size - gb->buf_get;
    if (count > len) {
        memcpy(buf, gb->buf_get, len);
        memcpy(buf+len, gb->buf_buf, count - len);
        gb->buf_get = gb->buf_buf + count - len;
    } else {
        memcpy(buf, gb->buf_get, count);
        if (count < len)
            gb->buf_get += count;
        else /* count == len */
            gb->buf_get = gb->buf_buf;
    }

    return count;
}

/*-------------------------------------------------------------------------*/

/* I/O glue between TTY (upper) and USB function (lower) driver layers */

/*
 * gs_alloc_req
 *
 * Allocate a usb_request and its buffer.  Returns a pointer to the
 * usb_request or NULL if there is an error.
 */
struct usb_request *gs_alloc_req(struct usb_ep *ep, unsigned len, void *buf)
{
    struct usb_request *req;

    if (!buf)
        return NULL;

    req = usb_ep_alloc_request(ep);
    if (req != NULL) {
        req->length = len;
        req->buf = buf;
    }

    return req;
}

/*
 * gs_free_req
 *
 * Free a usb_request and its buffer.
 */
void gs_free_req(struct usb_ep *ep, struct usb_request *req)
{
    usb_ep_free_request(ep, req);
}

/*
 * gs_send_packet
 *
 * If there is data to send, a packet is built in the given
 * buffer and the size is returned.  If there is no data to
 * send, 0 is returned.
 *
 * Called with port_lock held.
 */
static unsigned gs_send_packet(struct gs_port *port, char *packet, unsigned size)
{
    unsigned len;

    len = gs_buf_data_avail(&port->port_write_buf);
    if (len < size)
        size = len;
    if (size != 0)
        size = gs_buf_get(&port->port_write_buf, packet, size);
    return size;
}

unsigned gs_read_packet(struct gs_port *port, char *packet, unsigned size)
{
    unsigned len;

    len = gs_buf_data_avail(&port->port_read_buf);
    if (len < size)
        size = len;
    if (size != 0)
        size = gs_buf_get(&port->port_read_buf, packet, size);
    return size;
}

/*
 * gs_start_tx
 *
 * This function finds available write requests, calls
 * gs_send_packet to fill these packets with data, and
 * continues until either there are no more write requests
 * available or no more data to send.  This function is
 * run whenever data arrives or write requests are available.
 *
 * Context: caller owns port_lock; port_usb is non-null.
 */
static int gs_start_tx(struct gs_port *port)
{
    struct list_head    *pool = &port->write_pool;
    struct usb_ep        *in = port->port_usb->in;
    int            status = 0;

    struct usb_request    *req;
    int            len;

    /* abort immediately after disconnect */
    if (!port->port_usb)
        return -1;

    if (port->write_state == WRITE_STATE_BUSY)
        return 0;

    req = list_entry(pool->next, struct usb_request, list);
    if (!req) {
        pr_debug("%s, req is NULL\n", __func__);
        return -1;
    }

    len = gs_send_packet(port, req->buf, in->maxpacket);
    if (len == 0)
        return 0;

    port->write_state = WRITE_STATE_BUSY;
    req->length = len;
    req->zero = (gs_buf_data_avail(&port->port_write_buf) == 0);
    status = usb_ep_queue(in, req);
    if (status) {
        pr_debug("%s: %s %s err %d\n", __func__, "queue", in->name, status);
        return status;
    }

    return status;
}

/*
 * Context: caller owns port_lock, and port_usb is set
 */
static unsigned gs_start_rx(struct gs_port *port)
{
    struct list_head    *pool = &port->read_pool;
    struct usb_ep        *out = port->port_usb->out;
    struct usb_request    *req;
    int            status;

    req = list_entry(pool->next, struct usb_request, list);
    req->length = out->maxpacket;
    status = usb_ep_queue(out, req);
    if (status) {
        pr_debug ("[%s] usb ep queue failed, ret:%d\n", __func__, status);
        return -1;
    }

    return 0;
}

/*
 * RX tasklet takes data out of the RX queue and hands it up to the TTY
 * layer until it refuses to take any more data (or is throttled back).
 * Then it issues reads for any further data.
 *
 * If the RX queue becomes full enough that no usb_request is queued,
 * the OUT endpoint may begin NAKing as soon as its FIFO fills up.
 * So QUEUE_SIZE packets plus however many the FIFO holds (usually two)
 * can be buffered before the TTY layer's buffers (currently 64 KB).
 */
static void gs_rx_push(struct gs_port *port)
{
    struct list_head    *queue = &port->read_pool;
    bool            disconnect = false;

    struct usb_request    *req;
    req = list_entry(queue->next, struct usb_request, list);

    switch (req->status) {
    case -ESHUTDOWN:
        disconnect = true;
        pr_vdebug("ttyGS%d: shutdown\n", port->port_num);
        break;

    default:
        /* presumably a transient fault */
        pr_warn("ttyGS%d: unexpected RX status %d\n", port->port_num, req->status);
        /* FALLTHROUGH */
    case 0:
        /* normal completion */
        break;
    }

    /* push data to (open) tty */
    if (req->actual) {
        unsigned int count;

        count = gs_buf_put(&port->port_read_buf, req->buf, req->actual);
        if (req->actual > count) {
            ; // some data is droped
        }
    }

    /* If we're still connected, refill the USB RX queue. */
    if (!disconnect && port->port_usb)
        gs_start_rx(port);
}

static void gs_read_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct gs_port    *port = ep->driver_data;

    pr_debug ("%s ep : %s, len : %d, buf : %s, pool : %p next : %p\n", __func__, ep->name, req->length, req->buf, pool, pool->next);

    gs_rx_push(port);
}

static void gs_write_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct gs_port    *port = ep->driver_data;

    port->write_state = WRITE_STATE_FINISH;

    switch (req->status) {
    default:
        /* presumably a transient fault */
        pr_warning("%s: unexpected %s status %d\n", __func__, ep->name, req->status);
        /* FALL THROUGH */
    case 0:
        /* normal completion */
        if (gs_buf_data_avail(&port->port_write_buf))
            gs_start_tx(port);
        break;

    case -ESHUTDOWN:
        /* disconnect */
        pr_vdebug("%s: %s shutdown\n", __func__, ep->name);
        break;
    }
}

static void gs_free_requests(struct usb_ep *ep, struct list_head *head,
        int *allocated)
{
    struct usb_request    *req;

    while (!list_empty(head)) {
        req = list_entry(head->next, struct usb_request, list);
        list_del(&req->list);
        gs_free_req(ep, req);
        if (allocated)
            (*allocated)--;
    }
}

static int gs_alloc_requests(struct usb_ep *ep, struct list_head *head,
        void (*fn)(struct usb_ep *, struct usb_request *),
        int *allocated, void *buf)
{
    int            i;
    struct usb_request    *req;
    int n = allocated ? QUEUE_SIZE - *allocated : QUEUE_SIZE;

    /* Pre-allocate up to QUEUE_SIZE transfers, but if we can't
     * do quite that many this time, don't fail ... we just won't
     * be as speedy as we might otherwise be.
     */
    for (i = 0; i < n; i++) {
        req = gs_alloc_req(ep, ep->maxpacket, buf);
        if (!req)
            return list_empty(head) ? -ENOMEM : 0;
        req->complete = fn;
        list_add_tail(&req->list, head);
        if (allocated)
            (*allocated)++;
    }
    return 0;
}

/**
 * gs_start_io - start USB I/O streams
 * @dev: encapsulates endpoints to use
 * Context: holding port_lock; port_tty and port_usb are non-null
 *
 * We only start I/O when something is connected to both sides of
 * this port.  If nothing is listening on the host side, we may
 * be pointlessly filling up our TX buffers and FIFO.
 */
static int gs_start_io(struct gs_port *port)
{
    struct list_head    *head = &port->read_pool;
    struct usb_ep        *ep = port->port_usb->out;
    int            status;
    unsigned        started;

    /* Allocate RX and TX I/O buffers.  We can't easily do this much
     * earlier (with GFP_KERNEL) because the requests are coupled to
     * endpoints, as are the packet sizes we'll be using.  Different
     * configurations may use different endpoints with a given port;
     * and high speed vs full speed changes packet sizes too.
     */
    status = gs_alloc_requests(ep, head, gs_read_complete,
            &port->read_allocated, read_buf);
    if (status)
        return status;

    status = gs_alloc_requests(port->port_usb->in, &port->write_pool,
            gs_write_complete, &port->write_allocated, write_buf);
    if (status) {
        gs_free_requests(ep, head, &port->read_allocated);
        return status;
    }

    /* queue read requests */
    port->n_read = 0;
    started = gs_start_rx(port);
    if (started) {
        gs_free_requests(ep, head, &port->read_allocated);
        gs_free_requests(port->port_usb->in, &port->write_pool, &port->write_allocated);
        status = -EIO;
    }

    return status;
}

/*-------------------------------------------------------------------------*/

/* TTY Driver */

/*
 * gs_open sets up the link between a gs_port and its associated TTY.
 * That link is broken *only* by TTY close(), and all driver methods
 * know that.
 */
int gs_open(void)
{
    struct gs_port    *port = &gport;
    int        status;

    do {
        if (!port)
            status = -ENODEV;
        else {
            /* already open?  Great. */
            if (port->count) {
                status = 0;
                port->count++;

                /* currently opening/closing? wait ... */
            } else if (port->openclose) {
                status = -EBUSY;

                /* ... else we do the work */
            } else {
                status = -EAGAIN;
                port->openclose = true;
            }
        }

        switch (status) {
        default:
            /* fully handled */
            return status;
        case -EAGAIN:
            /* must do the work */
            break;
        case -EBUSY:
            /* wait for EAGAIN task to finish */
            mdelay(1);
            /* REVISIT could have a waitchannel here, if
             * concurrent open performance is important
             */
            break;
        }
    } while (status != -EAGAIN);

    /* allocate circular buffer on first open */
    if (port->port_write_buf.buf_buf == NULL) {
        status = gs_buf_alloc(&port->port_write_buf, port_wr_buf, WRITE_BUF_SIZE);
        if (status) {
            pr_debug("gs_open: ttyGS%d no buffer\n", port->port_num);
            port->openclose = false;
            goto exit_unlock_port;
        }
    }

    if (port->port_read_buf.buf_buf == NULL) {
        status = gs_buf_alloc(&port->port_read_buf, port_rd_buf, READ_BUF_SIZE);
        if (status) {
            pr_debug("gs_open: ttyGS%d no buffer\n", port->port_num);
            gs_buf_free(&port->port_write_buf);
            port->openclose = false;
            goto exit_unlock_port;
        }
    }

    port->count = 1;
    port->openclose = false;

    /* if connected, start the I/O stream */
    if (port->port_usb) {
        struct gserial    *gser = port->port_usb;

        pr_debug("gs_open: start ttyGS%d\n", port->port_num);
        gs_start_io(port);

        if (gser->connect)
            gser->connect(gser);
    }

    pr_debug("gs_open: ttyGS%d\n", port->port_num);

    status = 0;

exit_unlock_port:
    return status;
}

#if 0
static int gs_writes_finished(struct gs_port *p)
{
    int cond;

    /* return true on disconnect or empty buffer */
    cond = (p->port_usb == NULL) || !gs_buf_data_avail(&p->port_write_buf);

    return cond;
}
#endif

void gs_close(void)
{
    struct gs_port *port = &gport;
    struct gserial    *gser;

    if (port->count != 1) {
        if (port->count == 0) {
            pr_debug ("Warn on this %s %d\n", __func__, __LINE__);
            //WARN_ON(1);
        } else
            --port->count;
        return ;
    }

    pr_debug("gs_close: ttyGS%d ...\n", port->port_num);

    /* mark port as closing but in use; we can drop port lock
     * and sleep if necessary
     */
    port->openclose = true;
    port->count = 0;

    gser = port->port_usb;
    if (gser && gser->disconnect)
        gser->disconnect(gser);

    /* wait for circular write buffer to drain, disconnect, or at
     * most GS_CLOSE_TIMEOUT seconds; then discard the rest
     */
    if (gs_buf_data_avail(&port->port_write_buf) > 0 && gser) {
        gser = port->port_usb;
    }

    /* Iff we're disconnected, there can be no I/O in flight so it's
     * ok to free the circular buffer; else just scrub it.  And don't
     * let the push tasklet fire again until we're re-opened.
     */
    if (gser == NULL) {
        gs_buf_free(&port->port_write_buf);
        gs_buf_free(&port->port_read_buf);
    } else {
        gs_buf_clear(&port->port_write_buf);
        gs_buf_clear(&port->port_read_buf);
    }

    port->openclose = false;

    pr_debug("gs_close: ttyGS%d done!\n", port->port_num);
}

int acm_write( const unsigned char *buf, int count)
{
    struct gs_port    *port = &gport;

    pr_vdebug("gs_write: ttyGS%d writing %d bytes\n", port->port_num, count);

    if (count) {
        gx_disable_irq();
        count = gs_buf_put(&port->port_write_buf, buf, count);
        gx_enable_irq();
    }
    /* treat count == 0 as flush_chars() */
    if (port->port_usb)
        gs_start_tx(port);

    return count;
}

int acm_put_char(unsigned char ch)
{
    struct gs_port    *port = &gport;
    int        status;

    status = gs_buf_put(&port->port_write_buf, &ch, 1);

    return status;
}

#if 0
static void gs_flush_chars(struct tty_struct *tty)
{
    struct gs_port    *port = tty->driver_data;
    unsigned long    flags;

    pr_vdebug("gs_flush_chars: (%d,%p)\n", port->port_num, tty);

    if (port->port_usb)
        gs_start_tx(port);
}

static int gs_write_room(struct tty_struct *tty)
{
    struct gs_port    *port = tty->driver_data;
    unsigned long    flags;
    int        room = 0;

    if (port->port_usb)
        room = gs_buf_space_avail(&port->port_write_buf);

    pr_vdebug("gs_write_room: (%d,%p) room=%d\n",
            port->port_num, tty, room);

    return room;
}

static int gs_chars_in_buffer(struct tty_struct *tty)
{
    struct gs_port    *port = tty->driver_data;
    unsigned long    flags;
    int        chars = 0;

    chars = gs_buf_data_avail(&port->port_write_buf);

    pr_vdebug("gs_chars_in_buffer: (%d,%p) chars=%d\n",
            port->port_num, tty, chars);

    return chars;
}

/* undo side effects of setting TTY_THROTTLED */
static void gs_unthrottle(struct tty_struct *tty)
{
    struct gs_port        *port = tty->driver_data;
    unsigned long        flags;

    if (port->port_usb) {
        /* Kickstart read queue processing.  We don't do xon/xoff,
         * rts/cts, or other handshaking with the host, but if the
         * read queue backs up enough we'll be NAKing OUT packets.
         */
        pr_vdebug("ttyGS%d: unthrottle\n", port->port_num);
    }
}

static int gs_break_ctl(struct tty_struct *tty, int duration)
{
    struct gs_port    *port = tty->driver_data;
    int        status = 0;
    struct gserial    *gser;

    pr_vdebug("gs_break_ctl: ttyGS%d, send break (%d) \n",
            port->port_num, duration);

    gser = port->port_usb;
    if (gser && gser->send_break)
        status = gser->send_break(gser, duration);

    return status;
}

static const struct tty_operations gs_tty_ops = {
    .open =            gs_open,
    .close =        gs_close,
    .write =        gs_write,
    .put_char =        gs_put_char,
    .flush_chars =        gs_flush_chars,
    .write_room =        gs_write_room,
    .chars_in_buffer =    gs_chars_in_buffer,
    .unthrottle =        gs_unthrottle,
    .break_ctl =        gs_break_ctl,
};
#endif

/*-------------------------------------------------------------------------*/

static int gs_port_alloc(void)
{
    struct gs_port    *port;
    int        ret = 0;

    port = &gport;

    INIT_LIST_HEAD(&port->read_pool);
    INIT_LIST_HEAD(&port->write_pool);

    port->port_num = 0;

    return ret;
}

static void gserial_free_port(struct gs_port *port)
{
    if (port->port_usb != NULL)
        pr_err ("%s error, port has not enabled\n", __func__);
}

void gserial_free_line(unsigned char port_num)
{
    struct gs_port    *port = &gport;

    gserial_free_port(port);
}

int gserial_alloc_line(unsigned char *line_num)
{
    int                ret;
    int                port_num = 0;

    ret = gs_port_alloc();
    if (ret)
        return ret;

    *line_num = port_num;
    return ret;
}

/**
 * gserial_connect - notify TTY I/O glue that USB link is active
 * @gser: the function, set up with endpoints and descriptors
 * @port_num: which port is active
 * Context: any (usually from irq)
 *
 * This is called activate endpoints and let the TTY layer know that
 * the connection is active ... not unlike "carrier detect".  It won't
 * necessarily start I/O queues; unless the TTY is held open by any
 * task, there would be no point.  However, the endpoints will be
 * activated so the USB host can perform I/O, subject to basic USB
 * hardware flow control.
 *
 * Caller needs to have set up the endpoints and USB function in @dev
 * before calling this, as well as the appropriate (speed-specific)
 * endpoint descriptors, and also have allocate @port_num by calling
 * @gserial_alloc_line().
 *
 * Returns negative errno or zero.
 * On success, ep->driver_data will be overwritten.
 */
int gserial_connect(struct gserial *gser, u8 port_num)
{
    struct gs_port    *port;
    int        status;

    if (port_num >= MAX_U_SERIAL_PORTS)
        return -ENXIO;

    port = &gport;
    if (port->port_usb) {
        pr_err("serial line %d is in use.\n", port_num);
        return -EBUSY;
    }

    /* activate the endpoints */
    status = usb_ep_enable(gser->in);
    if (status < 0)
        return status;
    gser->in->driver_data = port;

    status = usb_ep_enable(gser->out);
    if (status < 0)
        goto fail_out;
    gser->out->driver_data = port;

    /* then tell the tty glue that I/O can work */
    gser->ioport = port;
    port->port_usb = gser;
    gser->port_line_coding = port->port_line_coding;

    /* if it's already open, start I/O ... and notify the serial
     * protocol about open/close status (connect/disconnect).
     */
    if (port->count) {
        pr_debug("gserial_connect: start ttyGS%d\n", port->port_num);
        gs_start_io(port);
        if (gser->connect)
            gser->connect(gser);
    } else {
        if (gser->disconnect)
            gser->disconnect(gser);
    }

    return status;

fail_out:
    usb_ep_disable(gser->in);
    return status;
}

/**
 * gserial_disconnect - notify TTY I/O glue that USB link is inactive
 * @gser: the function, on which gserial_connect() was called
 * Context: any (usually from irq)
 *
 * This is called to deactivate endpoints and let the TTY layer know
 * that the connection went inactive ... not unlike "hangup".
 *
 * On return, the state is as if gserial_connect() had never been called;
 * there is no active USB I/O on these endpoints.
 */
void gserial_disconnect(struct gserial *gser)
{
    struct gs_port    *port = gser->ioport;

    if (!port)
        return;

    /* tell the TTY glue not to do I/O here any more */

    /* REVISIT as above: how best to track this? */
    //port->port_line_coding = gser->port_line_coding;

    port->port_usb = NULL;
    gser->ioport = NULL;
    if (port->count > 0 || port->openclose) {
#if 0
        wake_up_interruptible(&port->drain_wait);
        if (port->port.tty)
            tty_hangup(port->port.tty);
#endif
    }

    /* disable endpoints, aborting down any active I/O */
    usb_ep_disable(gser->out);
    usb_ep_disable(gser->in);

    /* finally, free any unused/unusable I/O buffers */
    if (port->count == 0 && !port->openclose) {
        gs_buf_free(&port->port_write_buf);
        gs_buf_free(&port->port_read_buf);
    }
    gs_free_requests(gser->out, &port->read_pool, NULL);
    gs_free_requests(gser->in, &port->write_pool, NULL);

    port->read_allocated = port->read_started =
        port->write_allocated = port->write_started = 0;
}

// -----------------------  API  -----------------------

int GsOpen(void)
{
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    gs_open();

    return (int)&gport;
#else
    return 0;
#endif
}

void GsClose(void)
{
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    gs_close();
#else
    return ;
#endif
}

int GsInfo(GS_INFO *info)
{
    struct gs_port    *port = &gport;

    if (!info)
        return -1;

    /*
     * 不使用关闭中断的方式读取 buf 的长度信息，因为当前接口可能会被频繁调用，且如下的两个函数内部均使用了取余的函数，
     * 可能会比较费时，会影响中断的处理。当前这种不关闭中断对 fifo 信息获取影响如下：
     * 1. 对于发送，可能导致返回的可用空间比实际可用空间小，这个没问题
     * 2. 对于接收，可能导致返回的可用数据比实际可用数据小，这个也没问题
     */
    info->write_fifo.total_len = port->port_write_buf.buf_size;
    info->write_fifo.used_len  = gs_buf_data_avail(&port->port_write_buf);
    info->write_fifo.free_len  = gs_buf_space_avail(&port->port_write_buf);

    info->read_fifo.total_len = port->port_read_buf.buf_size;
    info->read_fifo.used_len  = gs_buf_data_avail(&port->port_read_buf);
    info->read_fifo.free_len  = gs_buf_space_avail(&port->port_read_buf);

    return 0;
}

int GsWrite(const char *buf, int count)
{
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    return acm_write((const unsigned char *)buf, count);
#else
    return 0;
#endif
}

int GsRead(char *buf, int count)
{
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    struct gs_port *port = &gport;

    if (count) {
        gx_disable_irq();
        count = gs_read_packet(port, buf, count);
        gx_enable_irq();
    }

    return count;
#else
    return 0;
#endif
}
