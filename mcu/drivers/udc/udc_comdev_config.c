/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * udc_comdev_config.c: usb slave composite device for uac
 *
 */

#include "include/ch9.h"
#include "include/gadget.h"
#include "include/usb.h"
#include "include/composite.h"
#include <driver/delay.h>
#include "include/audio.h"
#include "include/audio-v2.h"

#include <driver/usb_gadget.h>
#include <driver/udc.h>

#include "include/u_uac2.h"
#include "include/uac2.h"
#include "include/naked.h"
#include "include/hid.h"
#include "include/gs.h"
#include "include/ms.h"
#include "include/usbls.h"
#include "include/uac1.h"

#define UDC_TAG "[UDC] "
#define ERR(fmt, ...) printf (UDC_TAG "ERR : " fmt, ##__VA_ARGS__)
#define DBG(fmt, ...)

#define DRIVER_DESC           "GX USB Audio Gadget"
#define DRIVER_VERSION        "AUG 28, 2017"

#define AUDIO_VENDOR_NUM       0x1d6b     /* Linux Foundation */

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
#define AUDIO_PRODUCT_NUM      0xa4a6     /* Linux-USB Audio Gadget */
#elif defined(CONFIG_MCU_UDC_ENABLE_UAC1)
#define AUDIO_PRODUCT_NUM      0xa4a7     /* Linux-USB Audio Gadget */
#else
#define AUDIO_PRODUCT_NUM      0xa4a6     /* Linux-USB Audio Gadget */
#endif

static struct usb_device_descriptor device_desc = {
    .bLength            =  sizeof device_desc,
    .bDescriptorType    =  USB_DT_DEVICE,
    .bcdUSB             =  cpu_to_le16(0x200),
    .bDeviceClass       =  USB_CLASS_MISC,
    .bDeviceSubClass    =  0x02,
    .bDeviceProtocol    =  0x01,
    .idVendor           =  cpu_to_le16(AUDIO_VENDOR_NUM),
    .idProduct          =  cpu_to_le16(AUDIO_PRODUCT_NUM),
    .bcdDevice          =  0x100,
    .bNumConfigurations =  1,
};

/* string IDs are assigned dynamically */

static struct usb_string strings_dev[] = {
    [USB_GADGET_MANUFACTURER_IDX].s =  "",
    [USB_GADGET_PRODUCT_IDX].s      =  DRIVER_DESC,
    [USB_GADGET_SERIAL_IDX].s       =  "",
    {  } /* end of list */
};

static struct usb_gadget_strings stringtab_dev = {
    .language =  0x0409,     /* en-us */
    .strings  =  strings_dev,
};

static struct usb_gadget_strings *comdev_strings[] = {
    &stringtab_dev,
    NULL,
};

static int comdev_do_config(struct usb_configuration *c)
{
    int status = 0;

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    status = Uac2DoConfig(c);
    if (status < 0) {
        ERR ("%s, uac 2.0 config failed\n", __func__);
        return status;
    }
#elif defined (CONFIG_MCU_UDC_ENABLE_UAC1)
    status = Uac1DoConfig(c);
    if (status < 0) {
        ERR ("%s, uac 1.0 config failed\n", __func__);
        return status;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_HID
    status = HidDoConfig(c);
    if (status < 0) {
        ERR ("%s, hid config failed\n", __func__);
        return status;
    }
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    status = GsDoConfig(c);
    if (status < 0) {
        ERR ("%s, gs config failed\n", __func__);
        return status;
    }
#endif
#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    status = MsgDoConfig(c);
    if (status < 0) {
        ERR ("%s, msg config failed\n", __func__);
        return status;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    status = NakedDoConfig(c);
    if (status < 0) {
        ERR ("%s, priv msg config failed\n", __func__);
        return status;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_LS
    status = UsbLSDoConfig(c);
    if (status < 0) {
        ERR ("%s, priv msg config failed\n", __func__);
        return status;
    }
#endif

    return status;
}

static struct usb_configuration comdev_main_config = {
    .label                =  DRIVER_DESC,
    .bConfigurationValue  =  1,
    /* .iConfiguration = DYNAMIC */
    .bmAttributes         =  0,
    .bMaxPower            =  50,
};

static int udc_comdev_bind(struct usb_composite_dev *cdev)
{
    int status = 0;

    status = usb_string_ids_tab(cdev, strings_dev);
    if (status < 0)
        goto fail;

    device_desc.iManufacturer = strings_dev[USB_GADGET_MANUFACTURER_IDX].id;
    device_desc.iProduct = strings_dev[USB_GADGET_PRODUCT_IDX].id;
    device_desc.iSerialNumber = strings_dev[USB_GADGET_SERIAL_IDX].id;

    DBG("%s, version: %s\n", DRIVER_DESC, DRIVER_VERSION);

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    status = Uac2Bind(cdev);
    if (status < 0) {
        ERR ("%s, uac 2.0 bind failed\n", __func__);
        goto audio_fail;
    }
#elif defined CONFIG_MCU_UDC_ENABLE_UAC1
    status = Uac1Bind(cdev);
    if (status < 0) {
        ERR ("%s, uac 1.0 bind failed\n", __func__);
        goto audio_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_HID
    status = HidBind(cdev);
    if (status < 0) {
        ERR ("%s, hid bind failed\n", __func__);
        goto hid_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_GS
    status = GsBind(cdev);
    if (status < 0) {
        ERR ("%s, gs bind failed\n", __func__);
        goto gs_fail;
    }

#endif
#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    status = MsgBind(cdev);
    if (status < 0) {
        ERR ("%s, msg bind failed\n", __func__);
        goto ms_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    status = NakedBind(cdev);
    if (status < 0) {
        ERR ("%s, naked bind failed\n", __func__);
        goto naked_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_LS
    status = UsbLSBind(cdev);
    if (status < 0) {
        ERR ("%s, naked bind failed\n", __func__);
        goto usbls_fail;
    }
#endif

    status = usb_add_config(cdev, &comdev_main_config, comdev_do_config);
    if (status < 0)
        goto fail;

    return 0;

fail:

#ifdef CONFIG_MCU_UDC_ENABLE_LS
    UsbLSUnbind(cdev);
usbls_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    NakedUnbind(cdev);
naked_fail:
#endif

#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    MsgUnbind(cdev);
ms_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_GS
    GsUnbind(cdev);
gs_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_HID
    HidUnbind(cdev);
hid_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    Uac2Unbind(cdev);
audio_fail:
#elif defined (CONFIG_MCU_UDC_ENABLE_UAC1)
    Uac1Unbind(cdev);
audio_fail:
#endif

    return status;
}

static int udc_comdev_unbind(struct usb_composite_dev *cdev)
{
#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    Uac2Unbind(cdev);
#elif defined (CONFIG_MCU_UDC_ENABLE_UAC1)
    Uac1Unbind(cdev);
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_HID
    HidUnbind(cdev);
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    GsUnbind(cdev);
#endif
#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    MsgUnbind(cdev);
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    NakedUnbind(cdev);
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_LS
    UsbLSUnbind(cdev);
#endif

    return 0;
}

static struct usb_composite_driver comdev_driver = {
    .name    =  "udc-comdev",
    .dev     =  &device_desc,
    .strings =  comdev_strings,
    .bind    =  udc_comdev_bind,
    .unbind  =  udc_comdev_unbind,
};

static int udc_comdev_init(void)
{
    return usb_composite_register(&comdev_driver);
}

static void udc_comdev_cleanup(void)
{
    usb_composite_unregister(&comdev_driver);

    return ;
}

// First configuration : uac ----------------------------------------------------------------------------------

int UsbCompositeInit(const UAC2_CHANNEL_CONFIG *audio_in_config,
        const UAC2_CHANNEL_CONFIG *audio_out_config,
        const UAC2_DEVICE_DESCRIPTION *device_description,
        const UAC2_CALLBACKS *callbacks,
        void *private_data)
{
    int ret = 0;

#ifndef CONFIG_MCU_UDC_ENABLED
    return 0;
#endif

#ifdef CONFIG_MCU_UDC_ENABLED
 #ifdef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
    ret = UdcInit(UDC_SPEED_FULL);
 #else
    ret = UdcInit(UDC_SPEED_HIGH);
 #endif
    if (ret != 0) {
        ERR ("%s, udc init failed\n", __func__);
        return ret;
    }
#endif

    strings_dev[USB_GADGET_MANUFACTURER_IDX].s = device_description->manufacturer_name;
    strings_dev[USB_GADGET_PRODUCT_IDX].s = device_description->product_name;
    strings_dev[USB_GADGET_SERIAL_IDX].s = device_description->serial_number;

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    ret = Uac2Init(audio_in_config,
            audio_out_config,
            device_description,
            callbacks,
            private_data);
    if (ret < 0) {
        ERR ("%s, uac 2.0 init failed\n", __func__);
        goto uac_fail;
    }
#elif defined (CONFIG_MCU_UDC_ENABLE_UAC1)
    ret = Uac1Init(audio_in_config,
            audio_out_config,
            device_description,
            callbacks,
            private_data);
    if (ret < 0) {
        ERR ("%s, uac 1.0 init failed\n", __func__);
        goto uac_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_HID
    ret = HidInit();
    if (ret < 0) {
        ERR ("%s, Hid init failed\n", __func__);
        goto hid_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_GS
    ret = GsInit();
    if (ret < 0) {
        ERR ("%s, Gs init failed\n", __func__);
        goto gs_fail;
    }
#endif

#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    ret = MsInit();
    if (ret < 0) {
        ERR ("%s, Ms init failed\n", __func__);
        goto ms_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    ret = NakedInit();
    if (ret < 0) {
        ERR ("%s, Private ms init failed\n", __func__);
        goto  naked_fail;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_LS
    ret = UsbLSInit();
    if (ret < 0) {
        ERR ("%s, usbls init failed\n", __func__);
        goto  usbls_fail;
    }
#endif

    ret = udc_comdev_init();
    if (ret < 0) {
        ERR ("%s, udc comdev init failed\n", __func__);
        goto init_fail;
    }

    return 0;

init_fail:

#ifdef CONFIG_MCU_UDC_ENABLE_LS
    UsbLSDone();
usbls_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    NakedDone();
naked_fail:
#endif

#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    MsDone();
ms_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_GS
    GsDone();
gs_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_HID
    HidDone();
hid_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    Uac2Done();
uac_fail:
#elif defined (CONFIG_MCU_UDC_ENABLE_UAC1)
    Uac1Done();
uac_fail:
#endif

#ifdef CONFIG_MCU_UDC_ENABLED
    UdcDone();
#endif

    return ret;
}

int UsbCompositeDone(void)
{
#ifndef CONFIG_MCU_UDC_ENABLED
    return 0;
#endif

    udc_comdev_cleanup();
#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    Uac2Done();
#elif defined (CONFIG_MCU_UDC_ENABLE_UAC1)
    Uac1Done();
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_HID
    HidDone();
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_GS
    GsDone();
#endif
#if defined(CONFIG_MCU_UDC_ENABLE_MS)
    MsDone();
#endif
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG1
    NakedDone();
#endif

#ifdef CONFIG_MCU_UDC_ENABLED
    UdcDone();
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_LS
    UsbLSDone();
#endif

    return 0;
}

// Second configuration : download -----------------------------------------------------------------------

#define DOWNLOADER_DRIVER_DESC         "NationalChip Upgrade Dedicated"
#define DOWNLOADER_DRIVER_VERSION      "AUG 28, 2017"

#define DOWNLOADER_VID      0xa700
#define DOWNLOADER_PID      0x0001

static struct usb_device_descriptor downloader_device_desc = {
    .bLength            =  sizeof downloader_device_desc,
    .bDescriptorType    =  USB_DT_DEVICE,
    .bcdUSB             =  cpu_to_le16(0x200),
    .bDeviceClass       =  USB_CLASS_PER_INTERFACE,

    .idVendor           =  cpu_to_le16(DOWNLOADER_VID),
    .idProduct          =  cpu_to_le16(DOWNLOADER_PID),
    .bNumConfigurations =  1,
};

/* string IDs are assigned dynamically */

static struct usb_string downloader_strings_dev[] = {
    [USB_GADGET_MANUFACTURER_IDX].s =  "",
    [USB_GADGET_PRODUCT_IDX].s      =  DOWNLOADER_DRIVER_DESC,
    [USB_GADGET_SERIAL_IDX].s       =  "",
    {  } /* end of list */
};

static struct usb_gadget_strings downloader_stringtab_dev = {
    .language =  0x0409,     /* en-us */
    .strings  =  downloader_strings_dev,
};

static struct usb_gadget_strings *downloader_comdev_strings[] = {
    &downloader_stringtab_dev,
    NULL,
};

static int comdev_do_downloader_config(struct usb_configuration *c)
{
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    int status = 0;

    status = NakedDoConfig(c);
    if (status < 0) {
        ERR ("%s, priv msg config failed\n", __func__);
        return status;
    }
#endif

    return 0;
}

#define DOWNLOADER_CONFIG_DESC DOWNLOADER_DRIVER_DESC
static struct usb_configuration comdev_downloader_config = {
    .label               =  DOWNLOADER_CONFIG_DESC,
    .bConfigurationValue =  1,
    /* .iConfiguration = DYNAMIC */
    .bmAttributes        =  0,
    .bMaxPower           =  50,
};

static int downloader_udc_comdev_bind(struct usb_composite_dev *cdev)
{
    int status = 0;

    status = usb_string_ids_tab(cdev, downloader_strings_dev);
    if (status < 0)
        goto fail;

    downloader_device_desc.iManufacturer = downloader_strings_dev[USB_GADGET_MANUFACTURER_IDX].id;
    downloader_device_desc.iProduct      = downloader_strings_dev[USB_GADGET_PRODUCT_IDX].id;
    downloader_device_desc.iSerialNumber = downloader_strings_dev[USB_GADGET_SERIAL_IDX].id;

    DBG("%s, version: %s\n", DOWNLOADER_DRIVER_DESC, DOWNLOADER_DRIVER_VERSION);

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    status = NakedBind(cdev);
    if (status < 0) {
        ERR ("%s, naked bind failed\n", __func__);
        goto naked_fail;
    }
#endif

    status = usb_add_config(cdev, &comdev_downloader_config, comdev_do_downloader_config);
    if (status < 0)
        goto fail;

    return 0;

fail:
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    NakedUnbind(cdev);
naked_fail:
#endif

    return status;
}

static int downloader_udc_comdev_unbind(struct usb_composite_dev *cdev)
{
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    NakedUnbind(cdev);
#endif

    return 0;
}

static struct usb_composite_driver comdev_downloader_driver = {
    .name    =  "udc-comdev-downloader",
    .dev     =  &downloader_device_desc,
    .strings =  downloader_comdev_strings,
    .bind    =  downloader_udc_comdev_bind,
    .unbind  =  downloader_udc_comdev_unbind,
};

static int downloadr_udc_comdev_init(void)
{
    return usb_composite_register(&comdev_downloader_driver);
}

static void downloader_udc_comdev_cleanup(void)
{
    usb_composite_unregister(&comdev_downloader_driver);

    return ;
}

int UsbslaveDownloaderInit(void)
{
    int ret = 0;

#ifndef CONFIG_MCU_UDC_ENABLED
    return 0;
#endif

#if 0
    downloader_strings_dev[USB_GADGET_MANUFACTURER_IDX].s = device_description->manufacturer_name;
    downloader_strings_dev[USB_GADGET_PRODUCT_IDX].s = device_description->product_name;
    downloader_strings_dev[USB_GADGET_SERIAL_IDX].s = device_description->serial_number;
#endif

#ifdef CONFIG_MCU_UDC_ENABLED
 #ifdef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
    ret = UdcInit(UDC_SPEED_FULL);
 #else
    ret = UdcInit(UDC_SPEED_HIGH);
 #endif
    if (ret != 0) {
        ERR ("%s, udc init failed\n", __func__);
        return ret;
    }
#endif

#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    ret = NakedInit();
    if (ret < 0) {
        ERR ("%s, Private ms init failed\n", __func__);
        goto naked_fail;
    }
#endif

    ret = downloadr_udc_comdev_init();
    if (ret < 0) {
        ERR ("%s, udc comdev init failed\n", __func__);
        goto init_fail;
    }

    return 0;

init_fail:
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    NakedDone();
naked_fail:
#endif
#ifdef CONFIG_MCU_UDC_ENABLED
    UdcDone();
#endif
    return ret;
}

int UsbslaveDownloaderDone(void)
{
    int ret = 0;

#ifndef CONFIG_MCU_UDC_ENABLED
    return 0;
#endif

    downloader_udc_comdev_cleanup();
#ifdef CONFIG_MCU_UDC_ENABLE_NAKED_IN_CONFIG2
    NakedDone();
#endif

#ifdef CONFIG_MCU_UDC_ENABLED
    UdcDone();
#endif

    return ret;
}
