/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * hid_gx.c: usb human interface device driver
 *
 */

#include <stdio.h>
#include <common.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include <driver/delay.h>
#include "../include/u_uac2.h"
#include "../include/hid_gx.h"

#include <driver/irq.h>

#define ERR(fmt, ...) printf (fmt, ##__VA_ARGS__)
#define DBG(fmt, ...)

#define REPORT_LENGTH 64

struct f_hidg_req_list {
    struct usb_request    *req;
    unsigned int           pos;
    struct list_head       list;
};

/* circular buffer */
struct circular_buff {
    unsigned         buf_size;
    char            *buf;
    char            *buf_get;
    char            *buf_put;
};

struct f_hidg {
    /* configuration */
    unsigned char            bInterfaceSubClass;
    unsigned char            bInterfaceProtocol;
    unsigned short           report_desc_length;
    unsigned char           *report_desc;
    unsigned short           report_length;

#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    unsigned int             qlen;
#endif
    /* send report */
    bool                     write_pending;
    struct usb_request      *req;
    struct usb_function      func;
    struct usb_ep           *in_ep;
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    struct usb_ep           *out_ep;
#endif
    bool                     is_open;
    bool                     running;

    struct circular_buff     us_cb;
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    struct circular_buff     ds_cb;
#endif
};

struct hidg_func_node {
    struct usb_function_instance *fi;
    struct usb_function *f;
    struct list_head node;
    struct hidg_func_descriptor *func;
};

static inline struct f_hidg *func_to_hidg(struct usb_function *f)
{
    return container_of(f, struct f_hidg, func);
}

/*-------------------------------------------------------------------------*/
/*                           Static descriptors                            */

static struct usb_interface_descriptor hidg_interface_desc = {
    .bLength              =  sizeof hidg_interface_desc,
    .bDescriptorType      =  USB_DT_INTERFACE,
    /* .bInterfaceNumber     = DYNAMIC */
    .bAlternateSetting    =  0,
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    .bNumEndpoints        = 2,
#else
    .bNumEndpoints        = 1,
#endif
    .bInterfaceClass      =  USB_CLASS_HID,
    /* .bInterfaceSubClass    = DYNAMIC */
    /* .bInterfaceProtocol    = DYNAMIC */
    /* .iInterface        = DYNAMIC */
};

static struct hid_descriptor hidg_desc = {
    .bLength              =  sizeof hidg_desc,
    .bDescriptorType      =  HID_DT_HID,
    .bcdHID               =  0x0111,
    .bCountryCode         =  0x00,
    .bNumDescriptors      =  0x1,
    /*.desc[0].bDescriptorType    = DYNAMIC */
    /*.desc[0].wDescriptorLenght    = DYNAMIC */
};

/* High-Speed Support */
static struct usb_endpoint_descriptor hidg_hs_in_ep_desc = {
    .bLength             =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType     =  USB_DT_ENDPOINT,
    .bEndpointAddress    =  USB_DIR_IN,
    .bmAttributes        =  USB_ENDPOINT_XFER_INT,
    /*.wMaxPacketSize    = DYNAMIC */
    .bInterval           =  4, /* FIXME: Add this field in the
                                * HID gadget configuration?
                                * (struct hidg_func_descriptor)
                                */
};

#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
static struct usb_endpoint_descriptor hidg_hs_out_ep_desc = {
    .bLength             = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType     = USB_DT_ENDPOINT,
    .bEndpointAddress    = USB_DIR_OUT,
    .bmAttributes        = USB_ENDPOINT_XFER_INT,
    /*.wMaxPacketSize    = DYNAMIC */
    .bInterval           = 4, /* FIXME: Add this field in the
                               * HID gadget configuration?
                               * (struct hidg_func_descriptor)
                               */
};
#endif

static struct usb_descriptor_header *hidg_hs_descriptors[] = {
    (struct usb_descriptor_header *)&hidg_interface_desc,
    (struct usb_descriptor_header *)&hidg_desc,
    (struct usb_descriptor_header *)&hidg_hs_in_ep_desc,
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    (struct usb_descriptor_header *)&hidg_hs_out_ep_desc,
#endif
    NULL,
};

/* Full-Speed Support */

static struct usb_endpoint_descriptor hidg_fs_in_ep_desc = {
    .bLength             =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType     =  USB_DT_ENDPOINT,
    .bEndpointAddress    =  USB_DIR_IN,
    .bmAttributes        =  USB_ENDPOINT_XFER_INT,
    /*.wMaxPacketSize    = DYNAMIC */
    .bInterval           =  10, /* FIXME: Add this field in the
                                 * HID gadget configuration?
                                 * (struct hidg_func_descriptor)
                                 */
};

#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
static struct usb_endpoint_descriptor hidg_fs_out_ep_desc = {
    .bLength        = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType    = USB_DT_ENDPOINT,
    .bEndpointAddress    = USB_DIR_OUT,
    .bmAttributes        = USB_ENDPOINT_XFER_INT,
    /*.wMaxPacketSize    = DYNAMIC */
    .bInterval        = 10, /* FIXME: Add this field in the
                             * HID gadget configuration?
                             * (struct hidg_func_descriptor)
                             */
};
#endif

static struct usb_descriptor_header *hidg_fs_descriptors[] = {
    (struct usb_descriptor_header *)&hidg_interface_desc,
    (struct usb_descriptor_header *)&hidg_desc,
    (struct usb_descriptor_header *)&hidg_fs_in_ep_desc,
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    (struct usb_descriptor_header *)&hidg_fs_out_ep_desc,
#endif
    NULL,
};

#define CT_FUNC_HID_IDX    0

static struct usb_string ct_func_string_defs[] = {
    [CT_FUNC_HID_IDX].s    = "HID Interface",
    {},            /* end of list */
};

static struct usb_gadget_strings ct_func_string_table = {
    .language   = 0x0409,    /* en-US */
    .strings    = ct_func_string_defs,
};

static struct usb_gadget_strings *ct_func_strings[] = {
    &ct_func_string_table,
    NULL,
};

#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
extern struct uac_internal_callbacks our_callback_ops;
#endif

static LIST_HEAD(hidg_func_list);

static struct hidg_func_node g_entry;
static struct f_hidg g_hidg;
static struct f_hid_opts g_hid_opts;
#define MAX_REPORT_LEN 8

static u8 g_report_rbuf[REPORT_LENGTH];

#define US_BUFF_SIZE (REPORT_LENGTH * 10)
#define DS_BUFF_SIZE (REPORT_LENGTH * 10)
static u8 us_buff[US_BUFF_SIZE];
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
static u8 ds_buff[DS_BUFF_SIZE];
#endif

#if defined (CONFIG_MCU_UDC_HID_PROTOCOL_NONE)
static struct hidg_func_descriptor our_hid_data = {
    .subclass            = 0, /* No subclass */
    .protocol            = 0, /* Keyboard */
    //.protocol        = 0, /* NULL */
    .report_length        = 64,
    .report_desc_length  = 28,
    .report_desc         = {
		0x06, 0x00, 0xFF,   /*  Usage Page (FF00h),         */
		0x09, 0x00,         /*  Usage (00h),                */
		0xA1, 0x01,         /*  Collection (Application),   */
			0x06, 0x00, 0xFF,   /*      Usage Page (FF00h),     */
			0x09, 0x00,         /*      Usage (00h),            */
			0x15, 0x00,         /*      Logical Minimum (0),    */
			0x26, 0xFF, 0x00,   /*      Logical Maximum (255),  */
			0x75, 0x08,         /*      Report Size (8),        */
			0x95, 0x40,         /*      Report Count (64),      */
			0x81, 0x02,         /*      Input (Variable),       */
			0x09, 0x00,         /*      Usage (00h),            */
			0x91, 0x02,         /*      Output (Variable),      */
		0xC0                /*  End Collection              */
    }
};
#elif defined CONFIG_MCU_UDC_HID_PROTOCOL_GENERIC_VOLUME_CONTROL
static struct hidg_func_descriptor our_hid_data = {
    .subclass            = 0, /* No subclass */
    .protocol            = 0, /* Keyboard */
    //.protocol        = 0, /* NULL */
    .report_length        = 2,
    .report_desc_length  = 29,
	.report_desc         = {
		0x05, 0x0C, /*  Usage Page (Consumer),          */
		0x09, 0x01, /*  Usage (Consumer Control),       */
		0xA1, 0x01, /*  Collection (Application),       */
			0x85, 0x01, /*      Report ID (1),              */
			0x75, 0x01, /*      Report Size (1),            */
			0x15, 0x00, /*      Logical Minimum (0),        */
			0x25, 0x01, /*      Logical Maximum (1),        */
			0x09, 0xE9, /*      Usage (Volume Inc),         */
			0x09, 0xEA, /*      Usage (Volume Dec),         */
			0x09, 0xE2, /*      Usage (Mute),               */
			0x95, 0x03, /*      Report Count (3),           */
			0x81, 0x06, /*      Input (Variable, Relative), */
			0x95, 0x05, /*      Report Count (5),           */
			0x81, 0x01, /*      Input (Constant),           */
		0xC0,           /*  End Collection                  */
	}
};
#elif defined (CONFIG_MCU_UDC_HID_PROTOCOL_HUACHUANGSHIXUN)
static struct hidg_func_descriptor our_hid_data = {
    .subclass            = 0, /* No subclass */
    .protocol            = 0, /* Keyboard */
    //.protocol        = 0, /* NULL */
    .report_length        = REPORT_LENGTH,
    .report_desc_length  = 142,
	.report_desc         = {
		0x05, 0x0C, //Usage Page (Consumer Devices)
		0x09, 0x01, //Usage (Consumer Control)
		0xA1, 0x01, //Collection (Application)
			0x85, 0x01, //Report ID (1)
			0x75, 0x01, //Report Size (1)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x01, //Logical Maximum (1)
			0x09, 0xE9, //Usage (Volume Increment)
			0x09, 0xEA, //Usage (Volume Decrement)
			0x95, 0x02, //Report Count (2)
			0x81, 0x06, //Input (Data,Var,Rel,NWrp,Lin,Pref,NNul,Bit)
			0x95, 0x06, //Report Count (6)
			0x81, 0x01, //Input (Cnst,Ary,Abs)
		0xC0, //End Collection

		0x05, 0x0B, //Usage Page (Telephony Devices)
		0x09, 0x05, //Usage (Headset)
		0xA1, 0x01, //Collection (Application)
			0x85, 0x02, //Report ID (2)
			0x95, 0x01, //Report Count (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0x20, //Usage (Hook Switch)
			0x81, 0x22, //Input (Data,Var,Abs,NWrp,Lin,NPrf,NNul,Bit)
			0x09, 0x2F, //Usage (Phone Mute)
			0x81, 0x06, //Input (Data,Var,Rel,NWrp,Lin,Pref,NNul,Bit)
			0x95, 0x06, //Report Count (6)
			0x81, 0x01, //Input (Cnst,Ary,Abs)
			0x05, 0x08, //Usage Page (LEDs)
			0x85, 0x03, //Report ID (3)
			0x95, 0x01, //Report Count (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0x18, //Usage (Ring)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x04, //Report ID (4)
			0x75, 0x01, //Report Size (1)
			0x09, 0x09, //Usage (Mute)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x05, //Report ID (5)
			0x75, 0x01, //Report Size (1)
			0x09, 0x17, //Usage (Off-Hook)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x06, //Report ID (6)
			0x75, 0x01, //Report Size (1)
			0x09, 0x20, //Usage (Hold)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
		0xC0, //End Collection

#if 1
		0x06, 0x00, 0xff, //Usage Page (Vendor Defined Page1)
		0x09, 0x01, //Usage (Vendor Usage1)
		0xa1, 0x00, //Collection(Phsical)
			0x85, 0x07, //Report ID(7)
			0x75, 0x08, //Report Size(8)
			0x95, 0x3f, //Report Count (63)
			0x26, 0xff, 0x00, //Logical Maximum (255)
			0x15, 0x00, //Logical Minimum (0)
			0x09, 0x00, //Usage (Undefined)
			0x81, 0x22, //Input (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x85, 0x08, //Report ID(8)
			0x95, 0x3f, //Report Count(63)
			0x75, 0x08, //Report Size(8)
			0x15, 0x00, //Logical Minimum (0)
			0x26, 0xff, 0x00, //Logical Maximum (255)
			0x09, 0x00, //Usage (Undefined)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
		0xc0,
#else
		0x06, 0xF1, 0xFF, //Usage Page (Vendor-Defined 242)
		0x09, 0x01, //Usage (Vendor-Defined 1)
		0xA1, 0x01, //Collection (Application)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x01, //Logical Maximum (1)
			0x85, 0x12, //Report ID (18)
			0x95, 0x01, //Report Count (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0xC1, //Usage (Vendor-Defined 193)
			0x81, 0x22, //Input (Data,Var,Abs,NWrp,Lin,NPrf,NNul,Bit)
			0x09, 0xC0, //Usage (Vendor-Defined 192)
			0x81, 0x22, //Input (Data,Var,Abs,NWrp,Lin,NPrf,NNul,Bit)
			0x75, 0x06, //Report Size (6)
			0x81, 0x01, //Input (Cnst,Ary,Abs)
			0x85, 0x13, //Report ID (19)
			0x95, 0x01, //Report Count (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0xC6, //Usage (Vendor-Defined 198)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x14, //Report ID (20)
			0x75, 0x01, //Report Size (1)
			0x09, 0xC0, //Usage (Vendor-Defined 192)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x15, //Report ID (21)
			0x75, 0x01, //Report Size (1)
			0x09, 0xC1, //Usage (Vendor-Defined 193)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x16, //Report ID (22)
			0x75, 0x01, //Report Size (1)
			0x09, 0xE0, //Usage (Vendor-Defined 224)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x17, //Report ID (23)
			0x75, 0x01, //Report Size (1)
			0x09, 0xC7, //Usage (Vendor-Defined 199)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0x91, 0x01, //Output (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x7F, //Logical Maximum (127)
			0x95, 0x01, //Report Count (1)
			0x85, 0x20, //Report ID (32)
			0x09, 0xA0, //Usage (Vendor-Defined 160)
			0x75, 0x08, //Report Size (8)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x85, 0x21, //Report ID (33)
			0x15, 0x80, //Logical Minimum (-128)
			0x25, 0x7F, //Logical Maximum (127)
			0x75, 0x08, //Report Size (8)
			0x95, 0x3E, //Report Count (62)
			0x09, 0xE6, //Usage (Vendor-Defined 230)
			0x81, 0x22, //Input (Data,Var,Abs,NWrp,Lin,NPrf,NNul,Bit)
			0x85, 0x22, //Report ID (34)
			0x15, 0x80, //Logical Minimum (-128)
			0x25, 0x7F, //Logical Maximum (127)
			0x75, 0x08, //Report Size (8)
			0x95, 0x3E, //Report Count (62)
			0x09, 0xE6, //Usage (Vendor-Defined 230)
			0x91, 0x22, //Output (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x85, 0x10, //Report ID (16)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x03, //Logical Maximum (3)
			0x95, 0x01, //Report Count (1)
			0x75, 0x02, //Report Size (2)
			0x09, 0xC2, //Usage (Vendor-Defined 194)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x09, 0xC3, //Usage (Vendor-Defined 195)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x09, 0xC4, //Usage (Vendor-Defined 196)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x09, 0xC8, //Usage (Vendor-Defined 200)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x85, 0x30, //Report ID (48)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x01, //Logical Maximum (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0xE0, //Usage (Vendor-Defined 224)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x09, 0xE3, //Usage (Vendor-Defined 227)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x06, //Report Size (6)
			0xB1, 0x01, //Feature (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x31, //Report ID (49)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x01, //Logical Maximum (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0xE1, //Usage (Vendor-Defined 225)
			0xB1, 0x22, //Feature (Data,Var,Abs,NWrp,Lin,NPrf,NNul,NVol,Bit)
			0x75, 0x07, //Report Size (7)
			0xB1, 0x01, //Feature (Cnst,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)
			0x85, 0x11, //Report ID (17)
			0x15, 0x00, //Logical Minimum (0)
			0x25, 0x01, //Logical Maximum (1)
			0x95, 0x01, //Report Count (1)
			0x75, 0x01, //Report Size (1)
			0x09, 0xE2, //Usage (Vendor-Defined 226)
			0x81, 0x22, //Input (Data,Var,Abs,NWrp,Lin,NPrf,NNul,Bit)
			0x75, 0x07, //Report Size (7)
			0x81, 0x01, //Input (Cnst,Ary,Abs)
		0xC0, //End Collection
#endif
	}
};
#else
// default CONFIG_MCU_UDC_HID_PROTOCOL_KEYBOARD
/* hid descriptor for a keyboard temperarily*/
static struct hidg_func_descriptor our_hid_data = {
    .subclass            = 0, /* No subclass */
    .protocol            = 1, /* Keyboard */
    //.protocol        = 0, /* NULL */
    .report_length       = 8,
    .report_desc_length  = 63,
    .report_desc         = {
        0x05, 0x01,    /* USAGE_PAGE (Generic Desktop)              */
        0x09, 0x06,    /* USAGE (Keyboard)                       */
        0xa1, 0x01,    /* COLLECTION (Application)               */

        0x05, 0x07,    /*   USAGE_PAGE (Keyboard)                */
        0x19, 0xe0,    /*   USAGE_MINIMUM (Keyboard LeftControl) */
        0x29, 0xe7,    /*   USAGE_MAXIMUM (Keyboard Right GUI)   */
        0x15, 0x00,    /*   LOGICAL_MINIMUM (0)                  */
        0x25, 0x01,    /*   LOGICAL_MAXIMUM (1)                  */
        0x75, 0x01,    /*   REPORT_SIZE (1)                      */
        0x95, 0x08,    /*   REPORT_COUNT (8)                     */
        0x81, 0x02,    /*   INPUT (Data,Var,Abs)                 */

        0x95, 0x01,    /*   REPORT_COUNT (1)                     */
        0x75, 0x08,    /*   REPORT_SIZE (8)                      */
        0x81, 0x03,    /*   INPUT (Cnst,Var,Abs)                 */

        0x95, 0x05,    /*   REPORT_COUNT (5)                     */
        0x75, 0x01,    /*   REPORT_SIZE (1)                      */
        0x05, 0x08,    /*   USAGE_PAGE (LEDs)                    */
        0x19, 0x01,    /*   USAGE_MINIMUM (Num Lock)             */
        0x29, 0x05,    /*   USAGE_MAXIMUM (Kana)                 */
        0x91, 0x02,    /*   OUTPUT (Data,Var,Abs)                */

        0x95, 0x01,    /*   REPORT_COUNT (1)                     */
        0x75, 0x03,    /*   REPORT_SIZE (3)                      */
        0x91, 0x03,    /*   OUTPUT (Cnst,Var,Abs)                */

        0x95, 0x06,    /*   REPORT_COUNT (6)                     */
        0x75, 0x08,    /*   REPORT_SIZE (8)                      */
        0x15, 0x00,    /*   LOGICAL_MINIMUM (0)                  */
        0x25, 0x65,    /*   LOGICAL_MAXIMUM (101)                */
        0x05, 0x07,    /*   USAGE_PAGE (Keyboard)                */
        0x19, 0x00,    /*   USAGE_MINIMUM (Reserved)             */
        0x29, 0x65,    /*   USAGE_MAXIMUM (Keyboard Application) */
        0x81, 0x00,    /*   INPUT (Data,Ary,Abs)                 */

        0xc0        /* END_COLLECTION                         */
    }
};
#endif

/*-------------------------------------------------------------------------*/
/*
 * circular_buf_alloc
 *
 * Allocate a circular buffer and all associated memory.
 */
static int circular_buf_alloc(struct circular_buff *cb, void *buf, unsigned size)
{
    if (!buf)
        return -1;

    cb->buf      = (char *)buf;
    cb->buf_size = size;
    cb->buf_put  = cb->buf;
    cb->buf_get  = cb->buf;

    DBG ("cb : %p buf : %p, size : %d\n", cb, buf, size);

    return 0;
}

/*
 * circular_buf_free
 *
 * Free the buffer and all associated memory.
 */
static void circular_buf_free(struct circular_buff *cb)
{
    cb->buf      = NULL;
    cb->buf_size = 0;
}

/*
 * circular_buf_clear
 *
 * Clear out all data in the circular buffer.
 */
static void circular_buf_clear(struct circular_buff *cb)
{
    cb->buf_get = cb->buf_put;
    /* equivalent to a get of all data available */
}

/*
 * circular_buf_data_avail
 *
 * Return the number of bytes of data written into the circular
 * buffer.
 */
static unsigned circular_buf_data_avail(struct circular_buff *cb)
{
    return (cb->buf_size + cb->buf_put - cb->buf_get) % cb->buf_size;
}

/*
 * circular_buf_space_avail
 *
 * Return the number of bytes of space available in the circular
 * buffer.
 */
static unsigned circular_buf_space_avail(struct circular_buff *cb)
{
    return (cb->buf_size + cb->buf_get - cb->buf_put - 1) % cb->buf_size;
}

/*
 * circular_buf_put
 *
 * Copy data data from a user buffer and put it into the circular buffer.
 * Restrict to the amount of space available.
 *
 * Return the number of bytes copied.
 */
static unsigned circular_buf_put(struct circular_buff *cb, const unsigned char *buf, unsigned count)
{
    unsigned len;

    len  = circular_buf_space_avail(cb);
    if (count > len)
        count = len;

    if (count == 0)
        return 0;

    len = cb->buf + cb->buf_size - cb->buf_put;
    if (count > len) {
        memcpy(cb->buf_put, buf, len);
        memcpy(cb->buf, buf+len, count - len);
        cb->buf_put = cb->buf + count - len;
    } else {
        memcpy(cb->buf_put, buf, count);
        if (count < len)
            cb->buf_put += count;
        else /* count == len */
            cb->buf_put = cb->buf;
    }

    return count;
}

/*
 * circular_buf_get
 *
 * Get data from the circular buffer and copy to the given buffer.
 * Restrict to the amount of data available.
 *
 * Return the number of bytes copied.
 */
static unsigned circular_buf_get(struct circular_buff *cb, unsigned char *buf, unsigned count)
{
    unsigned len;

    len = circular_buf_data_avail(cb);
    if (count > len)
        count = len;

    if (count == 0)
        return 0;

    len = cb->buf + cb->buf_size - cb->buf_get;
    if (count > len) {
        memcpy(buf, cb->buf_get, len);
        memcpy(buf+len, cb->buf, count - len);
        cb->buf_get = cb->buf + count - len;
    } else {
        memcpy(buf, cb->buf_get, count);
        if (count < len)
            cb->buf_get += count;
        else /* count == len */
            cb->buf_get = cb->buf;
    }

    return count;
}

/*
 * hid_send_packet
 *
 * If there is data to send, a packet is built in the given
 * buffer and the size is returned.  If there is no data to
 * send, 0 is returned.
 *
 * Called with port_lock held.
 */
static int hid_send_packet(struct f_hidg *hidg, unsigned char *packet, unsigned size)
{
    unsigned len;

    len = circular_buf_data_avail(&hidg->us_cb);
    if (len < size)
        size = len;
    if (size != 0)
        size = circular_buf_get(&hidg->us_cb, packet, size);
    return size;
}

#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
static int hid_read_packet(struct f_hidg *hidg, unsigned char *packet, unsigned size)
{
    unsigned len;

    len = circular_buf_data_avail(&hidg->ds_cb);
    if (len < size)
        size = len;
    if (size != 0)
        size = circular_buf_get(&hidg->ds_cb, packet, size);
    return size;
}
#endif

/*-------------------------------------------------------------------------*/
/*                                usb_function                             */
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
static unsigned char req_buff[REPORT_LENGTH];
struct usb_request *alloc_ep_req(struct usb_ep *ep, int len, int default_len)
{
    struct usb_request      *req;

    req = usb_ep_alloc_request(ep);
    if (!req) {
        printf ("%s, alloc req failed\n", __func__);
        return NULL;
    }

    req->length = len;
    req->buf    = req_buff;

    if (len > REPORT_LENGTH) {
        printf ("%s, buff is too small, want : %d, actual : %d\n", __func__, len, REPORT_LENGTH);
        usb_ep_free_request(ep, req);
        req = NULL;
    }

    return req;
}

static inline struct usb_request *hidg_alloc_ep_req(struct usb_ep *ep,
        unsigned length)
{
    return alloc_ep_req(ep, length, length);
}

#if 0
static void pt_buf(unsigned char *buff, unsigned int len)
{
    unsigned int i;

    printf ("buff : ");

    for (i = 0; i < len; i++) {
        if (i % 16 == 0)
            printf ("\n");

        printf ("%02x ", buff[i]);
    }

    printf ("\n");
}
#endif

static void hidg_set_report_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct f_hidg *hidg = (struct f_hidg *) req->context;
    bool disconnect = false;
    int status;

    switch (req->status) {
        case -ESHUTDOWN:
            disconnect = true;
            DBG ("hid disconnect\n");
            break;

        default:
            /* presumably a transient fault */
            DBG ("%s hid unknow status %d\n", __func__, req->status);
            /* FALLTHROUGH */
        case 0:
            /* normal completion */
            break;
    }

    DBG ("%s, received, req len : %d, actual : %d, status : %d\n", __func__, req->length, req->actual, req->status);
    //pt_buf(req->buf, req->length);

    if (req->actual) {
        unsigned int count;
        count = circular_buf_put(&hidg->ds_cb, req->buf, req->actual);
        if (count < req->actual)
            DBG ("buff is overflowed\n");
    }

    if (disconnect || !hidg->running)
        return ;

    status = usb_ep_queue(hidg->out_ep, req);
    if (status)
        printf("%s queue req error --> %d\n", hidg->out_ep->name, status);
}
#endif

static int hidwrite(struct f_hidg *hidg);

static void f_hidg_req_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct f_hidg *hidg = (struct f_hidg *)ep->driver_data;

    hidg->write_pending = 0;

    switch (req->status) {
        default:
            DBG ("%s unknow status %d\n", __func__, req->status);
            /* FALL THROUGH */
        case 0:
            /* normal completion */
            if (circular_buf_data_avail(&hidg->us_cb))
                hidwrite(hidg);
            break;

        case -ESHUTDOWN:
            /* disconnect */
            DBG ("%s hid shutdown\n", __func__);
            break;
    }

    return ;
}

static int hidwrite(struct f_hidg *hidg)
{
    int status = -ENOMEM;
    unsigned int count;

    if (!hidg->running)
        return -ENXIO;
    if (!hidg->is_open)
        return -ENXIO;

#define HID_IS_BUSY (hidg->write_pending)
    if (HID_IS_BUSY)
        return -1;

    count = hid_send_packet(hidg, hidg->req->buf, hidg->report_length);
    if (count == 0)
        return 0;

    hidg->req->status   = 0;
    hidg->req->zero     = 0;
    hidg->req->length   = count;
    hidg->req->complete = f_hidg_req_complete;
    hidg->req->context  = hidg;
    hidg->write_pending = 1;

    status = usb_ep_queue(hidg->in_ep, hidg->req);
    if (status < 0) {
        DBG("usb_ep_queue error on int endpoint %zd\n", status);
        hidg->write_pending = 0;
    }

    return status;
}

#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
static void hid_setup_complete(struct usb_ep *ep, struct usb_request *req)
{
		struct f_hidg *hidg = (struct f_hidg *) req->context;

		if (req->status || req->actual != req->length) {
				DBG("%s %d, %u/%u\n", __func__, req->status, req->actual, req->length);
				return ;
		}

		if (req->actual) {
				unsigned int count;
				count = circular_buf_put(&hidg->ds_cb, req->buf, req->actual);
				if (count < req->actual)
						DBG ("buff is overflowed\n");
		}
}
#endif

static int hidg_setup(struct usb_function *f,
        const struct usb_ctrlrequest *ctrl)
{
    struct f_hidg            *hidg = func_to_hidg(f);
    struct usb_composite_dev    *cdev = f->config->cdev;
    struct usb_request        *req  = cdev->req;
    int status = 0;
    __u16 value, length;

    value    = le16_to_cpu(ctrl->wValue);
    length    = le16_to_cpu(ctrl->wLength);

    DBG("%s crtl_request : bRequestType:0x%x bRequest:0x%x Value:0x%x\n",
            __func__, ctrl->bRequestType, ctrl->bRequest, value);

    switch ((ctrl->bRequestType << 8) | ctrl->bRequest) {
        case ((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8
                | HID_REQ_GET_REPORT):
            DBG("get_report\n");

			/* send an empty report */
			length = min_t(unsigned, length, hidg->report_length);
			memset(req->buf, 0x0, length);

			goto respond;
			break;

        case ((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8
                | HID_REQ_GET_PROTOCOL):
            DBG("get_protocol\n");
			goto stall;
			break;

        case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8
                | HID_REQ_SET_IDLE):
            DBG ("recv set idle\n");
            length = 0;
			break;

        case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8
                | HID_REQ_SET_REPORT):
            DBG("set_report | wLength=%d\n", ctrl->wLength);

#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
            length = min_t(unsigned, length, hidg->report_length);
            memset(req->buf, 0x0, length);
           	req->complete = hid_setup_complete;
           	req->context  = hidg;

        	goto respond;
#else
			goto stall;
#endif
        	break;

        case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8
                | HID_REQ_SET_PROTOCOL):
        	DBG("set_protocol\n");
            goto stall;
			break;

        case ((USB_DIR_IN | USB_TYPE_STANDARD | USB_RECIP_INTERFACE) << 8
                | USB_REQ_GET_DESCRIPTOR):
            switch (value >> 8) {
            	case HID_DT_HID:
                {
                    struct hid_descriptor hidg_desc_copy = hidg_desc;
                    DBG("USB_REQ_GET_DESCRIPTOR: HID\n");
                    hidg_desc_copy.desc[0].bDescriptorType = HID_DT_REPORT;
                    hidg_desc_copy.desc[0].wDescriptorLength =
                        cpu_to_le16(hidg->report_desc_length);

                    length = min_t(unsigned short, length,
                            hidg_desc_copy.bLength);
                    DBG("USB_REQ_GET_DESCRIPTOR: HID length %d\n",length);
                    memcpy(req->buf, &hidg_desc_copy, length);
                    goto respond;
                    break;
                }
               case HID_DT_REPORT:
               		length = min_t(unsigned short, length, hidg->report_desc_length);
					DBG("USB_REQ_GET_DESCRIPTOR: REPORT length %d\n",length);
					memcpy(req->buf, hidg->report_desc, length);
                	goto respond;
                	break;

				default:
					DBG("Unknown descriptor request 0x%x\n", value >> 8);
					goto stall;
					break;
            }
        	break;

    default:
        DBG("Unknown request 0x%x\n",
                ctrl->bRequest);
        goto stall;
        break;
    }

stall:
    return -EOPNOTSUPP;

respond:
    req->zero = 0;
    req->length = length;
    status = usb_ep_queue(cdev->gadget->ep0, req);
    if (status < 0)
        ERR ("usb_ep_queue error on ep0 %d\n", value);
    return status;
}

static void hidg_disable(struct usb_function *f)
{
    struct f_hidg *hidg = func_to_hidg(f);

    usb_ep_disable(hidg->in_ep);
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    usb_ep_disable(hidg->out_ep);
#endif

    return ;
}

static void hidg_resume(struct usb_function *f)
{
    struct f_hidg *hidg = func_to_hidg(f);

    hidg->running = 1;

    return ;
}

static void hidg_suspend(struct usb_function *f)
{
    struct f_hidg *hidg = func_to_hidg(f);

    hidg->running = 0;

    return ;
}

static int hidg_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
    struct f_hidg *hidg = func_to_hidg(f);
    int status = 0;

    hidg->running = 0;
    DBG("hidg_set_alt intf:%d alt:%d\n", intf, alt);
    if (hidg->in_ep != NULL) {
        /* restart endpoint */
        usb_ep_disable(hidg->in_ep);

        status = config_ep_by_speed(f->config->cdev->gadget, f,
                hidg->in_ep);
        if (status) {
            printf ("config_ep_by_speed in FAILED!\n");
            goto fail;
        }
        status = usb_ep_enable(hidg->in_ep);
        if (status < 0) {
            printf ("Enable IN endpoint FAILED!\n");
            goto fail;
        }
        hidg->in_ep->driver_data = hidg;

        circular_buf_clear(&hidg->us_cb);
    }
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    if (hidg->out_ep != NULL) {
        int i;
        usb_ep_disable(hidg->out_ep);

        status = config_ep_by_speed(f->config->cdev->gadget, f,
                hidg->out_ep);
        if (status) {
            printf ("config_ep_by_speed out FAILED!\n");
            goto fail;
        }

        status = usb_ep_enable(hidg->out_ep);
        if (status < 0) {
            printf ("Enable OUT endpoint FAILED!\n");
            goto fail;
        }

        hidg->out_ep->driver_data = hidg;
        circular_buf_clear(&hidg->ds_cb);

        /*
         * allocate a bunch of read buffers and queue them all at once.
         */
        for (i = 0; i < hidg->qlen && status == 0; i++) {
            struct usb_request *req = hidg_alloc_ep_req(hidg->out_ep, hidg->report_length);
            if (req) {
                req->complete = hidg_set_report_complete;
                req->context  = hidg;
                status = usb_ep_queue(hidg->out_ep, req);
                if (status)
                    printf ("%s queue req --> %d\n", hidg->out_ep->name, status);
            } else {
                usb_ep_disable(hidg->out_ep);
                status = -ENOMEM;
                goto fail;
            }
        }
    }
#endif

    hidg->running = 1;
#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    our_callback_ops.state |= (1<<UAC2_HID_STATE_BIT);
    if (our_callback_ops.callback_ops.notify_callback)
        our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_HID, USB_FUNC_STATUS_ENABLE);
#endif
fail:
    return status;
}

static int hidg_bind(struct usb_configuration *c, struct usb_function *f)
{
    struct usb_ep        *ep;
    struct f_hidg        *hidg = func_to_hidg(f);
    struct usb_string    *us;
    int            status;

    /* maybe allocate device-global string IDs, and patch descriptors */
    us = usb_gstrings_attach(c->cdev, ct_func_strings,
            ARRAY_SIZE(ct_func_string_defs));
    if (!us)
        return -ENOMEM;


    hidg_interface_desc.iInterface = us[CT_FUNC_HID_IDX].id;

    /* allocate instance-specific interface IDs, and patch descriptors */
    status = usb_interface_id(c, f);
    if (status < 0)
        goto fail;
    hidg_interface_desc.bInterfaceNumber = status;

    /* allocate instance-specific endpoints */
    status = -ENODEV;
    ep = usb_ep_autoconfig(c->cdev->gadget, &hidg_fs_in_ep_desc);
    if (!ep)
        goto fail;
    hidg->in_ep = ep;
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    status = -ENODEV;
    ep = usb_ep_autoconfig(c->cdev->gadget, &hidg_fs_out_ep_desc);
    if (!ep)
        goto fail;
    hidg->out_ep = ep;
#endif
    /* preallocate request and buffer */
    status = -ENOMEM;
    hidg->req = usb_ep_alloc_request(hidg->in_ep);
    if (!hidg->req)
        goto fail;

    hidg->req->buf = &g_report_rbuf[0];

    /* set descriptor dynamic values */
    hidg_interface_desc.bInterfaceSubClass = hidg->bInterfaceSubClass;
    hidg_interface_desc.bInterfaceProtocol = hidg->bInterfaceProtocol;

    hidg_hs_in_ep_desc.wMaxPacketSize = cpu_to_le16(hidg->report_length);
    hidg_fs_in_ep_desc.wMaxPacketSize = cpu_to_le16(hidg->report_length);
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    hidg_hs_out_ep_desc.wMaxPacketSize = cpu_to_le16(hidg->report_length);
    hidg_fs_out_ep_desc.wMaxPacketSize = cpu_to_le16(hidg->report_length);
#endif

    /*
     * We can use hidg_desc struct here but we should not relay
     * that its content won't change after returning from this function.
     */
    hidg_desc.desc[0].bDescriptorType = HID_DT_REPORT;
    hidg_desc.desc[0].wDescriptorLength =
        cpu_to_le16(hidg->report_desc_length);

    hidg_hs_in_ep_desc.bEndpointAddress =
        hidg_fs_in_ep_desc.bEndpointAddress;
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    hidg_hs_out_ep_desc.bEndpointAddress =
        hidg_fs_out_ep_desc.bEndpointAddress;
#endif

    status = usb_assign_descriptors(f, hidg_fs_descriptors,
            hidg_hs_descriptors, NULL);
    if (status)
        goto fail;

    return 0;

fail:
    if (hidg->req != NULL) {
        if (hidg->in_ep != NULL)
            usb_ep_free_request(hidg->in_ep, hidg->req);
    }

    return status;
}

static void hidg_free_inst(struct usb_function_instance *f)
{
    struct f_hid_opts *opts;

    opts = container_of(f, struct f_hid_opts, func_inst);

    memset(opts, 0, sizeof(struct f_hid_opts));

    return ;
}

static struct usb_function_instance *hidg_alloc_inst(void)
{
    struct f_hid_opts *opts;
    struct usb_function_instance *ret;

    opts = &g_hid_opts;
    memset(opts, 0, sizeof(struct f_hid_opts));

    opts->func_inst.free_func_inst = hidg_free_inst;
    ret = &opts->func_inst;

    return ret;
}

static void hidg_free(struct usb_function *f)
{
    struct f_hidg *hidg;
    struct f_hid_opts *opts;

    hidg = func_to_hidg(f);

    circular_buf_free(&hidg->us_cb);
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    circular_buf_free(&hidg->ds_cb);
#endif

    opts = container_of(f->fi, struct f_hid_opts, func_inst);
    memset(hidg, 0, sizeof(struct f_hidg));

    --opts->refcnt;

    return ;
}

static void hidg_unbind(struct usb_configuration *c, struct usb_function *f)
{
    struct f_hidg *hidg = func_to_hidg(f);

    /* disable/free request and end point */
    usb_ep_disable(hidg->in_ep);
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    usb_ep_disable(hidg->out_ep);
#endif
    usb_ep_free_request(hidg->in_ep, hidg->req);
    usb_free_all_descriptors(f);

    return ;
}

static struct usb_function *hidg_alloc(struct usb_function_instance *fi)
{
    struct f_hidg *hidg;
    struct f_hid_opts *opts;

    hidg = &g_hidg;
    memset(hidg, 0, sizeof(struct f_hidg));
    DBG("hidg_alloc hidg %p\n",hidg);

    opts = container_of(fi, struct f_hid_opts, func_inst);

    ++opts->refcnt;

    hidg->bInterfaceSubClass = opts->subclass;
    hidg->bInterfaceProtocol = opts->protocol;
    hidg->report_length = opts->report_length;
    hidg->report_desc_length = opts->report_desc_length;
    if (opts->report_desc) {
        hidg->report_desc =     opts->report_desc;
    }

    hidg->func.name    = "hid";
    hidg->func.bind    = hidg_bind;
    hidg->func.unbind  = hidg_unbind;
    hidg->func.set_alt = hidg_set_alt;
    hidg->func.disable = hidg_disable;
    hidg->func.setup   = hidg_setup;
    hidg->func.free_func = hidg_free;
    hidg->func.resume  = hidg_resume;
    hidg->func.suspend = hidg_suspend;

    hidg->running      = 0;

    circular_buf_alloc(&hidg->us_cb, us_buff, sizeof(us_buff));
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    circular_buf_alloc(&hidg->ds_cb, ds_buff, sizeof(ds_buff));
    hidg->qlen = 1;
#endif

    return &hidg->func;
}

/*-------------------------- internal interface --------------------------*/

static struct usb_function_driver hid_func = {
    .name = "hid",
    .alloc_inst = hidg_alloc_inst,
    .alloc_func = hidg_alloc,
};

static int hid_mod_init(void)
{
    return usb_function_register(&hid_func);
}

static int hid_mod_uninit(void)
{
    usb_function_unregister(&hid_func);
    return 0;
}

int HidDoConfig(struct usb_configuration *c)
{
    int status;
    struct hidg_func_node *e,*n;

    list_for_each_entry(e, &hidg_func_list, node) {
        e->f = usb_get_function(e->fi);
        status = usb_add_function(c, e->f);
        if (status < 0) {
            usb_put_function(e->f);
            goto put;
        }
    }

    return 0;
put:
    list_for_each_entry(n, &hidg_func_list, node) {
        if (n == e)
            break;
        usb_remove_function(c, n->f);
        usb_put_function(n->f);
    }
    return status;
}

int HidBind(struct usb_composite_dev *cdev)
{
    int funcs = 0;
    struct hidg_func_node *n;
    struct f_hid_opts *hid_opts;
    struct list_head *tmp;

    list_for_each(tmp, &hidg_func_list)
        funcs++;

    if (!funcs)
        return -ENODEV;

    list_for_each_entry(n, &hidg_func_list, node) {
        n->fi = usb_get_function_instance("hid");

        hid_opts = container_of(n->fi, struct f_hid_opts, func_inst);
        hid_opts->subclass = n->func->subclass;
        hid_opts->protocol = n->func->protocol;
        hid_opts->report_length = n->func->report_length;
        hid_opts->report_desc_length = n->func->report_desc_length;
        hid_opts->report_desc = n->func->report_desc;
    }
    return 0;
}


int HidUnbind(struct usb_composite_dev *cdev)
{
    struct hidg_func_node *n;

    list_for_each_entry(n, &hidg_func_list, node) {
        usb_put_function(n->f);
        usb_put_function_instance(n->fi);
    }

    list_del_init(&hidg_func_list);

    return 0;
}

int HidInit(void)
{
    int ret;
    struct hidg_func_node *entry;
    struct hidg_func_descriptor *func = &our_hid_data;

    memset(&g_entry, 0, sizeof(g_entry));
    entry = &g_entry;
    entry->func = func;
    list_add_tail(&entry->node, &hidg_func_list);

    ret = hid_mod_init();

    return ret;
}

int HidDone(void)
{
    int ret = 0;
    ret = hid_mod_uninit();

    return ret;
}

/*----------------------------------- API -----------------------------------*/

int HidOpen(void)
{
    int *handle = NULL;
    handle = (int *)&g_hidg;
    DBG("handle %p\n",handle);
    g_hidg.is_open = true;
    g_hidg.write_pending = 0;

    return (int)handle;
}

int HidClose(int handle)
{
    struct f_hidg *p_f_hidg = (struct f_hidg *)handle;
    p_f_hidg->write_pending = 1;
    p_f_hidg->is_open = false;

    return 0;
}

int HidWrite(int handle, unsigned char *buffer, unsigned int count)
{
    struct f_hidg *hidg  = (struct f_hidg *)handle;
    int ret = 0;

    if (!hidg || hidg->is_open == false)
        return -1;

    if (count) {
        gx_disable_irq();
        ret = circular_buf_put(&hidg->us_cb, buffer, count);
        gx_enable_irq();
    }

    hidwrite(hidg);

    return ret;
}

int HidRead(int handle, unsigned char *buffer, unsigned int count)
{
#ifdef CONFIG_MCU_UDC_HID_ENABLE_DOWNSTREAM
    struct f_hidg *hidg  = (struct f_hidg *)handle;
    int ret = 0;

    if (!hidg || hidg->is_open == false)
        return -1;

    if (count) {
        gx_disable_irq();
        ret = hid_read_packet(hidg, buffer, count);
        gx_enable_irq();
    }

    return ret;
#else
    return 0;
#endif
}

unsigned int Uac2HidGetState(void)
{
#ifdef CONFIG_MCU_UDC_ENABLE_UAC2
    if (our_callback_ops.callback_ops.notify_callback)
    {
        return our_callback_ops.state;
    }
    else
#endif
        return 0;
}
