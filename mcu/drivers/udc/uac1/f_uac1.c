/*
 * f_audio.c -- USB Audio class function driver
  *
 * Copyright (C) 2008 Bryan Wu <cooloney@kernel.org>
 * Copyright (C) 2008 Analog Devices, Inc
 *
 * Enter bugs at http://blackfin.uclinux.org/
 *
 * Licensed under the GPL-2 or later.
 */

//#include <linux/slab.h>
//#include <linux/kernel.h>
//#include <linux/module.h>
//#include <linux/device.h>
//#include <linux/atomic.h>

#include "../include/ch9.h"
#include "../include/audio.h"
#include "../include/u_uac2.h"
#include "u_uac1.h"

#include <driver/uac2_core.h>
#include <driver/vsp_uac_fifo_manager.h>
#include <driver/usb_gadget.h>
#include <driver/delay.h>

#define LOG_TAG "<UAC 1.0> "
#define ERROR(fmt, ...) printf (LOG_TAG "ERR : " fmt, ##__VA_ARGS__)
#define DBG(fmt, ...)


// -------------------- get channel number --------------------
#ifdef CONFIG_VSP_SAMPLE_RATE_8K
#define VSP_SAMPLE_RATE     8000
#endif
#ifdef CONFIG_VSP_SAMPLE_RATE_16K
#define VSP_SAMPLE_RATE     16000
#endif
#ifdef CONFIG_VSP_SAMPLE_RATE_48K
#define VSP_SAMPLE_RATE     48000
#endif
#ifndef VSP_SAMPLE_RATE
#error "Unknown Sample Rate"
#endif

#ifdef CONFIG_VSP_HAS_UAC_MODE //uac mode
 #ifdef CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_16K
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 16000
 #elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_32K)
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 32000
 #elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_48K)
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 48000
 #elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_64K)
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 64000
 #elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_80K)
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 80000
 #elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_96K)
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 96000
 #else
 #define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 16000
 #endif

 #ifndef CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER
 #define CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER 1
 #endif

 #define UAC_PLAYBACK_CHANNEL_NUM_PRE  CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER
 #define UAC_CAPTURE_CHANNEL_NUM_PRE  ((CONFIG_VSP_OUT_INTERLACED_NUM * VSP_SAMPLE_RATE) / VSP_UAC_MODE_CAPTURE_SAMPLE_RATE)
#elif defined (CONFIG_VSP_HAS_PLC_MODE) // plc mode
/* assert :
 *     capture  : 16k
 *     playback : no
 */
 #define UAC_PLAYBACK_CHANNEL_NUM_PRE 2
 #define UAC_CAPTURE_CHANNEL_NUM_PRE  CONFIG_VSP_OUT_INTERLACED_NUM
#else // default mode
 #define UAC_PLAYBACK_CHANNEL_NUM_PRE 2
 #define UAC_CAPTURE_CHANNEL_NUM_PRE  1
#endif

#if (UAC_PLAYBACK_CHANNEL_NUM_PRE == 6)
#define UAC_PLAYBACK_CHANNEL_NUM 6
#elif (UAC_PLAYBACK_CHANNEL_NUM_PRE == 5)
#define UAC_PLAYBACK_CHANNEL_NUM 5
#elif (UAC_PLAYBACK_CHANNEL_NUM_PRE == 4)
#define UAC_PLAYBACK_CHANNEL_NUM 4
#elif (UAC_PLAYBACK_CHANNEL_NUM_PRE == 3)
#define UAC_PLAYBACK_CHANNEL_NUM 3
#elif (UAC_PLAYBACK_CHANNEL_NUM_PRE == 2)
#define UAC_PLAYBACK_CHANNEL_NUM 2
#elif (UAC_PLAYBACK_CHANNEL_NUM_PRE == 1)
#define UAC_PLAYBACK_CHANNEL_NUM 1
#else
#define UAC_PLAYBACK_CHANNEL_NUM 1
#endif

#if (UAC_CAPTURE_CHANNEL_NUM_PRE == 9)
#define UAC_CAPTURE_CHANNEL_NUM 9
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 8)
#define UAC_CAPTURE_CHANNEL_NUM 8
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 7)
#define UAC_CAPTURE_CHANNEL_NUM 7
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 6)
#define UAC_CAPTURE_CHANNEL_NUM 6
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 5)
#define UAC_CAPTURE_CHANNEL_NUM 5
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 4)
#define UAC_CAPTURE_CHANNEL_NUM 4
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 3)
#define UAC_CAPTURE_CHANNEL_NUM 3
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 2)
#define UAC_CAPTURE_CHANNEL_NUM 2
#elif (UAC_CAPTURE_CHANNEL_NUM_PRE == 1)
#define UAC_CAPTURE_CHANNEL_NUM 1
#else
#define UAC_CAPTURE_CHANNEL_NUM 1
#endif

// -------------------- define feature unit struct --------------------

DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(1);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(2);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(3);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(4);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(5);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(6);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(7);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(8);
DECLARE_UAC_FEATURE_UNIT_DESCRIPTOR(9);

#define TMP_X(x) struct uac_feature_unit_descriptor_##x
#define FEATURE_UNIT_STRUCT(channel_num) TMP_X(channel_num)
#define FEATURE_UNIT_PLAYBACK_STURCT FEATURE_UNIT_STRUCT(UAC_PLAYBACK_CHANNEL_NUM)
#define FEATURE_UNIT_CAPTURE_STRUCT  FEATURE_UNIT_STRUCT(UAC_CAPTURE_CHANNEL_NUM)

#define PLAYBACK_FEATURE_UNIT_SIZE UAC_DT_FEATURE_UNIT_SIZE(UAC_PLAYBACK_CHANNEL_NUM)
#define CAPTURE_FEATURE_UNIT_SIZE  UAC_DT_FEATURE_UNIT_SIZE(UAC_CAPTURE_CHANNEL_NUM)

// -------------------- define format type --------------------
DECLARE_UAC_FORMAT_TYPE_I_DISCRETE_DESC(1);

// -------------------- define ac header struct and others --------------------
/*
 * The number of AudioStreaming and MIDIStreaming interfaces
 * in the Audio Interface Collection
 */
#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) && defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
 #define F_AUDIO_NUM_INTERFACES	2
 DECLARE_UAC_AC_HEADER_DESCRIPTOR(2);
 #define AC_HEADER_STRUCT struct uac1_ac_header_descriptor_2
 #define UAC_DT_AC_HEADER_LENGTH	UAC_DT_AC_HEADER_SIZE(F_AUDIO_NUM_INTERFACES)
 #define UAC_DT_TOTAL_LENGTH (UAC_DT_AC_HEADER_LENGTH + UAC_DT_INPUT_TERMINAL_SIZE + UAC_DT_OUTPUT_TERMINAL_SIZE + PLAYBACK_FEATURE_UNIT_SIZE +  UAC_DT_INPUT_TERMINAL_SIZE + UAC_DT_OUTPUT_TERMINAL_SIZE + CAPTURE_FEATURE_UNIT_SIZE)
#elif defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
 #define F_AUDIO_NUM_INTERFACES	1
 DECLARE_UAC_AC_HEADER_DESCRIPTOR(1);
 #define AC_HEADER_STRUCT struct uac1_ac_header_descriptor_1
 #define UAC_DT_AC_HEADER_LENGTH	UAC_DT_AC_HEADER_SIZE(F_AUDIO_NUM_INTERFACES)
 #ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
 #define UAC_DT_TOTAL_LENGTH (UAC_DT_AC_HEADER_LENGTH + UAC_DT_INPUT_TERMINAL_SIZE + UAC_DT_OUTPUT_TERMINAL_SIZE + CAPTURE_FEATURE_UNIT_SIZE)
 #elif (defined CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
 #define UAC_DT_TOTAL_LENGTH (UAC_DT_AC_HEADER_LENGTH + UAC_DT_INPUT_TERMINAL_SIZE + UAC_DT_OUTPUT_TERMINAL_SIZE + PLAYBACK_FEATURE_UNIT_SIZE)
 #endif
#else
 #define F_AUDIO_NUM_INTERFACES	0
 DECLARE_UAC_AC_HEADER_DESCRIPTOR(0);
 #define AC_HEADER_STRUCT struct uac1_ac_header_descriptor_0
 #define UAC_DT_AC_HEADER_LENGTH	UAC_DT_AC_HEADER_SIZE(F_AUDIO_NUM_INTERFACES)
 #define UAC_DT_TOTAL_LENGTH (UAC_DT_AC_HEADER_LENGTH)
#endif

#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
static int generic_set_cmd(struct usb_audio_control *con, u8 cmd, int value);
static int generic_get_cmd(struct usb_audio_control *con, u8 cmd);
#endif
extern void uac_core_playback_callback(unsigned int data_size);
extern struct uac_internal_callbacks our_callback_ops;

// --------------- volume config --------------------
#define PLAYBACK_VOLUME_MIN (-25)
#define PLAYBACK_VOLUME_MAX (25)
#define PLAYBACK_VOLUME_RES 1
#define PLAYBACK_VOLUME_DEFAULT 0

#define CAPTURE_VOLUME_MIN 0
#define CAPTURE_VOLUME_MAX 100
#define CAPTURE_VOLUME_RES 1
#define CAPTURE_VOLUME_DEFAULT 50

#define UAC1_FU_MUTE               (1 << 0)
#define UAC1_FU_VOLUME             (1 << 1)
#define UAC1_FU_BASS               (1 << 2)
#define UAC1_FU_MID                (1 << 3)
#define UAC1_FU_TREBLE             (1 << 4)
#define UAC1_FU_GRAPHIC_EQUALIZER  (1 << 5)
#define UAC1_FU_AUTOMATIC_GAIN     (1 << 6)

#define FU_CONTROL_UNDEFINED      0x00
#define MUTE_CONTROL              0x01
#define VOLUME_CONTROL            0x02
#define BASS_CONTROL              0x03
#define MID_CONTROL               0x04
#define TREBLE_CONTROL            0x05
#define GRAPHIC_EQUALIZER_CONTROL 0x06
#define AUTOMATIC_GAIN_CONTROL    0x07


typedef enum uac_info_type {
	UAC_STREAM_DOWN = 0,
	UAC_STREAM_UP,
} uac_info_type_t;

struct uac_info {
	uac_info_type_t     type;
	unsigned int        psize;
	unsigned int        max_psize;
	struct usb_request *req[UAC1_REQ_COUNT];
};

struct f_audio {
	struct gaudio			card;

	/* endpoints handle full and/or high speeds */
	struct usb_ep			*out_ep;
	struct usb_ep			*in_ep;

	struct uac_info         downstream;
	struct uac_info         upstream;

	//spinlock_t			lock;
	struct f_audio_buf *copy_buf;
	//struct work_struct playback_work;
	struct list_head play_queue;

	/* Control Set command */
	struct list_head cs;
	u8 set_cmd;
	struct usb_audio_control *set_con;
	int cs_id;
};

// Will be rewritten during initialization
static int ds_channel_num = 2;
static int ds_sample_rate = 48000;
static int ds_bit_width   = 2;
static int us_channel_num = 1;
static int us_sample_rate = 16000;
static int us_bit_width   = 2;

#define DS_REQ_BUF_SIZE (48 * 2 * 4) // 48k 16bit 4 channel
#define US_REQ_BUF_SIZE (16 * 2 * 8) // 16k 16bit 8 channel
#define MAX_PACKET_ONLY (1 << 7)
#define TRIGGER_GATE 1950
#define FACTOR 1000
#define UAC1_NAME "UAC 1.0"

static unsigned int first_xfer = 0;
static unsigned int last_xfer_addr, last_xfer_size;

static struct usb_function_instance *fi_uac1;
static struct usb_function *f_uac1;
static struct f_audio f_audio_;

static unsigned char ds_req_buff[UAC1_REQ_COUNT][DS_REQ_BUF_SIZE]; // downstream req buff
static unsigned char us_req_buff[UAC1_REQ_COUNT][US_REQ_BUF_SIZE];  // upstream req buff

/*
 * We have two interfaces- AudioControl and AudioStreaming
 * TODO: only supcard playback currently
 */
enum {
	F_AUDIO_AC_INTERFACE = 0,

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	F_AUDIO_AS_INTERFACE_DOWNSTREAM,
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	F_AUDIO_AS_INTERFACE_UPSTREAM,
#endif
};

/* B.3.1  Standard AC Interface Descriptor */
static struct usb_interface_descriptor ac_interface_desc = {
	.bLength            = USB_DT_INTERFACE_SIZE,
	.bDescriptorType    = USB_DT_INTERFACE,
	.bNumEndpoints      = 0,
	.bInterfaceClass    = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOCONTROL,
	.bInterfaceProtocol = 0,
};

/* B.3.2  Class-Specific AC Interface Descriptor */
static AC_HEADER_STRUCT ac_header_desc = {
	.bLength            = UAC_DT_AC_HEADER_LENGTH,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_HEADER,
	.bcdADC             = cpu_to_le16(0x0100),
	.wTotalLength       = cpu_to_le16(UAC_DT_TOTAL_LENGTH),
	.bInCollection      = F_AUDIO_NUM_INTERFACES,

#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
	.baInterfaceNr = {
	/* Interface number of the first AudioStream interface */
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
		[F_AUDIO_AS_INTERFACE_DOWNSTREAM - 1] = F_AUDIO_AS_INTERFACE_DOWNSTREAM,
#endif
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
		[F_AUDIO_AS_INTERFACE_UPSTREAM - 1] = F_AUDIO_AS_INTERFACE_UPSTREAM,
#endif
	}
#endif
};

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
#define INPUT_TERMINAL_ID         1
#define FEATURE_UNIT_ID_PLAYBACK  2
#define OUTPUT_TERMINAL_ID        3

static struct uac_input_terminal_descriptor input_terminal_desc = {
	.bLength            = UAC_DT_INPUT_TERMINAL_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_INPUT_TERMINAL,
	.bTerminalID        = INPUT_TERMINAL_ID,
	.wTerminalType      = UAC_TERMINAL_STREAMING,
	.bAssocTerminal     = 0,

	// Below field will be overwritten later
	.bNrChannels        = 0,
	.wChannelConfig     = 0x0,
};

static FEATURE_UNIT_PLAYBACK_STURCT feature_unit_desc_playback = {
	.bLength		    = PLAYBACK_FEATURE_UNIT_SIZE,
	.bDescriptorType	= USB_DT_CS_INTERFACE,
	.bDescriptorSubtype	= UAC_FEATURE_UNIT,
	.bUnitID		    = FEATURE_UNIT_ID_PLAYBACK,
	.bSourceID		    = INPUT_TERMINAL_ID,
	.bControlSize		= 1,
	.bmaControls[0]		= (UAC1_FU_MUTE),

	// other logical channel will be configed later
};


static struct uac1_output_terminal_descriptor output_terminal_desc = {
	.bLength            = UAC_DT_OUTPUT_TERMINAL_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_OUTPUT_TERMINAL,
	.bTerminalID        = OUTPUT_TERMINAL_ID,
#ifdef CONFIG_MCU_UDC_UAC2_PLAYBACK_TERMINAL_TYPE
	.wTerminalType      = cpu_to_le16(CONFIG_MCU_UDC_UAC2_PLAYBACK_TERMINAL_TYPE),
#else
	.wTerminalType      = cpu_to_le16(UAC_OUTPUT_TERMINAL_SPEAKER),
#endif
	.bAssocTerminal     = 0,
	.bSourceID          = FEATURE_UNIT_ID_PLAYBACK,
};

static struct usb_audio_control mute_control_playback = {
	.list = LIST_HEAD_INIT(mute_control_playback.list),
	.name = "Mute Control",
	.type = MUTE_CONTROL,
	/* Todo: add real Mute control code */
	.set = generic_set_cmd,
	.get = generic_get_cmd,
};

static struct usb_audio_control volume_control_playback = {
	.list = LIST_HEAD_INIT(volume_control_playback.list),
	.name = "Volume Control",
	.type = VOLUME_CONTROL,
	/* Todo: add real Volume control code */
	.set = generic_set_cmd,
	.get = generic_get_cmd,
};

static struct usb_audio_control_selector feature_unit_playback = {
	.list = LIST_HEAD_INIT(feature_unit_playback.list),
	.id = FEATURE_UNIT_ID_PLAYBACK,
	.name = "Mute & Volume",
	.type = UAC_FEATURE_UNIT,
	.desc = (struct usb_descriptor_header *)&feature_unit_desc_playback,
};
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
#define AIN_INPUT_TERMINAL_ID	10
#define USB_OUTPUT_TERMINAL_ID	11
#define FEATURE_UNIT_ID_CAPTURE 12

static struct uac_input_terminal_descriptor ain_input_terminal_desc = {
	.bLength            = UAC_DT_INPUT_TERMINAL_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_INPUT_TERMINAL,
	.bTerminalID        = AIN_INPUT_TERMINAL_ID,
#ifdef CONFIG_MCU_UDC_UAC2_CAPTURE_TERMINAL_TYPE
	.wTerminalType      = cpu_to_le16(CONFIG_MCU_UDC_UAC2_CAPTURE_TERMINAL_TYPE),
#else
	.wTerminalType      = cpu_to_le16(UAC_INPUT_TERMINAL_MICROPHONE),
#endif
	.bAssocTerminal     = 0,

	// Below field will be overwritten later
	.bNrChannels        = 0,
	.wChannelConfig     = 0,
};

static FEATURE_UNIT_CAPTURE_STRUCT feature_unit_desc_capture = {
	.bLength            = CAPTURE_FEATURE_UNIT_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_FEATURE_UNIT,
	.bUnitID            = FEATURE_UNIT_ID_CAPTURE,
	.bSourceID          = AIN_INPUT_TERMINAL_ID,
	.bControlSize       = 1,
	.bmaControls[0]     = UAC1_FU_MUTE | UAC1_FU_AUTOMATIC_GAIN,

	// other logical channel will be configed later
};

static struct uac1_output_terminal_descriptor usb_output_terminal_desc = {
	.bLength            = UAC_DT_OUTPUT_TERMINAL_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_OUTPUT_TERMINAL,
	.bTerminalID        = USB_OUTPUT_TERMINAL_ID,
	.wTerminalType      = UAC_TERMINAL_STREAMING,
	.bAssocTerminal     = 0,
	.bSourceID          = FEATURE_UNIT_ID_CAPTURE,
};

static struct usb_audio_control mute_control_capture = {
	.list = LIST_HEAD_INIT(mute_control_capture.list),
	.name = "Mute Control",
	.type = MUTE_CONTROL,
	/* Todo: add real Mute control code */
	.set = generic_set_cmd,
	.get = generic_get_cmd,
};

static struct usb_audio_control volume_control_capture = {
	.list = LIST_HEAD_INIT(volume_control_capture.list),
	.name = "Volume Control",
	.type = VOLUME_CONTROL,
	/* Todo: add real Volume control code */
	.set = generic_set_cmd,
	.get = generic_get_cmd,
};

static struct usb_audio_control agc_control_capture = {
	.list = LIST_HEAD_INIT(agc_control_capture.list),
	.name = "AGC Control",
	.type = AUTOMATIC_GAIN_CONTROL,
	/* Todo: add real Volume control code */
	.set = generic_set_cmd,
	.get = generic_get_cmd,
};

static struct usb_audio_control_selector feature_unit_capture = {
	.list = LIST_HEAD_INIT(feature_unit_capture.list),
	.id = FEATURE_UNIT_ID_CAPTURE,
	.name = "Mute & Volume & AGC Control",
	.type = UAC_FEATURE_UNIT,
	.desc = (struct usb_descriptor_header *)&feature_unit_desc_capture,
};
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
/* B.4.1  Standard AS Interface Descriptor */
static struct usb_interface_descriptor as_interface_alt_0_desc = {
	.bLength            = USB_DT_INTERFACE_SIZE,
	.bDescriptorType    = USB_DT_INTERFACE,
	.bAlternateSetting  = 0,
	.bNumEndpoints      = 0,
	.bInterfaceClass    = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
};

static struct usb_interface_descriptor as_interface_alt_1_desc = {
	.bLength            = USB_DT_INTERFACE_SIZE,
	.bDescriptorType    = USB_DT_INTERFACE,
	.bAlternateSetting  = 1,
	.bNumEndpoints      = 1,
	.bInterfaceClass    = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
};

/* B.4.2  Class-Specific AS Interface Descriptor */
static struct uac1_as_header_descriptor as_header_desc = {
	.bLength            = UAC_DT_AS_HEADER_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_AS_GENERAL,
	.bTerminalLink      = INPUT_TERMINAL_ID,
	.bDelay             = 0,
	.wFormatTag         = UAC_FORMAT_TYPE_I_PCM,
};

static struct uac_format_type_i_discrete_descriptor_1 as_type_i_desc = {
	.bLength            = UAC_FORMAT_TYPE_I_DISCRETE_DESC_SIZE(1),
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_FORMAT_TYPE,
	.bFormatType        = UAC_FORMAT_TYPE_I,
	.bSubframeSize      = 2,
	.bBitResolution     = 16,
	.bSamFreqType       = 1,

	// Below field will be overwritten later
	//.bNrChannels        = 0,
	//.tSamFreq[0]        = 0,
};

/* Standard ISO OUT Endpoint Descriptor */
static struct usb_endpoint_descriptor as_out_ep_desc  = {
	.bLength          = USB_DT_ENDPOINT_AUDIO_SIZE,
	.bDescriptorType  = USB_DT_ENDPOINT,
	.bEndpointAddress = USB_DIR_OUT,
	.bmAttributes     = USB_ENDPOINT_SYNC_ADAPTIVE | USB_ENDPOINT_XFER_ISOC,
	.wMaxPacketSize   = cpu_to_le16(UAC1_OUT_EP_MAX_PACKET_SIZE),
	.bInterval        = 1,
};

/* Class-specific AS ISO OUT Endpoint Descriptor */
static struct uac_iso_endpoint_descriptor as_iso_out_desc = {
	.bLength            = UAC_ISO_ENDPOINT_DESC_SIZE,
	.bDescriptorType    = USB_DT_CS_ENDPOINT,
	.bDescriptorSubtype = UAC_EP_GENERAL,
	.bmAttributes       = 0,
	.bLockDelayUnits    = 0,
	.wLockDelay         = cpu_to_le16(0),
};
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
//             add for in
/* B.4.1  Standard AS Interface Descriptor */
static struct usb_interface_descriptor as_in_interface_alt_0_desc = {
	.bLength            = USB_DT_INTERFACE_SIZE,
	.bDescriptorType    = USB_DT_INTERFACE,
	.bAlternateSetting  = 0,
	.bNumEndpoints      = 0,
	.bInterfaceClass    = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
};

static struct usb_interface_descriptor as_in_interface_alt_1_desc = {
	.bLength            = USB_DT_INTERFACE_SIZE,
	.bDescriptorType    = USB_DT_INTERFACE,
	.bAlternateSetting  = 1,
	.bNumEndpoints      = 1,
	.bInterfaceClass    = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
};

/* B.4.2  Class-Specific AS Interface Descriptor */
static struct uac1_as_header_descriptor as_in_header_desc = {
	.bLength            = UAC_DT_AS_HEADER_SIZE,
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_AS_GENERAL,
	.bTerminalLink      = USB_OUTPUT_TERMINAL_ID,
	.bDelay             = 1,
	.wFormatTag         = UAC_FORMAT_TYPE_I_PCM,
};

static struct uac_format_type_i_discrete_descriptor_1 as_in_type_i_desc = {
	.bLength            = UAC_FORMAT_TYPE_I_DISCRETE_DESC_SIZE(1),
	.bDescriptorType    = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_FORMAT_TYPE,
	.bFormatType        = UAC_FORMAT_TYPE_I,
	.bSubframeSize      = 2,
	.bBitResolution     = 16,
	.bSamFreqType       = 1,

	// Below field will be overwritten later
	//.bNrChannels        = 0,
	//.tSamFreq[0]        = 0,
};

/* Standard ISO IN Endpoint Descriptor */
static struct usb_endpoint_descriptor as_in_ep_desc  = {
	.bLength =		USB_DT_ENDPOINT_AUDIO_SIZE,
	.bDescriptorType =	USB_DT_ENDPOINT,
	.bEndpointAddress =	USB_DIR_IN,
	.bmAttributes =		USB_ENDPOINT_SYNC_SYNC
				| USB_ENDPOINT_XFER_ISOC,
	//.wMaxPacketSize	=	cpu_to_le16(UAC1_IN_EP_MAX_PACKET_SIZE),
	.bInterval =		1,
};

/* Class-specific AS ISO IN Endpoint Descriptor */
static struct uac_iso_endpoint_descriptor as_iso_in_desc = {
	.bLength =		UAC_ISO_ENDPOINT_DESC_SIZE,
	.bDescriptorType =	USB_DT_CS_ENDPOINT,
	.bDescriptorSubtype =	UAC_EP_GENERAL,
	.bmAttributes =     MAX_PACKET_ONLY,
	.bLockDelayUnits =	0,
	.wLockDelay =		0,
};
#endif

static struct usb_descriptor_header *f_audio_desc[] = {
	(struct usb_descriptor_header *)&ac_interface_desc,
	(struct usb_descriptor_header *)&ac_header_desc,

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	(struct usb_descriptor_header *)&input_terminal_desc,
	(struct usb_descriptor_header *)&feature_unit_desc_playback,
	(struct usb_descriptor_header *)&output_terminal_desc,
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	(struct usb_descriptor_header *)&ain_input_terminal_desc,
	(struct usb_descriptor_header *)&feature_unit_desc_capture,
	(struct usb_descriptor_header *)&usb_output_terminal_desc,
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	(struct usb_descriptor_header *)&as_interface_alt_0_desc,
	(struct usb_descriptor_header *)&as_interface_alt_1_desc,
	(struct usb_descriptor_header *)&as_header_desc,
	(struct usb_descriptor_header *)&as_type_i_desc,

	(struct usb_descriptor_header *)&as_out_ep_desc,
	(struct usb_descriptor_header *)&as_iso_out_desc,
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	(struct usb_descriptor_header *)&as_in_interface_alt_0_desc,
	(struct usb_descriptor_header *)&as_in_interface_alt_1_desc,
	(struct usb_descriptor_header *)&as_in_header_desc,
	(struct usb_descriptor_header *)&as_in_type_i_desc,

	(struct usb_descriptor_header *)&as_in_ep_desc,
	(struct usb_descriptor_header *)&as_iso_in_desc,
#endif
	NULL,
};

enum {
	STR_AC_IF,

#if 0
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	STR_INPUT_TERMINAL,
	STR_INPUT_TERMINAL_CH_NAMES,
	STR_FEAT_DESC_0,
	STR_OUTPUT_TERMINAL,
	STR_AS_IF_ALT0,
	STR_AS_IF_ALT1,
#endif
#endif
};

static struct usb_string strings_uac1[] = {
#ifdef CONFIG_VSP_UAC_NAME_IN_WINDOWS
	[STR_AC_IF].s = CONFIG_VSP_UAC_NAME_IN_WINDOWS, //"AC Interface",
#else
	[STR_AC_IF].s = "USB Audio Device", //"AC Interface",
#endif

#if 0
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	[STR_INPUT_TERMINAL].s = "Input terminal",
	[STR_INPUT_TERMINAL_CH_NAMES].s = "Channels",
	[STR_FEAT_DESC_0].s = "Volume control & mute",
	[STR_OUTPUT_TERMINAL].s = "Output terminal",
	[STR_AS_IF_ALT0].s = "AS Interface",
	[STR_AS_IF_ALT1].s = "AS Interface",
#endif
#endif
	{ },
};

static struct usb_gadget_strings str_uac1 = {
	.language = 0x0409,	/* en-us */
	.strings = strings_uac1,
};

static struct usb_gadget_strings *uac1_strings[] = {
	&str_uac1,
	NULL,
};

/*-------------------------------------------------------------------------*/

static inline struct f_audio *func_to_audio(struct usb_function *f)
{
	return container_of(f, struct f_audio, card.func);
}

static inline uint num_channels(uint chanmask)
{
    uint num = 0;

    while (chanmask) {
        num += (chanmask & 1);
        chanmask >>= 1;
    }

    return num;
}

static int cal_lost_packet_num(unsigned long long time)
{
    int lost_num = 0;

    while (1) {
        if (time < TRIGGER_GATE)
            break;

        if (time <= 1000) {
            lost_num++;
            break;
        }

        time -= 1000;
        lost_num++;
    }

    return lost_num;
}

/*-------------------------------------------------------------------------*/

static int f_audio_out_ep_complete(struct usb_ep *ep, struct usb_request *req)
{
	int err;

	DBG ("%s %s %d\n", __func__, ep->name, req->actual);

	uac_core_playback_callback(req->actual);
	req->buf = (void *)get_next_buff();

	err = usb_ep_queue(ep, req);
	if (err)
		ERROR("%s queue req: %d\n", ep->name, err);

	return 0;
}

static int f_audio_in_ep_complete(struct usb_ep *ep, struct usb_request *req)
{
	int err;
	void *ret_buf;
	struct f_audio *audio = req->context;
	struct uac_info *info = &audio->upstream;

	unsigned long long cur_time, time_interval;
	static unsigned long long prev_time;

	unsigned int ain_handle = our_callback_ops.callback_ops.ain_buff_handle;

	req->length = info->psize;

	uac_core_capture_callback(req->actual);

	cur_time = get_time_us();
	if (unlikely(first_xfer)) {
		prev_time = cur_time;
		first_xfer = 0;
	}

	time_interval = cur_time - prev_time;
	if (unlikely(time_interval >= TRIGGER_GATE)) {
		uac_up_lost_pkt_num    = cal_lost_packet_num(time_interval);
		uac_up_left_pkt_num       += uac_up_lost_pkt_num;

		// 统计用
		uac_up_lost_pkt        = 1;
		uac_up_lost_pkt_time   = time_interval;
		uac_up_lost_pkt_total += uac_up_lost_pkt_num;
	}
	prev_time = cur_time;

	// 发包策略
	if (likely(req->status == 0)) {
		ret_buf = AudioBuffConsume(ain_handle, last_xfer_size);
		if (likely(ret_buf))
			req->buf = ret_buf;

		// 丢包发双倍
		if (uac_up_left_pkt_num) {
			if (!AudioBuffConsumeCheck(ain_handle, req->length * 2)) {
				req->length *= 2;
				uac_up_left_pkt_num--;
			}
		}
	} else {
		uac_up_left_pkt_num++;

		if (last_xfer_size != req->length)
			uac_up_left_pkt_num++;

		if (!AudioBuffConsumeCheck(ain_handle, req->length * 2)) {
			req->length *= 2;
			uac_up_left_pkt_num--;
		}

		req->buf = (void *)last_xfer_addr;
	}

	last_xfer_addr = (unsigned int)req->buf;
	last_xfer_size = req->length;

	err = usb_ep_queue(ep, req);
	if (err)
		ERROR("%s queue req: %d\n", ep->name, err);

	return 0;
}

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
/*
 * volume_conf: The volume configured by the host
 * volume_new: Volume configured for audio play
 *
 * volume_new / volume_conf = AUDIO_PLAY_VOLUME_MAX / PLAYBACK_VOLUME_MAX
 */
static short playback_volume_convert(short volume_conf)
{
#define AUDIO_PLAY_VOLUME_MAX  18 //db
#define AUDIO_PLAY_VOLUME_MIN -18
	if (volume_conf <= PLAYBACK_VOLUME_MIN)
		return AUDIO_PLAY_VOLUME_MIN;

	if (volume_conf >= PLAYBACK_VOLUME_MAX)
		return AUDIO_PLAY_VOLUME_MAX;

	return volume_conf * AUDIO_PLAY_VOLUME_MAX / PLAYBACK_VOLUME_MAX;
}
#endif

static void f_audio_complete(struct usb_ep *ep, struct usb_request *req)
{
	struct f_audio *audio = req->context;
	int status = req->status;
	u32 data = 0;
	struct usb_ep *out_ep = audio->out_ep;
	struct usb_ep *in_ep = audio->in_ep;
	short volume = 0;

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	short act_volume = 0;
#endif

	switch (status) {

	case 0:				/* normal completion? */
		if (ep == out_ep)
			f_audio_out_ep_complete(ep, req);
		else if (ep == in_ep)
			f_audio_in_ep_complete(ep, req);
		else if (audio->set_con) {
			memcpy(&data, req->buf, req->length);
			audio->set_con->set(audio->set_con, audio->set_cmd, le32_to_cpu(data));

			DBG ("cmd : %d csid : \n", audio->set_cmd, audio->cs_id);

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
			if (audio->cs_id == FEATURE_UNIT_ID_PLAYBACK) {
				switch (audio->set_con->type) {
					case MUTE_CONTROL:
						DBG ("playback mute : %d\n", le32_to_cpu(data));
						our_callback_ops.callback_ops.mute_callback(UAC2_CONTROL_DOWNSTREAM, le32_to_cpu(data));
						break;
					case VOLUME_CONTROL:
						volume = (signed short)le32_to_cpu(data);
						act_volume = playback_volume_convert(volume);
						DBG ("playback volume : %d %hx new : %d %hx\n", volume, volume, act_volume, act_volume);
						our_callback_ops.callback_ops.volume_callback(UAC2_CONTROL_DOWNSTREAM, act_volume);
						break;
					default:
						break;
				}
			}
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
			if (audio->cs_id == FEATURE_UNIT_ID_CAPTURE) {
				switch (audio->set_con->type) {
					case MUTE_CONTROL:
						DBG ("capture mute : %d\n", le32_to_cpu(data));
						our_callback_ops.callback_ops.mute_callback(UAC2_CONTROL_UPSTREAM, le32_to_cpu(data));
						break;
					case VOLUME_CONTROL:
						volume = (signed short)le32_to_cpu(data);
						DBG ("capture volume : %d\n", volume);
						our_callback_ops.callback_ops.volume_callback(UAC2_CONTROL_UPSTREAM, volume);
						break;
					case AUTOMATIC_GAIN_CONTROL:
						DBG ("capture agc : %d\n", le32_to_cpu(data));
						our_callback_ops.callback_ops.auto_gain_control_callback(UAC2_CONTROL_UPSTREAM, le32_to_cpu(data));
						break;
					default:
						break;
				}
			}
#endif

			audio->set_con = NULL;
			audio->cs_id   = 0;
		}
		break;
	default:
		break;
	}
}

static int audio_set_intf_req(struct usb_function *f,
		const struct usb_ctrlrequest *ctrl)
{
	struct f_audio		*audio = func_to_audio(f);
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request	*req = cdev->req;
	u8			id = ((le16_to_cpu(ctrl->wIndex) >> 8) & 0xFF);
	u16			len = le16_to_cpu(ctrl->wLength);
	u16			w_value = le16_to_cpu(ctrl->wValue);
	u8			con_sel = (w_value >> 8) & 0xFF;
	u8			cmd = (ctrl->bRequest & 0x0F);
	struct usb_audio_control_selector *cs;
	struct usb_audio_control *con;

	DBG ("bRequest 0x%x, w_value 0x%04x, len %d, entity %d\n",
			ctrl->bRequest, w_value, len, id);

	list_for_each_entry(cs, &audio->cs, list) {
		if (cs->id == id) {
			list_for_each_entry(con, &cs->control, list) {
				DBG ("contype : %x con : %x\n", con->type, con_sel);
				if (con->type == con_sel) {
					audio->set_con = con;
					break;
				}
			}
			break;
		}
	}

	audio->cs_id = id;
	audio->set_cmd = cmd;
	req->context = audio;
	req->complete = f_audio_complete;

	return len;
}

static int audio_get_intf_req(struct usb_function *f,
		const struct usb_ctrlrequest *ctrl)
{
	struct f_audio		*audio = func_to_audio(f);
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request	*req = cdev->req;
	int			value = -EOPNOTSUPP;
	u8			id = ((le16_to_cpu(ctrl->wIndex) >> 8) & 0xFF);
	u16			len = le16_to_cpu(ctrl->wLength);
	u16			w_value = le16_to_cpu(ctrl->wValue);
	u8			con_sel = (w_value >> 8) & 0xFF;
	u8			cmd = (ctrl->bRequest & 0x0F);
	struct usb_audio_control_selector *cs;
	struct usb_audio_control *con;

	DBG ("bRequest 0x%x, w_value 0x%04x, len %d, entity %d\n",
			ctrl->bRequest, w_value, len, id);

	list_for_each_entry(cs, &audio->cs, list) {
		if (cs->id == id) {
			list_for_each_entry(con, &cs->control, list) {
				if (con->type == con_sel && con->get) {
					value = con->get(con, cmd);
					DBG ("send value : %x\n", value);
					break;
				}
			}
			break;
		}
	}

	req->context = audio;
	req->complete = f_audio_complete;
	len = min_t(size_t, sizeof(value), len);
	memcpy(req->buf, &value, len);

	return len;
}

static int audio_set_endpoint_req(struct usb_function *f,
		const struct usb_ctrlrequest *ctrl)
{
	//struct usb_composite_dev *cdev = f->config->cdev;
	int			value = -EOPNOTSUPP;
	//u16			ep = le16_to_cpu(ctrl->wIndex);
	u16			len = le16_to_cpu(ctrl->wLength);
	//u16			w_value = le16_to_cpu(ctrl->wValue);

	DBG ("bRequest 0x%x, w_value 0x%04x, len %d, endpoint %d\n",
			ctrl->bRequest, w_value, len, ep);

	switch (ctrl->bRequest) {
	case UAC_SET_CUR:
		value = len;
		break;
	case UAC_SET_MIN:
		break;
	case UAC_SET_MAX:
		break;
	case UAC_SET_RES:
		break;
	case UAC_SET_MEM:
		break;
	default:
		break;
	}

	return value;
}

static int audio_get_endpoint_req(struct usb_function *f,
		const struct usb_ctrlrequest *ctrl)
{
	//struct usb_composite_dev *cdev = f->config->cdev;
	int value = -EOPNOTSUPP;
	//u8 ep = ((le16_to_cpu(ctrl->wIndex) >> 8) & 0xFF);
	u16 len = le16_to_cpu(ctrl->wLength);
	//u16 w_value = le16_to_cpu(ctrl->wValue);

	DBG ("bRequest 0x%x, w_value 0x%04x, len %d, endpoint %d\n",
			ctrl->bRequest, w_value, len, ep);

	switch (ctrl->bRequest) {
	case UAC_GET_CUR:
	case UAC_GET_MIN:
	case UAC_GET_MAX:
	case UAC_GET_RES:
		value = len;
		break;
	case UAC_GET_MEM:
		break;
	default:
		break;
	}

	return value;
}

static int f_audio_setup(struct usb_function *f, const struct usb_ctrlrequest *ctrl)
{
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request	*req = cdev->req;
	int			value = -EOPNOTSUPP;
	u16			w_index = le16_to_cpu(ctrl->wIndex);
	u16			w_value = le16_to_cpu(ctrl->wValue);
	u16			w_length = le16_to_cpu(ctrl->wLength);

	DBG ("%s request type : %x req : %x index : %x value : %x length : %x\n", __func__,
		ctrl->bRequestType, ctrl->bRequest, w_index, w_value, w_length);

	/* composite driver infrastructure handles everything; interface
	 * activation uses set_alt().
	 */
	switch (ctrl->bRequestType) {
	case USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE: // 0x21
		value = audio_set_intf_req(f, ctrl);
		break;

	case USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE:  // 0xa1
		value = audio_get_intf_req(f, ctrl);
		break;

	case USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_ENDPOINT:  // 0x22
		value = audio_set_endpoint_req(f, ctrl);
		break;

	case USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_ENDPOINT:  //  0xa2
		value = audio_get_endpoint_req(f, ctrl);
		break;

	default:
		ERROR("invalid control req %02x.%02x v%04x i%04x l%d\n",
			ctrl->bRequestType, ctrl->bRequest,
			w_value, w_index, w_length);
	}

	/* respond with data transfer or status phase? */
	if (value >= 0) {
		DBG("audio req %02x.%02x v%04x i%04x l%d\n",
			ctrl->bRequestType, ctrl->bRequest,
			w_value, w_index, w_length);
		req->zero = 0;
		req->length = value;
		value = usb_ep_queue(cdev->gadget->ep0, req);
		if (value < 0)
			ERROR("audio response on err %d\n", value);
	}

	/* device either stalls (value < 0) or reports success */
	return value;
}

static inline void free_ep(struct uac_info *info, struct usb_ep *ep)
{
    int i;

    if (!info)
    	return ;

    for (i = 0; i < ARRAY_SIZE(info->req); i++) {
        if (info->req[i]) {
            usb_ep_dequeue(ep, info->req[i]);
            usb_ep_free_request(ep, info->req[i]);
            info->req[i] = NULL;
        }
    }

    if (usb_ep_disable(ep))
        ERROR ("%s:%d Error!\n", __func__, __LINE__);

    return ;
}

static int f_audio_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
	int err = 0;
#if defined(CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK) || defined(CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE)
	struct f_audio		*audio = func_to_audio(f);
	struct usb_composite_dev *cdev = f->config->cdev;
	int i = 0;
	struct uac_info *info;
	struct usb_request *req;
	int req_count;
	struct f_uac1_opts *opts;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	struct usb_ep *out_ep = audio->out_ep;
#endif
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	struct usb_ep *in_ep  = audio->in_ep;
#endif

	DBG ("intf %d, alt %d\n", intf, alt);

	switch (intf) {
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
		case F_AUDIO_AS_INTERFACE_DOWNSTREAM:
			opts = container_of(f->fi, struct f_uac1_opts, func_inst);
			req_count = opts->req_count;
			info = &audio->downstream;
			if (alt == 1) {
				err = config_ep_by_speed(cdev->gadget, f, out_ep);
				if (err)
					return err;

				uac_core_playback_start_callback();
				usb_ep_enable(out_ep);
				/*
				 * allocate a bunch of read buffers
				 * and queue them all at once.
				 */
				for (i = 0; i < req_count && err == 0; i++) {
					req = usb_ep_alloc_request(out_ep);
					if (req) {
						if (sizeof (ds_req_buff[i]) < info->psize)
							ERROR ("%s, ds request buff size is too small. %d/%d\n", __func__, sizeof (ds_req_buff[i]), info->psize);

						req->buf = ds_req_buff[i];
						memset (req->buf, 0, info->psize);
						req->length = info->psize;
						req->context = audio;
						req->complete = f_audio_complete;
						info->req[i] = req;
						err = usb_ep_queue(out_ep, req);
						if (err)
							ERROR("%s queue req: %d\n", out_ep->name, err);
					} else {
						err = -ENOMEM;
						ERROR("%s ep alloc req failed\n", __func__);
					}
				}

				if (our_callback_ops.callback_ops.notify_callback)
					our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM, USB_FUNC_STATUS_ENABLE);
			} else {
				uac_core_playback_stop_callback();
				free_ep(info, out_ep);

				if (our_callback_ops.callback_ops.notify_callback)
					our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM, USB_FUNC_STATUS_DISABLE);
			}
			break;
#endif
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
		case F_AUDIO_AS_INTERFACE_UPSTREAM:
			opts = container_of(f->fi, struct f_uac1_opts, func_inst);
			req_count = opts->req_count;
			info = &audio->upstream;

			if (alt == 1) {
				err = config_ep_by_speed(cdev->gadget, f, in_ep);
				if (err)
					return err;

				uac_core_capture_start_callback();
				uac_up_left_pkt_num = 0;
				last_xfer_size  = 0;
				first_xfer      = 1;
				AudioBuffConsumeStart(our_callback_ops.callback_ops.ain_buff_handle);
				usb_ep_enable(in_ep);

				/*
				 * allocate a bunch of read buffers
				 * and queue them all at once.
				 */
				for (i = 0; i < req_count && err == 0; i++) {
					req = usb_ep_alloc_request(in_ep);
					if (req) {
						if (sizeof (us_req_buff[i]) < info->psize)
							ERROR ("%s, us request buff size is too small. %d/%d\n", __func__, sizeof (us_req_buff[i]), info->psize);

						req->buf = us_req_buff[i];
						memset (req->buf, 0, info->psize);
						req->length = info->psize;
						req->context = audio;
						req->complete = f_audio_complete;
						info->req[i] = req;
						err = usb_ep_queue(in_ep, req);
						if (err)
							ERROR("%s queue req: %d\n", in_ep->name, err);
					} else {
						err = -ENOMEM;
						ERROR ("%s ep alloc req failed\n", __func__);
					}
				}

				if (our_callback_ops.callback_ops.notify_callback)
					our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM, USB_FUNC_STATUS_ENABLE);
			} else{
				uac_core_capture_stop_callback();
				AudioBuffConsumeStop(our_callback_ops.callback_ops.ain_buff_handle);
				free_ep(info, in_ep);

				if (our_callback_ops.callback_ops.notify_callback)
					our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM, USB_FUNC_STATUS_DISABLE);
			}
			break;
#endif
		default:
			DBG ("%s invalid intf : %d\n", __func__, intf);
			break;
	}

	return err;
}

static void f_audio_disable(struct usb_function *f)
{
#if defined(CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK) || defined(CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE)
	struct f_audio *audio = func_to_audio(f);
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	struct usb_ep *out_ep = audio->out_ep;
#endif
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	struct usb_ep *in_ep  = audio->in_ep;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	uac_core_capture_stop_callback();
	AudioBuffConsumeStop(our_callback_ops.callback_ops.ain_buff_handle);
	free_ep(&audio->upstream, in_ep);

	if (our_callback_ops.callback_ops.notify_callback)
		our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM, USB_FUNC_STATUS_DISABLE);
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	uac_core_playback_stop_callback();
	free_ep(&audio->downstream, out_ep);

	if (our_callback_ops.callback_ops.notify_callback)
		our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM, USB_FUNC_STATUS_DISABLE);
#endif

	return;
}

/*-------------------------------------------------------------------------*/

static unsigned short create_channel_bitmap(int channel_num)
{
	unsigned short ret = 0;
	int i;

	for (i = 0; i < channel_num && i < 16; i++)
		ret |= (1 << i);

	return ret;
}

static void f_audio_build_desc(struct f_audio *audio)
{
#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
	u8 *sam_freq;
	int rate;
	int i;
#endif

	// down stream
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	/* Set channel numbers */
	input_terminal_desc.bNrChannels = ds_channel_num;
	input_terminal_desc.wChannelConfig = create_channel_bitmap(ds_channel_num);

	for (i = 1; i <= ds_channel_num; i++)
		feature_unit_desc_playback.bmaControls[i] = UAC1_FU_VOLUME;

	as_type_i_desc.bNrChannels = ds_channel_num;

	/* Set sample rates */
	rate = ds_sample_rate;
	sam_freq = as_type_i_desc.tSamFreq[0];
	memcpy(sam_freq, &rate, 3);
#endif

	// up stream
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	/* Set channel numbers */
	ain_input_terminal_desc.bNrChannels = us_channel_num;
	ain_input_terminal_desc.wChannelConfig = create_channel_bitmap(us_channel_num);

	for (i = 1; i <= us_channel_num; i++)
		feature_unit_desc_capture.bmaControls[i] = UAC1_FU_VOLUME;

	as_in_type_i_desc.bNrChannels = us_channel_num;

	/* Set sample rates */
	rate = us_sample_rate;
	sam_freq = as_in_type_i_desc.tSamFreq[0];
	memcpy(sam_freq, &rate, 3);
#endif

	return;
}

#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
static void set_ep_max_packet_size(struct usb_endpoint_descriptor *ep_desc,
        bool is_downstream)
{
    int channel_num, srate, ssize;
    u16 max_packet_size;

	if (is_downstream) {
		channel_num = ds_channel_num;
		srate       = ds_sample_rate;
		ssize       = ds_bit_width;
	} else {
		channel_num = us_channel_num;
		srate       = us_sample_rate;
		ssize       = us_bit_width;
	}

    max_packet_size = channel_num * ssize *
        DIV_ROUND_UP(srate, FACTOR / (1 << (ep_desc->bInterval - 1)));
    ep_desc->wMaxPacketSize = cpu_to_le16(min_t(u16, max_packet_size,
                le16_to_cpu(ep_desc->wMaxPacketSize)));

    // uac 上行 maxpktsize 大小翻倍，用于重传
	if (!is_downstream)
		ep_desc->wMaxPacketSize *= 2;

    return ;
}
#endif

/* audio function driver setup/binding */
static int f_audio_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct usb_composite_dev *cdev = c->cdev;
	struct f_audio		*audio = func_to_audio(f);
	struct usb_string	*us;
	int			status;
	struct f_uac1_opts	*audio_opts;

#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
	struct usb_ep		*ep = NULL;
#endif

	audio_opts = container_of(f->fi, struct f_uac1_opts, func_inst);
	audio->card.gadget = c->cdev->gadget;

	/* set up ASLA audio devices */
	if (!audio_opts->bound) {
		status = gaudio_setup(&audio->card);
		if (status < 0)
			return status;
		audio_opts->bound = true;
	}

	memset (&audio->downstream, 0, sizeof(audio->downstream));
	memset (&audio->upstream,   0, sizeof(audio->upstream));
	audio->downstream.type = UAC_STREAM_DOWN;
	audio->upstream.type   = UAC_STREAM_UP;

	memset (ds_req_buff, 0x0, sizeof(ds_req_buff[0]));
	memset (us_req_buff, 0x0, sizeof(us_req_buff[0]));

	us = usb_gstrings_attach(cdev, uac1_strings, ARRAY_SIZE(strings_uac1));
	if (!us)
		return -EINVAL;

	ac_interface_desc.iInterface = us[STR_AC_IF].id;

#if 0
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	input_terminal_desc.iTerminal = us[STR_INPUT_TERMINAL].id;
	input_terminal_desc.iChannelNames = us[STR_INPUT_TERMINAL_CH_NAMES].id;
	feature_unit_desc_playback.iFeature = us[STR_FEAT_DESC_0].id;
	output_terminal_desc.iTerminal = us[STR_OUTPUT_TERMINAL].id;
	as_interface_alt_0_desc.iInterface = us[STR_AS_IF_ALT0].id;
	as_interface_alt_1_desc.iInterface = us[STR_AS_IF_ALT1].id;
#endif
#endif

	f_audio_build_desc(audio);

	/* allocate instance-specific interface IDs, and patch descriptors */
	status = usb_interface_id(c, f);
	if (status < 0)
		goto fail;
	ac_interface_desc.bInterfaceNumber = status;

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	status = usb_interface_id(c, f);
	if (status < 0)
		goto fail;
	as_interface_alt_0_desc.bInterfaceNumber = status;
	as_interface_alt_1_desc.bInterfaceNumber = status;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	status = usb_interface_id(c, f);
	if (status < 0)
		goto fail;
	as_in_interface_alt_0_desc.bInterfaceNumber = status;
	as_in_interface_alt_1_desc.bInterfaceNumber = status;
#endif

	status = -ENODEV;

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	/* allocate instance-specific endpoints */
	ep = usb_ep_autoconfig(cdev->gadget, &as_out_ep_desc);
	if (!ep)
		goto fail;
	audio->out_ep = ep;
	audio->out_ep->desc = &as_out_ep_desc;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	/* allocate instance-specific endpoints */
	ep = usb_ep_autoconfig(cdev->gadget, &as_in_ep_desc);
	if (!ep)
		goto fail;
	audio->in_ep = ep;
	audio->in_ep->desc = &as_in_ep_desc;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	set_ep_max_packet_size(&as_out_ep_desc, true);
	audio->downstream.max_psize = as_out_ep_desc.wMaxPacketSize;
	audio->downstream.psize = audio->downstream.max_psize;
	printf ("downstream : max packet size : %d, packet size : %d\n", audio->downstream.max_psize, audio->upstream.psize);
#endif
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	set_ep_max_packet_size(&as_in_ep_desc, false);
	audio->upstream.max_psize = as_in_ep_desc.wMaxPacketSize;
	audio->upstream.psize = DIV_ROUND_UP(us_sample_rate, FACTOR / (1 << (as_in_ep_desc.bInterval - 1))) *
		us_bit_width * us_channel_num;
	printf ("upstream : max packet size : %d, packet size : %d\n", audio->upstream.max_psize, audio->upstream.psize);
#endif

	status = -ENOMEM;

	/* copy descriptors, and track endpoint copies */
	status = usb_assign_descriptors(f, f_audio_desc, f_audio_desc, NULL);
	if (status)
		goto fail;
	return 0;

fail:
	//gaudio_cleanup(&audio->card);
	return status;
}

/*-------------------------------------------------------------------------*/

#if defined (CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE) || defined (CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK)
static int generic_set_cmd(struct usb_audio_control *con, u8 cmd, int value)
{
	con->data[cmd] = value;

	return 0;
}

static int generic_get_cmd(struct usb_audio_control *con, u8 cmd)
{
	return con->data[cmd];
}
#endif

/* Todo: add more control selecotor dynamically */
static int control_selector_init(struct f_audio *audio)
{
	INIT_LIST_HEAD(&audio->cs);

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	list_add(&feature_unit_playback.list, &audio->cs);

	INIT_LIST_HEAD(&feature_unit_playback.control);
	list_add(&mute_control_playback.list, &feature_unit_playback.control);
	list_add(&volume_control_playback.list, &feature_unit_playback.control);

	mute_control_playback.data[1] = 0;
	volume_control_playback.data[UAC__CUR] = PLAYBACK_VOLUME_DEFAULT;
	volume_control_playback.data[UAC__MIN] = PLAYBACK_VOLUME_MIN;
	volume_control_playback.data[UAC__MAX] = PLAYBACK_VOLUME_MAX;
	volume_control_playback.data[UAC__RES] = PLAYBACK_VOLUME_RES;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	list_add(&feature_unit_capture.list, &audio->cs);

	INIT_LIST_HEAD(&feature_unit_capture.control);
	list_add(&mute_control_capture.list, &feature_unit_capture.control);
	list_add(&volume_control_capture.list, &feature_unit_capture.control);
	list_add(&agc_control_capture.list, &feature_unit_capture.control);

	mute_control_capture.data[1] = 0;
	volume_control_capture.data[UAC__CUR] = CAPTURE_VOLUME_DEFAULT;
	volume_control_capture.data[UAC__MIN] = CAPTURE_VOLUME_MIN;
	volume_control_capture.data[UAC__MAX] = CAPTURE_VOLUME_MAX;
	volume_control_capture.data[UAC__RES] = CAPTURE_VOLUME_RES;
#endif

	return 0;
}

static void f_audio_free_inst(struct usb_function_instance *f)
{
#if 0
	struct f_uac1_opts *opts;
	opts = container_of(f, struct f_uac1_opts, func_inst);
#endif
}

static struct f_uac1_opts f_uac1_opt_;

static struct usb_function_instance *f_audio_alloc_inst(void)
{
	struct f_uac1_opts *opts;

	opts = &f_uac1_opt_;

	opts->func_inst.free_func_inst = f_audio_free_inst;

	opts->req_count = UAC1_REQ_COUNT;

	return &opts->func_inst;
}

static void f_audio_free(struct usb_function *f)
{
#if 0
	//struct f_audio *audio = func_to_audio(f);
	struct f_uac1_opts *opts;

	//gaudio_cleanup(&audio->card);
	opts = container_of(f->fi, struct f_uac1_opts, func_inst);
#endif
}


static void f_audio_suspend(struct usb_function *func)
{
	f_audio_disable(func);

    return ;
}

static void f_audio_resume(struct usb_function *func)
{
    func = func;

    return ;
}

static void f_audio_unbind(struct usb_configuration *c, struct usb_function *f)
{
	usb_free_all_descriptors(f);
}

static struct usb_function *f_audio_alloc(struct usb_function_instance *fi)
{
	struct f_audio *audio;
	//struct f_uac1_opts *opts;

	/* allocate and initialize one new instance */
	audio = &f_audio_;
	memset(audio, 0, sizeof(struct f_audio));

	audio->card.func.name = "g_audio";

	//opts = container_of(fi, struct f_uac1_opts, func_inst);
	INIT_LIST_HEAD(&audio->play_queue);

	audio->card.func.bind = f_audio_bind;
	audio->card.func.unbind = f_audio_unbind;
	audio->card.func.set_alt = f_audio_set_alt;
	audio->card.func.setup = f_audio_setup;
	audio->card.func.disable = f_audio_disable;
	audio->card.func.free_func = f_audio_free;

	audio->card.func.suspend = f_audio_suspend;
	audio->card.func.resume = f_audio_resume;

	control_selector_init(audio);

	return &audio->card.func;
}

static struct usb_function_driver uac1_usb_func = {
	.name = UAC1_NAME,
	.alloc_inst = f_audio_alloc_inst,
	.alloc_func = f_audio_alloc,
};

/*-------------------------------------------------------------------------*/

int audio_do_config(struct usb_configuration *c)
{
	int status;

	f_uac1 = usb_get_function(fi_uac1);
	if (!f_uac1) {
		status = -EINVAL;
		return status;
	}

	status = usb_add_function(c, f_uac1);
	if (status < 0) {
		usb_put_function(f_uac1);
		return status;
	}

	return 0;
}

/*-------------------------------------------------------------------------*/

int audio_bind(struct usb_composite_dev *cdev)
{
	struct f_uac1_opts	*uac1_opts;
	//int			status;

	fi_uac1 = usb_get_function_instance(UAC1_NAME);
	if (!fi_uac1)
		return -EINVAL;

	uac1_opts = container_of(fi_uac1, struct f_uac1_opts, func_inst);

	uac1_opts->req_count = UAC1_REQ_COUNT;

#if 0
	uac1_opts->ds_channel_num = ds_channel_num;
	uac1_opts->ds_srate = ds_sample_rate;
	uac1_opts->ds_ssize = ds_bit_width;
	uac1_opts->us_channel_num = us_channel_num;
	uac1_opts->us_srate = us_sample_rate;
	uac1_opts->us_ssize = us_bit_width;
#endif

	return 0;
}

int audio_unbind(struct usb_composite_dev *cdev)
{
	if (!f_uac1)
		usb_put_function(f_uac1);
	if (!fi_uac1)
		usb_put_function_instance(fi_uac1);

	return 0;
}

//------------------------------ API ------------------------------------------

#ifdef CONFIG_VSP_UAC_VERSION_1
int UacGetControlInfo(struct uac_control_info *info)
{
	if (!info)
		return -1;

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	info->volume_ds_cur = volume_control_playback.data[UAC__CUR];
	info->volume_ds_min = volume_control_playback.data[UAC__MIN];
	info->volume_ds_max = volume_control_playback.data[UAC__MAX];
	info->mute_ds       = mute_control_playback.data[1];
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	info->volume_us_cur = volume_control_capture.data[UAC__CUR];
	info->volume_us_min = volume_control_capture.data[UAC__MIN];
	info->volume_us_max = volume_control_capture.data[UAC__MAX];
	info->mute_us       = mute_control_capture.data[1];
#endif

	return 0;
}
#endif

int Uac1DoConfig(struct usb_configuration *c)
{
	return audio_do_config(c);
}

int Uac1Bind(struct usb_composite_dev *cdev)
{
	int ret = 0;

	ret = audio_bind(cdev);
	if (ret != 0)
		return ret;

	return 0;
}

int Uac1Unbind(struct usb_composite_dev *cdev)
{
	return audio_unbind(cdev);
}

static int uac_init_pre(void)
{
#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_CAPTURE
	if (UAC_CAPTURE_CHANNEL_NUM != us_channel_num)
		return -1;
#endif

#ifdef CONFIG_MCU_UDC_UAC1_ENABLE_PLAYBACK
	if (UAC_PLAYBACK_CHANNEL_NUM != ds_channel_num)
		return -1;
#endif

	return 0;
}

int Uac1Init(const UAC2_CHANNEL_CONFIG *audio_in_config,
        const UAC2_CHANNEL_CONFIG *audio_out_config,
        const UAC2_DEVICE_DESCRIPTION *device_description,
        const UAC2_CALLBACKS *callbacks,
        void *private_data)
{
    if (!audio_in_config || !audio_out_config) {
        ERROR ("%s, invalid uac config\n", __func__);
        return -1;
    }

    if (!device_description) {
        ERROR ("%s, invalid uac device description\n", __func__);
        return -1;
    }

    us_channel_num = num_channels(audio_in_config->channel_mask);
    us_sample_rate = audio_in_config->sample_rate;

    ds_channel_num = num_channels(audio_out_config->channel_mask);
    ds_sample_rate = audio_out_config->sample_rate;

    our_callback_ops.callback_ops = *callbacks;
    our_callback_ops.private_data = private_data;

    if (uac_init_pre()) {
        ERROR ("%s init failed\n", __func__);
        return -1;
    }

    return usb_function_register(&uac1_usb_func);
}

int Uac1Done(void)
{
    usb_function_unregister(&uac1_usb_func);
    return 0;
}
