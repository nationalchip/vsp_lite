/*
 * u_uac1.h -- interface to USB gadget "ALSA AUDIO" utilities
 *
 * Copyright (C) 2008 Bryan Wu <cooloney@kernel.org>
 * Copyright (C) 2008 Analog Devices, Inc
 *
 * Enter bugs at http://blackfin.uclinux.org/
 *
 * Licensed under the GPL-2 or later.
 */

#ifndef __U_AUDIO_H
#define __U_AUDIO_H

//#include <linux/device.h>
//#include <linux/err.h>
//#include <linux/usb/audio.h>
#include "../include/composite.h"

//#include <sound/core.h>
//#include <sound/pcm.h>
//#include <sound/pcm_params.h>

#define FILE_PCM_PLAYBACK	"/dev/snd/pcmC0D0p"
#define FILE_PCM_CAPTURE	"/dev/snd/pcmC0D0c"
#define FILE_CONTROL		"/dev/snd/controlC0"

#define UAC1_IN_EP_MAX_PACKET_SIZE	32
#define UAC1_OUT_EP_MAX_PACKET_SIZE	200
#define UAC1_REQ_COUNT			1 //256
#define UAC1_AUDIO_BUF_SIZE		48000

/*
 * This represents the USB side of an audio card device, managed by a USB
 * function which provides control and stream interfaces.
 */

struct gaudio_snd_dev {
	struct gaudio			*card;
	//struct file			*filp;
	//struct snd_pcm_substream	*substream;
	int				access;
	int				format;
	int				channels;
	int				rate;
};

struct gaudio {
	struct usb_function		func;
	struct usb_gadget		*gadget;

	/* ALSA sound device interfaces */
	struct gaudio_snd_dev		control;
	struct gaudio_snd_dev		playback;
	struct gaudio_snd_dev		capture;

	/* TODO */
};

struct f_uac1_opts {
	struct usb_function_instance	func_inst;
	int				req_count;

	unsigned int    ds_channel_num;
	unsigned int    ds_srate;
	unsigned int    ds_ssize;

	unsigned int    us_channel_num;
	unsigned int    us_srate;
	unsigned int    us_ssize;

	unsigned int    bound;
//	int				refcnt;
};

int gaudio_setup(struct gaudio *card);
void gaudio_cleanup(struct gaudio *the_card);

size_t u_audio_playback(struct gaudio *card, void *buf, size_t count);
int u_audio_get_playback_channels(struct gaudio *card);
int u_audio_get_playback_rate(struct gaudio *card);

#endif /* __U_AUDIO_H */
