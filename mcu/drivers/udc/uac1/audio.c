/*
 * audio.c -- Audio gadget driver
 *
 * Copyright (C) 2008 Bryan Wu <cooloney@kernel.org>
 * Copyright (C) 2008 Analog Devices, Inc
 *
 * Enter bugs at http://blackfin.uclinux.org/
 *
 * Licensed under the GPL-2 or later.
 */

/* #define VERBOSE_DEBUG */

//#include <linux/kernel.h>
//#include <linux/module.h>
#include "../include/composite.h"
#include "u_uac1.h"

static int req_count = UAC1_REQ_COUNT;
static struct usb_function_instance *fi_uac1;
static struct usb_function *f_uac1;

/*-------------------------------------------------------------------------*/

int audio_do_config(struct usb_configuration *c)
{
	int status;

	f_uac1 = usb_get_function(fi_uac1);
	if (!f_uac1) {
		status = -EINVAL;
		return status;
	}

	status = usb_add_function(c, f_uac1);
	if (status < 0) {
		usb_put_function(f_uac1);
		return status;
	}

	return 0;
}

/*-------------------------------------------------------------------------*/

int audio_bind(struct usb_composite_dev *cdev)
{
	struct f_uac1_opts	*uac1_opts;
	//int			status;

	fi_uac1 = usb_get_function_instance("UAC 1.0");
	if (!fi_uac1)
		return -EINVAL;

	uac1_opts = container_of(fi_uac1, struct f_uac1_opts, func_inst);

	uac1_opts->req_count = req_count;

	return 0;
}

int audio_unbind(struct usb_composite_dev *cdev)
{
	if (!f_uac1)
		usb_put_function(f_uac1);
	if (!fi_uac1)
		usb_put_function_instance(fi_uac1);

	return 0;
}