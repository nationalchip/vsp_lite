/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * ms_gx.c : usb mass storage
 *
 */
#include <stdio.h>
#include <common.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include <driver/delay.h>

#include <board_config.h>

#include "f_mass_storage.h"
#include "driver/flash.h"

#define FSG_DRIVER_DESC        "Mass Storage Function"
#define FSG_DRIVER_VERSION    "2009/09/11"

#define DRIVER_DESC        "Nationalchip Mass Storage"
#define DRIVER_VERSION        "2018/07/02"

#if 0
#define DBG(fmt, ...) printf (fmt, ##__VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif

struct fsg_dev;
struct fsg_common;

struct fsg_common {
    struct usb_gadget             *gadget;
    struct usb_composite_dev      *cdev;
    struct fsg_dev                *fsg;
    struct fsg_dev                *new_fsg;

    struct usb_ep                 *ep0;        /* Copy of gadget->ep0 */
    struct usb_request            *ep0req;    /* Copy of cdev->req */
    unsigned int                   ep0_req_tag;

    struct fsg_buffhd             *next_buffhd_to_fill;
    struct fsg_buffhd             *next_buffhd_to_drain;
    struct fsg_buffhd             *buffhds;
    unsigned int                   fsg_num_buffers;

    int                            cmnd_size;
    u8                             cmnd[MAX_COMMAND_SIZE];

    unsigned int                   bulk_out_maxpacket;
    enum fsg_state                 state;        /* For exception handling */
    unsigned int                   exception_req_tag;

    enum data_direction            data_dir;
    u32                            data_size;
    u32                            data_size_from_cmnd;
    u32                            tag;
    u32                            residue;
    u32                            usb_amount_left;

    unsigned int                   can_stall:1;
    unsigned int                   free_storage_on_release:1;
    unsigned int                   phase_error:1;
    unsigned int                   short_packet_received:1;
    unsigned int                   bad_lun_okay:1;
    unsigned int                   running:1;
    unsigned int                   sysfs:1;

    /* Callback functions. */
    const struct fsg_operations    *ops;
    /* Gadget's private data. */
    void                           *private_data;

    /*
     * Vendor (8 chars), product (16 chars), release (4
     * hexadecimal digits) and NUL byte
     */
    char                            inquiry_string[8 + 16 + 4 + 1];
    loff_t                          num_sectors;
    unsigned int                    blkbits; /* Bits of logical block size
                                                of bound block device */
    unsigned int                    blksize; /* logical block size of bound block device */

};

struct fsg_dev {
    struct usb_function   function;
    struct usb_gadget    *gadget;    /* Copy of cdev->gadget */
    struct fsg_common    *common;

    u16                   interface_number;

    unsigned int          bulk_in_enabled:1;
    unsigned int          bulk_out_enabled:1;

    unsigned long         atomic_bitflags;
#define IGNORE_BULK_OUT        0

    struct usb_ep        *bulk_in;
    struct usb_ep        *bulk_out;
};

/*
 * USB 2.0 devices need to expose both high speed and full speed
 * descriptors, unless they only run at full speed.
 * That means alternate endpoint descriptors (bigger packets)
 * and a "device qualifier" ... plus more construction options
 * for the configuration descriptor.
 */

/*
 * Three full-speed endpoint descriptors: bulk-in, bulk-out, and
 * interrupt-in.
 */

static struct usb_interface_descriptor fsg_intf_desc = {
    .bLength            =  sizeof fsg_intf_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bNumEndpoints      =  2,        /* Adjusted during fsg_bind() */
    .bInterfaceClass    =  USB_CLASS_MASS_STORAGE,
    .bInterfaceSubClass =  USB_SC_SCSI,    /* Adjusted during fsg_bind() */
    .bInterfaceProtocol =  USB_PR_BULK,    /* Adjusted during fsg_bind() */
    .iInterface         =  FSG_STRING_INTERFACE,
};

static struct usb_endpoint_descriptor fsg_fs_bulk_in_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_IN,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
    /* wMaxPacketSize set by autoconfiguration */
};

static struct usb_endpoint_descriptor fsg_fs_bulk_out_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_OUT,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
    /* wMaxPacketSize set by autoconfiguration */
};

static struct usb_descriptor_header *fsg_fs_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_fs_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_fs_bulk_out_desc,
    NULL,
};

static struct usb_endpoint_descriptor fsg_hs_bulk_in_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
    .bmAttributes    =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize  =  cpu_to_le16(512),
};

static struct usb_endpoint_descriptor fsg_hs_bulk_out_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,

    /* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
    .bmAttributes    =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize  =  cpu_to_le16(512),
    .bInterval       =  1,    /* NAK every 1 uframe */
};

static struct usb_descriptor_header *fsg_hs_function[] = {
    (struct usb_descriptor_header *) &fsg_intf_desc,
    (struct usb_descriptor_header *) &fsg_hs_bulk_in_desc,
    (struct usb_descriptor_header *) &fsg_hs_bulk_out_desc,
    NULL,
};

static struct fsg_module_parameters mod_data = {
    .file[0]      =  "fuke_dev",
    .removable[0] =  1,
    .luns         =  1,
    .stall        =  0// 1
};

/* structure for GET_INFO CMD */
struct system_info {
    __le32 info_version;
    __le32 len;
};

#define __round_mask(x, y) ((__typeof__(x))((y)-1))
#define round_up(x, y) ((((x)-1) | __round_mask(x, y))+1)
#define round_down(x, y) ((x) & ~__round_mask(x, y))

//  global data structure start

static struct usb_function_instance *fi_msg;
static struct usb_function *f_msg;
static struct fsg_common *g_fsg_common;
static struct fsg_common g_common;

#define G_BUFFHD_NUM 2
static struct fsg_buffhd g_buffhds[G_BUFFHD_NUM];
#define FSG_BUF_LEN  (4*1024)
static u8 g_buffhd_buf0[FSG_BUF_LEN] __attribute__ ((aligned(64)));
static u8 g_buffhd_buf1[FSG_BUF_LEN] __attribute__ ((aligned(64)));

static struct fsg_opts g_opts;
static struct fsg_dev  g_fsg;

typedef struct flash_info
{
    struct flash_dev *dev;
    u32               partition_blks;
    u32               page_size;
    u32               page_bit;
    u32               blk_size;
    u32               blk_bit;
    u32               partition_start;
    u32               partition_len;
    u32               patition_pages;
} FLASH;

static FLASH flash;
static int   read_only = 0;

static inline int __fsg_is_set(struct fsg_common *common,
        const char *func, unsigned line)
{
    if (common->fsg)
        return 1;
    DBG("common->fsg is NULL in %s at %u\n", func, line);

    return 0;
}

#define fsg_is_set(common) likely(__fsg_is_set(common, __func__, __LINE__))

static inline struct fsg_dev *fsg_from_func(struct usb_function *f)
{
    return container_of(f, struct fsg_dev, function);
}

/* check if fsg_num_buffers is within a valid range */
static inline int fsg_num_buffers_validate(unsigned int fsg_num_buffers)
{
#define FSG_MAX_NUM_BUFFERS    32

    if (fsg_num_buffers >= 2 && fsg_num_buffers <= FSG_MAX_NUM_BUFFERS)
        return 0;
    DBG("fsg_num_buffers %u is out of range (%d to %d)\n",
            fsg_num_buffers, 2, FSG_MAX_NUM_BUFFERS);
    return -EINVAL;
}

static void _fsg_common_free_buffers(struct fsg_buffhd *buffhds, unsigned n)
{
#if 0
    if (buffhds) {
        struct fsg_buffhd *bh = buffhds;
        while (n--) {
            free(bh->buf);
            ++bh;
        }
        free(buffhds);
    }
#endif

    return ;
}

static void fsg_common_free_buffers(struct fsg_common *common)
{
    _fsg_common_free_buffers(common->buffhds, common->fsg_num_buffers);
    common->buffhds = NULL;

    return ;
}

static int fsg_common_set_num_buffers(struct fsg_common *common, unsigned int n)
{
    struct fsg_buffhd *bh, *buffhds;
    int rc;

    rc = fsg_num_buffers_validate(n);
    if (rc != 0)
        return rc;

    buffhds = g_buffhds;
    bh = buffhds;
    bh->buf = g_buffhd_buf0;
    bh->next = bh + 1;
    ++bh;
    bh->buf = g_buffhd_buf1;
    bh->next = buffhds;

    _fsg_common_free_buffers(common->buffhds, common->fsg_num_buffers);
    common->fsg_num_buffers = n;
    common->buffhds = buffhds;

    return 0;
}

static void set_bulk_out_req_length(struct fsg_common *common,
        struct fsg_buffhd *bh, unsigned int length)
{
    unsigned int    rem = 0;

    bh->bulk_out_intended_length = length;

    rem = length % common->bulk_out_maxpacket;

    if (rem > 0)
        length += common->bulk_out_maxpacket - rem;
    bh->outreq->length = length;

    return ;
}

static int fsg_set_halt(struct fsg_dev *fsg, struct usb_ep *ep)
{
#if 0
    const char    *name;

    if (ep == fsg->bulk_in)
        name = "bulk-in";
    else if (ep == fsg->bulk_out)
        name = "bulk-out";
    else
        name = ep->name;
    DBG("%s set halt\n", name);
#endif

    return usb_ep_set_halt(ep);
}

static int wedge_bulk_in_endpoint(struct fsg_dev *fsg)
{
    int    rc;

    printf ("bulk-in set wedge\n");

    rc = usb_ep_set_wedge(fsg->bulk_in);
    if (rc == -EAGAIN)
        printf ("delayed bulk-in endpoint wedge\n");
    while (rc != 0) {
        if (rc != -EAGAIN) {
            DBG("usb_ep_set_wedge -> %d\n", rc);
            rc = 0;
            break;
        }

        /* Wait for a short time and then try again */
        udelay(100);
        rc = usb_ep_set_wedge(fsg->bulk_in);
    }
    return rc;
}

#if 0
static int ep0_queue(struct fsg_common *common)
{
    int    rc;

    rc = usb_ep_queue(common->ep0, common->ep0req);
    common->ep0->driver_data = common;
    if (rc != 0 && rc != -ESHUTDOWN) {
        /* We can't do much more than wait for a reset */
        DBG( "error in submission: %s --> %d\n",
                common->ep0->name, rc);
    }
    return rc;
}
#endif

static void bulk_in_complete(struct usb_ep *ep, struct usb_request *req)
{
    //struct fsg_common    *common = ep->driver_data;
    struct fsg_buffhd    *bh = req->context;

    if (req->status || req->actual != req->length)
        DBG ("%s --> %d, %u/%u\n", __func__, req->status, req->actual, req->length);

    if (req->status == -ECONNRESET)        /* Request was cancelled */
        usb_ep_fifo_flush(ep);

    /* Hold the lock while we update the request and buffer states */
    bh->inreq_busy = 0;
    bh->state = BUF_STATE_EMPTY;

    return ;
}

static void bulk_out_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct fsg_buffhd    *bh = req->context;
    //printf("bulk out complete\n");
    if (req->status || req->actual != bh->bulk_out_intended_length)
        DBG ("%s --> %d, %u/%u\n", __func__,req->status, req->actual, bh->bulk_out_intended_length);

    if (req->status == -ECONNRESET)        /* Request was cancelled */
        usb_ep_fifo_flush(ep);

    /* Hold the lock while we update the request and buffer states */
    bh->outreq_busy = 0;
    bh->state = BUF_STATE_FULL;

    return ;
}

static void start_transfer(struct fsg_dev *fsg, struct usb_ep *ep,
        struct usb_request *req, int *pbusy,
        enum fsg_buffer_state *state)
{
    int    rc;

    if (ep == fsg->bulk_in)
        DBG("bulk-in buf(%p),len(0x%x)\n", req->buf, req->length);

    *pbusy = 1;
    *state = BUF_STATE_BUSY;

    rc = usb_ep_queue(ep, req);
    if (rc == 0)
        return;  /* All good, we're done */

    *pbusy = 0;
    *state = BUF_STATE_EMPTY;

    /*
     * Note: currently the net2280 driver fails zero-length
     * submissions if DMA is enabled.
     */
    if (rc != -ESHUTDOWN && !(rc == -EOPNOTSUPP && req->length == 0))
        printf ("error in submission: %s --> %d\n", ep->name, rc);

    return ;
}

static bool start_in_transfer(struct fsg_common *common, struct fsg_buffhd *bh)
{
    if (!fsg_is_set(common) || !bh->inreq->buf) {
        DBG("%s, empty buffer point\n", __func__);
        return false;
    }

    if (bh->inreq->length == 0)
        return true;

    start_transfer(common->fsg, common->fsg->bulk_in,
            bh->inreq, &bh->inreq_busy, &bh->state);

    return true;
}

static bool start_out_transfer(struct fsg_common *common, struct fsg_buffhd *bh)
{
    if (!fsg_is_set(common) || !bh->outreq->buf) {
        DBG("%s, empty buffer point\n", __func__);
        return false;
    }

    start_transfer(common->fsg, common->fsg->bulk_out,
            bh->outreq, &bh->outreq_busy, &bh->state);
    return true;
}

static int halt_bulk_in_endpoint(struct fsg_dev *fsg)
{
    int    rc;

    rc = fsg_set_halt(fsg, fsg->bulk_in);
    if (rc == -EAGAIN)
        printf ("delayed bulk-in endpoint halt\n");
    while (rc != 0) {
        if (rc != -EAGAIN) {
            DBG("usb_ep_set_halt -> %d\n", rc);
            rc = 0;
            break;
        }
#if 0
        /* Wait for a short time and then try again */
        if (msleep_interruptible(100) != 0)
            return -EINTR;
#endif
        udelay(100);
        rc = usb_ep_set_halt(fsg->bulk_in);
    }
    return rc;
}

static int ms_read(unsigned int addr, unsigned char *buf, unsigned int len)
{
    int ret;
    ret = spi_flash_logic_read(flash.dev, flash.partition_start, addr, buf, len);
    if(ret == 0)
        ret = len;
    return ret;
}

#ifndef CONFIG_MCU_UDC_ENABLE_MS_RD_ONLY
static char temp_buf[128*1024];
#endif
static int ms_write(unsigned int addr, unsigned char *buf, unsigned int len)
{
    int ret;
#ifdef CONFIG_MCU_UDC_ENABLE_MS_RD_ONLY
    return len;
#else
    int blk_start_nm,blk_end_nm;
    int blk_start_addr,blk_end_addr;
    int blk_start_offset,blk_end_offset;
    unsigned int phy_blk_start_addr, phy_blk_end_addr;
    //int page_num_in_block;

    if(read_only == 1)
        return len;

    //page_num_in_block = flash.blk_size/flash.page_size;

    //because the data len won't beyond 1 blks, so we just get the start blk and end blk
    blk_start_nm = addr/flash.blk_size;
    blk_start_addr = blk_start_nm * flash.blk_size;
    blk_start_offset = addr - blk_start_addr;//data offset in a block

    //printf("blk_start_addr is %x\n",blk_start_addr);

    blk_end_nm = (addr+len)/flash.blk_size;
    blk_end_addr = blk_end_nm * flash.blk_size;
    blk_end_offset = addr + len - blk_end_addr;
    if(blk_end_offset == 0)
        blk_end_nm -= 1;//end addr is on edge

    if(blk_start_nm == blk_end_nm) {
        //data in one block

        //printf("write one block\n");
        //read first block data into buf
        spi_flash_logic_read(flash.dev, flash.partition_start, blk_start_addr, temp_buf, flash.blk_size);

        //calc the phy start block
        spi_flash_calc_phy_offset(flash.dev, flash.partition_start, blk_start_addr, &phy_blk_start_addr);
        //printf("phy addr is %x\n",phy_blk_start_addr);
        //erase first block
        //printf("erase addr is %x\n",flash.partition_start+phy_blk_start_addr);
        spi_flash_erasedata(flash.dev,  flash.partition_start+phy_blk_start_addr, flash.blk_size);
        //cpy data into temp_buf
        memcpy(temp_buf+blk_start_offset, buf, len);
        //write data to flash
        spi_flash_logic_program(flash.dev, flash.partition_start, blk_start_addr, temp_buf, flash.blk_size);
    } else {
        //printf("write two blocks\n");

        //read first block data into buf
        spi_flash_logic_read(flash.dev, flash.partition_start, blk_start_addr, temp_buf, flash.blk_size);
        //calc the phy start block
        spi_flash_calc_phy_offset(flash.dev, flash.partition_start, blk_start_addr, &phy_blk_start_addr);
        //erase first block
        spi_flash_erasedata(flash.dev,  flash.partition_start+phy_blk_start_addr, flash.blk_size);
        //cpy data into temp_buf,
        memcpy(temp_buf+blk_start_offset, buf, blk_start_addr+flash.blk_size-addr);
        //write data to flash
        spi_flash_logic_program(flash.dev, flash.partition_start, blk_start_addr, temp_buf, flash.blk_size);

        //read second block data into buf
        spi_flash_logic_read(flash.dev, flash.partition_start, blk_end_addr, temp_buf, flash.blk_size);
        //calc the phy start block
        spi_flash_calc_phy_offset(flash.dev, flash.partition_start, blk_end_addr, &phy_blk_end_addr);
        //erase second block
        spi_flash_erasedata(flash.dev,  flash.partition_start+phy_blk_end_addr, flash.blk_size);
        //cpy data into temp_buf, first time cp data len is blk_start_addr+flash.blk_size-addr
        memcpy(temp_buf, buf+(blk_start_addr+flash.blk_size-addr), len-(blk_start_addr+flash.blk_size-addr));
        //write data to flash
        spi_flash_logic_program(flash.dev, flash.partition_start, blk_end_addr, temp_buf, flash.blk_size);
    }
#endif
    ret = len;
    return ret;
}

static int send_status(struct fsg_common *common)
{
    struct fsg_buffhd    *bh;
    struct bulk_cs_wrap    *csw;
    u8            status = US_BULK_STAT_OK;
    //printf("%s in line %d\n",__func__,__LINE__);
    /* Wait for the next buffer to become available */
    bh = common->next_buffhd_to_fill;
    //printf("next_buffhd_to_fill in send status is %x\n",common->next_buffhd_to_fill);
    while (bh->state != BUF_STATE_EMPTY) {
        udelay(100);
    }
    //printf("%s in line %d\n",__func__,__LINE__);
    /* Store and send the Bulk-only CSW */
    csw = (void *)bh->buf;

    csw->Signature = cpu_to_le32(US_BULK_CS_SIGN);
    csw->Tag = common->tag;
    csw->Residue = cpu_to_le32(common->residue);
    csw->Status = status;

    bh->inreq->length = US_BULK_CS_WRAP_LEN;
    bh->inreq->zero = 0;

    //printf("bh len is %d!!!!!!\n",bh->inreq->length);
    if (!start_in_transfer(common, bh))
        /* Don't know what to do if common->fsg is NULL */
        return -EIO;

    common->next_buffhd_to_fill = bh->next;
    return 0;
}

static int throw_away_data(struct fsg_common *common)
{
    struct fsg_buffhd   *bh;
    u32         amount;

    for (bh = common->next_buffhd_to_drain;
            bh->state != BUF_STATE_EMPTY || common->usb_amount_left > 0;
            bh = common->next_buffhd_to_drain) {

        /* Throw away the data in a filled buffer */
        if (bh->state == BUF_STATE_FULL) {
            bh->state = BUF_STATE_EMPTY;
            common->next_buffhd_to_drain = bh->next;

            /* A short packet or an error ends everything */
            if (bh->outreq->actual < bh->bulk_out_intended_length ||
                    bh->outreq->status != 0) {
                return -EINTR;
            }
            continue;
        }

        /* Try to submit another request if we need one */
        bh = common->next_buffhd_to_fill;
        if (bh->state == BUF_STATE_EMPTY
                && common->usb_amount_left > 0) {
            amount = min(common->usb_amount_left, FSG_BUFLEN);

            /*
             * Except at the end of the transfer, amount will be
             * equal to the buffer size, which is divisible by
             * the bulk-out maxpacket size.
             */
            set_bulk_out_req_length(common, bh, amount);
            if (!start_out_transfer(common, bh))
                /* Dunno what to do if common->fsg is NULL */
                return -EIO;
            common->next_buffhd_to_fill = bh->next;
            common->usb_amount_left -= amount;
            continue;
        }
    }

    return 0;
}

static int finish_reply(struct fsg_common *common)
{
    struct fsg_buffhd    *bh = common->next_buffhd_to_fill;
    int            rc = 0;

    //printf("finish_reply %d\n", __LINE__);
    //printf("common->data_dir is %d\n",common->data_dir);
    switch (common->data_dir) {
    case DATA_DIR_NONE:
        break;            /* Nothing to send */

        /*
         * If we don't know whether the host wants to read or write,
         * this must be CB or CBI with an unknown command.  We mustn't
         * try to send or receive any data.  So stall both bulk pipes
         * if we can and wait for a reset.
         */
    case DATA_DIR_UNKNOWN:
        if (!common->can_stall) {
            /* Nothing */
        } else if (fsg_is_set(common)) {
            fsg_set_halt(common->fsg, common->fsg->bulk_out);
            rc = halt_bulk_in_endpoint(common->fsg);
        } else {
            /* Don't know what to do if common->fsg is NULL */
            rc = -EIO;
        }
        break;
        /* All but the last buffer of data must have already been sent */
    case DATA_DIR_TO_HOST:
        //printf("common->data_size is %d\n",common->data_size);
        //printf("common->residue is %d\n",common->residue);
        if (common->data_size == 0) {
            /* Nothing to send */

            /* Don't know what to do if common->fsg is NULL */
        } else if (!fsg_is_set(common)) {
            rc = -EIO;

            /* If there's no residue, simply send the last buffer */
        } else if (common->residue == 0) {
            bh->inreq->zero = 0;
            if (!start_in_transfer(common, bh))
                return -EIO;
            common->next_buffhd_to_fill = bh->next;

            /*
             * For Bulk-only, mark the end of the data with a short
             * packet.  If we are allowed to stall, halt the bulk-in
             * endpoint.  (Note: This violates the Bulk-Only Transport
             * specification, which requires us to pad the data if we
             * don't halt the endpoint.  Presumably nobody will mind.)
             */
        } else {
            bh->inreq->zero = 1;
            if (!start_in_transfer(common, bh))
                rc = -EIO;
            common->next_buffhd_to_fill = bh->next;
            if (common->can_stall)
            {
                rc = halt_bulk_in_endpoint(common->fsg);
            }
        }
        break;

        /*
         * We have processed all we want from the data the host has sent.
         * There may still be outstanding bulk-out requests.
         */
    case DATA_DIR_FROM_HOST:
        if (common->residue == 0) {
            /* Nothing to receive */

            /* Did the host stop sending unexpectedly early? */
        } else if (common->short_packet_received) {
            //raise_exception(common, FSG_STATE_ABORT_BULK_OUT);
            send_status(common);
            common->state = FSG_STATE_IDLE;
            rc = -EINTR;

            /*
             * We can't stall.  Read in the excess data and throw it
             * all away.
             */
        } else {
            rc = throw_away_data(common);
        }
        break;
    }
    //printf("finish_reply rc is %d\n",rc);
    return rc;
}

static int do_read(struct fsg_common *common)
{
    //struct fsg_lun        *curlun = common->curlun;
    u32            lba;
    struct fsg_buffhd    *bh;
    //int            rc;
    u32            amount_left;
    loff_t            file_offset;
    unsigned int        amount;
    ssize_t            nread;
    //printf("do_read!!!!!!\n");
    /*
     * Get the starting Logical Block Address and check that it's
     * not too big.
     */
    if (common->cmnd[0] == READ_6)
        lba = get_unaligned_be24(&common->cmnd[1]);
    else {
        lba = get_unaligned_be32(&common->cmnd[2]);
        //printf("lba is %d\n",lba);
#if 0
        /*
         * We allow DPO (Disable Page Out = don't save data in the
         * cache) and FUA (Force Unit Access = don't read from the
         * cache), but we don't implement them.
         */
        if ((common->cmnd[1] & ~0x18) != 0) {
            curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
            return -EINVAL;
        }
#endif
    }
#if 0
    if (lba >= curlun->num_sectors) {
        curlun->sense_data = SS_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE;
        return -EINVAL;
    }
    file_offset = ((loff_t) lba) << curlun->blkbits;
#else
    if (lba >= flash.patition_pages/*flash.partition_blks*/) {
        DBG("lba out of the partition\n");
        return -EINVAL;
    }

    //file_offset = ((loff_t) lba) << flash.blk_bit;
    file_offset = ((loff_t) lba) << flash.page_bit;
#endif

    /* Carry out the file reads */
    amount_left = common->data_size_from_cmnd;
    //printf("amount_left is %d!!!\n",amount_left);
    if (amount_left == 0)
        return -EIO;        /* No default reply */

    for (;;) {
        /*
         * Figure out how much we need to read:
         * Try to read the remaining amount.
         * But don't read more than the buffer size.
         * And don't try to read past the end of the file.
         */
        //amount = min(amount_left, FSG_BUFLEN);
        amount = min(amount_left, FSG_BUF_LEN);
        //amount = min((loff_t)amount,
        //         curlun->file_length - file_offset);
        amount = min(amount, flash.partition_len - file_offset);

        /* Wait for the next buffer to become available */
        bh = common->next_buffhd_to_fill;
#if 0
        while (bh->state != BUF_STATE_EMPTY) {
            rc = sleep_thread(common, false);
            if (rc)
                return rc;
        }
#else
        while(bh->state != BUF_STATE_EMPTY) {
            //wait until buf available
            //add wait code here
            udelay(10);
        }
#endif
        /*
         * If we were asked to read past the end of file,
         * end with an empty buffer.
         */
        if (amount == 0) {
#if 0
            curlun->sense_data =
                SS_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE;
            curlun->sense_data_info =
                file_offset >> curlun->blkbits;
            curlun->info_valid = 1;
#endif
            bh->inreq->length = 0;
            bh->state = BUF_STATE_FULL;
            break;
        }
#if 0
        /* Perform the read */
        file_offset_tmp = file_offset;

        nread = vfs_read(curlun->filp,
                (char __user *)bh->buf,
                amount, &file_offset_tmp);
#else
        nread = ms_read(file_offset, bh->buf, amount);
#endif
        DBG("file read %u @ %llu -> %d\n", amount,
                (unsigned long long)file_offset, (int)nread);
#if 0
        if (signal_pending(current))
            return -EINTR;
#endif
        if (nread < 0) {
            DBG("error in file read: %d\n", (int)nread);
            nread = 0;
        } else if ((unsigned int)nread < amount) {
            DBG("partial file read: %d/%u\n",
                    (int)nread, amount);
            nread = round_down(nread, flash.blk_size);
        }
        file_offset  += nread;
        amount_left  -= nread;
        common->residue -= nread;

        /*
         * Except at the end of the transfer, nread will be
         * equal to the buffer size, which is divisible by the
         * bulk-in maxpacket size.
         */
        bh->inreq->length = nread;
        bh->state = BUF_STATE_FULL;
#if 0
        /* If an error occurred, report it and its position */
        if (nread < amount) {
            curlun->sense_data = SS_UNRECOVERED_READ_ERROR;
            curlun->sense_data_info =
                file_offset >> curlun->blkbits;
            curlun->info_valid = 1;
            break;
        }
#endif
        if (amount_left == 0)
            break;        /* No more left to read */

        /* Send this buffer and go read some more */
        bh->inreq->zero = 0;
        if (!start_in_transfer(common, bh))
            /* Don't know what to do if common->fsg is NULL */
            return -EIO;
        common->next_buffhd_to_fill = bh->next;
    }

    return -EIO;        /* No default reply */
}

static int do_write(struct fsg_common *common)
{
    //struct fsg_lun        *curlun = common->curlun;
    u32            lba;
    struct fsg_buffhd    *bh;
    int            get_some_more;
    u32            amount_left_to_req, amount_left_to_write;
    unsigned int        usb_offset, file_offset;
    unsigned int        amount;
    ssize_t            nwritten;
    //int            rc;
#if 0
    if (curlun->ro) {
        curlun->sense_data = SS_WRITE_PROTECTED;
        return -EINVAL;
    }
#endif
    //printf("do_write!!!!!!\n");
    /*
     * Get the starting Logical Block Address and check that it's
     * not too big
     */
    if (common->cmnd[0] == WRITE_6)
        lba = get_unaligned_be24(&common->cmnd[1]);
    else {
        lba = get_unaligned_be32(&common->cmnd[2]);
        //printf("lba is %d\n",lba);
#if 0
        /*
         * We allow DPO (Disable Page Out = don't save data in the
         * cache) and FUA (Force Unit Access = write directly to the
         * medium).  We don't implement DPO; we implement FUA by
         * performing synchronous output.
         */
        if (common->cmnd[1] & ~0x18) {
            curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
            return -EINVAL;
        }
        if (!curlun->nofua && (common->cmnd[1] & 0x08)) { /* FUA */
            spin_lock(&curlun->filp->f_lock);
            curlun->filp->f_flags |= O_SYNC;
            spin_unlock(&curlun->filp->f_lock);
        }
#endif
    }
#if 0
    if (lba >= curlun->num_sectors) {
        curlun->sense_data = SS_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE;
        return -EINVAL;
    }
#else
    if (lba >=flash.patition_pages/*flash.partition_blks*/) {
        DBG("lba beyond partition end\n");
        return -EINVAL;
    }
#endif

    /* Carry out the file writes */
    get_some_more = 1;
    //file_offset = usb_offset = ((loff_t) lba) << flash.blk_bit;
    file_offset = usb_offset = ((loff_t) lba) << flash.page_bit;
    //printf("file offset is %d\n", file_offset);
    amount_left_to_req = common->data_size_from_cmnd;
    amount_left_to_write = common->data_size_from_cmnd;
    //printf("write len is %d\n", common->data_size_from_cmnd);

    while (amount_left_to_write > 0) {

        /* Queue a request for more data from the host */
        bh = common->next_buffhd_to_fill;
        if (bh->state == BUF_STATE_EMPTY && get_some_more) {
            /*
             * Figure out how much we want to get:
             * Try to get the remaining amount,
             * but not more than the buffer size.
             */
            //amount = min(amount_left_to_req, FSG_BUFLEN);
            amount = min(amount_left_to_req, FSG_BUF_LEN);
#if 0
            /* Beyond the end of the backing file? */
            if (usb_offset >= curlun->file_length) {
                get_some_more = 0;
                curlun->sense_data =
                    SS_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE;
                curlun->sense_data_info =
                    usb_offset >> curlun->blkbits;
                curlun->info_valid = 1;
                continue;
            }
#else
            if (usb_offset >= flash.partition_len) {
                get_some_more = 0;
                //printf("usb_offset is %d, flash.partition_len is %d\n",usb_offset,flash.partition_len);
                DBG("offset is beyond the end of partition\n");
                continue;
            }
#endif
            /* Get the next buffer */
            usb_offset += amount;
            common->usb_amount_left -= amount;
            amount_left_to_req -= amount;
            if (amount_left_to_req == 0)
                get_some_more = 0;

            /*
             * Except at the end of the transfer, amount will be
             * equal to the buffer size, which is divisible by
             * the bulk-out maxpacket size.
             */
            set_bulk_out_req_length(common, bh, amount);
            if (!start_out_transfer(common, bh))
                /* Dunno what to do if common->fsg is NULL */
                return -EIO;
            common->next_buffhd_to_fill = bh->next;
            continue;
        }

        /* Write the received data to the backing file */
        bh = common->next_buffhd_to_drain;
        if (bh->state == BUF_STATE_EMPTY && !get_some_more)
            break;            /* We stopped early */
        if (bh->state == BUF_STATE_FULL) {
            //printf("in line %d\n",__LINE__);
            //smp_rmb();
            common->next_buffhd_to_drain = bh->next;
            bh->state = BUF_STATE_EMPTY;
#if 0
            /* Did something go wrong with the transfer? */
            if (bh->outreq->status != 0) {
                curlun->sense_data = SS_COMMUNICATION_FAILURE;
                curlun->sense_data_info =
                    file_offset >> curlun->blkbits;
                curlun->info_valid = 1;
                break;
            }
#endif
            amount = bh->outreq->actual;
#if 0
            if (curlun->file_length - file_offset < amount) {
                LERROR(curlun,
                        "write %u @ %llu beyond end %llu\n",
                        amount, (unsigned long long)file_offset,
                        (unsigned long long)curlun->file_length);
                amount = curlun->file_length - file_offset;
            }
#else
            if (flash.partition_len - file_offset < amount) {
                DBG("write %u @ %llu beyond end %llu\n",
                        amount, (unsigned long long)file_offset,
                        flash.partition_len);
                amount = flash.partition_len - file_offset;
            }
#endif
            /* Don't accept excess data.  The spec doesn't say
             * what to do in this case.  We'll ignore the error.
             */
            amount = min(amount, bh->bulk_out_intended_length);

            /* Don't write a partial block */
            amount = round_down(amount, flash.page_size);
            //printf("amount is %d\n",amount);
            if (amount == 0)
                goto empty_write;

            /* Perform the write */
#if 0
            file_offset_tmp = file_offset;
            nwritten = vfs_write(curlun->filp,
                    (char __user *)bh->buf,
                    amount, &file_offset_tmp);
            VLDBG(curlun, "file write %u @ %llu -> %d\n", amount,
                    (unsigned long long)file_offset, (int)nwritten);
#else
            nwritten = ms_write(file_offset, bh->buf, amount);
#endif
#if 0
            if (signal_pending(current))
                return -EINTR;        /* Interrupted! */
#endif
            if (nwritten < 0) {
                DBG("error in file write: %d\n",
                        (int)nwritten);
                nwritten = 0;
            } else if ((unsigned int)nwritten < amount) {
                DBG("partial file write: %d/%u\n",
                        (int)nwritten, amount);
                nwritten = round_down(nwritten, flash.blk_size);
            }
            file_offset += nwritten;
            amount_left_to_write -= nwritten;
            common->residue -= nwritten;
#if 0
            /* If an error occurred, report it and its position */
            if (nwritten < amount) {
                curlun->sense_data = SS_WRITE_ERROR;
                curlun->sense_data_info =
                    file_offset >> curlun->blkbits;
                curlun->info_valid = 1;
                break;
            }
#endif
empty_write:
            /* Did the host decide to stop early? */
            if (bh->outreq->actual < bh->bulk_out_intended_length) {
                common->short_packet_received = 1;
                break;
            }
            continue;
        }
#if 0
        /* Wait for something to happen */
        rc = sleep_thread(common, false);
        if (rc)
            return rc;
#endif
    }

    return -EIO;        /* No default reply */
}

static int do_inquiry(struct fsg_common *common, struct fsg_buffhd *bh)
{
    //struct fsg_lun *curlun = common->curlun;
    u8    *buf = (u8 *) bh->buf;
#if 0
    if (!curlun) {        /* Unsupported LUNs are okay */
        common->bad_lun_okay = 1;
        memset(buf, 0, 36);
        buf[0] = TYPE_NO_LUN;    /* Unsupported, no device-type */
        buf[4] = 31;        /* Additional length */
        return 36;
    }
#endif
    buf[0] = TYPE_DISK;
    buf[1] = 0x80;
    buf[2] = 2;        /* ANSI SCSI level 2 */
    buf[3] = 2;        /* SCSI-2 INQUIRY data format */
    buf[4] = 31;        /* Additional length */
    buf[5] = 0;        /* No special options */
    buf[6] = 0;
    buf[7] = 0;
    memcpy(buf + 8, common->inquiry_string, sizeof common->inquiry_string);
    //printf("end of do inquiry\n");
    return 36;
}

static int do_mode_select(struct fsg_common *common, struct fsg_buffhd *bh)
{
#if 0
    struct fsg_lun    *curlun = common->curlun;

    /* We don't support MODE SELECT */
    if (curlun)
        curlun->sense_data = SS_INVALID_COMMAND;
#endif
    return -EINVAL;
}

static int do_mode_sense(struct fsg_common *common, struct fsg_buffhd *bh)
{
    //struct fsg_lun    *curlun = common->curlun;
    int        mscmnd = common->cmnd[0];
    u8        *buf = (u8 *) bh->buf;
    u8        *buf0 = buf;
    int        pc, page_code;
    int        changeable_values, all_pages;
    int        valid_page = 0;
    int        len, limit;

    if ((common->cmnd[1] & ~0x08) != 0) {    /* Mask away DBD */
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        printf ("%s %d : %x\n", __func__, __LINE__, common->cmnd[1]);
        return -EINVAL;
    }
    pc = common->cmnd[2] >> 6;
    page_code = common->cmnd[2] & 0x3f;
    if (pc == 3) {
        //curlun->sense_data = SS_SAVING_PARAMETERS_NOT_SUPPORTED;
        printf ("%s %d : %x\n", __func__, __LINE__, common->cmnd[2]);
        return -EINVAL;
    }
    changeable_values = (pc == 1);
    all_pages = (page_code == 0x3f);

    /*
     * Write the mode parameter header.  Fixed values are: default
     * medium type, no cache control (DPOFUA), and no block descriptors.
     * The only variable value is the WriteProtect bit.  We will fill in
     * the mode data length later.
     */
    memset(buf, 0, 8);
    if (mscmnd == MODE_SENSE) {
        //buf[2] = (curlun->ro ? 0x80 : 0x00);        /* WP, DPOFUA */
        buf[2] = (0x00);        /* WP, DPOFUA */
        buf += 4;
        limit = 255;
    } else {            /* MODE_SENSE_10 */
        //buf[3] = (curlun->ro ? 0x80 : 0x00);        /* WP, DPOFUA */
        buf[3] = (0x00);        /* WP, DPOFUA */
        buf += 8;
        limit = 65535;        /* Should really be FSG_BUFLEN */
    }

    /* No block descriptors */

    /*
     * The mode pages, in numerical order.  The only page we support
     * is the Caching page.
     */
    if (page_code == 0x08 || all_pages) {
        valid_page = 1;
        buf[0] = 0x08;        /* Page code */
        buf[1] = 10;        /* Page length */
        memset(buf+2, 0, 10);    /* None of the fields are changeable */

        if (!changeable_values) {
            buf[2] = 0x04;    /* Write cache enable, */
            /* Read cache not disabled */
            /* No cache retention priorities */
            put_unaligned_be16(0xffff, &buf[4]);
            /* Don't disable prefetch */
            /* Minimum prefetch = 0 */
            put_unaligned_be16(0xffff, &buf[8]);
            /* Maximum prefetch */
            put_unaligned_be16(0xffff, &buf[10]);
            /* Maximum prefetch ceiling */
        }
        buf += 12;
    } else if (page_code == 0x1c) {
        // for windows
        memset(buf, 0, common->cmnd[4]);
        buf[0] = common->cmnd[4] - 1; // mode data length
        buf[1] = 0; //medium
        buf[2] = 0;
        buf[3] = 0;

        //  Informational Exceptions Control mode page (1C h)
        buf[4] = 0x1c;
        buf[5] = 0xa;
        return common->cmnd[4];
    }

    /*
     * Check that a valid page was requested and the mode data length
     * isn't too long.
     */
    len = buf - buf0;
    if (!valid_page || len > limit) {
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        printf ("%s %d : %d-%d-%d\n", __func__, __LINE__, valid_page, len, limit);
        return -EINVAL;
    }

    /*  Store the mode data length */
    if (mscmnd == MODE_SENSE)
        buf0[0] = len - 1;
    else
        put_unaligned_be16(len - 2, buf0);
    return len;
}

static int do_read_capacity(struct fsg_common *common, struct fsg_buffhd *bh)
{
    //struct fsg_lun    *curlun = common->curlun;
    u32        lba = get_unaligned_be32(&common->cmnd[2]);
    int        pmi = common->cmnd[8];
    u8        *buf = (u8 *)bh->buf;

    /* Check the PMI and LBA fields */
    if (pmi > 1 || (pmi == 0 && lba != 0)) {
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        return -EINVAL;
    }
#if 0
    put_unaligned_be32(curlun->num_sectors - 1, &buf[0]);
    /* Max logical block */
    put_unaligned_be32(curlun->blksize, &buf[4]);/* Block length */
#else
    //put_unaligned_be32(flash.partition_blks - 1, &buf[0]);
    /* Max logical block */
    //put_unaligned_be32(flash.blk_size, &buf[4]);/* Block length */
    put_unaligned_be32(flash.patition_pages/*flash.partition_blks*/ - 1, &buf[0]);
    /* Max logical block */
    put_unaligned_be32(flash.page_size, &buf[4]);/* Block length */

#endif
    return 8;
}

#if 0
void store_cdrom_address(u8 *dest, int msf, u32 addr)
{
    if (msf) {
        /* Convert to Minutes-Seconds-Frames */
        addr >>= 2;        /* Convert to 2048-byte frames */
        addr += 2*75;        /* Lead-in occupies 2 seconds */
        dest[3] = addr % 75;    /* Frames */
        addr /= 75;
        dest[2] = addr % 60;    /* Seconds */
        addr /= 60;
        dest[1] = addr;        /* Minutes */
        dest[0] = 0;        /* Reserved */
    } else {
        /* Absolute sector */
        put_unaligned_be32(addr, dest);
    }
}

static int do_read_header(struct fsg_common *common, struct fsg_buffhd *bh)
{
    //struct fsg_lun    *curlun = common->curlun;
    int        msf = common->cmnd[1] & 0x02;
    u32        lba = get_unaligned_be32(&common->cmnd[2]);
    u8        *buf = (u8 *)bh->buf;

    if (common->cmnd[1] & ~0x02) {        /* Mask away MSF */
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        return -EINVAL;
    }
    //if (lba >= curlun->num_sectors) {
    if (lba >= flash.patition_pages/*flash.partition_blks*/) {
        //curlun->sense_data = SS_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE;
        return -EINVAL;
    }

    memset(buf, 0, 8);
    buf[0] = 0x01;        /* 2048 bytes of user data, rest is EC */
    store_cdrom_address(&buf[4], msf, lba);
    return 8;
}

static int do_read_toc(struct fsg_common *common, struct fsg_buffhd *bh)
{
    //struct fsg_lun    *curlun = common->curlun;
    int        msf = common->cmnd[1] & 0x02;
    int        start_track = common->cmnd[6];
    u8        *buf = (u8 *)bh->buf;

    if ((common->cmnd[1] & ~0x02) != 0 ||    /* Mask away MSF */
            start_track > 1) {
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        return -EINVAL;
    }

    memset(buf, 0, 20);
    buf[1] = (20-2);        /* TOC data length */
    buf[2] = 1;            /* First track number */
    buf[3] = 1;            /* Last track number */
    buf[5] = 0x16;            /* Data track, copying allowed */
    buf[6] = 0x01;            /* Only track is number 1 */
    store_cdrom_address(&buf[8], msf, 0);

    buf[13] = 0x16;            /* Lead-out track is data */
    buf[14] = 0xAA;            /* Lead-out track number */
    //store_cdrom_address(&buf[16], msf, curlun->num_sectors);
    store_cdrom_address(&buf[16], msf, flash.patition_pages/*flash.partition_blks*/);
    return 20;
}
#endif

static int do_read_format_capacities(struct fsg_common *common,
        struct fsg_buffhd *bh)
{
    //struct fsg_lun    *curlun = common->curlun;
    u8        *buf = (u8 *) bh->buf;

    buf[0] = buf[1] = buf[2] = 0;
    buf[3] = 8;    /* Only the Current/Maximum Capacity Descriptor */
    buf += 4;

    //put_unaligned_be32(curlun->num_sectors, &buf[0]);
    put_unaligned_be32(flash.patition_pages/*flash.partition_blks*/, &buf[0]);

    /* Number of blocks */
    //put_unaligned_be32(curlun->blksize, &buf[4]);/* Block length */
    put_unaligned_be32(flash.blk_size, &buf[4]);/* Block length */
    buf[4] = 0x02;                /* Current capacity */
    return 12;
}

/*
 * Check whether the command is properly formed and whether its data size
 * and direction agree with the values we already have.
 */
static int check_command(struct fsg_common *common, int cmnd_size,
        enum data_direction data_dir, unsigned int mask,
        int needs_medium, const char *name)
{
    //int            i;
    //unsigned int        lun = common->cmnd[1] >> 5;
    static const char    dirletter[4] = {'u', 'o', 'i', 'n'};
    char            hdlen[20];
    //struct fsg_lun        *curlun;

    hdlen[0] = 0;
    if (common->data_dir != DATA_DIR_UNKNOWN)
        sprintf(hdlen, ", H%c=%u", dirletter[(int) common->data_dir],
                common->data_size);
#if 0
    if(strcmp(name, "TEST UNIT READY")!=0)
        printf ("SCSI command: %s;  Dc=%d, D%c=%u;  Hc=%d%s\n",
                name, cmnd_size, dirletter[(int) data_dir],
                common->data_size_from_cmnd, common->cmnd_size, hdlen);
#endif
    /*
     * We can't reply at all until we know the correct data direction
     * and size.
     */
    if (common->data_size_from_cmnd == 0)
        data_dir = DATA_DIR_NONE;
    if (common->data_size < common->data_size_from_cmnd) {
        /*
         * Host data size < Device data size is a phase error.
         * Carry out the command, but only transfer as much as
         * we are allowed.
         */
        common->data_size_from_cmnd = common->data_size;
        common->phase_error = 1;
    }
    common->residue = common->data_size;
    common->usb_amount_left = common->data_size;

    /* Conflicting data directions is a phase error */
    if (common->data_dir != data_dir && common->data_size_from_cmnd > 0) {
        common->phase_error = 1;
        return -EINVAL;
    }

    /* Verify the length of the command itself */
    if (cmnd_size != common->cmnd_size) {

        /*
         * Special case workaround: There are plenty of buggy SCSI
         * implementations. Many have issues with cbw->Length
         * field passing a wrong command size. For those cases we
         * always try to work around the problem by using the length
         * sent by the host side provided it is at least as large
         * as the correct command length.
         * Examples of such cases would be MS-Windows, which issues
         * REQUEST SENSE with cbw->Length == 12 where it should
         * be 6, and xbox360 issuing INQUIRY, TEST UNIT READY and
         * REQUEST SENSE with cbw->Length == 10 where it should
         * be 6 as well.
         */
        if (cmnd_size <= common->cmnd_size) {
            DBG("%s is buggy! Expected length %d "
                    "but we got %d\n", name,
                    cmnd_size, common->cmnd_size);
            cmnd_size = common->cmnd_size;
        } else {
            common->phase_error = 1;
            return -EINVAL;
        }
    }
#if 0
    /* Check that the LUN values are consistent */
    if (common->lun != lun)
        DBG(common, "using LUN %u from CBW, not LUN %u from CDB\n",
                common->lun, lun);

    /* Check the LUN */
    curlun = common->curlun;
    if (curlun) {
        if (common->cmnd[0] != REQUEST_SENSE) {
            curlun->sense_data = SS_NO_SENSE;
            curlun->sense_data_info = 0;
            curlun->info_valid = 0;
        }
    } else {
        common->bad_lun_okay = 0;

        /*
         * INQUIRY and REQUEST SENSE commands are explicitly allowed
         * to use unsupported LUNs; all others may not.
         */
        if (common->cmnd[0] != INQUIRY &&
                common->cmnd[0] != REQUEST_SENSE) {
            DBG(common, "unsupported LUN %u\n", common->lun);
            return -EINVAL;
        }
    }

    /*
     * If a unit attention condition exists, only INQUIRY and
     * REQUEST SENSE commands are allowed; anything else must fail.
     */
    if (curlun && curlun->unit_attention_data != SS_NO_SENSE &&
            common->cmnd[0] != INQUIRY &&
            common->cmnd[0] != REQUEST_SENSE) {
        curlun->sense_data = curlun->unit_attention_data;
        curlun->unit_attention_data = SS_NO_SENSE;
        return -EINVAL;
    }

    /* Check that only command bytes listed in the mask are non-zero */
    common->cmnd[1] &= 0x1f;            /* Mask away the LUN */
    for (i = 1; i < cmnd_size; ++i) {
        if (common->cmnd[i] && !(mask & (1 << i))) {
            if (curlun)
                curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
            return -EINVAL;
        }
    }

    /* If the medium isn't mounted and the command needs to access
     * it, return an error. */
    if (curlun && !fsg_lun_is_open(curlun) && needs_medium) {
        curlun->sense_data = SS_MEDIUM_NOT_PRESENT;
        return -EINVAL;
    }
#endif
    return 0;
}

static int do_prevent_allow(struct fsg_common *common)
{
#if 0
    //struct fsg_lun    *curlun = common->curlun;
    int        prevent;
    if (!common->curlun) {
        return -EINVAL;
    } else if (!common->curlun->removable) {
        common->curlun->sense_data = SS_INVALID_COMMAND;
        return -EINVAL;
    }
    prevent = common->cmnd[4] & 0x01;
#endif
    if ((common->cmnd[4] & ~0x01) != 0) {    /* Mask away Prevent */
        //curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        return -EINVAL;
    }
#if 0
    if (curlun->prevent_medium_removal && !prevent)
        fsg_lun_fsync_sub(curlun);
    curlun->prevent_medium_removal = prevent;
#endif
    return 0;
}

#if 0
static int do_start_stop(struct fsg_common *common)
{
#if 0
    struct fsg_lun    *curlun = common->curlun;
    int        loej, start;

    if (!curlun) {
        return -EINVAL;
    } else if (!curlun->removable) {
        curlun->sense_data = SS_INVALID_COMMAND;
        return -EINVAL;
    } else if ((common->cmnd[1] & ~0x01) != 0 || /* Mask away Immed */
            (common->cmnd[4] & ~0x03) != 0) { /* Mask LoEj, Start */
        curlun->sense_data = SS_INVALID_FIELD_IN_CDB;
        return -EINVAL;
    }

    loej  = common->cmnd[4] & 0x02;
    start = common->cmnd[4] & 0x01;

    /*
     * Our emulation doesn't support mounting; the medium is
     * available for use as soon as it is loaded.
     */
    if (start) {
        if (!fsg_lun_is_open(curlun)) {
            curlun->sense_data = SS_MEDIUM_NOT_PRESENT;
            return -EINVAL;
        }
        return 0;
    }

    /* Are we allowed to unload the media? */
    if (curlun->prevent_medium_removal) {
        LDBG(curlun, "unload attempt prevented\n");
        curlun->sense_data = SS_MEDIUM_REMOVAL_PREVENTED;
        return -EINVAL;
    }

    if (!loej)
        return 0;

    up_read(&common->filesem);
    down_write(&common->filesem);
    fsg_lun_close(curlun);
    up_write(&common->filesem);
    down_read(&common->filesem);
#endif
    return 0;
}
#endif

/* wrapper of check_command for data size in blocks handling */
static int check_command_size_in_blocks(struct fsg_common *common,
        int cmnd_size, enum data_direction data_dir,
        unsigned int mask, int needs_medium, const char *name)
{
    //if (common->curlun)
    //    common->data_size_from_cmnd <<= common->curlun->blkbits;
    common->data_size_from_cmnd <<= flash.page_bit;
    //common->data_size_from_cmnd =256;//<<= flash.page_bit;
    return check_command(common, cmnd_size, data_dir,
            mask, needs_medium, name);
}

static int do_scsi_command(struct fsg_common *common)
{
    struct fsg_buffhd    *bh;
    //u32                      addr;
    //unsigned int crc, size;
    int i;
    int            reply = -EINVAL;
    static char        unknown[16];
    //printf("%s in line %d\n", __func__,__LINE__);
    /* Wait for the next buffer to become available for data or status */
    bh = common->next_buffhd_to_fill;
    common->next_buffhd_to_drain = bh;

    while (bh->state != BUF_STATE_EMPTY);
    //printf("%s in line %d\n", __func__,__LINE__);
    common->phase_error = 0;
    common->short_packet_received = 0;
    //printf("command is %x@@@@@@@@@@@@@@@@@\n",common->cmnd[0]);
    switch (common->cmnd[0]) {
    case INQUIRY:
        common->data_size_from_cmnd = common->cmnd[4];
        reply = check_command(common, 6, DATA_DIR_TO_HOST,
                (1<<4), 0,
                "INQUIRY");
        if (reply == 0)
            reply = do_inquiry(common, bh);
        break;
    case MODE_SELECT:
        common->data_size_from_cmnd = common->cmnd[4];
        reply = check_command(common, 6, DATA_DIR_FROM_HOST,
                (1<<1) | (1<<4), 0,
                "MODE SELECT(6)");
        if (reply == 0)
            reply = do_mode_select(common, bh);
        break;

    case MODE_SELECT_10:
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        reply = check_command(common, 10, DATA_DIR_FROM_HOST,
                (1<<1) | (3<<7), 0,
                "MODE SELECT(10)");
        if (reply == 0)
            reply = do_mode_select(common, bh);
        break;
    case MODE_SENSE:
        common->data_size_from_cmnd = common->cmnd[4];
        reply = check_command(common, 6, DATA_DIR_TO_HOST,
                (1<<1) | (1<<2) | (1<<4), 0,
                "MODE SENSE(6)");
        if (reply == 0)
            reply = do_mode_sense(common, bh);
        break;

    case MODE_SENSE_10:
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        reply = check_command(common, 10, DATA_DIR_TO_HOST,
                (1<<1) | (1<<2) | (3<<7), 0,
                "MODE SENSE(10)");
        if (reply == 0)
            reply = do_mode_sense(common, bh);
        break;

    case ALLOW_MEDIUM_REMOVAL:
        common->data_size_from_cmnd = 0;
        reply = check_command(common, 6, DATA_DIR_NONE,
                (1<<4), 0,
                "PREVENT-ALLOW MEDIUM REMOVAL");
        if (reply == 0)
            reply = do_prevent_allow(common);
        break;

    case READ_6:
        i = common->cmnd[4];
        common->data_size_from_cmnd = (i == 0) ? 256 : i;
        reply = check_command_size_in_blocks(common, 6,
                DATA_DIR_TO_HOST,
                (7<<1) | (1<<4), 1,
                "READ(6)");
        if (reply == 0)
            reply = do_read(common);
        break;

    case READ_10:
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        //printf("common->data_size_from_cmnd is %d\n",common->data_size_from_cmnd);
        reply = check_command_size_in_blocks(common, 10,
                DATA_DIR_TO_HOST,
                (1<<1) | (0xf<<2) | (3<<7), 1,
                "READ(10)");
        if (reply == 0)
            reply = do_read(common);
        //printf("do read return %d!!!\n",reply);
        break;

    case READ_12:
        common->data_size_from_cmnd =
            get_unaligned_be32(&common->cmnd[6]);
        reply = check_command_size_in_blocks(common, 12,
                DATA_DIR_TO_HOST,
                (1<<1) | (0xf<<2) | (0xf<<6), 1,
                "READ(12)");
        if (reply == 0)
            reply = do_read(common);
        break;

    case READ_CAPACITY:
        common->data_size_from_cmnd = 8;
        reply = check_command(common, 10, DATA_DIR_TO_HOST,
                (0xf<<2) | (1<<8), 1,
                "READ CAPACITY");
        if (reply == 0)
            reply = do_read_capacity(common, bh);
        break;
#if 0
    case READ_HEADER:
        //if (!common->curlun || !common->curlun->cdrom)
        //    goto unknown_cmnd;
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        reply = check_command(common, 10, DATA_DIR_TO_HOST,
                (3<<7) | (0x1f<<1), 1,
                "READ HEADER");
        if (reply == 0)
            reply = do_read_header(common, bh);
        break;

    case READ_TOC:
        //if (!common->curlun || !common->curlun->cdrom)
        //    goto unknown_cmnd;
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        reply = check_command(common, 10, DATA_DIR_TO_HOST,
                (7<<6) | (1<<1), 1,
                "READ TOC");
        if (reply == 0)
            reply = do_read_toc(common, bh);
        break;
#endif

    case READ_FORMAT_CAPACITIES:
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        reply = check_command(common, 10, DATA_DIR_TO_HOST,
                (3<<7), 1,
                "READ FORMAT CAPACITIES");
        if (reply == 0)
            reply = do_read_format_capacities(common, bh);
        break;
#if 0
#if 0
    case REQUEST_SENSE:
        common->data_size_from_cmnd = common->cmnd[4];
        reply = check_command(common, 6, DATA_DIR_TO_HOST,
                (1<<4), 0,
                "REQUEST SENSE");
        if (reply == 0)
            reply = do_request_sense(common, bh);
        break;
#endif
    case START_STOP:
        common->data_size_from_cmnd = 0;
        reply = check_command(common, 6, DATA_DIR_NONE,
                (1<<1) | (1<<4), 0,
                "START-STOP UNIT");
        if (reply == 0)
            reply = do_start_stop(common);
        break;
#if 0
    case SYNCHRONIZE_CACHE:
        common->data_size_from_cmnd = 0;
        reply = check_command(common, 10, DATA_DIR_NONE,
                (0xf<<2) | (3<<7), 1,
                "SYNCHRONIZE CACHE");
        if (reply == 0)
            reply = do_synchronize_cache(common);
        break;
#endif
#endif
    case TEST_UNIT_READY:
        common->data_size_from_cmnd = 0;
        reply = check_command(common, 6, DATA_DIR_NONE,
                0, 1,
                "TEST UNIT READY");
        break;
#if 0
        /*
         * Although optional, this command is used by MS-Windows.  We
         * support a minimal version: BytChk must be 0.
         */
    case VERIFY:
        common->data_size_from_cmnd = 0;
        reply = check_command(common, 10, DATA_DIR_NONE,
                (1<<1) | (0xf<<2) | (3<<7), 1,
                "VERIFY");
        if (reply == 0)
            reply = do_verify(common);
        break;
#endif
    case WRITE_6:
        i = common->cmnd[4];
        common->data_size_from_cmnd = (i == 0) ? 256 : i;
        reply = check_command_size_in_blocks(common, 6,
                DATA_DIR_FROM_HOST,
                (7<<1) | (1<<4), 1,
                "WRITE(6)");
        if (reply == 0)
            reply = do_write(common);
        break;

    case WRITE_10:
        common->data_size_from_cmnd =
            get_unaligned_be16(&common->cmnd[7]);
        reply = check_command_size_in_blocks(common, 10,
                DATA_DIR_FROM_HOST,
                (1<<1) | (0xf<<2) | (3<<7), 1,
                "WRITE(10)");
        if (reply == 0)
            reply = do_write(common);
        break;

    case WRITE_12:
        common->data_size_from_cmnd =
            get_unaligned_be32(&common->cmnd[6]);
        reply = check_command_size_in_blocks(common, 12,
                DATA_DIR_FROM_HOST,
                (1<<1) | (0xf<<2) | (0xf<<6), 1,
                "WRITE(12)");
        if (reply == 0)
            reply = do_write(common);
        break;
        /*
         * Some mandatory commands that we recognize but don't implement.
         * They don't mean much in this setting.  It's left as an exercise
         * for anyone interested to implement RESERVE and RELEASE in terms
         * of Posix locks.
         */
    case FORMAT_UNIT:
    case RELEASE:
    case RESERVE:
    case SEND_DIAGNOSTIC:
        /* Fall through */

    default:
        //unknown_cmnd:
        common->data_size_from_cmnd = 0;
        sprintf(unknown, "Unknown x%02x", common->cmnd[0]);
        reply = check_command(common, common->cmnd_size,
                DATA_DIR_UNKNOWN, ~0, 0, unknown);
        if (reply == 0) {
            //common->curlun->sense_data = SS_INVALID_COMMAND;
            reply = -EINVAL;
        }
        break;
    }

    if (reply == -EINTR)
        return -EINTR;

    /* Set up the single reply buffer for finish_reply() */
    if (reply == -EINVAL)
        reply = 0;        /* Error reply length */
    if (reply >= 0 && common->data_dir == DATA_DIR_TO_HOST) {
        reply = min((u32)reply, common->data_size_from_cmnd);
        bh->inreq->length = reply;
        //printf("reply is %d!!!!!!!!!!!!!!!!\n",reply);
        bh->state = BUF_STATE_FULL;
        common->residue -= reply;
    }                /* Otherwise it's already set */

    return 0;
}

static int received_cbw(struct fsg_dev *fsg, struct fsg_buffhd *bh)
{
    struct usb_request    *req = bh->outreq;
    struct bulk_cb_wrap    *cbw = req->buf;
    struct fsg_common    *common = fsg->common;

    /* Was this a real packet?  Should it be ignored? */
#if 0
    if (req->status || test_bit(IGNORE_BULK_OUT, &fsg->atomic_bitflags))
        return -EINVAL;
#else
    if (req->status || ((1 << IGNORE_BULK_OUT) & fsg->atomic_bitflags) )
        return -EINVAL;
#endif

    /* Is the CBW valid? */
    if (req->actual != US_BULK_CB_WRAP_LEN ||
            cbw->Signature != cpu_to_le32(
                US_BULK_CB_SIGN)) {
        printf ("invalid CBW: len %u sig 0x%x\n",
                req->actual,
                le32_to_cpu(cbw->Signature));

        /*
         * The Bulk-only spec says we MUST stall the IN endpoint
         * (6.6.1), so it's unavoidable.  It also says we must
         * retain this state until the next reset, but there's
         * no way to tell the controller driver it should ignore
         * Clear-Feature(HALT) requests.
         *
         * We aren't required to halt the OUT endpoint; instead
         * we can simply accept and discard any data received
         * until the next reset.
         */
        wedge_bulk_in_endpoint(fsg);
#if 0
        set_bit(IGNORE_BULK_OUT, &fsg->atomic_bitflags);
#else
        fsg->atomic_bitflags |= (1 << IGNORE_BULK_OUT);
#endif
        return -EINVAL;
    }

    /* Save the command for later */
    common->cmnd_size = cbw->Length;
    memcpy(common->cmnd, cbw->CDB, common->cmnd_size);
    if (cbw->Flags & US_BULK_FLAG_IN)
        common->data_dir = DATA_DIR_TO_HOST;
    else
        common->data_dir = DATA_DIR_FROM_HOST;
    common->data_size = le32_to_cpu(cbw->DataTransferLength);
    if (common->data_size == 0)
        common->data_dir = DATA_DIR_NONE;

    common->usb_amount_left = common->data_size;

    common->residue = common->data_size;
    //printf("common->residue is set to %d\n",common->residue);
    common->tag = cbw->Tag;

    return 0;
}

static int tran_num=0;
static int get_next_command(struct fsg_common *common)
{
    struct fsg_buffhd    *bh;
    int            rc = 0;

    bh = common->next_buffhd_to_fill;

    //send request to read, then exit,
    //dont wait the callback return
    if(tran_num == 0) {
        while (bh->state != BUF_STATE_EMPTY) {
            return -EIO;
        }
        /* Queue a request to read a Bulk-only CBW */
        set_bulk_out_req_length(common, bh, US_BULK_CB_WRAP_LEN);
        if (!start_out_transfer(common, bh))
            /* Don't know what to do if common->fsg is NULL */
            return -EIO;
        tran_num ++;
        //printf("tran_num is %d\n",tran_num);
        /*
         * We will drain the buffer in software, which means we
         * can reuse it for the next filling.  No need to advance
         * next_buffhd_to_fill.
         */

        /* Wait for the CBW to arrive */
        while (bh->state != BUF_STATE_FULL) {
            //udelay(10);
            return -EIO;
        }

        rc = fsg_is_set(common) ? received_cbw(common->fsg, bh) : -EIO;
        tran_num --;
        bh->state = BUF_STATE_EMPTY;
    } else {
        //if callback return,  read cbw
        /* Wait for the CBW to arrive */
        while (bh->state != BUF_STATE_FULL) {
            //udelay(10);
            return -EIO;
        }
        rc = fsg_is_set(common) ? received_cbw(common->fsg, bh) : -EIO;
        tran_num --;
        bh->state = BUF_STATE_EMPTY;
    }
    //printf("get_next_command %d rc is %d\n", __LINE__,rc);
    return rc;
}

void MassstorageProcess(void)
{
    if (g_fsg_common->running == 0) {
        //udelay(1);
        return;
    }

    DBG ("[ms] : goto get next cmd---->\n");

    if(get_next_command(g_fsg_common)==0) {
        DBG ("[ms] : get command\n");

        if (do_scsi_command(g_fsg_common) || finish_reply(g_fsg_common)) {
            ;
        } else {
            DBG ("[ms] : handle cmd fin\n");
            send_status(g_fsg_common);
            DBG ("[ms] : goto send status\n");
        }
    }

    return ;
}

static int alloc_request(struct fsg_common *common, struct usb_ep *ep,
        struct usb_request **preq)
{
    *preq = usb_ep_alloc_request(ep);
    if (*preq)
        return 0;
    DBG("can't allocate request for %s\n", ep->name);
    return -ENOMEM;
}

static struct fsg_common *fsg_common_setup(struct fsg_common *common)
{
    if (!common) {
        common = &g_common;
        memset(common, 0, sizeof(*common));
        common->free_storage_on_release = 1;
    } else {
        common->free_storage_on_release = 0;
    }

    common->state = FSG_STATE_IDLE;

    return common;
}

/* Reset interface setting and re-init endpoint state (toggle etc). */
static int do_set_interface(struct fsg_common *common, struct fsg_dev *new_fsg)
{
    struct fsg_dev *fsg;
    int rc = 0;
    unsigned int i;

#if 0
    if (common->running)
        DBG("reset interface\n");
#endif

reset:
    /* Deallocate the requests */
    if (common->fsg) {
        fsg = common->fsg;

        for (i = 0; i < common->fsg_num_buffers; ++i) {
            struct fsg_buffhd *bh = &common->buffhds[i];

            if (bh->inreq) {
                usb_ep_free_request(fsg->bulk_in, bh->inreq);
                bh->inreq = NULL;
            }
            if (bh->outreq) {
                usb_ep_free_request(fsg->bulk_out, bh->outreq);
                bh->outreq = NULL;
            }
        }

        /* Disable the endpoints */
        if (fsg->bulk_in_enabled) {
            usb_ep_disable(fsg->bulk_in);
            fsg->bulk_in_enabled = 0;
        }
        if (fsg->bulk_out_enabled) {
            usb_ep_disable(fsg->bulk_out);
            fsg->bulk_out_enabled = 0;
        }

        common->fsg = NULL;
        //wake_up(&common->fsg_wait);
    }

    common->running = 0;
    if (!new_fsg || rc)
        return rc;

    common->fsg = new_fsg;
    fsg = common->fsg;

    /* Enable the endpoints */
    rc = config_ep_by_speed(common->gadget, &(fsg->function), fsg->bulk_in);
    if (rc)
        goto reset;
    rc = usb_ep_enable(fsg->bulk_in);
    if (rc)
        goto reset;
    fsg->bulk_in->driver_data = common;
    fsg->bulk_in_enabled = 1;

    rc = config_ep_by_speed(common->gadget, &(fsg->function),
            fsg->bulk_out);
    if (rc)
        goto reset;
    rc = usb_ep_enable(fsg->bulk_out);
    if (rc)
        goto reset;
    fsg->bulk_out->driver_data = common;
    fsg->bulk_out_enabled = 1;
    common->bulk_out_maxpacket = usb_endpoint_maxp(fsg->bulk_out->desc);

#if 0
    clear_bit(IGNORE_BULK_OUT, &fsg->atomic_bitflags);
#else
    fsg->atomic_bitflags &= ~(1 << IGNORE_BULK_OUT);
#endif

    /* Allocate the requests */
    for (i = 0; i < common->fsg_num_buffers; ++i) {
        struct fsg_buffhd    *bh = &common->buffhds[i];

        rc = alloc_request(common, fsg->bulk_in, &bh->inreq);
        if (rc)
            goto reset;
        rc = alloc_request(common, fsg->bulk_out, &bh->outreq);
        if (rc)
            goto reset;
        bh->inreq->buf = bh->outreq->buf = bh->buf;
        bh->inreq->context = bh->outreq->context = bh;
        bh->inreq->complete = bulk_in_complete;
        bh->outreq->complete = bulk_out_complete;
    }

    common->running = 1;
    return rc;
}

static void basic_env_init(void)
{
    tran_num = 0;

    return ;
}

static int fsg_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
    struct fsg_dev *fsg = fsg_from_func(f);
    struct fsg_common    *common = fsg->common;
    struct fsg_buffhd *bh;
    unsigned int i;

    fsg->common->new_fsg = fsg;
    //printf("fsg_set_alt\n");
    for (i = 0; i < common->fsg_num_buffers; ++i) {
        bh = &common->buffhds[i];
        bh->state = BUF_STATE_EMPTY;
        DBG("fsg_set_alt set buf empty,i = %d\n",i);
    }

    common->next_buffhd_to_fill = &common->buffhds[0];
    common->next_buffhd_to_drain = &common->buffhds[0];
    //exception_req_tag = common->exception_req_tag;

    do_set_interface(common, common->new_fsg);
    if (common->new_fsg)
        usb_composite_setup_continue(common->cdev);

    // 在当前功能正常开始前，需要保证部分环境的干净。而 set_alt 就是当前功能的开始
    basic_env_init();

    return 0;
}

static void fsg_disable(struct usb_function *f)
{
    struct fsg_dev *fsg = fsg_from_func(f);

    fsg->common->new_fsg = NULL;
    fsg->common->running = 0;

    return ;
}

static int ep0_queue(struct fsg_common *common)
{
    int    rc;

    rc = usb_ep_queue(common->ep0, common->ep0req);
    common->ep0->driver_data = common;
    if (rc != 0 && rc != -ESHUTDOWN) {
        /* We can't do much more than wait for a reset */
        DBG("error in submission: %s --> %d\n",
                common->ep0->name, rc);
    }
    return rc;
}

static int fsg_setup(struct usb_function *f,
        const struct usb_ctrlrequest *ctrl)
{
    struct fsg_dev        *fsg = fsg_from_func(f);
    struct usb_request    *req = fsg->common->ep0req;
    u16            w_index = le16_to_cpu(ctrl->wIndex);
    u16            w_value = le16_to_cpu(ctrl->wValue);
    u16            w_length = le16_to_cpu(ctrl->wLength);
    if (!fsg_is_set(fsg->common))
        return -EOPNOTSUPP;

    ++fsg->common->ep0_req_tag;    /* Record arrival of a new request */
    req->context = NULL;
    req->length = 0;

    switch (ctrl->bRequest) {

    case US_BULK_RESET_REQUEST:
        DBG("fsg_setup can't handle US_BULK_RESET_REQUEST\n");
        break;

    case US_BULK_GET_MAX_LUN:
        if (ctrl->bRequestType != (USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE))
            break;
        if (w_index != fsg->interface_number || w_value != 0 || w_length != 1)
            return -EDOM;
        //DBG("get max LUN\n");
        *(u8 *)req->buf = 0;

        /* Respond with data/status */
        req->length = min((u16)1, w_length);
        return ep0_queue(fsg->common);

    }
    DBG("unknown class-specific control req %02x.%02x v%04x i%04x l%u\n",
            ctrl->bRequestType, ctrl->bRequest,
            le16_to_cpu(ctrl->wValue), w_index, w_length);
    return -EOPNOTSUPP;
}

void fsg_common_set_inquiry_string(struct fsg_common *common, const char *vn,
        const char *pn)
{
    int i;
    printf("fsg_common_set_inquiry_string\n");
    /* Prepare inquiryString */
    i = 0;
    snprintf(common->inquiry_string, sizeof(common->inquiry_string),
            "%-8s%-16s%04x", vn ?: "Linux",
            /* Assume product name dependent on the first LUN */
            "File-Stor Gadget",
            i);

    return ;
}

static int fsg_common_set_cdev(struct fsg_common *common,
        struct usb_composite_dev *cdev, bool can_stall)
{
#if 0
    struct usb_string *us;
#endif

    common->gadget = cdev->gadget;
    common->ep0 = cdev->gadget->ep0;
    common->ep0req = cdev->req;
    common->cdev = cdev;

#if 0
    us = usb_gstrings_attach(cdev, fsg_strings_array,
            ARRAY_SIZE(fsg_strings));
    if (!us)
        return -1;

    fsg_intf_desc.iInterface = us[FSG_STRING_INTERFACE].id;
#endif

    /*
     * Some peripheral controllers are known not to be able to
     * halt bulk endpoints correctly.  If one of them is present,
     * disable stalls.
     */
    common->can_stall = can_stall;

    return 0;
}

static int fsg_bind(struct usb_configuration *c, struct usb_function *f)
{
    struct fsg_dev        *fsg = fsg_from_func(f);
    struct usb_gadget    *gadget = c->cdev->gadget;
    int            i;
    struct usb_ep        *ep;
    int            ret;
    struct fsg_opts        *opts;
    //printf("%s!!!!!!!!!!\n",__func__);
    opts = fsg_opts_from_func_inst(f->fi);
    if (!opts->no_configfs) {
        ret = fsg_common_set_cdev(fsg->common, c->cdev,
                fsg->common->can_stall);
        if (ret)
            return ret;
        fsg_common_set_inquiry_string(fsg->common, NULL, NULL);
    }

    fsg->gadget = gadget;

    /* New interface */
    i = usb_interface_id(c, f);
    if (i < 0)
        goto fail;
    fsg_intf_desc.bInterfaceNumber = i;
    fsg->interface_number = i;

    /* Find all the endpoints we will use */
    ep = usb_ep_autoconfig(gadget, &fsg_fs_bulk_in_desc);
    if (!ep)
        goto autoconf_fail;
    fsg->bulk_in = ep;

    ep = usb_ep_autoconfig(gadget, &fsg_fs_bulk_out_desc);
    if (!ep)
        goto autoconf_fail;
    fsg->bulk_out = ep;

    /* Assume endpoint addresses are the same for both speeds */
    fsg_hs_bulk_in_desc.bEndpointAddress =
        fsg_fs_bulk_in_desc.bEndpointAddress;
    fsg_hs_bulk_out_desc.bEndpointAddress =
        fsg_fs_bulk_out_desc.bEndpointAddress;

#if 0
    /* Calculate bMaxBurst, we know packet size is 1024 */
    max_burst = min_t(unsigned, fsg_buff_len / 1024, 15);

    fsg_ss_bulk_in_desc.bEndpointAddress =
        fsg_fs_bulk_in_desc.bEndpointAddress;
    fsg_ss_bulk_in_comp_desc.bMaxBurst = max_burst;

    fsg_ss_bulk_out_desc.bEndpointAddress =
        fsg_fs_bulk_out_desc.bEndpointAddress;
    fsg_ss_bulk_out_comp_desc.bMaxBurst = max_burst;
#endif

    ret = usb_assign_descriptors(f, fsg_fs_function, fsg_hs_function,
            NULL);
    if (ret)
        goto autoconf_fail;

    return 0;

autoconf_fail:
    //ERROR(fsg, "unable to autoconfigure all endpoints\n");
    i = -ENOTSUPP;
fail:
    /* terminate the thread */
    if (fsg->common->state != FSG_STATE_TERMINATED) {
        ;
    }
    return i;
}

static void fsg_unbind(struct usb_configuration *c, struct usb_function *f)
{

    struct fsg_dev        *fsg = fsg_from_func(f);
#if 0
    struct fsg_common    *common = fsg->common;

    DBG(fsg, "unbind\n");
    if (fsg->common->fsg == fsg) {
        fsg->common->new_fsg = NULL;
        //raise_exception(fsg->common, FSG_STATE_CONFIG_CHANGE);
        /* FIXME: make interruptible or killable somehow? */
        //wait_event(common->fsg_wait, common->fsg != fsg);
    }
#endif
    usb_free_all_descriptors(&fsg->function);

    return ;
}

static void fsg_free_inst(struct usb_function_instance *fi)
{
#if 0
    struct fsg_opts *opts;

    opts = fsg_opts_from_func_inst(fi);
    fsg_common_put(opts->common);
    free(opts);
#else
    return ;
#endif
}

static void fsg_free(struct usb_function *f)
{
#if 0
    struct fsg_dev *fsg;
    struct fsg_opts *opts;

    fsg = container_of(f, struct fsg_dev, function);
    opts = container_of(f->fi, struct fsg_opts, func_inst);

    free(fsg);
#else
    return ;
#endif
}

static struct usb_function_instance *fsg_alloc_inst(void)
{
    struct fsg_opts *opts;
    int rc;

    opts = &g_opts;

    memset(opts, 0, sizeof(*opts));
    opts->func_inst.free_func_inst = fsg_free_inst;
    opts->common = fsg_common_setup(opts->common);
    if (!opts->common)
        goto release_opts;

    g_fsg_common = opts->common;

    rc = fsg_common_set_num_buffers(opts->common,2);
    if (rc)
        goto release_opts;

    return &opts->func_inst;

release_opts:
    return NULL;
}

static void fsg_suspend(struct usb_function *func)
{
    struct fsg_dev *fsg = fsg_from_func(func);

    fsg->common->running = 0;

    return ;
}

static void fsg_resume(struct usb_function *func)
{
    func = func;

    return ;
}

static struct usb_function *fsg_alloc(struct usb_function_instance *fi)
{
    struct fsg_opts *opts = fsg_opts_from_func_inst(fi);
    struct fsg_common *common = opts->common;
    struct fsg_dev *fsg;

    fsg = &g_fsg;

    memset(fsg, 0, sizeof(*fsg));
    fsg->function.name    = FSG_DRIVER_DESC;
    fsg->function.bind    = fsg_bind;
    fsg->function.unbind    = fsg_unbind;
    fsg->function.setup    = fsg_setup;
    fsg->function.set_alt    = fsg_set_alt;
    fsg->function.disable    = fsg_disable;
    fsg->function.free_func    = fsg_free;

    fsg->function.suspend    = fsg_suspend;
    fsg->function.resume    = fsg_resume;

    fsg->common               = common;

    return &fsg->function;
}

#define USB_FUNC_NAME "mass_storage"
static struct usb_function_driver msg_usb_func = {
    .name = USB_FUNC_NAME,
    .alloc_inst = fsg_alloc_inst,
    .alloc_func = fsg_alloc,
};

int MsgDoConfig(struct usb_configuration *c)
{
    int ret;

    f_msg = usb_get_function(fi_msg);
    if (!f_msg){
        DBG("%s f_msg mem fail\n", __func__);
        return 0;
    }
    ret = usb_add_function(c, f_msg);
    if (ret)
        goto put_func;

    return 0;

put_func:
    usb_put_function(f_msg);
    return ret;
}

int MsgBind(struct usb_composite_dev *cdev)
{
    struct fsg_opts *opts;
    int can_stall;
    int status;

    fi_msg = usb_get_function_instance(USB_FUNC_NAME); // fsg_alloc_inst

    opts = fsg_opts_from_func_inst(fi_msg);

    opts->no_configfs = true;

    can_stall = mod_data.stall;
    status = fsg_common_set_cdev(opts->common, cdev, can_stall);
    if (status)
        goto fail_set_cdev;

    /*fsg_common_set_inquiry_string(opts->common, config.vendor_name,
      config.product_name);*/

    //usb_composite_overwrite_options(cdev, &coverwrite);
    DBG(DRIVER_DESC ", version: " DRIVER_VERSION "\n");
    return 0;

fail_set_cdev:
    fsg_common_free_buffers(opts->common);
    usb_put_function_instance(fi_msg);
    return status;
}

int MsgUnbind(struct usb_composite_dev *cdev)
{
    usb_put_function_instance(fi_msg);
    return 0;
}

int  msg_mod_init(void)
{
    struct fsg_buffhd *bh;

    bh = g_buffhds;
    bh->inreq->buf = bh->outreq->buf = bh->buf = g_buffhd_buf0;
    ++bh;
    bh->inreq->buf = bh->outreq->buf = bh->buf = g_buffhd_buf1;
    return usb_function_register(&msg_usb_func);
}

int  msg_mod_uninit(void)
{
    usb_function_unregister(&msg_usb_func);
    return 0;
}

int partition_init(int partition_start, int partition_len, int reserver_for_badblocks, int r_only)
{
    int ret;
    read_only = r_only;

    flash.dev = spi_flash_probe(CONFIG_SF_SAMPLE_DELAY,\
            CONFIG_SF_DEFAULT_CS, CONFIG_SF_DEFAULT_SPEED, CONFIG_SF_DEFAULT_MODE);
    if (!flash.dev) {
        printf ("%s, spi flash probe failed\n", __func__);
        return -1;
    }
    flash.blk_size = spi_flash_getsize(flash.dev, SPI_FLASH_SIZE_BLOCK);
    switch(flash.blk_size) {
    case 128*1024:
        flash.blk_bit = 17;
        break;
    default:
        flash.blk_bit = 17;
        break;
    }
    flash.page_size = spi_flash_getsize(flash.dev, SPI_FLASH_SIZE_PAGE);
    switch(flash.page_size) {
    case 2048:
        flash.page_bit = 11;
        break;
    case 512:
        flash.page_bit = 9;
        break;
    default:
        flash.page_size = 2048;
        flash.page_bit = 11;
        break;
    }

    flash.partition_start = partition_start;
    flash.partition_len = partition_len;
    flash.partition_blks = (flash.partition_len-reserver_for_badblocks)/flash.blk_size;
    flash.patition_pages = flash.partition_blks *(flash.blk_size/flash.page_size);

    DBG("partition start is %x have %u blocks\n",flash.partition_start,flash.partition_blks);

    ret = spi_flash_write_protect_unlock(flash.dev);
    if(ret < 0){
        printf("unlock write protect error!\n");
        return -1;
    }

    return 0;
}

/*
 * 指定 mass storage 所操作的 flash 空间
 * partition_start        : 分区起始地址
 * partition_len          : 分区总长度
 * reserver_for_badblocks : 允许坏块占用的空间，nor flash 配置为 0, nand 根据需要分区大小进行配置
 *
 * 实际可用空间为 partition_len - reserver_for_badblocks
 */
int MassstorageInit(int partition_start, int partition_len, int reserver_for_badblocks)
{
    int ret = 0;

#ifdef CONFIG_MCU_UDC_ENABLE_MS_RD_ONLY
    ret = partition_init(partition_start, partition_len, reserver_for_badblocks, 1);
#else
    ret = partition_init(partition_start, partition_len, reserver_for_badblocks, 0);
#endif

    return ret;
}

/*
 * 释放 MassstorageInit 时创建的资源
 */
int MassstorageDone(void)
{
    return 0;
}

int MsInit(void)
{
    int ret;
    //ret = flash_init();
    ret = msg_mod_init();

    return ret;
}

int MsDone(void)
{
    int ret;
    ret = msg_mod_uninit();

    return ret;
}
