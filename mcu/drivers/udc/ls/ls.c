/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * ls.c -- USB gadget like serial function driver
 *
 */

#include <stdio.h>
#include <common.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include <driver/delay.h>
#include <driver/irq.h>

#ifdef CONFIG_MCU_UDC_ENABLE_LS_SEND_DATA_TO_UAC_CORE
#include <driver/uac2_core.h>
#endif

#if 0
#define DBG(fmt, ...) printf (fmt, ##__VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif


#define GS_FUNC_SERIAL_IDX 0
//default 1 ,max set to 4
#define MAX_U_SERIAL_PORTS    1
//static unsigned n_ports = 1;
#define QUEUE_SIZE 1

#define USB_SERIAL_HS_MAX_PACKAET_LEN 512
#define USBSERIAL_BUFFER_SIZE 2048
static char s_usbserial_in[USBSERIAL_BUFFER_SIZE] = {0,};
static char s_usbserial_out[USBSERIAL_BUFFER_SIZE] = {0,};
static char s_read_ureq_buf[USB_SERIAL_HS_MAX_PACKAET_LEN] = {0,};
static char s_write_ureq_buf[USB_SERIAL_HS_MAX_PACKAET_LEN] = {0,};

struct f_serial_opts {
    struct usb_function_instance func_inst;
    u8                           port_num;
};

typedef struct circbuf {
    unsigned int  size;      /* current number of bytes held */
    unsigned int  totalsize; /* number of bytes allocated */

    char         *top;       /* pointer to current buffer start */
    char         *tail;      /* pointer to space for next element */

    char         *data;      /* all data */
    char         *end;       /* end of data buffer */
} circbuf_t;

static int buf_init (circbuf_t * buf, unsigned int size,bool is_serial_in)
{
    buf->size = 0;

    if (size > USBSERIAL_BUFFER_SIZE)
        return -1;

    buf->totalsize = size;
    if (is_serial_in)
        buf->data = s_usbserial_in;
    else
        buf->data = s_usbserial_out;

    buf->top = buf->data;
    buf->tail = buf->data;
    buf->end = &(buf->data[size]);

    return 1;
}

static int buf_free (circbuf_t * buf)
{
    memset (buf, 0, sizeof (circbuf_t));
    return 1;
}

static int buf_pop (circbuf_t * buf, char *dest, unsigned int len)
{
    unsigned int i;
    char *p = buf->top;

    /* Cap to number of bytes in buffer */
    if (len > buf->size)
        len = buf->size;

    for (i = 0; i < len; i++) {
        dest[i] = *p++;
        /* Bounds check. */
        if (p == buf->end) {
            p = buf->data;
        }
    }

    /* Update 'top' pointer */
    buf->top = p;
    buf->size -= len;

    return len;
}

static unsigned buf_data_avail(circbuf_t * buf)
{
    return buf->size;
}

#ifndef CONFIG_MCU_UDC_ENABLE_LS_SEND_DATA_TO_UAC_CORE
static int buf_push (circbuf_t * buf, const char *src, unsigned int len)
{
    /* NOTE:  this function allows push to overwrite old data. */
    unsigned int i;
    char *p = buf->tail;

    for (i = 0; i < len; i++) {
        *p++ = src[i];
        if (p == buf->end) {
            p = buf->data;
        }
        /* Make sure pushing too much data just replaces old data */
        if (buf->size < buf->totalsize) {
            buf->size++;
        } else {
            buf->top++;
            if (buf->top == buf->end) {
                buf->top = buf->data;
            }
        }
    }

    /* Update 'tail' pointer */
    buf->tail = p;

    return len;
}

static int fill_buffer (circbuf_t * buf, const char *src, unsigned int len)
{

    if (src && len) {
        unsigned int nb = 0;
        unsigned int rx_avail = buf->totalsize - buf->size;

        nb = min(rx_avail, len);
        buf_push (buf, src, nb);

        return nb;
    }
    return 0;
}
#endif

enum buf_state {
    WRITE_STATE_NONE = 0,
    WRITE_STATE_BUSY,
    WRITE_STATE_FINISH
};

struct gs_port {
    struct gserial      *port_usb;

    bool                 openclose;    /* open/close in progress */
    u8                   port_num;
    u8                   count;

    struct list_head     read_pool;
    int                  read_started;
    int                  read_allocated;
    struct list_head     read_queue;
    unsigned             n_read;
    circbuf_t            port_read_buf;
    void                *read_req_buf_ptr;

    struct list_head     write_pool;
    int                  write_started;
    int                  write_allocated;
    circbuf_t            port_write_buf;
    void                *write_req_buf_ptr;

    bool                 write_busy;
    enum buf_state       write_state;
};

struct gserial {
    struct usb_function     func;

    /* port is managed by gserial_{connect,disconnect} */
    struct gs_port         *ioport;

    struct usb_ep          *in;
    struct usb_ep          *out;
};

static struct usb_function_instance *fi_serial[MAX_U_SERIAL_PORTS];
static struct usb_function          *f_serial[MAX_U_SERIAL_PORTS];
static struct gs_port                       g_gs_port;
static struct f_gser                        g_gser;
static struct f_serial_opts                 g_serial_opts;

/*
 * This function packages a simple "generic serial" port with no real
 * control mechanisms, just raw data transfer over two bulk endpoints.
 *
 * Because it's not standardized, this isn't as interoperable as the
 * CDC ACM driver.  However, for many purposes it's just as functional
 * if you can arrange appropriate host side drivers.
 */

struct f_gser {
    struct gserial    port;
    u8                data_id;
    u8                port_num;
};

static inline struct f_gser *func_to_gser(struct usb_function *f)
{
    return container_of(f, struct f_gser, port.func);
}

/*-------------------------------------------------------------------------*/

/* interface descriptor: */

static struct usb_interface_descriptor gser_interface_desc = {
    .bLength            =  USB_DT_INTERFACE_SIZE,
    .bDescriptorType    =  USB_DT_INTERFACE,
    /* .bInterfaceNumber = DYNAMIC */
    .bNumEndpoints      =  2,
    .bInterfaceClass    =  USB_CLASS_VENDOR_SPEC,
    .bInterfaceSubClass =  0,
    .bInterfaceProtocol =  0,
    /* .iInterface = DYNAMIC */
};

/* full speed support: */

static struct usb_endpoint_descriptor gser_fs_in_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,
    .bEndpointAddress =  USB_DIR_IN,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
};

static struct usb_endpoint_descriptor gser_fs_out_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,
    .bEndpointAddress =  USB_DIR_OUT,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
};

static struct usb_descriptor_header *gser_fs_function[] = {
    (struct usb_descriptor_header *) &gser_interface_desc,
    (struct usb_descriptor_header *) &gser_fs_in_desc,
    (struct usb_descriptor_header *) &gser_fs_out_desc,
    NULL,
};

/* high speed support: */

static struct usb_endpoint_descriptor gser_hs_in_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,
    .bmAttributes    =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize  =  cpu_to_le16(512),
};

static struct usb_endpoint_descriptor gser_hs_out_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,
    .bmAttributes    =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize  =  cpu_to_le16(512),
};

static struct usb_descriptor_header *gser_hs_function[] = {
    (struct usb_descriptor_header *) &gser_interface_desc,
    (struct usb_descriptor_header *) &gser_hs_in_desc,
    (struct usb_descriptor_header *) &gser_hs_out_desc,
    NULL,
};

/* string descriptors: */

static struct usb_string gser_string_defs[] = {
    [0].s = "Generic Serial",
    {  } /* end of list */
};

static struct usb_gadget_strings gser_string_table = {
    .language = 0x0409,    /* en-us */
    .strings  = gser_string_defs,
};

static struct usb_gadget_strings *gser_strings[] = {
    &gser_string_table,
    NULL,
};

static struct usb_request *gs_alloc_req(struct usb_ep *ep, unsigned len, char *req_buf)
{
    struct usb_request *req;

    req = usb_ep_alloc_request(ep);

    if (req != NULL) {
        req->length = len;
        req->buf = req_buf;
        if (req->buf == NULL) {
            usb_ep_free_request(ep, req);
            return NULL;
        }
    }

    return req;
}

/*
 * gs_free_req
 *
 * Free a usb_request and its buffer.
 */
static void gs_free_req(struct usb_ep *ep, struct usb_request *req)
{
    usb_ep_free_request(ep, req);

    return ;
}

/*
 * gs_send_packet
 *
 * If there is data to send, a packet is built in the given
 * buffer and the size is returned.  If there is no data to
 * send, 0 is returned.
 *
 * Called with port_lock held.
 */
static unsigned gs_send_packet(struct gs_port *port, char *packet, unsigned size)
{
    unsigned len;

    len = buf_data_avail(&port->port_write_buf);
    if (len < size)
        size = len;
    if (size != 0)
        size = buf_pop(&port->port_write_buf, packet, size);
    return size;
}

/*
 * gs_start_tx
 *
 * This function finds available write requests, calls
 * gs_send_packet to fill these packets with data, and
 * continues until either there are no more write requests
 * available or no more data to send.  This function is
 * run whenever data arrives or write requests are available.
 *
 * Context: caller owns port_lock; port_usb is non-null.
 */
static int gs_start_tx(struct gs_port *port)
{
    struct list_head    *pool = &port->write_pool;
    struct usb_ep       *in = port->port_usb->in;
    struct usb_request  *req;
    unsigned int         xfer_len;
    int                  status = 0;

    if (port->write_state == WRITE_STATE_BUSY)
        return 0;

    req = list_entry(pool->next, struct usb_request, list);
    if (!req) {
        DBG("%s, req is NULL\n", __func__);
        return -1;
    }

    if (buf_data_avail(&port->port_write_buf) == 0)
        return 0;

    port->write_state = WRITE_STATE_NONE;
    xfer_len    = gs_send_packet(port, req->buf, in->maxpacket);
    req->length = xfer_len;
    req->zero   = 0;
    port->write_state = WRITE_STATE_BUSY;

    status = usb_ep_queue(in, req);
    if (status) {
        DBG("%s, usb ep queue failed! ret : %d\n", __func__, status);
        return -2;
    }

    return status;
}

/*
 * Context: caller owns port_lock, and port_usb is set
 */
static int gs_start_rx(struct gs_port *port)
{
    struct list_head    *pool = &port->read_pool;
    struct usb_ep        *out = port->port_usb->out;
    struct usb_request    *req;
    int            status;

    if (!port->port_usb)
        return -1;

    req = list_entry(pool->next, struct usb_request, list);
    req->length = out->maxpacket;
    status = usb_ep_queue(out, req);
    if (status) {
        printf ("%s: %s %s err %d\n", __func__, "queue", out->name, status);
        return status;
    }

    return 0;
}

static void gs_rx_push(unsigned long _port)
{
    struct gs_port        *port = (void *)_port;
    struct list_head    *queue = &port->read_pool;
    bool            disconnect = false;
    struct usb_request    *req;

    req = list_entry(queue->next, struct usb_request, list);
    switch (req->status) {
    case -ESHUTDOWN:
        disconnect = true;
        DBG("ttyGS%d: shutdown\n", port->port_num);
        break;

    default:
        /* presumably a transient fault */
        DBG("ttyGS%d: unexpected RX status %d\n", port->port_num, req->status);
        /* FALLTHROUGH */
    case 0:
        /* normal completion */
        break;
    }

    /* push data to (open) upper layer ring buffer*/
    if (req->actual) {
#ifdef CONFIG_MCU_UDC_ENABLE_LS_SEND_DATA_TO_UAC_CORE
        uac_core_put_data(req->actual);
        req->buf = (void *)get_next_buff();
#else
        char        *packet = req->buf;
        unsigned    size = req->actual;

        fill_buffer(&port->port_read_buf,packet,size);
        port->n_read = 0;
 #endif
    }

    /* If we're still connected, refill the USB RX queue. */
    if (!disconnect && port->port_usb)
        gs_start_rx(port);

    return ;
}

static void gs_read_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct gs_port    *port = ep->driver_data;

    gs_rx_push((unsigned long)port);

    return ;
}

static void gs_write_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct gs_port    *port = ep->driver_data;

    port->write_state = WRITE_STATE_FINISH;

    if (buf_data_avail(&port->port_write_buf))
        gs_start_tx(port);

    return ;
}

static void gs_free_requests(struct usb_ep *ep, struct list_head *head)
{
    struct usb_request    *req;

    while (!list_empty(head)) {
        req = list_entry(head->next, struct usb_request, list);
        list_del(&req->list);
        gs_free_req(ep, req);
    }

    return ;
}

static int gs_alloc_requests(struct usb_ep *ep, struct list_head *head,
        void (*fn)(struct usb_ep *, struct usb_request *),
        char *req_buf)
{
    int            i;
    struct usb_request    *req;
    //int n = allocated ? QUEUE_SIZE - *allocated : QUEUE_SIZE;
    //currently only support one usb_request for each ep.
    int n = QUEUE_SIZE;

    /* Pre-allocate up to QUEUE_SIZE transfers, but if we can't
     * do quite that many this time, don't fail ... we just won't
     * be as speedy as we might otherwise be.
     */
    for (i = 0; i < n; i++) {
        req = gs_alloc_req(ep, ep->maxpacket,req_buf);
        if (!req)
            return list_empty(head) ? -ENOMEM : 0;
        req->complete = fn;
        list_add_tail(&req->list, head);
    }
    return 0;
}

/**
 * gs_start_io - start USB I/O streams
 * @dev: encapsulates endpoints to use
 * Context: holding port_lock; port_tty and port_usb are non-null
 *
 * We only start I/O when something is connected to both sides of
 * this port.  If nothing is listening on the host side, we may
 * be pointlessly filling up our TX buffers and FIFO.
 */
static int gs_start_io(struct gs_port *port)
{
    struct list_head    *head = &port->read_pool;
    struct usb_ep        *ep = port->port_usb->out;
    int            status;
    unsigned        ret;

    status = gs_alloc_requests(ep, head, gs_read_complete,port->read_req_buf_ptr);
    if (status)
        return status;

    status = gs_alloc_requests(port->port_usb->in, &port->write_pool,
            gs_write_complete, port->write_req_buf_ptr);
    if (status) {
        gs_free_requests(ep, head);
        return status;
    }

    /* queue read requests */
    port->n_read = 0;
    ret = gs_start_rx(port);
    /* unblock any pending writes into our circular buffer */
    if (ret) {
        gs_free_requests(ep, head);
        gs_free_requests(port->port_usb->in, &port->write_pool);
        status = -EIO;
    }

    return status;
}

static int gserial_connect(struct gserial *gser)
{
    struct gs_port    *port;
    int        status;

    port = gser->ioport;

    if (port->port_usb) {
        DBG("serial line 0 is in use.\n");
        return -EBUSY;
    }

    /* activate the endpoints */
    status = usb_ep_enable(gser->in);
    if (status < 0)
        return status;
    gser->in->driver_data = port;

    status = usb_ep_enable(gser->out);
    if (status < 0)
        goto fail_out;
    gser->out->driver_data = port;

    /* then tell the tty glue that I/O can work */
    gser->ioport = port;
    port->port_usb = gser;

    /* REVISIT if waiting on "carrier detect", signal. */

    /* if it's already open, start I/O ... and notify the serial
     * protocol about open/close status (connect/disconnect).
     */
    if (port->count) {
        DBG("gserial_connect: start ttyGS%d\n", port->port_num);
        gs_start_io(port);
    }
    return status;

fail_out:
    usb_ep_disable(gser->in);
    return status;
}

static void gserial_disconnect(struct gserial *gser)
{
    struct gs_port    *port = gser->ioport;

    if (!port)
        return;

    port->port_usb = NULL;
    gser->ioport = NULL;
    if (port->count > 0 || port->openclose) {
        ;
    }

    /* disable endpoints, aborting down any active I/O */
    usb_ep_disable(gser->out);
    usb_ep_disable(gser->in);

    /* finally, free any unused/unusable I/O buffers */
    if (port->count == 0 && !port->openclose)
        buf_free(&port->port_write_buf);
    gs_free_requests(gser->out, &port->read_pool);
    gs_free_requests(gser->out, &port->read_queue);
    gs_free_requests(gser->in, &port->write_pool);

    port->read_allocated = port->read_started =
        port->write_allocated = port->write_started = 0;

    return ;
}
/*-------------------------------------------------------------------------*/

static int gser_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
    struct f_gser        *gser = func_to_gser(f);
    struct usb_composite_dev *cdev = f->config->cdev;

    /* we know alt == 0, so this is an activation or a reset */

    if (gser->port.in->enabled) {
        DBG("reset generic ttyGS%d\n", gser->port_num);
        gserial_disconnect(&gser->port);
    }
    if (!gser->port.in->desc || !gser->port.out->desc) {
        DBG("activate generic ttyGS%d\n", gser->port_num);
        if (config_ep_by_speed(cdev->gadget, f, gser->port.in) ||
                config_ep_by_speed(cdev->gadget, f, gser->port.out)) {
            gser->port.in->desc = NULL;
            gser->port.out->desc = NULL;
            return -EINVAL;
        }
    }
    gserial_connect(&gser->port);
    return 0;
}

static void gser_disable(struct usb_function *f)
{
    struct f_gser    *gser = func_to_gser(f);

    DBG("generic ttyGS%d deactivated\n", gser->port_num);
    gserial_disconnect(&gser->port);

    return ;
}

/*-------------------------------------------------------------------------*/

/* serial function driver setup/binding */
static int gser_bind(struct usb_configuration *c, struct usb_function *f)
{
    struct usb_composite_dev *cdev = c->cdev;
    struct f_gser        *gser = func_to_gser(f);
    int            status;
    struct usb_ep        *ep;
    struct usb_string *us;

    /* REVISIT might want instance-specific strings to help
     * distinguish instances ...
     */
    us = usb_gstrings_attach(cdev, gser_strings, ARRAY_SIZE(gser_strings));
    if (!us)
        return -ENOMEM;

    gser_interface_desc.iInterface = us[GS_FUNC_SERIAL_IDX].id;
    /* allocate instance-specific interface IDs */
    status = usb_interface_id(c, f);
    if (status < 0)
        goto fail;
    gser->data_id = status;
    gser_interface_desc.bInterfaceNumber = status;

    status = -ENODEV;

    /* allocate instance-specific endpoints */
    ep = usb_ep_autoconfig(cdev->gadget, &gser_fs_in_desc);
    if (!ep)
        goto fail;
    gser->port.in = ep;

    ep = usb_ep_autoconfig(cdev->gadget, &gser_fs_out_desc);
    if (!ep)
        goto fail;
    gser->port.out = ep;

    //there are only one port currently.
    g_gs_port.port_num = 0;
    g_gs_port.write_state = WRITE_STATE_NONE;
    INIT_LIST_HEAD(&g_gs_port.read_pool);
    INIT_LIST_HEAD(&g_gs_port.read_queue);
    INIT_LIST_HEAD(&g_gs_port.write_pool);
    buf_init(&g_gs_port.port_read_buf,USBSERIAL_BUFFER_SIZE,false);
    buf_init(&g_gs_port.port_write_buf,USBSERIAL_BUFFER_SIZE,true);
    g_gs_port.read_req_buf_ptr = s_read_ureq_buf;
    g_gs_port.write_req_buf_ptr = s_write_ureq_buf;

    gser->port.ioport = &g_gs_port;

    /* support all relevant hardware speeds... we expect that when
     * hardware is dual speed, all bulk-capable endpoints work at
     * both speeds
     */
    gser_hs_in_desc.bEndpointAddress = gser_fs_in_desc.bEndpointAddress;
    gser_hs_out_desc.bEndpointAddress = gser_fs_out_desc.bEndpointAddress;

    status = usb_assign_descriptors(f, gser_fs_function, gser_hs_function,NULL);
    if (status)
        goto fail;

    return 0;

fail:
    DBG("%s: can't bind, err %d\n", f->name, status);

    return status;
}

static void gser_free_inst(struct usb_function_instance *f)
{
#if 0
    struct f_serial_opts *opts;
    opts = container_of(f, struct f_serial_opts, func_inst);
#endif
    return ;
}

static struct usb_function_instance *gser_alloc_inst(void)
{
    struct f_serial_opts *opts;

    opts = &g_serial_opts;
    opts->func_inst.free_func_inst = gser_free_inst;

    return &opts->func_inst;
}

static void gser_free(struct usb_function *f)
{
#if 0
    struct f_gser *serial;
    serial = func_to_gser(f);
#endif
    return ;
}

static void gser_unbind(struct usb_configuration *c, struct usb_function *f)
{
    usb_free_all_descriptors(f);

    return ;
}

static struct usb_function *gser_alloc(struct usb_function_instance *fi)
{
    struct f_gser    *gser;
    struct f_serial_opts *opts;

    /* allocate and initialize one new instance */
    gser = &g_gser;

    opts = container_of(fi, struct f_serial_opts, func_inst);

    gser->port_num = opts->port_num;

    gser->port.func.name = "gser";
    gser->port.func.strings = gser_strings;
    gser->port.func.bind = gser_bind;
    gser->port.func.unbind = gser_unbind;
    gser->port.func.set_alt = gser_set_alt;
    gser->port.func.disable = gser_disable;
    gser->port.func.free_func = gser_free;

    return &gser->port.func;
}


/****************************** Configurations ******************************/
#define USBLS_NAME "LikeSerial"
static struct usb_function_driver serial_func = {
    .name       = USBLS_NAME,
    .alloc_inst = gser_alloc_inst,
    .alloc_func = gser_alloc,
};

int  usbls_mod_init(void)
{
    return usb_function_register(&serial_func);
}

int  usbls_mod_uninit(void)
{
    usb_function_unregister(&serial_func);
    return 0;
}

int UsbLSDoConfig(struct usb_configuration *c)
{
    int status;

    f_serial[0] = usb_get_function(fi_serial[0]);
    status = usb_add_function(c, f_serial[0]);
    if (status < 0)
    {
        usb_put_function(f_serial[0]);
    }

    return status;
}

int UsbLSBind(struct usb_composite_dev *cdev)
{
    fi_serial[0] = usb_get_function_instance(USBLS_NAME);
    if (!fi_serial[0]) {
        return -ENODEV;
    }

    return 0;
}

int UsbLSUnbind(struct usb_composite_dev *cdev)
{
    if (f_serial[0])
        usb_put_function(f_serial[0]);
    if (fi_serial[0])
        usb_put_function_instance(fi_serial[0]);

    return 0;
}

int UsbLSInit(void)
{
    int ret;
    ret = usbls_mod_init();

    return ret;
}

int UsbLSDone(void)
{
    int ret;
    ret = usbls_mod_uninit();

    return ret;
}

/*                      API                      */

int UsbLSOpen(void)
{
    struct gs_port    *port;
    int        status = 0;
    port = g_gser.port.ioport;

    if (!port)
        status = -ENODEV;
    //already open?
    if(port->count)
        return 0;

    port->openclose = true;

    /* Do the "real open" */

    /* allocate circular buffer on first open */
    if (port->port_write_buf.data == NULL) {
        //status = gs_buf_alloc(&port->port_write_buf, WRITE_BUF_SIZE);
        status = buf_init(&g_gs_port.port_write_buf,USBSERIAL_BUFFER_SIZE,true);

        if (status != 1) {
            DBG("gs_open: ttyGS%d no buffer\n",
                    port->port_num);
            port->openclose = false;
            goto exit_port;
        }
    }

    port->count = 1;
    port->openclose = false;

    /* if connected, start the I/O stream */
    if (port->port_usb) {
        DBG("gs_open: start ttyGS%d\n", port->port_num);
        gs_start_io(port);
    }
    DBG("gs_open: ttyGS%d \n", port->port_num);
    status = (int)port;
exit_port:
    return status;
}

void UsbLSClose(int handle)
{
    struct gs_port *port = (struct gs_port *)handle;
    struct gserial    *gser;

    if (port->count != 1) {
        if (port->count == 0)
            DBG("warning: close unopened GS device!\n");
        else
            --port->count;
        goto exit;
    }

    DBG("gs_close: ttyGS%d...\n", port->port_num);

    /* mark port as closing but in use; we can drop port lock
     * and sleep if necessary
     */
    port->openclose = true;
    port->count = 0;
    gser = port->port_usb;
    if (!gser)
        buf_free(&port->port_write_buf);

    port->openclose = false;
exit:
    DBG("gs_close: ttyGS%d done!\n", port->port_num);

    return ;
}

#if 0
int GsWrite(int handle, const  char *buf, int count)
{
    struct gs_port    *port = (struct gs_port    *)handle;
    int        ret;

    DBG("GsWrite: ttyGS%d writing %d bytes\n",
            port->port_num, count);

    if (count) {
        gx_disable_irq();
        count = buf_push(&port->port_write_buf, buf, count);
        gx_enable_irq();
    }

    ret = gs_start_tx(port);
    if (ret)
        return -1;

    return count;
}

int GsRead(int handle, char *buf, int count)
{
    struct gs_port    *port = (struct gs_port    *)handle;

    DBG("GsRead: ttyGS%d reading %d bytes\n",
            port->port_num, count);

    if (count) {
        gx_disable_irq();
        count = buf_pop(&port->port_read_buf, buf, count);
        gx_enable_irq();
    }

    return count;
}
#endif
