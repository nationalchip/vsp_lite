/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * uac1.h : uac 1.0
 */

#ifndef __UAC1_H__
#define __UAC1_H__

#include "composite.h"

int Uac1Init(const UAC2_CHANNEL_CONFIG *audio_in_config,
        const UAC2_CHANNEL_CONFIG *audio_out_config,
        const UAC2_DEVICE_DESCRIPTION *device_description,
        const UAC2_CALLBACKS *callbacks,
        void *private_data);
int Uac1Done(void);

int Uac1DoConfig(struct usb_configuration *c);
int Uac1Bind(struct usb_composite_dev *cdev);
int Uac1Unbind(struct usb_composite_dev *cdev);

#endif
