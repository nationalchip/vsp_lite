#ifndef U_UAC2_H
#define U_UAC2_H

#include "../include/composite.h"
#include <driver/usb_gadget.h>

#define UAC2_DEF_PCHMASK 0x3
#define UAC2_DEF_PSRATE  48000
#define UAC2_DEF_PSSIZE  2
#define UAC2_DEF_CCHMASK 0x3
#define UAC2_DEF_CSRATE  48000
#define UAC2_DEF_CSSIZE  2

struct f_uac2_opts {
    struct usb_function_instance  func_inst;
    int                           p_chmask;
    int                           p_srate;
    int                           p_ssize;
    int                           c_chmask;
    int                           c_srate;
    int                           c_ssize;
    bool                          bound;
    int                           refcnt;
};

typedef int (*UAC2_SOF_TIMER_CALLBACK)(void);

struct uac_internal_callbacks {
    UAC2_CALLBACKS           callback_ops;
    UAC2_SOF_TIMER_CALLBACK  sof_timer_callback;
    u32                      sof_timer_interval;
    volatile u32             sof_incr;
    void                    *private_data;
    unsigned int             state;
};

#endif
