#ifndef _HID_GX_H
#define _HID_GX_H

#include "composite.h"


/*
 * USB HID (Human Interface Device) interface class code
 */

#define USB_INTERFACE_CLASS_HID        3

/*
 * USB HID interface subclass and protocol codes
 */

#define USB_INTERFACE_SUBCLASS_BOOT    1
#define USB_INTERFACE_PROTOCOL_KEYBOARD    1
#define USB_INTERFACE_PROTOCOL_MOUSE    2

/*
 * HID class requests
 */

#define HID_REQ_GET_REPORT        0x01
#define HID_REQ_GET_IDLE        0x02
#define HID_REQ_GET_PROTOCOL        0x03
#define HID_REQ_SET_REPORT        0x09
#define HID_REQ_SET_IDLE        0x0A
#define HID_REQ_SET_PROTOCOL        0x0B

/*
 * HID class descriptor types
 */

#define HID_DT_HID            (USB_TYPE_CLASS | 0x01)
#define HID_DT_REPORT            (USB_TYPE_CLASS | 0x02)
#define HID_DT_PHYSICAL            (USB_TYPE_CLASS | 0x03)

#define HID_MAX_DESCRIPTOR_SIZE        4096


struct hidg_func_descriptor {
    unsigned char        subclass;
    unsigned char        protocol;
    unsigned short        report_length;
    unsigned short        report_desc_length;
    unsigned char        report_desc[];
};

struct f_hid_opts {
    struct usb_function_instance    func_inst;
    unsigned char            subclass;
    unsigned char            protocol;
    unsigned short            report_length;
    unsigned short            report_desc_length;
    unsigned char            *report_desc;
    bool                report_desc_alloc;

    /*
     * Protect the data form concurrent access by read/write
     * and create symlink/remove symlink.
     */
    int                refcnt;
};

//int ghid_setup(struct usb_gadget *g, int count);
//void ghid_cleanup(void);


/* Variable Length Array Macros **********************************************/
#define vla_group(groupname) size_t groupname##__next = 0
#define vla_group_size(groupname) groupname##__next

#define vla_item(groupname, type, name, n) \
    size_t groupname##_##name##__offset = ({                   \
            size_t align_mask = __alignof__(type) - 1;               \
            size_t offset = (groupname##__next + align_mask) & ~align_mask;\
            size_t size = (n) * sizeof(type);                   \
            groupname##__next = offset + size;                   \
            offset;                                   \
            })

#define vla_item_with_sz(groupname, type, name, n) \
    size_t groupname##_##name##__sz = (n) * sizeof(type);               \
    size_t groupname##_##name##__offset = ({                   \
            size_t align_mask = __alignof__(type) - 1;               \
            size_t offset = (groupname##__next + align_mask) & ~align_mask;\
            size_t size = groupname##_##name##__sz;                   \
            groupname##__next = offset + size;                   \
            offset;                                   \
            })

#define vla_ptr(ptr, groupname, name) \
    ((void *) ((char *)ptr + groupname##_##name##__offset))

struct usb_ep;
struct usb_request;

struct usb_request *alloc_ep_req(struct usb_ep *ep, int len, int default_len);

struct hid_class_descriptor {
    __u8  bDescriptorType;
    __le16 wDescriptorLength;
} __attribute__ ((packed));

struct hid_descriptor {
    __u8  bLength;
    __u8  bDescriptorType;
    __le16 bcdHID;
    __u8  bCountryCode;
    __u8  bNumDescriptors;

    struct hid_class_descriptor desc[1];
} __attribute__ ((packed));


#endif

