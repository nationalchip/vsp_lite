/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * ms.h : mass storage
 *
 */

#ifndef __MS_H__
#define __MS_H__

#include "composite.h"

int MsInit(void);
int MsDone(void);

int MsgBind(struct usb_composite_dev *cdev);
int MsgUnbind(struct usb_composite_dev *cdev);
int MsgDoConfig(struct usb_configuration *c);

#endif
