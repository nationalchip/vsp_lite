/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * hid.h : usb human interace device driver
 *
 */

#ifndef __HID_H__
#define __HID_H__

#include "composite.h"

int HidInit(void);
int HidDone(void);

int HidBind(struct usb_composite_dev *cdev);
int HidUnbind(struct usb_composite_dev *cdev);
int HidDoConfig(struct usb_configuration *c);

#endif
