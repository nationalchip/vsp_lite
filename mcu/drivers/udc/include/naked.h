/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * naked.h : usb read/write without protocol
 *
 */

#ifndef __NAKED_H__
#define __NAKED_H__

#include "composite.h"

int NakedInit(void);
int NakedDone(void);

int NakedBind(struct usb_composite_dev *cdev);
int NakedUnbind(struct usb_composite_dev *cdev);
int NakedDoConfig(struct usb_configuration *c);

#endif
