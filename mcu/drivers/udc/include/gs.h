/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * gs.h : gadget serial
 */

#ifndef __GS_H__
#define __GS_H__

#include "composite.h"

int GsInit(void);
int GsDone(void);

int GsBind(struct usb_composite_dev *cdev);
int GsUnbind(struct usb_composite_dev *cdev);
int GsDoConfig(struct usb_configuration *c);

#endif
