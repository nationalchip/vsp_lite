/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * usbls.h : usb like serial
 */

#ifndef __usbls_h__
#define __usbls_h__

#include "composite.h"

int UsbLSInit(void);
int UsbLSDone(void);

int UsbLSDoConfig(struct usb_configuration *c);
int UsbLSBind(struct usb_composite_dev *cdev);
int UsbLSUnbind(struct usb_composite_dev *cdev);

#endif
