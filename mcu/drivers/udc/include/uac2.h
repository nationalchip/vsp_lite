/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * uac2.h : usb audio class 2.0 api
 *
 */

#ifndef __UAC2_H__
#define __UAC2_H__

#include <driver/usb_gadget.h>
#include "composite.h"

int Uac2Init(const UAC2_CHANNEL_CONFIG *audio_in_config,
        const UAC2_CHANNEL_CONFIG *audio_out_config,
        const UAC2_DEVICE_DESCRIPTION *device_description,
        const UAC2_CALLBACKS *callbacks,
        void *private_data);
int Uac2Done(void);

int Uac2Bind(struct usb_composite_dev *cdev);
int Uac2Unbind(struct usb_composite_dev *cdev);
int Uac2DoConfig(struct usb_configuration *c);

#endif
