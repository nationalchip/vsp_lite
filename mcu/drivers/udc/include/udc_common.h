#ifndef __UDC_COMMON_H
#define __UDC_COMMON_H

#include <common.h>
#include <list.h>


static inline u16 __get_unaligned_be16(const u8 *p)
{
    return p[0] << 8 | p[1];
}

static inline u16 get_unaligned_be16(const void *p)
{
    return __get_unaligned_be16((const u8 *)p);
}


static inline u32 __get_unaligned_be32(const u8 *p)
{
    return p[0] << 24 | p[1] << 16 | p[2] << 8 | p[3];
}

static inline u32 get_unaligned_be32(const void *p)
{
    return __get_unaligned_be32((const u8 *)p);
}


static inline void __put_unaligned_be16(u16 val, u8 *p)
{
    *p++ = val >> 8;
    *p++ = val;
}

static inline void __put_unaligned_be32(u32 val, u8 *p)
{
    __put_unaligned_be16(val >> 16, p);
    __put_unaligned_be16(val, p + 2);
}



static inline void put_unaligned_be32(u32 val, void *p)
{
    __put_unaligned_be32(val, p);
}

static inline void put_unaligned_be16(u32 val, void *p)
{
    __put_unaligned_be16(val, p);
}

struct device {
    struct device        *parent;
    const char        *init_name; /* initial name of the device */
    void        *driver_data;    /* Driver data, set and get with
                                    dev_set/get_drvdata */
    u32            id;    /* device instance */
    struct list_head     devres_head;
    void    (*release)(struct device *dev);
    bool            offline_disabled:1;
    bool            offline:1;
};




#endif
