/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * uac2_gx.c: usb audio class 2.0 driver
 *
 */
#include <stdio.h>
#include <common.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include <driver/delay.h>
#include <driver/clock.h>

#include "../include/audio.h"
#include "../include/audio-v2.h"

#include "../include/u_uac2.h"
#include <driver/usb_gadget.h>
#include <driver/vsp_uac_fifo_manager.h>
#include <driver/uac2_core.h>

extern void uac_core_playback_start_callback(void);
extern void uac_core_playback_stop_callback(void);
extern void uac_core_capture_start_callback(void);
extern void uac_core_capture_stop_callback(void);

#define INFO(fmt, ...) printf (fmt, ##__VA_ARGS__)
#define ERR(fmt, ...) printf (fmt, ##__VA_ARGS__)
#define DBG(fmt, ...)

//#define UAC_SYNC_MODE_ASYNC 1
#define UAC_SYNC_MODE_ADAPT 1


/* Keep everyone on toes */
#define USB_XFERS               1
#define DMA_TIMES_EACH_TRANSFER 8

//#define CUR_HS_EPOUT_MAXPACKET_SIZE 192
#define CUR_HS_EPOUT_NORMAL_MAXPACKET_SIZE 192
#define CUR_HS_EPOUT_MAXPACKET_SIZE        240
#define MAX_FREQ_ADJUST_STEP_LEN           100
#define CUR_HS_EPIN_MAXPACKET_SIZE         12

#define MAX_CHANNEL_NUM           8
#define FAST_FREQ                 0X78000
#define SLOW_FREQ                 0X54000
#define NORMAL_FREQ               0X60000
#define MAX_FRAME_SIZE            3072
#define AOUT_THRED_SCALE_FACTOR   2
#define AOUT_THRED_PERCENTAGE     10
#define AIN_THRED_SCALE_FACTOR    2
#define MAX_FB_RBUF_SIZE          4

static struct audio_dev   g_agdev;
static struct f_uac2_opts g_opts;
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
static u8 g_hs_epout_rbuf[USB_XFERS*CUR_HS_EPOUT_MAXPACKET_SIZE];
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
static u8 g_hs_epin_rbuf[USB_XFERS*DMA_TIMES_EACH_TRANSFER*CUR_HS_EPIN_MAXPACKET_SIZE*MAX_CHANNEL_NUM];
#endif

#ifdef UAC_SYNC_MODE_ASYNC
static u8 g_hs_epout_fb_rbuf[MAX_FB_RBUF_SIZE];
static u8 g_hs_epin_fb_rbuf[MAX_FB_RBUF_SIZE];
#endif

/*
 * The driver implements a simple UAC_2 topology.
 * USB-OUT -> IT_1 -> OT_3 -> ALSA_Capture
 * ALSA_Playback -> IT_2 -> OT_4 -> USB-IN
 * Capture and Playback sampling rates are independently
 *  controlled by two clock sources :
 *    CLK_5 := c_srate, and CLK_6 := p_srate
 */
#define USB_OUT_IT_ID      1
#define IO_IN_IT_ID        2
#define IO_OUT_OT_ID       3
#define USB_IN_OT_ID       4
#define USB_OUT_CLK_ID     5
#define USB_IN_CLK_ID      6
#define FEATURE_UNIT_ID    7
#define FEATURE_UNIT_ID_C  8  // capture, upstream

#define CONTROL_ABSENT   0
#define CONTROL_RDONLY   1
#define CONTROL_RDWR     3

#define CLK_FREQ_CTRL    0
#define CLK_VLD_CTRL     2

#define COPY_CTRL     0
#define CONN_CTRL     2
#define OVRLD_CTRL    4
#define CLSTR_CTRL    6
#define UNFLW_CTRL    8
#define OVFLW_CTRL    10

#define INTERVAL_MS4  4
#define INTERVAL_MS1  1

#define DRIVER_DESC           "GX USB Audio Gadget"
#define DRIVER_VERSION        "AUG 28, 2017"

#define MUTE_CONTROL              (CONTROL_RDWR << 0)
#define VOLUME_CONTROL            (CONTROL_RDWR << 2)
#define BASS_CONTROL              (0x3 << 4)
#define MID_CONTROL               (0x3 << 6)
#define TREBLE_CONTROL            (0x3 << 8)
#define GRAPHIC_EQUALIZER_CONTROL (0x3 << 10)
#define AUTOMATIC_GAIN_CONTROL    (CONTROL_RDWR << 12)
#define DELAY_CONTROL             (0x3 << 14)
#define BASS_BOOST_CONTROL        (CONTROL_RDWR << 16)
#define LOUDNESS_CONTROL          (0x3 << 18)
#define INPUT_GAIN_CONTROL        (0x3 << 20)
#define INPUT_GAIN_PAID_CONTROL   (0x3 << 22)
#define PHASE_INVERTER_CONTROL    (0x3 << 24)
#define UNDERFLOW_CONTROL         (0x3 << 26)
#define OVERFLOW_CONTROL          (0x3 << 28)

static struct usb_function_instance *fi_uac2;
static struct usb_function          *f_uac2;

/* Playback(USB-IN) Default Stereo - Fl/Fr */
static int p_chmask = UAC2_DEF_PCHMASK;
/* Playback Default 48 KHz */
static int p_srate  = UAC2_DEF_PSRATE;
/* Playback Default 16bits/sample */
static int p_ssize  = UAC2_DEF_PSSIZE;
/* Capture(USB-OUT) Default Stereo - Fl/Fr */
static int c_chmask = UAC2_DEF_CCHMASK;
/* Capture Default 48 KHz */
static int c_srate  = UAC2_DEF_CSRATE;
/* Capture Default 16bits/sample */static int c_ssize  = UAC2_DEF_CSSIZE;

// Feature unit status

#define PLAYBACK_VOLUME_MIN -18
#define PLAYBACK_VOLUME_MAX 18
#define PLAYBACK_VOLUME_RES 1
#define PLAYBACK_VOLUME_DEFAULT 0

#define CAPTURE_VOLUME_MIN -18
#define CAPTURE_VOLUME_MAX 18
#define CAPTURE_VOLUME_RES 1
#define CAPTURE_VOLUME_DEFAULT 0
static short cur_volume     = PLAYBACK_VOLUME_DEFAULT;
static int          cur_mute       = 0; // 0 or 1
static int          cur_bass_boost = 0; // 0 or 1
static int          cur_ag         = 0; // 0 or 1

static short cur_volume_c     = CAPTURE_VOLUME_DEFAULT;
static int          cur_mute_c       = 0; // 0 or 1
static int          cur_bass_boost_c = 0; // 0 or 1
static int          cur_ag_c         = 0; // 0 or 1

struct uac2_req {
    struct uac2_rtd_params *pp; /* parent param */
    struct usb_request     *req;
};

struct uac2_rtd_params {
    struct snd_uac2_chip *uac2; /* parent chip */
    bool                  ep_enabled; /* if the ep is enabled */
    /* Size of the ring buffer */
    size_t                dma_bytes;
    unsigned char        *dma_area;

    /* Ring buffer */
    ssize_t                hw_ptr;
    ssize_t                appl_ptr;

    void                  *rbuf;

    size_t                 period_size;

    unsigned               max_psize;
    struct uac2_req        ureq[USB_XFERS];
};

struct snd_uac2_chip {
    struct uac2_rtd_params p_prm;
    struct uac2_rtd_params c_prm;

    /* timekeeping for the playback endpoint */
    unsigned int           p_interval;
    unsigned int           p_residue;

    /* pre-calculated values for playback iso completion */
    unsigned int           p_pktsize;
    unsigned int           p_pktsize_residue;
    unsigned int           p_framesize;
};

struct uac_internal_callbacks our_callback_ops = {{0,0,NULL,NULL,NULL,NULL,NULL},NULL,0,0,NULL,0};
typedef enum freq_state {
    NORMAL_STATE,
    FAST_STATE,
    SLOW_STATE
} freq_state;

struct audio_dev {
    u8                  ac_intf,     ac_alt;
    u8                  as_out_intf, as_out_alt;
    u8                  as_in_intf,  as_in_alt;

    struct usb_ep      *in_ep,     *out_ep;
#ifdef UAC_SYNC_MODE_ASYNC
    struct usb_ep       *in_fb_ep, *out_fb_ep;
    struct usb_request  *req_fb_for_in_fb_ep;
    bool                 req_fb_in_done;
#endif
    struct usb_function  func;

    freq_state           old_freq_state;
    unsigned int         aout_buf_len ;
    unsigned int         aout_high_thrd_sz;
    unsigned int         aout_low_thrd_sz;

    unsigned int         ain_buf_len ;
    unsigned int         ain_high_thrd_sz;
    unsigned int         ain_low_thrd_sz;

    /* The ALSA Sound Card it represents on the USB-Client side */
    struct snd_uac2_chip uac2;
};

struct cntrl_cur_lay1 {
    __u8    bCUR;
};

struct cntrl_range_lay1 {
    __u16   wNumSubRanges;
    __u8    bMIN;
    __u8    bMAX;
    __u8    bRES;
}__attribute__((packed));


struct cntrl_cur_lay2 {
    __u16   wCUR;
};

struct cntrl_range_lay2 {
    __u16   wNumSubRanges;
    __u16   wMIN;
    __u16   wMAX;
    __u16   wRES;
}__attribute__((packed));


struct cntrl_cur_lay3 {
    __u32    dCUR;
};

struct cntrl_range_lay3 {
    __u16    wNumSubRanges;
    __u32    dMIN;
    __u32    dMAX;
    __u32    dRES;
}  __attribute__((packed));

#define UAC2_DT_FEATURE_UNIT_SIZE(ch) (6 + (ch + 1) * 4)
#define DECLARE_UAC2_FEATURE_UNIT_DESCRIPTOR(ch)         \
    struct uac2_feature_unit_descriptor_##ch {           \
        __u8   bLength;                                  \
        __u8   bDescriptorType;                          \
        __u8   bDescriptorSubtype;                       \
        __u8   bUnitID;                                  \
        __u8   bSourceID;                                \
        __le32 bmaControls[ch + 1];                      \
        __u8   iFeature;                                 \
    } __attribute__ ((packed))

static inline struct audio_dev *func_to_agdev(struct usb_function *f)
{
    return container_of(f, struct audio_dev, func);
}

static inline struct audio_dev *uac2_to_agdev(struct snd_uac2_chip *u)
{
    return container_of(u, struct audio_dev, uac2);
}

static inline struct f_uac2_opts *agdev_to_uac2_opts(struct audio_dev *agdev)
{
    return container_of(agdev->func.fi, struct f_uac2_opts, func_inst);
}

static inline uint num_channels(uint chanmask)
{
    uint num = 0;

    while (chanmask) {
        num += (chanmask & 1);
        chanmask >>= 1;
    }

    return num;
}

static void agout_ep_complete(struct usb_ep *ep, struct usb_request *req)
{
    int control_selector = req->stream_id & 0xff;
    int entity_id = req->stream_id >> 8;

    if (entity_id == FEATURE_UNIT_ID) {
        switch (control_selector) {
            case UAC_FU_VOLUME:
                {
                    struct cntrl_cur_lay2 *volume = req->buf;

                    cur_volume = volume->wCUR;

                    if (our_callback_ops.callback_ops.volume_callback)
                        our_callback_ops.callback_ops.volume_callback(UAC2_CONTROL_DOWNSTREAM, cur_volume);
                    DBG ("set volume : %d\n", cur_volume);
                }
                break;
            case UAC_FU_MUTE:
                {
                    struct cntrl_cur_lay1 *mute = req->buf;

                    if (mute->bCUR == 1)
                        cur_mute = 1;
                    else
                        cur_mute = 0;

                    if (our_callback_ops.callback_ops.mute_callback)
                        our_callback_ops.callback_ops.mute_callback(UAC2_CONTROL_DOWNSTREAM, cur_mute);
                    DBG ("set mute : %d\n", cur_mute);
                }
                break;
            case UAC_FU_AUTOMATIC_GAIN:
                {
                    struct cntrl_cur_lay1 *ag = req->buf;

                    if (ag->bCUR == 1)
                        cur_ag = 1;
                    else
                        cur_ag = 0;

                    if (our_callback_ops.callback_ops.auto_gain_control_callback)
                        our_callback_ops.callback_ops.auto_gain_control_callback(UAC2_CONTROL_DOWNSTREAM, cur_ag);
                    DBG ("set ag : %d\n", cur_ag);
                }
                break;
            case UAC_FU_BASS_BOOST:
                {
                    struct cntrl_cur_lay1 *bb = req->buf;

                    if (bb->bCUR == 1)
                        cur_bass_boost = 1;
                    else
                        cur_bass_boost = 0;

                    if (our_callback_ops.callback_ops.bass_boost_callback)
                        our_callback_ops.callback_ops.bass_boost_callback(UAC2_CONTROL_DOWNSTREAM, cur_bass_boost);
                    DBG ("set bass boot : %d\n", cur_bass_boost);
                }
                break;

            default:
                ;
        }
    } else if (entity_id == FEATURE_UNIT_ID_C) {
        switch (control_selector) {
            case UAC_FU_VOLUME:
                {
                    struct cntrl_cur_lay2 *volume = req->buf;

                    cur_volume_c = volume->wCUR;

                    if (our_callback_ops.callback_ops.volume_callback)
                        our_callback_ops.callback_ops.volume_callback(UAC2_CONTROL_UPSTREAM, cur_volume_c);
                    DBG ("set volume : 0x%x\n", cur_volume_c);
                }
                break;
            case UAC_FU_MUTE:
                {
                    struct cntrl_cur_lay1 *mute = req->buf;

                    if (mute->bCUR == 1)
                        cur_mute_c = 1;
                    else
                        cur_mute_c = 0;

                    if (our_callback_ops.callback_ops.mute_callback)
                        our_callback_ops.callback_ops.mute_callback(UAC2_CONTROL_UPSTREAM, cur_mute_c);
                    DBG ("set mute : %d\n", cur_mute_c);
                }
                break;
            case UAC_FU_AUTOMATIC_GAIN:
                {
                    struct cntrl_cur_lay1 *ag = req->buf;

                    if (ag->bCUR == 1)
                        cur_ag_c = 1;
                    else
                        cur_ag_c = 0;

                    if (our_callback_ops.callback_ops.auto_gain_control_callback)
                        our_callback_ops.callback_ops.auto_gain_control_callback(UAC2_CONTROL_UPSTREAM, cur_ag_c);
                    DBG ("set ag : %d\n", cur_ag_c);
                }
                break;
            case UAC_FU_BASS_BOOST:
                {
                    struct cntrl_cur_lay1 *bb = req->buf;

                    if (bb->bCUR == 1)
                        cur_bass_boost_c = 1;
                    else
                        cur_bass_boost_c = 0;

                    if (our_callback_ops.callback_ops.bass_boost_callback)
                        our_callback_ops.callback_ops.bass_boost_callback(UAC2_CONTROL_UPSTREAM, cur_bass_boost_c);
                    DBG ("set bass boot : %d\n", cur_bass_boost_c);
                }
                break;

            default:
                ;
        }
    } else {
        ;
    }

    return ;
}

/*-------------------------------------------------------------------------*/
#if 0
#define AIN_BUFF_SIZE (3*1024)
unsigned char ain_buff[AIN_BUFF_SIZE];
#define W_BUF_SIZE AIN_BUFF_SIZE
static unsigned int w_ptr = 0;
static unsigned int get_ain_buff(unsigned int size)
{
    w_ptr += size;
    if (w_ptr > W_BUF_SIZE)
        w_ptr = 0;

    return (unsigned int)ain_buff + w_ptr;
}
#endif

unsigned int first_xfer = 0;
unsigned int last_xfer_addr, last_xfer_size;

// 丢包数量统计
unsigned int uac_up_left_pkt_num;
unsigned int uac_up_lost_pkt, uac_up_lost_pkt_num, uac_up_lost_pkt_total;
unsigned long long uac_up_lost_pkt_time;

#define TRIGGER_GATE 1950

static int cal_lost_packet_num(unsigned long long time)
{
    int lost_num = 0;

    while (1) {
        if (time < TRIGGER_GATE)
            break;

        if (time <= 1000) {
            lost_num++;
            break;
        }

        time -= 1000;
        lost_num++;
    }

    return lost_num;
}

static void agdev_iso_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct uac2_req *ur = req->context;
    struct uac2_rtd_params *prm = ur->pp;
    struct snd_uac2_chip *uac2 = prm->uac2;
    void * ret_buf = NULL;

    unsigned int ain_handle;
    //unsigned int aout_handle;

    unsigned long long cur_time, time_interval;
    static unsigned long long prev_time;

    ain_handle  = our_callback_ops.callback_ops.ain_buff_handle;
    //aout_handle = our_callback_ops.callback_ops.aout_buff_handle;

    /* i/f shutting down */
    if (!prm->ep_enabled || req->status == -ESHUTDOWN)
        return ;

    req->buf = req->orig_buf;

    if (prm == &uac2->c_prm) {
        uac_core_playback_callback(req->actual);
        req->buf = (void *)get_next_buff();
    }

    if (prm == &uac2->p_prm) {
        if (ain_handle) {
            req->length = uac2->p_pktsize;

            uac_core_capture_callback(req->actual);

            cur_time = get_time_us();
            if (unlikely(first_xfer)) {
                prev_time = cur_time;
                first_xfer = 0;
            }

            time_interval = cur_time - prev_time;
            if (unlikely(time_interval >= TRIGGER_GATE)) {
                uac_up_lost_pkt_num    = cal_lost_packet_num(time_interval);
                uac_up_left_pkt_num       += uac_up_lost_pkt_num;

                // 统计用
                uac_up_lost_pkt        = 1;
                uac_up_lost_pkt_time   = time_interval;
                uac_up_lost_pkt_total += uac_up_lost_pkt_num;
            }
            prev_time = cur_time;

            // 发包策略
            if (likely(req->status == 0)) {
                ret_buf = AudioBuffConsume(ain_handle, last_xfer_size);
                if (likely(ret_buf))
                    req->buf = ret_buf;

                // 丢包发双倍
                if (uac_up_left_pkt_num) {
                    if (!AudioBuffConsumeCheck(ain_handle, req->length * 2)) {
                        req->length *= 2;
                        uac_up_left_pkt_num--;
                    }
                }
            } else {
                if (last_xfer_size != req->length) {
                    uac_up_left_pkt_num++;
                }

                if (!AudioBuffConsumeCheck(ain_handle, req->length * 2)) {
                    req->length *= 2;
                    uac_up_left_pkt_num--;
                }

                req->buf = (void *)last_xfer_addr;
            }

            last_xfer_addr = (unsigned int)req->buf;
            last_xfer_size = req->length;
        }
    }

    if (usb_ep_queue(ep, req))
        ERR("%s, usb requst enqueue failed\n", __func__);

    return;
}

#ifdef UAC_SYNC_MODE_ASYNC
static void agdev_iso_fb_out_complete(struct usb_ep *ep, struct usb_request *req)
{
    unsigned int f;
    struct uac2_req *ur = req->context;

    struct uac2_rtd_params *prm = ur->pp;
    struct snd_uac2_chip *uac2 = prm->uac2;
    //f = __le32_to_cpup(req->buf);
    f = *(unsigned int *)(req->buf);
    f &= 0x0fffffff;
    if (f == 0)
        return;

    uac2->p_pktsize = (f>>16)*uac2->p_framesize;

    if (uac2->p_pktsize != 4 || f != 0x20000)
        ERR("agdev_iso_fb_complete called f = 0x%x uac2->p_pktsize %d!!!!\n",f,uac2->p_pktsize);

    if (usb_ep_queue(ep, req))
        DBG("%d Error!\n", __LINE__);

    return;
}

static void agdev_iso_fb_in_complete(struct usb_ep *ep, struct usb_request *req)
{
    struct audio_dev *agdev;
    agdev = &g_agdev;

    agdev->req_fb_in_done = true;
    if (req->status == 0) {
        our_callback_ops.sof_timer_interval = INTERVAL_MS4;
        our_callback_ops.sof_incr = 0;
    } else {
        our_callback_ops.sof_timer_interval = INTERVAL_MS1;
        our_callback_ops.sof_incr = 0;
    }

    return;
}

static int agdev_sof_timer_callback(void)
{
    struct audio_dev *agdev;
    agdev = &g_agdev;
    unsigned int aout_sdc_addr = -1;
    unsigned int aout_wptr_pos = -1;
    unsigned int data_area_len = 0;
    freq_state new_freq_state = NORMAL_STATE;

    if (agdev->in_fb_ep->enabled)
        if (agdev->req_fb_in_done) {
            agdev->req_fb_in_done = false;
            if (our_callback_ops.callback_ops.audio_out_get_sdc_addr_callback &&
                    our_callback_ops.callback_ops.aout_buff_handle) {
                //get wptr ,rptr and buff size to cal new_freq_state
                aout_sdc_addr = our_callback_ops.callback_ops.audio_out_get_sdc_addr_callback();
                aout_wptr_pos = AudioBUffGetWritePtr(our_callback_ops.callback_ops.aout_buff_handle);

                //calculate new_freq_state corresponding to the usage condition of aout buff
                if (aout_wptr_pos > aout_sdc_addr) {
                    data_area_len = aout_wptr_pos - aout_sdc_addr;
                } else {
                    data_area_len = agdev->aout_buf_len - (aout_sdc_addr - aout_wptr_pos);
                }

                if (data_area_len >= agdev->aout_high_thrd_sz) {
                    new_freq_state = SLOW_STATE;
                } else if (data_area_len <= agdev->aout_low_thrd_sz) {
                    new_freq_state = FAST_STATE;
                } else
                    new_freq_state = NORMAL_STATE;

            }

            if (agdev->old_freq_state != new_freq_state) {
                if (new_freq_state == NORMAL_STATE) {
                    *(unsigned int *)(agdev->req_fb_for_in_fb_ep->buf) = NORMAL_FREQ;
                    agdev->old_freq_state = NORMAL_STATE;
                } else if (new_freq_state == FAST_STATE) {
                    *(unsigned int *)(agdev->req_fb_for_in_fb_ep->buf) = FAST_FREQ;
                    agdev->old_freq_state = FAST_STATE;
                } else if (new_freq_state == SLOW_STATE) {
                    *(unsigned int *)(agdev->req_fb_for_in_fb_ep->buf) = SLOW_FREQ;
                    agdev->old_freq_state = SLOW_STATE;
                }
            }

            if (usb_ep_queue(agdev->in_fb_ep, agdev->req_fb_for_in_fb_ep))
                ERR("%s:%d Error!\n", __func__, __LINE__);
        }

    return 0;
}
#endif

/* --------- USB Function Interface ------------- */
enum {
    STR_ASSOC,
    STR_IF_CTRL,
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    STR_CLKSRC_IN,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    STR_CLKSRC_OUT,
    STR_USB_IT,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    STR_IO_IT,
    STR_USB_OT,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    STR_IO_OT,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    STR_AS_OUT_ALT0,
    STR_AS_OUT_ALT1,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    STR_AS_IN_ALT0,
    STR_AS_IN_ALT1,
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    //STR_FEATURE_UNIT,
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    //STR_FEATURE_UNIT_C,
#endif
};

#if 0
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
static char clksrc_in[8];
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
static char clksrc_out[8];
#endif
#endif

#define DEFALUT_ASSOC_STR "Source/Sink"

static struct usb_string strings_fn[] = {
    [STR_ASSOC].s        =  NULL,
    [STR_IF_CTRL].s      =  "Topology Control",

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    [STR_CLKSRC_IN].s    =  "MIC",
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    [STR_CLKSRC_OUT].s   =  "PCM",
    [STR_USB_IT].s       =  "PCM",
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    [STR_IO_IT].s        =  "MIC",
    [STR_USB_OT].s       =  "USBH In",
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    [STR_IO_OT].s        =  "USBD In",
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    [STR_AS_OUT_ALT0].s  =  "Playback Inactive",
    [STR_AS_OUT_ALT1].s  =  "Playback Active",
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    [STR_AS_IN_ALT0].s   =  "Capture Inactive",
    [STR_AS_IN_ALT1].s   =  "Capture Active",
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    //[STR_FEATURE_UNIT].s = "Playback Feature Unit",
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    //[STR_FEATURE_UNIT_C].s = "Capture Feature Unit",
#endif
    { },
};

static struct usb_gadget_strings str_fn = {
    .language = 0x0409,    /* en-us */
    .strings  = strings_fn,
};

static struct usb_gadget_strings *fn_strings[] = {
    &str_fn,
    NULL,
};

static struct usb_qualifier_descriptor devqual_desc = {
    .bLength            =  sizeof devqual_desc,
    .bDescriptorType    =  USB_DT_DEVICE_QUALIFIER,

    .bcdUSB             =  cpu_to_le16(0x200),
    .bDeviceClass       =  USB_CLASS_MISC,
    .bDeviceSubClass    =  0x02,
    .bDeviceProtocol    =  0x01,
    .bNumConfigurations =  1,
    .bRESERVED          =  0,
};

static struct usb_interface_assoc_descriptor iad_desc = {
    .bLength           =  sizeof iad_desc,
    .bDescriptorType   =  USB_DT_INTERFACE_ASSOCIATION,

    .bFirstInterface   =  0,
    .bInterfaceCount   =  1,
    .bFunctionClass    =  USB_CLASS_AUDIO,
    .bFunctionSubClass =  UAC2_FUNCTION_SUBCLASS_UNDEFINED,
    .bFunctionProtocol =  UAC_VERSION_2,
};

/* Audio Control Interface */
static struct usb_interface_descriptor std_ac_if_desc = {
    .bLength            =  sizeof std_ac_if_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bAlternateSetting  =  0,
    .bNumEndpoints      =  0,
    .bInterfaceClass    =  USB_CLASS_AUDIO,
    .bInterfaceSubClass =  USB_SUBCLASS_AUDIOCONTROL,
    .bInterfaceProtocol =  UAC_VERSION_2,
};

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
/* Clock source for IN traffic */
static struct uac_clock_source_descriptor in_clk_src_desc = {
    .bLength            =  sizeof in_clk_src_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC2_CLOCK_SOURCE,
    .bClockID           =  USB_IN_CLK_ID,
    .bmAttributes       =  UAC_CLOCK_SOURCE_TYPE_INT_FIXED,
    .bmControls         =  (CONTROL_RDONLY << CLK_FREQ_CTRL),
    .bAssocTerminal     =  0,
};
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
/* Clock source for OUT traffic */
static struct uac_clock_source_descriptor out_clk_src_desc = {
    .bLength            =  sizeof out_clk_src_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC2_CLOCK_SOURCE,
    .bClockID           =  USB_OUT_CLK_ID,
    .bmAttributes       =  UAC_CLOCK_SOURCE_TYPE_INT_FIXED,
    .bmControls         =  (CONTROL_RDONLY << CLK_FREQ_CTRL),
    .bAssocTerminal     =  0,
};
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
/* Input Terminal for USB_OUT */
static struct uac2_input_terminal_descriptor usb_out_it_desc = {
    .bLength            =  sizeof usb_out_it_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC_INPUT_TERMINAL,
    .bTerminalID        =  USB_OUT_IT_ID,
    .wTerminalType      =  cpu_to_le16(UAC_TERMINAL_STREAMING),
    .bAssocTerminal     =  0,
    .bCSourceID         =  USB_OUT_CLK_ID,
    .iChannelNames      =  0,
    .bmControls         =  (CONTROL_RDWR << COPY_CTRL),
};
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
/* Input Terminal for I/O-In */
static struct uac2_input_terminal_descriptor io_in_it_desc = {
    .bLength            =  sizeof io_in_it_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC_INPUT_TERMINAL,
    .bTerminalID        =  IO_IN_IT_ID,

#ifdef CONFIG_MCU_UDC_UAC2_CAPTURE_TERMINAL_TYPE
    .wTerminalType      =  cpu_to_le16(CONFIG_MCU_UDC_UAC2_CAPTURE_TERMINAL_TYPE),
#else
    .wTerminalType      =  cpu_to_le16(UAC_INPUT_TERMINAL_MICROPHONE),
#endif

    .bAssocTerminal     =  0,
    .bCSourceID         =  USB_IN_CLK_ID,
    .iChannelNames      =  0,
    .bmControls         =  (CONTROL_RDWR << COPY_CTRL),
};

/* Ouput Terminal for USB_IN */
static struct uac2_output_terminal_descriptor usb_in_ot_desc = {
    .bLength            =  sizeof usb_in_ot_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC_OUTPUT_TERMINAL,
    .bTerminalID        =  USB_IN_OT_ID,
    .wTerminalType      =  cpu_to_le16(UAC_TERMINAL_STREAMING),
    .bAssocTerminal     =  0,
    .bSourceID          =  FEATURE_UNIT_ID_C, //IO_IN_IT_ID,
    .bCSourceID         =  USB_IN_CLK_ID,
    .bmControls         =  (CONTROL_RDWR << COPY_CTRL),
};
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
/* Ouput Terminal for I/O-Out */
static struct uac2_output_terminal_descriptor io_out_ot_desc = {
    .bLength            =  sizeof io_out_ot_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC_OUTPUT_TERMINAL,
    .bTerminalID        =  IO_OUT_OT_ID,

#ifdef CONFIG_MCU_UDC_UAC2_PLAYBACK_TERMINAL_TYPE
    .wTerminalType      =  cpu_to_le16(CONFIG_MCU_UDC_UAC2_PLAYBACK_TERMINAL_TYPE),
#else
    .wTerminalType      =  cpu_to_le16(UAC_OUTPUT_TERMINAL_SPEAKER),
#endif

    .bAssocTerminal     =  0,
    .bSourceID          =  FEATURE_UNIT_ID,
    .bCSourceID         =  USB_OUT_CLK_ID,
    .bmControls         =  (CONTROL_RDWR << COPY_CTRL),
};
#endif

#if 0
DECLARE_UAC2_FEATURE_UNIT_DESCRIPTOR(2);
struct uac2_feature_unit_descriptor_2 feature_unit_desc = {
    .bLength = UAC2_DT_FEATURE_UNIT_SIZE(2),
    .bDescriptorType = USB_DT_CS_INTERFACE,

    .bDescriptorSubtype = UAC_FEATURE_UNIT,
    .bUnitID = FEATURE_UNIT_ID,
    .bSourceID = USB_OUT_IT_ID,
    .bmaControls[0] = cpu_to_le16(MUTE_CONTROL | VOLUME_CONTROL | AUTOMATIC_GAIN_CONTROL | BASS_BOOST_CONTROL),
};
#else

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
struct uac2_feature_unit_descriptor_c {
    __u8   bLength;
    __u8   bDescriptorType;
    __u8   bDescriptorSubtype;
    __u8   bUnitID;
    __u8   bSourceID;
    __le32 bmaControls[CONFIG_MCU_UDC_UAC2_CAPTURE_CHANNEL + 1];
    __u8   iFeature;
} __attribute__ ((packed));

struct uac2_feature_unit_descriptor_c feature_unit_desc_c = {
    .bLength = (6 + (CONFIG_MCU_UDC_UAC2_CAPTURE_CHANNEL + 1) * 4),
    .bDescriptorType = USB_DT_CS_INTERFACE,

    .bDescriptorSubtype = UAC_FEATURE_UNIT,
    .bUnitID = FEATURE_UNIT_ID_C,
    .bSourceID = IO_IN_IT_ID,
    .bmaControls[0] = cpu_to_le16(MUTE_CONTROL | AUTOMATIC_GAIN_CONTROL),
};
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
struct uac2_feature_unit_descriptor_p {
    __u8   bLength;
    __u8   bDescriptorType;
    __u8   bDescriptorSubtype;
    __u8   bUnitID;
    __u8   bSourceID;
    __le32 bmaControls[CONFIG_MCU_UDC_UAC2_PLAYBACK_CHANNEL + 1];
    __u8   iFeature;
} __attribute__ ((packed));

struct uac2_feature_unit_descriptor_p feature_unit_desc_p = {
    .bLength = (6 + (CONFIG_MCU_UDC_UAC2_PLAYBACK_CHANNEL + 1) * 4),
    .bDescriptorType = USB_DT_CS_INTERFACE,

    .bDescriptorSubtype = UAC_FEATURE_UNIT,
    .bUnitID = FEATURE_UNIT_ID,
    .bSourceID = USB_OUT_IT_ID,
    .bmaControls[0] = cpu_to_le16(MUTE_CONTROL | VOLUME_CONTROL | AUTOMATIC_GAIN_CONTROL | BASS_BOOST_CONTROL),
};
#endif

#endif

static struct uac2_ac_header_descriptor ac_hdr_desc = {
    .bLength            =  sizeof ac_hdr_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype = UAC_MS_HEADER,
    .bcdADC = cpu_to_le16(0x200),
    .bCategory = UAC2_FUNCTION_IO_BOX,
    .wTotalLength = sizeof ac_hdr_desc +
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
        sizeof in_clk_src_desc  +
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
        sizeof out_clk_src_desc +
        sizeof usb_out_it_desc  +
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
        sizeof io_in_it_desc    +
        sizeof usb_in_ot_desc   +
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
        sizeof io_out_ot_desc    +
        sizeof feature_unit_desc_p +
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
        sizeof feature_unit_desc_c +
#endif
        0,
    .bmControls = 0,
};

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
/* Audio Streaming OUT Interface - Alt0 */
static struct usb_interface_descriptor std_as_out_if0_desc = {
    .bLength            =  sizeof std_as_out_if0_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bAlternateSetting  =  0,
    .bNumEndpoints      =  0,
    .bInterfaceClass    =  USB_CLASS_AUDIO,
    .bInterfaceSubClass =  USB_SUBCLASS_AUDIOSTREAMING,
    .bInterfaceProtocol =  UAC_VERSION_2,
};

/* Audio Streaming OUT Interface - Alt1 */
static struct usb_interface_descriptor std_as_out_if1_desc = {
    .bLength            =  sizeof std_as_out_if1_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bAlternateSetting  =  1,
    .bNumEndpoints      =  1,
    .bInterfaceClass    =  USB_CLASS_AUDIO,
    .bInterfaceSubClass =  USB_SUBCLASS_AUDIOSTREAMING,
    .bInterfaceProtocol =  UAC_VERSION_2,
};

/* Audio Stream OUT Intface Desc */
static struct uac2_as_header_descriptor as_out_hdr_desc = {
    .bLength            =  sizeof as_out_hdr_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC_AS_GENERAL,
    .bTerminalLink      =  USB_OUT_IT_ID,
    .bmControls         =  0,
    .bFormatType        =  UAC_FORMAT_TYPE_I,
    .bmFormats          =  cpu_to_le32(UAC_FORMAT_TYPE_I_PCM),
    .iChannelNames      =  0,
};

/* Audio USB_OUT Format */
static struct uac2_format_type_i_descriptor as_out_fmt1_desc = {
    .bLength            =  sizeof as_out_fmt1_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,
    .bDescriptorSubtype =  UAC_FORMAT_TYPE,
    .bFormatType        =  UAC_FORMAT_TYPE_I,
};

/* STD AS ISO OUT Endpoint */
static struct usb_endpoint_descriptor fs_epout_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_OUT,
    .bmAttributes     =  USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ADAPTIVE | USB_ENDPOINT_USAGE_DATA,
    .wMaxPacketSize   =  cpu_to_le16(1023),
    .bInterval        =  1,
};

static struct usb_endpoint_descriptor hs_epout_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,

    .bmAttributes    =  USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ADAPTIVE | USB_ENDPOINT_USAGE_DATA,
    .wMaxPacketSize  =  cpu_to_le16(1024),
    .bInterval       =  4,
};

/* CS AS ISO OUT Endpoint */
static struct uac2_iso_endpoint_descriptor as_iso_out_desc = {
    .bLength            =  sizeof as_iso_out_desc,
    .bDescriptorType    =  USB_DT_CS_ENDPOINT,

    .bDescriptorSubtype =  UAC_EP_GENERAL,
    .bmAttributes       =  0,
    .bmControls         =  0,
    .bLockDelayUnits    =  0,
    .wLockDelay         =  0,
};

#ifdef UAC_SYNC_MODE_ASYNC
/* STD AS ISO OUT Endpoint */
static struct usb_endpoint_descriptor fs_epin_fb_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE+2,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_IN,
    .bmAttributes     =  USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_NONE | USB_ENDPOINT_USAGE_FEEDBACK,
    .wMaxPacketSize   =  cpu_to_le16(3),
    .bInterval        =  1,
    .bRefresh         =  0x1,
    .bSynchAddress    =  0,
};

static struct usb_endpoint_descriptor hs_epin_fb_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE+2,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_IN,
    .bmAttributes     =  USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_NONE | USB_ENDPOINT_USAGE_FEEDBACK,
    .wMaxPacketSize   =  cpu_to_le16(4),
    .bInterval        =  4,
    .bRefresh         =  0x2,
    .bSynchAddress    =  0,
};
#endif
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
/* Audio Streaming IN Interface - Alt0 */
static struct usb_interface_descriptor std_as_in_if0_desc = {
    .bLength            =  sizeof std_as_in_if0_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bAlternateSetting  =  0,
    .bNumEndpoints      =  0,
    .bInterfaceClass    =  USB_CLASS_AUDIO,
    .bInterfaceSubClass =  USB_SUBCLASS_AUDIOSTREAMING,
    .bInterfaceProtocol =  UAC_VERSION_2,
};

/* Audio Streaming IN Interface - Alt1 */
static struct usb_interface_descriptor std_as_in_if1_desc = {
    .bLength            =  sizeof std_as_in_if1_desc,
    .bDescriptorType    =  USB_DT_INTERFACE,

    .bAlternateSetting  =  1,
    .bNumEndpoints      =  1,
    .bInterfaceClass    =  USB_CLASS_AUDIO,
    .bInterfaceSubClass =  USB_SUBCLASS_AUDIOSTREAMING,
    .bInterfaceProtocol =  UAC_VERSION_2,
};

/* Audio Stream IN Intface Desc */
static struct uac2_as_header_descriptor as_in_hdr_desc = {
    .bLength            =  sizeof as_in_hdr_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,

    .bDescriptorSubtype =  UAC_AS_GENERAL,
    .bTerminalLink      =  USB_IN_OT_ID,
    .bmControls         =  0,
    .bFormatType        =  UAC_FORMAT_TYPE_I,
    .bmFormats          =  cpu_to_le32(UAC_FORMAT_TYPE_I_PCM),
    .iChannelNames      =  0,
};

/* Audio USB_IN Format */
static struct uac2_format_type_i_descriptor as_in_fmt1_desc = {
    .bLength            =  sizeof as_in_fmt1_desc,
    .bDescriptorType    =  USB_DT_CS_INTERFACE,
    .bDescriptorSubtype =  UAC_FORMAT_TYPE,
    .bFormatType        =  UAC_FORMAT_TYPE_I,
};

/* STD AS ISO IN Endpoint */
static struct usb_endpoint_descriptor fs_epin_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_IN,
    .bmAttributes     =  USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_SYNC,//USB_ENDPOINT_SYNC_ADAPTIVE,
    .wMaxPacketSize   =  cpu_to_le16(1023),
    .bInterval        =  1,
};

static struct usb_endpoint_descriptor hs_epin_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType =  USB_DT_ENDPOINT,

    .bmAttributes    =  USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_SYNC,
    .wMaxPacketSize  =  cpu_to_le16(1024),
    .bInterval       =  4,
};

/* CS AS ISO IN Endpoint */
static struct uac2_iso_endpoint_descriptor as_iso_in_desc = {
    .bLength            =  sizeof as_iso_in_desc,
    .bDescriptorType    =  USB_DT_CS_ENDPOINT,

    .bDescriptorSubtype =  UAC_EP_GENERAL,
    .bmAttributes       =  0x80,  // maxpacketsonly
    .bmControls         =  0,
    .bLockDelayUnits    =  0,
    .wLockDelay         =  0,
};

#ifdef UAC_SYNC_MODE_ASYNC
static struct usb_endpoint_descriptor fs_epout_fb_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE+2,
    .bDescriptorType  =  USB_DT_ENDPOINT,

    .bEndpointAddress =  USB_DIR_OUT,
    .bmAttributes     =  USB_ENDPOINT_XFER_ISOC|USB_ENDPOINT_SYNC_NONE|USB_ENDPOINT_USAGE_FEEDBACK,
    .wMaxPacketSize   =  cpu_to_le16(3),
    .bInterval        =  1,
    .bRefresh         =  8,
    .bSynchAddress    =  0,
};

static struct usb_endpoint_descriptor hs_epout_fb_desc = {
    .bLength         =  USB_DT_ENDPOINT_SIZE+2,
    .bDescriptorType =  USB_DT_ENDPOINT,

    .bmAttributes    =  USB_ENDPOINT_XFER_ISOC|USB_ENDPOINT_SYNC_NONE|USB_ENDPOINT_USAGE_FEEDBACK,
    .wMaxPacketSize  =  cpu_to_le16(4),
    .bInterval       =  1,
    .bRefresh        =  8,
    .bSynchAddress   =  0,
};
#endif
#endif

static struct usb_descriptor_header *fs_audio_desc[] = {
    (struct usb_descriptor_header *)&iad_desc,
    (struct usb_descriptor_header *)&std_ac_if_desc,

    (struct usb_descriptor_header *)&ac_hdr_desc,
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&in_clk_src_desc,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    (struct usb_descriptor_header *)&out_clk_src_desc,
    (struct usb_descriptor_header *)&usb_out_it_desc,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&io_in_it_desc,
    (struct usb_descriptor_header *)&usb_in_ot_desc,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    (struct usb_descriptor_header *)&io_out_ot_desc,
    (struct usb_descriptor_header *)&feature_unit_desc_p,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&feature_unit_desc_c,
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&std_as_in_if0_desc,
    (struct usb_descriptor_header *)&std_as_in_if1_desc,

    (struct usb_descriptor_header *)&as_in_hdr_desc,
    (struct usb_descriptor_header *)&as_in_fmt1_desc,
    (struct usb_descriptor_header *)&fs_epin_desc,
    (struct usb_descriptor_header *)&as_iso_in_desc,
#ifdef UAC_SYNC_MODE_ASYNC
    (struct usb_descriptor_header *)&fs_epout_fb_desc,
#endif
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    (struct usb_descriptor_header *)&std_as_out_if0_desc,
    (struct usb_descriptor_header *)&std_as_out_if1_desc,

    (struct usb_descriptor_header *)&as_out_hdr_desc,
    (struct usb_descriptor_header *)&as_out_fmt1_desc,
    (struct usb_descriptor_header *)&fs_epout_desc,
    (struct usb_descriptor_header *)&as_iso_out_desc,
#ifdef UAC_SYNC_MODE_ASYNC
    (struct usb_descriptor_header *)&fs_epin_fb_desc,
#endif
#endif

    NULL,
};

static struct usb_descriptor_header *hs_audio_desc[] = {
    (struct usb_descriptor_header *)&iad_desc,
    (struct usb_descriptor_header *)&std_ac_if_desc,

    (struct usb_descriptor_header *)&ac_hdr_desc,
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&in_clk_src_desc,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    (struct usb_descriptor_header *)&out_clk_src_desc,
    (struct usb_descriptor_header *)&usb_out_it_desc,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&io_in_it_desc,
    (struct usb_descriptor_header *)&usb_in_ot_desc,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    (struct usb_descriptor_header *)&io_out_ot_desc,
    (struct usb_descriptor_header *)&feature_unit_desc_p,
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&feature_unit_desc_c,
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    (struct usb_descriptor_header *)&std_as_in_if0_desc,
    (struct usb_descriptor_header *)&std_as_in_if1_desc,

    (struct usb_descriptor_header *)&as_in_hdr_desc,
    (struct usb_descriptor_header *)&as_in_fmt1_desc,
    (struct usb_descriptor_header *)&hs_epin_desc,

    (struct usb_descriptor_header *)&as_iso_in_desc,
#ifdef UAC_SYNC_MODE_ASYNC
    (struct usb_descriptor_header *)&hs_epout_fb_desc,
#endif
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    (struct usb_descriptor_header *)&std_as_out_if0_desc,
    (struct usb_descriptor_header *)&std_as_out_if1_desc,

    (struct usb_descriptor_header *)&as_out_hdr_desc,
    (struct usb_descriptor_header *)&as_out_fmt1_desc,
    (struct usb_descriptor_header *)&hs_epout_desc,
    (struct usb_descriptor_header *)&as_iso_out_desc,
#ifdef UAC_SYNC_MODE_ASYNC
    (struct usb_descriptor_header *)&hs_epin_fb_desc,
#endif
#endif

    NULL,
};

static inline void free_ep(struct uac2_rtd_params *prm, struct usb_ep *ep)
{
    int i;

    if (!prm || !ep)
        return ;

    if (!prm->ep_enabled)
        return;

    prm->ep_enabled = false;

    for (i = 0; i < USB_XFERS; i++) {
        if (prm->ureq[i].req) {
            usb_ep_dequeue(ep, prm->ureq[i].req);
            usb_ep_free_request(ep, prm->ureq[i].req);
            prm->ureq[i].req = NULL;
        }
    }

    if (usb_ep_disable(ep))
        ERR ("%s:%d Error!\n", __func__, __LINE__);

    return ;
}

#if defined (CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK) || defined (CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE)
static void set_ep_max_packet_size(const struct f_uac2_opts *uac2_opts,
        struct usb_endpoint_descriptor *ep_desc,
        unsigned int factor, bool is_playback)
{
    int chmask, srate, ssize;
    u16 max_packet_size;

    if (is_playback) {
        chmask = uac2_opts->p_chmask;
        srate = uac2_opts->p_srate;
        ssize = uac2_opts->p_ssize;
    } else {
        chmask = uac2_opts->c_chmask;
        srate = uac2_opts->c_srate;
        ssize = uac2_opts->c_ssize;
    }

    max_packet_size = num_channels(chmask) * ssize *
        DIV_ROUND_UP(srate, factor / (1 << (ep_desc->bInterval - 1)));
    ep_desc->wMaxPacketSize = cpu_to_le16(min_t(u16, max_packet_size,
                le16_to_cpu(ep_desc->wMaxPacketSize)));

    DBG("srate %d factor %d bInterval %d, maxpktsz : %d\n",srate,factor,ep_desc->bInterval, ep_desc->wMaxPacketSize);

    // uac 上行 maxpktsize 大小翻倍，用于重传
    if (is_playback)
        ep_desc->wMaxPacketSize *= 2;

#ifdef UAC_SYNC_MODE_ASYNC
    if (ep_desc->bmAttributes & (USB_ENDPOINT_XFER_ISOC| USB_ENDPOINT_SYNC_ASYNC) && max_packet_size == CUR_HS_EPOUT_NORMAL_MAXPACKET_SIZE)
        ep_desc->wMaxPacketSize = CUR_HS_EPOUT_MAXPACKET_SIZE;
#endif

    return ;
}
#endif

static int afunc_bind(struct usb_configuration *cfg, struct usb_function *fn)
{
    struct audio_dev *agdev = func_to_agdev(fn);
    struct snd_uac2_chip *uac2 = &agdev->uac2;
    struct usb_composite_dev *cdev = cfg->cdev;
#if defined (CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK) || defined (CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE)
    struct usb_gadget *gadget = cdev->gadget;
    struct uac2_rtd_params *prm;
    struct f_uac2_opts *uac2_opts = container_of(fn->fi, struct f_uac2_opts, func_inst);
#endif

    struct usb_string *us;
    int ret;

#if 0
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    snprintf(clksrc_in, sizeof(clksrc_in), "%uHz", uac2_opts->p_srate);
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    snprintf(clksrc_out, sizeof(clksrc_out), "%uHz", uac2_opts->c_srate);
#endif
#endif

    us = usb_gstrings_attach(cdev, fn_strings, ARRAY_SIZE(strings_fn));
    if (!us)
        return -ENOMEM;

    iad_desc.iFunction = us[STR_ASSOC].id;
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    iad_desc.bInterfaceCount++;
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    iad_desc.bInterfaceCount++;
#endif

    std_ac_if_desc.iInterface = us[STR_IF_CTRL].id;

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    in_clk_src_desc.iClockSource = us[STR_CLKSRC_IN].id;
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    out_clk_src_desc.iClockSource = us[STR_CLKSRC_OUT].id;
    usb_out_it_desc.iTerminal = us[STR_USB_IT].id;
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    io_in_it_desc.iTerminal = us[STR_IO_IT].id;
    usb_in_ot_desc.iTerminal = us[STR_USB_OT].id;
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    io_out_ot_desc.iTerminal = us[STR_IO_OT].id;
    //feature_unit_desc_p.iFeature = us[STR_FEATURE_UNIT].id;
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    //feature_unit_desc_c.iFeature = us[STR_FEATURE_UNIT_C].id;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    std_as_out_if0_desc.iInterface = us[STR_AS_OUT_ALT0].id;
    std_as_out_if1_desc.iInterface = us[STR_AS_OUT_ALT1].id;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    std_as_in_if0_desc.iInterface = us[STR_AS_IN_ALT0].id;
    std_as_in_if1_desc.iInterface = us[STR_AS_IN_ALT1].id;
#endif

    /* Initialize the configurable parameters */
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    usb_out_it_desc.bNrChannels = num_channels(uac2_opts->c_chmask);
    usb_out_it_desc.bmChannelConfig = cpu_to_le32(uac2_opts->c_chmask);
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    io_in_it_desc.bNrChannels = num_channels(uac2_opts->p_chmask);
    io_in_it_desc.bmChannelConfig = cpu_to_le32(uac2_opts->p_chmask);
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    as_out_hdr_desc.bNrChannels = num_channels(uac2_opts->c_chmask);
    as_out_hdr_desc.bmChannelConfig = cpu_to_le32(uac2_opts->c_chmask);
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    as_in_hdr_desc.bNrChannels = num_channels(uac2_opts->p_chmask);
    as_in_hdr_desc.bmChannelConfig = cpu_to_le32(uac2_opts->p_chmask);
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    as_out_fmt1_desc.bSubslotSize = uac2_opts->c_ssize;
    as_out_fmt1_desc.bBitResolution = uac2_opts->c_ssize * 8;
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    as_in_fmt1_desc.bSubslotSize = uac2_opts->p_ssize;
    as_in_fmt1_desc.bBitResolution = uac2_opts->p_ssize * 8;
#endif

    ret = usb_interface_id(cfg, fn);
    if (ret < 0) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        return ret;
    }
    std_ac_if_desc.bInterfaceNumber = ret;
    agdev->ac_intf = ret;
    agdev->ac_alt = 0;

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    ret = usb_interface_id(cfg, fn);
    if (ret < 0) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        return ret;
    }
    std_as_in_if0_desc.bInterfaceNumber = ret;
    std_as_in_if1_desc.bInterfaceNumber = ret;
    agdev->as_in_intf = ret;
    agdev->as_in_alt = 0;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    ret = usb_interface_id(cfg, fn);
    if (ret < 0) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        return ret;
    }
    std_as_out_if0_desc.bInterfaceNumber = ret;
    std_as_out_if1_desc.bInterfaceNumber = ret;
    agdev->as_out_intf = ret;
    agdev->as_out_alt = 0;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    agdev->in_ep = usb_ep_autoconfig(gadget, &fs_epin_desc);
    if (!agdev->in_ep) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        goto err;
    }

#ifdef UAC_SYNC_MODE_ASYNC
    agdev->out_fb_ep = usb_ep_autoconfig(gadget, &fs_epout_fb_desc);
    if (!agdev->out_fb_ep) {
        ERR("%s:%d Error!\n", __func__, __LINE__);
        goto err;
    }
#endif
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    agdev->out_ep = usb_ep_autoconfig(gadget, &fs_epout_desc);
    if (!agdev->out_ep) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        goto err;
    }

#ifdef UAC_SYNC_MODE_ASYNC
    agdev->in_fb_ep = usb_ep_autoconfig(gadget, &fs_epin_fb_desc);
    if (!agdev->in_fb_ep) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        goto err;
    }
#endif
#endif

    uac2->p_prm.uac2 = uac2;
    uac2->c_prm.uac2 = uac2;

    /* Calculate wMaxPacketSize according to audio bandwidth */
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    set_ep_max_packet_size(uac2_opts, &fs_epin_desc, 1000, true);
    set_ep_max_packet_size(uac2_opts, &hs_epin_desc, 8000, true);
#endif
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    set_ep_max_packet_size(uac2_opts, &fs_epout_desc, 1000, false);
    set_ep_max_packet_size(uac2_opts, &hs_epout_desc, 8000, false);
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    hs_epout_desc.bEndpointAddress = fs_epout_desc.bEndpointAddress;
#ifdef UAC_SYNC_MODE_ASYNC
    hs_epin_fb_desc.bEndpointAddress = fs_epin_fb_desc.bEndpointAddress;
#endif
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    hs_epin_desc.bEndpointAddress = fs_epin_desc.bEndpointAddress;
#ifdef UAC_SYNC_MODE_ASYNC
    hs_epout_fb_desc.bEndpointAddress = fs_epout_fb_desc.bEndpointAddress;
#endif
#endif

    ret = usb_assign_descriptors(fn, fs_audio_desc, hs_audio_desc, NULL);
    if (ret)
        goto err;

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    prm = &agdev->uac2.c_prm;
    prm->max_psize = hs_epout_desc.wMaxPacketSize;
    memset(g_hs_epout_rbuf,0,USB_XFERS*CUR_HS_EPOUT_MAXPACKET_SIZE);
    prm->rbuf = g_hs_epout_rbuf;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    prm = &agdev->uac2.p_prm;
    prm->max_psize = hs_epin_desc.wMaxPacketSize;
    memset(g_hs_epin_rbuf,0,USB_XFERS*DMA_TIMES_EACH_TRANSFER*CUR_HS_EPIN_MAXPACKET_SIZE*MAX_CHANNEL_NUM);
    prm->rbuf = g_hs_epin_rbuf;
#endif

    return 0;

err:
    usb_free_all_descriptors(fn);
    return -EINVAL;
}

static void afunc_free_inst(struct usb_function_instance *f)
{
    struct f_uac2_opts *opts;

    opts = container_of(f, struct f_uac2_opts, func_inst);
    memset(opts, 0, sizeof(*opts));

    return ;
}

static void afunc_free(struct usb_function *f)
{
    struct audio_dev *agdev;
    struct f_uac2_opts *opts;

    agdev = func_to_agdev(f);
    opts = container_of(f->fi, struct f_uac2_opts, func_inst);
    memset(agdev, 0, sizeof(*agdev));
    --opts->refcnt;

    return ;
}

static void afunc_unbind(struct usb_configuration *c, struct usb_function *f)
{
#if defined(CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE) || defined(CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK)
    struct audio_dev *agdev = func_to_agdev(f);
    struct uac2_rtd_params *prm;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    prm = &agdev->uac2.p_prm;
    memset(prm->rbuf,0,USB_XFERS*DMA_TIMES_EACH_TRANSFER*CUR_HS_EPIN_MAXPACKET_SIZE*MAX_CHANNEL_NUM);
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    prm = &agdev->uac2.c_prm;
    memset(prm->rbuf,0,USB_XFERS*CUR_HS_EPOUT_MAXPACKET_SIZE);
#endif
    usb_free_all_descriptors(f);

    return ;
}

static int afunc_set_alt(struct usb_function *fn, unsigned intf, unsigned alt)
{
    struct audio_dev *agdev = func_to_agdev(fn);
#if defined(CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK) || defined(CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE)
    struct usb_composite_dev *cdev = fn->config->cdev;
    struct snd_uac2_chip *uac2 = &agdev->uac2;
    struct usb_gadget *gadget = cdev->gadget;
#endif
    struct usb_request *req;
    struct usb_ep *ep = NULL;
#ifdef UAC_SYNC_MODE_ASYNC
    struct usb_request *req_fb;
    struct usb_ep *ep_fb;
#endif
    struct uac2_rtd_params *prm = NULL;
    int req_len = 0, i=0;

    /* No i/f has more than 2 alt settings */
    if (alt > 1) {
        ERR("%s:%d Error!\n", __func__, __LINE__);
        return -EINVAL;
    }

    DBG ("%s, intf : %d, alt : %d\n", __func__, intf, alt);

    if (intf == agdev->ac_intf) {
        /* Control I/f has only 1 AltSetting - 0 */
        if (alt) {
            ERR("%s:%d Error!\n", __func__, __LINE__);
            return -EINVAL;
        }
        return 0;
    }

    // 状态通知
    if (intf == agdev->as_in_intf) {
        if (1 == alt)
            our_callback_ops.state |= (1<<UAC2_RECORD_STATE_BIT);
        else if (0 == alt)
            our_callback_ops.state &= ~(1<<UAC2_RECORD_STATE_BIT);
    } else if (intf == agdev->as_out_intf) {
        if (1 == alt)
            our_callback_ops.state |= (1<<UAC2_PLAYBACK_STATE_BIT);
        else if (0 == alt)
            our_callback_ops.state &= ~(1<<UAC2_PLAYBACK_STATE_BIT);
    }

    if (intf == agdev->as_out_intf) {
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
        ep = agdev->out_ep;
        prm = &uac2->c_prm;
        config_ep_by_speed(gadget, fn, ep);
        agdev->as_out_alt = alt;
        req_len = prm->max_psize;

#ifdef UAC_SYNC_MODE_ASYNC
        ep_fb = agdev->in_fb_ep;
        config_ep_by_speed(gadget, fn, ep_fb);
#endif
#endif
    } else if (intf == agdev->as_in_intf) {
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
        struct f_uac2_opts *opts = agdev_to_uac2_opts(agdev);
        unsigned int factor, rate;
        struct usb_endpoint_descriptor *ep_desc;

        ep = agdev->in_ep;
        prm = &uac2->p_prm;
        config_ep_by_speed(gadget, fn, ep);
        agdev->as_in_alt = alt;

        /* pre-calculate the playback endpoint's interval */
        if (gadget->speed == USB_SPEED_FULL) {
            ep_desc = &fs_epin_desc;
            factor = 1000;
        } else {
            ep_desc = &hs_epin_desc;
            factor = 8000;
        }

        /* pre-compute some values for iso_complete() */
        uac2->p_framesize = opts->p_ssize *
            num_channels(opts->p_chmask);
        rate = opts->p_srate * uac2->p_framesize;
        uac2->p_interval = factor / (1 << (ep_desc->bInterval - 1));
        uac2->p_pktsize = min_t(unsigned int, rate / uac2->p_interval,
                prm->max_psize);

        if (uac2->p_pktsize < prm->max_psize)
            uac2->p_pktsize_residue = rate % uac2->p_interval;
        else
            uac2->p_pktsize_residue = 0;

        req_len = uac2->p_pktsize;
        uac2->p_residue = 0;

#ifdef UAC_SYNC_MODE_ASYNC
        ep_fb = agdev->out_fb_ep;
        config_ep_by_speed(gadget, fn, ep_fb);
#endif
#endif
    } else {
        ERR( "Failed to set intface(%d), no such intface!\n", intf);
        return -EINVAL;
    }

    if (alt == 0) {
        if (intf == agdev->as_out_intf) {
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
            uac_core_playback_stop_callback();

            if (our_callback_ops.callback_ops.notify_callback)
                our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM, USB_FUNC_STATUS_DISABLE);
#endif
        } else if (intf == agdev->as_in_intf) {
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
            uac_core_capture_stop_callback();
            AudioBuffConsumeStop(our_callback_ops.callback_ops.ain_buff_handle);

            if (our_callback_ops.callback_ops.notify_callback)
                our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM, USB_FUNC_STATUS_DISABLE);
#endif
        }

        free_ep(prm, ep);
        return 0;
    }

    if (intf == agdev->as_out_intf) {
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
        uac_core_playback_start_callback();
        if (our_callback_ops.callback_ops.notify_callback)
            our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM, USB_FUNC_STATUS_ENABLE);
#endif
    } else if (intf == agdev->as_in_intf) {
#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
        uac_core_capture_start_callback();
        uac_up_left_pkt_num = 0;
        last_xfer_size  = 0;
        first_xfer      = 1;

        AudioBuffConsumeStart(our_callback_ops.callback_ops.ain_buff_handle);
        if (our_callback_ops.callback_ops.notify_callback)
            our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM, USB_FUNC_STATUS_ENABLE);
#endif
    }

    if (prm)
        prm->ep_enabled = true;
    usb_ep_enable(ep);

#ifdef UAC_SYNC_MODE_ASYNC
    usb_ep_enable(ep_fb);
    if (intf == agdev->as_in_intf && prm == &uac2->p_prm) {
        req_fb = usb_ep_alloc_request(ep_fb);
        if (req_fb == NULL) {
            ERR("Failed to alloc usb playback fb request\n");
            return -ENOMEM;
        }
        req_fb->zero = 0;
        req_fb->context = &prm->ureq[i];
        req_fb->length = 4;
        req_fb->complete = agdev_iso_fb_out_complete;
        req_fb->orig_buf = NULL;
        req_fb->buf = &g_hs_epout_fb_rbuf;

        agdev->ain_buf_len = AudioBuffLen(our_callback_ops.callback_ops.ain_buff_handle);
        agdev->ain_high_thrd_sz = (agdev->ain_buf_len>>1) + (agdev->ain_buf_len>>AIN_THRED_SCALE_FACTOR);
        agdev->ain_low_thrd_sz = (agdev->ain_buf_len>>1) - (agdev->ain_buf_len>>AIN_THRED_SCALE_FACTOR);

        if (usb_ep_queue(ep_fb, req_fb))
            ERR("%s:%d Error!\n", __func__, __LINE__);
    }
    else if (intf == agdev->as_out_intf && prm == &uac2->c_prm)//audio out
    {
        req_fb = usb_ep_alloc_request(ep_fb);
        if (req_fb == NULL) {
            ERR("Failed to alloc usb record fb request\n");
            return -ENOMEM;
        }
        req_fb->zero = 0;
        req_fb->context = &prm->ureq[i];
        req_fb->length = 4;
        req_fb->complete = agdev_iso_fb_in_complete;
        req_fb->orig_buf = NULL;
        req_fb->buf = &g_hs_epin_fb_rbuf;
        *(unsigned int *)(req_fb->buf) = NORMAL_FREQ;
        agdev->req_fb_for_in_fb_ep = req_fb;

        our_callback_ops.sof_timer_callback = agdev_sof_timer_callback;
        our_callback_ops.sof_timer_interval = INTERVAL_MS4;
        our_callback_ops.sof_incr = 0;
        agdev->req_fb_in_done = true;
        agdev->aout_buf_len = AudioBuffLen(our_callback_ops.callback_ops.aout_buff_handle);
        agdev->aout_high_thrd_sz = (agdev->aout_buf_len>>1) + (MAX_FRAME_SIZE>>AOUT_THRED_SCALE_FACTOR);
        agdev->aout_low_thrd_sz = (agdev->aout_buf_len>>1) - MAX_FRAME_SIZE/AOUT_THRED_PERCENTAGE;
    }
#endif

    for (i = 0; i < USB_XFERS; i++) {
        if (!prm->ureq[i].req) {
            req = usb_ep_alloc_request(ep);
            if (req == NULL) {
                ERR("Failed to alloc usb request\n");
                return -ENOMEM;
            }

            prm->ureq[i].req = req;
            prm->ureq[i].pp = prm;
            req->zero = 0;
            req->context = &prm->ureq[i];
            req->length = req_len;
            req->complete = agdev_iso_complete;
            req->buf = req->orig_buf = (void *)((int)prm->rbuf + i * prm->max_psize);
        }

        if (usb_ep_queue(ep, prm->ureq[i].req))
            ERR("Failed to enqueue a usb request\n\n");
    }

    return 0;
}

static int afunc_get_alt(struct usb_function *fn, unsigned intf)
{
    struct audio_dev *agdev = func_to_agdev(fn);

    if (intf == agdev->ac_intf)
        return agdev->ac_alt;
    else if (intf == agdev->as_out_intf)
        return agdev->as_out_alt;
    else if (intf == agdev->as_in_intf)
        return agdev->as_in_alt;
    else
        ERR ("%s:%d Invalid Interface %d!\n", __func__, __LINE__, intf);

    return -EINVAL;
}

static void afunc_disable(struct usb_function *fn)
{
    struct audio_dev *agdev = func_to_agdev(fn);
    struct snd_uac2_chip *uac2 = &agdev->uac2;

    free_ep(&uac2->p_prm, agdev->in_ep);
    agdev->as_in_alt = 0;

    free_ep(&uac2->c_prm, agdev->out_ep);
    agdev->as_out_alt = 0;

    return ;
}

static int in_rq_cur(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    struct usb_request *req = fn->config->cdev->req;
    struct audio_dev *agdev = func_to_agdev(fn);
    struct f_uac2_opts *opts;
    u16 w_length = le16_to_cpu(cr->wLength);
    u16 w_index = le16_to_cpu(cr->wIndex);
    u16 w_value = le16_to_cpu(cr->wValue);
    u8 channel_num = w_index & 0xff;
    u8 entity_id = (w_index >> 8) & 0xff;
    u8 control_selector = w_value >> 8;
    int value = -EOPNOTSUPP;
    int p_srate, c_srate;

    opts = agdev_to_uac2_opts(agdev);
    p_srate = opts->p_srate;
    c_srate = opts->c_srate;

    DBG ("%s entityid : %d cs : %d\n", __func__, entity_id, control_selector);

    if (control_selector == UAC2_CS_CONTROL_SAM_FREQ) {
        struct cntrl_cur_lay3 c;
        memset(&c, 0, sizeof(struct cntrl_cur_lay3));

        if (entity_id == USB_IN_CLK_ID)
            c.dCUR = p_srate;
        else if (entity_id == USB_OUT_CLK_ID)
            c.dCUR = c_srate;

        value = min_t(unsigned, w_length, sizeof c);
        memcpy(req->buf, &c, value);
        DBG("host get sample freq c.dCUR(%d) hehe\n",c.dCUR);
    } else if (control_selector == UAC2_CS_CONTROL_CLOCK_VALID) {
        *(u8 *)req->buf = 1;
        value = min_t(unsigned, w_length, 1);
    } else {
        DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
    }

    switch (entity_id) {
    case USB_OUT_CLK_ID:
    case USB_IN_CLK_ID:
        if (control_selector == UAC2_CS_CONTROL_SAM_FREQ) {
            struct cntrl_cur_lay3 c;
            memset(&c, 0, sizeof(struct cntrl_cur_lay3));

            if (entity_id == USB_IN_CLK_ID)
                c.dCUR = p_srate;
            else if (entity_id == USB_OUT_CLK_ID)
                c.dCUR = c_srate;

            value = min_t(unsigned, w_length, sizeof c);
            memcpy(req->buf, &c, value);
        } else if (control_selector == UAC2_CS_CONTROL_CLOCK_VALID) {
            *(u8 *)req->buf = 1;
            value = min_t(unsigned, w_length, 1);
        } else {
            DBG(
                    "%s:%d control_selector=%d TODO!\n",
                    __func__, __LINE__, control_selector);
        }
        break;
    case FEATURE_UNIT_ID:
        if (control_selector == UAC_FU_MUTE) {
            struct cntrl_cur_lay1 c;

            if (channel_num == 0) {
                c.bCUR = cur_mute;
            } else {
                c.bCUR = cur_mute;
            }

            //printk ("pc want know a sigle chanle's current mute control, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_VOLUME) {
            struct cntrl_cur_lay2 c;

            if (channel_num == 0) {
                c.wCUR = cur_volume;
            } else {
                c.wCUR = cur_volume;
            }

            //printk ("pc want know a sigle chanle's current volume control, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_AUTOMATIC_GAIN) {
            struct cntrl_cur_lay1 c;

            if (channel_num == 0) {
                c.bCUR = cur_ag;
            } else {
                c.bCUR = cur_ag;
            }

            //printk ("pc want know a sigle chanle's current automatic gain, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_BASS_BOOST) {
            struct cntrl_cur_lay1 c;

            if (channel_num == 0) {
                c.bCUR = cur_bass_boost;
            } else {
                c.bCUR = cur_bass_boost;
            }

            //printk ("pc want know a sigle chanle's current bass boost, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }
        break;
    case FEATURE_UNIT_ID_C:
        if (control_selector == UAC_FU_MUTE) {
            struct cntrl_cur_lay1 c;

            if (channel_num == 0)
                c.bCUR = cur_mute_c;
            else
                c.bCUR = cur_mute_c;

            //printk ("pc want know a sigle chanle's current mute control, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_VOLUME) {
            struct cntrl_cur_lay2 c;

            if (channel_num == 0) {
                c.wCUR = cur_volume_c;
            } else {
                c.wCUR = cur_volume_c;
            }

            //printk ("pc want know a sigle chanle's current volume control, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_AUTOMATIC_GAIN) {
            struct cntrl_cur_lay1 c;

            if (channel_num == 0) {
                c.bCUR = cur_ag_c;
            } else {
                c.bCUR = cur_ag_c;
            }

            //printk ("pc want know a sigle chanle's current automatic gain, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_BASS_BOOST) {
            struct cntrl_cur_lay1 c;

            if (channel_num == 0) {
                c.bCUR = cur_bass_boost_c;
            } else {
                c.bCUR = cur_bass_boost_c;
            }

            //printk ("pc want know a sigle chanle's current bass boost, CN : %d\n", channel_num);
            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }
        break;
    default :
        DBG("%s:%d entity id = %d TODO!\n", __func__, __LINE__, entity_id);
        break;
    }

    return value;
}

static int in_rq_range(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    struct usb_request *req = fn->config->cdev->req;
    struct audio_dev *agdev = func_to_agdev(fn);
    struct f_uac2_opts *opts;
    u16 w_length = le16_to_cpu(cr->wLength);
    u16 w_index = le16_to_cpu(cr->wIndex);
    u16 w_value = le16_to_cpu(cr->wValue);
    u8 entity_id = (w_index >> 8) & 0xff;
    u8 channel_num = w_index & 0xff;
    u8 control_selector = w_value >> 8;
    struct cntrl_range_lay3 r;
    int value = -EOPNOTSUPP;
    int p_srate, c_srate;

    opts = agdev_to_uac2_opts(agdev);
    p_srate = opts->p_srate;
    c_srate = opts->c_srate;

    DBG ("%s entityid : %d cs : %d\n", __func__, entity_id, control_selector);

    switch (entity_id) {
    case USB_OUT_CLK_ID:
    case USB_IN_CLK_ID:
        if (control_selector == UAC2_CS_CONTROL_SAM_FREQ) {
            if (entity_id == USB_IN_CLK_ID)
                r.dMIN = p_srate;
            else if (entity_id == USB_OUT_CLK_ID)
                r.dMIN = c_srate;
            else
                return -EOPNOTSUPP;

            r.dMAX = r.dMIN;
            r.dRES = 0;
            r.wNumSubRanges = 1;

            value = min_t(unsigned, w_length, sizeof r);
            memcpy(req->buf, &r, value);
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }
        break;
        //static int vol_level[20] = {0,2,3,4,6,8,11,16,23,32,45,64,91,128,181,256,362,512,724,1023};
    case FEATURE_UNIT_ID:
        if (control_selector == UAC_FU_MUTE)
            return -EOPNOTSUPP;
        else if (control_selector == UAC_FU_VOLUME) {
            struct cntrl_range_lay2 c;

            if (channel_num == 0) {
                c.wNumSubRanges = 1;
                c.wMIN = PLAYBACK_VOLUME_MIN;
                c.wMAX = PLAYBACK_VOLUME_MAX;
                c.wRES = PLAYBACK_VOLUME_RES;
            } else {
                c.wNumSubRanges = 1;
                c.wMIN = PLAYBACK_VOLUME_MIN;
                c.wMAX = PLAYBACK_VOLUME_MAX;
                c.wRES = PLAYBACK_VOLUME_RES;
                DBG ("pc want know a sigle chanle's range volume control, CN : %d\n", channel_num);
            }

            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_AUTOMATIC_GAIN) {
            return -EOPNOTSUPP;
        } else if (control_selector == UAC_FU_BASS_BOOST) {
            return -EOPNOTSUPP;
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }
        break;
    case FEATURE_UNIT_ID_C:
        if (control_selector == UAC_FU_MUTE)
            return -EOPNOTSUPP;
        else if (control_selector == UAC_FU_VOLUME) {
            struct cntrl_range_lay2 c;

            if (channel_num == 0) {
                c.wNumSubRanges = 1;
                c.wMIN = CAPTURE_VOLUME_MIN;
                c.wMAX = CAPTURE_VOLUME_MAX;
                c.wRES = CAPTURE_VOLUME_RES;
            } else {
                c.wNumSubRanges = 1;
                c.wMIN = CAPTURE_VOLUME_MIN;
                c.wMAX = CAPTURE_VOLUME_MAX;
                c.wRES = CAPTURE_VOLUME_RES;
                DBG ("pc want know a sigle chanle's range volume control, CN : %d\n", channel_num);
            }

            value = min_t(unsigned, w_length, sizeof c);
            memcpy (req->buf, &c, value);
        } else if (control_selector == UAC_FU_AUTOMATIC_GAIN) {
            return -EOPNOTSUPP;
        } else if (control_selector == UAC_FU_BASS_BOOST) {
            return -EOPNOTSUPP;
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }
        break;
    default :
        break;
    }

    return value;
}

static int ac_rq_in(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    if (cr->bRequest == UAC2_CS_CUR)
        return in_rq_cur(fn, cr);
    else if (cr->bRequest == UAC2_CS_RANGE)
        return in_rq_range(fn, cr);
    else
        return -EOPNOTSUPP;
}

static int out_rq_cur(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    u16 w_length = le16_to_cpu(cr->wLength);
    u16 w_value = le16_to_cpu(cr->wValue);
    u16 w_index = le16_to_cpu(cr->wIndex);
    u8 control_selector = w_value >> 8;
    u8 entity_id = (w_index >> 8) & 0xff;
    int value = -EOPNOTSUPP;

    struct usb_composite_dev *cdev = fn->config->cdev;
    struct usb_request *req = cdev->req;

    DBG ("%s, host want [ Set ] Cur Param [ID : %d], req : %p\n", __func__, entity_id, req);

    req->complete = agout_ep_complete;

    switch (entity_id) {
    case USB_OUT_CLK_ID:
    case USB_IN_CLK_ID:
        if (control_selector == UAC2_CS_CONTROL_SAM_FREQ)
        {
            value = w_length;
            DBG("host support sample freq............\n");
        }
        break;
    case FEATURE_UNIT_ID:
        if (control_selector == UAC_FU_MUTE) {
            req->stream_id= UAC_FU_MUTE;
            value = w_length;
        } else if (control_selector == UAC_FU_VOLUME) {
            req->stream_id = UAC_FU_VOLUME;
            value = w_length;
        } else if (control_selector == UAC_FU_AUTOMATIC_GAIN) {
            req->stream_id = UAC_FU_AUTOMATIC_GAIN;
            value = w_length;
        } else if (control_selector == UAC_FU_BASS_BOOST) {
            req->stream_id = UAC_FU_BASS_BOOST;
            value = w_length;
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }

        req->stream_id |= (FEATURE_UNIT_ID << 8);
        break;
    case FEATURE_UNIT_ID_C:
        if (control_selector == UAC_FU_MUTE) {
            req->stream_id= UAC_FU_MUTE;
            value = w_length;
        } else if (control_selector == UAC_FU_VOLUME) {
            req->stream_id = UAC_FU_VOLUME;
            value = w_length;
        } else if (control_selector == UAC_FU_AUTOMATIC_GAIN) {
            req->stream_id = UAC_FU_AUTOMATIC_GAIN;
            value = w_length;
        } else if (control_selector == UAC_FU_BASS_BOOST) {
            req->stream_id = UAC_FU_BASS_BOOST;
            value = w_length;
        } else {
            DBG("%s:%d control_selector=%d TODO!\n", __func__, __LINE__, control_selector);
        }
        req->stream_id |= (FEATURE_UNIT_ID_C << 8);
        break;
    default :
        break;
    }

    return value;
}

static int out_rq_range(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    //printf ("%s, host want Set Range Param\n", __func__);
    return -EOPNOTSUPP;
}

static int ac_rq_out(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    if (cr->bRequest == UAC2_CS_CUR)
        return out_rq_cur(fn, cr);
    else if (cr->bRequest == UAC2_CS_RANGE)
        return out_rq_range(fn, cr);
    else
        return -EOPNOTSUPP;
}

static int setup_rq_inf(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    struct audio_dev *agdev = func_to_agdev(fn);
    u16 w_index = le16_to_cpu(cr->wIndex);
    u8 intf = w_index & 0xff;

    if (intf != agdev->ac_intf) {
        DBG("%s:%d Error!\n", __func__, __LINE__);
        return -EOPNOTSUPP;
    }

    if (cr->bRequestType & USB_DIR_IN)
        return ac_rq_in(fn, cr);
    else if (cr->bRequest == UAC2_CS_CUR)
        return ac_rq_out(fn, cr);

    return -EOPNOTSUPP;
}

static int afunc_setup(struct usb_function *fn, const struct usb_ctrlrequest *cr)
{
    struct usb_composite_dev *cdev = fn->config->cdev;
    struct usb_request *req = cdev->req;
    u16 w_length = le16_to_cpu(cr->wLength);
    int value = -EOPNOTSUPP;

    DBG ("============  rt : 0x%02x r : 0x%02x v 0x%04x i 0x%04x l %d\n",
        cr->bRequestType, cr->bRequest, cr->wValue, cr->wIndex, cr->wLength);

    /* Only Class specific requests are supposed to reach here */
    if ((cr->bRequestType & USB_TYPE_MASK) != USB_TYPE_CLASS)
        return -EOPNOTSUPP;

    if ((cr->bRequestType & USB_RECIP_MASK) == USB_RECIP_INTERFACE)
        value = setup_rq_inf(fn, cr);
    else
        ERR ("%s:%d Error!\n", __func__, __LINE__);

    if (value >= 0) {
        req->length = value;
        req->zero = value < w_length;
        value = usb_ep_queue(cdev->gadget->ep0, req);
        if (value < 0) {
            ERR ("%s:%d Error!\n", __func__, __LINE__);
            req->status = 0;
        }
    }

    return value;
}

static struct usb_function_instance *afunc_alloc_inst(void)
{
    struct f_uac2_opts *opts;

    opts = &g_opts;
    memset(opts, 0, sizeof(*opts));

    opts->func_inst.free_func_inst = afunc_free_inst;

    opts->p_chmask = UAC2_DEF_PCHMASK;
    opts->p_srate = UAC2_DEF_PSRATE;
    opts->p_ssize = UAC2_DEF_PSSIZE;
    opts->c_chmask = UAC2_DEF_CCHMASK;
    opts->c_srate = UAC2_DEF_CSRATE;
    opts->c_ssize = UAC2_DEF_CSSIZE;
    DBG("afunc_alloc_inst p_srate %d\n",opts->p_srate);
    return &opts->func_inst;
}

static void afunc_suspend(struct usb_function *func)
{
    AudioBuffConsumeStop(our_callback_ops.callback_ops.ain_buff_handle);
    afunc_disable(func);

    uac_core_playback_stop_callback();
    uac_core_capture_stop_callback();

    return ;
}

static void afunc_resume(struct usb_function *func)
{
    func = func;

    return ;
}

static struct usb_function *afunc_alloc(struct usb_function_instance *fi)
{
    struct audio_dev *agdev;
    struct f_uac2_opts *opts;

    agdev = &g_agdev;
    memset(agdev, 0, sizeof(*agdev));

    opts = container_of(fi, struct f_uac2_opts, func_inst);
    ++opts->refcnt;

    agdev->func.name      = "uac2_func";
    agdev->func.bind      = afunc_bind;
    agdev->func.unbind    = afunc_unbind;
    agdev->func.set_alt   = afunc_set_alt;
    agdev->func.get_alt   = afunc_get_alt;
    agdev->func.disable   = afunc_disable;
    agdev->func.setup     = afunc_setup;
    agdev->func.suspend   = afunc_suspend;
    agdev->func.resume    = afunc_resume;
    agdev->func.free_func = afunc_free;

    return &agdev->func;
}

#define UAC_FUNCTION_NAME "uac2"
static struct usb_function_driver uac2_func = {
    .name       = UAC_FUNCTION_NAME,
    .alloc_inst = afunc_alloc_inst,
    .alloc_func = afunc_alloc,

};

static int uac2_mod_init(void)
{
    return usb_function_register(&uac2_func);
}

static int uac2_mod_uninit(void)
{
    usb_function_unregister(&uac2_func);
    return 0;
}

#ifdef CONFIG_VSP_UAC_VERSION_2
int UacGetControlInfo(struct uac_control_info *info)
{
    if (!info)
        return -1;

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_PLAYBACK
    info->volume_ds_cur = cur_volume;
    info->volume_ds_min = PLAYBACK_VOLUME_MIN;
    info->volume_ds_max = PLAYBACK_VOLUME_MAX;
    info->mute_ds       = cur_mute;
#endif

#ifdef CONFIG_MCU_UDC_UAC2_ENABLE_CAPTURE
    info->volume_us_cur = cur_volume_c;
    info->volume_us_min = CAPTURE_VOLUME_MIN;
    info->volume_us_max = CAPTURE_VOLUME_MAX;
    info->mute_us       = cur_mute_c;
#endif

    return 0;
}
#endif

int Uac2DoConfig(struct usb_configuration *c)
{
    int status;

    f_uac2 = usb_get_function(fi_uac2);

    status = usb_add_function(c, f_uac2);
    if (status < 0) {
        usb_put_function(f_uac2);
        return status;
    }

    return 0;
}

unsigned int Uac2GetState(void)
{
    if (our_callback_ops.callback_ops.notify_callback)
        return our_callback_ops.state;
    else
        return 0;
}

int Uac2Bind(struct usb_composite_dev *cdev)
{
    struct f_uac2_opts    *uac2_opts;
    fi_uac2 = usb_get_function_instance(UAC_FUNCTION_NAME);

    uac2_opts = container_of(fi_uac2, struct f_uac2_opts, func_inst);
    uac2_opts->p_chmask = p_chmask;
    uac2_opts->p_srate = p_srate;
    uac2_opts->p_ssize = p_ssize;
    uac2_opts->c_chmask = c_chmask;
    uac2_opts->c_srate = c_srate;
    uac2_opts->c_ssize = c_ssize;

    DBG("%s, version: %s p_srate %d\n", DRIVER_DESC, DRIVER_VERSION,uac2_opts->p_srate);
    return 0;
}

int Uac2Unbind(struct usb_composite_dev *cdev)
{
    if (f_uac2)
        usb_put_function(f_uac2);

    if (fi_uac2)
        usb_put_function_instance(fi_uac2);

    return 0;
}

int Uac2Init(const UAC2_CHANNEL_CONFIG *audio_in_config,
        const UAC2_CHANNEL_CONFIG *audio_out_config,
        const UAC2_DEVICE_DESCRIPTION *device_description,
        const UAC2_CALLBACKS *callbacks,
        void *private_data)
{
    int ret;

    if (!audio_in_config || !audio_out_config) {
        printf ("%s, invalid uac config\n", __func__);
        return -1;
    }

    if (!device_description) {
        printf ("%s, invalid uac device description\n", __func__);
        return -1;
    }

    ret = uac2_mod_init();
    if (ret < 0)
        return ret;

    c_chmask = audio_out_config->channel_mask;
    c_srate = audio_out_config->sample_rate;

    p_chmask = audio_in_config->channel_mask;
    p_srate = audio_in_config->sample_rate;

    our_callback_ops.callback_ops = *callbacks;
    our_callback_ops.private_data = private_data;

    // modify strings_fn
    if (device_description->sound_card_name_in_windows)
        strings_fn[STR_ASSOC].s = device_description->sound_card_name_in_windows;
    else
        strings_fn[STR_ASSOC].s = DEFALUT_ASSOC_STR;

    return 0;
}

int Uac2Done(void)
{
    uac2_mod_uninit();
    return 0;
}

