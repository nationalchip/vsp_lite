/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * udc_eva.c: MCU's USB Device Controller driver
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <types.h>
#include <base_addr.h>
#include <misc_regs.h>
#include <driver/irq.h>
#include <driver/timer.h>
#include <driver/delay.h>
#include <driver/clock.h>
#include <driver/udc.h>

#include "../include/ch9.h"
#include "../include/gadget.h"
#include "../include/usb.h"
#include "../include/composite.h"
#include "../include/gadget.h"
#include "../include/u_uac2.h"
#include "../include/udc_common.h"

#define USB_BUFSIZ                  512 //4096 256//
#define MAX_FUNC_NUM                4
#define CONFIG_USB_GADGET_VBUS_DRAW 2

#define UDC_TAG "[UDC] "
#define ERR(fmt, ...) printf (UDC_TAG "ERR : " fmt, ##__VA_ARGS__)
#define DBG(fmt, ...)

#define ARCH_LEO      // have 3 in endpoints, 5 out endponints
//#define ARCH_LEO_MINI // have 5 in endpoints, 5 out endponints

// data structure

#if defined(CONFIG_MCU_UDC_ENABLE_UAC2) || defined(CONFIG_MCU_UDC_ENABLE_UAC1)
extern struct uac_internal_callbacks our_callback_ops;
#endif

struct usb_udc {
    struct usb_gadget_driver  *driver;
    struct usb_gadget         *gadget;
    struct device              dev;
    struct list_head           list;
    bool                       vbus;
};

struct utf8_table {
    int     cmask;
    int     cval;
    int     shift;
    long    lmask;
    long    lval;
};

#define UNICODE_MAX    0x0010ffff
#define PLANE_SIZE    0x00010000

#define SURROGATE_MASK    0xfffff800
#define SURROGATE_PAIR    0x0000d800
#define SURROGATE_LOW    0x00000400
#define SURROGATE_BITS    0x000003ff
/* Plane-0 Unicode character */
//typedef u16 wchar_t;
//#define MAX_WCHAR_T    0xffff

typedef u32 unicode_t;

/* Byte order for UTF-16 strings */
enum utf16_endian {
    UTF16_HOST_ENDIAN,
    UTF16_LITTLE_ENDIAN,
    UTF16_BIG_ENDIAN
};

static struct usb_descriptor_header** next_ep_desc(struct usb_descriptor_header **t)
{
    for (; *t; t++) {
        if ((*t)->bDescriptorType == USB_DT_ENDPOINT)
            return t;
    }
    return NULL;
}


#define for_each_ep_desc(start, ep_desc)               \
    for (ep_desc = next_ep_desc(start);                \
            ep_desc; ep_desc = next_ep_desc(ep_desc+1))


static struct usb_udc g_udc;
static LIST_HEAD(func_list);

static struct usb_composite_dev g_cdev;
static u8 usbreq_buf[USB_BUFSIZ] __attribute__((aligned(64)));

static LIST_HEAD(udc_list);

static struct usb_composite_driver *composite;

static const struct utf8_table utf8_table[] =
{
    {0x80,  0x00,   0*6,    0x7F,           0,         /* 1 byte sequence */},
    {0xE0,  0xC0,   1*6,    0x7FF,          0x80,      /* 2 byte sequence */},
    {0xF0,  0xE0,   2*6,    0xFFFF,         0x800,     /* 3 byte sequence */},
    {0xF8,  0xF0,   3*6,    0x1FFFFF,       0x10000,   /* 4 byte sequence */},
    {0xFC,  0xF8,   4*6,    0x3FFFFFF,      0x200000,  /* 5 byte sequence */},
    {0xFE,  0xFC,   5*6,    0x7FFFFFFF,     0x4000000, /* 6 byte sequence */},
    {   0,     0,     0,             0,             0, /* end of table */}
};

enum buff_type {
    BUFF_TYPE_DESCRIPTOR,
    BUFF_TYPE_GADGET_STRING,
};

#define GADGET_STRING_BUFF_SIZE 160
struct buff_manage_gadget_string {
    int in_use;
    u8  buff[GADGET_STRING_BUFF_SIZE];
} bm_gs[MAX_FUNC_NUM];

#define DESCRIPTOR_BUFF_SIZE 512
struct buff_manage_descriptor {
    int in_use;
    u8  buff[DESCRIPTOR_BUFF_SIZE];
} bm_desc[MAX_FUNC_NUM];

static void init_mempool(void)
{
    memset(bm_gs,   0, sizeof(bm_gs));
    memset(bm_desc, 0, sizeof(bm_desc));
    return ;
}

static void *alloc_mem(enum buff_type type)
{
    int i;

    switch (type) {
        case BUFF_TYPE_DESCRIPTOR:
            for (i = 0; i < ARRAY_SIZE(bm_desc); i++) {
                if (!bm_desc[i].in_use) {
                    DBG ("<alloc> desc index : %d\n", i);
                    bm_desc[i].in_use = 1;
                    return (void *)bm_desc[i].buff;
                }
            }
            break;
        case BUFF_TYPE_GADGET_STRING:
            for (i = 0; i < ARRAY_SIZE(bm_gs); i++) {
                if (!bm_gs[i].in_use) {
                    DBG ("<alloc> gs index : %d\n", i);
                    bm_gs[i].in_use = 1;
                    return (void *)bm_gs[i].buff;
                }
            }
            break;
        default:
            break;
    }

    return NULL;
}

static void free_mem(enum buff_type type, void *buf)
{
    int i;

    switch (type) {
        case BUFF_TYPE_DESCRIPTOR:
            for (i = 0; i < ARRAY_SIZE(bm_desc); i++) {
                if ((unsigned int)bm_desc[i].buff == (unsigned int)buf) {
                    DBG ("<free> desc index : %d\n", i);
                    bm_desc[i].in_use = 0;
                    return ;
                }
            }
            break;
        case BUFF_TYPE_GADGET_STRING:
            for (i = 0; i < ARRAY_SIZE(bm_gs); i++) {
                if ((unsigned int)bm_gs[i].buff == (unsigned int)buf) {
                    DBG ("<free> gs index : %d\n", i);
                    bm_gs[i].in_use = 0;
                    return ;
                }
            }
            break;
        default:
            break;
    }

    return ;
}

//  function driver

static struct usb_function_instance *try_get_usb_function_instance(const char *name)
{
    struct usb_function_driver *fd;
    struct usb_function_instance *fi;

    fi = NULL;

    list_for_each_entry(fd, &func_list, list) {
        if (strcmp(name, fd->name))
            continue;
        fi = fd->alloc_inst();
        fi->fd = fd;
        break;
    }
    return fi;
}

struct usb_function_instance *usb_get_function_instance(const char *name)
{
    return try_get_usb_function_instance(name);
}

struct usb_function *usb_get_function(struct usb_function_instance *fi)
{
    struct usb_function *f;

    f = fi->fd->alloc_func(fi);
    f->fi = fi;
    return f;
}

void usb_put_function_instance(struct usb_function_instance *fi)
{
    if (!fi)
        return;

    fi->free_func_inst(fi);

    return ;
}

void usb_put_function(struct usb_function *f)
{
    if (!f)
        return;

    f->free_func(f);

    return ;
}

int usb_function_register(struct usb_function_driver *newf)
{
    struct usb_function_driver *fd;
    int ret;

    ret = -EEXIST;

    list_for_each_entry(fd, &func_list, list) {
        if (!strcmp(fd->name, newf->name)) {
            printf ("%s, Failed to register usb function, already exist\n", __func__);
            goto out;
        }
    }
    ret = 0;
    list_add_tail(&newf->list, &func_list);
out:
    return ret;
}

void usb_function_unregister(struct usb_function_driver *fd)
{
    list_del(&fd->list);

    return ;
}

int usb_descriptor_fillbuf(void *buf, unsigned buflen, const struct usb_descriptor_header **src)
{
    u8    *dest = buf;

    if (!src)
        return -EINVAL;

    /* fill buffer from src[] until null descriptor ptr */
    for (; NULL != *src; src++) {
        unsigned        len = (*src)->bLength;

        if (len > buflen)
            return -EINVAL;
        memcpy(dest, *src, len);
        buflen -= len;
        dest += len;
    }
    return dest - (u8 *)buf;
}

struct usb_descriptor_header **usb_copy_descriptors(struct usb_descriptor_header **src)
{
    struct usb_descriptor_header **tmp;
    unsigned bytes;
    unsigned n_desc;
    void *mem;
    struct usb_descriptor_header **ret;

    /* count descriptors and their sizes; then add vector size */
    for (bytes = 0, n_desc = 0, tmp = src; *tmp; tmp++, n_desc++)
        bytes += (*tmp)->bLength;
    bytes += (n_desc + 1) * sizeof(*tmp);

    mem = alloc_mem(BUFF_TYPE_DESCRIPTOR);
    if (!mem) {
        ERR ("%s alloc memory failed\n", __func__);
        return NULL;
    }

    /* fill in pointers starting at "tmp",
     * to descriptors copied starting at "mem";
     * and return "ret"
     */
    tmp = mem;
    ret = mem;
    mem = (void *)((unsigned int)mem + (n_desc + 1) * sizeof(*tmp));
    while (*src) {
        memcpy(mem, *src, (*src)->bLength);
        *tmp = mem;
        tmp++;
        mem = (void *)((unsigned int)mem + (*src)->bLength);
        src++;
    }
    *tmp = NULL;

    return ret;
}

static inline void usb_free_descriptors(struct usb_descriptor_header **v)
{
    if (v)
        free_mem(BUFF_TYPE_DESCRIPTOR, v);

    return ;
}

void usb_free_all_descriptors(struct usb_function *f)
{
    usb_free_descriptors(f->fs_descriptors);
    usb_free_descriptors(f->hs_descriptors);
    usb_free_descriptors(f->ss_descriptors);
}


int usb_assign_descriptors(struct usb_function *f,
        struct usb_descriptor_header **fs,
        struct usb_descriptor_header **hs,
        struct usb_descriptor_header **ss)
{
    struct usb_gadget *g = f->config->cdev->gadget;

    if (fs) {
        f->fs_descriptors = usb_copy_descriptors(fs);
        if (!f->fs_descriptors)
            goto err;
    }

    if (hs && gadget_is_dualspeed(g)) {
        f->hs_descriptors = usb_copy_descriptors(hs);
        if (!f->hs_descriptors)
            goto err;
    }

    if (ss && gadget_is_superspeed(g)) {
        f->ss_descriptors = usb_copy_descriptors(ss);
        if (!f->ss_descriptors)
            goto err;
    }

    return 0;
err:
    usb_free_all_descriptors(f);
    return -ENOMEM;
}

//   ep autoconfig

int usb_gadget_ep_match_desc(struct usb_gadget *gadget,
        struct usb_ep *ep, struct usb_endpoint_descriptor *desc,
        struct usb_ss_ep_comp_descriptor *ep_comp)
{
    u8        type;
    u16        max;
    int        num_req_streams = 0;

    if (ep->claimed)
        return 0;

    type = usb_endpoint_type(desc);
    max = 0x7ff & usb_endpoint_maxp(desc);

    if (usb_endpoint_dir_in(desc) && !ep->caps.dir_in)
        return 0;
    if (usb_endpoint_dir_out(desc) && !ep->caps.dir_out)
        return 0;

    if (max > ep->maxpacket_limit)
        return 0;

    /* "high bandwidth" works only at high speed */
    if (!gadget_is_dualspeed(gadget) && usb_endpoint_maxp(desc) & (3<<11))
        return 0;

    switch (type) {
        case USB_ENDPOINT_XFER_CONTROL:
            /* only support ep0 for portable CONTROL traffic */
            return 0;
        case USB_ENDPOINT_XFER_ISOC:
            /* ISO:  limit 1023 bytes full speed, 1024 high/super speed */
            if (!gadget_is_dualspeed(gadget) && max > 1023)
                return 0;
            break;
        case USB_ENDPOINT_XFER_BULK:
            if (ep_comp) {
                /* Get the number of required streams from the
                 * EP companion descriptor and see if the EP
                 * matches it
                 */
                num_req_streams = ep_comp->bmAttributes & 0x1f;
                if (num_req_streams > ep->max_streams)
                    return 0;
            }
            break;
        case USB_ENDPOINT_XFER_INT:
            /* INT:  limit 64 bytes full speed, 1024 high/super speed */
            if (!gadget_is_dualspeed(gadget) && max > 64)
                return 0;
            break;
    }

    return 1;
}

struct usb_ep *usb_ep_autoconfig_ss(
        struct usb_gadget        *gadget,
        struct usb_endpoint_descriptor    *desc,
        struct usb_ss_ep_comp_descriptor *ep_comp)
{
    struct usb_ep    *ep;
    u8        type;

    type = desc->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK;

    /* Second, look at endpoints until an unclaimed one looks usable */
    list_for_each_entry (ep, &gadget->ep_list, ep_list) {
        if (usb_gadget_ep_match_desc(gadget, ep, desc, ep_comp))
            goto found_ep;
    }

    ERR("There are no endpoints available\n");

    /* Fail */
    return NULL;
found_ep:

    /*
     * If the protocol driver hasn't yet decided on wMaxPacketSize
     * and wants to know the maximum possible, provide the info.
     */
    if (desc->wMaxPacketSize == 0)
        desc->wMaxPacketSize = cpu_to_le16(ep->maxpacket_limit);

    /* report address */
    desc->bEndpointAddress &= USB_DIR_IN;
    if (isdigit(ep->name[2])) {
        u8 num = simple_strtoul(&ep->name[2], NULL, 10);
        desc->bEndpointAddress |= num;
    } else if (desc->bEndpointAddress & USB_DIR_IN) {
        if (++gadget->in_epnum > 15)
            return NULL;
        desc->bEndpointAddress = USB_DIR_IN | gadget->in_epnum;
    } else {
        if (++gadget->out_epnum > 15)
            return NULL;
        desc->bEndpointAddress |= gadget->out_epnum;
    }

    /* report (variable) high speed bulk maxpacket */
    if ((type == USB_ENDPOINT_XFER_BULK) && !ep_comp) {
        int size = ep->maxpacket_limit;

        /* min() doesn't work on bitfields with gcc-3.5 */
        if (size > 64)
            size = 64;
        desc->wMaxPacketSize = cpu_to_le16(size);
    }

    ep->address = desc->bEndpointAddress;
    ep->desc = NULL;
    ep->comp_desc = NULL;
    ep->claimed = true;
    return ep;
}

struct usb_ep *usb_ep_autoconfig(
        struct usb_gadget        *gadget,
        struct usb_endpoint_descriptor    *desc
        )
{
    return usb_ep_autoconfig_ss(gadget, desc, NULL);
}

//  udc driver

void usb_ep_autoconfig_reset (struct usb_gadget *gadget)
{
    struct usb_ep    *ep;

    list_for_each_entry (ep, &gadget->ep_list, ep_list) {
        ep->claimed = false;
        ep->driver_data = NULL;
    }
    gadget->in_epnum = 0;
    gadget->out_epnum = 0;

    return ;
}

void usb_gadget_giveback_request(struct usb_ep *ep,
        struct usb_request *req)
{
    req->complete(ep, req);

    return ;
}

struct usb_ep *gadget_find_ep_by_name(struct usb_gadget *g, const char *name)
{
    struct usb_ep *ep;

    gadget_for_each_ep(ep, g) {
        if (!strcmp(ep->name, name))
            return ep;
    }

    return NULL;
}

static void usb_udc_connect_control(struct usb_udc *udc)
{
    if (udc->vbus)
        usb_gadget_connect(udc->gadget);
    else
        usb_gadget_disconnect(udc->gadget);

    return ;
}

void usb_udc_vbus_handler(struct usb_gadget *gadget, bool status)
{
    struct usb_udc *udc = gadget->udc;

    if (udc) {
        udc->vbus = status;
        usb_udc_connect_control(udc);
    }

    return ;
}

void usb_gadget_udc_reset(struct usb_gadget *gadget,
        struct usb_gadget_driver *driver)
{
    driver->reset(gadget);
    usb_gadget_set_state(gadget, USB_STATE_DEFAULT);

    return ;
}

static inline int usb_gadget_udc_start(struct usb_udc *udc)
{
    return udc->gadget->ops->udc_start(udc->gadget, udc->driver);
}

static inline void usb_gadget_udc_stop(struct usb_udc *udc)
{
    udc->gadget->ops->udc_stop(udc->gadget);

    return ;
}

static void usb_udc_release(struct device *dev)
{
    struct usb_udc *udc;

    udc = container_of(dev, struct usb_udc, dev);
    memset(udc, 0, sizeof(*udc));//free(udc);

    return ;
}

static void usb_udc_nop_release(struct device *dev)
{
    DBG("%s\n", __func__);

    return ;
}
void usb_gadget_set_state(struct usb_gadget *gadget,
        enum usb_device_state state)
{
    gadget->state = state;

    return ;
}

int usb_add_gadget_udc_release(struct device *parent, struct usb_gadget *gadget,
        void (*release)(struct device *dev))
{
    struct usb_udc        *udc;

    udc = &g_udc;

    memset(udc, 0, sizeof(struct usb_udc));
    gadget->dev.parent = parent;

    if (release)
        gadget->dev.release = release;
    else
        gadget->dev.release = usb_udc_nop_release;

    udc->dev.release = usb_udc_release;
    udc->dev.parent = parent;

    udc->gadget = gadget;
    gadget->udc = udc;

    list_add_tail(&udc->list, &udc_list);

    usb_gadget_set_state(gadget, USB_STATE_NOTATTACHED);
    udc->vbus = true;

    return 0;
}

int usb_add_gadget_udc(struct device *parent, struct usb_gadget *gadget)
{
    return usb_add_gadget_udc_release(parent, gadget, NULL);
}


static void usb_gadget_remove_driver(struct usb_udc *udc)
{
    usb_gadget_disconnect(udc->gadget);
    udc->driver->disconnect(udc->gadget);
    udc->driver->unbind(udc->gadget);
    usb_gadget_udc_stop(udc);

    udc->driver = NULL;

    return ;
}

void usb_del_gadget_udc(struct usb_gadget *gadget)
{
    struct usb_udc *udc = gadget->udc;

    if (!udc)
        return;

    DBG("unregistering gadget\n");

    list_del(&udc->list);

    if (udc->driver)
        usb_gadget_remove_driver(udc);

    return ;
}

static int udc_bind_to_driver(struct usb_udc *udc, struct usb_gadget_driver *driver)
{
    int ret;

    DBG("registering UDC driver [%s]\n",driver->function);

    udc->driver = driver;
    //udc->dev.driver = &driver->driver;
    //udc->gadget->dev.driver = &driver->driver;

    ret = driver->bind(udc->gadget/*, driver*/);//bind = composite_bind
    if (ret)
        goto err1;
    ret = usb_gadget_udc_start(udc);
    if (ret) {
        driver->unbind(udc->gadget);
        goto err1;
    }
    usb_udc_connect_control(udc);


    return 0;
err1:
    if (ret != -EISNAM)
        ERR ("failed to start %s: %d\n",udc->driver->function, ret);
    udc->driver = NULL;
    //udc->dev.driver = NULL;
    //udc->gadget->dev.driver = NULL;
    return ret;
}

int usb_udc_attach_driver(const char *name, struct usb_gadget_driver *driver)
{
    struct usb_udc *udc = NULL;
    int ret = -ENODEV;

    list_for_each_entry(udc, &udc_list, list) {
        ret = strcmp(name, /*dev_name(&udc->dev)*/"my_gadget");
        if (!ret)
            break;
    }

    if (ret) {
        ret = -ENODEV;
        goto out;
    }
    if (udc->driver) {
        ret = -EBUSY;
        goto out;
    }
    ret = udc_bind_to_driver(udc, driver);
out:
    return ret;
}

int usb_gadget_probe_driver(struct usb_gadget_driver *driver)
{
    struct usb_udc        *udc = NULL;
    int            ret;

    if (!driver || !driver->bind || !driver->setup)
        return -EINVAL;

    list_for_each_entry(udc, &udc_list, list) {
        /* For now we take the first one */
        if (!udc->driver)
            goto found;
    }

    return -ENODEV;
found:
    ret = udc_bind_to_driver(udc, driver);
    return ret;
}

int usb_gadget_unregister_driver(struct usb_gadget_driver *driver)
{
    struct usb_udc        *udc = NULL;
    int            ret = -ENODEV;

    if (!driver || !driver->unbind)
        return -EINVAL;
    list_for_each_entry(udc, &udc_list, list)
        if (udc->driver == driver) {
            usb_gadget_remove_driver(udc);
            usb_gadget_set_state(udc->gadget,
                    USB_STATE_NOTATTACHED);
            ret = 0;
            break;
        }

    return ret;
}

//  usb_strings

static inline void put_utf16(u16 *s, unsigned c, enum utf16_endian endian)
{
    switch (endian) {
        default:
            *s = (u16) c;
            break;
        case UTF16_LITTLE_ENDIAN:
            *s = cpu_to_le16(c);
            break;
        case UTF16_BIG_ENDIAN:
            *s = cpu_to_be16(c);
            break;
    }

    return ;
}

int utf8_to_utf32(const u8 *s, int inlen, unicode_t *pu)
{
    unsigned long l;
    int c0, c, nc;
    const struct utf8_table *t;

    nc = 0;
    c0 = *s;
    l = c0;
    for (t = utf8_table; t->cmask; t++) {
        nc++;
        if ((c0 & t->cmask) == t->cval) {
            l &= t->lmask;
            if (l < t->lval || l > UNICODE_MAX ||
                    (l & SURROGATE_MASK) == SURROGATE_PAIR)
                return -1;
            *pu = (unicode_t) l;
            return nc;
        }
        if (inlen <= nc)
            return -1;
        s++;
        c = (*s ^ 0x80) & 0xFF;
        if (c & 0xC0)
            return -1;
        l = (l << 6) | c;
    }
    return -1;
}

int utf8s_to_utf16s(const u8 *s, int inlen, enum utf16_endian endian,
        /*wchar_t*/u16 *pwcs, int maxout)
{
    u16 *op;
    int size;
    unicode_t u;

    op = pwcs;
    while (inlen > 0 && maxout > 0 && *s) {
        if (*s & 0x80) {
            size = utf8_to_utf32(s, inlen, &u);
            if (size < 0)
                return -EINVAL;
            s += size;
            inlen -= size;

            if (u >= PLANE_SIZE) {
                if (maxout < 2)
                    break;
                u -= PLANE_SIZE;
                put_utf16(op++, SURROGATE_PAIR |
                        ((u >> 10) & SURROGATE_BITS),
                        endian);
                put_utf16(op++, SURROGATE_PAIR |
                        SURROGATE_LOW |
                        (u & SURROGATE_BITS),
                        endian);
                maxout -= 2;
            } else {
                put_utf16(op++, u, endian);
                maxout--;
            }
        } else {
            put_utf16(op++, *s++, endian);
            inlen--;
            maxout--;
        }
    }
    return op - pwcs;
}

int usb_gadget_get_string (struct usb_gadget_strings *table, int id, u8 *buf)
{
    struct usb_string    *s;
    int            len;

    /* descriptor 0 has the language id */
    if (id == 0) {
        buf [0] = 4;
        buf [1] = USB_DT_STRING;
        buf [2] = (u8) table->language;
        buf [3] = (u8) (table->language >> 8);
        return 4;
    }
    for (s = table->strings; s && s->s; s++)
        if (s->id == id)
            break;

    /* unrecognized: stall. */
    if (!s || !s->s)
        return -EINVAL;

    /* string descriptors have length, tag, then UTF16-LE text */
    len = min ((size_t) 126, strlen (s->s));
    len = utf8s_to_utf16s((const u8 *)s->s, len, UTF16_LITTLE_ENDIAN,
            (/*wchar_t*/u16 *) &buf[2], 126);
    if (len < 0)
        return -EINVAL;
    buf [0] = (len + 1) * 2;
    buf [1] = USB_DT_STRING;
    return buf [0];
}

//  composite driver

static void composite_setup_complete(struct usb_ep *ep, struct usb_request *req)
{
    if (req->status || req->actual != req->length)
        DBG("%s: setup complete --> %d, %u/%u\n", __func__, req->status, req->actual, req->length);

    return ;
}

static int composite_ep0_queue(struct usb_composite_dev *cdev,
        struct usb_request *req)
{
    int ret;
    ret = usb_ep_queue(cdev->gadget->ep0, req);
    return ret;
}

void usb_composite_setup_continue(struct usb_composite_dev *cdev)
{
    int            value;
    struct usb_request    *req = cdev->req;

    req->length = 0;
    req->context = cdev;
    value = composite_ep0_queue(cdev, req);
    if (value < 0) {
        DBG("ep_queue --> %d\n", value);
        req->status = 0;
        composite_setup_complete(cdev->gadget->ep0, req);
    }

    return ;
}

int usb_interface_id(struct usb_configuration *config,
        struct usb_function *function)
{
    unsigned id = config->next_interface_id;

    if (id < MAX_CONFIG_INTERFACES) {
        config->interface[id] = function;
        config->next_interface_id = id + 1;
        return id;
    }
    return -ENODEV;
}

int usb_add_function(struct usb_configuration *config,
        struct usb_function *function)
{
    int    value = -EINVAL;

    DBG ("%s, new func name : %s\n", __func__, function->name);

    if (!function->set_alt || !function->disable) {
        printf ("%s, usb add function failed\n", __func__);
        goto done;
    }

    function->config = config;
    list_add_tail(&function->list, &config->functions);

    /* REVISIT *require* function->bind? */
    if (function->bind) {
        value = function->bind(config, function);
        if (value < 0) {
            list_del(&function->list);
            function->config = NULL;
        }
    } else
        value = 0;

    /* We allow configurations that don't work at both speeds.
     * If we run into a lowspeed Linux system, treat it the same
     * as full speed ... it's the function drivers that will need
     * to avoid bulk and ISO transfers.
     */

    if (!config->fullspeed && function->fs_descriptors)
        config->fullspeed = true;
    if (!config->highspeed && function->hs_descriptors)
        config->highspeed = true;
    if (!config->superspeed && function->ss_descriptors)
        config->superspeed = true;

    DBG ("%s, fs : %d hs : %d ss : %d\n", __func__, config->fullspeed, config->highspeed, config->superspeed);

done:
    if (value)
        ERR ("adding '%s'/%p --> %d\n", function->name, function, value);
    return value;
}

void usb_remove_function(struct usb_configuration *c, struct usb_function *f)
{
    if (f->disable)
        f->disable(f);

    bitmap_zero(f->endpoints, 32);
    list_del(&f->list);
    if (f->unbind)
        f->unbind(c, f);

    return ;
}

static void collect_langs(struct usb_gadget_strings **sp, __le16 *buf)
{
    const struct usb_gadget_strings    *s;
    u16                language;
    __le16                *tmp;

    while (*sp) {
        s = *sp;
        language = cpu_to_le16(s->language);
        for (tmp = buf; *tmp && tmp < &buf[126]; tmp++) {
            if (*tmp == language)
                goto repeat;
        }
        *tmp++ = language;
repeat:
        sp++;
    }

    return ;
}

static int lookup_string(
        struct usb_gadget_strings    **sp,
        void                *buf,
        u16                language,
        int                id
        )
{
    int                value;
    struct usb_gadget_strings    *s;

    while (*sp) {
        s = *sp++;
        if (s->language != language)
            continue;
        value = usb_gadget_get_string(s, id, buf);
        if (value > 0)
            return value;
    }
    return -EINVAL;
}

static struct usb_gadget_strings **get_containers_gs(
        struct usb_gadget_string_container *uc)
{
    return (struct usb_gadget_strings **)uc->stash;
}

static int get_string(struct usb_composite_dev *cdev,
        void *buf, u16 language, int id)
{
    struct usb_composite_driver *composite = cdev->driver;
    struct usb_gadget_string_container *uc;
    struct usb_configuration    *c;
    struct usb_function        *f;
    int                len;

    /*
     * Yes, not only is USB's I18N support probably more than most
     * folk will ever care about ... also, it's all supported here.
     * (Except for UTF8 support for Unicode's "Astral Planes".)
     */

    /* 0 == report all available language codes */
    if (id == 0) {
        struct usb_string_descriptor    *s = buf;
        struct usb_gadget_strings    **sp;

        memset(s, 0, /*256*/USB_BUFSIZ);
        s->bDescriptorType = USB_DT_STRING;

        sp = composite->strings;
        if (sp)
            collect_langs(sp, s->wData);

        list_for_each_entry(c, &cdev->configs, list) {
            sp = c->strings;
            if (sp)
                collect_langs(sp, s->wData);

            list_for_each_entry(f, &c->functions, list) {
                sp = f->strings;
                if (sp)
                    collect_langs(sp, s->wData);
            }
        }

        list_for_each_entry(uc, &cdev->gstrings, list) {
            struct usb_gadget_strings **sp;

            sp = get_containers_gs(uc);
            collect_langs(sp, s->wData);
        }

        for (len = 0; len <= 126 && s->wData[len]; len++)
            continue;
        if (!len)
            return -EINVAL;

        s->bLength = 2 * (len + 1);
        return s->bLength;
    }

#if 0
    if (cdev->use_os_string && language == 0 && id == OS_STRING_IDX) {
        struct usb_os_string *b = buf;
        b->bLength = sizeof(*b);
        b->bDescriptorType = USB_DT_STRING;
        compiletime_assert(
                sizeof(b->qwSignature) == sizeof(cdev->qw_sign),
                "qwSignature size must be equal to qw_sign");
        memcpy(&b->qwSignature, cdev->qw_sign, sizeof(b->qwSignature));
        b->bMS_VendorCode = cdev->b_vendor_code;
        b->bPad = 0;
        return sizeof(*b);
    }
#endif

    list_for_each_entry(uc, &cdev->gstrings, list) {
        struct usb_gadget_strings **sp;

        sp = get_containers_gs(uc);
        len = lookup_string(sp, buf, language, id);
        if (len > 0)
            return len;
    }

    /*
     * Otherwise, look up and return a specified string.  String IDs
     * are device-scoped, so we look up each string table we're told
     * about.  These lookups are infrequent; simpler-is-better here.
     */
    if (composite->strings) {
        len = lookup_string(composite->strings, buf, language, id);
        if (len > 0)
            return len;
    }
    list_for_each_entry(c, &cdev->configs, list) {
        if (c->strings) {
            len = lookup_string(c->strings, buf, language, id);
            if (len > 0)
                return len;
        }
        list_for_each_entry(f, &c->functions, list) {
            if (!f->strings)
                continue;
            len = lookup_string(f->strings, buf, language, id);
            if (len > 0)
                return len;
        }
    }
    return -EINVAL;
}

static u8 encode_bMaxPower(enum usb_device_speed speed,
        struct usb_configuration *c)
{
    unsigned val;

    if (c->bMaxPower)
        val = c->bMaxPower;
    else
        val = CONFIG_USB_GADGET_VBUS_DRAW;
    if (!val)
        return 0;
    switch (speed) {
        case USB_SPEED_SUPER:
            return DIV_ROUND_UP(val, 8);
        default:
            return DIV_ROUND_UP(val, 2);
    }
}

static int config_buf(struct usb_configuration *config,
        enum usb_device_speed speed, void *buf, u8 type)
{
    int                len = USB_BUFSIZ - USB_DT_CONFIG_SIZE;
    void                *next = (void *)((unsigned int)buf + USB_DT_CONFIG_SIZE);
    struct usb_descriptor_header    **descriptors;
    struct usb_config_descriptor    *c = buf;
    int                status;
    struct usb_function        *f;

    /* write the config descriptor */
    c = buf;
    c->bLength = USB_DT_CONFIG_SIZE;
    c->bDescriptorType = type;

    c->bNumInterfaces = config->next_interface_id;
    c->bConfigurationValue = config->bConfigurationValue;
    c->iConfiguration = config->iConfiguration;
    c->bmAttributes = USB_CONFIG_ATT_ONE | config->bmAttributes;
    c->bMaxPower = encode_bMaxPower(speed, config);

    /* There may be e.g. OTG descriptors */
    if (config->descriptors) {
        status = usb_descriptor_fillbuf(next, len,
                config->descriptors);
        if (status < 0)
            return status;
        len -= status;
        next = (void *)((unsigned int)next + status);
    }

    /* add each function's descriptors */
    list_for_each_entry(f, &config->functions, list) {

        switch (speed) {
            case USB_SPEED_SUPER:
                descriptors = f->ss_descriptors;
                break;
            case USB_SPEED_HIGH:
                descriptors = f->hs_descriptors;
                break;
            default:
                descriptors = f->fs_descriptors;
        }

        if (!descriptors)
            continue;

        status = usb_descriptor_fillbuf(next, len,
                (const struct usb_descriptor_header **) descriptors);

        if (status < 0)
            return status;
        len -= status;
        next = (void *)((unsigned int)next + status);
    }

    len = (unsigned int)next - (unsigned int)buf;
    c->wTotalLength = cpu_to_le16(len);
    return len;
}

static int config_desc(struct usb_composite_dev *cdev, unsigned w_value)
{
    enum usb_device_speed        speed = USB_SPEED_UNKNOWN;
    struct usb_gadget        *gadget = cdev->gadget;
    u8                type = w_value >> 8;
    struct usb_configuration        *c;

    if (gadget->speed == USB_SPEED_SUPER)
        speed = gadget->speed;
    else if (gadget_is_dualspeed(gadget)) {
        int hs = 0;
        if (gadget->speed == USB_SPEED_HIGH)
            hs = 1;
        if (type == USB_DT_OTHER_SPEED_CONFIG)
            hs = !hs;
        if (hs)
            speed = USB_SPEED_HIGH;
    }

    w_value &= 0xff;

    list_for_each_entry(c, &cdev->configs, list) {

        switch (speed) {
            case USB_SPEED_SUPER:
                if (!c->superspeed)
                    continue;
                break;
            case USB_SPEED_HIGH:
                if (!c->highspeed)
                    continue;
                break;
            default:
                if (!c->fullspeed)
                    continue;
        }
        if (w_value == 0)
            return config_buf(c, speed, cdev->req->buf, type);
        w_value--;
    }

    return -EINVAL;
}

static int count_configs(struct usb_composite_dev *cdev, unsigned type)
{
    struct usb_gadget        *gadget = cdev->gadget;
    struct usb_configuration    *c;
    unsigned            count = 0;
    int                hs = 0;
    int                ss = 0;

    if (gadget_is_dualspeed(gadget)) {
        if (gadget->speed == USB_SPEED_HIGH)
            hs = 1;
        if (gadget->speed == USB_SPEED_SUPER)
            ss = 1;
        if (type == USB_DT_DEVICE_QUALIFIER)
            hs = !hs;
    }
    list_for_each_entry(c, &cdev->configs, list) {
        /* ignore configs that won't work at this speed */

        if (ss) {
            if (!c->superspeed)
                continue;
        } else if (hs) {
            if (!c->highspeed)
                continue;
        } else {
            if (!c->fullspeed)
                continue;
        }
        count++;
    }
    return count;
}

static void device_qual(struct usb_composite_dev *cdev)
{
    struct usb_qualifier_descriptor    *qual = cdev->req->buf;

    qual->bLength = sizeof(*qual);
    qual->bDescriptorType = USB_DT_DEVICE_QUALIFIER;
    /* POLICY: same bcdUSB and device type info at both speeds */
    qual->bcdUSB = cdev->desc.bcdUSB;
    qual->bDeviceClass = cdev->desc.bDeviceClass;
    qual->bDeviceSubClass = cdev->desc.bDeviceSubClass;
    qual->bDeviceProtocol = cdev->desc.bDeviceProtocol;
    /* ASSUME same EP0 fifo size at both speeds */
    qual->bMaxPacketSize0 = cdev->gadget->ep0->maxpacket;
    qual->bNumConfigurations = count_configs(cdev, USB_DT_DEVICE_QUALIFIER);
    qual->bRESERVED = 0;

    return ;
}

static void reset_config(struct usb_composite_dev *cdev)
{
    struct usb_function        *f;

    list_for_each_entry(f, &cdev->config->functions, list) {

        if (f->disable)
            f->disable(f);

        bitmap_zero(f->endpoints, 32);
    }
    cdev->config = NULL;

    return ;
}

static int set_config(struct usb_composite_dev *cdev,
        const struct usb_ctrlrequest *ctrl, unsigned number)
{
    struct usb_gadget    *gadget = cdev->gadget;
    struct usb_configuration *c = NULL;
    int            result = -EINVAL;
    unsigned        power = gadget_is_otg(gadget) ? 8 : 100;
    int            tmp;

    DBG("%s, use %d configuration\n", __func__, number);

    if (number) {
        list_for_each_entry(c, &cdev->configs, list) {
            if (c->bConfigurationValue == number) {
                /*
                 * We disable the FDs of the previous
                 * configuration only if the new configuration
                 * is a valid one
                 */
                if (cdev->config)
                    reset_config(cdev);

                result = 0;
                break;
            }
        }
        if (result < 0)
            goto done;
    } else { /* Zero configuration value - need to reset the config */
        if (cdev->config)
            reset_config(cdev);
        result = 0;
    }

    debug("%s: %s speed config #%d: %s\n", __func__,
            ({ char *speed;
             switch (gadget->speed) {
                 case USB_SPEED_LOW:
                     speed = "low";
                     break;
                 case USB_SPEED_FULL:
                     speed = "full";
                     break;
                 case USB_SPEED_HIGH:
                     speed = "high";
                     break;
                 default:
                     speed = "?";
                     break;
             };
             speed;
             }), number, c ? c->label : "unconfigured");

    if (!c)
        goto done;

    usb_gadget_set_state(gadget, USB_STATE_CONFIGURED);
    cdev->config = c;

    /* Initialize all interfaces by setting them to altsetting zero. */
    for (tmp = 0; tmp < MAX_CONFIG_INTERFACES; tmp++) {
        struct usb_function    *f = c->interface[tmp];
        struct usb_descriptor_header **descriptors;

        if (!f)
            break;

        /*
         * Record which endpoints are used by the function. This is used
         * to dispatch control requests targeted at that endpoint to the
         * function's setup callback instead of the current
         * configuration's setup callback.
         */
        switch (gadget->speed) {
            case USB_SPEED_SUPER:
                descriptors = f->ss_descriptors;
                break;
            case USB_SPEED_HIGH:
                descriptors = f->hs_descriptors;
                break;
            default:
                descriptors = f->fs_descriptors;
        }

        for (; *descriptors; ++descriptors) {
            struct usb_endpoint_descriptor *ep;
            int addr;

            if ((*descriptors)->bDescriptorType != USB_DT_ENDPOINT)
                continue;

            ep = (struct usb_endpoint_descriptor *)*descriptors;
            addr = ((ep->bEndpointAddress & 0x80) >> 3)
                |    (ep->bEndpointAddress & 0x0f);
            set_bit(addr, f->endpoints);
        }

        result = f->set_alt(f, tmp, 0);
        if (result < 0) {
            debug("interface %d (%s/%p) alt 0 --> %d\n", tmp, f->name, f, result);

            reset_config(cdev);
            goto done;
        }
    }

    /* when we return, be sure our power usage is valid */
    power = c->bMaxPower ? c->bMaxPower : CONFIG_USB_GADGET_VBUS_DRAW;
done:
    usb_gadget_vbus_draw(gadget, power);
    return result;
}

int usb_add_config_only(struct usb_composite_dev *cdev,
        struct usb_configuration *config)
{
    struct usb_configuration *c;

    DBG ("%s, bconfigvalue : %d\n", __func__, config->bConfigurationValue);

    if (!config->bConfigurationValue) {
        printf ("%s, Invalid bConfigurationValue of 0\n", __func__);
        return -EINVAL;
    }

    /* Prevent duplicate configuration identifiers */
    list_for_each_entry(c, &cdev->configs, list) {
        if (c->bConfigurationValue == config->bConfigurationValue) {
            printf ("%s, Already exist this configuration\n", __func__);
            return -EBUSY;
        }
    }

    config->cdev = cdev;
    list_add_tail(&config->list, &cdev->configs);

    INIT_LIST_HEAD(&config->functions);
    config->next_interface_id = 0;
    memset(config->interface, 0, sizeof(config->interface));

    return 0;
}

int usb_add_config(struct usb_composite_dev *cdev,
        struct usb_configuration *config,
        int (*bind)(struct usb_configuration *))
{
    int                status = -EINVAL;

    if (!bind)
        goto done;

    DBG("adding config #%u '%s'/%p\n", config->bConfigurationValue, config->label, config);

    status = usb_add_config_only(cdev, config);
    if (status)
        goto done;

    status = bind(config);
    if (status < 0) {
        ERR ("%s, Failed to bind config\n", __func__);

        while (!list_empty(&config->functions)) {
            struct usb_function        *f;

            f = list_first_entry(&config->functions, struct usb_function, list);
            list_del(&f->list);
            if (f->unbind) {
                DBG( "unbind function '%s'/%p\n", f->name, f);
                f->unbind(config, f);
                /* may free memory for "f" */
            }
        }
        list_del(&config->list);
        config->cdev = NULL;
    } else {
        unsigned    i;

        DBG ("cfg %d/%p speeds:%s%s%s\n",
                config->bConfigurationValue, config,
                config->highspeed ? " high" : "",
                config->fullspeed
                ? (gadget_is_dualspeed(cdev->gadget)
                    ? " full"
                    : " full/low")
                : "");

        for (i = 0; i < MAX_CONFIG_INTERFACES; i++) {
            struct usb_function    *f = config->interface[i];

            if (!f)
                continue;

            DBG ("  interface %d = %s/%p\n", i, f->name, f);
        }
    }

    /* set_alt(), or next bind(), sets up ep->claimed as needed */
    usb_ep_autoconfig_reset(cdev->gadget);

done:
    if (status)
        ERR ("added config '%s'/%u --> %d\n", config->label,
                config->bConfigurationValue, status);
    return status;
}

int usb_string_id(struct usb_composite_dev *cdev)
{
    if (cdev->next_string_id < 254) {
        /* string id 0 is reserved by USB spec for list of
         * supported languages */
        /* 255 reserved as well? -- mina86 */
        cdev->next_string_id++;
        return cdev->next_string_id;
    }
    return -ENODEV;
}

int usb_string_ids_tab(struct usb_composite_dev *cdev, struct usb_string *str)
{
    int next = cdev->next_string_id;

    for (; str->s; ++str) {
        if (unlikely(next >= 254))
            return -ENODEV;
        str->id = ++next;
    }

    cdev->next_string_id = next;

    return 0;
}

static struct usb_gadget_string_container *copy_gadget_strings(
        struct usb_gadget_strings **sp, unsigned n_gstrings,
        unsigned n_strings)
{
    struct usb_gadget_string_container *uc;
    struct usb_gadget_strings **gs_array;
    struct usb_gadget_strings *gs;
    struct usb_string *s;
    unsigned mem;
    unsigned n_gs;
    unsigned n_s;
    void *stash;

    mem = sizeof(*uc);
    mem += sizeof(void *) * (n_gstrings + 1);
    mem += sizeof(struct usb_gadget_strings) * n_gstrings;
    mem += sizeof(struct usb_string) * (n_strings + 1) * (n_gstrings);
    uc = alloc_mem(BUFF_TYPE_GADGET_STRING);
    if (!uc) {
        ERR ("%s alloc memory failed\n", __func__);
        return NULL;
    }

    memset(uc, 0, mem);
    gs_array = get_containers_gs(uc);
    stash = uc->stash;
    stash = (void *)((unsigned int)stash + sizeof(void *) * (n_gstrings + 1));
    for (n_gs = 0; n_gs < n_gstrings; n_gs++) {
        struct usb_string *org_s;

        gs_array[n_gs] = stash;
        gs = gs_array[n_gs];
        stash = (void *)((unsigned int)stash + sizeof(struct usb_gadget_strings));
        gs->language = sp[n_gs]->language;
        gs->strings = stash;
        org_s = sp[n_gs]->strings;

        for (n_s = 0; n_s < n_strings; n_s++) {
            s = stash;
            stash = (void *)((unsigned int)stash + sizeof(struct usb_string));
            if (org_s->s)
                s->s = org_s->s;
            else
                s->s = "";
            org_s++;
        }
        s = stash;
        s->s = NULL;
        stash = (void *)((unsigned int)stash + sizeof(struct usb_string));

    }
    gs_array[n_gs] = NULL;
    return uc;
}

struct usb_string *usb_gstrings_attach(struct usb_composite_dev *cdev,
        struct usb_gadget_strings **sp, unsigned n_strings)
{
    struct usb_gadget_string_container *uc;
    struct usb_gadget_strings **n_gs;
    unsigned n_gstrings = 0;
    unsigned i;
    int ret;

    for (i = 0; sp[i]; i++)
        n_gstrings++;

    if (!n_gstrings)
        return NULL;

    uc = copy_gadget_strings(sp, n_gstrings, n_strings);
    if (!uc)
        return NULL;

    DBG ("%s, n_gstrings : %d n_strings : %d\n", __func__, n_gstrings, n_strings);

    n_gs = get_containers_gs(uc);
    ret = usb_string_ids_tab(cdev, n_gs[0]->strings);
    if (ret)
        goto err;

    for (i = 1; i < n_gstrings; i++) {
        struct usb_string *m_s;
        struct usb_string *s;
        unsigned n;

        m_s = n_gs[0]->strings;
        s = n_gs[i]->strings;
        for (n = 0; n < n_strings; n++) {
            s->id = m_s->id;
            s++;
            m_s++;
        }
    }
    list_add_tail(&uc->list, &cdev->gstrings);
    return n_gs[0]->strings;
err:
    free_mem(BUFF_TYPE_GADGET_STRING, uc);
    return NULL;
}

int config_ep_by_speed(struct usb_gadget *g,
        struct usb_function *f,
        struct usb_ep *_ep)
{
    struct usb_endpoint_descriptor *chosen_desc = NULL;
    struct usb_descriptor_header **speed_desc = NULL;

    struct usb_ss_ep_comp_descriptor *comp_desc = NULL;
    int want_comp_desc = 0;

    struct usb_descriptor_header **d_spd; /* cursor for speed desc */

    if (!g || !f || !_ep)
        return -EIO;

    /* select desired speed */
    switch (g->speed) {
        case USB_SPEED_SUPER:
            if (gadget_is_superspeed(g)) {
                speed_desc = f->ss_descriptors;
                want_comp_desc = 1;
                break;
            }
            /* else: Fall trough */
        case USB_SPEED_HIGH:
            if (gadget_is_dualspeed(g)) {
                speed_desc = f->hs_descriptors;
                break;
            }
            /* else: fall through */
        default:
            speed_desc = f->fs_descriptors;
            break;
    }
    /* find descriptors */
    for_each_ep_desc(speed_desc, d_spd) {
        chosen_desc = (struct usb_endpoint_descriptor *)*d_spd;
        if (chosen_desc->bEndpointAddress == _ep->address)
            goto ep_found;
    }
    return -EIO;

ep_found:
    /* commit results */
    _ep->maxpacket = usb_endpoint_maxp(chosen_desc);
    _ep->desc = chosen_desc;
    _ep->comp_desc = NULL;
    _ep->maxburst = 0;
    if (!want_comp_desc)
        return 0;

    /*
     * Companion descriptor should follow EP descriptor
     * USB 3.0 spec, #9.6.7
     */
    comp_desc = (struct usb_ss_ep_comp_descriptor *)*(++d_spd);
    if (!comp_desc ||
            (comp_desc->bDescriptorType != USB_DT_SS_ENDPOINT_COMP))
        return -EIO;
    _ep->comp_desc = comp_desc;
    if (g->speed == USB_SPEED_SUPER) {
        switch (usb_endpoint_type(_ep->desc)) {
            case USB_ENDPOINT_XFER_BULK:
            case USB_ENDPOINT_XFER_INT:
                _ep->maxburst = comp_desc->bMaxBurst + 1;
                break;
            default:
                if (comp_desc->bMaxBurst != 0)
                    ERR ("ep0 bMaxBurst must be 0\n");
                _ep->maxburst = 1;
                break;
        }
    }
    return 0;
}

/*
 * The setup() callback implements all the ep0 functionality that's
 * not handled lower down, in hardware or the hardware driver(like
 * device and endpoint feature flags, and their status).  It's all
 * housekeeping for the gadget function we're implementing.  Most of
 * the work is in config and function specific setup.
 */
static int composite_setup(struct usb_gadget *gadget, const struct usb_ctrlrequest *ctrl)
{
    u16                w_length = le16_to_cpu(ctrl->wLength);
    u16                w_index = le16_to_cpu(ctrl->wIndex);
    u16                w_value = le16_to_cpu(ctrl->wValue);
    struct usb_composite_dev    *cdev = get_gadget_data(gadget);
    u8                intf = w_index & 0xFF;
    int                value = -EOPNOTSUPP;
    struct usb_request        *req = cdev->req;
    struct usb_function        *f = NULL;
    int                standard;
    u8                endp;
    struct usb_configuration    *c;

    /*
     * partial re-init of the response message; the function or the
     * gadget might need to intercept e.g. a control-OUT completion
     * when we delegate to it.
     */
    req->zero = 0;
    req->complete = composite_setup_complete;
    req->length = USB_BUFSIZ;
    gadget->ep0->driver_data = cdev;
    standard = (ctrl->bRequestType & USB_TYPE_MASK)
        == USB_TYPE_STANDARD;
    if (!standard)
        goto unknown;

    switch (ctrl->bRequest) {
        /* we handle all standard USB descriptors */
        case USB_REQ_GET_DESCRIPTOR:
            if (ctrl->bRequestType != USB_DIR_IN)
                goto unknown;
            switch (w_value >> 8) {
                case USB_DT_DEVICE:
                    cdev->desc.bNumConfigurations =
                        count_configs(cdev, USB_DT_DEVICE);
                    cdev->desc.bMaxPacketSize0 =
                        cdev->gadget->ep0->maxpacket;
                    if (gadget_is_superspeed(gadget)) {
                        if (gadget->speed >= USB_SPEED_SUPER) {
                            cdev->desc.bcdUSB = cpu_to_le16(0x0300);
                            cdev->desc.bMaxPacketSize0 = 9;
                        } else {
                            cdev->desc.bcdUSB = cpu_to_le16(0x0210);
                        }
                    } else if (gadget_is_dualspeed(gadget)) {
                        cdev->desc.bcdUSB = cpu_to_le16(0x0200);
                    } else {
                        cdev->desc.bcdUSB = cpu_to_le16(0x0110);
                    }
                    value = min(w_length, (u16) sizeof cdev->desc);
                    memcpy(req->buf, &cdev->desc, value);
                    break;
                case USB_DT_DEVICE_QUALIFIER:
                    if (!gadget_is_dualspeed(gadget) ||
                            gadget->speed >= USB_SPEED_SUPER)
                        break;
                    device_qual(cdev);
                    value = min_t(int, w_length,
                        sizeof(struct usb_qualifier_descriptor));
                    break;
                case USB_DT_OTHER_SPEED_CONFIG:
                    if (!gadget_is_dualspeed(gadget) ||
                        gadget->speed >= USB_SPEED_SUPER)
                    break;
                case USB_DT_CONFIG:
                    value = config_desc(cdev, w_value);
                    if (value >= 0)
                        value = min(w_length, (u16) value);
                    break;
                case USB_DT_STRING:
                    value = get_string(cdev, req->buf,
                        w_index, w_value & 0xff);
                    if (value >= 0)
                        value = min(w_length, (u16) value);
                    break;
                case USB_DT_BOS:
                    /*
                     * The USB compliance test (USB 2.0 Command Verifier)
                     * issues this request. We should not run into the
                     * default path here. But return for now until
                     * the superspeed support is added.
                     */
                    break;
                default:
                    goto unknown;
            }
            break;

        /* any number of configs can work */
        case USB_REQ_SET_CONFIGURATION:
            if (ctrl->bRequestType != 0)
                goto unknown;
            if (gadget_is_otg(gadget)) {
                if (gadget->a_hnp_support)
                    printf ("HNP available\n");
                else if (gadget->a_alt_hnp_support)
                    printf ("HNP on another port\n");
                else
                    printf ("HNP inactive\n");
            }

            value = set_config(cdev, ctrl, w_value);
#if defined(CONFIG_MCU_UDC_ENABLE_UAC2) || defined(CONFIG_MCU_UDC_ENABLE_UAC1)
            if (our_callback_ops.callback_ops.notify_callback)
                our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_DEVICE, USB_DEVICE_CONFIGURED);
#endif
            break;
        case USB_REQ_GET_CONFIGURATION:
            if (ctrl->bRequestType != USB_DIR_IN)
                goto unknown;
            if (cdev->config)
                *(u8 *)req->buf = cdev->config->bConfigurationValue;
            else
                *(u8 *)req->buf = 0;
            value = min(w_length, (u16) 1);
            break;

        /*
         * function drivers must handle get/set altsetting; if there's
         * no get() method, we know only altsetting zero works.
         */
        case USB_REQ_SET_INTERFACE:
            if (ctrl->bRequestType != USB_RECIP_INTERFACE)
                goto unknown;
            if (!cdev->config || intf >= MAX_CONFIG_INTERFACES)
                break;
            f = cdev->config->interface[intf];
            if (!f)
                break;
            if (w_value && !f->set_alt)
                break;
            value = f->set_alt(f, w_index, w_value);
            break;
        case USB_REQ_GET_INTERFACE:
            if (ctrl->bRequestType != (USB_DIR_IN|USB_RECIP_INTERFACE))
                goto unknown;
            if (!cdev->config || intf >= MAX_CONFIG_INTERFACES)
                break;
            f = cdev->config->interface[intf];
            if (!f)
                break;
            /* lots of interfaces only need altsetting zero... */
            value = f->get_alt ? f->get_alt(f, w_index) : 0;
            if (value < 0)
                break;
            *((u8 *)req->buf) = value;
            value = min(w_length, (u16) 1);
            break;
        default:
unknown:
            debug("non-core control req%02x.%02x v%04x i%04x l%d\n",
                    ctrl->bRequestType, ctrl->bRequest,
                    w_value, w_index, w_length);

            /*
             * functions always handle their interfaces and endpoints...
             * punt other recipients (other, WUSB, ...) to the current
             * configuration code.
             */
            switch (ctrl->bRequestType & USB_RECIP_MASK) {
                case USB_RECIP_INTERFACE:
                    if (!cdev->config || intf >= MAX_CONFIG_INTERFACES)
                        break;
                    f = cdev->config->interface[intf];
                    break;

                case USB_RECIP_ENDPOINT:
                    endp = ((w_index & 0x80) >> 3) | (w_index & 0x0f);
                    list_for_each_entry(f, &cdev->config->functions, list) {
                        if (test_bit(endp, f->endpoints))
                            break;
                    }
                    if (&f->list == &cdev->config->functions)
                        f = NULL;
                    break;
            /*
             * dfu-util (version 0.5) sets bmRequestType.Receipent = Device
             * for non-standard request (w_value = 0x21,
             * bRequest = GET_DESCRIPTOR in this case).
             * When only one interface is registered (as it is done now),
             * then this request shall be handled as it was requested for
             * interface.
             *
             * In the below code it is checked if only one interface is
             * present and proper function for it is extracted. Due to that
             * function's setup (f->setup) is called to handle this
             * special non-standard request.
             */
                case USB_RECIP_DEVICE:
                    debug("cdev->config->next_interface_id: %d intf: %d\n",
                            cdev->config->next_interface_id, intf);
                    if (cdev->config->next_interface_id == 1)
                        f = cdev->config->interface[intf];
                    break;
            }

            if (f && f->setup)
                value = f->setup(f, ctrl);
            else {
                c = cdev->config;
                if (!c)
                    goto done;

                if (c->setup) {
                    value = c->setup(c, ctrl);
                    goto done;
                }

                /* try the only function in the current config */
                if (!list_is_singular(&c->functions))
                    goto done;
                f = list_first_entry(&c->functions, struct usb_function,
                        list);
                if (f->setup)
                    value = f->setup(f, ctrl);
            }

            goto done;
    }

    /* respond with data transfer before status phase? */
    if (value >= 0) {
        req->length = value;
        req->context = cdev;
        req->zero = value < w_length;
        value = usb_ep_queue(gadget->ep0, req);
        if (value < 0) {
            debug("ep_queue --> %d\n", value);
            req->status = 0;
            composite_setup_complete(gadget->ep0, req);
        }
    }

done:
    /* device either stalls (value < 0) or reports success */
    return value;
}

static void composite_disconnect(struct usb_gadget *gadget)
{
    struct usb_composite_dev    *cdev = get_gadget_data(gadget);

    if (cdev->config)
        reset_config(cdev);
    if (composite->disconnect)
        composite->disconnect(cdev);

    return ;
}

void composite_dev_cleanup(struct usb_composite_dev *cdev)
{
    struct usb_gadget_string_container *uc, *tmp;

    list_for_each_entry_safe(uc, tmp, &cdev->gstrings, list) {
        list_del(&uc->list);
        free_mem(BUFF_TYPE_GADGET_STRING, uc);
    }

    if (cdev->req) {
        //        kfree(cdev->req->buf);
        usb_ep_free_request(cdev->gadget->ep0, cdev->req);
    }

    cdev->next_string_id = 0;
}

static void remove_config(struct usb_composite_dev *cdev,
        struct usb_configuration *config)
{
    while (!list_empty(&config->functions)) {
        struct usb_function     *f;

        f = list_first_entry(&config->functions,
                struct usb_function, list);
        list_del(&f->list);
        if (f->unbind) {
            DBG(cdev, "unbind function '%s'/%p\n", f->name, f);
            f->unbind(config, f);
            /* may free memory for "f" */
        }
    }
    list_del(&config->list);
    if (config->unbind) {
        DBG(cdev, "unbind config '%s'/%p\n", config->label, config);
        config->unbind(config);
        /* may free memory for "c" */
    }
}

static void __composite_unbind(struct usb_gadget *gadget, bool unbind_driver)
{
    struct usb_composite_dev    *cdev = get_gadget_data(gadget);

    /*
     * composite_disconnect() must already have been called
     * by the underlying peripheral controller driver!
     * so there's no i/o concurrency that could affect the
     * state protected by cdev->lock.
     */
    BUG_ON(cdev->config);

    while (!list_empty(&cdev->configs)) {
        struct usb_configuration    *c;
        c = list_first_entry(&cdev->configs,
                struct usb_configuration, list);
        remove_config(cdev, c);
    }
    if (cdev->driver->unbind && unbind_driver)
        cdev->driver->unbind(cdev);

    composite_dev_cleanup(cdev);

    //    free(cdev);
    set_gadget_data(gadget, NULL);

    composite = NULL;
}

static void composite_unbind(struct usb_gadget *gadget)
{
    __composite_unbind(gadget, true);
}

static int composite_bind(struct usb_gadget *gadget/*,struct usb_gadget_driver *gdriver*/)
{
    int             status = -ENOMEM;
    struct usb_composite_dev    *cdev;

    cdev = &g_cdev;

    memset(cdev, 0, sizeof(*cdev));
    cdev->gadget = gadget;
    set_gadget_data(gadget, cdev);
    INIT_LIST_HEAD(&cdev->configs);
    INIT_LIST_HEAD(&cdev->gstrings);

    /* preallocate control response and buffer */
    cdev->req = usb_ep_alloc_request(gadget->ep0);
    if (!cdev->req)
        goto fail;
    cdev->req->buf = usbreq_buf;
    cdev->req->complete = composite_setup_complete;
    gadget->ep0->driver_data = cdev;

    cdev->bufsiz = USB_BUFSIZ;
    cdev->driver = composite;

    usb_gadget_set_selfpowered(gadget);
    usb_ep_autoconfig_reset(cdev->gadget);

    status = composite->bind(cdev);//bind=msg_driver.msg_bind
    if (status < 0)
        goto fail;

    memcpy(&cdev->desc, composite->dev,
            sizeof(struct usb_device_descriptor));
    cdev->desc.bMaxPacketSize0 = gadget->ep0->maxpacket;

    DBG("%s: ready\n", composite->name);
    return 0;

fail:
    __composite_unbind(gadget, false);
    return status;

}

static void composite_suspend(struct usb_gadget *gadget)
{
    struct usb_composite_dev    *cdev = get_gadget_data(gadget);
    struct usb_function        *f;

    debug("%s: suspend\n", __func__);
    if (cdev->config) {
        list_for_each_entry(f, &cdev->config->functions, list) {
            if (f->suspend)
                f->suspend(f);
        }
    }
    if (composite->suspend)
        composite->suspend(cdev);

    cdev->suspended = 1;

    return ;
}

static void composite_resume(struct usb_gadget *gadget)
{
    struct usb_composite_dev    *cdev = get_gadget_data(gadget);
    struct usb_function        *f;

    printf ("%s: resume\n", __func__);
    if (composite->resume)
        composite->resume(cdev);
    if (cdev->config) {
        list_for_each_entry(f, &cdev->config->functions, list) {
            if (f->resume)
                f->resume(f);
        }
    }

    cdev->suspended = 0;

    return ;
}

static struct usb_gadget_driver gadget_driver = {
    .function   =  "composite",
    .max_speed  =  USB_SPEED_HIGH,
    .bind       =  composite_bind,
    .unbind     =  composite_unbind,

    .setup      =  composite_setup,
    .reset      =  composite_disconnect,
    .disconnect =  composite_disconnect,

    .suspend    =  composite_suspend,
    .resume     =  composite_resume,
};

int usb_composite_register(struct usb_composite_driver *driver)
{
    int res;

    if (!driver || !driver->dev || !driver->bind || composite)
        return -EINVAL;

    if (!driver->name)
        driver->name = "composite";
    composite = driver;//current composite = msg_driver

    //res = usb_gadget_register_driver(&composite_driver);
    res = usb_gadget_probe_driver(&gadget_driver);
    if (res != 0)
        composite = NULL;

    return res;
}

void usb_composite_unregister(struct usb_composite_driver *driver)
{
    usb_gadget_unregister_driver(&gadget_driver);

    return ;
}

//  udc eva driver

#define IRQ_USB_SLAVE_WU  0x29
#define IRQ_USB_SLAVE_USB 0x2a
#define IRQ_USB_SLAVE_DMA 0x2b

#define bEN_WU_SET_EXTERAN_INT        (4)
#define bEN_USB_SET_EXTERAN_INT       (5)
#define bEN_DMA_SET_EXTERAN_INT       (6)
#define bEN_DUMP_SET_EXTERAN_INT      (7)

#define bEN_WU_SET_EXTERAN_CLR        (4)
#define bEN_USB_SET_EXTERAN_CLR       (5)
#define bEN_DMA_SET_EXTERAN_CLR       (6)
#define bEN_DUMP_SET_EXTERAN_CLR      (7)

#define bEN_WU_SET_EXTERAN_INTEN      (4)
#define bEN_USB_SET_EXTERAN_INTEN     (5)
#define bEN_DMA_SET_EXTERAN_INTEN     (6)
#define bEN_DUMP_SET_EXTERAN_INTEN    (7)

enum INT_TYPE_sel
{
    WU_INT_SEL   = (1<<4),
    USB_INT_SEL  = (1<<5),
    DMA_INT_SEL  = (1<<6),
    DUMP_INT_SEL = (1<<7)
};

#define REG_SET_BIT(reg, bit) do {               \
    (*(volatile unsigned int *)reg) |= 1<<(bit); \
}while(0)

#define USBSLAVE_SET_EXTERN_INT(reg, r)                            \
    do                                                             \
{                                                                  \
    if ((WU_INT_SEL & r) == WU_INT_SEL)                            \
    REG_SET_BIT(&(reg->INT_STATUS), bEN_WU_SET_EXTERAN_INT);       \
    else if ((USB_INT_SEL & r) == USB_INT_SEL)                     \
    REG_SET_BIT(&(reg->INT_STATUS), bEN_USB_SET_EXTERAN_INT);      \
    else if ((DMA_INT_SEL & r) == DMA_INT_SEL)                     \
    REG_SET_BIT(&(reg->INT_STATUS), bEN_DMA_SET_EXTERAN_INT);      \
    else if ((DUMP_INT_SEL & r) == DUMP_INT_SEL)                   \
    REG_SET_BIT(&(reg->INT_STATUS), bEN_DUMP_SET_EXTERAN_INT);     \
}while(0)


#define USBSLAVE_SET_EXTERN_INT_ENABLE(reg, r)                     \
    do                                                             \
{                                                                  \
    if ((WU_INT_SEL & r) == WU_INT_SEL)                            \
    REG_SET_BIT(&(reg->INT_ENABLE), bEN_WU_SET_EXTERAN_INTEN);     \
    else if ((USB_INT_SEL & r) == USB_INT_SEL)                     \
    REG_SET_BIT(&(reg->INT_ENABLE), bEN_USB_SET_EXTERAN_INTEN);    \
    else if ((DMA_INT_SEL & r) == DMA_INT_SEL)                     \
    REG_SET_BIT(&(reg->INT_ENABLE), bEN_DMA_SET_EXTERAN_INTEN);    \
    else if ((DUMP_INT_SEL & r) == DUMP_INT_SEL)                   \
    REG_SET_BIT(&(reg->INT_ENABLE), bEN_DUMP_SET_EXTERAN_INTEN);   \
}while(0)

#define USBSLAVE_SET_EXTERN_ISR_CLR(reg, r)                        \
    do                                                             \
{                                                                  \
    if ((WU_INT_SEL & r) == WU_INT_SEL)                            \
    REG_SET_BIT(&(reg->INT_CLEAR), bEN_WU_SET_EXTERAN_CLR);        \
    else if ((USB_INT_SEL & r) == USB_INT_SEL)                     \
    REG_SET_BIT(&(reg->INT_CLEAR), bEN_USB_SET_EXTERAN_CLR);       \
    else if ((DMA_INT_SEL & r) == DMA_INT_SEL)                     \
    REG_SET_BIT(&(reg->INT_CLEAR), bEN_DMA_SET_EXTERAN_CLR);       \
    else if ((DUMP_INT_SEL & r) == DUMP_INT_SEL)                   \
    REG_SET_BIT(&(reg->INT_CLEAR), bEN_DUMP_SET_EXTERAN_CLR);      \
}while(0)

#define USB_BASE_ADDR_OFFSET    0x0

#define EP0MAXPACK_REG_ADDR           (USB_BASE_ADDR_OFFSET + 0x0)
#define EP0CS_REG_ADDR                (USB_BASE_ADDR_OFFSET + 0x02)

#define UDC_EP_OUT1MAXPACK_REG_ADDR   (USB_BASE_ADDR_OFFSET + 0x08)
#define UDC_EP_OUT1CON_REG_ADDR       (USB_BASE_ADDR_OFFSET + 0x0A)
#define UDC_EP_OUT1CS_REG_ADDR        (USB_BASE_ADDR_OFFSET + 0x0B)

#define UDC_EP_IN1MAXPACK_REG_ADDR    (UDC_EP_OUT1MAXPACK_REG_ADDR + 0x4)
#define UDC_EP_IN1CON_REG_ADDR        (UDC_EP_OUT1CON_REG_ADDR + 0x4)
#define UDC_EP_IN1CS_REG_ADDR         (UDC_EP_OUT1CS_REG_ADDR + 0x4)

#define SETUP_PACKET_REG_ADDR         (USB_BASE_ADDR_OFFSET+0x180)

#define UDC_EP_INIRQ_REG_ADDR         (USB_BASE_ADDR_OFFSET + 0x188)
#define PING_USB_IRQ_REG_ADDR         (USB_BASE_ADDR_OFFSET + 0x18C)

#define UDC_INIEN_REG_ADDR            (USB_BASE_ADDR_OFFSET + 0x194)
#define UDC_OUTIEN_REG_ADDR           (UDC_INIEN_REG_ADDR + 2)

#define EP0DMAADDR_REG_ADDR           (USB_BASE_ADDR_OFFSET + 0x400)

#define UDC_EP_OUT1DMAADDR_REG_ADDR   (USB_BASE_ADDR_OFFSET + 0x420)
#define UDC_EP_IN1DMAADDR_REG_ADDR    (USB_BASE_ADDR_OFFSET + 0x430)

#define EP0FNADDR_REG_ADDR            (USB_BASE_ADDR_OFFSET + 0x200)
#define HC_IN1FNADDR_REG_ADDR         (USB_BASE_ADDR_OFFSET + 0x210)
#define HC_OUT1FNADDR_REG_ADDR        (USB_BASE_ADDR_OFFSET + 0x218)

#define UDC_USB_CTRL_REG_ADDR         (USB_BASE_ADDR_OFFSET + 0x1A0)


#define UDC_FNADDR_REG_ADDR           (USB_BASE_ADDR_OFFSET + 0x1A6)
#define UDC_FN_CTRL_REG_ADDR          (USB_BASE_ADDR_OFFSET + 0x1A4)

#define HC_PORT_CTRL_REG_ADDR         (USB_BASE_ADDR_OFFSET + 0x1A8)


#define TAAIDLBDIS                    (USB_BASE_ADDR_OFFSET + 0x1c1)
#define UDC_DMASTART                  (USB_BASE_ADDR_OFFSET + 0x1CC)
#define UDC_DMASTOP                   (USB_BASE_ADDR_OFFSET + 0x1D0)
#define UDC_DMARESET                  (USB_BASE_ADDR_OFFSET + 0x1D4)

#define USBCS_REG_ADDR                (USB_BASE_ADDR_OFFSET + 0x1A3)
#define HC_PORTCTRL_REG_ADDR          (USB_BASE_ADDR_OFFSET + 0x1AB)

#define UDC_DMA_OUTIRQ_REG_ADDR       (USB_BASE_ADDR_OFFSET + 0x192)
#define UDC_DMA_INIRQ_REG_ADDR        (USB_BASE_ADDR_OFFSET + 0x190)


#define UDC_PING_USB_IEN_REG_ADDR     (USB_BASE_ADDR_OFFSET + 0x198)


#define UDC_DMA_OUTIEN_REG_ADDR       (USB_BASE_ADDR_OFFSET + 0x19E)
#define UDC_DMA_INIEN_REG_ADDR        (USB_BASE_ADDR_OFFSET + 0x19C)

#define UDC_TOGSET_REG_ADDR           (USB_BASE_ADDR_OFFSET + 0x1D8)
#define UDC_TOGRESET_REG_ADDR         (USB_BASE_ADDR_OFFSET + 0x1DC)

//Endpoint FIFO size
#define UDC_OCM_FIFO_SIZE_REG_ADDR    (USB_BASE_ADDR_OFFSET + 0x300)
#define UDC_OCM_FIFO_ALLOC_REG_ADDR   (USB_BASE_ADDR_OFFSET + 0x304)

#define UDC_OTG_REG_ADDR              (USB_BASE_ADDR_OFFSET + 0x1BC)

/* USB device specific global configuration constants.*/
#define EVAUSB_MAX_ENDPOINTS         6    /* Maximum End Points */
#define EVAUSB_EP_NUMBER_ZERO        0    /* End point Zero */

/* Phase States */
#define SETUP_PHASE                  0x0000  /* Setup Phase */
#define DATA_PHASE                   0x0001  /* Data Phase */
#define STATUS_PHASE                 0x0002  /* Status Phase */

#define EP0_MAX_PACKET               64 /* Endpoint 0 maximum packet length */
#define STATUSBUFF_SIZE               2  /* Buffer size for GET_STATUS command */
#define EPNAME_SIZE                   4  /* Buffer size for endpoint name */

/* Endpoint Configuration Status Register */
#define EVAUSB_EP0_CFG_HSNAK_MASK                  0x20000 /* Endpoint0 HSNAK bit */
#define EVAUSB_EP0_CFG_STALL_MASK                  0x10000 /* Endpoint0 Stall bit */

#define EVAUSB_EP_CFG_VALID_MASK                  0x800000 /* Endpoint Valid bit */
#define EVAUSB_EP_CFG_STALL_MASK                  0x400000 /* Endpoint Stall bit */

#define EVAUSB_CONTROL_USB_READY_MASK           0x40000000
#define EVAUSB_CONTROL_USB_RMTWAKE_MASK         0x20000000

/* Interrupt register related masks.*/
#define EVAUSB_STATUS_PING_ALL_OUTEPIEIRQ_MASK  0xFFFF0000 /* for test use */
#define EVAUSB_STATUS_OVERFLOWIEIRQ_MASK        0x00000040
#define EVAUSB_STATUS_HSPEEDIEIRQ_MASK          0x00000020
#define EVAUSB_STATUS_URESIEIRQ_MASK            0x00000010
#define EVAUSB_STATUS_SUSPIEIRQ_MASK            0x00000008
#define EVAUSB_STATUS_SUTOKEIEIRQ_MASK          0x00000004
#define EVAUSB_STATUS_SOFIEIRQ_MASK             0x00000002
#define EVAUSB_STATUS_SUDAVIEIRQ_MASK           0x00000001

#define EVAUSB_STATUS_EP1_OUT_IRQ_MASK          0x00020000
#define EVAUSB_STATUS_EP1_IN_IRQ_MASK           0x00000002
#define EVAUSB_STATUS_EP0_OUT_IRQ_MASK          0x00010000
#define EVAUSB_STATUS_EP0_IN_IRQ_MASK           0x00000001

//register dmactrl
#define REG_DMACTRL_SEND_0_LEN                     0x80000      /*send short packet*/
#define REG_DMACTRL_START_DMA                      0x40000      /*start dma */
#define REG_DMACTRL_STOP_DMA                       0x20000      /*stop dma*/
#define REG_DMACTRL_BIG_ENDIAN                     0x10000      /*Read/write - Endianess of AHB master interface*/
#define REG_DMACTRL_HSIZE_32                     0x8000000      /*Read/write - AHB transfer size*/
#define REG_DMACTRL_HPROT_0                     0x10000000


/* container_of helper macros */
#define to_udc(g)          container_of((g), struct evausb_udc, gadget)
#define to_evausb_ep(ep)   container_of((ep), struct evausb_ep, ep_usb)
#define to_evausb_req(req) container_of((req), struct evausb_req, usb_req)

#define bEN_WU_SET_EXTERAN_INT        (4)
#define bEN_USB_SET_EXTERAN_INT       (5)
#define bEN_DMA_SET_EXTERAN_INT       (6)
#define bEN_DUMP_SET_EXTERAN_INT      (7)

/**
 * struct xusb_req - Xilinx USB device request structure
 * @usb_req: Linux usb request structure
 * @queue: usb device request queue
 * @ep: pointer to xusb_endpoint structure
 */
struct evausb_req {
    struct usb_request usb_req;
    struct list_head   queue;
    struct evausb_ep  *ep;
    void              *saved_req_buf;
    u32                real_dma_length;
    bool               used;
};

/*
 *    hc_ep - describe one in or out endpoint in host mode
 */
struct eva_ep_dma_struct
{
    u32   dmaaddr;
    u32   dmatsize;
    u32   dmaringptr;
    u32   dmactrl;
};

/**
 * struct evausb_ep - USB end point structure.
 * @ep_usb: usb endpoint instance
 * @queue: endpoint message queue
 * @udc: xilinx usb peripheral driver instance pointer
 * @desc: pointer to the usb endpoint descriptor
 * @rambase: the endpoint buffer address
 * @offset: the endpoint register offset value
 * @name: name of the endpoint
 * @epnumber: endpoint number
 * @maxpacket: maximum packet size the endpoint can store
 * @buffer0count: the size of the packet recieved in the first buffer
 * @buffer1count: the size of the packet received in the second buffer
 * @curbufnum: current buffer of endpoint that will be processed next
 * @buffer0ready: the busy state of first buffer
 * @buffer1ready: the busy state of second buffer
 * @is_in: endpoint direction (IN or OUT)
 * @is_iso: endpoint type(isochronous or non isochronous)
 */
struct evausb_ep {
    struct usb_ep                         ep_usb;
    struct list_head                      queue;
    struct evausb_udc                    *udc;
    const struct usb_endpoint_descriptor *desc;
    u32                                   rambase;
    u32                                   offset;
    char                                  name[8];
    u16                                   epnumber;
    u16                                   maxpacket;
    volatile struct eva_ep_dma_struct    *dma;
    bool                                  is_in;
    bool                                  is_iso;
    bool                                  is_int;
};

struct general_dma
{
    u16 dmastart_in;
    u16 dmastart_out;

    u16 dmastop_in;
    u16 dmastop_out;

    u16 dmasrst_in;
    u16 dmasrst_out;

    u16 togsetq_in;
    u16 togsetq_out;

    u16 togrst_in;
    u16 togrst_out;

};

/**
 * struct evausb_udc -  USB peripheral driver structure
 * @gadget: USB gadget driver instance
 * @ep: an array of endpoint structures
 * @driver: pointer to the usb gadget driver instance
 * @setup: usb_ctrlrequest structure for control requests
 * @req: pointer to dummy request for get status command
 * @dev: pointer to device structure in gadget
 * @usb_state: device in suspended state or not
 * @remote_wkp: remote wakeup enabled by host
 * @setupseqtx: tx status
 * @setupseqrx: rx status
 * @addr: the usb device base address
 * @lock: instance of spinlock
 * @dma_enabled: flag indicating whether the dma is included in the system
 * @read_fn: function pointer to read device registers
 * @write_fn: function pointer to write to device registers
 */
struct evausb_udc {
    struct usb_gadget             gadget;

    struct evausb_ep              ep0;
    struct evausb_ep              ep1_in;
    struct evausb_ep              ep1_out;
    struct evausb_ep              ep2_in;
    struct evausb_ep              ep2_out;
    struct evausb_ep              ep3_in;
    struct evausb_ep              ep3_out;
    struct evausb_ep              ep4_in;
    struct evausb_ep              ep4_out;

    struct usb_gadget_driver     *driver;
    struct usb_ctrlrequest        setup;
    struct evausb_req            *req;
    struct device                *dev;
    u32                           usb_state;
    u32                           remote_wkp;
    u32                           setupseqtx;
    u32                           setupseqrx;
    void __iomem                 *addr;
    bool                          dma_enabled;
    volatile u8                  *fnaddr;
    volatile u8                  *usbcs;
    volatile struct general_dma  *dma;

    unsigned int                (*read_fn) (u32);
    void                        (*write_fn)(void __iomem *, u32, u32);
};

static const char driver_name[] = "gxeva-udc";
static struct usb_ep *g_ep1in = NULL;
static struct usb_ep *g_ep2in = NULL;

/* Control endpoint configuration.*/
static const struct usb_endpoint_descriptor config_bulk_out_desc = {
    .bLength          =  USB_DT_ENDPOINT_SIZE,
    .bDescriptorType  =  USB_DT_ENDPOINT,
    .bEndpointAddress =  USB_DIR_OUT,
    .bmAttributes     =  USB_ENDPOINT_XFER_BULK,
    .wMaxPacketSize   =  cpu_to_le16(EP0_MAX_PACKET),
};

/**
 * evaudc_write32 - little endian write to device registers
 * @addr: base addr of device registers
 * @offset: register offset
 * @val: data to be written
 */
static void evaudc_write32(void __iomem *addr, u32 offset, u32 val)
{
    writel(val,(u32)addr + offset);

    return ;
}

/**
 * evaudc_read32 - little endian read from device registers
 * @addr: addr of device register
 * Return: value at addr
 */
static unsigned int evaudc_read32(u32 addr)
{
    return readl(addr);
}

/**
 * evaudc_wrstatus - Sets up the usb device status stages.
 * @udc: pointer to the usb device controller structure.
 */
static void evaudc_wrstatus(struct evausb_udc *udc)
{
    /* ep0 only */
    u32 epcfgreg;
    struct evausb_ep *ep0 = &udc->ep0;

    epcfgreg = udc->read_fn((u32)udc->addr + ep0->offset);
    epcfgreg |= EVAUSB_EP0_CFG_HSNAK_MASK;
    udc->write_fn(udc->addr, ep0->offset, epcfgreg);

    return ;
}

/**
 * evaudc_epconfig - Configures the given endpoint.
 * @ep: pointer to the usb device endpoint structure.
 * @udc: pointer to the usb peripheral controller structure.
 *
 * This function configures a specific endpoint with the given configuration
 * data.
 */
static void evaudc_epconfig(struct evausb_ep *ep, struct evausb_udc *udc)
{
    u32 epcfgreg;
    epcfgreg = 0;

    if (ep->epnumber == 0)
        udc->write_fn(udc->addr, ep->offset, ep->ep_usb.maxpacket);
    else {
        DBG("evaudc_epconfig %s is_iso %d is_int %d\n",ep->name,ep->is_iso,ep->is_int);

        if (ep->is_iso)
            epcfgreg = (0x5<<18)|(ep->ep_usb.maxpacket);
        else if (ep->is_int)
            epcfgreg = (0x3<<18)|(ep->ep_usb.maxpacket);
        else
            epcfgreg = (0x2<<18)|(ep->ep_usb.maxpacket);
        udc->write_fn(udc->addr, ep->offset, epcfgreg);
    }

    return ;
}

#if 0
static void evaudc_handle_unaligned_buf_complete(struct evausb_udc *udc,
        struct evausb_ep *ep, struct evausb_req *req)
{
    /* If dma is not being used or buffer was aligned */
    if (!udc->dma_enabled|| !req->saved_req_buf)
        return;

    /* Copy data from bounce buffer on successful out transfer */
    if (!ep->is_in && !req->usb_req.status)
    {
        memcpy(req->saved_req_buf, req->usb_req.buf,
                req->usb_req.actual);
    }

    /* Free bounce buffer */
    kfree(req->usb_req.buf);

    req->usb_req.buf = req->saved_req_buf;
    req->saved_req_buf = NULL;
}

static int evaudc_handle_unaligned_buf_start(struct evausb_udc *udc,
        struct evausb_ep *ep, struct evausb_req *req)
{
    void *req_buf = req->usb_req.buf;

    /* If dma is not being used or buffer is aligned */
    if (!udc->dma_enabled || !((long)req_buf & 3))
        return 0;

    //WARN_ON(req->saved_req_buf);

    req->usb_req.buf = kmalloc(req->usb_req.length, GFP_ATOMIC);
    if (!req->usb_req.buf) {
        req->usb_req.buf = req_buf;
        printk("%s: unable to allocate memory for bounce buffer\n",__FUNCTION__);
        return -ENOMEM;
    }

    /* Save actual buffer */
    req->saved_req_buf = req_buf;

    if (ep->is_in)
    {
        memcpy(req->usb_req.buf, req_buf, req->usb_req.length);
    }
    return 0;
}
#endif

/**
 * evaudc_start_dma - Starts DMA transfer.
 * @ep: pointer to the usb device endpoint structure.
 * @src: DMA source address.
 * @dst: DMA destination address.
 * @length: number of bytes to transfer.
 *
 * Return: 0 on success, error code on failure
 *
 * This function starts DMA transfer by writing to DMA source,
 * destination and lenth registers.
 */
static int evaudc_start_dma(struct evausb_ep *ep, dma_addr_t addr,u32 length,bool zero)
{
    struct evausb_udc *udc = ep->udc;
    unsigned int ctrl_value;
    int rc = 0;

    /*
     * Set the addresses in the DMA source and
     * destination registers and then set the length
     * into the DMA length register.
     */
    if (ep->is_in)
        udc->dma->dmasrst_in |= 1<<ep->epnumber;
    else
        udc->dma->dmasrst_out |= 1<<ep->epnumber;

    ep->dma->dmaaddr = addr;

    DBG ("%s, ep name : %s, len : %d\n", __func__, ep->name, length);

    if (ep->is_in) {
        ctrl_value = REG_DMACTRL_HSIZE_32;
        /*If send0len=1, the device sends the short packet.*/
        if ( length < ep->ep_usb.maxpacket || zero)
            ctrl_value |= REG_DMACTRL_SEND_0_LEN;

        ep->dma->dmatsize = length;
        ep->dma->dmactrl = ctrl_value;
        udc->dma->dmastart_in |= (0x01<<ep->epnumber);

        DBG ("%s, %s start : %x, stop : %x, rst : %x\n", __func__, ep->name,
                *(volatile unsigned int *)&udc->dma->dmastart_in,
                *(volatile unsigned int *)&udc->dma->dmastop_in,
                *(volatile unsigned int *)&udc->dma->dmasrst_in);

        return -EINPROGRESS;
    } else {
        ep->dma->dmatsize = ep->ep_usb.maxpacket;
        ep->dma->dmactrl = REG_DMACTRL_HSIZE_32;
        udc->dma->dmastart_out |= (0x01<<ep->epnumber);

        return -EINPROGRESS;
    }

    return rc;
}

/**
 * evaudc_dma_send - Sends IN data using DMA.
 * @ep: pointer to the usb device endpoint structure.
 * @req: pointer to the usb request structure.
 * @buffer: pointer to data to be sent.
 * @length: number of bytes to send.
 *
 * Return: 0 on success, -EAGAIN if no buffer is free and error
 *       code on failure.
 *
 * This function sends data using DMA.
 */
static int evaudc_dma_send(struct evausb_ep *ep, struct evausb_req *req,
        u8 *buffer, u32 length)
{
    dma_addr_t src;
    int zero;

    src = req->usb_req.dma + req->usb_req.actual;
    if (!req->usb_req.length)
        return -EAGAIN;

    if ((ep->epnumber == 0) &&
        (length == ep->ep_usb.maxpacket) &&
        (req->usb_req.length - req->usb_req.actual == length)) {
        zero = 1;
        DBG ("ep : %s zero : %d\n", ep->name, zero);
    } else
        zero = 0;

    return evaudc_start_dma(ep, src, length,(bool)zero);
}

/**
 * evaudc_dma_receive - Receives OUT data using DMA.
 * @ep: pointer to the usb device endpoint structure.
 * @req: pointer to the usb request structure.
 * @buffer: pointer to storage buffer of received data.
 * @length: number of bytes to receive.
 *
 * Return: 0 on success, -EAGAIN if no buffer is free and error
 *       code on failure.
 *
 * This function receives data using DMA.
 */
static int evaudc_dma_receive(struct evausb_ep *ep, struct evausb_req *req,
        u8 *buffer, u32 length)
{
    dma_addr_t dst;

    dst = req->usb_req.dma + req->usb_req.actual;
    //dst = req->usb_req.dma;
    if (req->usb_req.length) {
        /*dma_sync_single_for_device(udc->dev, dst,
          length, DMA_FROM_DEVICE);*/
        //clear cache for dst
    } else {
        /* None of ping pong buffers are ready currently .*/
        return -EAGAIN;
    }

    return evaudc_start_dma(ep, dst, length, 0);
}

/**
 * evaudc_eptxrx - Transmits or receives data to or from an endpoint.
 * @ep: pointer to the usb endpoint configuration structure.
 * @req: pointer to the usb request structure.
 * @bufferptr: pointer to buffer containing the data to be sent.
 * @bufferlen: The number of data bytes to be sent.
 *
 * Return: 0 on success, -EAGAIN if no buffer is free.
 *
 * This function copies the transmit/receive data to/from the end point buffer
 * and enables the buffer for transmission/reception.
 */
static int evaudc_eptxrx(struct evausb_ep *ep, struct evausb_req *req,
        u8 *bufferptr, u32 bufferlen)
{
    int rc = 0;
    struct evausb_udc *udc = ep->udc;

    if (udc->dma_enabled) {
        if (ep->is_in)
            rc = evaudc_dma_send(ep, req, bufferptr, bufferlen);
        else
            rc = evaudc_dma_receive(ep, req, bufferptr, bufferlen);
        return rc;
    }

    return rc;
}

/**
 * evaudc_done - Exeutes the endpoint data transfer completion tasks.
 * @ep: pointer to the usb device endpoint structure.
 * @req: pointer to the usb request structure.
 * @status: Status of the data transfer.
 *
 * Deletes the message from the queue and updates data transfer completion
 * status.
 */
static void evaudc_done(struct evausb_ep *ep, struct evausb_req *req, int status)
{
    list_del_init(&req->queue);

    if (req->usb_req.status == -EINPROGRESS)
        req->usb_req.status = status;

    /*
        else
            status = req->usb_req.status;

       if (status && status != -ESHUTDOWN)
       DBG ("%s done %p, status %d req->usb_req.complete %p\n",
       ep->ep_usb.name, req, status,req->usb_req.complete);
       */
    /* unmap request if DMA is present*/
    /*
       if (udc->dma_enabled && ep->epnumber && req->usb_req.length)
       usb_gadget_unmap_request(&udc->gadget, &req->usb_req,
       ep->is_in);
       */

    if (req->usb_req.complete)
        req->usb_req.complete(&ep->ep_usb, &req->usb_req);

    return ;
}

/*
 * lightweight evaudc_done
 */
static void evaudc_done_lw(struct evausb_ep *ep, struct evausb_req *req, int status)
{
    if (req->usb_req.status == -EINPROGRESS)
        req->usb_req.status = status;

    /*
        else
            status = req->usb_req.status;

       if (status && status != -ESHUTDOWN)
       DBG ("%s done %p, status %d req->usb_req.complete %p\n",
       ep->ep_usb.name, req, status,req->usb_req.complete);
       */
    /* unmap request if DMA is present*/
    /*
       if (udc->dma_enabled && ep->epnumber && req->usb_req.length)
       usb_gadget_unmap_request(&udc->gadget, &req->usb_req,
       ep->is_in);
       */

    if (req->usb_req.complete)
        req->usb_req.complete(&ep->ep_usb, &req->usb_req);

    return ;
}


/**
 * evaudc_read_fifo - Reads the data from the given endpoint buffer.
 * @ep: pointer to the usb device endpoint structure.
 * @req: pointer to the usb request structure.
 *
 * Return: 0 if request is completed and -EAGAIN if not completed.
 *
 * Pulls OUT packet data from the endpoint buffer.
 */
static int evaudc_read_fifo(struct evausb_ep *ep, struct evausb_req *req)
{
    u8 *buf;
    u32 count, bufferspace;
    int ret;

    buf = (u8 *)((u32)req->usb_req.buf + req->usb_req.actual);
    //evaudc_handle_unaligned_buf_start(udc,ep,req);

    bufferspace = req->usb_req.length - req->usb_req.actual;
    count = bufferspace;
    if (count > ep->ep_usb.maxpacket)
        count = ep->ep_usb.maxpacket;

    if (unlikely(!bufferspace)) {
        /*
         * This happens when the driver's buffer
         * is smaller than what the host sent.
         * discard the extra data.
         */
        if (req->usb_req.status != -EOVERFLOW)
            ERR ("%s overflow %d\n", ep->ep_usb.name, count);

        req->usb_req.status = -EOVERFLOW;
        evaudc_done(ep, req, -EOVERFLOW);
        return 0;
    }

    ret = evaudc_eptxrx(ep, req, buf, count);

    return ret;
}

/**
 * evaudc_write_fifo - Writes data into the given endpoint buffer.
 * @ep: pointer to the usb device endpoint structure.
 * @req: pointer to the usb request structure.
 *
 * Return: 0 if request is completed and -EAGAIN if not completed.
 *
 * Loads endpoint buffer for an IN packet.
 */
static int evaudc_write_fifo(struct evausb_ep *ep, struct evausb_req *req)
{
    u32 max;
    u32 length;
    int ret;
    u8 *buf;

    max = le16_to_cpu(ep->desc->wMaxPacketSize);
    buf = (u8 *)((u32)req->usb_req.buf + req->usb_req.actual);
    //evaudc_handle_unaligned_buf_start(udc,ep,req);
    length = req->usb_req.length - req->usb_req.actual;

    if (ep->is_in && ep->is_iso)
        ;
    else
        length = min(length, max);

    req->real_dma_length = length;
    ret = evaudc_eptxrx(ep, req, buf, length);

    return ret;
}

/**
 * evaudc_nuke - Cleans up the data transfer message list.
 * @ep: pointer to the usb device endpoint structure.
 * @status: Status of the data transfer.
 */
static void evaudc_nuke(struct evausb_ep *ep, int status)
{
    struct evausb_req *req;

    while (!list_empty(&ep->queue)) {
        req = list_first_entry(&ep->queue, struct evausb_req, queue);
        evaudc_done(ep, req, status);
    }

    return ;
}
#if 0
/**
 * evaudc_ep_set_halt - Stalls/unstalls the given endpoint.
 * @_ep: pointer to the usb device endpoint structure.
 * @value: value to indicate stall/unstall.
 *
 * Return: 0 for success and error value on failure
 */
static int evaudc_ep_set_halt(struct usb_ep *_ep, int value)
{
    struct evausb_ep *ep = to_evausb_ep(_ep);
    struct evausb_udc *udc;
    u32 epcfgreg;

    if (!_ep || (!ep->desc && ep->epnumber)) {
        return -EINVAL;
    }
    udc = ep->udc;

    if (ep->is_in && (!list_empty(&ep->queue)) && value) {
        DBG ("requests pending can't halt\n");
        return -EAGAIN;
    }

    if (value) {
        /* Stall the device.*/
        epcfgreg = udc->read_fn((u32)udc->addr + ep->offset);
        epcfgreg |= EVAUSB_EP_CFG_STALL_MASK;
        udc->write_fn(udc->addr, ep->offset, epcfgreg);
    } else {
        /* Unstall the device.*/
        epcfgreg = udc->read_fn((u32)udc->addr + ep->offset);
        epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
        udc->write_fn(udc->addr, ep->offset, epcfgreg);
        if (ep->epnumber) {
            /* Reset the toggle bit.*/
            epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
            if (ep->is_in)
                epcfgreg |= 1<<(ep->epnumber);
            else
                epcfgreg |= 1<<(ep->epnumber+16);
            udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);

        }
    }

    return 0;
}
#endif
/**
 * evaudc_ep_enable - Enables the given endpoint.
 * @ep: pointer to the xusb endpoint structure.
 * @desc: pointer to usb endpoint descriptor.
 *
 * Return: 0 for success and error value on failure
 */
static int __evaudc_ep_enable(struct evausb_ep *ep,
        const struct usb_endpoint_descriptor *desc)
{
    struct evausb_udc *udc = ep->udc;
    u32 tmp;
    u32 epcfg;
    u32 ier;
    u32 dma_ier;
    u16 maxpacket;

    ep->is_in = ((desc->bEndpointAddress & USB_DIR_IN) != 0);
    /* Bit 3...0:endpoint number */
    ep->epnumber = (desc->bEndpointAddress & 0x0f);
    ep->desc = desc;
    ep->ep_usb.desc = desc;
    tmp = desc->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK;
    ep->ep_usb.maxpacket = maxpacket = le16_to_cpu(desc->wMaxPacketSize);

    switch (tmp) {
        case USB_ENDPOINT_XFER_CONTROL:
            DBG("only one control endpoint\n");
            /* NON- ISO */
            ep->is_iso = 0;
            return -EINVAL;
        case USB_ENDPOINT_XFER_INT:
            /* NON- ISO */
            ep->is_iso = 0;
            ep->is_int = true;
            break;
        case USB_ENDPOINT_XFER_BULK:
            /* NON- ISO */
            ep->is_iso = 0;
            break;
        case USB_ENDPOINT_XFER_ISOC:
            /* ISO */
            ep->is_iso = 1;
            break;
    }

    evaudc_epconfig(ep, udc);

    DBG("Enable Endpoint %s %d max pkt is %d\n", ep->name,ep->epnumber, maxpacket);

    /* Enable the End point.*/
    if (ep->epnumber) {
        epcfg = udc->read_fn((u32)udc->addr + ep->offset);
        epcfg |= EVAUSB_EP_CFG_VALID_MASK;
        udc->write_fn(udc->addr, ep->offset, epcfg);
    }

    /* Enable the End point int and dma int.  UDC_INIEN_REG_ADDR*/
    ier = udc->read_fn((u32)udc->addr + UDC_INIEN_REG_ADDR);
    if (!ep->epnumber) {
        ier |= ((1<<ep->epnumber)|(1<<(ep->epnumber + 16)));
    } else {
        if (ep->is_in)
        {
            ier |= (1<<ep->epnumber);
        }
        else
        {
            ier |= ((1<<(ep->epnumber + 16)));
        }
    }
    udc->write_fn(udc->addr, UDC_INIEN_REG_ADDR, ier);
    ier = udc->read_fn((u32)udc->addr + UDC_INIEN_REG_ADDR);
    if (udc->dma_enabled) {
        dma_ier = udc->read_fn((u32)udc->addr + UDC_DMA_INIEN_REG_ADDR);
        if (!ep->epnumber)
            dma_ier |= 0x10001;
        else {
            if (ep->is_in)
                dma_ier |= (1<<ep->epnumber);
            else
                dma_ier |= ((1<<(ep->epnumber + 16)));
        }
        udc->write_fn(udc->addr, UDC_DMA_INIEN_REG_ADDR, dma_ier);
        ier = udc->read_fn((u32)udc->addr + UDC_DMA_INIEN_REG_ADDR);
    }

    return 0;
}

/**
 * evaudc_ep_enable - Enables the given endpoint.
 * @_ep: pointer to the usb endpoint structure.
 * @desc: pointer to usb endpoint descriptor.
 *
 * Return: 0 for success and error value on failure
 */
static int evaudc_ep_enable(struct usb_ep *_ep,
        const struct usb_endpoint_descriptor *desc)
{
    struct evausb_ep *ep;
    struct evausb_udc *udc;
    int ret;

    if (!_ep || !desc || desc->bDescriptorType != USB_DT_ENDPOINT) {
        printf ("%s, Invalid parameter\n", __func__);
        return -EINVAL;
    }

    ep = to_evausb_ep(_ep);
    udc = ep->udc;

    if (!udc->driver || udc->gadget.speed == USB_SPEED_UNKNOWN) {
        printf ("bogus device state\n");
        return -ESHUTDOWN;
    }

    ret = __evaudc_ep_enable(ep, desc);

    return ret;
}

/**
 * evaudc_ep_disable - Disables the given endpoint.
 * @_ep: pointer to the usb endpoint structure.
 *
 * Return: 0 for success and error value on failure
 */
static int evaudc_ep_disable(struct usb_ep *_ep)
{
    struct evausb_ep *ep;
    u32 epcfg;
    struct evausb_udc *udc;
    u32 ier;
    u32 dma_ier;

    if (!_ep) {
        printf ("%s, Invalid parameter\n", __func__);
        return -EINVAL;
    }

    ep = to_evausb_ep(_ep);
    udc = ep->udc;

    evaudc_nuke(ep, -ESHUTDOWN);

    /* Restore the endpoint's pristine config */
    ep->desc = NULL;
    ep->ep_usb.desc = NULL;

    DBG ("USB Ep %d disable\n ", ep->epnumber);
    /* Disable the endpoint.*/
    epcfg = udc->read_fn((u32)udc->addr + ep->offset);
    epcfg &= ~EVAUSB_EP_CFG_VALID_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfg);

    /* disable the End point int and dma int.  UDC_INIEN_REG_ADDR*/
    ier = udc->read_fn((u32)udc->addr + UDC_INIEN_REG_ADDR);
    if (!ep->epnumber) {
        ier &= ~((1<<ep->epnumber)|(1<<(ep->epnumber + 16)));
    } else {
        if (ep->is_in)
            ier &= ~(1<<ep->epnumber);
        else
            ier &= ~((1<<(ep->epnumber + 16)));
    }
    udc->write_fn(udc->addr, UDC_INIEN_REG_ADDR, ier);

    if (udc->dma_enabled) {
        dma_ier = udc->read_fn((u32)udc->addr + UDC_DMA_INIEN_REG_ADDR);
        if (!ep->epnumber) {
            dma_ier &= ~((1<<ep->epnumber)|(1<<(ep->epnumber + 16)));
        } else {
            if (ep->is_in)
                dma_ier &= ~(1<<ep->epnumber);
            else
                dma_ier &= ~((1<<(ep->epnumber + 16)));
        }
        udc->write_fn(udc->addr, UDC_DMA_INIEN_REG_ADDR, dma_ier);
    }

    if (ep->is_in)
        udc->dma->dmasrst_in |= 1<<ep->epnumber;
    else
        udc->dma->dmasrst_out |= 1<<ep->epnumber;

    return 0;
}

/**
 * evaudc_ep_alloc_request - Initializes the request queue.
 * @_ep: pointer to the usb endpoint structure.
 * @gfp_flags: Flags related to the request call.
 *
 * Return: pointer to request structure on success and a NULL on failure.
 */
#define MAX_USB_REQ_NUM 32
static struct evausb_req g_eva_data_req[MAX_USB_REQ_NUM];

static struct usb_request *evaudc_ep_alloc_request(struct usb_ep *_ep)
{
    struct evausb_ep *ep = to_evausb_ep(_ep);
    struct evausb_req *req = NULL;
    u32 i;
    bool founded = false;

    for (i = 0; i < MAX_USB_REQ_NUM; i++) {
        if (!g_eva_data_req[i].used) {
            req = &g_eva_data_req[i];
            req->used = true;
            founded = true;
            break;
        }
    }

    if (founded) {
        memset(&req->usb_req, 0, sizeof(req->usb_req));
        req->ep = ep;
        INIT_LIST_HEAD(&req->queue);

        return &req->usb_req;
    } else
        return NULL;
}

/**
 * evaudc_free_request - Releases the request from queue.
 * @_ep: pointer to the usb device endpoint structure.
 * @_req: pointer to the usb request structure.
 */
static void evaudc_free_request(struct usb_ep *_ep, struct usb_request *_req)
{
    struct evausb_req *req = to_evausb_req(_req);
    req->used = false;
    req->ep = NULL;

    return ;
}

/**
 * evaudc_ep0_queue - Adds the request to endpoint 0 queue.
 * @ep0: pointer to the xusb endpoint 0 structure.
 * @req: pointer to the xusb request structure.
 *
 * Return: 0 for success and error value on failure
 */
static int __evaudc_ep0_queue(struct evausb_ep *ep0, struct evausb_req *req)
{
    struct evausb_udc *udc = ep0->udc;
    bool first;

    if (!udc->driver || udc->gadget.speed == USB_SPEED_UNKNOWN) {
        DBG ("%s, bogus device state\n", __func__);
        return -EINVAL;
    }
    if (!list_empty(&ep0->queue)) {
        DBG("%s:ep0 busy\n", __func__);
        return -EBUSY;
    }

    req->usb_req.status = -EINPROGRESS;
    req->usb_req.actual = 0;

    gx_disable_irq();

    first = list_empty(&ep0->queue);

    list_add_tail(&req->queue, &ep0->queue);
    //evaudc_handle_unaligned_buf_start(udc,ep0,req);

    if (udc->setup.bRequestType & USB_DIR_IN) {
        if (udc->dma_enabled) {
            req->usb_req.dma = (unsigned int)req->usb_req.buf - 0x40000000;
        }

    } else {
        if (udc->setup.wLength) {
            if (udc->dma_enabled) {
                req->usb_req.dma = (unsigned int)req->usb_req.buf - 0x40000000;
            }

        } else {
            evaudc_wrstatus(udc);
            list_del_init(&req->queue);
        }
    }

    gx_enable_irq();

    if (first && udc->setup.wLength) {
        if (udc->setup.bRequestType & USB_DIR_IN) {
            ep0->is_in = true;
            evaudc_write_fifo(ep0,req);
        } else {
            ep0->is_in = false;
            evaudc_read_fifo(ep0,req);
        }
    }

    return 0;
}

/**
 * evaudc_ep0_queue - Adds the request to endpoint 0 queue.
 * @_ep: pointer to the usb endpoint 0 structure.
 * @_req: pointer to the usb request structure.
 * @gfp_flags: Flags related to the request call.
 *
 * Return: 0 for success and error value on failure
 */
static int evaudc_ep0_queue(struct usb_ep *_ep, struct usb_request *_req)
{
    struct evausb_req *req    = to_evausb_req(_req);
    struct evausb_ep    *ep0    = to_evausb_ep(_ep);
    int ret;

    ret = __evaudc_ep0_queue(ep0, req);

    return ret;
}

/**
 * evaudc_ep_queue - Adds the request to endpoint queue.
 * @_ep: pointer to the usb endpoint structure.
 * @_req: pointer to the usb request structure.
 * @gfp_flags: Flags related to the request call.
 *
 * Return: 0 for success and error value on failure
 */
static int evaudc_ep_queue(struct usb_ep *_ep, struct usb_request *_req)
{
    struct evausb_req *req = to_evausb_req(_req);
    struct evausb_ep    *ep  = to_evausb_ep(_ep);
    struct evausb_udc *udc = ep->udc;
    int is_empty;
    struct evausb_req *first_req;

    if (!ep->desc) {
        DBG ("%s:queing request to disabled %s\n",
                __func__, ep->name);
        return -ESHUTDOWN;
    }

    if (!udc->driver || udc->gadget.speed == USB_SPEED_UNKNOWN) {
        DBG ("%s, bogus device state\n", __func__);
        return -EINVAL;
    }

    _req->status = -EINPROGRESS;
    _req->actual = 0;

    if (udc->dma_enabled) {
        req->usb_req.dma = (u32)req->usb_req.buf - 0x40000000;
    }

    /*
     * iso 传输时，调用 ep_queue 的上下文是在中断中，现在没有类似 in_interrupt 的接口
     */
    if (!ep->is_iso)
        gx_disable_irq();

    is_empty = list_empty(&ep->queue);
    if (!is_empty) {
        first_req = list_first_entry(&ep->queue, struct evausb_req, queue);
        if (first_req == req)
            return 0;
    }
    list_add_tail(&req->queue, &ep->queue);

    /*
     * iso 传输时，调用 ep_queue 的上下文是在中断中，现在没有类似 in_interrupt 的接口
     */
    if (!ep->is_iso)
        gx_enable_irq();

    if (is_empty) {
        if (ep->is_in) {
            if (ep->is_iso) {
                // send req in irq
                ;
            } else
                evaudc_write_fifo(ep, req);
        } else {
            evaudc_read_fifo(ep, req);
        }

    }


    return 0;
}

/**
 * evaudc_ep_dequeue - Removes the request from the queue.
 * @_ep: pointer to the usb device endpoint structure.
 * @_req: pointer to the usb request structure.
 *
 * Return: 0 for success and error value on failure
 */
static int evaudc_ep_dequeue(struct usb_ep *_ep, struct usb_request *_req)
{
    struct evausb_ep *ep    = to_evausb_ep(_ep);
    struct evausb_req *req    = to_evausb_req(_req);


    /* Make sure it's actually queued on this endpoint */
    list_for_each_entry(req, &ep->queue, queue) {
        if (&req->usb_req == _req)
            break;
    }
    if (&req->usb_req != _req) {
        return -EINVAL;
    }
    evaudc_done(ep, req, -ECONNRESET);

    return 0;
}

/**
 * evaudc_ep0_enable - Enables the given endpoint.
 * @ep: pointer to the usb endpoint structure.
 * @desc: pointer to usb endpoint descriptor.
 *
 * Return: error always.
 *
 * endpoint 0 enable should not be called by gadget layer.
 */
static int evaudc_ep0_enable(struct usb_ep *ep,
        const struct usb_endpoint_descriptor *desc)
{
    return -EINVAL;
}

/**
 * evaudc_ep0_disable - Disables the given endpoint.
 * @ep: pointer to the usb endpoint structure.
 *
 * Return: error always.
 *
 * endpoint 0 disable should not be called by gadget layer.
 */
static int evaudc_ep0_disable(struct usb_ep *ep)
{
    return -EINVAL;
}

static const struct usb_ep_ops evausb_ep0_ops = {
    .enable        =  evaudc_ep0_enable,
    .disable       =  evaudc_ep0_disable,
    .alloc_request =  evaudc_ep_alloc_request,
    .free_request  =  evaudc_free_request,
    .queue         =  evaudc_ep0_queue,
    .dequeue       =  evaudc_ep_dequeue,
    //.set_halt    =  evaudc_ep_set_halt,
};

static const struct usb_ep_ops evausb_ep_ops = {
    .enable        =  evaudc_ep_enable,
    .disable       =  evaudc_ep_disable,
    .alloc_request =  evaudc_ep_alloc_request,
    .free_request  =  evaudc_free_request,
    .queue         =  evaudc_ep_queue,
    .dequeue       =  evaudc_ep_dequeue,
    //.set_halt    =  evaudc_ep_set_halt,
};

#if 0
/**
 * evaudc_wakeup - Send remote wakeup signal to host
 * @gadget: pointer to the usb gadget structure.
 *
 * Return: 0 on success and error on failure
 */
static int evaudc_wakeup(struct usb_gadget *gadget)
{
    struct evausb_udc *udc = to_udc(gadget);
    u32 crtlreg;
    int status = -EINVAL;
    unsigned long flags;

    /* Remote wake up not enabled by host */
    if (!udc->remote_wkp)
        goto done;

    crtlreg = udc->read_fn(udc->addr + UDC_USB_CTRL_REG_ADDR);
    crtlreg |= EVAUSB_CONTROL_USB_RMTWAKE_MASK;
    /* set remote wake up bit */
    udc->write_fn(udc->addr, UDC_USB_CTRL_REG_ADDR, crtlreg);
    /*
     * wait for a while and reset remote wake up bit since this bit
     * is not cleared by HW after sending remote wakeup to host.
     */
    mdelay(2);

    crtlreg &= ~EVAUSB_CONTROL_USB_RMTWAKE_MASK;
    udc->write_fn(udc->addr, UDC_USB_CTRL_REG_ADDR, crtlreg);
    status = 0;
done:
    return status;
}
#endif

/**
 * evaudc_pullup - start/stop USB traffic
 * @gadget: pointer to the usb gadget structure.
 * @is_on: flag to start or stop
 *    usbcs.6 bit (discon)Peripheral Mode operation:
 *    Software disconnect bit.
 *    The microprocessor can write this bit (a '1' value) to force the "disconnect" state. In the
 *    "disconnect" state, an external USB2.0 transceiver disconnects pull-up resistors from
 *     the D+ and D- lines and the USB host detects device disconnection.
 * Return: 0 always
 *
 * This function starts/stops SIE engine of IP based on is_on.
 */
static int evaudc_pullup(struct usb_gadget *gadget, int is_on)
{
    struct evausb_udc *udc = to_udc(gadget);
    u32 crtlreg;
    //u32 otgreg;
    u32 ier;

    crtlreg = udc->read_fn((u32)udc->addr + UDC_USB_CTRL_REG_ADDR);
    if (is_on)
        crtlreg &= ~EVAUSB_CONTROL_USB_READY_MASK;
#if 0 // Using this method to disconnect the device is not very good
    else
        crtlreg |= EVAUSB_CONTROL_USB_READY_MASK;
#endif

    udc->write_fn(udc->addr, UDC_USB_CTRL_REG_ADDR, crtlreg);
#if 0
    DBG("%s, config start : %s\n", __func__, is_on ? "connect" : "disconnect");
    otgreg = udc->read_fn((u32)udc->addr + UDC_OTG_REG_ADDR);
    DBG("%s, otgreg : %x\n", __func__, otgreg);
#endif

    if (is_on) {
        /* Enable the interrupts. */
        ier = EVAUSB_STATUS_HSPEEDIEIRQ_MASK|
            EVAUSB_STATUS_URESIEIRQ_MASK|
            EVAUSB_STATUS_SUSPIEIRQ_MASK |
            EVAUSB_STATUS_SOFIEIRQ_MASK;
        udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,ier);
        ier = udc->read_fn((u32)udc->addr + UDC_PING_USB_IEN_REG_ADDR);
        ier |= EVAUSB_STATUS_OVERFLOWIEIRQ_MASK|
            EVAUSB_STATUS_SUTOKEIEIRQ_MASK|
            EVAUSB_STATUS_SUDAVIEIRQ_MASK;
        udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,ier);
    } else {
        ier =0;
        udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,ier);
    }

#if 0
    mdelay(1);
    otgreg = udc->read_fn((u32)udc->addr + UDC_OTG_REG_ADDR);
    DBG("%s, otgreg : %x\n", __func__, otgreg);
    crtlreg >>= 8;
    crtlreg &= 0xff;
    //if (crtlreg == 0x) {
    otgreg |= (9 << 16);
    udc->write_fn(udc->addr, UDC_OTG_REG_ADDR, otgreg);
    //}
#endif

    gx_unmask_irq(IRQ_USB_SLAVE_USB);
    gx_unmask_irq(IRQ_USB_SLAVE_DMA);
    gx_unmask_irq(IRQ_USB_SLAVE_WU);

    return 0;
}

/**
 * xudc_eps_init - initialize endpoints.
 * @udc: pointer to the usb device controller structure.
 */
static void evaudc_initep(struct evausb_udc *udc,
        struct evausb_ep *ep,
        int epnum,
        bool dir_in)
{
    char *dir;

    if (epnum == 0)
        dir = "";
    else if (dir_in)
        dir = "in";
    else
        dir = "out";

    ep->is_in = dir_in;
    ep->epnumber = epnum;

    snprintf(ep->name, sizeof(ep->name), "ep%d%s", epnum, dir);

    INIT_LIST_HEAD(&ep->queue);
    INIT_LIST_HEAD(&ep->ep_usb.ep_list);

    /* add to the list of endpoints known by the gadget driver */
    if (epnum)
        list_add_tail(&ep->ep_usb.ep_list, &udc->gadget.ep_list);

    ep->udc = udc;
    ep->ep_usb.name = ep->name;
    usb_ep_set_maxpacket_limit(&ep->ep_usb, epnum ? 1024 : EP0_MAX_PACKET);

    if (epnum == 0) {
        ep->ep_usb.caps.type_control = true;
        ep->ep_usb.ops = &evausb_ep0_ops;
        ep->offset = EP0MAXPACK_REG_ADDR;
        ep->dma = (volatile struct eva_ep_dma_struct *)((u32)udc->addr + EP0DMAADDR_REG_ADDR);
    } else {
        ep->ep_usb.caps.type_iso = true;
        ep->ep_usb.caps.type_bulk = true;
        ep->ep_usb.caps.type_int = true;
        ep->ep_usb.ops = &evausb_ep_ops;
        if (dir_in) {
            ep->offset = UDC_EP_IN1MAXPACK_REG_ADDR + ((epnum-1) * 0x8);
            ep->dma = (volatile struct eva_ep_dma_struct *)((u32)udc->addr + UDC_EP_IN1DMAADDR_REG_ADDR + (epnum-1) * 0x20);
        } else {
            ep->offset = UDC_EP_OUT1MAXPACK_REG_ADDR + ((epnum-1) * 0x8);
            ep->dma = (volatile struct eva_ep_dma_struct *)((u32)udc->addr + UDC_EP_OUT1DMAADDR_REG_ADDR + (epnum-1) * 0x20);
        }
    }

    if (dir_in)
        ep->ep_usb.caps.dir_in = true;
    else
        ep->ep_usb.caps.dir_out = true;


    ep->is_iso = 0;
    evaudc_epconfig(ep, udc);

    return ;
}

static void evaudc_eps_init(struct evausb_udc *udc)
{
    evaudc_initep(udc, &udc->ep0, 0, 0);

    INIT_LIST_HEAD(&udc->gadget.ep_list);
    /* initialise the endpoints now the core has been initialised */
    evaudc_initep(udc, &udc->ep1_in,1, 1);
    evaudc_initep(udc, &udc->ep1_out,1, 0);
    evaudc_initep(udc, &udc->ep2_in,2, 1);
    evaudc_initep(udc, &udc->ep2_out,2, 0);
    evaudc_initep(udc, &udc->ep3_in,3, 1);
    evaudc_initep(udc, &udc->ep3_out,3, 0);

#ifdef ARCH_LEO
    // ep4in can't use
#else
    evaudc_initep(udc, &udc->ep4_in,4, 1);
#endif
    evaudc_initep(udc, &udc->ep4_out,4, 0);
    g_ep1in = &(udc->ep1_in.ep_usb);
    g_ep2in = &(udc->ep2_in.ep_usb);

    return ;
}

/**
 * evaudc_stop_activity - Stops any further activity on the device.
 * @udc: pointer to the usb device controller structure.
 */
static void evaudc_stop_activity(struct evausb_udc *udc)
{
    struct evausb_ep *ep;

    ep = &udc->ep1_in;
    evaudc_nuke(ep, -ESHUTDOWN);
    ep = &udc->ep1_out;
    evaudc_nuke(ep, -ESHUTDOWN);
    ep = &udc->ep2_in;
    evaudc_nuke(ep, -ESHUTDOWN);
    ep = &udc->ep2_out;
    evaudc_nuke(ep, -ESHUTDOWN);
    ep = &udc->ep3_in;
    evaudc_nuke(ep, -ESHUTDOWN);
    ep = &udc->ep3_out;
    evaudc_nuke(ep, -ESHUTDOWN);
#ifdef ARCH_LEO
#else
    ep = &udc->ep4_in;
    evaudc_nuke(ep, -ESHUTDOWN);
#endif
    ep = &udc->ep4_out;
    evaudc_nuke(ep, -ESHUTDOWN);

    return ;
}

/**
 * xudc_start - Starts the device.
 * @gadget: pointer to the usb gadget structure
 * @driver: pointer to gadget driver structure
 *
 * Return: zero on success and error on failure
 */
static int evaudc_start(struct usb_gadget *gadget,
        struct usb_gadget_driver *driver)
{
    struct evausb_udc *udc    = to_udc(gadget);
    struct evausb_ep *ep0    = &udc->ep0;
    const struct usb_endpoint_descriptor *desc = &config_bulk_out_desc;
    int ret = 0;

    if (udc->driver) {
        DBG("%s is already bound to %s\n", udc->gadget.name, udc->driver->function);
        ret = -EBUSY;
        goto err;
    }

    /* hook up the driver */
    udc->driver = driver;
    udc->gadget.speed = driver->max_speed;

    /* Enable the control endpoint. */
    ret = __evaudc_ep_enable(ep0, desc);

    /* Set device address and remote wakeup to 0 */
    *udc->fnaddr = 0x0;
    udc->remote_wkp = 0;
err:
    return ret;
}

/**
 * xudc_stop - stops the device.
 * @gadget: pointer to the usb gadget structure
 * @driver: pointer to usb gadget driver structure
 *
 * Return: zero always
 */
static int evaudc_stop(struct usb_gadget *gadget)
{
    struct evausb_udc *udc = to_udc(gadget);

    udc->gadget.speed = USB_SPEED_UNKNOWN;
    udc->driver = NULL;

    /* Set device address and remote wakeup to 0 */
    *udc->fnaddr = 0x0;
    udc->remote_wkp = 0;

    evaudc_stop_activity(udc);

    return 0;
}

static const struct usb_gadget_ops evausb_udc_ops = {
    //.get_frame   =  evaudc_get_frame,
    //.wakeup      =  evaudc_wakeup,
    .pullup        =  evaudc_pullup,
    .udc_start     =  evaudc_start,
    .udc_stop      =  evaudc_stop,
};

static struct evausb_udc g_eva_udc;
static struct evausb_req g_eva_status_req;
static u8 g_req_buff[STATUSBUFF_SIZE];

/**
 * xudc_clear_stall_all_ep - clears stall of every endpoint.
 * @udc: pointer to the udc structure.
 */
static void evaudc_clear_stall_all_ep(struct evausb_udc *udc)
{
    struct evausb_ep *ep;
    u32 epcfgreg = 0;

    ep = &udc->ep0;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP0_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);


    ep = &udc->ep1_in;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<ep->epnumber;
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }
    ep = &udc->ep1_out;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<(ep->epnumber+16);
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }

    ep = &udc->ep2_in;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<ep->epnumber;
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }
    ep = &udc->ep2_out;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<(ep->epnumber+16);
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }

    ep = &udc->ep3_in;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<ep->epnumber;
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }
    ep = &udc->ep3_out;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<(ep->epnumber+16);
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }

#ifdef ARCH_LEO
#else
    ep = &udc->ep4_in;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<ep->epnumber;
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }
#endif
    ep = &udc->ep4_out;
    epcfgreg = udc->read_fn((u32)udc->addr+ep->offset);
    epcfgreg &= ~EVAUSB_EP_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep->offset, epcfgreg);
    if (ep->epnumber) {
        /* Reset the toggle bit.*/
        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
        epcfgreg |= 1<<(ep->epnumber+16);
        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
    }

    return ;
}

static void lower_freq(void);
static void recover_freq(void);

static int do_adjust_freq = 0;
static int did_control_transfer = 0;

static void evaudc_suspend_usbphy(void)
{
    unsigned int intrreg;
    struct evausb_udc *udc = &g_eva_udc;
    /*
     * 按照 otg 控制器手册中说明的配置（2.10.2），这个操作会关闭 clkusb
     * 时钟并使外部 UTMI+ 组件（usb phy）进入 suspend 状态，以降低功耗
     */
    intrreg = udc->read_fn((u32)udc->addr + UDC_FN_CTRL_REG_ADDR);
    intrreg = intrreg | (0x1 << 24);
    udc->write_fn(udc->addr, UDC_FN_CTRL_REG_ADDR, intrreg);
}

typedef enum {
    UDC_STATUS_UNKNOW  = 0,
    UDC_STATUS_SUSPEND = 1,
    UDC_STATUS_RESET   = 2,
    UDC_STATUS_RESUME  = 3,
} udc_status_t;

static udc_status_t udc_status = UDC_STATUS_UNKNOW;

/**
 * evaudc_handle_resume - usb device controller resume signal handler
 * @udc: pointer to the udc structure.
 *
 * This function handles the resume interrupts.
 */
static void evaudc_handle_resume(struct evausb_udc *udc)
{
    u32 intrreg;

    printf("Resume\n");

    if (udc_status != UDC_STATUS_SUSPEND)
        return ;

#if defined(CONFIG_MCU_UDC_ENABLE_UAC2) || defined(CONFIG_MCU_UDC_ENABLE_UAC1)
    our_callback_ops.state |= (1<<UAC2_CONNECT_STATE_BIT);
    if (our_callback_ops.callback_ops.notify_callback)
        our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_DEVICE, USB_DEVICE_RESUME);
#endif

    evaudc_stop_activity(udc);
    evaudc_clear_stall_all_ep(udc);

    // default is in full speed
    udc->gadget.speed = USB_SPEED_FULL;
    udc->remote_wkp = 0;

    /* Enable the suspend, resume and disconnect */
    intrreg = udc->read_fn((u32)udc->addr + UDC_PING_USB_IEN_REG_ADDR);
    intrreg |= EVAUSB_STATUS_OVERFLOWIEIRQ_MASK |
        EVAUSB_STATUS_HSPEEDIEIRQ_MASK|EVAUSB_STATUS_URESIEIRQ_MASK |
        EVAUSB_STATUS_SUSPIEIRQ_MASK|EVAUSB_STATUS_SUTOKEIEIRQ_MASK |
        EVAUSB_STATUS_SUDAVIEIRQ_MASK | EVAUSB_STATUS_SOFIEIRQ_MASK;
    udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,intrreg);

    if (udc->driver && udc->driver->resume) {
        udc->driver->resume(&udc->gadget);
    }

    udc_status = UDC_STATUS_RESUME;
}

/**
 * evaudc_startup_handler - The usb device controller interrupt handler.
 * @udc: pointer to the udc structure.
 * @intrstatus: The mask value containing the interrupt sources.
 *
 * This function handles the RESET,SUSPEND and DISCONNECT interrupts.
 */
static void evaudc_startup_handler(struct evausb_udc *udc, u32 intrstatus)
{
    u32 intrreg;

    if (intrstatus & EVAUSB_STATUS_URESIEIRQ_MASK) {

        printf("Reset\n");

        if (do_adjust_freq) {
            lower_freq();
        }

#if defined(CONFIG_MCU_UDC_ENABLE_UAC2) || defined(CONFIG_MCU_UDC_ENABLE_UAC1)
        our_callback_ops.state |= (1<<UAC2_CONNECT_STATE_BIT);
        if (our_callback_ops.callback_ops.notify_callback)
            our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_DEVICE, USB_DEVICE_RESET);
#endif
        evaudc_stop_activity(udc);
        evaudc_clear_stall_all_ep(udc);
        //clear the interrupt
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_URESIEIRQ_MASK);

        // default is in full speed
        udc->gadget.speed = USB_SPEED_FULL;

        /* Set device address and remote wakeup to 0 */
        *udc->fnaddr = 0x0;
        udc->remote_wkp = 0;

        /* Enable the suspend, resume and disconnect */
        intrreg = udc->read_fn((u32)udc->addr + UDC_PING_USB_IEN_REG_ADDR);
        intrreg |= EVAUSB_STATUS_OVERFLOWIEIRQ_MASK |
            EVAUSB_STATUS_HSPEEDIEIRQ_MASK|EVAUSB_STATUS_URESIEIRQ_MASK |
            EVAUSB_STATUS_SUSPIEIRQ_MASK|EVAUSB_STATUS_SUTOKEIEIRQ_MASK |
            EVAUSB_STATUS_SUDAVIEIRQ_MASK | EVAUSB_STATUS_SOFIEIRQ_MASK;
        udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,intrreg);

        udc_status = UDC_STATUS_RESET;
    }
    if (intrstatus & EVAUSB_STATUS_SUSPIEIRQ_MASK) {

        printf("Suspend\n");
#if defined(CONFIG_MCU_UDC_ENABLE_UAC2) || defined(CONFIG_MCU_UDC_ENABLE_UAC1)
        our_callback_ops.state &= ~(1<<UAC2_CONNECT_STATE_BIT);
        if (our_callback_ops.callback_ops.notify_callback)
            our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_DEVICE, USB_DEVICE_SUSPEND);
#endif
        //clear the interrupt
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_SUSPIEIRQ_MASK);

        /* Enable the reset, resume and disconnect */
        intrreg = udc->read_fn((u32)udc->addr + UDC_PING_USB_IEN_REG_ADDR);
        intrreg |= EVAUSB_STATUS_OVERFLOWIEIRQ_MASK |
            EVAUSB_STATUS_HSPEEDIEIRQ_MASK|EVAUSB_STATUS_URESIEIRQ_MASK |
            EVAUSB_STATUS_SUSPIEIRQ_MASK|EVAUSB_STATUS_SUTOKEIEIRQ_MASK |
            EVAUSB_STATUS_SUDAVIEIRQ_MASK;
        udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,intrreg);
        intrreg = udc->read_fn((u32)udc->addr + UDC_PING_USB_IEN_REG_ADDR);

        udc->usb_state = USB_STATE_SUSPENDED;
        if (udc->driver && udc->driver->suspend) {
            udc->driver->suspend(&udc->gadget);
        }

        evaudc_suspend_usbphy();

        udc_status = UDC_STATUS_SUSPEND;
        //otgreg = udc->read_fn((u32)udc->addr + UDC_OTG_REG_ADDR);
        //otgreg |= (9 << 16);
        //udc->write_fn(udc->addr, UDC_OTG_REG_ADDR, otgreg);
    }

    /*In the Device mode the core can be resumed in three ways:
      Resume signaling from host
      Reset signaling from host
      Wakeup pin (remote wakeup) ,we need to handle the situation...*/

    return ;
}

/**
 * evaudc_ep0_stall - Stall endpoint zero.
 * @udc: pointer to the udc structure.
 *
 * This function stalls endpoint zero.
 */
static void evaudc_ep0_stall(struct evausb_udc *udc)
{
    u32 epcfgreg;
    struct evausb_ep *ep0 = &udc->ep0;

    epcfgreg = udc->read_fn((u32)udc->addr + ep0->offset);
    epcfgreg |= EVAUSB_EP0_CFG_STALL_MASK;
    udc->write_fn(udc->addr, ep0->offset, epcfgreg);

    return ;
}

/**
 * xudc_setaddress - executes SET_ADDRESS command
 * @udc: pointer to the udc structure.
 *
 * This function executes USB SET_ADDRESS command
 */
static void evaudc_setaddress(struct evausb_udc *udc)
{
    struct evausb_ep *ep0    = &udc->ep0;
    struct evausb_req *req    = udc->req;
    int ret;

    req->usb_req.length = 0;
    ret = __evaudc_ep0_queue(ep0, req);
    if (ret == 0)
        return;

    DBG ("Can't respond to SET ADDRESS request\n");
    evaudc_ep0_stall(udc);

    return ;
}

/**
 * xudc_getstatus - executes GET_STATUS command
 * @udc: pointer to the udc structure.
 *
 * This function executes USB GET_STATUS command
 */
static void evaudc_getstatus(struct evausb_udc *udc)
{
    struct evausb_ep *ep0    = &udc->ep0;
    struct evausb_req *req    = udc->req;
    struct evausb_ep *target_ep;
    u16 status = 0;
    u32 epcfgreg;
    int epnum;
    u32 halt;
    int ret;

    switch (udc->setup.bRequestType & USB_RECIP_MASK) {
        case USB_RECIP_DEVICE:
            /* Get device status */
            status = 1 << USB_DEVICE_SELF_POWERED;
            if (udc->remote_wkp)
                status |= (1 << USB_DEVICE_REMOTE_WAKEUP);
            break;
        case USB_RECIP_INTERFACE:
            break;
        case USB_RECIP_ENDPOINT:
            epnum = udc->setup.wIndex & USB_ENDPOINT_NUMBER_MASK;
            if (!epnum) {
                target_ep = &udc->ep0;
                epcfgreg = udc->read_fn((u32)udc->addr + target_ep->offset);
                halt = epcfgreg & EVAUSB_EP0_CFG_STALL_MASK;
            } else {
                DBG("evaudc_getstatus : epnum %d\n",epnum);
                if (udc->setup.wIndex & USB_DIR_IN) {
                    if (epnum == 1)
                        target_ep = &udc->ep1_in;
                    else if (epnum == 2)
                        target_ep = &udc->ep2_in;
                    else if (epnum == 3)
                        target_ep = &udc->ep3_in;
#ifdef ARCH_LEO
#else
                    else if (epnum == 4)
                        target_ep = &udc->ep4_in;
#endif
                    else
                        target_ep = NULL;
                } else {
                    if (epnum == 1)
                        target_ep = &udc->ep1_out;
                    else if (epnum == 2)
                        target_ep = &udc->ep2_out;
                    else if (epnum == 3)
                        target_ep = &udc->ep3_out;
                    else if (epnum == 4)
                        target_ep = &udc->ep4_out;
                    else
                        target_ep = NULL;
                }

                if (!target_ep)
                    goto stall;

                epcfgreg = udc->read_fn((u32)udc->addr + target_ep->offset);
                halt = epcfgreg & EVAUSB_EP_CFG_STALL_MASK;
                if (udc->setup.wIndex & USB_DIR_IN) {
                    if (!target_ep->is_in)
                        goto stall;
                } else {
                    if (target_ep->is_in)
                        goto stall;
                }
            }
            if (halt)
                status = 1 << USB_ENDPOINT_HALT;
            break;
        default:
            goto stall;
    }

    req->usb_req.length = 2;
    *(u16 *)req->usb_req.buf = cpu_to_le16(status);
    ret = __evaudc_ep0_queue(ep0, req);
    if (ret == 0)
        return;
stall:
    DBG ("Can't respond to getstatus request\n");
    evaudc_ep0_stall(udc);

    return ;
}

/**
 * xudc_set_clear_feature - Executes the set feature and clear feature commands.
 * @udc: pointer to the usb device controller structure.
 *
 * Processes the SET_FEATURE and CLEAR_FEATURE commands.
 */
static void evaudc_set_clear_feature(struct evausb_udc *udc)
{
    struct evausb_ep *ep0    = &udc->ep0;
    struct evausb_req *req    = udc->req;
    struct evausb_ep *target_ep;
    u8 endpoint;
    u8 outinbit;
    u32 epcfgreg;
    int flag = (udc->setup.bRequest == USB_REQ_SET_FEATURE ? 1 : 0);
    int ret;

    switch (udc->setup.bRequestType) {
        case USB_RECIP_DEVICE:
            switch (udc->setup.wValue) {
                case USB_DEVICE_TEST_MODE:
                    /*
                     * The Test Mode will be executed
                     * after the status phase.
                     */
                    break;
                case USB_DEVICE_REMOTE_WAKEUP:
                    if (flag)
                        udc->remote_wkp = 1;
                    else
                        udc->remote_wkp = 0;
                    break;
                default:
                    evaudc_ep0_stall(udc);
                    break;
            }
            break;
        case USB_RECIP_ENDPOINT:
            if (!udc->setup.wValue) {
                endpoint = udc->setup.wIndex & USB_ENDPOINT_NUMBER_MASK;
                outinbit = udc->setup.wIndex & USB_ENDPOINT_DIR_MASK;
                if (!endpoint) {
                    target_ep = &udc->ep0;
                    epcfgreg = udc->read_fn((u32)udc->addr + target_ep->offset);
                } else {
                    DBG("evaudc_set_clear_feature : endpoint(%d)\n",endpoint);
                    if (udc->setup.wIndex & USB_DIR_IN) {
                        if (endpoint == 1)
                            target_ep = &udc->ep1_in;
                        else if(endpoint == 2)
                            target_ep = &udc->ep2_in;
                        else if(endpoint == 3)
                            target_ep = &udc->ep3_in;
#ifdef ARCH_LEO
#else
                        else if(endpoint == 4)
                            target_ep = &udc->ep4_in;
#endif
                        else
                            target_ep = NULL;
                    } else {
                        if(endpoint == 1)
                            target_ep = &udc->ep1_out;
                        else if (endpoint == 2)
                            target_ep = &udc->ep2_out;
                        else if (endpoint == 3)
                            target_ep = &udc->ep3_out;
                        else if (endpoint == 4)
                            target_ep = &udc->ep4_out;
                        else
                            target_ep = NULL;
                    }
                }

                if (!target_ep)
                    goto stall;

                outinbit = outinbit >> 7;

                /* Make sure direction matches.*/
                if (outinbit != target_ep->is_in) {
                    evaudc_ep0_stall(udc);
                    return;
                }
                epcfgreg = udc->read_fn((u32)udc->addr + target_ep->offset);
                if (!endpoint) {
                    /* Clear the stall.*/
                    epcfgreg &= ~EVAUSB_EP0_CFG_STALL_MASK;
                    udc->write_fn(udc->addr, target_ep->offset, epcfgreg);
                } else {
                    if (flag) {
                        epcfgreg |= EVAUSB_EP_CFG_STALL_MASK;
                        udc->write_fn(udc->addr, target_ep->offset, epcfgreg);
                    } else {
                        /* Unstall the endpoint.*/
                        epcfgreg &= ~(EVAUSB_EP_CFG_STALL_MASK);
                        udc->write_fn(udc->addr, target_ep->offset, epcfgreg);
                        /* Reset the toggle bit.*/
                        epcfgreg = udc->read_fn((u32)udc->addr + UDC_TOGRESET_REG_ADDR);
                        if (target_ep->is_in)
                            epcfgreg |= 1<<(target_ep->epnumber);
                        else
                            epcfgreg |= 1<<(target_ep->epnumber+16);
                        udc->write_fn(udc->addr,UDC_TOGRESET_REG_ADDR, epcfgreg);
                    }
                }
            }
            break;
        default:
            evaudc_ep0_stall(udc);
            return;
    }

    req->usb_req.length = 0;
    ret = __evaudc_ep0_queue(ep0, req);
    if (ret == 0)
        return;

stall:
    DBG ("Can't respond to SET/CLEAR FEATURE\n");
    evaudc_ep0_stall(udc);

    return ;
}

/**
 * evaudc_handle_setup - Processes the setup packet.
 * @udc: pointer to the usb device controller structure.
 *
 * Process setup packet and delegate to gadget layer.
 */
static void evaudc_handle_setup(struct evausb_udc *udc)
{
    struct evausb_ep *ep0 = &udc->ep0;
    struct usb_ctrlrequest setup;
    u32 *setupdat;
    //    u32 dcfg;

    /* Load up the chapter 9 command buffer.*/
    setupdat = (u32 *) ((u32)udc->addr + SETUP_PACKET_REG_ADDR);
    memcpy(&setup, setupdat, 8);
    udc->setup = setup;
    udc->setup.wValue = cpu_to_le16(setup.wValue);
    udc->setup.wIndex = cpu_to_le16(setup.wIndex);
    udc->setup.wLength = cpu_to_le16(setup.wLength);

    DBG ("Setup : type : 0x%x, req : 0x%x, value : 0x%x, index : 0x%x, length : %d\n", \
            udc->setup.bRequestType, udc->setup.bRequest, udc->setup.wValue, \
            udc->setup.wIndex, udc->setup.wLength);

    /* Clear previous requests */
    evaudc_nuke(ep0, -ECONNRESET);

    if (udc->setup.bRequestType & USB_DIR_IN) {
        /* Execute the get command.*/
        udc->setupseqrx = STATUS_PHASE;
        udc->setupseqtx = DATA_PHASE;
    } else {
        /* Execute the put command.*/
        udc->setupseqrx = DATA_PHASE;
        udc->setupseqtx = STATUS_PHASE;
    }

    switch (udc->setup.bRequest) {
        case USB_REQ_GET_STATUS:
            /* Data+Status phase form udc */
            if ((udc->setup.bRequestType &
                        (USB_DIR_IN | USB_TYPE_MASK)) !=
                        (USB_DIR_IN | USB_TYPE_STANDARD))
                break;
            evaudc_getstatus(udc);
            return;
        case USB_REQ_SET_ADDRESS:
            /* Status phase from udc */
            if (udc->setup.bRequestType != (USB_DIR_OUT |
                        USB_TYPE_STANDARD | USB_RECIP_DEVICE))
                break;
#if 0
        /* Set the address of the device.*/
        dcfg = udc->read_fn((u32)udc->addr+UDC_FN_CTRL_REG_ADDR);
        dcfg |= (udc->setup.wValue & 0x7f) << 16;
        udc->write_fn(udc->addr, UDC_FN_CTRL_REG_ADDR, dcfg);
#endif
            evaudc_setaddress(udc);
#if defined(CONFIG_MCU_UDC_ENABLE_UAC2) || defined(CONFIG_MCU_UDC_ENABLE_UAC1)
            if (our_callback_ops.callback_ops.notify_callback)
                our_callback_ops.callback_ops.notify_callback(NOTIFY_STATUS_DEVICE, USB_DEVICE_ADDRESSED);
#endif
            return;
        case USB_REQ_CLEAR_FEATURE:
        case USB_REQ_SET_FEATURE:
            /* Requests with no data phase, status phase from udc */
            if ((udc->setup.bRequestType & USB_TYPE_MASK)
                    != USB_TYPE_STANDARD)
                break;
            evaudc_set_clear_feature(udc);
            return;
        default:
            break;
    }

    if (udc->driver && udc->driver->setup(&udc->gadget, &setup) < 0)
        evaudc_ep0_stall(udc);

    return ;
}

/**
 * evaudc_ep0_out - Processes the endpoint 0 OUT token.
 * @udc: pointer to the usb device controller structure.
 */
static void evaudc_ep0_out(struct evausb_udc *udc)
{
    struct evausb_ep *ep0 = &udc->ep0;
    struct evausb_req *req;
    u32 dma_cur_sz;
    u32 act_len;

    ep0->is_in = false;

    if (list_empty(&ep0->queue))
        return;

    req = list_first_entry(&ep0->queue, struct evausb_req,queue);
    switch (udc->setupseqrx) {
        case STATUS_PHASE:
            /*
             * This resets both state machines for the next
             * Setup packet.
             */
            udc->setupseqrx = SETUP_PHASE;
            udc->setupseqtx = SETUP_PHASE;
            req->usb_req.actual = req->usb_req.length;
            evaudc_done(ep0, req, 0);
            break;
        case DATA_PHASE:
            evaudc_wrstatus(udc);
            dma_cur_sz = ep0->dma->dmatsize;
            act_len =  ep0->ep_usb.maxpacket - dma_cur_sz;
            req->usb_req.actual += act_len;

            /* Completion */
            if (req->usb_req.actual == req->usb_req.length) {
                if (udc->dma_enabled && req->usb_req.length)
                    evaudc_done(ep0, req, 0);
                return;
            }
            evaudc_read_fifo(ep0,req);
            break;
        default:
            break;
    }

    return ;
}

/**
 * evaudc_ep0_in - Processes the endpoint 0 IN token.
 * @udc: pointer to the usb device controller structure.
 */
static void evaudc_ep0_in(struct evausb_udc *udc)
{
    struct evausb_ep *ep0 = &udc->ep0;
    struct evausb_req *req;
    u32 epcfgreg = 0;
    u32 dma_cur_sz;
    u32 act_len;


    u8 test_mode = udc->setup.wIndex >> 8;

    ep0->is_in = true;

    if (list_empty(&ep0->queue))
        return ;
    req = list_first_entry(&ep0->queue, struct evausb_req, queue);

    switch (udc->setupseqtx) {
        case STATUS_PHASE:
            switch (udc->setup.bRequest) {
                case USB_REQ_SET_ADDRESS:
                    /* Set the address of the device.*/
                    epcfgreg = udc->read_fn((u32)udc->addr+UDC_FN_CTRL_REG_ADDR);
                    epcfgreg |= (udc->setup.wValue & 0x7f) << 16;
                    udc->write_fn(udc->addr, UDC_FN_CTRL_REG_ADDR, epcfgreg);
                    break;
                case USB_REQ_SET_FEATURE:
                    if (udc->setup.bRequestType ==
                            USB_RECIP_DEVICE) {
                        if (udc->setup.wValue ==
                                USB_DEVICE_TEST_MODE)
                            epcfgreg = udc->read_fn((u32)udc->addr + HC_PORT_CTRL_REG_ADDR);
                        epcfgreg |= ((test_mode & 0x1f)<<16);
                        udc->write_fn(udc->addr,HC_PORT_CTRL_REG_ADDR,epcfgreg);
                    }
                    break;
            }
            req->usb_req.actual = req->usb_req.length;
            evaudc_done(ep0, req, 0);
            break;
        case DATA_PHASE:
            evaudc_wrstatus(udc);
            dma_cur_sz = ep0->dma->dmatsize;
            act_len = req->real_dma_length - dma_cur_sz;
            req->usb_req.actual += act_len;
            /* Completion */
            if (req->usb_req.actual == req->usb_req.length) {
                if (udc->dma_enabled && req->usb_req.length)
                    evaudc_done(ep0, req, 0);

                return;
            }
            evaudc_write_fifo(ep0, req);
            break;
        default:
            break;
    }

    return ;
}

/**
 * evaudc_ctrl_ep_handler - Endpoint 0 interrupt handler.
 * @udc: pointer to the udc structure.
 * @intrstatus:    It's the mask value for the interrupt sources on endpoint 0.
 *
 * Processes the commands received during enumeration phase.
 */
static void evaudc_ctrl_ep_handler(struct evausb_udc *udc, u32 intrstatus)
{
    if (intrstatus & EVAUSB_STATUS_EP0_IN_IRQ_MASK) {
        /*clear interrupt firest*/
        udc->write_fn(udc->addr,UDC_DMA_INIRQ_REG_ADDR,EVAUSB_STATUS_EP0_IN_IRQ_MASK);
        udc->write_fn(udc->addr,UDC_EP_INIRQ_REG_ADDR,EVAUSB_STATUS_EP0_IN_IRQ_MASK);
        evaudc_ep0_in(udc);
    } else if (intrstatus & EVAUSB_STATUS_EP0_OUT_IRQ_MASK) {
        //clear the interrupt
        udc->write_fn(udc->addr,UDC_DMA_INIRQ_REG_ADDR,EVAUSB_STATUS_EP0_OUT_IRQ_MASK);
        udc->write_fn(udc->addr,UDC_EP_INIRQ_REG_ADDR,EVAUSB_STATUS_EP0_OUT_IRQ_MASK);
        evaudc_ep0_out(udc);
    } else {
        DBG("no possible to arrive here ?\n");
    }

    return ;
}

#if 0
/**
 * evaudc_nonctrl_ep_handler - Non control endpoint interrupt handler.
 * @udc: pointer to the udc structure.
 * @epnum: End point number for which the interrupt is to be processed
 * @intrstatus:    mask value for interrupt sources of endpoints other
 *        than endpoint 0.
 *
 * Processes the buffer completion interrupts.
 */
static void evaudc_nonctrl_ep_handler(struct evausb_udc *udc, u8 epnum,bool is_in,
        u32 intrstatus)
{

    struct evausb_req *req;
    struct evausb_ep *ep;
    if (is_in)
        ep = &udc->ep1_in;
    else
        ep = &udc->ep1_out;

    if (list_empty(&ep->queue))
        return;

    req = list_first_entry(&ep->queue, struct evausb_req, queue);

    if (ep->is_in)
        evaudc_write_fifo(ep, req);
}
#endif

static void evaudc_nonctrl_ep_dma_handler(struct evausb_udc *udc, u8 epnum,bool is_in,
        u32 intrstatus)
{
    struct evausb_req *req = NULL;
    struct evausb_ep *ep = NULL;;
    u32 is_short = 0;
    u32 dma_cur_sz;
    u32 act_len;
    int stop = 0;

    if (is_in) {
        if (epnum == 1)
            ep = &udc->ep1_in;
        else if (epnum == 2)
            ep = &udc->ep2_in;
        else if (epnum == 3)
            ep = &udc->ep3_in;
#ifdef ARCH_LEO
#else
        else if (epnum == 4)
            ep = &udc->ep4_in;
#endif
    } else {
        if (epnum == 1)
            ep = &udc->ep1_out;
        else if (epnum == 2)
            ep = &udc->ep2_out;
        else if (epnum == 3)
            ep = &udc->ep3_out;
        else if (epnum == 4)
            ep = &udc->ep4_out;
    }

    DBG ("%s, %s start : %x, stop : %x, rst : %x\n", __func__, ep->name,
            *(volatile unsigned int *)&udc->dma->dmastart_in,
            *(volatile unsigned int *)&udc->dma->dmastop_in,
            *(volatile unsigned int *)&udc->dma->dmasrst_in);

    if (list_empty(&ep->queue)) {
        printf ("%s, ep(%s) is already empty\n", __func__, ep->name);
        return;
    }

    req = list_first_entry(&ep->queue, struct evausb_req, queue);
    dma_cur_sz = ep->dma->dmatsize;

    /*
     * ep(bulk) 开始 dma 后，会收到 dma 完成中断时，大部分情况下 dmastart 中对应 ep 的
     * dma start bit 会被清 0，但是有时会出现这个 dma start bit 没有被清，而如果在此期间
     * 收到主机端发送的 token in 时，就会发送一个 0 长度的包，这是我们不想看到的，我们肯
     * 定不愿意发任何多余的数据。所以在每次完成 dma 后，复位下该 ep 的 dma channel
     *
     * 复位 dma channel 的影响：
     *     1. dma ctrl 寄存器被清 0，dma size 寄存器也被清 0
     *     2. dma start(dmastart_in)中对应的 bit 被清 0
     */
    if (ep->is_in)
        udc->dma->dmasrst_in |= 1<<ep->epnumber;
    else
        udc->dma->dmasrst_out |= 1<<ep->epnumber;

    if (is_in)
        act_len =  req->real_dma_length - dma_cur_sz;
    else {
        act_len =  ep->ep_usb.maxpacket - dma_cur_sz;
        if (act_len < ep->ep_usb.maxpacket)
            is_short = 1;
    }

    req->usb_req.actual += act_len;
    //evaudc_handle_unaligned_buf_complete(ep->udc,ep,req);

    /* Completion */
    if (is_in) {
        DBG("evaudc_nonctrl_ep_dma_handler :ep(%s) req->usb_req.actual(%x) req->usb_req.length(%x)\n",ep->name,req->usb_req.actual,req->usb_req.length);

        if (req->usb_req.actual == req->usb_req.length) {
            if (udc->dma_enabled && req->usb_req.length) {
                list_del_init(&req->queue);
                stop = list_empty(&ep->queue); // if empty, stop write fifo
                evaudc_done_lw(ep, req, 0);
            }
        }
    }
    else
    {
        DBG("ep(%s) : req->usb_req(%p).actual(0x%x) length(0x%x) is_short %d\n", ep->name,&req->usb_req,req->usb_req.actual,req->usb_req.length,is_short);
        if (req->usb_req.actual == req->usb_req.length || is_short) {
            if (udc->dma_enabled && req->usb_req.length) {
                list_del_init(&req->queue);
                stop = list_empty(&ep->queue); // if empty, stop read fifo
                evaudc_done_lw(ep, req, 0);
            }
        }
    }

    if (list_empty(&ep->queue) || stop)
        return;

    req = list_first_entry(&ep->queue, struct evausb_req, queue);

    if (is_in) {
        // todo，下一个版本的 udc 设计要考虑这里
        if (!ep->is_iso)
            evaudc_write_fifo(ep, req);
    } else
        evaudc_read_fifo(ep, req);

    return;
}

//#define ENABLE_CLOCK_SYNC_BY_SOF_DEBUG

// will be used in uac core
unsigned long long sof_times,     sof_times_1ms;
unsigned long long prev_sof_time, prev_sof_time_1ms;

#ifdef CONFIG_MCU_UDC_ENABLE_CLOCK_SYNC_BY_SOF
#define SOF_INTERVAL_US 125
#define TRIGGER_GATE (125 * 3 / 2)
static int cal_lost_sof_num(unsigned long long time)
{
    int lost_num = 0;

    while (1) {
        if (time < TRIGGER_GATE)
            break;

        time -= SOF_INTERVAL_US;
        lost_num++;
    }

    return lost_num;
}

#define MAX_TOLERABLE_TIME_DIFF 50

// for debug
#ifdef ENABLE_CLOCK_SYNC_BY_SOF_DEBUG
unsigned int sof_lost_happened, sof_diff_times;
unsigned long long sof_diff_time;
#endif
#endif

#ifdef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
static struct udc_timer_info {
    struct evausb_ep *eva_ep;
    struct evausb_req *req;
} uti;

static int job_count = -1;

static int send_data_job(void *priv)
{
    struct udc_timer_info *info = priv;

    if (job_count > 0) {
        job_count--;
        if (job_count == 0) {
            if (info->eva_ep && info->req)
                evaudc_write_fifo(info->eva_ep, info->req);
            job_count = -1;
        }
    }

    return 0;
}
#else
static unsigned int prev_buff_addr;
#endif
/**
 * evaudc_irq - The main interrupt handler.
 * @irq: The interrupt number.
 * @_udc: pointer to the usb device controller structure.
 *    After servicing the core\u2019s interrupt, the microprocessor clears the individual interrupt request flag in the core\u2019s
 *    SFRs. If any other USB interrupts are pending, the act of clearing the interrupt request flag causes the core
 *    to generate another pulse for the highest priority pending interrupt. If more than one interrupt is pending,
 *    each is serviced in the priority order shown in Table 2-16.
 *    note: The sequence of clearing interrupt requests is important. The microprocessor should first clear the
 *    main interrupt request flag (the usbintreq request) and then the individual interrupt requests in the core\u2019s
 *    register
 * Return: IRQ_HANDLED after the interrupt is handled.
 */
int evaudc_irq(int irq, void *_udc)
{
    struct evausb_udc *udc = _udc;
    u32 intrstatus;
    u32 usb_intr;
    u8 index;
    u32 epx_dmairq_in,epx_dmairq_out;
    u32 epx_irq_in,epx_irq_out;
    u32 dma_intrstatus;
    //u32 crtlreg;
    struct evausb_ep *eva_ep;
    struct evausb_req *req;

    if (irq == IRQ_USB_SLAVE_WU) {
        evaudc_handle_resume(udc);
	return 0;
    }

    /* Read the ping&usb  Interrupt Request Register.*/
    intrstatus = udc->read_fn((u32)udc->addr + PING_USB_IRQ_REG_ADDR);
    DBG("intrstatus(0x%x)!!!\n",intrstatus);
    if (intrstatus & (EVAUSB_STATUS_URESIEIRQ_MASK | EVAUSB_STATUS_SUSPIEIRQ_MASK)) {
        evaudc_startup_handler(udc, intrstatus);
    }
    if (intrstatus & EVAUSB_STATUS_SUDAVIEIRQ_MASK) {
        //clear the interrupt
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_SUDAVIEIRQ_MASK);
        evaudc_handle_setup(udc);
    }
    if (intrstatus & EVAUSB_STATUS_SUTOKEIEIRQ_MASK) {
        //clear the interrupt
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_SUTOKEIEIRQ_MASK);
    }
    if (intrstatus & EVAUSB_STATUS_OVERFLOWIEIRQ_MASK) {
        /*the core sends a NAK handshake when it works as a USB peripheral device, and
          When a FIFO overflow error occurs, the overflowir bit is set in the usbirq register.
          USB transfer for all OUT endpoints is stopped.Transfer is resumed when the overflowir flag is cleared by the CPU*/
        DBG("overflow!!!\n");
        udelay(10);
        //clear the interrupt
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_OVERFLOWIEIRQ_MASK);
    }

    if (intrstatus & EVAUSB_STATUS_HSPEEDIEIRQ_MASK) {
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_HSPEEDIEIRQ_MASK);
        udc->gadget.speed = USB_SPEED_HIGH;

        if (do_adjust_freq) {
            recover_freq();
        }
    }

    /* Read the EP Interrupt Request Register.*/
    usb_intr = udc->read_fn((u32)udc->addr + UDC_EP_INIRQ_REG_ADDR);
    dma_intrstatus = udc->read_fn((u32)udc->addr + UDC_DMA_INIRQ_REG_ADDR);
    //crtlreg = udc->read_fn((u32)udc->addr + UDC_USB_CTRL_REG_ADDR);
    DBG ("ep : usb_intr 0x%x dma_intrstatus 0x%x dma&usb ivec:0x%x\n", usb_intr, dma_intrstatus,crtlreg&0xff);
    if ((dma_intrstatus & 0xffffffff)) {
        for (index = 1; index < EVAUSB_MAX_ENDPOINTS; index++) {
            epx_dmairq_out = (dma_intrstatus &(EVAUSB_STATUS_EP1_OUT_IRQ_MASK <<(index - 1)));
            epx_dmairq_in = (dma_intrstatus &(EVAUSB_STATUS_EP1_IN_IRQ_MASK <<(index - 1)));

            if(epx_dmairq_out) {
                //clear the interrupt first
                udc->write_fn(udc->addr,UDC_DMA_INIRQ_REG_ADDR,epx_dmairq_out);
                evaudc_nonctrl_ep_dma_handler(udc, index,false,
                        dma_intrstatus);
            }
            if(epx_dmairq_in) {
                //clear the interrupt first
                udc->write_fn(udc->addr,UDC_DMA_INIRQ_REG_ADDR,epx_dmairq_in);
                evaudc_nonctrl_ep_dma_handler(udc, index,true,
                        dma_intrstatus);
            }
        }
    }

    if ((usb_intr & 0xffffffff)) {
        if (usb_intr & (EVAUSB_STATUS_EP0_IN_IRQ_MASK | EVAUSB_STATUS_EP0_OUT_IRQ_MASK)) {
            //clear and handle interrupts
            evaudc_ctrl_ep_handler(udc, usb_intr);

            if (do_adjust_freq) {
                if (!did_control_transfer) {
                    lower_freq();
                    did_control_transfer = 1;
                }
            }
        }
#if 1
        for (index = 1; index < EVAUSB_MAX_ENDPOINTS; index++) {
            epx_irq_in = (usb_intr &(EVAUSB_STATUS_EP1_IN_IRQ_MASK <<(index - 1))) ;
            epx_irq_out = (usb_intr &(EVAUSB_STATUS_EP1_OUT_IRQ_MASK <<(index - 1)));
            if (epx_irq_in ) {
                //clear the interrupt first
                udc->write_fn(udc->addr,UDC_EP_INIRQ_REG_ADDR,epx_irq_in);
                /*evaudc_nonctrl_ep_handler(udc, index,true,
                  usb_intr);*/
            } else if(epx_irq_out ) {
                //clear the interrupt first
                udc->write_fn(udc->addr,UDC_EP_INIRQ_REG_ADDR,epx_irq_out);
                /*evaudc_nonctrl_ep_handler(udc, index,false,
                  usb_intr);*/
            }
        }
#endif
    }

    if (intrstatus & EVAUSB_STATUS_SOFIEIRQ_MASK) {
        //clear the interrupt
        udc->write_fn(udc->addr,PING_USB_IRQ_REG_ADDR,EVAUSB_STATUS_SOFIEIRQ_MASK);

#ifdef CONFIG_MCU_UDC_ENABLE_CLOCK_SYNC_BY_SOF
        /* add clock synchronization strategy */
        unsigned long long diff_time  = get_time_us() - prev_sof_time;
        int diff_times = 0;

        prev_sof_time = get_time_us();

        if (diff_time > (2 * SOF_INTERVAL_US - MAX_TOLERABLE_TIME_DIFF)) {
            diff_times = cal_lost_sof_num(diff_time);
#ifdef ENABLE_CLOCK_SYNC_BY_SOF_DEBUG
            sof_lost_happened = 1;
            sof_diff_time     = diff_time;
            sof_diff_times    = diff_times;
#endif
        }

        do {
            // only check frame 0 every 8 frames
            if (!(sof_times & 0x7)) {
                prev_sof_time_1ms = get_time_us();
                sof_times_1ms++;
            }
            sof_times++;
        } while (diff_times--);
#endif /* #ifdef CONFIG_MCU_UDC_ENABLE_CLOCK_SYNC_BY_SOF */

        /* handle iso transfer */
        if (g_ep1in) {
            eva_ep = to_evausb_ep(g_ep1in);
            if (eva_ep->is_in && eva_ep->is_iso) {
                if (!list_empty(&eva_ep->queue)){
                    req = list_first_entry(&eva_ep->queue, struct evausb_req, queue);

#ifndef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
                    // high speed
                    if ((u32)req->usb_req.buf == prev_buff_addr) {
#define TIMEOUT 20
                        unsigned long long start_time;
                        start_time  = get_time_us();
                        do {
                            dma_intrstatus = udc->read_fn((u32)udc->addr + UDC_DMA_INIRQ_REG_ADDR);
                            if (dma_intrstatus & EVAUSB_STATUS_EP1_IN_IRQ_MASK) {
                                udc->write_fn(udc->addr, UDC_DMA_INIRQ_REG_ADDR, EVAUSB_STATUS_EP1_IN_IRQ_MASK);
                                evaudc_nonctrl_ep_dma_handler(udc, 1, true, dma_intrstatus);
                                break;
                            }

                            if ((get_time_us() - start_time) >= TIMEOUT)
                                break;
                        } while (1);
                    }
                    prev_buff_addr = (u32)req->usb_req.buf;
                    evaudc_write_fifo(eva_ep, req);
#else
                    // full speed
                    uti.eva_ep = eva_ep;
                    uti.req    = req;
                    job_count = 6;
#endif
                }
            }
        }
    }

    return 0;
}

/**
 * evaudc_probe - The device probe function for driver initialization.
 * @pdev: pointer to the platform device structure.
 *
 * Return: 0 for success and error value on failure
 */

int evaudc_hw_init(void)
{
    struct evausb_udc *udc;
    int ret;
    u32 i = 0;
    udc = &g_eva_udc;
    memset(udc, 0, sizeof(*udc));

    /* Create a dummy request for GET_STATUS, SET_ADDRESS */
    udc->req = &g_eva_status_req;
    memset(udc->req, 0, sizeof(struct evausb_req));

    udc->req->usb_req.buf = g_req_buff;

    /* Map the registers */
    udc->addr = (void *)MCU_REG_BASE_USB_SLAVE;

    /* Setup gadget structure */
    udc->gadget.ops = &evausb_udc_ops;
#ifdef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
    udc->gadget.max_speed = USB_SPEED_FULL;
#else
    udc->gadget.max_speed = USB_SPEED_HIGH;
#endif
    udc->gadget.speed = USB_SPEED_UNKNOWN;
    udc->gadget.ep0 = &udc->ep0.ep_usb;
    udc->gadget.name = driver_name;

    /* Check for IP endianness */
    udc->write_fn = evaudc_write32;
    udc->read_fn = evaudc_read32;

    evaudc_eps_init(udc);
    udc->dma = (volatile struct general_dma*)((u32)udc->addr + UDC_DMASTART);

    udc->fnaddr = (volatile u8*)((u32)udc->addr + UDC_FNADDR_REG_ADDR);
    udc->usbcs = (volatile u8*)((u32)udc->addr + USBCS_REG_ADDR);

    /* Set device address to 0.*/
    *udc->fnaddr = 0x0;

    udc->dma_enabled = true;

    for (i = 0; i < MAX_USB_REQ_NUM;i++)
        g_eva_data_req[i].used = false;

    udc->write_fn(udc->addr,UDC_PING_USB_IEN_REG_ADDR,0);
    gx_request_irq(IRQ_USB_SLAVE_WU, evaudc_irq, udc);
    gx_request_irq(IRQ_USB_SLAVE_USB, evaudc_irq, udc);
    gx_request_irq(IRQ_USB_SLAVE_DMA, evaudc_irq, udc);
    gx_mask_irq(IRQ_USB_SLAVE_USB);
    gx_mask_irq(IRQ_USB_SLAVE_DMA);
    gx_mask_irq(IRQ_USB_SLAVE_WU);

    ret = usb_add_gadget_udc(NULL, &udc->gadget);
    if (ret)
        goto fail;

    udc->dev = &udc->gadget.dev;

#ifdef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
    TimerRegisterCallback_us(send_data_job, &uti, 100);
#endif

    return 0;
fail:
    return ret;
}

static void evaudc_hw_simple_init(void)
{
    struct evausb_udc *udc = &g_eva_udc;

    memset(udc, 0, sizeof(*udc));

    /* Map the registers */
    udc->addr = (void *)MCU_REG_BASE_USB_SLAVE;

    /* Check for IP endianness */
    udc->write_fn = evaudc_write32;
    udc->read_fn = evaudc_read32;
}

static void udc_cold_reset_release(void)
{
    volatile RESET_REGS *rst_regs = (volatile RESET_REGS *)(MCU_REG_BASE_RESET);
    volatile unsigned j = 100;

    while (j--);
    rst_regs->mepg_cld_rst_norm.bits.usb_slave_cold_rst_n = 0;

    return ;
}

static void udc_cold_reset(void)
{
    volatile RESET_REGS *rst_regs = (volatile RESET_REGS *)(MCU_REG_BASE_RESET);

    rst_regs->mepg_cld_rst_norm.bits.usb_slave_cold_rst_n = 1;

    return ;
}

static void clock_init(void)
{
    static int init = 0;
    volatile CLOCK_DIV_CONFIG2 *cdc = (volatile CLOCK_DIV_CONFIG2 *)MISC_REG_CLOCK_DIV_CONFIG2;

    if (!init) {
        cdc->bits.usbphy_clk_div_rstn    = 0;
        cdc->bits.usbphy_clk_div_rstn    = 1;

        cdc->bits.usbphy_clk_div_ratio   = 0;
        cdc->bits.usbphy_clk_div_ratio   = 6;

        cdc->bits.usbphy_clk_div_load_en = 0;
        cdc->bits.usbphy_clk_div_load_en = 1;

        init = 1;
    }

    return ;
}

#if 0
static void usb_phy_cold_reset(void)
{
    *((volatile unsigned int *)(0xa030a110)) |= (1 << 28);
    printf ("%s regs : %x\n", __func__, *((volatile unsigned int *)(0xa030a110)));

    return ;
}

static void usb_phy_cold_reset_release(void)
{
    volatile unsigned j = 100;

    while (j--);
    *((volatile unsigned int *)(0xa030a110)) &= ~(1 << 28);
    printf ("%s regs : %x\n", __func__, *((volatile unsigned int *)(0xa030a110)));

    return ;
}
#endif

static void usb_pll_en_clear(void)
{
    volatile USB4_CONFIG *usb4_config = (volatile USB4_CONFIG *)MISC_REG_USB4_CONFIG;

    usb4_config->bits.usb_pll_en = 0;

    return ;
}

static void usb_pll_en_set(void)
{
    volatile USB4_CONFIG *usb4_config = (volatile USB4_CONFIG *)MISC_REG_USB4_CONFIG;
    volatile unsigned j = 100;

    while (j--);
    usb4_config->bits.usb_pll_en = 1;

    return ;
}

static void lower_freq(void)
{
    volatile SOURCE_SEL *ss = (volatile SOURCE_SEL *)MISC_REG_SOURCE_SEL;

    ss->bits.clock_scpu_usb_source = 1;

    return ;
}

static void recover_freq(void)
{
    volatile SOURCE_SEL *ss = (volatile SOURCE_SEL *)MISC_REG_SOURCE_SEL;

    ss->bits.clock_scpu_usb_source = 0;

    return ;
}

static void udc_speed_set(enum udc_speed speed)
{
#ifdef CONFIG_GX8010NRE
    // NOT SUPPORT SPEED SWITCH
#elif defined (CONFIG_GX8008B)
    volatile USB4_CONFIG *usb4_config = (volatile USB4_CONFIG *)MISC_REG_USB4_CONFIG;

    switch (speed) {
        case UDC_SPEED_HIGH:
            usb4_config->bits.disablehs = 0;
            break;
        case UDC_SPEED_FULL:
            usb4_config->bits.disablehs = 1;
            break;
        default:
            break;
    }
#endif
}

/*
 * 注意：
 * 1. 为了低功耗的考虑，原本放在 UdcDone 中的 udc_cold_reset 调用，迁移到 UdcInit 中调用。这是因为调用
 *    udc_cold_reset 去复位 usb slave 时，会让原本进入到 suspend 中的 usb slave 和 usb phy 退出 suspend，
 *    这就增加了功耗
 * 2. 为了能让 usb slave 启动时能收到 suspend 中断，需要每次都配置 usb_pll_en bit，这就是增加
 *    usb_pll_en_clear 接口和 usb_pll_en_set 接口调用的原因
 */
int UdcInit(enum udc_speed speed)
{
    init_mempool();

    udc_speed_set(speed);

    clock_init();
    ClockEnable(CLOCK_UDC);

    usb_pll_en_clear();
    udc_cold_reset();

    usb_pll_en_set();
    udc_cold_reset_release();

    return evaudc_hw_init();
}

int UdcDone(void)
{
#ifdef CONFIG_MCU_UDC_WORK_IN_FULL_SPEED
	TimerUnregisterCallback(send_data_job);
#endif

    usb_del_gadget_udc(&g_eva_udc.gadget);
    gx_free_irq(IRQ_USB_SLAVE_WU);
    gx_free_irq(IRQ_USB_SLAVE_USB);
    gx_free_irq(IRQ_USB_SLAVE_DMA);
    *g_eva_udc.fnaddr = 0x0;
    g_eva_udc.dma_enabled = false;

    ClockDisable(CLOCK_UDC);

    return 0;
}

#define MAX_TRY_TIMES 50
int UdcDisable(void)
{
    struct evausb_udc *udc = &g_eva_udc;
    unsigned int regs;
    int count = 0;

    ClockEnable(CLOCK_UDC);

    usb_pll_en_clear();
    udc_cold_reset();
    usb_pll_en_set();
    udc_cold_reset_release();
    evaudc_hw_simple_init();

    // check suspend status
    while (1) {
        count++;

        regs = udc->read_fn((u32)udc->addr + PING_USB_IRQ_REG_ADDR);
        if (regs & EVAUSB_STATUS_SUSPIEIRQ_MASK)
            break;

        if (count == MAX_TRY_TIMES)
            break;

        mdelay(1);
    }

    if (regs & EVAUSB_STATUS_SUSPIEIRQ_MASK)
        evaudc_suspend_usbphy();

    ClockDisable(CLOCK_UDC);

    return 0;
}
