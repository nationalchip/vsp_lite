/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * rtc.c: RTC Driver
 *
 */

#include <autoconf.h>

#include <common.h>
#include <string.h>

#include <base_addr.h>
#include <soc_config.h>

#include <driver/irq.h>
#include <driver/rtc.h>

#include "rtc_regs.h"

#define RTC_MIN_DURATION    10
#define RTC_CLOCK           (CONFIG_APB2_CLK * RTC_MIN_DURATION / 1000000)
#define RTC_ITEM_NUM        4

static struct {
    struct {
        RTC_CALLBACK func;
        void        *priv;
    } callbacks[RTC_ITEM_NUM];
} s_rtc_state;

//=================================================================================================
// Internal Routinue

static unsigned int _Bcd2Dec(unsigned int value)
{
    unsigned int result = 0;
    for (int i = 7; i >= 0; i--) {
        result *= 10;
        result += ((value >> (i * 4)) & 0xF);
    }
    return result;
}

static unsigned int _Dec2Bcd(unsigned int value)
{
    unsigned int result = 0;
    for (int i = 0; i < 8; i++) {
        result |= (value % 10) << (i * 4);
        value /= 10;
    }
    return result;
}

static void _SetTime(volatile RTC_TIME_REGS* time_regs, const RTC_DATE_TIME *date_time)
{
    if (time_regs && date_time) {
        time_regs->year         = _Dec2Bcd(date_time->year);
        time_regs->month        = _Dec2Bcd(date_time->month);
        time_regs->month_day    = _Dec2Bcd(date_time->month_day);
        time_regs->week_day     = _Dec2Bcd(date_time->week_day);

        time_regs->hour         = _Dec2Bcd(date_time->hour);
        time_regs->minute       = _Dec2Bcd(date_time->minute);
        time_regs->second       = _Dec2Bcd(date_time->second);

        time_regs->ms           = date_time->microsecond / 1000;
        time_regs->us           = (date_time->microsecond % 1000) / 10;
    }
}

static void _GetTime(volatile RTC_TIME_REGS* time_regs, RTC_DATE_TIME *date_time)
{
    if (time_regs && date_time) {
        date_time->second       = _Bcd2Dec(time_regs->second);
        date_time->minute       = _Bcd2Dec(time_regs->minute);
        date_time->hour         = _Bcd2Dec(time_regs->hour);

        date_time->week_day     = _Bcd2Dec(time_regs->week_day);
        date_time->month_day    = _Bcd2Dec(time_regs->month_day);
        date_time->month        = _Bcd2Dec(time_regs->month);
        date_time->year         = _Bcd2Dec(time_regs->year);

        date_time->microsecond  = time_regs->us * 10 + time_regs->ms * 1000;
    }
}

static int _RtcISR(int irq, void *pdata)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    for (int i = 0; i < 4; i++) {
        if (regs->int_state.value & (1 << i)) {
            regs->int_state.value &= ~(1 << i);
            if (s_rtc_state.callbacks[i].func) {
                regs->int_state.value |= (1 << i);
                s_rtc_state.callbacks[i].func(i, s_rtc_state.callbacks[i].priv);
            }
            else {
                regs->int_enable.value &= ~(1 << i);
            }
        }
    }
    return 0;
}

//=================================================================================================

void RtcInit(void)
{
    // Only initialize the IRQ Table
    memset(&s_rtc_state, 0, sizeof(s_rtc_state));
}

void RtcDone(void)
{
    // Only finish the IRQ

}

void RtcReset(void)
{
    // Reset the RTC
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    regs->div_prescale.value = RTC_CLOCK;
    regs->ctrl.value = 0;
}

//-------------------------------------------------------------------------------------------------

int RtcSetCallback(RTC_ITEM item, RTC_CALLBACK callback, void *priv)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    if (callback) { // Setup
        s_rtc_state.callbacks[item].priv = priv;
        s_rtc_state.callbacks[item].func = callback;
        regs->int_enable.value |= 1 << item;
    }
    else {
        regs->int_enable.value &= ~(1 << item);
        s_rtc_state.callbacks[item].priv = NULL;
        s_rtc_state.callbacks[item].func = NULL;
    }

    if (regs->int_enable.value) {
        gx_request_irq(MCU_IRQ_RTC, _RtcISR, &s_rtc_state);
    }
    else {
        gx_free_irq(MCU_IRQ_RTC);
    }

    return 0;
}

int RtcSetISR(RTC_ISR isr, void *priv)
{
    if (isr) {
        gx_request_irq(MCU_IRQ_RTC, isr, priv);
    }
    else {
        gx_free_irq(MCU_IRQ_RTC);
    }

    return 0;
}

//=================================================================================================

void RtcStartTime(const RTC_DATE_TIME *date_time)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    regs->ctrl.time_en = 0;
    _SetTime(&regs->time, date_time);
    regs->ctrl.time_en = 1;
}

void RtcStopTime(void)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    regs->ctrl.time_en = 0;
}

void RtcGetTime(RTC_DATE_TIME *date_time)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    regs->snap_mode.value   = (1 << 2);
    _GetTime(&regs->time, date_time);
}

//=================================================================================================

void RtcStartTick(RTC_ITEM tick, const unsigned int duration_us)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    if (tick == RTC_ITEM_TICK1) {
        regs->tick1_count = duration_us / 10;
        regs->ctrl.tick1_en = 1;
    }
    else if (tick == RTC_ITEM_TICK2) {
        regs->tick2_count = duration_us / 10;
        regs->ctrl.tick2_en = 1;
    }
}

void RtcStopTick(RTC_ITEM tick)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    if (tick == RTC_ITEM_TICK1) {
        regs->ctrl.tick1_en = 0;
    }
    else if (tick == RTC_ITEM_TICK2) {
        regs->ctrl.tick2_en = 0;
    }
}

//=================================================================================================

void RtcStartAlarm(RTC_ITEM alarm, const RTC_DATE_TIME *date_time, RTC_ALARM_MASK mask)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    if (alarm == RTC_ITEM_ALARM1) {
        regs->ctrl.alarm1_en = 0;
        _SetTime(&regs->alarm[0].time, date_time);
        regs->alarm[0].mask = mask;
        regs->ctrl.alarm1_en = 1;
    }
    else if (alarm == RTC_ITEM_ALARM2) {
        regs->ctrl.alarm2_en = 0;
        _SetTime(&regs->alarm[1].time, date_time);
        regs->alarm[1].mask = mask;
        regs->ctrl.alarm2_en = 1;
    }
}

void RtcStopAlarm(RTC_ITEM alarm)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    if (alarm == RTC_ITEM_ALARM1) {
        regs->ctrl.alarm1_en = 0;
    }
    else if (alarm == RTC_ITEM_ALARM2) {
        regs->ctrl.alarm2_en = 0;
    }
}

void RtcGetAlarmTime(RTC_ITEM alarm, RTC_DATE_TIME *date_time, RTC_ALARM_MASK *mask)
{
    volatile RTC_REGS *regs = (volatile RTC_REGS *)MCU_REG_BASE_RTC;
    if (alarm == RTC_ITEM_ALARM1) {
        if (date_time) _GetTime(&regs->alarm[0].time, date_time);
        if (mask) *mask = regs->alarm[0].mask;
    }
    else if (alarm == RTC_ITEM_ALARM2) {
        if (date_time) _GetTime(&regs->alarm[1].time, date_time);
        if (mask) *mask = regs->alarm[1].mask;
    }
}
