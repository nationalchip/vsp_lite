/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * rtc_regs.c: Real Time Clock
 *
 */

#ifndef __RTC_REGS_H__
#define __RTC_REGS_H__

typedef union {
    unsigned int value;
    struct {
        unsigned time_en:1;
        unsigned tick1_en:1;
        unsigned tick2_en:1;
        unsigned alarm1_en:1;
        unsigned alarm2_en:1;
        unsigned :27;
    };
} RTC_CTRL;

typedef union {
    unsigned int value;
    struct {
        unsigned tick1:1;
        unsigned tick2:1;
        unsigned alarm1:1;
        unsigned alarm2:1;
        unsigned :28;
    };
} RTC_INT_BITS;

typedef union {
    unsigned int value;
    struct {
        unsigned div_prescaler:16;
        unsigned :16;
    };
} RTC_DIV_PRESCALE;

typedef union {
    unsigned int value;
    struct {
        unsigned us:1;              // microsecond
        unsigned ms:1;              // millisecond
        unsigned second:1;
        unsigned minute:1;
        unsigned hour:1;
        unsigned week_day:1;
        unsigned month_day:1;
        unsigned month:1;
        unsigned year:1;
        unsigned :23;
    };
} RTC_SNAP_MODE;

//=================================================================================================

typedef struct {
    unsigned int        us;         // 0 - 99, in unit of 10us
    unsigned int        ms;         // 0 - 999
    unsigned int        second;     // 0 - 59, BCD code
    unsigned int        minute;     // 0 - 59, BCD code
    unsigned int        hour;       // 0 - 23, BCD code
    unsigned int        week_day;   // 1 -  7, BCD code
    unsigned int        month_day;  // 1 - 31, BCD code
    unsigned int        month;      // 1 - 12, BCD code
    unsigned int        year;       // 1900 - 2100, BCD code
} RTC_TIME_REGS;

typedef struct {
    unsigned int        mask;       // 1: don't care the field
    RTC_TIME_REGS       time;
} RTC_ALARM_REGS;

typedef struct {
    RTC_CTRL            ctrl;
    RTC_INT_BITS        int_enable;
    RTC_INT_BITS        int_state;
    RTC_DIV_PRESCALE    div_prescale;
    unsigned int        tick1_count;
    RTC_ALARM_REGS      alarm[2];
    RTC_TIME_REGS       time;
    unsigned int        tick2_count;
    RTC_SNAP_MODE       snap_mode;
} RTC_REGS;

#endif /* __RTC_REGS_H__ */
