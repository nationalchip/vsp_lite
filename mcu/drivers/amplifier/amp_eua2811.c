/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * amp_eua2811.c:  Digital audio power amplifier with
 *             I2C interface
 *
 */

#include <common.h>
#include <string.h>

#include <driver/i2c.h>
#include <driver/gpio.h>
#include <driver/delay.h>

#include <driver/amp_eua2811.h>

#define EUA2811_POWER_DOWN 6
#define EUA2811_RESET_GPIO 19

struct regsister {
    int sysctl;     // System control register
    int ratectl;    // No MCLK and High Sample Rate Control
    int inmux;      // Input MUX register
    int pbtl;       // For pbtl
    int volctl;     // Master volume
};

static struct
{
    int i2c_bus;
    int chip;
    struct regsister regs;
} amplifier_dev = {
    .i2c_bus = 1,
    .chip = 0x1b,
    .regs = {
        .sysctl = 0x05,
        .ratectl = 0x1E,
        .inmux = 0x20,
        .pbtl = 0x80,
        .volctl = 0x07,
    },
};

static void int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            short2char(data, addr);
            break;
        case 3:
            int24_2char(data, addr);
            break;
        case 4:
            int2char(data, addr);
            break;
    }
}

static int gx_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    int i2c_bus = amplifier_dev.i2c_bus;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};
    int chip = amplifier_dev.chip;

    addr2buf(addr_buf, addr, alen);

    msg[0].addr  = chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = chip;
    msg[1].flags = 0;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);
    return status!=0?-1:0;
}


int AmpEua2811Init(int i2c_bus, int chip)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;

    amplifier_dev.i2c_bus = i2c_bus;
    amplifier_dev.chip = chip;

    unsigned char fault_mode = 0x00;
    unsigned char slave_mode = 0xA0;
    unsigned char volume_set = 0x30;
    unsigned char ch4_sel[4] = {0x11,0x89,0x7B,0x7A};
    unsigned char ch4_sel1[4] = {0x40,0x00,0x00,0x00};

    GpioSetDirection(EUA2811_POWER_DOWN, GPIO_DIRECTION_OUTPUT);
    GpioSetDirection(EUA2811_RESET_GPIO, GPIO_DIRECTION_OUTPUT);
    GpioSetLevel(EUA2811_POWER_DOWN, GPIO_LEVEL_HIGH);
    mdelay(100);

    //set i2c bus speed to 200 kb/s
    if (gx_i2c_set_speed(amplifier_dev.i2c_bus, 200) != 0) {
        printf("set i2c bus speed error!");
        return -1;
    }

    GpioSetLevel(EUA2811_RESET_GPIO, GPIO_LEVEL_LOW);
    mdelay(10);
    GpioSetLevel(EUA2811_RESET_GPIO, GPIO_LEVEL_HIGH);
    mdelay(60);

    gx_i2c_send(amplifier_dev.regs.sysctl, 1, &fault_mode, 1);
    mdelay(55);

    gx_i2c_send(amplifier_dev.regs.ratectl, 1, &slave_mode, 1);
    gx_i2c_send(amplifier_dev.regs.inmux, 1, ch4_sel, 4);
    gx_i2c_send(amplifier_dev.regs.pbtl, 1, ch4_sel1, 4);
    gx_i2c_send(amplifier_dev.regs.volctl, 1, &volume_set, 1);

    return 0;
}

