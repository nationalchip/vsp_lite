/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * dto.c: DTO API
 */

#include <misc_regs.h>

#include <driver/dto.h>

unsigned int DtoGetStep(DTO_TYPE dto_type)
{
    volatile DTO_CONFIG *dto_config = (volatile DTO_CONFIG *)(MISC_REG_DTO1_CONFIG + dto_type * 4);
    return dto_config->bits.step;
}

int DtoSetStep(DTO_TYPE dto_type, unsigned int dto_step)
{
    volatile unsigned int *dto_config = (volatile unsigned int *)(MISC_REG_DTO1_CONFIG + dto_type * 4);
    dto_step &= 0x3FFFFFFF;
    *dto_config = (1 << 31) | dto_step;
    *dto_config = (1 << 31) | (1 << 30) | dto_step;
    *dto_config = (1 << 31) | (1 << 30) | dto_step;
    *dto_config = (1 << 31) | dto_step;
    return 0;
}
