
#if 0   // Not Need yet

#include <stdio.h>
#include <common.h>
#include <string.h>
#include <errno.h>
#include <kernel.h>
#include <div64.h>

#define PGN40LP25F2000

#ifdef PGN40LP25F2000
#define BS_RANGE 1
#define OD_RANGE 4
#define R_RANGE  32
#define F_RANGE  128
#define FOUT_MIN_LOW 125000000
#define FOUT_MAX_LOW 2000000000
#define FVCO_MIN_LOW 1000000000
#define FVCO_MAX_LOW 2000000000
#define FOUT_MIN_HIG 125000000
#define FOUT_MAX_HIG 2000000000
#define FVCO_MIN_HIG 1000000000
#define FVCO_MAX_HIG 2000000000
#define FREF_MIN     10000000
#define FREF_MAX     50000000

#define CLK_GET(x, mask, shift)       (((x) & ((mask) << (shift))) >> (shift))

#define CLK_SET(x, mask, shift)       (((x) & (mask)) << (shift))

#define CLK_CON_SHIFT 14
#define CLK_OD_SHIFT 12
#define CLK_N_SHIFT  7
#define CLK_M_SHIFT  0

#define CLK_MASK_CON
#define CLK_MASK_OD 0x3
#define CLK_MASK_N  0x1f
#define CLK_MASK_M  0x7f

#endif


int pll_round_rate(int *BS, int *OD, int *R, int *F, unsigned long FIN,  unsigned long *FOUT_NEAR){
    int NR, NF, NO;
    unsigned long long FREF, DIFF, RATE;
    unsigned long long FVCO, FOUT;
    RATE = *FOUT_NEAR;
    FVCO = 0;
    FOUT = 0;
    DIFF = 1 << 31;
    FREF = 0;
    for(*BS=0;*BS<BS_RANGE;*BS=*BS+1){
        for(*OD=0;*OD<OD_RANGE;*OD=*OD+1){
            for(*R=0;*R<R_RANGE;*R=*R+1){
                for(*F=0;*F<F_RANGE;*F=*F+1){
                    NR = *R + 1 ;
                    NO = 1 << *OD;
                    NF = 2*(*F + 1) ;
                    FOUT = FIN * NF;
                    do_div(FOUT, NR * NO);
                    /*FOUT = ((FIN*NF) / (NR*NO));*/
                    FVCO = FOUT * NO;
                    FREF = FIN;
                    do_div(FREF, NR);

                    /*FREF = FIN / NR;*/
                    if((FREF <= FREF_MAX)&&(FREF >= FREF_MIN)){
                        if(*BS){
                            if((FOUT >=FOUT_MIN_HIG) && (FOUT<=FOUT_MAX_HIG)
                               && (FVCO >= FVCO_MIN_HIG) && (FVCO <= FVCO_MAX_HIG)){
                                if ((abs(FOUT - RATE) < DIFF)){
                                    DIFF = abs(FOUT - RATE);
                                    *FOUT_NEAR = FOUT;
                                }
                                if (FOUT == RATE){
                                    return 0;
                                }
                            }
                        }
                        else{
                            if((FOUT >=FOUT_MIN_LOW) && (FOUT<=FOUT_MAX_LOW)
                               && (FVCO >= FVCO_MIN_LOW) && (FVCO <= FVCO_MAX_LOW)){
                                if ((abs(FOUT - RATE) < DIFF)){
                                    DIFF = abs(FOUT - RATE);
                                    *FOUT_NEAR = FOUT;
                                }
                                if (FOUT == RATE){
                                    return 0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
}

unsigned long clk_pll_recalc_rate(unsigned long reg,
                                  unsigned long parent_rate)
{
    unsigned long long int rate;
    int clkod, clkn, clkm;
    u32 data;

    data  = readl(reg);
    clkm  = CLK_GET(data, CLK_MASK_M, CLK_M_SHIFT);
    clkn  = CLK_GET(data, CLK_MASK_N, CLK_N_SHIFT);
    clkod = CLK_GET(data, CLK_MASK_OD, CLK_OD_SHIFT);

    rate = parent_rate * 2 * (clkm + 1);
    do_div(rate, (clkn + 1) * (1 << clkod));

    return (unsigned long )rate;

}
long clk_pll_round_rate(unsigned long rate,
                        unsigned long prate)
{
    int BS, OD, R, F;
    unsigned long FOUT_NEAR = rate;
    pll_round_rate(&BS, &OD, &R, &F, prate, &FOUT_NEAR);
    return FOUT_NEAR;
    
}
int clk_pll_set_rate(unsigned long reg, unsigned long user_rate,
                     unsigned long parent_rate)
{
    int clkbp, clkod, clkn, clkm, j = 100;
    unsigned long FOUT_NEAR = user_rate;
    u32 data;
    pll_round_rate(&clkbp, &clkod, &clkn, &clkm, parent_rate, &FOUT_NEAR);
    data = (1 << 15) | (1 << 14) | (clkod << 12) | (clkn << 7) | clkm;
    writel(data, reg);
    while(j--);
    j = 100;
    data = (0 << 15) | (1 << 14) | (clkod << 12) | (clkn << 7) | clkm;
    writel(data, reg);
    while(j--);
    j = 100;
    data = (0 << 15) | (0 << 14) | (clkod << 12) | (clkn << 7) | clkm;
    writel(data, reg);
    while(j--);
    return 0;
}

/*const struct clk_ops pll_ops = {*/
/*.recalc_rate = clk_pll_recalc_rate,*/
/*.round_rate = clk_pll_round_rate,*/
/*.set_rate = clk_pll_set_rate,*/
/*};*/

#endif
