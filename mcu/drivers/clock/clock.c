/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * MCU's clock management
 */

#include <stdio.h>
#include <common.h>

#include <driver/clock.h>
#include <driver/irq.h>
#include <driver/multicore_lock.h>

#define   CONFIG_BASE           0xa030a000
#define   CLK_CONFIG_PLL_AUDIO  0xc0
#define   CLK_CONFIG_PLL_DTO    0xc8
#define   CLK_CONFIG_PLL_IN     0xe8
#define   CLK_CONFIG_GATE_1     0x18
#define   CLK_CONFIG_GATE_2     0x68
#define   CLK_CONFIG_SEL        0x170
#define   MCU_CLK_GATE          0x19c
#define   SRAM_CLK_GATE         0x25c

#define MCU_UART_GATE          (1 << 5)
#define MCU_APB2_GATE          (1 << 4)
#define MCU_CORE_SDC_GATE      (1 << 3)
#define MCU_SDC_GATE           (1 << 3)
#define MCU_AHB_GATE           (1 << 2)
#define MCU_CORE_GATE          (1 << 1)

#define MCU_IDLE_CLK_GATE (MCU_UART_GATE | MCU_APB2_GATE | MCU_CORE_SDC_GATE | MCU_SDC_GATE | MCU_AHB_GATE | MCU_CORE_GATE)

void ClockInit(void)
{
    *(volatile unsigned int *)(CONFIG_BASE + SRAM_CLK_GATE) = 0x01;
    *(volatile unsigned int *)(CONFIG_BASE + MCU_CLK_GATE) = MCU_IDLE_CLK_GATE;
    ClockDisable(CLOCK_MODULES);
}

void ClockEnable(uint64_t clock_type)
{
    uint32_t gate1 = clock_type & 0xFFFFFFFF;
    uint32_t gate2 = (clock_type >> 32) & 0xFFFFFFFF;

    *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_1) &= ~gate1;
    *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_2) &= ~gate2;
}

void ClockDisable(uint64_t clock_type)
{
    uint32_t gate1 = clock_type & 0xFFFFFFFF;
    uint32_t gate2 = (clock_type >> 32) & 0xFFFFFFFF;

    if (CLOCK_AUDIO_IN == clock_type) {
        if (0 == MultiCoreTryLock(MULTICORE_LOCK_OTP)) {
            *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_1) |= gate1;
            MultiCoreUnlock(MULTICORE_LOCK_OTP);
        }
    } else {
        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_1) |= gate1;
        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_2) |= gate2;
    }
}

typedef struct {
    unsigned int gate_1;
    unsigned int sel;
    unsigned int pll_audio;
}CLOCK_INFO;

static int _ClockWakeupHandler(int status, void *pdata)
{
    unsigned int config_pll_in;
    CLOCK_INFO *clock_info = (CLOCK_INFO *)pdata;

    if (clock_info->pll_audio) {
        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_AUDIO) |= (1 << 14);
    } else
        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_AUDIO) &= ~(1 << 14);

    *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_DTO)   &= ~(1 << 14);
    while(1) {
        config_pll_in = *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_IN);
        if (config_pll_in & 0x01)
            break;
    }
    *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_SEL) = clock_info->sel;
    *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_1) = clock_info->gate_1;

    return 0;
}

WAKEUP_SOURCE ClockSleep(WAKEUP_SOURCE wakeup_source)
{
    static CLOCK_INFO clock_info;
    clock_info.gate_1 = *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_GATE_1);
    clock_info.sel  = *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_SEL);
    clock_info.pll_audio  = (*(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_AUDIO) >> 14) & 0x1;

    ClockDisable(CLOCK_MODULES);
    if (wakeup_source & WAKEUP_SOURCE_AVAD) {
        ClockEnable(CLOCK_AUDIO_IN);
    }
#ifdef CONFIG_GX8008B
    else
#endif
    {
        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_SEL) = 0;

        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_AUDIO) |= (1 << 14);
        *(volatile unsigned int *)(CONFIG_BASE + CLK_CONFIG_PLL_DTO)   |= (1 << 14);
    }

    gx_request_wake(wakeup_source, _ClockWakeupHandler, &clock_info);

    return wakeup_source;
}
