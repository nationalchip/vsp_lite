#
# Voice Signal Preprocess
# Copyright (C) 2001-2020 NationalChip Co., Ltd
# ALL RIGHTS RESERVED!
#
# Makefile: drivers/ source list
#
#=================================================================================#

# Amplifers
dev_objs += drivers/amplifier/amp_eua2811.o

# Audio In
ifeq ($(CONFIG_GX8010NRE), y)
dev_objs += drivers/audio_in/audio_in_leo.o
else ifeq ($(CONFIG_GX8008B), y)
dev_objs += drivers/audio_in/audio_in_leo_mini.o
endif

# Audio Out
dev_objs += drivers/audio_out/audio_out.o

# Clock
dev_objs += drivers/clock/clock.o
dev_objs += drivers/clock/dto.o

# Counter
dev_objs += drivers/counter/counter_delay.o
dev_objs += drivers/counter/counter_timer.o

# CPU
dev_objs += drivers/cpu/cpu.o

# DMA
dev_objs += drivers/dma/dma.o
dev_objs += drivers/dma/dw/dw_dmac.o
dev_objs += drivers/dma/dw_dmac_axi/dw_dmac_axi.o

# DSP
dev_objs += drivers/dsp/dsp.o

# GPIO
dev_objs += drivers/gpio/gpio_nre.o

# IRR
dev_objs += drivers/irr/irr.o

# I2C
dev_objs += drivers/i2c/drv_dw_i2c.o

# Interrupt
dev_objs += drivers/intc/drv_intc.o

# LED
dev_objs += drivers/led/led_is31fl3236a.o
dev_objs += drivers/led/led_sn3193.o
dev_objs += drivers/led/led_sgm31324.o
dev_objs += drivers/led/led_aw9523b.o
dev_objs += drivers/led/led_is31fl3208a.o

# Misc
dev_objs += drivers/misc/drv_misc.o

# OTP
-include drivers/otp/Makefile

# OEM
ifneq ($(CONFIG_MCU_ENABLE_CPU), y)
dev_objs += drivers/oem/oem.o
endif

# PADMUX
ifeq ($(CONFIG_GX8008B), y)
dev_objs += drivers/padmux/padmux_leo_mini.o
else ifeq ($(CONFIG_GX8010NRE), y)
dev_objs += drivers/padmux/padmux_leo.o
endif

# PMU
dev_objs += drivers/pmu/pmu.o
#dev_objs += drivers/pmu/bq25890_charger.o
#dev_objs += drivers/pmu/cw2015_adc.o
dev_objs += drivers/pmu/rt5037.o

# RTC
dev_objs += drivers/rtc/rtc.o

# FIFO
dev_objs += drivers/fifo/sw_fifo.o

# SNPU
ifeq ($(CONFIG_GX8010NRE), y)
-include drivers/snpu/Makefile
endif

# SPI Flash
dev_objs += drivers/flash/flash.o
dev_objs += drivers/flash/spi_flash.o
ifeq ($(CONFIG_GX8008B), y)
    dev_objs += drivers/flash/spinor/flash_spi.o
    dev_objs += drivers/spi/device.o
    dev_objs += drivers/spi/spi_master_v3.o
else
    dev_objs += drivers/spi/dw_spi.o
    dev_objs += drivers/flash/spinor/spi_nor.o
    dev_objs += drivers/flash/spinand/spi_nand.o
endif

# TPM
dev_objs += drivers/tpm/tpm_atsha204a.o

# UART
dev_objs += drivers/uart/dw_uart.o

# CONSOLE
dev_objs += drivers/console/console.o

# UART MESSAGE
include drivers/uart_message/*/Makefile

# UDC
dev_objs += drivers/udc/core/udc_eva.o
## udc uac2.0
-include drivers/udc/uac2/core/Makefile
dev_objs += drivers/udc/uac2/uac2_gx.o
## udc hid
dev_objs += drivers/udc/hid/hid_gx.o
## udc acm device
dev_objs += drivers/udc/acm/f_acm.o
## udc mass storage
dev_objs += drivers/udc/ms/ms_gx.o
dev_objs += drivers/udc/naked/pseudo_msg.o
dev_objs += drivers/udc/naked/crc32.o
## udc like serial
dev_objs += drivers/udc/ls/ls.o
## udc uac1.0
dev_objs += drivers/udc/uac1/f_uac1.o
dev_objs += drivers/udc/uac1/u_uac1.o
## udc
dev_objs += drivers/udc/udc_comdev_config.o

# CODEC
ifeq ($(CONFIG_BOARD_CODEC_AD82584F), y)
dev_objs += drivers/codec/codec_ad82584f/codec_ad82584f.o
#dev_objs += drivers/codec/codec_nau8822/codec_nau8822.o
else
dev_objs += drivers/codec/codec_npcp215x/codec_npcp215x.o
endif

# Debug
ifeq ($(CONFIG_MCU_ENABLE_DEBUG), y)
ifeq ($(CONFIG_GX8010NRE), y)
dev_objs += drivers/audio_in/audio_in_leo_debug.o
else ifeq ($(CONFIG_GX8008B), y)
dev_objs += drivers/audio_in/audio_in_leo_mini_debug.o
endif
dev_objs += drivers/audio_out/audio_out_debug.o
dev_objs += drivers/dsp/dsp_debug.o
endif

# Watchdog
dev_objs += drivers/watchdog/watchdog.o

#MultiCore Lock
dev_objs += drivers/multicore_lock/multicore_lock.o

# message
dev_objs += drivers/message/message.o

# imageinfo
dev_objs += drivers/imageinfo/imageinfo.o
