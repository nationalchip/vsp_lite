/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc_board.c: Misc settings for GX8008_BT_SSD_V1.0 board
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/usb_gadget.h>
#include <driver/misc.h>
#include <driver/uart.h>
#include <driver/gpio.h>
#include <driver/delay.h>
#include <driver/sw_fifo.h>
#include <driver/imageinfo.h>
#include <driver/otp.h>
#include <driver/uart_message.h>

typedef struct {
    unsigned char magic_number[2];
    unsigned char total_length;
    unsigned char ocf;
    unsigned char ogf;
    unsigned char start_byte;
    unsigned char payload_length;
    unsigned char type_id;
    unsigned char command_id;
    unsigned char command_data[256];
} UART_COMMAND_PACKET_RX;

typedef struct {
    unsigned char magic_number[3];
    unsigned char total_length;
    unsigned char ocf;
    unsigned char ogf;
    unsigned char start_byte;
    unsigned char payload_length;
    unsigned char type_id;
    unsigned char command_id;
    unsigned char command_data[32];
} UART_COMMAND_PACKET_TX;


//=================================================================================================
static UART_COMMAND_PACKET_RX s_command_packet;

//=================================================================================================
static unsigned char _UartCalcCheckSum(unsigned char *buff, int length)
{
    unsigned char sum = 0;
    for (int i = 0; i < length; i++)
        sum = sum + buff[i];
    return (0x100-sum) & 0xFF;
}

static char _UartSendMessage(UART_COMMAND_PACKET_TX *ptx, unsigned char len)
{
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
        GsWrite((const char *)ptx, len);
#else
    for (int i = 0; i < len; i++)
        UartSendByte(UART_PORT0, (int)*(((char *)ptx) + i));
#endif

    return 0;
}

char UartGetPacket(void)
{
    char *buff = (char *)&s_command_packet;
    char ch = 0;
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
    while (GsRead(&ch,1) > 0) {
#else
    while (UartTryRecvByte(UART_PORT0, &ch) == 0) {
#endif
        if (ch == 0x04) {
            buff[0] = ch;
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
            GsRead(&ch,1);
#else
            ch = UartRecvByte(UART_PORT0);
#endif
            if (ch == 0xff) {
                buff[1] = ch;
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
                buff[2] = 0;
                GsRead(&buff[2],1);
#else
                buff[2] = UartRecvByte(UART_PORT0);
#endif
                if (buff[2] <= (sizeof(s_command_packet.command_data)+6)) {
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
                    for (int i = 0; i < buff[2]; i++) {
                        GsRead(&buff[i + 3], 1);
                    }
#else
                    for (int i = 0; i < buff[2]; i++) buff[i + 3] = UartRecvByte(UART_PORT0);
#endif
                }
                else
                    return -1;

                UART_COMMAND_PACKET_RX *pCommandPacket = (UART_COMMAND_PACKET_RX *)buff;
                if (_UartCalcCheckSum(&(pCommandPacket->payload_length), pCommandPacket->payload_length + 1) == pCommandPacket->command_data[pCommandPacket->payload_length-2])
                    return 0;
            }
        }
    }

    return -1;
}

char UartSendCommand(UART_COMMAND_ID cmd, UART_COMMAND_ACTION data)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0x08,
        .ocf            = 0xff,
        .ogf            = 0x08,
        .start_byte      = 0x55,
        .payload_length  = 0x03,
        .type_id         = 0x02
    };
    tx_buff.command_id = cmd;
    tx_buff.command_data[0] = data;
    tx_buff.command_data[1] = _UartCalcCheckSum(&tx_buff.payload_length, tx_buff.payload_length + 1);
    _UartSendMessage(&tx_buff, tx_buff.total_length + 4);

    return 0;
}

char UartSendCommandWithData(UART_COMMAND_ID cmd, unsigned char *cmdData, unsigned char dataLen)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0x08,
        .ocf            = 0xff,
        .ogf            = 0x08,
        .start_byte      = 0x55,
        .payload_length  = 0x03,
        .type_id         = 0x02
    };
    tx_buff.command_id = cmd;
    tx_buff.payload_length = dataLen + 2;
    if (cmd == UCI_NOTIFY_TX_2MIC_OPUS_DATA) {
        tx_buff.total_length = dataLen + 6;
    }
    else {
        tx_buff.total_length = tx_buff.payload_length + 5;
    }

    char temp_checksum = _UartCalcCheckSum(&tx_buff.payload_length, 3);
    temp_checksum += _UartCalcCheckSum((unsigned char *)cmdData, dataLen);
    _UartSendMessage(&tx_buff, 10);
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
    GsWrite((const char *)cmdData, dataLen);
    GsWrite((const char *)&temp_checksum, 1);
#else
    for (int i = 0; i < dataLen; i++)
        UartSendByte(UART_PORT0, (int)cmdData[i]);
    UartSendByte(UART_PORT0, (int) temp_checksum);
#endif
    return 0;
}

char UartSendWavEnd(void)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0x07,
        .ocf            = 0xff,
        .ogf            = 0x08,
        .start_byte      = 0x55,
        .payload_length  = 0x02,
        .type_id         = 0x02,
        .command_id      = 0x05
    };
    tx_buff.command_data[0] = _UartCalcCheckSum(&tx_buff.payload_length, tx_buff.payload_length + 1);
    _UartSendMessage(&tx_buff, tx_buff.total_length + 4);

    return 0;
}

void UartGetImageinfoVersion(unsigned char *image_info, unsigned char len)
{
    uint32_t version;

    if (len < 3) return;
    ImageInfoGetImageVersion(&version);
    if (version != 0xFFFFFFFF) {
        image_info[0] = (version>>16) & 0xFF;
        image_info[1] = (version>>8) & 0xFF;
        image_info[2] = (version) & 0xFF;
    }
    else {
        // Default version 0.0.0
        image_info[0] = 0;
        image_info[1] = 0;
        image_info[2] = 0;
    }

    if (len < (3+OTP_CHIP_NAME_LENGTH)) return;
    OtpGetChipName((char *)image_info+3, OTP_CHIP_NAME_LENGTH);
}

char UartSendImageinfo(void)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0x00,
        .ocf             = 0xff,
        .ogf             = 0x08,
        .start_byte      = 0x55,
        .payload_length  = 0x00,
        .type_id         = 0x02,
        .command_id      = UCI_NOTIFY_TX_IMAGEINFO
    };
    unsigned char image_info[3+OTP_CHIP_NAME_LENGTH] = {0};
    unsigned char temp_checksum = 0;

    UartGetImageinfoVersion(image_info, sizeof(image_info));
    tx_buff.total_length = sizeof(image_info) + 6;
    tx_buff.payload_length = sizeof(image_info) + 2;
    temp_checksum = _UartCalcCheckSum(&tx_buff.payload_length, 3);
    temp_checksum += _UartCalcCheckSum(image_info, sizeof(image_info));

    _UartSendMessage(&tx_buff, 10);
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
    GsWrite((const char *)image_info, sizeof(image_info));
    GsWrite((const char *)&temp_checksum, 1);
#else
    for (int i = 0; i < sizeof(image_info); i++)
        UartSendByte(UART_PORT0, (int)image_info[i]);
    UartSendByte(UART_PORT0, (int) temp_checksum);
#endif
    return 0;
}

char UartSendPrepareBurnBtResult(unsigned char res)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0x00,
        .ocf             = 0xff,
        .ogf             = 0x08,
        .start_byte      = 0x55,
        .payload_length  = 0x00,
        .type_id         = 0x02,
        .command_id      = UCI_NOTIFY_TX_PREPARE_BURN_BT
    };
    unsigned char temp_checksum = 0;

    tx_buff.total_length = sizeof(res) + 6;
    tx_buff.payload_length = sizeof(res) + 2;
    temp_checksum = _UartCalcCheckSum(&tx_buff.payload_length, 3);
    temp_checksum += _UartCalcCheckSum(&res, sizeof(res));

    _UartSendMessage(&tx_buff, 10);
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
    GsWrite((const char*)&res, 1);
    GsWrite((const char*)&temp_checksum, 1);
#else
    UartSendByte(UART_PORT0, (int)res);
    UartSendByte(UART_PORT0, (int) temp_checksum);
#endif
    return 0;
}

char UartSendWav(char *outbuff, unsigned int outbuff_length)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0xff,
        .ocf            = 0xff,
        .ogf            = 0xff,
        .start_byte      = 0x55,
        .payload_length  = 0xff,
        .type_id         = 0x02,
        .command_id      = 0x04
    };
    _UartSendMessage(&tx_buff, 10);
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
    GsWrite((const char*)outbuff, outbuff_length);
#else
    for (int i = 0; i < outbuff_length; i++)
        UartSendByte(UART_PORT0, (int)outbuff[i]);
#endif
    return 0;
}

int UartGetCommandID(void)
{
    return (int)s_command_packet.command_id;
}

int UartGetCommandState(void)
{
    return (int)s_command_packet.command_data[0];
}

unsigned char *UartGetCommandData(void)
{
    return s_command_packet.command_data;
}

unsigned char UartGetCommandDataLength(void)
{
    return (s_command_packet.payload_length - 2);
}

char UartSendCompressedAudio(char *outbuff, char length)
{
    UART_COMMAND_PACKET_TX tx_buff = {
        .magic_number    = {0x42, 0x75, 0x78},
        .total_length    = 0x00,
        .ocf             = 0xff,
        .ogf             = 0x08,
        .start_byte      = 0x55,
        .payload_length  = 0x00,
        .type_id         = 0x02,
        .command_id      = 0x03
    };
    tx_buff.total_length = length + 6;
    tx_buff.payload_length = length + 2;
    char temp_checksum = _UartCalcCheckSum(&tx_buff.payload_length, 3);
    temp_checksum += _UartCalcCheckSum((unsigned char *)outbuff, length);
    _UartSendMessage(&tx_buff, 10);
#ifdef CONFIG_VSP_CF_BT_ENABLE_UART_SEND_OUTPUT_DATA_THROUGH_USB
    GsWrite((const char*)outbuff, length);
    GsWrite((const char*)&temp_checksum, 1);
#else
    for (int i = 0; i < length; i++)
        UartSendByte(UART_PORT0, (int)outbuff[i]);
    UartSendByte(UART_PORT0, (int) temp_checksum);
#endif
    return 0;
}
