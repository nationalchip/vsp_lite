#include <stdio.h>
#include <types.h>
#include <base_addr.h>
#include <board_config.h>
#include <driver/clock.h>
#include <driver/irq.h>
#include <driver/spi.h>
#include <driver/device.h>

#include "spi_master_v3.h"

#define md_debug(fmt, arg...) \
    printf(fmt, ##arg)

#define CONFIG_SPI_SLAVE_NUM_MAX 1

#define START_STATE     ((void *)0)
#define RUNNING_STATE   ((void *)1)
#define DONE_STATE      ((void *)2)
#define ERROR_STATE     ((void *)-1)

#define QUEUE_RUNNING   0
#define QUEUE_STOPPED   1

#define MRST_SPI_DISABLE   0
#define MRST_SPI_ENABLE    1

#define GRUS_GENERAL_SPI_VERSION      0x3130322a
#define LEO_MINI_GENERAL_SPI_VERSION  0x3430322a

/* SPI device running context */
struct device_context {
    struct spi_device *spi;
    uint32_t cr0_solid;
    uint32_t clk_div;        /* baud rate divider */
    uint32_t  sample_delay;
};

static struct device_context spi_device_context_tab[CONFIG_SPI_SLAVE_NUM_MAX];

static void spi_master_set_dws(struct spi_master *master, void *data)
{
    master->driver_data = data;
}
static struct dw_spi* spi_master_get_dws(struct spi_master *master)
{
    return master->driver_data;
}

/* ctldata is for the bus_master driver's runtime state */
static inline void *spi_get_ctldata(struct spi_device *spi)
{
    return spi->controller_state;
}

static inline void spi_set_ctldata(struct spi_device *spi, void *state)
{
    spi->controller_state = state;
}

static void wait_till_tf_empty(struct dw_spi *dws)
{
    while ((dw_readl(dws, SPIM_SR) & SR_TF_EMPT) != SR_TF_EMPT);
}

static void wait_till_not_busy(struct dw_spi *dws)
{
    while ((dw_readl(dws, SPIM_SR) & SR_BUSY) == SR_BUSY);
}

static void flush(struct dw_spi *dws)
{
    while ((dw_readl(dws, SPIM_SR) & SR_RF_NOT_EMPT))
        dw_readl(dws, SPIM_RXDR);

    wait_till_not_busy(dws);
}

static void spi_cs_init(void)
{
}

static void spi_cs_control(struct dw_spi *dws, uint32_t cs, uint8_t flag)
{
    if (cs != 0)
        return;

    switch(flag) {

    case MRST_SPI_DISABLE:
        writel(3, MCU_REG_BASE_SPI1_CS);
        break;
    case MRST_SPI_ENABLE:
        writel(2, MCU_REG_BASE_SPI1_CS);
        break;
    default:
        return;
    }
}

/* Caller already set message->status; dma and pio irqs are blocked */
static void msg_giveback(struct dw_spi *dws)
{
    dws->cur_msg      = NULL;
    dws->cur_transfer = NULL;
}

static int dw_half_duplex_poll_transfer(struct dw_spi *dws)
{
    int inc         = dws->n_bytes;
    int len         = dws->len;
    int steps       = len / inc;
    uint8_t *buffer = (uint8_t*)dws->buffer;
    int size;
#ifndef CONFIG_ENABLE_SPI_INT
    int i, left, items;
    uint8_t  *data8 = (uint8_t*)buffer;
    uint16_t  *data16 = (uint16_t*)buffer;
    uint32_t  *data32 = (uint32_t*)buffer;
    uint32_t rx_len_mask = dws->rx_fifo_len * 2 -1;
#endif
    struct spi_transfer *transfer = dws->cur_transfer;

    /* 长度对齐 */
    if (len % inc)
        return -EINVAL;

    /* 地址对齐 */
    if ((uint32_t)buffer % inc)
        return -EINVAL;

    dws->complete = 0;

    spi_enable_chip(dws, 0);
    dw_writel(dws, SPIM_SER, 1);
    dw_writel(dws, SPIM_DMACR, 0);

    if (transfer->tx_buf) {
        spi_enable_chip(dws, 1);

        while (steps) {
            items = dw_readl(dws, SPIM_TXFLR) & rx_len_mask;
            items = dws->tx_fifo_len - items;
            size = min(items, steps);

            for (i = 0; i < size; i++) {
                if (inc == 1)
                    dw_writel(dws, SPIM_TXDR, data8[i]);
                else if (inc == 2)
                    dw_writel(dws, SPIM_TXDR, data16[i]);
                else if (inc == 4)
                    dw_writel(dws, SPIM_TXDR, data32[i]);
            }

            if (inc == 1)
                data8 += size;
            else if (inc == 2)
                data16 += size;
            else if (inc == 4)
                data32 += size;

            steps -= size;
        }

        /* 等待fifo空 */
        wait_till_tf_empty(dws);
        /* 等待发送完成 */
        wait_till_not_busy(dws);
    } else {
        while (steps != 0) {
            spi_enable_chip(dws, 0);
            size = min(dws->rx_fifo_len, steps);
            dw_writel(dws, SPIM_CTRLR1, size-1);
            spi_enable_chip(dws, 1);
            dw_writel(dws, SPIM_TXDR, 0x00);

            left = size;

            while (left) {
                items = dw_readl(dws, SPIM_RXFLR) & rx_len_mask;

                for (i = 0; i < items; i++) {
                    if (inc == 1) {
                        *data8 = dw_readl(dws, SPIM_TXDR);
                        data8++;
                    } else if (inc == 2) {
                        *data16 = dw_readl(dws, SPIM_TXDR);
                        data16++;
                    } else if (inc == 4) {
                        *data32 = dw_readl(dws, SPIM_TXDR);
                        data32++;
                    }
                }
                left -= items;
            }

            steps -= size;
        }
    }

#ifdef CONFIG_ENABLE_SPI_INT
    dw_writel(dws, SPIM_IMR, SPI_INT_OVERFLOW);
#endif
    spi_enable_chip(dws, 0);

    return 0;
}

static int dw_pump_transfers(struct dw_spi *dws)
{
    struct spi_message  *message;
    struct spi_transfer *transfer;
    struct spi_device   *spi;
    struct device_context *context;
    uint8_t bits;
    uint32_t cr0;

    /* Get current state information */
    message  = dws->cur_msg;
    transfer = dws->cur_transfer;
    spi      = message->spi;
    context  = spi_get_ctldata(spi);

    dws->buffer     = (void*)(transfer->tx_buf ? transfer->tx_buf : transfer->rx_buf);
    dws->len        = transfer->len;

    if (transfer->bits_per_word){
        bits = transfer->bits_per_word;
        bits = roundup(bits, 8);
        bits = (bits == 24) ? 32 : bits;
        dws->n_bytes = bits / 8;
    } else {
        transfer->bits_per_word = 8;
        dws->n_bytes = 1;
    }

    /*
    * Adjust transfer mode if necessary. Requires platform dependent
    * chipselect mechanism.
    */
    cr0 = transfer->tx_buf ?  (SPI_TMOD_TO << SPI_TMOD_OFFSET) : \
            (SPI_TMOD_RO << SPI_TMOD_OFFSET);
    cr0 |= transfer->bits_per_word - 1;
    cr0 |= context->cr0_solid;

    spi_enable_chip(dws, 0);

    dw_writel(dws, SPIM_SAMPLE_DLY, context->sample_delay);
    dw_writel(dws, SPIM_CTRLR0, cr0);
    spi_set_clk(dws, context->clk_div);
    dw_writel(dws, SPIM_CTRLR1, 0);
    dw_writel(dws, SPIM_DMACR,  0);

    return dw_half_duplex_poll_transfer(dws);

    return 0;
}

static int dw_pump_messages(struct dw_spi *dws,  struct spi_message *msg)
{
    int ret = 0;
    struct list_head *cursor;
    struct spi_device   *spi;

    /* Make sure we are not already running a message */
    if (dws->cur_msg)
        return 0;

    dws->cur_msg = msg;

    spi = msg->spi;

    spi_cs_control(dws, spi->chip_select, MRST_SPI_ENABLE);

    list_for_each(cursor, &dws->cur_msg->transfers) {
        dws->cur_transfer = list_entry(cursor, \
                struct spi_transfer, transfer_list);
        ret = dw_pump_transfers(dws);
        if (ret < 0)
            break;
    }
    spi_cs_control(dws, spi->chip_select, MRST_SPI_DISABLE);

    return ret;

}

/* spi_device use this to queue in their spi_msg */
static int dw_spi_quick_transfer(struct spi_device *spi, struct spi_message *msg)
{
    struct dw_spi *dws = spi_master_get_dws(spi->master);
    int ret;

    msg->actual_length = 0;
    msg->spi           = spi;

    ret = dw_pump_messages(dws, msg);
    msg_giveback(dws);

    return ret;
}

/* This may be called twice for each spi dev */
static int dw_spi_setup(struct spi_device *spi)
{
    int i, div;
    struct device_context *spi_device_context;

    spi->mode = 0;
    if (spi->bits_per_word == 0)
        spi->bits_per_word = 8;

    /* try to find aviable context */
    spi_device_context = spi_get_ctldata(spi);
    if (!spi_device_context) {
        for (i = 0; i < CONFIG_SPI_SLAVE_NUM_MAX; i++) {
            if (spi_device_context_tab[i].spi == NULL) {
                spi_device_context = &spi_device_context_tab[i];
            }
        }
    }

    if (spi_device_context == NULL) {
        md_debug("spi error: spi_device context null\n");
        return -ENOMEM;
    }

    if (spi->max_speed_hz == 0)
        spi->max_speed_hz = 10000000;

    unsigned int frequence = CONFIG_GENERAL_SPI_CLK_SRC;

    /* SPI分频为偶数, 适配device最大时钟频率 */
    div = frequence / spi->max_speed_hz;
    div &= ~0x01;

    if ((div * spi->max_speed_hz) != frequence)
        div += 2;

    spi_device_context->spi = spi;
    spi_device_context->cr0_solid = SPI_MST | ((spi->mode << SPI_MODE_OFFSET) \
            & SPI_MODE_MASK) ;
    spi_device_context->clk_div = div;

    spi_device_context->sample_delay = 2;

    spi_set_ctldata(spi, spi_device_context);

    return 0;
}

static void dw_spi_cleanup(struct spi_device *spi)
{
    ClockDisable(CLOCK_SPI_MASTER);
}


/* Restart the controller, disable all interrupts, clean rx fifo */
static void spi_hw_init(struct dw_spi *dws)
{
    ClockEnable(CLOCK_SPI_MASTER);
    spi_enable_chip(dws, 0);
    dw_writel(dws, SPIM_IMR, SPI_INT_OVERFLOW);

    /*
    * Try to detect the FIFO depth if not set by interface driver,
    * the depth could be from 2 to 32 from HW spec
    */
    if (!dws->tx_fifo_len){
        uint32_t fifo;
        for (fifo = 2; fifo <= 257; fifo++){
            dw_writel(dws, SPIM_TXFTLR, fifo);
            if (fifo != dw_readl(dws, SPIM_TXFTLR))
                break;
        }

        dws->tx_fifo_len = (fifo == 257) ? 0 : fifo;
        dw_writel(dws, SPIM_TXFTLR, 0);
    }

    if (!dws->rx_fifo_len){
        uint32_t fifo;
        for (fifo = 2; fifo <= 257; fifo++){
            dw_writel(dws, SPIM_RXFTLR, fifo);
            if (fifo != dw_readl(dws, SPIM_RXFTLR))
                break;
        }

        dws->rx_fifo_len = (fifo == 257) ? 0 : fifo;
        dw_writel(dws, SPIM_TXFTLR, 0);
    }

    spi_cs_init();
    spi_cs_control(dws, 0, MRST_SPI_DISABLE);
    spi_enable_chip(dws, 1);
}

static struct dw_spi     dwspi         = {0};
static struct spi_master dw_spi_master = {0};

static int spi_master_irq_handler(int irq, void *pdata)
{
    struct dw_spi     *dws    = pdata;
    uint32_t int_status = dw_readl(dws, SPIM_ISR);
    if (int_status & SPI_INT_RXFI) {
        md_debug("Receive FIFO full\n");
    }

    if (int_status & SPI_INT_TXEI) {
        md_debug("Transmit FIFO empty\n");
    }

    if (int_status & SPI_INT_TXOI) {
        md_debug("Transmit FIFO overflow\n");
        readl(DW_SPI_TXOICR);
    }
    if (int_status & SPI_INT_RXUI) {
        md_debug("Receive FIFO underflow\n");
    }
    if (int_status & SPI_INT_RXOI) {
        md_debug("Receive FIFO overflow\n");
        readl(DW_SPI_RXOICR);
    }
    if (int_status & SPI_INT_MSTI) {
        md_debug("multi-master contention\n");
    }
    if (int_status & SPI_INT_XRXOI) {
        md_debug("xip receive fifo overflow\n");
    }

    return 0;
}

void spi_master_v3_probe(void)
{
    struct spi_master *master = &dw_spi_master;
    struct dw_spi     *dws    = NULL;
    int    ret  =     -ENOMEM;

    spi_master_set_dws(master, &dwspi);
    dws            = spi_master_get_dws(master);
    dws->regs      = MCU_REG_BASE_SPI1;                          // regs addr
    dws->master    = master;

    master->bus_num        = CONFIG_GENERAL_SPI_BUS_SN;
    master->num_chipselect = CONFIG_SPI_SLAVE_NUM_MAX;
    master->cleanup        = dw_spi_cleanup;
    master->setup          = dw_spi_setup;
    master->transfer       = dw_spi_quick_transfer;                 // quick_transfer

    spi_hw_init(dws);
    flush(dws);

    //修改状态, 队列启动
    dws->cur_msg      = NULL;
    dws->cur_transfer = NULL;

    ret = spi_register_master(master);
    if (ret) {
        md_debug("problem registering spi master\n");
    }

    gx_request_irq(MCU_IRQ_GENERAL_SPI, spi_master_irq_handler, dws);
}

