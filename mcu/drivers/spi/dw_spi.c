/*****************************************
  Copyright (c) 2003-2019
  Nationalchip Science & Technology Co., Ltd. All Rights Reserved
  Proprietary and Confidential
 *****************************************/

#include <board_config.h>
#include <driver/spi_device.h>
#include <driver/irq.h>
#include "dw_spi.h"

static spi_xfer_t spi_priv = {
	.regs     = SPI_BASE_ADDR,
	.cs_regs  = (volatile uint32_t *)SPI_CS_ADDR,
	.tx_fifo_len = -1,
	.rx_fifo_len = -1,
};

#define SPI_DMA_CHANNEL 0

#define cpu_addr_to_dma(_addr) ({\
	uint32_t __addr = (uint32_t)(_addr);\
	__addr - (__addr >= 0xa0000000 ? 0xa0000000 : 0x40000000);\
})

static struct dw_dmac_priv spl_priv = { .temp = ~0, };

static inline void dw_chan_clean(int ch)
{
	spl_priv.dw_dmac->CLEAR.XFER[0] |=   0x01  << ch;
	spl_priv.dw_dmac->RAW.XFER[0]   &= ~(0x01  << ch);
	spl_priv.dw_dmac->MASK.XFER[0]  |=   0x101 << ch;
}

static uint32_t cfg_ctrl[] ={ /*  MEM 00   SR 01   SPI 10 */
	[0x00] = DWC_ME2ME_CTRL0,     // MEM  -> MEM
	[0x01] = DWC_ME2SR_CTRL0,     // MEM  -> SRAM
	[0x02] = DWC_ME2SP_CTRL0,     // MEM  -> SPI
	[0x04] = DWC_SR2ME_CTRL0,     // SRAM -> MEM
	[0x06] = DWC_SR2SP_CTRL0,     // SRAM -> SPI
	[0x08] = DWC_SP2ME_CTRL0,     // SPI  -> MEM
	[0x09] = DWC_SP2SR_CTRL0,     // SPI  -> SRAM
};

static uint32_t get_dma_ctrl(const void *src, void *dest)
{
	uint32_t _SRC = (uint32_t)src;
	uint32_t _DST = (uint32_t)dest;
	int32_t  index;

	index  = (_SRC < 0xa0000000 ? 0 : _SRC < 0xa0110000 ? 1 : 2) << 2;
	index |= (_DST < 0xa0000000 ? 0 : _DST < 0xa0110000 ? 1 : 2);

	return cfg_ctrl[index];
}

static void dw_dmac_cfg(void *dst, const void *src, uint32_t len, bool is_rx)
{
	dw_dma_regs_t  *dma = spl_priv.dw_dmac;
	u32 tx_ctl, rx_ctl;

	spl_priv.dw_dmac->CFG[0] = 0;
	dw_chan_clean(SPI_DMA_CHANNEL);

	if(is_rx){
		rx_ctl = DWC_CTLL_DST_INC | DWC_CTLL_SRC_FIX;

		/* config for rx */
		dma->CHAN[SPI_DMA_CHANNEL].SAR[0] = cpu_addr_to_dma(src);
		dma->CHAN[SPI_DMA_CHANNEL].DAR[0] = cpu_addr_to_dma(dst);
		dma->CHAN[SPI_DMA_CHANNEL].CTL[0] = get_dma_ctrl(src, dst) | rx_ctl;
		dma->CHAN[SPI_DMA_CHANNEL].CTL[1] = len;
		dma->CHAN[SPI_DMA_CHANNEL].LLP[0] = 0;
	} else {
		tx_ctl = DWC_CTLL_DST_FIX | DWC_CTLL_SRC_INC;

		/* config for tx */
		dma->CHAN[SPI_DMA_CHANNEL].SAR[0] = cpu_addr_to_dma(src);
		dma->CHAN[SPI_DMA_CHANNEL].DAR[0] = cpu_addr_to_dma(dst);
		dma->CHAN[SPI_DMA_CHANNEL].CTL[0] = get_dma_ctrl(src, dst) | tx_ctl;
		dma->CHAN[SPI_DMA_CHANNEL].CTL[1] = len >> 2;
		dma->CHAN[SPI_DMA_CHANNEL].LLP[0] = 0;
	}
}

/* 发送最多BLOCK_TS个字节数据 */
static inline int simple_dma_send_block(uint8_t *dest, const uint8_t *src, uint32_t len)
{
	dw_writel(spi_priv.regs, SPIM_ENR, 0);
	dw_writel(spi_priv.regs, SPIM_CTRLR1,  len-1);

	dw_dmac_cfg(dest, src, len, 0);

	/* enable spi */
	dw_writel(spi_priv.regs, SPIM_ENR, 1);

	/* enable dma */
	spl_priv.dw_dmac->CFG[0]   = 1;
	spl_priv.dw_dmac->CH_EN[0] = 0x101;

	/* wait dma done */
	while((spl_priv.dw_dmac->RAW.XFER[0] & (0X01 << SPI_DMA_CHANNEL)) == 0);

	/* disable dma */
	spl_priv.dw_dmac->CFG[0]   = 0;
	spl_priv.dw_dmac->CH_EN[0] = 0;

	/* wait spi tx fifo empty */
	while(dw_readl(spi_priv.regs, SPIM_TXFLR) !=  0);

	/* wait spi not busy */
	while(dw_readl(spi_priv.regs, SPIM_SR) & 0x01);

	return 0;
}

static int simple_dma_send(uint8_t *dest, const uint8_t *src, uint32_t len)
{
	uint32_t left = 0;
	uint32_t size;
	const uint8_t *src_pos;

	src_pos = src;
	left = len;

	dw_writel(spi_priv.regs, SPIM_ENR, 0);
	dw_writel(spi_priv.regs, SPIM_DMACR,   0x02);
	dw_writel(spi_priv.regs, SPIM_SER,     1);
	dw_writel(spi_priv.regs, SPIM_CTRLR0,
		(SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_TO << SPI_TMOD_OFFSET));

	spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[1] = 0x02;
	spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[0] = 0;

	while (0 != left) {
		size = min(BLOCK_TS, left);
		simple_dma_send_block(dest, src_pos, size);
		left -= size;
		src_pos += size;
	}

	dw_writel(spi_priv.regs, SPIM_DMACR, 0);

	return 0;
}

/* 接收最多BLOCK_TS个字节数据 */
static inline int simple_dma_recv_block(uint8_t *dest, uint8_t *src, uint32_t len)
{
	dw_writel(spi_priv.regs, SPIM_ENR, 0);
	dw_writel(spi_priv.regs, SPIM_CTRLR1,  len-1);

	dw_dmac_cfg(dest, src, len, 1);

	/* enable spi */
	dw_writel(spi_priv.regs, SPIM_ENR, 1);

	/* enable dma */
	spl_priv.dw_dmac->CFG[0]   = 1;
	spl_priv.dw_dmac->CH_EN[0] = 0x101;

	/* spi start receive */
	dw_writel(spi_priv.regs, SPIM_TXDR, 0);

	/* wait dma done */
	while((spl_priv.dw_dmac->RAW.XFER[0] & (0X01 << SPI_DMA_CHANNEL)) == 0);

	/* disable dma */
	spl_priv.dw_dmac->CFG[0]   = 0;
	spl_priv.dw_dmac->CH_EN[0] = 0;

	/* wait spi rx fifo empty */
	while(dw_readl(spi_priv.regs, SPIM_RXFLR) !=  0);

	/* wait spi not busy */
	while(dw_readl(spi_priv.regs, SPIM_SR) & 0x01);

	return 0;
}

static int simple_dma_recv(uint8_t *dest, uint8_t *src, uint32_t len)
{
	uint32_t left = 0;
	uint32_t size;
	uint8_t *dst_pos;

	dst_pos = dest;
	left = len;
	dw_writel(spi_priv.regs, SPIM_ENR, 0);
	dw_writel(spi_priv.regs, SPIM_DMACR,   0x01);
	dw_writel(spi_priv.regs, SPIM_SER,     1);
	dw_writel(spi_priv.regs, SPIM_CTRLR0,
		(SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_RO << SPI_TMOD_OFFSET));

	spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[1] = 0x882;
	spl_priv.dw_dmac->CHAN[SPI_DMA_CHANNEL].CFG[0] = 0;
	while (0 != left) {
		size = min(BLOCK_TS, left);
		simple_dma_recv_block(dst_pos, src, size);
		left -= size;
		dst_pos += size;
	}

	dw_writel(spi_priv.regs, SPIM_DMACR, 0);

	return 0;
}

static inline int poll_xfer(const uint8_t *tx, uint8_t *rx, uint32_t len)
{
	uint32_t i, size;
	dw_writel(spi_priv.regs, SPIM_ENR,   0);
	dw_writel(spi_priv.regs, SPIM_SER,     1);
	dw_writel(spi_priv.regs, SPIM_DMACR, 0);

	if (tx) {
		dw_writel(spi_priv.regs, SPIM_CTRLR0,
			(SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_TO  << SPI_TMOD_OFFSET));
		dw_writel(spi_priv.regs, SPIM_ENR,   1);

		for (i = 0; i < len; i++) {
			while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_TF_NOT_FULL) == 0);
			dw_writel(spi_priv.regs, SPIM_TXDR, tx[i]);
		}

		while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_TF_EMPT) == 0); // 等待发送fifo 为空
		while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_BUSY) != 0);    // 等待发送busy位置0
	} else {
		dw_writel(spi_priv.regs, SPIM_CTRLR0,
			(SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_RO  << SPI_TMOD_OFFSET));

		while (len != 0) {
			dw_writel(spi_priv.regs, SPIM_ENR,   0);
			size = min_t(uint32_t, spi_priv.rx_fifo_len, len);
			dw_writel(spi_priv.regs, SPIM_CTRLR1,   size-1);
			dw_writel(spi_priv.regs, SPIM_ENR,   1);
			dw_writel(spi_priv.regs, SPIM_TXDR, 0);

			for (i = 0; i < size; i++) {
				while ((dw_readl(spi_priv.regs, SPIM_SR) & SR_RF_NOT_EMPT) == 0);
				rx[i] = dw_readl(spi_priv.regs, SPIM_TXDR);
			}
			rx += size;
			len -= size;
		}
	}

	return 0;
}

static int setup(const struct spi_device* spi_device)
{
	dw_writel(spi_priv.regs, SPIM_ENR, 0);
	dw_writel(spi_priv.regs, SPIM_CTRLR0, spi_device->priv.ctrlr0);
	dw_writel(spi_priv.regs, SPIM_BAUDR, spi_device->priv.baudr);
	dw_writel(spi_priv.regs, SPIM_RX_SAMPLE_DLY, spi_device->sample_dly);
	return 0;
}

int spi_xfer(const struct spi_device* spi_device, const uint8_t *tx, uint8_t *rx, uint32_t len)
{
	setup(spi_device);

	u32 tmp_len = 0;
	int offset;

	if((spi_device->mode & SPI_DMA) && len >= 64){
		if(rx){
			offset = (uint32_t)rx % 4;
			if (offset) {
				tmp_len = 4 - offset;
				poll_xfer(tx, rx, tmp_len);
				rx += tmp_len;
				len -= tmp_len;
			}
			tmp_len = len & ~0x03;
			simple_dma_recv(rx, (u8*)(spi_priv.regs + SPIM_RXDR), tmp_len);
		}else if(tx) {
			offset = (uint32_t)tx % 4;
			if (offset) {
				tmp_len = 4 - offset;
				poll_xfer(tx, rx, tmp_len);
				tx += tmp_len;
				len -= tmp_len;
			}
			tmp_len = len & ~0x03;
			simple_dma_send((u8*)(spi_priv.regs + SPIM_TXDR), tx, tmp_len);
		}

		if(len != tmp_len){
			len -= tmp_len;
			tx += tx ? tmp_len : 0;
			rx += rx ? tmp_len : 0;
		}else{
			goto out;
		}
	}

	poll_xfer(tx, rx, len);

out:
	return 0;
}

int spi_write_then_read(const struct spi_device* spi_device,
		const uint8_t *txbuf, unsigned int n_tx,
		void *rxbuf, unsigned int n_rx)
{
	spi_device->set_cs(CS_ENABLE);

	if (n_tx)
		spi_xfer(spi_device, txbuf, NULL, n_tx);
	if (n_rx)
		spi_xfer(spi_device, NULL, rxbuf, n_rx);

	spi_device->set_cs(CS_DISABLE);

	return 0;
}

static int spi_irq_handler(int irq, void *pdata)
{
	uint32_t int_status = dw_readl(spi_priv.regs, SPIM_ISR);
	printf("int status = %02x\n", int_status);
	if (int_status & 0x01)
	{
		printf("Transmit FIFO empty interrupt.\n");
	}
	if (int_status & 0x02) {
		printf("Transmit FIFO overflow interrupt.\n");
		dw_readl(spi_priv.regs, DW_SPI_TXOICR);
	}
	if (int_status & 0x04)
		printf("Receive FIFO underflow interrupt.\n");
	if (int_status & 0x08) {
		printf("Receive FIFO overflow interrupt.\n");
		dw_readl(spi_priv.regs, DW_SPI_RXOICR);
	}
	if (int_status & 0x10)
		printf("Receive FIFO full interrupt.\n");
	if (int_status & 0x20)
		printf("multi-master contention interrupt.\n");

	return 0;
}

int spi_device_init(struct spi_device* spi_device)
{
	uint32_t tmp;
	uint32_t div = CONFIG_DESIGNWARE_SPI_CLK / spi_device->freq;

	div = div < 2 ? 2 : div;
	spi_device->priv.baudr= roundup(div, 2);

	dw_writel(spi_priv.regs, SPIM_ENR, 0);

	tmp = spi_device->mode & (SPI_PHASE | SPI_SCPOL);
	spi_device->priv.ctrlr0 = (SPI_DFS_8BIT << SPI_DFS_OFFSET) | (SPI_TMOD_TR  << SPI_TMOD_OFFSET) | tmp;

	gx_request_irq(MCU_IRQ_DW_SPI, spi_irq_handler, (void*)MCU_IRQ_DW_SPI);

	dw_writel(spi_priv.regs, SPIM_IMR, 0x0e);
	dw_writel(spi_priv.regs, SPIM_CTRLR1,  0);

	if (spi_priv.tx_fifo_len < 0) {
		uint32_t fifo;
		for (fifo = 1; fifo < 256; fifo++) {
			dw_writel(spi_priv.regs, SPIM_TXFTLR, fifo);
			if (fifo != dw_readl(spi_priv.regs, SPIM_TXFTLR))
				break;
		}
		spi_priv.tx_fifo_len = (fifo == 1) ? 0 : fifo;
		dw_writel(spi_priv.regs, SPIM_TXFTLR, 0);
	}

	if (spi_priv.rx_fifo_len < 0) {
		uint32_t fifo;
		for (fifo = 1; fifo < 256; fifo++) {
			dw_writel(spi_priv.regs, SPIM_RXFTLR, fifo);
			if (fifo != dw_readl(spi_priv.regs, SPIM_RXFTLR))
				break;
		}
		spi_priv.rx_fifo_len = (fifo == 1) ? 0 : fifo;
		dw_writel(spi_priv.regs, SPIM_RXFTLR, spi_priv.rx_fifo_len-1);
	}

	dw_writel(spi_priv.regs, SPIM_SER, 1);

	spl_priv.dw_dmac = (dw_dma_regs_t*)GXSCPU_DMA_BASE;

	dw_writel(spi_priv.regs, SPIM_DMATDLR, 0x04);
	dw_writel(spi_priv.regs, SPIM_DMARDLR, 0x07);

	dw_writel(spi_priv.regs, SPIM_ENR, 0);

	return 0;
}

