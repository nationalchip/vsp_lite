/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * dw_uart.c MCU DESIGNWARE UART driver
 *
 */
#include <common.h>
#include <base_addr.h>
#include <board_config.h>
#include <driver/dma_axi.h>
#include <driver/uart.h>
#include <driver/irq.h>

#include "dw_uart.h"

#define UART_PORT_MAX 2

#define UART_PORT0_BASE    MCU_REG_BASE_UART0
#define UART_PORT1_BASE    MCU_REG_BASE_UART1
#define UART_PORT0_IRQ_NUM MCU_IRQ_DW_UART0
#define UART_PORT1_IRQ_NUM MCU_IRQ_DW_UART1

static DW_UART uart_config[UART_PORT_MAX] = {
    {
        .port = UART_PORT0,
        .regs = (void *)UART_PORT0_BASE,
        .pclk = CONFIG_DW_UART0_CLK,
        .baudrate = CONFIG_SERIAL_BAUD_RATE,
        .data_bits = 8,
        .stop_bits = 1,
        .flow = 0,
        .parity = 0,
        .irq_num = UART_PORT0_IRQ_NUM,
    }, {
        .port = UART_PORT1,
        .regs = (void *)UART_PORT1_BASE,
        .pclk = CONFIG_DW_UART1_CLK,
        .baudrate = CONFIG_SERIAL_BAUD_RATE,
        .data_bits = 8,
        .stop_bits = 1,
        .flow = 0,
        .parity = 0,
        .irq_num = UART_PORT1_IRQ_NUM,
    },
};

static inline void DwUartWritel(DW_UART *dev, int reg, uint32_t val)
{
    writel(val, (uint32_t)dev->regs + reg);
}

static inline uint32_t DwUartReadl(DW_UART *dev, int reg)
{
    return readl((uint32_t)dev->regs + reg);
}

static int DwUartCalcDivisor(DW_UART *dev)
{
    const uint32_t mode_div = 16;

    if (!dev)
        return -1;

    uint32_t clock    = dev->pclk;
    uint32_t baudrate = dev->baudrate;

#ifdef CONFIG_GX8008B
    dev->div = clock / (mode_div * baudrate);
    dev->dlf = (uint32_t)( ((clock % (mode_div * baudrate) * 1.0)) / (mode_div * baudrate) * (1 << UART_DLF_SIZE) );
#else
    dev->div = (clock + mode_div * baudrate / 2) / (mode_div * baudrate);
#endif

    return 0;
}

static void DwUartEnableInterrupt(DW_UART *dev, int en)
{
    DwUartWritel(dev, DW_UART_IER, en);
}

static void DwUartSetLineControl(DW_UART *dev, int cr)
{
    DwUartWritel(dev, DW_UART_FCR, 0);
    uint32_t value = DwUartReadl(dev, DW_UART_LCR);
    value &= ~0x1F;
    value |= cr;
    DwUartWritel(dev, DW_UART_LCR, value);
    DwUartWritel(dev, DW_UART_FCR, dev->flow ? UART_FCRVAL_AFCE : UART_FCRVAL);
}

static void DwUartSetModemCtrl(DW_UART *dev, int mode)
{
    DwUartWritel(dev, DW_UART_MCR, mode);
}

static void DwUartSetBrg(DW_UART *dev)
{
    int baud_divisor = dev->div;

    DwUartWritel(dev, DW_UART_FCR, 0); /* Disable & reset fifo*/
    uint32_t value = DwUartReadl(dev, DW_UART_LCR);
    DwUartWritel(dev, DW_UART_LCR, value | UART_LCR_DLAB);
    DwUartWritel(dev, DW_UART_DLL, baud_divisor & 0xFF);
    DwUartWritel(dev, DW_UART_DLH, (baud_divisor >> 8) & 0xFF);

#ifdef CONFIG_GX8008B
    DwUartWritel(dev, DW_UART_DLF, dev->dlf & 0xff);
#endif

    DwUartWritel(dev, DW_UART_LCR, value);
    DwUartWritel(dev, DW_UART_FCR, dev->flow ? UART_FCRVAL_AFCE : UART_FCRVAL);
}

static uint32_t DwUartGetRcvFifoLevel(DW_UART *dev)
{
    return DwUartReadl(dev, DW_UART_RFL);
}

static int DwUartGetFreeTxFifoLen(DW_UART *dev)
{
    uint32_t fifo_len = DwUartReadl(dev, DW_UART_TFL);
    uint32_t uart_cpr = DwUartReadl(dev, DW_UART_CPR);
    uint32_t fifo_mode = (uart_cpr >> UART_CPR_FIFO_MODE_SHIFT) & UART_CPR_FIFO_MODE_MASK;

    return fifo_mode * 16 - fifo_len;
}

static void DwUartSetFcrRxThres(DW_UART *dev, uint32_t rx_thres)
{
    uint32_t fcr = DwUartReadl(dev, DW_UART_FCR);
    fcr &= ~UART_FCR_RX_TRIGGER_MASK;
    fcr |= rx_thres;
    DwUartWritel(dev, DW_UART_FCR, fcr);
}

static void DwUartSetFcrTxThres(DW_UART *dev, uint32_t tx_thres)
{
    uint32_t fcr = DwUartReadl(dev, DW_UART_FCR);
    fcr &= ~UART_FCR_TX_TRIGGER_MASK;
    fcr |= tx_thres;
    DwUartWritel(dev, DW_UART_FCR, fcr);
}

static void DwUartTxFlush(DW_UART *dev)
{
    /* Wait transmit fifo empty */
    while ((DwUartReadl(dev, DW_UART_LSR) & UART_LSR_TEMT) == 0)
        continue;
}

static void DwUartRxFlush(DW_UART *dev)
{
    uint32_t recv_level = DwUartGetRcvFifoLevel(dev);

    for (int i = 0; i < recv_level; i++)
        DwUartReadl(dev, DW_UART_RBR);
}

static inline uint32_t DwUartGetIntStatus(DW_UART *dev)
{
    return  DwUartReadl(dev, DW_UART_IIR) & 0x0F;
}

static void DwUartDmaTransferCallback(void *priv)
{
    DW_UART *dev = priv;

    ((UART_SEND_DONE_CALLBACK)(dev->send_done_callback))(dev->port, dev->send_done_priv);
}

static int DwUartDmaTransfer(DW_UART *dev, const uint8_t *buf, int len)
{
    DW_DMAC_CH_CONFIG dma_config;
    uint32_t dst = (uint32_t)dev->regs + DW_UART_THR;

    int dma_channel = dw_dma_select_channel();
    if (dma_channel < 0)
        return -1;

    /* dma source config */
    dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_INC;
    dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;
    dma_config.src_master_select = AXI_MASTER_2;
    dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
    dma_config.src_per = 0;
    dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

    /* dma destination config */
    dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
    dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;
    dma_config.dst_master_select = AXI_MASTER_1;
    dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
    if (dev->port == 0)
        dma_config.dst_per = 0x07;
    else
        dma_config.dst_per = 0x05;
    dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

    dma_config.flow_ctrl = DWAXIDMAC_TT_FC_MEM_TO_PER_DMAC;

    dw_dma_channel_config(dma_channel, dma_config);
    dw_dma_register_complete_callback(dma_channel, DwUartDmaTransferCallback, dev);

    dw_dma_xfer_int((void *)dst, (void *)buf, len, dma_channel);

    return 0;
}

static void DwUartDmaReceiveCallback(void *priv)
{
    DW_UART *dev = priv;

    ((UART_RECV_DONE_CALLBACK)(dev->recv_done_callback))(dev->port, dev->recv_done_priv);
}

static int DwUartDmaReceive(DW_UART *dev, uint8_t *buf, int len)
{
    DW_DMAC_CH_CONFIG dma_config;
    uint32_t src = (uint32_t)dev->regs + DW_UART_RBR;

    int dma_channel = dw_dma_select_channel();
    if (dma_channel < 0)
        return -1;

    /* dma source config */
    dma_config.src_addr_update = DWAXIDMAC_CH_CTL_L_NOINC;
    dma_config.src_hs_select = DWAXIDMAC_HS_SEL_HW;
    dma_config.src_master_select = AXI_MASTER_1;
    dma_config.src_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
    if (dev->port == 0)
        dma_config.src_per = 0x06;
    else
        dma_config.src_per = 0x04;
    dma_config.src_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

    /* dma destination config */
    dma_config.dst_addr_update = DWAXIDMAC_CH_CTL_L_INC;
    dma_config.dst_hs_select = DWAXIDMAC_HS_SEL_HW;
    dma_config.dst_master_select = AXI_MASTER_2;
    dma_config.dst_msize = DWAXIDMAC_BURST_TRANS_LEN_32;
    dma_config.dst_per = 0;
    dma_config.dst_trans_width = DWAXIDMAC_TRANS_WIDTH_8;

    dma_config.flow_ctrl = DWAXIDMAC_TT_FC_PER_TO_MEM_DMAC;

    dw_dma_channel_config(dma_channel, dma_config);
    dw_dma_register_complete_callback(dma_channel, DwUartDmaReceiveCallback, dev);

    dw_dma_xfer_int((void *)buf, (void *)src, len, dma_channel);

    return 0;
}

static void DwUartPutChar(DW_UART *dev, char c)
{
    /* Wait transmit fifo idle */
    while ((DwUartReadl(dev, DW_UART_LSR) & UART_LSR_THRE) == 0)
        continue;
    DwUartWritel(dev, DW_UART_THR, c);
}

static char DwUartGetChar(DW_UART *dev)
{
    /* Wait data ready */
    while ((DwUartReadl(dev, DW_UART_LSR) & UART_LSR_DR) == 0)
        continue;
    return DwUartReadl(dev, DW_UART_RBR);
}

static int DwUartTstc(DW_UART *dev)
{
    return (DwUartReadl(dev, DW_UART_LSR) & UART_LSR_DR) != 0;
}

static int DwUartSendBuffer(DW_UART *dev, unsigned char *buffer, int length)
{
    for (int i = 0; i < length; i++)
        DwUartWritel(dev, DW_UART_THR, buffer[i]);

    return length;
}

static int DwUartRecvBuffer(DW_UART *dev, unsigned char *buffer, int length)
{
    for (int i = 0; i < length; i++)
        buffer[i] = DwUartReadl(dev, DW_UART_RBR);

    return length;
}

static int DwUartIsr(int irq, void *pdata)
{
    DW_UART *dev = pdata;
    uint32_t int_status = DwUartGetIntStatus(dev);

    /* Receive interrupt */
    if (int_status & UART_IIR_RDI) {
        uint32_t can_recv_length = DwUartGetRcvFifoLevel(dev);
        if (dev->async_mode == UART_ASYNC_MODE_INTERRUPT) {
            ((UART_CAN_RECV_CALLBACK)dev->can_recv_callback)(dev->port, can_recv_length, dev->can_recv_priv);
        } else if (dev->async_mode == UART_ASYNC_MODE_DMA) {
            uint32_t recv_length = can_recv_length < dev->recv_length ? can_recv_length : dev->recv_length;
            DwUartRecvBuffer(dev, dev->recv_buffer, recv_length);
            dev->recv_buffer += recv_length;
            dev->recv_length -= recv_length;
            if (dev->recv_length == 0) {
                uint32_t ier = DwUartReadl(dev, DW_UART_IER);
                ier &= ~UART_IER_RDI;
                DwUartEnableInterrupt(dev, ier);
                ((UART_RECV_DONE_CALLBACK)(dev->recv_done_callback))(dev->port, dev->recv_done_priv);
            }
        }
    }

    /* Send interrupt */
    if (int_status & UART_IIR_THRI) {
        uint32_t can_send_length = DwUartGetFreeTxFifoLen(dev);
        if (dev->async_mode == UART_ASYNC_MODE_INTERRUPT) {
            ((UART_CAN_SEND_CALLBACK)dev->can_send_callback)(dev->port, can_send_length, dev->can_send_priv);
        } else if (dev->async_mode == UART_ASYNC_MODE_DMA) {
            uint32_t send_length = can_send_length < dev->send_length ? can_send_length : dev->send_length;
            DwUartSendBuffer(dev, dev->send_buffer, send_length);
            dev->send_buffer += send_length;
            dev->send_length -= send_length;
            if (dev->send_length == 0) {
                DwUartTxFlush(dev);
                uint32_t ier = DwUartReadl(dev, DW_UART_IER);
                ier &= ~UART_IER_THRI;
                DwUartEnableInterrupt(dev, ier);
                ((UART_SEND_DONE_CALLBACK)(dev->send_done_callback))(dev->port, dev->send_done_priv);
            }
        }
    }

    return 0;
}

static int DwUartInit(DW_UART *dev)
{
    if(!dev->data_bits)
        dev->data_bits = 8;
    if(!dev->stop_bits)
        dev->stop_bits = 1;

    DwUartEnableInterrupt(dev, 0); /* Disable all interrupt */
    DwUartSetModemCtrl(dev, dev->flow ? UART_FLOWCTRL_AUTO : UART_FLOWCTRL_NONE);
    /* Reset and enable fifo */
    DwUartWritel(dev, DW_UART_FCR, dev->flow ? UART_FCRVAL_AFCE : UART_FCRVAL);
    if (dev->baudrate > 0) {
        if (DwUartCalcDivisor(dev))
            return -1;
        DwUartSetBrg(dev);
    }
    /* Default line control set n 8 1 */
    DwUartSetLineControl(dev, LCR_DATA_8BIT | LCR_STOP_1BIT | LCR_PARITY_NONE);
#ifdef CONFIG_GX8008B
    dev->use_dma = 1;
#else
    dev->use_dma = 0;
#endif
    gx_request_irq(dev->irq_num, DwUartIsr, dev);

    return 0;
}

int UartInit(int port, unsigned int baudrate)
{
    if (port >= UART_PORT_MAX || port < 0)
        return -1;

    uart_config[port].baudrate = baudrate;

    return DwUartInit(&uart_config[port]);
}

int UartDone(int port)
{
    return 0;
}

int UartSetBaudrate(int port, unsigned int baudrate)
{
    return UartInit(port, baudrate);
}

int UartSetSendFIFOThreshold(int port, UART_FIFO_SEND_THRESHOLD threshold)
{
    DW_UART *dev = &uart_config[port];

    switch (threshold) {
        case UART_FIFO_SEND_THRESHOLD_EMPTY:
            DwUartSetFcrTxThres(dev, UART_FCR_TX_TRIGGER_EMPTY);
            break;
        case UART_FIFO_SEND_THRESHOLD_2:
            DwUartSetFcrTxThres(dev, UART_FCR_TX_TRIGGER_2);
            break;
        case UART_FIFO_SEND_THRESHOLD_QUARTER:
            DwUartSetFcrTxThres(dev, UART_FCR_TX_TRIGGER_QUARTER);
            break;
        case UART_FIFO_SEND_THRESHOLD_HALF:
            DwUartSetFcrTxThres(dev, UART_FCR_TX_TRIGGER_HALF);
            break;
        default:
            return -1;
    }

    return 0;
}

int UartSetRecvFIFOThreshold(int port, UART_FIFO_RECV_THRESHOLD threshold)
{
    DW_UART *dev = &uart_config[port];

    switch (threshold) {
        case UART_FIFO_RECV_THRESHOLD_ONE_BYTE:
            DwUartSetFcrRxThres(dev, UART_FCR_RX_TRIGGER_1);
            break;
        case UART_FIFO_RECV_THRESHOLD_QUARTER:
            DwUartSetFcrRxThres(dev, UART_FCR_RX_TRIGGER_QUARTER);
            break;
        case UART_FIFO_RECV_THRESHOLD_HALF:
            DwUartSetFcrRxThres(dev, UART_FCR_RX_TRIGGER_HALF);
            break;
        case UART_FIFO_RECV_THRESHOLD_FULL:
            DwUartSetFcrRxThres(dev, UART_FCR_RX_TRIGGER_FULL);
            break;
        default:
            return -1;
    }

    return 0;
}

void UartFlushSendFIFO(int port)
{
    DwUartTxFlush(&uart_config[port]);
}

void UartFlushRecvFIFO(int port)
{
    DwUartRxFlush(&uart_config[port]);
}

void UartSendByte(int port, unsigned char byte)
{
    DwUartPutChar(&uart_config[port], byte);
}

void UartSyncSendBuffer(int port, unsigned char *buffer, unsigned int length)
{
    for (int i = 0; i< length; i++) {
        DwUartPutChar(&uart_config[port], buffer[i]);
    }
}

int UartTrySendByte(int port, unsigned char byte)
{
    DW_UART *dev = &uart_config[port];

    if ((DwUartReadl(dev, DW_UART_LSR) & UART_LSR_THRE) == 0)
        return -1;

    DwUartWritel(dev, DW_UART_THR, byte);

    return 0;
}

unsigned char UartRecvByte(int port)
{
    return DwUartGetChar(&uart_config[port]);
}

unsigned int UartSyncRecvBuffer(int port, unsigned char *buffer, unsigned int length)
{
    for (int i = 0; i< length; i++) {
        buffer[i] = DwUartGetChar(&uart_config[port]);
    }

    return length;
}

int UartTryRecvByte(int port, unsigned char *byte)
{
    DW_UART *dev = &uart_config[port];

    if ((DwUartReadl(dev, DW_UART_LSR) & UART_LSR_DR) == 0)
        return -1;

    *byte = DwUartReadl(dev, DW_UART_RBR) & 0xFF;

    return 0;
}

int UartCanRecvByte(int port)
{
    return DwUartTstc(&uart_config[port]);
}

int UartStartAsyncSend(int port, UART_CAN_SEND_CALLBACK callback, void *priv)
{
    DW_UART *dev = &uart_config[port];

    if (!callback)
        return -1;

    dev->async_mode = UART_ASYNC_MODE_INTERRUPT;
    dev->can_send_callback = callback;
    dev->can_send_priv = priv;

    uint32_t ier = DwUartReadl(dev, DW_UART_IER);
    ier |= UART_IER_THRI;
    DwUartEnableInterrupt(dev, ier);

    return 0;
}

int UartStopAsyncSend(int port)
{
    DW_UART *dev = &uart_config[port];

    dev->can_send_callback= NULL;
    dev->can_send_priv = NULL;

    uint32_t ier = DwUartReadl(dev, DW_UART_IER);
    ier &= ~UART_IER_THRI;
    DwUartEnableInterrupt(dev, ier);

    return 0;
}

int UartStartAsyncRecv(int port, UART_CAN_RECV_CALLBACK callback, void *priv)
{
    DW_UART *dev = &uart_config[port];

    if (!callback)
        return -1;

    dev->async_mode = UART_ASYNC_MODE_INTERRUPT;
    dev->can_recv_callback = callback;
    dev->can_recv_priv = priv;

    uint32_t ier = DwUartReadl(dev, DW_UART_IER);
    ier|= UART_IER_RDI;
    DwUartEnableInterrupt(dev, ier);

    return 0;
}

int UartStopAsyncRecv(int port)
{
    DW_UART *dev = &uart_config[port];

    dev->can_recv_callback= NULL;
    dev->can_recv_priv = NULL;

    uint32_t ier = DwUartReadl(dev, DW_UART_IER);
    ier &= ~UART_IER_RDI;
    DwUartEnableInterrupt(dev, ier);

    return 0;
}

int UartSendBuffer(int port, unsigned char *buffer, int length)
{
    return DwUartSendBuffer(&uart_config[port], buffer, length);
}

int UartRecvBuffer(int port, unsigned char *buffer, int length)
{
    return DwUartRecvBuffer(&uart_config[port], buffer, length);
}

int UartAsyncSendBuffer(int port, unsigned char *buffer, int length, UART_SEND_DONE_CALLBACK callback, void *priv)
{
    int ret = 0;
    DW_UART *dev = &uart_config[port];

    if (!buffer || !callback)
        return -1;

    dev->async_mode = UART_ASYNC_MODE_DMA;
    dev->send_buffer = buffer;
    dev->send_length = length;
    dev->send_done_callback = callback;
    dev->send_done_priv = priv;

    if (dev->use_dma) {
        ret = DwUartDmaTransfer(dev, buffer, length);
    } else {
        uint32_t ier = DwUartReadl(dev, DW_UART_IER);
        ier |= UART_IER_THRI;
        DwUartEnableInterrupt(dev, ier);
    }
    return ret;
}

int UartAsyncRecvBuffer(int port, unsigned char *buffer, int length, UART_RECV_DONE_CALLBACK callback, void *priv)
{
    int ret = 0;
    DW_UART *dev = &uart_config[port];

    if (!buffer || !callback)
        return -1;

    dev->async_mode = UART_ASYNC_MODE_DMA;
    dev->recv_buffer = buffer;
    dev->recv_length = length;
    dev->recv_done_callback = callback;
    dev->recv_done_priv = priv;

    if (dev->use_dma) {
        ret = DwUartDmaReceive(dev, buffer, length);
    } else {
        uint32_t ier = DwUartReadl(dev, DW_UART_IER);
        ier |= UART_IER_RDI;
        DwUartEnableInterrupt(dev, ier);
    }

    return ret;
}
