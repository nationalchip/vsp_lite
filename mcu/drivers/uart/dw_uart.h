/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 NationalChip Co., Ltd
 *
 * dw_uart.h MCU DESIGNWARE UART driver header
 *
 */
#ifndef __DW_UART_H__
#define __DW_UART_H__

#define UART_FIFO_DEPTH 128

/* Register offsets */
#define DW_UART_RBR   0x00 /* Receive Buffer Register */
#define DW_UART_THR   0x00 /* Transmit Holding Register */
#define DW_UART_DLL   0x00 /* Divisor Latch Low Register*/
#define DW_UART_IER   0x04 /* Interrupt Enable Register */
#define DW_UART_DLH   0x04 /* Divisor Latch High Register */
#define DW_UART_FCR   0x08 /* FIFO Control Register */
#define DW_UART_IIR   0x08 /* Interrupt Identification Register */
#define DW_UART_LCR   0x0C /* Line Control Register */
#define DW_UART_MCR   0x10 /* Modem Control Register */
#define DW_UART_LSR   0x14 /* Line Status Register */
#define DW_UART_MSR   0x18 /* Modem Status Register */
#define DW_UART_SCR   0x1C /* Scratchpad Regster */
#define DW_UART_LPDLL 0x20 /* Low Power Divisor Latch Low Register */
#define DW_UART_LPDLH 0x24 /* Low Power Divisor Latch High Register */
#define DW_UART_TFL   0x80 /* Transmit FIFO Level Register */
#define DW_UART_RFL   0x84 /* Receive FIFO Level Register */
#define DW_UART_DLF   0xC0 /* Divisor Latch Fraction Register */
#define DW_UART_CPR   0xF4 /* Component Paramenter Register */

/* Definition for the FIFO Control Register */
#define UART_FCR_FIFO_EN    0x01 /* FIFO enable */
#define UART_FCR_CLEAR_RCVR 0x02 /* Clear the RCVR FIFO */
#define UART_FCR_CLEAR_XMIT 0x04 /* Clear the XMIT FIFO */
#define UART_FCR_DMA_SELECT 0x08 /* For DMA applications */
#define UART_FCR_RX_TRIGGER_MASK    0xC0 /* Mask for the FIFO trigger range */
#define UART_FCR_RX_TRIGGER_1       0x00 /* Mask for trigger set at 1 */
#define UART_FCR_RX_TRIGGER_QUARTER 0x40 /* Mask for trigger set at 1/4 */
#define UART_FCR_RX_TRIGGER_HALF    0x80 /* Mask for trigger set at 1/2 */
#define UART_FCR_RX_TRIGGER_FULL    0xC0 /* Mask for trigger set at fifo_depth-2 */
#define UART_FCR_TX_TRIGGER_MASK    0x30 /* Mask for the FIFO trigger range */
#define UART_FCR_TX_TRIGGER_EMPTY   0x00 /* Mask for trigger set at empty */
#define UART_FCR_TX_TRIGGER_2       0x10 /* Mask for trigger set at 2 */
#define UART_FCR_TX_TRIGGER_QUARTER 0x20 /* Mask for trigger set at 1/4 */
#define UART_FCR_TX_TRIGGER_HALF    0x30 /* Mask for trigger set at 1/2 */

#define UART_FCR_RXSR 0x02 /* Receiver soft reset */
#define UART_FCR_TXSR 0x04 /* Transmitter soft reset */

/* Definition for the Model Control Register */
#define UART_MCR_DTR  0x01 /* DTR */
#define UART_MCR_RTS  0x02 /* RTS */
#define UART_MCR_OUT1 0x04 /* Out 1 */
#define UART_MCR_OUT2 0x08 /* Out 2 */
#define UART_MCR_LOOP 0x10 /* Enable loopback test mode */
#define UART_MCR_AFE  0x20 /* Enable auto-RTS/CTS */
#define UART_MCR_SIRE 0x40 /* Enable SIR Mode */

#define UART_MCR_DTR_BITS  0
#define UART_MCR_RTS_BITS  1
#define UART_MCR_OUT1_BITS 2
#define UART_MCR_OUT2_BITS 3
#define UART_MCR_LOOP_BITS 4
#define UART_MCR_AFE_BITS  5
#define UART_MCR_SIRE_BITS 6

/* Definition for the Line Control Register */
#define UART_LCR_DLS_MASK 0x03 /* Character length select mask */
#define UART_LCR_DLS_5    0x00 /* 5 bit character length */
#define UART_LCR_DLS_6    0x01 /* 6 bit character length */
#define UART_LCR_DLS_7    0x02 /* 7 bit character length */
#define UART_LCR_DLS_8    0x03 /* 8 bit character length */
#define UART_LCR_STB      0x04 /* Stop Bits, off=1, on=1.5 or 2) */
#define UART_LCR_PEN      0x08 /* Parity eneble */
#define UART_LCR_EPS      0x10 /* Even Parity Select */
#define UART_LCR_STKP     0x20 /* Stick Parity */
#define UART_LCR_SBRK     0x40 /* Set Break */
#define UART_LCR_DLAB     0x80 /* Divisor latch access bit */

/* Definition for the Line Status Register */
#define UART_LSR_DR   0x01 /* Data ready */
#define UART_LSR_OE   0x02 /* Overrun */
#define UART_LSR_PE   0x04 /* Parity error */
#define UART_LSR_FE   0x08 /* Framing error */
#define UART_LSR_BI   0x10 /* Break */
#define UART_LSR_THRE 0x20 /* Xmit holding register empty */
#define UART_LSR_TEMT 0x40 /* Xmitter empty */
#define UART_LSR_ERR  0x80 /* Error */

/* Definition for the Modem Status Register */
#define UART_MSR_DCTS 0x01 /* Delta CTS */
#define UART_MSR_DDSR 0x02 /* Delta DSR */
#define UART_MSR_TERI 0x04 /* Trailing edge ring indicator */
#define UART_MSR_DDCD 0x08 /* Delta DCD */
#define UART_MSR_CTS  0x10 /* Clear to Send */
#define UART_MSR_DSR  0x20 /* Data Set Ready */
#define UART_MSR_RI   0x40 /* Ring Indicator */
#define UART_MSR_DCD  0x80 /* Data Carrier Detect */

/* Definition for the Interrupt Identification Register */
#define UART_IIR_ID_MASK 0x06 /* Mask for the interrupt ID */

#define UART_IIR_MSI     0x00 /* Modem status interrupt */
#define UART_IIR_NO_INT  0x01 /* No interrupts pending */
#define UART_IIR_THRI    0x02 /* Transmitter holding register empty */
#define UART_IIR_RDI     0x04 /* Receiver data interrupt */
#define UART_IIR_RLSI    0x06 /* Receiver line status interrupt */
#define UART_IIR_CHTO    0x0C /* Character timeout */

/* Definition for the Interrupt Enable Register */
#define UART_IER_RDI  0x01 /* Enable receiver data interrupt */
#define UART_IER_THRI 0x02 /* Enable Transmitter holding register int. */
#define UART_IER_RLSI 0x04 /* Enable receiver line status interrupt */
#define UART_IER_MSI  0x08 /* Enable Modem status interrupt */

/* Definition for the Component Parameter Register */
#define UART_CPR_FIFO_MODE_MASK  0xFF
#define UART_CPR_FIFO_MODE_SHIFT 16

/* Some defaults for LCR */
#define UART_LCR_8N1 0x03

#define LCR_DATA_5BIT 0x00
#define LCR_DATA_6BIT 0x01
#define LCR_DATA_7BIT 0x02
#define LCR_DATA_8BIT 0x03

#define LCR_STOP_1BIT 0x00 /* 1 stop bit */
#define LCR_STOP_2BIT 0x04 /* 2 stop bit; 1.5 stop bit when use 5bit DATA */

#define LCR_PARITY_NONE 0x00
#define LCR_PARITY_ODD  0x08
#define LCR_PARITY_EVEN 0x18

/* DLF SIZE */
#define UART_DLF_SIZE 4

/* Some defaults for FCR */
#define UART_FCRVAL (UART_FCR_TX_TRIGGER_HALF | UART_FCR_RX_TRIGGER_QUARTER | \
        UART_FCR_DMA_SELECT | UART_FCR_RXSR | UART_FCR_TXSR | UART_FCR_FIFO_EN)

#define UART_FCRVAL_AFCE (UART_FCR_TX_TRIGGER_HALF | UART_FCR_RX_TRIGGER_HALF | \
        UART_FCR_DMA_SELECT | UART_FCR_RXSR | UART_FCR_TXSR | UART_FCR_FIFO_EN)

#define UART_FLOWCTRL_NONE (UART_MCR_DTR | UART_MCR_RTS)
#define UART_FLOWCTRL_AUTO (UART_MCR_RTS | UART_MCR_AFE)

#define UART_ASYNC_MODE_NONE      0x00
#define UART_ASYNC_MODE_INTERRUPT 0x01
#define UART_ASYNC_MODE_DMA       0x02

typedef struct dw_uart {
    int port;
    void *regs;
    int line;
    unsigned int pclk;
    unsigned int baudrate;
    unsigned int div;
    unsigned int dlf;
    int data_bits;
    int stop_bits;
    int flow;
    int parity;
    int use_dma;

    int irq_num;

    int async_mode;

    void *can_send_callback;
    void *can_send_priv;

    void *can_recv_callback;
    void *can_recv_priv;

    void *recv_done_callback;
    void *recv_done_priv;
    unsigned char *recv_buffer;
    unsigned int recv_length;

    void *send_done_callback;
    void *send_done_priv;
    unsigned char *send_buffer;
    unsigned int send_length;
} DW_UART;

#endif
