/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * gpio.c: MCU GPIO Driver for GX8010NRE
 *
 */

#include <stdio.h>
#include <types.h>
#include <div64.h>

#include <base_addr.h>
#include <soc_config.h>

#include <driver/gpio.h>
#include <driver/irq.h>

#define LOG_TAG "[GPIO]"

typedef struct {
    unsigned long  EPDDR;
    unsigned long  EPDDR_SET;
    unsigned long  EPDDR_CLR;
    unsigned long  reserve1;
    unsigned long  EPB_OUT;
    unsigned long  EPBSET;
    unsigned long  EPBCLR;
    unsigned long  EPDR;
    unsigned long  PWM_CHANNEL_SEL[4];
    unsigned long  PWM_MUX;
    unsigned long  DSEN;
    unsigned long  DSCNT;
    unsigned long  EPODR;
    unsigned long  INTC_EN;
    unsigned long  INTC_STA;
    unsigned long  INTC_MASK;
    unsigned long  reserve2;
    unsigned long  INTC_HIGHT;
    unsigned long  INTC_LOW;
    unsigned long  INTC_RISING;
    unsigned long  INTC_FALLING;
    unsigned long  reserve3[8];

    unsigned long  PWM_DACIN[8];
    unsigned long  reserve4[8];
    unsigned long  PWM_CYCLE[8];
    unsigned long  PWM_UPDATE;
    unsigned long  PWM_DACEN;
    unsigned long  reserve5[2];
    unsigned long  PWM_CLK_SEL;
}GPIO_REGS;

#define PWM_CHANNEL_SHIFT               0x8
#define PWM_CHANNEL_NUM                 0x8
#define PWM_CHANNEL_MASK                0xf
#define PWM_CHANNEL_WIDTH               0x4

#ifdef CONFIG_GX8010NRE
#define PWM_SEL_CHANGE(data, shift) (((data >> 3 * shift) & 0x7) << (shift * 4))
#define PWM_SEL_READ(data) (PWM_SEL_CHANGE(data,0) | PWM_SEL_CHANGE(data,1) | PWM_SEL_CHANGE(data,2) | PWM_SEL_CHANGE(data,3) | PWM_SEL_CHANGE(data,4) | PWM_SEL_CHANGE(data,5) | PWM_SEL_CHANGE(data,6) | PWM_SEL_CHANGE(data,7))
#else
#define PWM_SEL_READ(data) (data)
#endif

#define ARRAY_MAX_COUNT 1000
#define NSEC_PER_SEC    1000000000L
#define CLOCK_PWM       CONFIG_GPIO_CLK

//=================================================================================================

static GPIO_REGS * _GpioGetRegsBase(unsigned int port)
{
    if (port < 32) {
        return (GPIO_REGS *)MCU_REG_BASE_GPIO1;
    } else if (port < 64) {
        return (GPIO_REGS *)MCU_REG_BASE_GPIO2;
    } else if (port < 96) {
        return (GPIO_REGS *)MCU_REG_BASE_GPIO3;
    } else
        return (GPIO_REGS *)NULL;
}

//=================================================================================================

GPIO_DIRECTION GpioGetDirection(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs)
        return (regs->EPDDR & (1 << (port % 32))) ? GPIO_DIRECTION_OUTPUT : GPIO_DIRECTION_INPUT;

    return -1;
}

int GpioSetDirection(unsigned int port, GPIO_DIRECTION direction)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs) {
        unsigned int offset = port % 32;
        if (direction)
            regs->EPDDR |= 1 << offset;
        else
            regs->EPDDR &= ~(1 << offset);
        return 0;
    }
    return -1;
}

//=================================================================================================

GPIO_LEVEL GpioGetLevel(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs)
        return (regs->EPDR & (1 << (port % 32))) ? GPIO_LEVEL_HIGH : GPIO_LEVEL_LOW;

    return -1;
}

int GpioSetLevel(unsigned int port, GPIO_LEVEL level)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (regs) {
        unsigned int offset = port % 32;
        if (level == GPIO_LEVEL_HIGH)
            regs->EPBSET = 1 << offset;
        else
            regs->EPBCLR = 1 << offset;
        return 0;
    }
    return -1;
}

unsigned long  direction_31_00;
unsigned long  direction_63_32;

void GpioSuspend(void)
{
    volatile GPIO_REGS *regs_31_00 = (volatile GPIO_REGS *)MCU_REG_BASE_GPIO1;
    volatile GPIO_REGS *regs_63_32 = (volatile GPIO_REGS *)MCU_REG_BASE_GPIO2;
    direction_31_00 = regs_31_00->EPDDR;
    direction_63_32 = regs_63_32->EPDDR;
    regs_31_00->EPDDR = 0;
    regs_63_32->EPDDR = 0;
}

void GpioResume(void)
{
    volatile GPIO_REGS *regs_31_00 = (volatile GPIO_REGS *)MCU_REG_BASE_GPIO1;
    volatile GPIO_REGS *regs_63_32 = (volatile GPIO_REGS *)MCU_REG_BASE_GPIO2;
    regs_31_00->EPDDR = direction_31_00;
    regs_63_32->EPDDR = direction_63_32;
}

//=================================================================================================

#define ONE_GROUP_GPIO_NUM 32
#define GPIO_GROUP_NUM 3

typedef struct gpio_irq_info {
    unsigned int port;
    GPIO_CALLBACK callback;
    void *pdata;
} GPIO_IRQ_INFO;

typedef struct {
    volatile GPIO_REGS *regs;
    unsigned int counter;
    GPIO_IRQ_INFO irq_info[ONE_GROUP_GPIO_NUM];
} GPIO_TRIGGER_GROUP;

static GPIO_TRIGGER_GROUP s_gpio_trigger_groups[GPIO_GROUP_NUM] = {{NULL}};

void GpioMaskTrigger(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    regs->INTC_MASK |= 1 << port % 32;
}

void GpioUnmaskTrigger(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    regs->INTC_MASK &= ~(1 << port % 32);
}

static int _GpioTriggerISR(int irq, void *pdata)
{
    if (pdata == NULL)
        return -1;

    GPIO_TRIGGER_GROUP *trigger_group = (GPIO_TRIGGER_GROUP *)pdata;

    int result = -1;
    for (int i = 0; i < ONE_GROUP_GPIO_NUM; i++) {
        if (trigger_group->regs->INTC_STA & (0x1 << i)) {
            trigger_group->regs->INTC_MASK |= 1 << i;
            trigger_group->irq_info[i].callback(trigger_group->irq_info[i].port,
                                                trigger_group->irq_info[i].pdata);
            trigger_group->regs->INTC_STA  = 1 << i;
            trigger_group->regs->INTC_MASK &= ~(1 << i);
            result = 0;
        }
    }
    return result;
}

int GpioEnableTrigger(unsigned int port, GPIO_TRIGGER_EDGE edge, GPIO_CALLBACK callback, void *pdata)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (NULL == regs)
        return -1;

    if (callback == NULL) {
        printf(LOG_TAG"callback can not be NULL\n");
        return -1;
    }

    if (GpioSetDirection(port, GPIO_DIRECTION_INPUT))
        return -1;

    GPIO_TRIGGER_GROUP *trigger_group = &s_gpio_trigger_groups[port / 32];
    trigger_group->regs = regs;

    if (trigger_group->irq_info[port % 32].callback == NULL) {

        trigger_group->irq_info[port % 32].callback = callback;
        trigger_group->irq_info[port % 32].pdata = pdata;
        trigger_group->irq_info[port % 32].port = port % 32;
        trigger_group->counter += 1;

        regs->INTC_EN |= 1 << port % 32;

        if (edge & GPIO_TRIGGER_LEVEL_HIGH)
            regs->INTC_HIGHT   |=  (1 << port % 32);
        else
            regs->INTC_HIGHT   &= ~(1 << port % 32);
        if (edge & GPIO_TRIGGER_LEVEL_LOW)
            regs->INTC_LOW     |=  (1 << port % 32);
        else
            regs->INTC_LOW     &= ~(1 << port % 32);
        if (edge & GPIO_TRIGGER_EDGE_RISING)
            regs->INTC_RISING  |=  (1 << port % 32);
        else
            regs->INTC_RISING  &= ~(1 << port % 32);
        if (edge & GPIO_TRIGGER_EDGE_FALLING)
            regs->INTC_FALLING |=  (1 << port % 32);
        else
            regs->INTC_FALLING &= ~(1 << port % 32);

        regs->INTC_MASK &= ~(1 << port % 32);

        gx_request_irq(MCU_IRQ_GPIO0 + port / 32, _GpioTriggerISR, trigger_group);
        return 0;
    }
    return 0;
}

int GpioDisableTrigger(unsigned int port)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (NULL == regs)
        return -1;

    GPIO_TRIGGER_GROUP *trigger_group = &s_gpio_trigger_groups[port / 32];

    if (trigger_group->irq_info[port % 32].callback != NULL) {

        trigger_group->irq_info[port % 32].callback = NULL;
        trigger_group->irq_info[port % 32].pdata = NULL;
        trigger_group->irq_info[port % 32].port = 254;   // impossible value
        trigger_group->counter -= 1;   // impossible value

        trigger_group->regs->INTC_MASK |= 1 << port % 32;
        trigger_group->regs->INTC_STA  |= 1 << port % 32;

    } else {
        //printf(LOG_TAG"This port haven't be registe!\n");
        return -1;
    }
    //printf(LOG_TAG"trigger_group->counter = %d\n", trigger_group->counter);
    if (trigger_group->counter == 0) {
        //printf(LOG_TAG"gx_free_irq %d\n", MCU_IRQ_GPIO0 + port / 32);
        gx_free_irq(MCU_IRQ_GPIO0 + port / 32);
    }
    return 0;
}

//=================================================================================================
typedef struct {
    int cycle;
    int duty;
    int counter;
} PWM_CHANNEL_INFO;

static struct {
    int port_pwm_flag;
    int gpio_to_channel[ONE_GROUP_GPIO_NUM * GPIO_GROUP_NUM];
    PWM_CHANNEL_INFO pwm_channel_info[PWM_CHANNEL_NUM];
} s_gpio_pwm_info = {0};

static int _GpioEnablePWM(unsigned int port, unsigned int enable, unsigned int cycle_ns, unsigned int duty_ns)
{
    volatile GPIO_REGS *regs = (volatile GPIO_REGS *)_GpioGetRegsBase(port);
    if (NULL == regs)
        return -1;

    if (enable) {
        regs->PWM_DACEN  = 0xff;
        regs->PWM_UPDATE = 0x00;

        int port_channel;
        port_channel = regs->PWM_CHANNEL_SEL[port % 32 / PWM_CHANNEL_SHIFT];

        if (s_gpio_pwm_info.gpio_to_channel[port] > 0)
            s_gpio_pwm_info.pwm_channel_info[s_gpio_pwm_info.gpio_to_channel[port] - 1].counter -= 1;

        int i = 0;
        for (i = 0; i < PWM_CHANNEL_NUM; i++) {
            if (s_gpio_pwm_info.pwm_channel_info[i].cycle == cycle_ns && s_gpio_pwm_info.pwm_channel_info[i].duty == duty_ns) {
                port_channel = PWM_SEL_READ(port_channel);
                port_channel &= (0xffffffff & ~(PWM_CHANNEL_MASK << ((port % 32 % PWM_CHANNEL_SHIFT) * PWM_CHANNEL_WIDTH)));
                port_channel |= (i << ((port % 32 % PWM_CHANNEL_SHIFT) * PWM_CHANNEL_WIDTH));
                regs->PWM_CHANNEL_SEL[port % 32 / PWM_CHANNEL_SHIFT] = port_channel;

                s_gpio_pwm_info.pwm_channel_info[i].counter += 1;
                s_gpio_pwm_info.gpio_to_channel[port] = i + 1;
                s_gpio_pwm_info.port_pwm_flag |= 0x1 << port % 32;
                i = PWM_CHANNEL_NUM + PWM_CHANNEL_NUM;
                break;
            }
        }

        if (i < PWM_CHANNEL_NUM + PWM_CHANNEL_NUM) {
            for (i = 0; i < PWM_CHANNEL_NUM; i++) {
                if (s_gpio_pwm_info.pwm_channel_info[i].counter == 0) {
                    port_channel= PWM_SEL_READ(port_channel);
                    port_channel &= (0xffffffff & ~(PWM_CHANNEL_MASK << ((port % 32 % PWM_CHANNEL_SHIFT) * PWM_CHANNEL_WIDTH)));
                    port_channel |= (i << ((port % 32 % PWM_CHANNEL_SHIFT) * PWM_CHANNEL_WIDTH));
                    regs->PWM_CHANNEL_SEL[port % 32 / PWM_CHANNEL_SHIFT] = port_channel;

                    s_gpio_pwm_info.pwm_channel_info[i].cycle = cycle_ns;
                    s_gpio_pwm_info.pwm_channel_info[i].duty = duty_ns;
                    s_gpio_pwm_info.pwm_channel_info[i].counter += 1;
                    s_gpio_pwm_info.gpio_to_channel[port] = i + 1;
                    s_gpio_pwm_info.port_pwm_flag |= 0x1 << port % 32;
                    break;
                } else if (i == 7) {
                    printf(LOG_TAG"No enough channel!\n");
                    return -1;
                }
            }
        }

        port_channel = regs->PWM_CHANNEL_SEL[port % 32 / PWM_CHANNEL_SHIFT];
        port_channel = PWM_SEL_READ(port_channel);
        port_channel = (port_channel >> ((port % 32 % PWM_CHANNEL_SHIFT) * PWM_CHANNEL_WIDTH)) & PWM_CHANNEL_MASK;

        u64 tmp = 0;
        int pc = 0;
        int dc = 0;

        tmp = cycle_ns * (u64)CLOCK_PWM;
        do_div(tmp, NSEC_PER_SEC);
        pc = tmp;

        tmp = duty_ns * (u64)CLOCK_PWM;
        do_div(tmp, NSEC_PER_SEC);
        dc = tmp;

        if (pc > 0xffff) {
            printf(LOG_TAG"This cycle is too big!\n");
            return -1;
        }else if (pc < dc) {
            printf(LOG_TAG"This duty is too big!\n");
        }

        regs->PWM_CYCLE[port_channel] = pc;
        regs->PWM_DACIN[port_channel] = dc;
        regs->PWM_MUX |= 0x1 << port % 32;

    } else if (s_gpio_pwm_info.port_pwm_flag & (0x1 << port % 32)) {
        if (s_gpio_pwm_info.gpio_to_channel[port] > 0)
            s_gpio_pwm_info.pwm_channel_info[s_gpio_pwm_info.gpio_to_channel[port] - 1].counter -= 1;

        s_gpio_pwm_info.port_pwm_flag &= ~(0x1 << port % 32);
        s_gpio_pwm_info.gpio_to_channel[port] = 0;
        regs->PWM_MUX &= ~(1 << port % 32);
        return 0;
    } else {
        return -1;
    }
    return 0;
}

int GpioEnablePWM(unsigned int port, unsigned int freq, unsigned int duty)
{
    // Frequence should <= NSEC_PER_SEC
    if (freq > NSEC_PER_SEC)
        freq = NSEC_PER_SEC;
    // Frequence should >= 2289
    if (freq < 2289)
        freq = 2289;
    // 0 <= duty <= 100
    if (duty > 100)
        duty = 100;

    unsigned int cycle_ns = NSEC_PER_SEC / freq;
    return _GpioEnablePWM(port, 1, cycle_ns, cycle_ns * duty / 100);
}

int GpioDisablePWM(unsigned int port)
{
    return _GpioEnablePWM(port, 0, 0, 0);
}
