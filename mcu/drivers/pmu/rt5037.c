#include <common.h>
#include <driver/i2c.h>

#define RT5037_I2C_ADDR  0x38
#define RT5037_BUCK_BASE 0x41
#define RT5037_LDO_BASE  0x48
#define RT5037_NORMAL_ENABLE  0x4F
#define BUCK_PER_STEP 25
#define BUCK_MIN_VOLTAGE 800
#define LDO_MIN_VOLTAGE  800

#define LDO1_EN_N 7
#define BUCK1_EN_N 3

#define RT5037_ON  1
#define RT5037_OFF 0

int rt5037_bus_id = 0;

void RT5037BuckSet(unsigned char buck_num, unsigned int voltage)
{
	unsigned char step = 0, data = 0;
	int ret;
	ret = gx_i2c_rx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_NORMAL_ENABLE, &data, 1);
	if (ret != 0){
		printf("rt5037 read fail\n");
		return;
	}
	if (voltage == 0){
		data &= ~(1 << (BUCK1_EN_N - buck_num + 1));
		gx_i2c_tx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_NORMAL_ENABLE, &data, 1);
		return;
	}
	data |= 1 << (BUCK1_EN_N - buck_num + 1);
	step = (voltage - BUCK_MIN_VOLTAGE) / BUCK_PER_STEP;
	gx_i2c_tx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_BUCK_BASE + (buck_num - 1), &step, 1);
	gx_i2c_tx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_NORMAL_ENABLE, &data, 1);
}

void RT5037LdoSet(unsigned char ldo_num, unsigned int voltage)
{
	unsigned char step = 0;
	unsigned char data = 0;
	int ret;
	ret = gx_i2c_rx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_NORMAL_ENABLE, &data, 1);
	if (ret != 0){
		printf("rt5037 read fail\n");
		return;
	}
	if (voltage == 0){
		data &= ~(1 << (LDO1_EN_N - ldo_num + 1));
		gx_i2c_tx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_NORMAL_ENABLE, &data, 1);
		return;
	}
	data |= 1 << (LDO1_EN_N - ldo_num + 1);
	step = (voltage - LDO_MIN_VOLTAGE) / BUCK_PER_STEP;
	gx_i2c_tx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_LDO_BASE + (ldo_num - 1), &step, 1);
	gx_i2c_tx(rt5037_bus_id, RT5037_I2C_ADDR, RT5037_NORMAL_ENABLE, &data, 1);
}

void RT5037Init(int bus_id)
{
	rt5037_bus_id = bus_id;
}
