#include <common.h>
#include <driver/i2c.h>
#include <driver/pmu.h>
#include <driver/delay.h>

#define BQ25890_MANUFACTURER		"Texas Instruments"
#define BQ25890_IRQ_PIN			"bq25890_irq"

#define BQ25890_ID			3

#define BQ25890_LOW_CHARGE_TEMP			0
#define BQ25890_HIGH_CHARGE_TEMP			450
#define BQ25890_OFF_CHARGE_TEMP			600
#define BQ25890_HIGH_CHARGE_CURRENT		400000 //uA
#define BQ25890_HIGH_CHARGE_FULL_VOL			4100000 //uV
#define BQ25890_INIT_PORT           10

#define dev_err(dev, format, ...) printf (format, ## __VA_ARGS__)
#if 0
#define pr_info(dev, format, ...) printf (format, ## __VA_ARGS__)
#else
#define pr_info(dev, format, ...)
#endif

enum {
	POWER_SUPPLY_STATUS_UNKNOWN = 0,
	POWER_SUPPLY_STATUS_CHARGING,
	POWER_SUPPLY_STATUS_DISCHARGING,
	POWER_SUPPLY_STATUS_NOT_CHARGING,
	POWER_SUPPLY_STATUS_FULL,
};

enum {
	POWER_SUPPLY_HEALTH_UNKNOWN = 0,
	POWER_SUPPLY_HEALTH_GOOD,
	POWER_SUPPLY_HEALTH_OVERHEAT,
	POWER_SUPPLY_HEALTH_DEAD,
	POWER_SUPPLY_HEALTH_OVERVOLTAGE,
	POWER_SUPPLY_HEALTH_UNSPEC_FAILURE,
	POWER_SUPPLY_HEALTH_COLD,
	POWER_SUPPLY_HEALTH_WATCHDOG_TIMER_EXPIRE,
	POWER_SUPPLY_HEALTH_SAFETY_TIMER_EXPIRE,
};

enum bq25890_fields {
	F_EN_HIZ, F_EN_ILIM, F_IILIM,				     /* Reg00 */
	F_BHOT, F_BCOLD, F_VINDPM_OFS,				     /* Reg01 */
	F_CONV_START, F_CONV_RATE, F_BOOSTF, F_ICO_EN,
	F_HVDCP_EN, F_MAXC_EN, F_FORCE_DPM, F_AUTO_DPDM_EN,	     /* Reg02 */
	F_BAT_LOAD_EN, F_WD_RST, F_OTG_CFG, F_CHG_CFG, F_SYSVMIN,    /* Reg03 */
	F_PUMPX_EN, F_ICHG,					     /* Reg04 */
	F_IPRECHG, F_ITERM,					     /* Reg05 */
	F_VREG, F_BATLOWV, F_VRECHG,				     /* Reg06 */
	F_TERM_EN, F_STAT_DIS, F_WD, F_TMR_EN, F_CHG_TMR,
	F_JEITA_ISET,						     /* Reg07 */
	F_BATCMP, F_VCLAMP, F_TREG,				     /* Reg08 */
	F_FORCE_ICO, F_TMR2X_EN, F_BATFET_DIS, F_JEITA_VSET,
	F_BATFET_DLY, F_BATFET_RST_EN, F_PUMPX_UP, F_PUMPX_DN,	     /* Reg09 */
	F_BOOSTV, F_BOOSTI,					     /* Reg0A */
	F_VBUS_STAT, F_CHG_STAT, F_PG_STAT, F_SDP_STAT, F_VSYS_STAT, /* Reg0B */
	F_WD_FAULT, F_BOOST_FAULT, F_CHG_FAULT, F_BAT_FAULT,
	F_NTC_FAULT,						     /* Reg0C */
	F_FORCE_VINDPM, F_VINDPM,				     /* Reg0D */
	F_THERM_STAT, F_BATV,					     /* Reg0E */
	F_SYSV,							     /* Reg0F */
	F_TSPCT,						     /* Reg10 */
	F_VBUS_GD, F_VBUSV,					     /* Reg11 */
	F_ICHGR,						     /* Reg12 */
	F_VDPM_STAT, F_IDPM_STAT, F_IDPM_LIM,			     /* Reg13 */
	F_REG_RST, F_ICO_OPTIMIZED, F_PN, F_TS_PROFILE, F_DEV_REV,   /* Reg14 */

	F_MAX_FIELDS
};

/* initial field values, converted to register values */
struct bq25890_init_data {
	u8 ichg;	/* charge current		*/
	u8 vreg;	/* regulation voltage		*/
	u8 iterm;	/* termination current		*/
	u8 iprechg;	/* precharge current		*/
	u8 sysvmin;	/* minimum system voltage limit */
	u8 boostv;	/* boost regulation voltage	*/
	u8 boosti;	/* boost current limit		*/
	u8 boostf;	/* boost frequency		*/
	u8 ilim_en;	/* enable ILIM pin		*/
	u8 treg;	/* thermal regulation threshold */
	u8 enable;
};

struct bq25890_state {
	u8 online;
	u8 chrg_status;
	u8 chrg_fault;
	u8 vsys_status;
	u8 boost_fault;
	u8 bat_fault;
};

enum bq25890_temp_charge_status {
	BQ25890_NOMAL_CHARGING = 0,
	BQ25890_LOW_CHARGING = 1,
	BQ25890_HIGH_CHARGING = 2,
	BQ25890_OFF_CHARGING = 3,
};

struct bq25890_device {
	char *dev;
	int chip_id;
	int bus_id;
	unsigned int devid;
	enum bq25890_temp_charge_status charge_status;
	struct bq25890_init_data init_data;
	struct bq25890_state state;
};

struct bq25890_device *bq;

struct reg_field {
	unsigned int reg;
	unsigned int lsb;
	unsigned int msb;
	unsigned int id_size;
	unsigned int id_offset;
	unsigned int mask;
	unsigned int shift;
};

#define GENMASK(h, l)       ((((1UL) << ((h) - (l) + 1)) - 1) << (l))
#define REG_FIELD(_reg, _lsb, _msb) {       \
	.reg   = _reg,    \
	.lsb   = _lsb,    \
	.msb   = _msb,    \
	.shift = _lsb,    \
	.mask  = GENMASK(_msb, _lsb) \
}

static struct reg_field bq25890_reg_fields[] = {
	/* REG00 */
	[F_EN_HIZ]		= REG_FIELD(0x00, 7, 7),
	[F_EN_ILIM]		= REG_FIELD(0x00, 6, 6),
	[F_IILIM]		= REG_FIELD(0x00, 0, 5),
	/* REG01 */
	[F_BHOT]		= REG_FIELD(0x01, 6, 7),
	[F_BCOLD]		= REG_FIELD(0x01, 5, 5),
	[F_VINDPM_OFS]		= REG_FIELD(0x01, 0, 4),
	/* REG02 */
	[F_CONV_START]		= REG_FIELD(0x02, 7, 7),
	[F_CONV_RATE]		= REG_FIELD(0x02, 6, 6),
	[F_BOOSTF]		= REG_FIELD(0x02, 5, 5),
	[F_ICO_EN]		= REG_FIELD(0x02, 4, 4),
	[F_HVDCP_EN]		= REG_FIELD(0x02, 3, 3),
	[F_MAXC_EN]		= REG_FIELD(0x02, 2, 2),
	[F_FORCE_DPM]		= REG_FIELD(0x02, 1, 1),
	[F_AUTO_DPDM_EN]	= REG_FIELD(0x02, 0, 0),
	/* REG03 */
	[F_BAT_LOAD_EN]		= REG_FIELD(0x03, 7, 7),
	[F_WD_RST]		= REG_FIELD(0x03, 6, 6),
	[F_OTG_CFG]		= REG_FIELD(0x03, 5, 5),
	[F_CHG_CFG]		= REG_FIELD(0x03, 4, 4),
	[F_SYSVMIN]		= REG_FIELD(0x03, 1, 3),
	/* REG04 */
	[F_PUMPX_EN]		= REG_FIELD(0x04, 7, 7),
	[F_ICHG]		= REG_FIELD(0x04, 0, 6),
	/* REG05 */
	[F_IPRECHG]		= REG_FIELD(0x05, 4, 7),
	[F_ITERM]		= REG_FIELD(0x05, 0, 3),
	/* REG06 */
	[F_VREG]		= REG_FIELD(0x06, 2, 7),
	[F_BATLOWV]		= REG_FIELD(0x06, 1, 1),
	[F_VRECHG]		= REG_FIELD(0x06, 0, 0),
	/* REG07 */
	[F_TERM_EN]		= REG_FIELD(0x07, 7, 7),
	[F_STAT_DIS]		= REG_FIELD(0x07, 6, 6),
	[F_WD]			= REG_FIELD(0x07, 4, 5),
	[F_TMR_EN]		= REG_FIELD(0x07, 3, 3),
	[F_CHG_TMR]		= REG_FIELD(0x07, 1, 2),
	[F_JEITA_ISET]		= REG_FIELD(0x07, 0, 0),
	/* REG08 */
	[F_BATCMP]		= REG_FIELD(0x08, 6, 7),
	[F_VCLAMP]		= REG_FIELD(0x08, 2, 4),
	[F_TREG]		= REG_FIELD(0x08, 0, 1),
	/* REG09 */
	[F_FORCE_ICO]		= REG_FIELD(0x09, 7, 7),
	[F_TMR2X_EN]		= REG_FIELD(0x09, 6, 6),
	[F_BATFET_DIS]		= REG_FIELD(0x09, 5, 5),
	[F_JEITA_VSET]		= REG_FIELD(0x09, 4, 4),
	[F_BATFET_DLY]		= REG_FIELD(0x09, 3, 3),
	[F_BATFET_RST_EN]	= REG_FIELD(0x09, 2, 2),
	[F_PUMPX_UP]		= REG_FIELD(0x09, 1, 1),
	[F_PUMPX_DN]		= REG_FIELD(0x09, 0, 0),
	/* REG0A */
	[F_BOOSTV]		= REG_FIELD(0x0A, 4, 7),
	[F_BOOSTI]		= REG_FIELD(0x0A, 0, 2),
	/* REG0B */
	[F_VBUS_STAT]		= REG_FIELD(0x0B, 5, 7),
	[F_CHG_STAT]		= REG_FIELD(0x0B, 3, 4),
	[F_PG_STAT]		= REG_FIELD(0x0B, 2, 2),
	[F_SDP_STAT]		= REG_FIELD(0x0B, 1, 1),
	[F_VSYS_STAT]		= REG_FIELD(0x0B, 0, 0),
	/* REG0C */
	[F_WD_FAULT]		= REG_FIELD(0x0C, 7, 7),
	[F_BOOST_FAULT]		= REG_FIELD(0x0C, 6, 6),
	[F_CHG_FAULT]		= REG_FIELD(0x0C, 4, 5),
	[F_BAT_FAULT]		= REG_FIELD(0x0C, 3, 3),
	[F_NTC_FAULT]		= REG_FIELD(0x0C, 0, 2),
	/* REG0D */
	[F_FORCE_VINDPM]	= REG_FIELD(0x0D, 7, 7),
	[F_VINDPM]		= REG_FIELD(0x0D, 0, 6),
	/* REG0E */
	[F_THERM_STAT]		= REG_FIELD(0x0E, 7, 7),
	[F_BATV]		= REG_FIELD(0x0E, 0, 6),
	/* REG0F */
	[F_SYSV]		= REG_FIELD(0x0F, 0, 6),
	/* REG10 */
	[F_TSPCT]		= REG_FIELD(0x10, 0, 6),
	/* REG11 */
	[F_VBUS_GD]		= REG_FIELD(0x11, 7, 7),
	[F_VBUSV]		= REG_FIELD(0x11, 0, 6),
	/* REG12 */
	[F_ICHGR]		= REG_FIELD(0x12, 0, 6),
	/* REG13 */
	[F_VDPM_STAT]		= REG_FIELD(0x13, 7, 7),
	[F_IDPM_STAT]		= REG_FIELD(0x13, 6, 6),
	[F_IDPM_LIM]		= REG_FIELD(0x13, 0, 5),
	/* REG14 */
	[F_REG_RST]		= REG_FIELD(0x14, 7, 7),
	[F_ICO_OPTIMIZED]	= REG_FIELD(0x14, 6, 6),
	[F_PN]			= REG_FIELD(0x14, 3, 5),
	[F_TS_PROFILE]		= REG_FIELD(0x14, 2, 2),
	[F_DEV_REV]		= REG_FIELD(0x14, 0, 1)
};

/*
 * Most of the val -> idx conversions can be computed, given the minimum,
 * maximum and the step between values. For the rest of conversions, we use
 * lookup tables.
 */
enum bq25890_table_ids {
	/* range tables */
	TBL_ICHG,
	TBL_ITERM,
	TBL_IPRECHG,
	TBL_VREG,
	TBL_BATCMP,
	TBL_VCLAMP,
	TBL_BOOSTV,
	TBL_SYSVMIN,

	/* lookup tables */
	TBL_TREG,
	TBL_BOOSTI,
};

/* Thermal Regulation Threshold lookup table, in degrees Celsius */
static const u32 bq25890_treg_tbl[] = { 60, 80, 100, 120 };

#define BQ25890_TREG_TBL_SIZE		ARRAY_SIZE(bq25890_treg_tbl)

/* Boost mode current limit lookup table, in uA */
static const u32 bq25890_boosti_tbl[] = {
	500000, 700000, 1100000, 1300000, 1600000, 1800000, 2100000, 2400000
};

#define BQ25890_BOOSTI_TBL_SIZE		ARRAY_SIZE(bq25890_boosti_tbl)

struct bq25890_range {
	u32 min;
	u32 max;
	u32 step;
};

struct bq25890_lookup {
	const u32 *tbl;
	u32 size;
};

static const union {
	struct bq25890_range  rt;
	struct bq25890_lookup lt;
} bq25890_tables[] = {
	/* range tables */
	[TBL_ICHG] =	{ .rt = {0,	  5056000, 64000} },	 /* uA */
	[TBL_ITERM] =	{ .rt = {64000,   1024000, 64000} },	 /* uA */
	[TBL_VREG] =	{ .rt = {3840000, 4608000, 16000} },	 /* uV */
	[TBL_BATCMP] =	{ .rt = {0,	  140,     20} },	 /* mOhm */
	[TBL_VCLAMP] =	{ .rt = {0,	  224000,  32000} },	 /* uV */
	[TBL_BOOSTV] =	{ .rt = {4550000, 5510000, 64000} },	 /* uV */
	[TBL_SYSVMIN] = { .rt = {3000000, 3700000, 100000} },	 /* uV */

	/* lookup tables */
	[TBL_TREG] =	{ .lt = {bq25890_treg_tbl, BQ25890_TREG_TBL_SIZE} },
	[TBL_BOOSTI] =	{ .lt = {bq25890_boosti_tbl, BQ25890_BOOSTI_TBL_SIZE} }
};

static int bq25890_field_read(struct bq25890_device *bq,
			      enum bq25890_fields field_id)
{
	int ret, val, reg_val;
	struct reg_field *field = &bq25890_reg_fields[field_id];
	ret = gx_i2c_rx(bq->bus_id, bq->devid, field->reg, (unsigned char *)&reg_val, 1);
	if (ret < 0)
		return ret;

	reg_val &= field->mask;
	reg_val >>= field->shift;
	val = reg_val;

	return val;
}

static int bq25890_field_write(struct bq25890_device *bq,
			       enum bq25890_fields field_id, u8 val)
{
	int ret, reg_val;
	struct reg_field *field = &bq25890_reg_fields[field_id];
	ret = gx_i2c_rx(bq->bus_id, bq->devid, field->reg, (unsigned char *)&reg_val, 1);
	if (ret < 0)
		return ret;

	reg_val &= ~field->mask;
	reg_val |= (val << field->shift) & field->mask;
	ret = gx_i2c_tx(bq->bus_id, bq->devid, field->reg, (unsigned char *)&reg_val, 1);

	return ret;
}

static u8 bq25890_find_idx(u32 value, enum bq25890_table_ids id)
{
	u8 idx;

	if (id >= TBL_TREG) {
		const u32 *tbl = bq25890_tables[id].lt.tbl;
		u32 tbl_size = bq25890_tables[id].lt.size;

		for (idx = 1; idx < tbl_size && tbl[idx] <= value; idx++)
			;
	} else {
		const struct bq25890_range *rtbl = &bq25890_tables[id].rt;
		u8 rtbl_size;

		rtbl_size = (rtbl->max - rtbl->min) / rtbl->step + 1;

		for (idx = 1;
		     idx < rtbl_size && (idx * rtbl->step + rtbl->min <= value);
		     idx++)
			;
	}

	return idx - 1;
}

static u32 bq25890_find_val(u8 idx, enum bq25890_table_ids id)
{
	const struct bq25890_range *rtbl;

	/* lookup table? */
	if (id >= TBL_TREG)
		return bq25890_tables[id].lt.tbl[idx];

	/* range table */
	rtbl = &bq25890_tables[id].rt;

	return (rtbl->min + idx * rtbl->step);
}

enum bq25890_status {
	STATUS_NOT_CHARGING,
	STATUS_PRE_CHARGING,
	STATUS_FAST_CHARGING,
	STATUS_TERMINATION_DONE,
};

enum bq25890_chrg_fault {
	CHRG_FAULT_NORMAL,
	CHRG_FAULT_INPUT,
	CHRG_FAULT_THERMAL_SHUTDOWN,
	CHRG_FAULT_TIMER_EXPIRED,
};

struct bq2589x_map_pt {
	int x;
	int y;
};

/*
 r_pullup = 5.1(kohm)
 r_pulldn = 30(kohm)
 r_ts = r_pulldn * r_ntc / ( r_pulldn + r_ntc )
 vol_percentage = r_ts / ( r_pullup + r_ts ) * 100 * 1000
 vol_percentage = round(vol_percentage)
*/
/* vol_percentage (*1000) to temperature (*10) */
static const struct bq2589x_map_pt ntc[] = {
	{83692, -400},
	{83129, -350},
	{82423, -300},
	{81550, -250},
	{80485, -200},
	{79201, -150},
	{77676, -100},
	{75890, -50},
	{73827, 0},
	{71479, 50},
	{68852, 100},
	{65963, 150},
	{62848, 200},
	{59524, 250},
	{56060, 300},
	{52500, 350},
	{48898, 400},
	{45302, 450},
	{41556, 500},
	{38322, 550},
	{35028, 600},
	{31895, 650},
	{28953, 700},
	{26220, 750},
	{23695, 800},
	{21346, 850},
	{19178, 900},
	{17185, 950},
	{15377, 1000},
};

static int bq2589x_map_calc(const struct bq2589x_map_pt *pts,
							unsigned int tablesize, int input, int *output)
{
	bool descending = 1;
	unsigned int i = 0;

	if (pts == NULL)
		return -EINVAL;

	/* Check if table is descending or ascending */
	if (tablesize > 1) {
		if (pts[0].x < pts[1].x)
			descending = 0;
	}

	while (i < tablesize) {
		if ((descending == 1) && (pts[i].x < input)) {
			/* table entry is less than measured
				value and table is descending, stop */
			break;
		} else if ((descending == 0) && (pts[i].x > input)) {
			/* table entry is greater than measured
				value and table is ascending, stop */
			break;
		} else
			i++;
	}

	if (i == 0)
		*output = pts[0].y;
	else if (i == tablesize)
		*output = pts[tablesize - 1].y;
	else {
		/* result is between search_index and search_index-1 */
		/* interpolate linearly */
		*output = (((int) ((pts[i].y - pts[i - 1].y) *
						   (input - pts[i - 1].x)) /
					(pts[i].x - pts[i - 1].x)) +
				   pts[i - 1].y);
	}

	return 0;
}

int bq2589x_adc_read_temperature(struct bq25890_device *bq)
{
	int temp;
	int ret;

	ret = bq25890_field_read(bq, F_TSPCT);
	if (ret < 0) {
		dev_err(bq->dev, "read temperature failed :%d\n", ret);
		return ret;
	} else {
		temp = 21000 + (ret & 0x7f) * 465;
		return temp;
	}
}

#define DEFAULT_TEMP		250
static int bq2589x_batt_temp(struct bq25890_device *bq)
{
	int ret = 0;
	int val = 0;
	static int last_val = 350;

	ret = bq2589x_adc_read_temperature(bq);
	if (ret < 0) return DEFAULT_TEMP;

	ret = bq2589x_map_calc(ntc, ARRAY_SIZE(ntc), ret, &val);
	if (ret < 0) return DEFAULT_TEMP;

	if (val == 857) {
		dev_err(bq->dev, "Read battery info abnormal(vbat volt:2304,\
				temp:85.7),return last temp:%d\n", last_val);
		val = last_val;
	} else {
	    last_val = val;
	}

	return val;
}

static int bq25890_get_chip_state(struct bq25890_device *bq,
				  struct bq25890_state *state)
{
	int i, ret;

	struct {
		enum bq25890_fields id;
		u8 *data;
	} state_fields[] = {
		{F_CHG_STAT,	&state->chrg_status},
		{F_PG_STAT,	    &state->online},
		{F_VSYS_STAT,	&state->vsys_status},
		{F_BOOST_FAULT, &state->boost_fault},
		{F_BAT_FAULT,	&state->bat_fault},
		{F_CHG_FAULT,	&state->chrg_fault},
	};

	for (i = 0; i < ARRAY_SIZE(state_fields); i++) {
		ret = bq25890_field_read(bq, state_fields[i].id);
		if (ret < 0)
			return ret;

		*state_fields[i].data = ret;
	}

	pr_info("%s:CHG/PG/VSYS=%d/%d/%d, F:CHG/BOOST/BAT=%d/%d/%d\n", __func__,
		state->chrg_status, state->online, state->vsys_status,
		state->chrg_fault, state->boost_fault, state->bat_fault);

	return 0;
}

static bool bq25890_state_changed(struct bq25890_device *bq,
				  struct bq25890_state *new_state)
{
	struct bq25890_state old_state;

	old_state = bq->state;

	return (old_state.chrg_status != new_state->chrg_status ||
		old_state.chrg_fault != new_state->chrg_fault	||
		old_state.online != new_state->online		||
		old_state.bat_fault != new_state->bat_fault	||
		old_state.boost_fault != new_state->boost_fault ||
		old_state.vsys_status != new_state->vsys_status);
}

static void bq25890_charge_work(void *data)
{
	int ret;
	int temp = 250;
	int adc_val = 0;
	struct bq25890_device *bq = (struct bq25890_device *)data;

	temp = bq2589x_batt_temp(bq);
	//pr_info("%s, battery temp %d, ichg max %dmA, charge_status %d\n", __func__, temp, (bq->init_data.ichg * 64), bq->charge_status);
	if ((temp < BQ25890_LOW_CHARGE_TEMP) && (bq->charge_status != BQ25890_OFF_CHARGING)) {
		// disable charging
		ret = bq25890_field_write(bq, F_CHG_CFG, 0);
		if (ret < 0) {
			dev_err(bq->dev, "%s:Failed to disable charger:%d\n",
				__func__, ret);
			return;
		}
		bq->charge_status = BQ25890_OFF_CHARGING;
		pr_info("%s, battery temp below %d, disable charger\n", __func__,
			BQ25890_LOW_CHARGE_TEMP);
	}
	else if ((temp >= BQ25890_LOW_CHARGE_TEMP && temp < BQ25890_HIGH_CHARGE_TEMP) && (bq->charge_status != BQ25890_NOMAL_CHARGING)) {
		/*set charging full voltage to  NORMAL*/
		ret = bq25890_field_write(bq, F_VREG, bq->init_data.vreg);
		if (ret < 0) {
			dev_err(bq->dev, "%s:Failed to set charger voltage:%d\n",
				__func__, ret);
			return;
		}
		/* soft buffer temp 5*/
		if (bq->charge_status == BQ25890_HIGH_CHARGING) {
			if (temp > BQ25890_HIGH_CHARGE_TEMP - 50)
				return;
		}

		ret = bq25890_field_write(bq, F_ICHG, bq->init_data.ichg);
		if (ret < 0) {
			dev_err(bq->dev, "%s:Failed to set charger current max:%d\n",
				__func__, ret);
			return;
		}
		if (bq->charge_status == BQ25890_OFF_CHARGING) {
			ret = bq25890_field_write(bq, F_CHG_CFG, 1);
			if (ret < 0) {
				dev_err(bq->dev, "%s:Failed to enable charger:%d\n",
					__func__, ret);
				return;
			}
		}
		bq->charge_status = BQ25890_NOMAL_CHARGING;
		pr_info("%s, battery temp below %d, enable charger %dmA\n", __func__,
			BQ25890_HIGH_CHARGE_TEMP, (bq->init_data.ichg * 64));
	}
	else if ((temp >= BQ25890_HIGH_CHARGE_TEMP && temp < BQ25890_OFF_CHARGE_TEMP) &&
			(bq->charge_status != BQ25890_HIGH_CHARGING)) {
		/*set charging full voltage to  4100mV*/
		adc_val = (BQ25890_HIGH_CHARGE_FULL_VOL - 3840000)/16000;
		ret = bq25890_field_write(bq, F_VREG, adc_val);
		if (ret < 0) {
			dev_err(bq->dev, "%s:Failed to set charger voltage:%d\n",
				__func__, ret);
			return;
		}
		/* soft buffer temp 5*/
		if (bq->charge_status == BQ25890_OFF_CHARGING) {
			if (temp > BQ25890_OFF_CHARGE_TEMP - 50)
				return;
		}

		/*set charging current  448mA*/
		adc_val = (BQ25890_HIGH_CHARGE_CURRENT/64000) + 1;
		ret = bq25890_field_write(bq, F_ICHG, adc_val);
		if (ret < 0) {
			dev_err(bq->dev, "%s:Failed to set charger current max:%d\n",
				__func__, ret);
			return;
		}
		if (bq->charge_status == BQ25890_OFF_CHARGING) {
			ret = bq25890_field_write(bq, F_CHG_CFG, 1);
			if (ret < 0) {
				dev_err(bq->dev, "%s:Failed to enable charger:%d\n",
					__func__, ret);
				return;
			}
		}
		bq->charge_status = BQ25890_HIGH_CHARGING;
		pr_info("%s, battery temp betwen %d-%d, set charge current to %dmA\n", __func__,
			BQ25890_HIGH_CHARGE_TEMP, BQ25890_OFF_CHARGE_TEMP, adc_val *64);
	}
	else if ((temp >= BQ25890_OFF_CHARGE_TEMP) &&
			(bq->charge_status != BQ25890_OFF_CHARGING)) {
		// disable charging
		ret = bq25890_field_write(bq, F_CHG_CFG, 0);
		if (ret < 0) {
			dev_err(bq->dev, "%s:Failed to disable charger:%d\n",
				__func__, ret);
			return;
		}
		bq->charge_status = BQ25890_OFF_CHARGING;
		pr_info("%s, battery temp over %d, disable charger\n", __func__,
			BQ25890_OFF_CHARGE_TEMP);
	}

	return;
}

static void bq25890_handle_state_change(struct bq25890_device *bq,
					struct bq25890_state *new_state)
{
	int ret;
	struct bq25890_state old_state;

	old_state = bq->state;

	if (!new_state->online) {			     /* power removed */
		/* disable ADC */
		ret = bq25890_field_write(bq, F_CONV_START, 0);
		if (ret < 0)
			goto error;
	} else if (!old_state.online) {			    /* power inserted */
		/* enable ADC, to have control of charge current/voltage */
		ret = bq25890_field_write(bq, F_CONV_START, 1);
		if (ret < 0)
			goto error;
	}

	return;

error:
	dev_err(bq->dev, "Error communicating with the chip.\n");
}

static int bq25890_irq_handler_thread(int irq, void *private)
{
	struct bq25890_device *bq = private;
	int ret;
	struct bq25890_state state;

	//pr_info( "%s.\n", __func__);
	ret = bq25890_get_chip_state(bq, &state);
	if (ret < 0)
		goto handled;

	if (!bq25890_state_changed(bq, &state))
		goto handled;

	bq25890_handle_state_change(bq, &state);

	bq->state = state;

	bq25890_charge_work(bq);

handled:
	return 0;
}

static int bq25890_chip_reset(struct bq25890_device *bq)
{
	int ret;
	int rst_check_counter = 10;

	ret = bq25890_field_write(bq, F_REG_RST, 1);
	if (ret < 0)
		return ret;

	do {
		ret = bq25890_field_read(bq, F_REG_RST);
		if (ret < 0)
			return ret;

		udelay(5);
	} while (ret == 1 && --rst_check_counter);

	if (!rst_check_counter)
		return -ETIMEDOUT;

	return 0;
}

static int bq25890_hw_init(struct bq25890_device *bq)
{
	int ret;
	int i;
	struct bq25890_state state;

	const struct {
		enum bq25890_fields id;
		u32 value;
	} init_data[] = {
		{F_CHG_CFG,	 bq->init_data.enable},    // 充电启用
		{F_ICHG,	 bq->init_data.ichg},      // 快速充电电流限制
		{F_VREG,	 bq->init_data.vreg},      // 充电电压限制
		{F_ITERM,	 bq->init_data.iterm},     // 终止电流限值
		{F_IPRECHG,	 bq->init_data.iprechg},   // 预充电电流限制
		{F_SYSVMIN,	 bq->init_data.sysvmin},   // 最小系统电压
		{F_BOOSTV,	 bq->init_data.boostv},    // 升压模式电压调节
		{F_BOOSTI,	 bq->init_data.boosti},    // 升压模式电流限制
		{F_BOOSTF,	 bq->init_data.boostf},    // 升压模式频率选择
		{F_EN_ILIM,	 bq->init_data.ilim_en},   // 启用ILIM引脚
		{F_TREG,	 bq->init_data.treg}       // 热调节阈值
	};

	ret = bq25890_chip_reset(bq);
	if (ret < 0)
		return ret;

	/* disable watchdog */
	ret = bq25890_field_write(bq, F_WD, 0);
	if (ret < 0)
		return ret;

	/* initialize currents/voltages and other parameters */
	for (i = 0; i < ARRAY_SIZE(init_data); i++) {
		ret = bq25890_field_write(bq, init_data[i].id,
					  init_data[i].value);
		if (ret < 0)
			return ret;
	}

	/* Configure ADC for continuous conversions. This does not enable it. */
	ret = bq25890_field_write(bq, F_CONV_RATE, 1);
	if (ret < 0)
		return ret;

	ret = bq25890_get_chip_state(bq, &state);
	if (ret < 0)
		return ret;

	bq->state = state;

	return 0;
}

static int bq25890_fw_probe(struct bq25890_device *bq, struct BQ25890Config *config)
{
	int i;
	struct bq25890_init_data *init = &bq->init_data;
	struct {
		enum bq25890_table_ids tbl_id;
		u32 *cfg_data;
		u8 *conv_data; /* holds converted value from given property */
	} props[] = {
		/* required properties */
		{TBL_ICHG,    &config->ichg,    &init->ichg},
		{TBL_VREG,    &config->vreg,    &init->vreg},
		{TBL_ITERM,   &config->iterm,   &init->iterm},
		{TBL_ITERM,   &config->iprechg, &init->iprechg},
		{TBL_SYSVMIN, &config->sysvmin, &init->sysvmin},
		{TBL_BOOSTV,  &config->boostv,  &init->boostv},
		{TBL_BOOSTI,  &config->boosti,  &init->boosti},
		/* optional properties */
		{TBL_TREG,    &config->treg,    &init->treg}
	};

	for (i = 0; i < ARRAY_SIZE(props); i++) {
		*props[i].conv_data = bq25890_find_idx(*props[i].cfg_data,
						       props[i].tbl_id);
	}

	init->ilim_en = config->ilim_en;
	init->boostf = config->boostf;
	init->enable = config->enable;

	return 0;
}


int bq25890_init(int i2c_bus, struct BQ25890Config *config)
{
	int ret;
	struct BQ25890Config def_config;

	def_config.ichg = 2000000;
	def_config.vreg = 4200000;
	def_config.iterm = 50000;
	def_config.iprechg = 128000;
	def_config.sysvmin = 3600000;
	def_config.boostv = 5000000;
	def_config.boosti = 1000000;
	def_config.boostf = 0;
	def_config.ilim_en = 1;
	def_config.treg = 60;
	def_config.enable = 1;
	if (config == NULL){
		config = &def_config;
	}

	bq = malloc(sizeof(*bq));
	if (!bq)
		return -ENOMEM;

	bq->bus_id = i2c_bus;
	bq->devid = 0xd4 >> 1;

	bq->chip_id = bq25890_field_read(bq, F_PN);
	if (bq->chip_id < 0) {
		dev_err(dev, "Cannot read chip ID.\n");
		return bq->chip_id;
	}

	if (bq->chip_id != BQ25890_ID) {
		dev_err(dev, "Chip with ID=%d, not supported!\n", bq->chip_id);
		return -ENODEV;
	}

	if (config == NULL){
		dev_err(dev, "Config error.\n");
		return -ENODEV;
	}

	bq25890_fw_probe(bq, config);

	ret = bq25890_hw_init(bq);
	if (ret < 0) {
		dev_err(dev, "Cannot initialize the chip.\n");
		return ret;
	}

	bq->charge_status = BQ25890_NOMAL_CHARGING;

	return 0;
}

int bq25890_get_property(struct BQ25890Info *info)
{
	int ret;
	/*struct bq25890_device *bq = (struct bq25890_device *)(psy);*/
	struct bq25890_state state;

	bq25890_irq_handler_thread(0, bq);

	state = bq->state;

	//当前充电状态
	state.chrg_status = bq25890_field_read(bq, F_CHG_STAT);
#if 1
	if (state.chrg_status == 0)
		printf("Not Charging\n");
	else if (state.chrg_status == 1)
		printf("Pre Charging\n");
	else if (state.chrg_status == 2)
		printf("Fast Charging\n");
	else if (state.chrg_status == 3)
		printf("Done Charging\n");
#endif
	if (!state.online)
		info->chrg_status = POWER_SUPPLY_STATUS_DISCHARGING;
	else if (state.chrg_status == STATUS_NOT_CHARGING)
		info->chrg_status = POWER_SUPPLY_STATUS_NOT_CHARGING;
	else if (state.chrg_status == STATUS_PRE_CHARGING ||
			state.chrg_status == STATUS_FAST_CHARGING)
		info->chrg_status = POWER_SUPPLY_STATUS_CHARGING;
	else if (state.chrg_status == STATUS_TERMINATION_DONE)
		info->chrg_status = POWER_SUPPLY_STATUS_FULL;
	else
		info->chrg_status = POWER_SUPPLY_STATUS_UNKNOWN;

	//电源状态
	info->online = state.online;

	//Charge Enable
	ret = bq25890_field_read(bq, F_CHG_CFG);
	if (ret < 0)
		return ret;

	info->enable = ret;

	//电源健康状态
	if (!state.chrg_fault && !state.bat_fault && !state.boost_fault)
		info->bat_fault = POWER_SUPPLY_HEALTH_GOOD;
	else if (state.bat_fault)
		info->bat_fault = POWER_SUPPLY_HEALTH_OVERVOLTAGE;
	else if (state.chrg_fault == CHRG_FAULT_TIMER_EXPIRED)
		info->bat_fault = POWER_SUPPLY_HEALTH_SAFETY_TIMER_EXPIRE;
	else if (state.chrg_fault == CHRG_FAULT_THERMAL_SHUTDOWN)
		info->bat_fault = POWER_SUPPLY_HEALTH_OVERHEAT;
	else
		info->bat_fault = POWER_SUPPLY_HEALTH_UNSPEC_FAILURE;

	//温度
	info->temp = bq2589x_batt_temp(bq);

	//VBUS电压
	ret = bq25890_field_read(bq, F_VBUSV);
	if (ret < 0)
		return ret;
	/* converted_val = 2.6V + ADC_val * 100mV*/
	info->Vbus = 2600000 + (ret & 0x7f) * 100000;

	//充电电流
	ret = bq25890_field_read(bq, F_ICHGR); /* read measured value */
	if (ret < 0)
		return ret;

	/* converted_val = ADC_val * 50mA (table 10.3.19) */
	info->chrg_I = ret * 50000;

	//快速充电电流限制
	ret = bq25890_field_read(bq, F_ICHG); /* read measured value */
	if (ret < 0)
		return ret;

	/* converted_val = val * 64mA */
	info->chrg_Imax = ret * 64000;

	//充电电压
	if (!state.online) {
		info->chrg_V = 0;
	}else{

		ret = bq25890_field_read(bq, F_BATV); /* read measured value */
		if (ret < 0)
			return ret;

		/* converted_val = 2.304V + ADC_val * 20mV (table 10.3.15) */
		info->chrg_V = 2304000 + ret * 20000;
	}

	//充电电压限制
	ret = bq25890_field_read(bq, F_VREG); /* read measured value */
	if (ret < 0)
		return ret;

	/* converted_val = 3.84V + val * 16mV */
	info->chrg_Vmax = 3840000 + ret * 16000;

	// 终止电流限值
	ret = bq25890_field_read(bq, F_ITERM); /* read measured value */
	info->chrg_Iterm = bq25890_find_val(ret, TBL_ITERM);

	return 0;
}
