#include <common.h>
#include <driver/i2c.h>
#include <driver/delay.h>
#include <driver/pmu.h>

#define CWFG_ENABLE_LOG 0//CHANGE   Customer need to change this for enable/disable log
#define DOUBLE_SERIES_BATTERY 0
#define pr_err printf

#define REG_VERSION             0x0
#define REG_VCELL               0x2
#define REG_SOC                 0x4
#define REG_RRT_ALERT           0x6
#define REG_CONFIG              0x8
#define REG_MODE                0xA
#define REG_VTEMPL              0xC
#define REG_VTEMPH              0xD
#define REG_BATINFO             0x10

#define MODE_SLEEP_MASK         (0x3<<6)
#define MODE_SLEEP              (0x3<<6)
#define MODE_NORMAL             (0x0<<6)
#define MODE_RESTART            (0xf<<0)

#define CONFIG_UPDATE_FLG       (0x1<<1)
#define ATHD                    (0x0<<3)        // ATHD = 0%

struct cw_battery *cw_bat = NULL;

#define cw_printk(fmt, arg...)        \
	({                                    \
	 if(CWFG_ENABLE_LOG){              \
	 printk("FG_CW2015 : %s : " fmt, __FUNCTION__ ,##arg);  \
	 }else{}                           \
	 })     //need check by Chaman

#define CWFG_I2C_ADDRESS 0x62
#define CWFG_I2C_BUSID   0x1
#define SIZE_BATINFO  64


#if 0
#define BATTERY_VOLETAGE_MAX 4200000
#define BATTERY_VOLETAGE_MIN 3600000
static unsigned char config_info[SIZE_BATINFO] = {
	0x17, 0x67, 0x66, 0x6C, 0x6A, 0x69, 0x64, 0x5E,
	0x65, 0x6B, 0x4E, 0x52, 0x4F, 0x4F, 0x46, 0x3C,
	0x35, 0x2B, 0x24, 0x20, 0x21, 0x2F, 0x42, 0x4C,
	0x24, 0x4A, 0x0B, 0x85, 0x31, 0x51, 0x57, 0x6D,
	0x77, 0x6B, 0x6A, 0x6F, 0x40, 0x1C, 0x7C, 0x42,
	0x0F, 0x31, 0x1E, 0x50, 0x86, 0x95, 0x97, 0x27,
	0x57, 0x73, 0x95, 0xC3, 0x80, 0xD8, 0xFF, 0xCB,
	0x2F, 0x7D, 0x72, 0xA5, 0xB5, 0xC1, 0x73, 0x09,
};
#else
static unsigned char config_info[SIZE_BATINFO] = {
	0x15, 0x9E, 0x75, 0x72, 0x73, 0x6A, 0x66, 0x65,
	0x63, 0x5F, 0x5C, 0x56, 0x54, 0x51, 0x52, 0x36,
	0x28, 0x25, 0x23, 0x22, 0x26, 0x34, 0x4D, 0x5E,
	0x63, 0x41, 0x0B, 0x85, 0x18, 0x2F, 0x54, 0x6A,
	0x6A, 0x6F, 0x78, 0x7A, 0x39, 0x17, 0x8A, 0x18,
	0x00, 0x35, 0x52, 0x87, 0x8F, 0x91, 0x94, 0x52,
	0x82, 0x8C, 0x92, 0x96, 0xB1, 0x95, 0xB2, 0xCB,
	0x2F, 0x7D, 0x72, 0xA5, 0xB5, 0xC1, 0xE0, 0x8A,
};
#endif

struct i2c_client {
	unsigned int devid;
	unsigned int bus_id;
};

struct cw_battery {
	struct i2c_client *client;
	int capacity;
	int voltage;
	int time_to_empty;
};

/*Define CW2015 iic read function*/
int cw_read(struct i2c_client *client, unsigned char reg, unsigned char buf[])
{
	int ret = 0;
	ret = gx_i2c_rx(client->bus_id, client->devid, reg, buf, 1);
	cw_printk("%d %2x = %2x\n", ret, reg, buf[0]);
	return ret;
}
/*Define CW2015 iic write function*/
int cw_write(struct i2c_client *client, unsigned char reg, unsigned char buf[])
{
	int ret = 0;
	ret = gx_i2c_tx(client->bus_id, client->devid, reg, &buf[0], 1);
	cw_printk("%d %2x = %2x\n", ret, reg, buf[0]);
	return ret;
}
/*Define CW2015 iic read word function*/
int cw_read_word(struct i2c_client *client, unsigned char reg, unsigned char buf[])
{
	int ret = 0;
	ret = gx_i2c_rx(client->bus_id, client->devid, reg, buf, 2);
	cw_printk("%d %2x = %2x %2x\n", ret, reg, buf[0], buf[1]);
	return ret;
}

/*CW2015 update profile function, Often called during initialization*/
int cw_update_config_info(struct cw_battery *cw_bat)
{
	int ret;
	unsigned char reg_val;
	int i;
	unsigned char reset_val;

	cw_printk("### \n");
	cw_printk("[FGADC] test config_info = 0x%x\n", config_info[0]);

	// make sure no in sleep mode
	ret = cw_read(cw_bat->client, REG_MODE, &reg_val);
	if(ret < 0) {
		return ret;
	}

	reset_val = reg_val;
	if((reg_val & MODE_SLEEP_MASK) == MODE_SLEEP) {
		return -1;
	}

	// update new battery info
	for (i = 0; i < SIZE_BATINFO; i++) {
		cw_printk("%X\n", config_info[i]);
		ret = cw_write(cw_bat->client, REG_BATINFO + i, &config_info[i]);
		if(ret < 0)
			return ret;
	}

	reg_val |= CONFIG_UPDATE_FLG;   // set UPDATE_FLAG
	reg_val &= 0x07;                // clear ATHD
	reg_val |= ATHD;                // set ATHD
	ret = cw_write(cw_bat->client, REG_CONFIG, &reg_val);
	if(ret < 0)
		return ret;
	// read back and check
	ret = cw_read(cw_bat->client, REG_CONFIG, &reg_val);
	if(ret < 0) {
		return ret;
	}

	if (!(reg_val & CONFIG_UPDATE_FLG)) {
		pr_err("Error: The new config set fail\n");
		//return -1;
	}

	if ((reg_val & 0xf8) != ATHD) {
		pr_err("Error: The new ATHD set fail\n");
		//return -1;
	}

	// reset
	reset_val &= ~(MODE_RESTART);
	reg_val = reset_val | MODE_RESTART;
	ret = cw_write(cw_bat->client, REG_MODE, &reg_val);
	if(ret < 0) return ret;

	ret = cw_write(cw_bat->client, REG_MODE, &reset_val);
	if(ret < 0) return ret;

	cw_printk("### cw2015 update config success!\n");

	return 0;
}
/*CW2015 init function, Often called during initialization*/
static int cw_init(struct cw_battery *cw_bat)
{
	int ret;
	int i;
	unsigned char reg_val = MODE_SLEEP;
	unsigned char version = 0;

	if ((reg_val & MODE_SLEEP_MASK) == MODE_SLEEP) {
		reg_val = MODE_NORMAL;
		ret = cw_write(cw_bat->client, REG_MODE, &reg_val);
		if (ret < 0)
			return ret;
	}

	ret = cw_read(cw_bat->client, REG_VERSION, &version);
	printk("cw2015 version: %d\n", version);
	if (version == 0xff || ret < 0) {
		pr_err("cw2015 I2C failed to read version");
		return -1;
	}
	ret = cw_read(cw_bat->client, REG_CONFIG, &reg_val);
	if (ret < 0)
		return ret;

	if ((reg_val & 0xf8) != ATHD) {
		reg_val &= 0x07;    /* clear ATHD */
		reg_val |= ATHD;    /* set ATHD */
		ret = cw_write(cw_bat->client, REG_CONFIG, &reg_val);
		if (ret < 0)
			return ret;
	}

	ret = cw_read(cw_bat->client, REG_CONFIG, &reg_val);
	if (ret < 0) {
		return ret;
	}

	if (!(reg_val & CONFIG_UPDATE_FLG)) {
		cw_printk("update config flg is true, need update config\n");
		ret = cw_update_config_info(cw_bat);
		if (ret < 0) {
			pr_err("%s : update config fail\n", __func__);
			return ret;
		}
	} else {
		for(i = 0; i < SIZE_BATINFO; i++) {
			ret = cw_read(cw_bat->client, (REG_BATINFO + i), &reg_val);
			if (ret < 0) {
				return ret;
			}

			//pr_err("%s, %X\n", __func__, reg_val);
			if (config_info[i] != reg_val) {
				break;
			}
		}
		if (i != SIZE_BATINFO) {
			cw_printk("config didn't match, need update config\n");
			ret = cw_update_config_info(cw_bat);
			if (ret < 0){
				return ret;
			}
		}
	}

	for (i = 0; i < 300; i++) {
		ret = cw_read(cw_bat->client, REG_SOC, &reg_val);
		if (ret < 0)
			return ret;
		else if (reg_val <= 0x64)
			break;
		mdelay(10);
	}

	if (i >= 30) {
		reg_val = MODE_SLEEP;
		ret = cw_write(cw_bat->client, REG_MODE, &reg_val);
		cw_printk("cw2015 input unvalid power error, cw2015 join sleep mode\n");
		return -1;
	}

	cw_printk("### cw2015 init success!\n");
	return 0;
}

static int cw_por(struct cw_battery *cw_bat)
{
	int ret;
	unsigned char reset_val;

	reset_val = MODE_SLEEP;
	ret = cw_write(cw_bat->client, REG_MODE, &reset_val);
	if (ret < 0)
		return ret;
	reset_val = MODE_NORMAL;
	ret = cw_write(cw_bat->client, REG_MODE, &reset_val);
	if (ret < 0)
		return ret;
	ret = cw_init(cw_bat);
	if (ret)
		return ret;
	return 0;
}

static int cw_get_capacity(struct cw_battery *cw_bat)
{
	int cw_capacity;
	int ret;
	unsigned char reg_val[2];

	ret = cw_read_word(cw_bat->client, REG_SOC, reg_val);
	if (ret < 0)
		return ret;

	cw_capacity = reg_val[0];

	return cw_capacity;
}

/*This function called when get voltage from cw2015*/
static int cw_get_voltage(struct cw_battery *cw_bat)
{
	int ret;
	unsigned char reg_val[2];
	u16 value16, value16_1, value16_2, value16_3;
	int voltage;

	ret = cw_read_word(cw_bat->client, REG_VCELL, reg_val);
	if (ret < 0) {
		return ret;
	}
	value16 = (reg_val[0] << 8) + reg_val[1];

	ret = cw_read_word(cw_bat->client, REG_VCELL, reg_val);
	if (ret < 0) {
		return ret;
	}
	value16_1 = (reg_val[0] << 8) + reg_val[1];

	ret = cw_read_word(cw_bat->client, REG_VCELL, reg_val);
	if (ret < 0) {
		return ret;
	}
	value16_2 = (reg_val[0] << 8) + reg_val[1];

	if (value16 > value16_1) {
		value16_3 = value16;
		value16 = value16_1;
		value16_1 = value16_3;
	}

	if (value16_1 > value16_2) {
		value16_3 =value16_1;
		value16_1 =value16_2;
		value16_2 =value16_3;
	}

	if (value16 >value16_1) {
		value16_3 =value16;
		value16 =value16_1;
		value16_1 =value16_3;
	}

	voltage = value16_1 * 312 / 1024;

	if (DOUBLE_SERIES_BATTERY)
		voltage = voltage * 2;
	return voltage;
}

/*This function called when get RRT from cw2015*/
static int cw_get_time_to_empty(struct cw_battery *cw_bat)
{
	int ret;
	unsigned char reg_val;
	u16 value16;

	ret = cw_read(cw_bat->client, REG_RRT_ALERT, &reg_val);
	if (ret < 0)
		return ret;

	value16 = reg_val;

	ret = cw_read(cw_bat->client, REG_RRT_ALERT + 1, &reg_val);
	if (ret < 0)
		return ret;

	value16 = ((value16 << 8) + reg_val) & 0x1fff;
	return value16;
}

static void cw_update_capacity(struct cw_battery *cw_bat)
{
	int cw_capacity;
	cw_capacity = cw_get_capacity(cw_bat);

	if ((cw_capacity >= 0) && (cw_capacity <= 100) && (cw_bat->capacity != cw_capacity)) {
		cw_bat->capacity = cw_capacity;
	}
}


static void cw_update_vol(struct cw_battery *cw_bat)
{
	int ret;
	ret = cw_get_voltage(cw_bat);
	if ((ret >= 0) && (cw_bat->voltage != ret)) {
		if (ret > 5000) ret =0;// error vol, set to 0, may battery is out
		cw_bat->voltage = ret;
	}
}

static void cw_update_time_to_empty(struct cw_battery *cw_bat)
{
	int ret;
	ret = cw_get_time_to_empty(cw_bat);
	if ((ret >= 0) && (cw_bat->time_to_empty != ret)) {
		cw_bat->time_to_empty = ret;
	}
}

static void cw_bat_work(struct cw_battery *cw_bat)
{
	/*Add for battery swap start*/
	int ret;
	unsigned char reg_val;
	//u16 value16;
	int i = 0;
	/*Add for battery swap end*/

	/*Add for battery swap start*/
	ret = cw_read(cw_bat->client, REG_MODE, &reg_val);
	if (ret < 0 || (reg_val == 0xff)) {
		//battery is out , you can send new battery capacity vol here what you want set
		//for example
		cw_bat->capacity = 100;
		cw_bat->voltage = 0;
	} else {
		if ((reg_val & MODE_SLEEP_MASK) == MODE_SLEEP) {
			for (i = 0; i < 5; i++) {
				if (cw_por(cw_bat) == 0)
					break;
			}
		}
		cw_update_capacity(cw_bat);
		cw_update_vol(cw_bat);
		cw_update_time_to_empty(cw_bat);
	}
	/*Add for battery swap end*/
	cw_printk("capacity = %d\n", cw_bat->capacity);
	cw_printk("voltage = %d\n", cw_bat->voltage);
}

int cw2015_init(int bus_id)
{
	int ret = -1;
	//int loop = 0;

	cw_printk("### \n");

	cw_bat = malloc(sizeof(*cw_bat));
	if (!cw_bat) {
		pr_err("cw_bat create fail!\n");
		return -ENOMEM;
	}

	cw_bat->client = malloc(sizeof(*cw_bat->client));
	if (!cw_bat->client) {
		pr_err("cw_bat create fail!\n");
		return -ENOMEM;
	}

	cw_bat->client->bus_id = bus_id;
	cw_bat->client->devid = 0xc4 >> 1;
	cw_bat->capacity = 1;
	cw_bat->voltage = 0;

	ret = cw_init(cw_bat);
	if (ret) {
		pr_err("%s : cw2015 init fail!\n", __func__);
		return ret;
	}

	cw_printk("### cw2015 driver probe success!\n");
	return 0;
}

int cw2015_get_property(struct CW2015Info *info)
{
	int ret = 0;

	cw_bat_work(cw_bat);

	info->capacity = cw_bat->capacity;

	info->voltage = cw_bat->voltage * 1000;

	info->time = cw_bat->time_to_empty;

	return ret;
}
