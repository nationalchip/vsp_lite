/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * pmu.c: PMU Driver for HC18P11xS_L
 *
 */

#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/pmu.h>

#define PMU_IC_POWER_ON  0x35
#define PMU_IC_POWER_OFF 0x3A

struct regsister {
	int Power_Switch;
    int Adc_High_2Bit;
    int Adc_Low_8Bit;
};

static struct
{
    int i2c_bus;
    int chip;
    struct regsister regs;
} pmu_dev = {
    .i2c_bus = 1,
    .chip = 0xa0,
    .regs = {
        .Power_Switch  = 0x12 ,
        .Adc_High_2Bit = 0x13 ,
        .Adc_Low_8Bit  = 0x14 ,
    },
};

static int i2c_operate(unsigned int addr, int alen, unsigned char *buf, int len, int flag)
{
    int status;
    int i2c_bus = pmu_dev.i2c_bus;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};
    int chip = pmu_dev.chip;

    addr_buf[0] = addr&0xff;

    msg[0].addr  = chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = chip;
    msg[1].flags = flag ? 0 : (msg[1].flags | I2C_M_RD);
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);

    if (status != 0 )
    {
        printf("PMU Transfer Error! | %d\n", status);
        return -1;
    }
    return 0;
}

int PmuReadVoltage(unsigned short *voltage)
{
    int status = 0;
    unsigned char data[2] = {0};
    int high_bit_reg = pmu_dev.regs.Adc_High_2Bit;

    status = i2c_operate(high_bit_reg, 1, data, 2, I2C_READ);

    if (status != 0)
        return -1;

    *voltage = ((unsigned short)(data[0] & 0x03)<<8)|data[1];

    return 0;
}

int PmuPowerOff()
{
    int status;
    int val = PMU_IC_POWER_OFF;
    int reg = pmu_dev.regs.Power_Switch;

    status = i2c_operate(reg, 1, (unsigned char*)&val, 1, I2C_WRITE);
    return status;
}

int PmuInit(int i2c_bus)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;
    pmu_dev.i2c_bus = i2c_bus;
    
    //set i2c bus speed to 20 kb/s
    if (gx_i2c_set_speed(i2c_bus, 20) != 0)
        return -1;

    return 0;
}