/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * drv_misc.h: Misc Device Driver
 *
 */

#include <common.h>

#include <base_addr.h>

#define DSK_MAX_LEN  (6 * 4)

int get_dsk(unsigned char *buf, int len)
{
	if (len > DSK_MAX_LEN)
		return -1;

	int *sp = (int *)buf;
	switch (len) {
		case 8:
			return -1;
		case 16:
			*(sp + 0) = readl(MCU_DSK_3);
			*(sp + 1) = readl(MCU_DSK_2);
			*(sp + 2) = readl(MCU_DSK_1);
			*(sp + 3) = readl(MCU_DSK_0);
			break;
		case 24:
			return -1;
		default:
			return -1;
	}

	return 0;
}

// for cache
#define CACHE_1K   0x09
#define CACHE_2K   0x0a
#define CACHE_4K   0x0b
#define CACHE_8K   0x0c
#define CACHE_16K  0x0d
#define CACHE_32K  0x0e
#define CACHE_64K  0x0f
#define CACHE_128K 0x10
#define CACHE_256K 0x11
#define CACHE_512K 0x12
#define CACHE_1M   0x13
#define CACHE_2M   0x14
#define CACHE_4M   0x15
#define CACHE_8M   0x16
#define CACHE_16M  0x17
#define CACHE_32M  0x18
#define CACHE_64M  0x19
#define CACHE_128M 0x1a
#define CACHE_256M 0x1b
#define CACHE_512M 0x1c
#define CACHE_1G   0x1d
#define CACHE_2G   0x1e
#define CACHE_4G   0x1f

void CacheInit(void)
{
	writel(1, CK802_CIR);   // invalid all
	writel(0x20000000 | (CACHE_2M   << 1) | 1, CK802_CRCR0);
	writel(0x30000000 | (CACHE_128M << 1) | 1, CK802_CRCR1);
	writel(1, CK802_CER);   // work
}

void CacheDone(void)
{
	writel(1, CK802_CIR);   // invalid all
	writel(0, CK802_CER);   // disable
}

