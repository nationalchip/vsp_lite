/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_aw9523b.c: LED Driver for AW9523B
 *
 */

#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/led_aw9523b.h>

//Register
#define REG_INPUT_P0        0x00
#define REG_INPUT_P1        0x01
#define REG_OUTPUT_P0       0x02
#define REG_OUTPUT_P1       0x03
#define REG_CONFIG_P0       0x04
#define REG_CONFIG_P1       0x05
#define REG_INT_P0          0x06
#define REG_INT_P1          0x07
#define REG_ID              0x10
#define REG_CTRL            0x11
#define REG_WORK_MODE_P0    0x12
#define REG_WORK_MODE_P1    0x13
#define REG_DIM00           0x20
#define REG_DIM01           0x21
#define REG_DIM02           0x22
#define REG_DIM03           0x23
#define REG_DIM04           0x24
#define REG_DIM05           0x25
#define REG_DIM06           0x26
#define REG_DIM07           0x27
#define REG_DIM08           0x28
#define REG_DIM09           0x29
#define REG_DIM10           0x2a
#define REG_DIM11           0x2b
#define REG_DIM12           0x2c
#define REG_DIM13           0x2d
#define REG_DIM14           0x2e
#define REG_DIM15           0x2f
#define REG_SWRST           0x7f


static int s_i2c_bus = 1;
static int s_i2c_chip = 0x5b;

static void _int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void _short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void _int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void _addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            _short2char(data, addr);
            break;
        case 3:
            _int24_2char(data, addr);
            break;
        case 4:
            _int2char(data, addr);
            break;
    }
}

static int _gx_aw9523b_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};

    _addr2buf(addr_buf, addr, alen);

    msg[0].addr  = s_i2c_chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = s_i2c_chip;
    msg[1].flags = 0;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(s_i2c_bus);
    if (!i2c_dev) {
        printf("Failed to open I2C Chip 0x%x at Bus %d!\n", s_i2c_chip, s_i2c_bus);
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);
    return status != 0 ? -1 : 0;
}

static int _gx_i2c_aw9523b_i2c_recv(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};

    _addr2buf(addr_buf, addr, alen);

    msg[0].addr  = s_i2c_chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = s_i2c_chip;
    msg[1].flags = I2C_M_RD;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(s_i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }

    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);

    return status ? -1 : 0;
}

static int _write8(unsigned int addr, unsigned char data)
{
    unsigned char buf = data;
    return _gx_aw9523b_i2c_send(addr, 1, &buf, 1);
}


int LedAW9523BSetColor(int index, LED_COLOR rgb)
{
    switch(index){
        case 0:
           _write8(REG_DIM00, rgb.r);
           _write8(REG_DIM01, rgb.g);
           _write8(REG_DIM02, rgb.b);
           break;
        case 1:
           _write8(REG_DIM03, rgb.r);
           _write8(REG_DIM04, rgb.g);
           _write8(REG_DIM05, rgb.b);
           break;
        case 2:
           _write8(REG_DIM06, rgb.r);
           _write8(REG_DIM07, rgb.g);
           _write8(REG_DIM08, rgb.b);
           break;
        default:
           break;
    }

    return 0;
}

int LedAW9523BReadID(unsigned char *read_ID)
{
    _gx_i2c_aw9523b_i2c_recv(REG_ID,1,read_ID,1);

    if(*read_ID == 0x23)
        return 0;
    else
        return -1;
}

int LedAW9523BReset(void)
{
    _write8(REG_SWRST,0x00);

    return 0;
}

void LedAW9523BInit(void)
{
    LedAW9523BReset();
    _write8(REG_CTRL, 0x02);        //02: 0~(I MAX ×2/4)

    _write8(REG_WORK_MODE_P0, 0x00);//LED mode switch register P0
    _write8(REG_WORK_MODE_P1, 0x00);//LED mode switch register P1
}

int LedAW9523BSelectChip(int i2c_bus, int chip)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;
    s_i2c_bus = i2c_bus;
    s_i2c_chip = chip;

    return 0;
}
