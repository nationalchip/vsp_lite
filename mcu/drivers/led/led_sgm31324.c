/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_sgm31324.c: LED Driver for SGM31324
 *
 */
#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/led_sgm31324.h>

// I2C commands
#define REGISTER_CONTROL    0x00
#define FLASH_PERIOD        0x01
#define FLASH_ON_TIME1      0x02
#define FLASH_ON_TIME2      0x03
#define CHANNEL_CONTROL     0x04
#define RAMP_RATE           0x05
#define LED1_I_OUT          0x06
#define LED2_I_OUT          0x07
#define LED3_I_OUT          0x08
#define AUTO_BLINK          0x09

static int s_i2c_bus = 1;
static int s_i2c_chip = 0x30;

static SGM31324_CONFIG s_sgm31324_config = {
    .flash_source_period            = 6000,
    .flash_rise_time_slot           = 10000,
    .flash_fall_time_slot           = 10000,
    .time_slot_multiple             = SGM31324_TIME_SLOT_MULTIPLE_X8,
    .thousandth_on_time_channel_1   = 800,
    .thousandth_on_time_channel_2   = 800,
    .switch_mode_led_1              = SGM31324_LED_MODE_CHANNEL_1,
    .switch_mode_led_2              = SGM31324_LED_MODE_CHANNEL_1,
    .switch_mode_led_3              = SGM31324_LED_MODE_CHANNEL_1,
    .current_led_1                  = 24000,
    .current_led_2                  = 24000,
    .current_led_3                  = 24000,
    .auto_flash_flag                = 0,
    .register_control               = 0x1f,
    .on_off_led_all                 = 1
};

static void _int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void _short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void _int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void _addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            _short2char(data, addr);
            break;
        case 3:
            _int24_2char(data, addr);
            break;
        case 4:
            _int2char(data, addr);
            break;
    }
}

static int _gx_sgm31324_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    //int i2c_bus = I2C_BUS;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};
    //int chip = I2C_ADDR;
    _addr2buf(addr_buf, addr, alen);
    msg[0].addr  = s_i2c_chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;
    msg[1].addr  = s_i2c_chip;
    msg[1].flags = 0;
    msg[1].buf   = buf;
    msg[1].len   = len;
    i2c_dev = gx_i2c_open(s_i2c_bus);
    if (!i2c_dev) {
        printf("Failed to open I2C Chip 0x%x at Bus %d!\n", s_i2c_chip, s_i2c_bus);
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);
    return status != 0 ? -1 : 0;
}

static int _write8(unsigned int addr, unsigned char data)
{
    unsigned char buf = data;
    return _gx_sgm31324_i2c_send(addr, 1, &buf, 1);
}

//=================================================================================================

int LedSGM31324SetConfig(SGM31324_CONFIG *config)
{
    unsigned char regs[10] = {0};
    if (config->flash_source_period < 128 || config->flash_source_period > 16510) {
        printf("error : Flash period out of range (128ms-16510ms) !\n");
        return -1;
    }

    if (config->flash_rise_time_slot < 1500 || config->flash_rise_time_slot > 180000 ||\
        config->flash_fall_time_slot < 1500 || config->flash_fall_time_slot > 180000) {
        printf("error : Flash time slot out of range (1500ns-180000ns) !\n");
        return -1;
    }

    if (config->thousandth_on_time_channel_1 > 1000 || config->thousandth_on_time_channel_2 > 1000) {
        printf("error : Thousandth of on_time out of range (0-1000) !\n");
        return -1;
    }

    int time_slot_multiple = 0;
    switch (config->time_slot_multiple) {
        case SGM31324_TIME_SLOT_MULTIPLE_X8:
            time_slot_multiple = 8;
            break;
        case SGM31324_TIME_SLOT_MULTIPLE_X16:
            time_slot_multiple = 16;
            break;
        case SGM31324_TIME_SLOT_MULTIPLE_X32:
            time_slot_multiple = 32;
            break;
        case SGM31324_TIME_SLOT_MULTIPLE_X1:
            time_slot_multiple = 1;
            break;
        default :
            break;
    }

    if (config->flash_source_period * config->thousandth_on_time_channel_1 / 1000 < config->flash_rise_time_slot * time_slot_multiple / 1000 || \
        config->flash_source_period * (1000 - config->thousandth_on_time_channel_1) / 1000 < config->flash_fall_time_slot * time_slot_multiple / 1000 || \
        config->flash_source_period * config->thousandth_on_time_channel_2 / 1000 < config->flash_rise_time_slot * time_slot_multiple / 1000 || \
        config->flash_source_period * (1000 - config->thousandth_on_time_channel_2) / 1000 < config->flash_fall_time_slot * time_slot_multiple / 1000) {
        printf("warning : Please make sure time slot of rise less than on_time and time slot of fall less than off_time! \n");
    }

    if (config->current_led_1 < 125 || config->current_led_1 > 24000 || \
        config->current_led_1 < 125 || config->current_led_1 > 24000 || \
        config->current_led_1 < 125 || config->current_led_1 > 24000 ) {
        printf("error : Current of led out of range (125nA-24000nA) !\n");
        return -1;
    }

    memcpy(&s_sgm31324_config, config, sizeof(SGM31324_CONFIG));

    regs[0] |= s_sgm31324_config.time_slot_multiple << 6;
    regs[1] = 0;
    regs[1] |= s_sgm31324_config.flash_source_period / 128 > 2 ? config->flash_source_period / 128 - 2 : 0;
    regs[2] = 0;
    regs[2] |= s_sgm31324_config.thousandth_on_time_channel_1 / 4;
    regs[3] = 0;
    regs[3] |= s_sgm31324_config.thousandth_on_time_channel_2 / 4;
    regs[4] = 0;
    regs[4] |= (s_sgm31324_config.on_off_led_all << 6) | (s_sgm31324_config.switch_mode_led_1) | (s_sgm31324_config.switch_mode_led_2 << 2) | (s_sgm31324_config.switch_mode_led_3 << 4);
    regs[5] = 0;
    regs[5] |= (s_sgm31324_config.flash_fall_time_slot / 12000 << 4) | (s_sgm31324_config.flash_rise_time_slot / 12000);
    regs[6] = 0;
    regs[6] |= s_sgm31324_config.current_led_1 / 125;
    regs[7] = 0;
    regs[7] |= s_sgm31324_config.current_led_2 / 125;
    regs[8] = 0;
    regs[8] |= s_sgm31324_config.current_led_3 / 125;
    regs[9] = s_sgm31324_config.auto_flash_flag;

    if (s_sgm31324_config.register_control & 0x80) {
        regs[0] = s_sgm31324_config.register_control & 0x7f;
    }

    _write8(REGISTER_CONTROL    , regs[0]);
    _write8(AUTO_BLINK          , regs[9]);
    _write8(FLASH_PERIOD        , regs[1]);
    _write8(FLASH_ON_TIME1      , regs[2]);
    _write8(FLASH_ON_TIME2      , regs[3]);
    _write8(RAMP_RATE           , regs[5]);
    _write8(LED1_I_OUT          , regs[6]);
    _write8(LED1_I_OUT          , regs[7]);
    _write8(LED1_I_OUT          , regs[8]);
    _write8(CHANNEL_CONTROL     , regs[4]);

    return 0;
}

int LedSGM31324GetConfig(SGM31324_CONFIG *config) {
    memcpy(config, &s_sgm31324_config, sizeof(SGM31324_CONFIG));
    return 0;
}

int LedSGM31324Init(int i2c_bus, int i2c_chip)
{
    s_i2c_bus = i2c_bus;
    s_i2c_chip = i2c_chip;
    _write8(REGISTER_CONTROL, 0x1f);
    _write8(AUTO_BLINK      , 0);
    return 0;
}

int LedSGM31324Done(void)
{
    _write8(CHANNEL_CONTROL , 0x00);
    return 0;
}

int LedSGM31324SetColor(LED_COLOR rbg)
{
    _write8(LED1_I_OUT, rbg.r);
    _write8(LED2_I_OUT, rbg.g);
    _write8(LED3_I_OUT, rbg.b);

    char value = 0x55;
    if (rbg.r == 0)
        value &= 0xfc;
    if (rbg.g == 0)
        value &= 0xf3;
    if (rbg.b == 0)
        value &= 0xcf;

    _write8(CHANNEL_CONTROL , value);
    return 0;
}

int LedSGM31324SetBlink(LED_COLOR rgb , SGM31324_LED_BLINK_MODE mode)
{
    unsigned char regs[10] = {0x00,0x0f,0x80,0x00,0x6a,0x00,0xbf,0xbf,0xbf,0x00};
    regs[6] = rgb.r;
    regs[7] = rgb.g;
    regs[8] = rgb.b;
    switch(mode)
    {
        case SGM31324_LED_BLINK_ALWAYSON:
            regs[4] = 0x55;
            break;
        case SGM31324_LED_BLINK_ALWAYSOFF:
            regs[4] = 0x40;
            break;
        case SGM31324_LED_BLINK_SLOWLY:
            regs[0] = 0x20;
            regs[1] = 0x14;
            regs[5] = 0x85;
            break;
        case SGM31324_LED_BLINK_NORMAL:
            regs[5] = 0xaa;
            break;
        case SGM31324_LED_BLINK_FAST:
            regs[1] = 0x06;
            regs[2] = 0xc0;
            regs[5] = 0x66;
            break;
        default:
            printf("State Parameter error\n");
            break;
    }

    _write8(REGISTER_CONTROL    , regs[0]);
    _write8(AUTO_BLINK          , regs[9]);
    _write8(FLASH_PERIOD        , regs[1]);
    _write8(FLASH_ON_TIME1      , regs[2]);
    _write8(FLASH_ON_TIME2      , regs[3]);
    _write8(RAMP_RATE           , regs[5]);
    _write8(LED1_I_OUT          , regs[6]);
    _write8(LED2_I_OUT          , regs[7]);
    _write8(LED3_I_OUT          , regs[8]);
    _write8(CHANNEL_CONTROL     , regs[4]);

    return 0;
}
