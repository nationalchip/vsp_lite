/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_sn3193.c: LED Driver for SN3193
 *
 */

#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/led_sn3193.h>

/*
 * Reference resources:   https://github.com/Yoja939/SN3193
 */

// I2C commands
#define SHUTDOWN_REG            0x00
#define BREATING_CONTROL_REG    0x01
#define LED_MODE_REG            0x02
#define CURRENT_SETTING_REG     0x03
#define PWM_1_REG               0x04
#define PWM_2_REG               0x05
#define PWM_3_REG               0x06
#define PWM_UPDATE_REG          0x07
#define LED_CONTROL_REG         0x1D
#define RESET_REG               0x2F

static unsigned char _ledctrl = 0;
static int s_i2c_bus = 0;
static int s_i2c_chip = 0x68;

static void _int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void _short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void _int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void _addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            _short2char(data, addr);
            break;
        case 3:
            _int24_2char(data, addr);
            break;
        case 4:
            _int2char(data, addr);
            break;
    }
}

static int _gx_sn3193_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    //int i2c_bus = I2C_BUS;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};
    //int chip = I2C_ADDR;

    _addr2buf(addr_buf, addr, alen);

    msg[0].addr  = s_i2c_chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = s_i2c_chip;
    msg[1].flags = 0;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(s_i2c_bus);
    if (!i2c_dev) {
        printf("Failed to open I2C Chip 0x%x at Bus %d!\n", s_i2c_chip, s_i2c_bus);
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);
    return status != 0 ? -1 : 0;
}

static int _write8(unsigned int addr, unsigned char data)
{
    unsigned char buf = data;
    return _gx_sn3193_i2c_send(addr, 1, &buf, 1);
}

static void _PowerOn(void)
{
    _write8(RESET_REG           , 0x00);
    _write8(SHUTDOWN_REG        , 0x20);
    _write8(BREATING_CONTROL_REG, 0x00);
}

static void _PowerOff(void)
{
    _write8(SHUTDOWN_REG, 0x20);
}

// =0 PWM mode, =1 One slot programming mode
static void _SetMode(uint8_t mode)
{
    _write8(LED_MODE_REG, mode<<5);
}

// need to write to PWM_UPDATE_REG after setting up PWM_x_REG, LED_CONTROL_REG
static void _PwmUpdate(void)
{
    _write8(PWM_UPDATE_REG,0x00);
}

static void _SetAllPWM(uint8_t ch1_value,uint8_t ch2_value,uint8_t ch3_value)
{
    _write8(PWM_1_REG, ch1_value);
    _write8(PWM_2_REG, ch2_value);
    _write8(PWM_3_REG, ch3_value);
    _PwmUpdate();
}

// 0: 42mA, 1: 10mA, 2: 5mA, 3: 30mA, 4-7: 17.5mA
static void _SetCurrent(uint8_t level)
{
    _write8(CURRENT_SETTING_REG, level<<2);
}

static void _TurnOnAll(void)
{
    _ledctrl |= 0x7;
    _write8(LED_CONTROL_REG, _ledctrl);
}

static void _TurnOffAll(void)
{
    _ledctrl &= ~(0x7);
    _write8(LED_CONTROL_REG, _ledctrl);
}

//=================================================================================================

int LedSN3193Init(int i2c_bus, int i2c_chip)
{
    s_i2c_bus = i2c_bus;
    s_i2c_chip = i2c_chip;
    _PowerOn();
    _TurnOnAll();
    _SetMode(0);
    _SetCurrent(2);
    return 0;
}

int LedSN3193Done(void)
{
    _TurnOffAll();
    _PowerOff();
    return 0;
}

int LedSN3193SetColor(LED_COLOR rbg)
{
    _SetAllPWM(rbg.r, rbg.g, rbg.b);
    _PwmUpdate();
    return 0;
}
