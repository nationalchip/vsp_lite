/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_is31fl3208a.c: LED Driver for IS31FL3208A
 *
 */

#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/led_is31fl3208a.h>
#include <unistd.h>


#define IS31FL3208A_LED_MAX_NUM  6

static unsigned char init_flag = 0;
static unsigned char currentbuffer[3 * IS31FL3208A_LED_MAX_NUM] = {0};


enum IS31FL3208A_LED_SSE_SET { //Software Shutdown Enable
    IS31FL3208A_LED_SSE_SHUTDOWN = 0,
    IS31FL3208A_LED_SSE_NORMAL,
};

enum IS31FL3208A_LED_GEN_SET { //Global Enable
    IS31FL3208A_LED_GEN_NORMAL = 0,
    IS31FL3208A_LED_GEN_SHUTDOWN_ALL,
};


struct regsister {
    int sse;//set shutdown or enable
    int pwm;//pwm set start register
    int sync; //sync write value;
    int ctrl;//enable or disable set start register
    int g_ctrl;//enable or disable all led(Global Enable)
    int g_pwm_ctrl;//set pwm output frequency
    int reset;//reset all register
};

struct led_table {
    int index_vir;
    int index_phy;
    int ena;
    LED_LIGHT light;
    LED_COLOR rbg;
};


static struct
{
    int i2c_bus;
    int chip;
    int cnt;
    LED_FREQUENCE freq;
    struct led_table tbl[IS31FL3208A_LED_MAX_NUM];
    struct regsister regs;
} led_dev = {
    .i2c_bus = 1,
    .chip = 0x6c,
    .cnt = IS31FL3208A_LED_MAX_NUM,
    .freq = LED_FREQUENCE_23K,
    .tbl = {{0}},
    .regs = {
        .sse = 0x0 ,
        .pwm = 0x01 ,
        .sync = 0x13,
        .ctrl = 0x14,
        .g_ctrl = 0x26,
        .g_pwm_ctrl = 0x27,
        .reset = 0x2f
    },
};


static void int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            short2char(data, addr);
            break;
        case 3:
            int24_2char(data, addr);
            break;
        case 4:
            int2char(data, addr);
            break;
    }
}

static int gx_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    int i2c_bus = led_dev.i2c_bus;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};
    int chip = led_dev.chip;

    addr2buf(addr_buf, addr, alen);

    msg[0].addr  = chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = chip;
    msg[1].flags = 0;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);
    return status!=0?-1:0;
}

static int is31fl3208a_led_sync(void)
{
    unsigned char val = 0x00;
    int sync_reg = led_dev.regs.sync;
    return gx_i2c_send(sync_reg, 1, &val, 1);
}

int LedIS31FL3208ASetCurrent(int current)
{
    int i;
    unsigned char val = 0;
    int reset_reg = led_dev.regs.ctrl;
    int ret = 0;

    if(current >= 0x00 && current < 0x10){
      //when current is IOUT=0,currentbuffer nedd zero
      memset(currentbuffer, 0, sizeof(currentbuffer));
    }

    if(current < 0 || current > 0x13){
      current= 0x12;
    }

    val = current;

    for (i = led_dev.regs.ctrl; i < led_dev.regs.g_ctrl; i++) {
      reset_reg = i;
      ret |= gx_i2c_send(reset_reg, 1, &val, 1);
      if(ret !=0){
        printf("wirte reset_reg=0x%x error  ret=%d\n",reset_reg,ret);
        break;
      }

      ret |=is31fl3208a_led_sync();
      if(ret !=0){
        printf("wirte 0x13 error  status=%d\n",ret);
      }

    }

    return (ret != 0) ? -1 : 0;

}

int LedIS31FL3208AEnable(void)
{
    int status;
    unsigned char val = IS31FL3208A_LED_SSE_NORMAL;
    int reg = led_dev.regs.sse;
    status = gx_i2c_send(reg, 1, &val, 1);
    return status?-1:0;
}

int LedIS31FL3208ADisable(void)
{
    int status;
    unsigned char val = IS31FL3208A_LED_SSE_SHUTDOWN;
    int reg = led_dev.regs.sse;
    status = gx_i2c_send(reg, 1, &val, 1);
    return status?-1:0;
}

int LedIS31FL3208AReset(void)
{

    int i;
    unsigned char val = 0x00;
    struct led_table * tbl = led_dev.tbl;
    int cnt = led_dev.cnt;
    int reset_reg = led_dev.regs.reset;
    int ret = 0;

    for (i = 0; i < cnt; i++) {
        tbl[i].index_phy = i;
        tbl[i].index_vir = i;
        tbl[i].ena = 0;
        memset(&tbl[i].rbg, 0, sizeof(tbl[i].rbg));
    }

    val = 0;
    ret |= gx_i2c_send(reset_reg, 1, &val, 1);
    return (ret != 0) ? -1 : 0;

}


int LedIS31FL3208ASetFrequency(LED_FREQUENCE val)
{
    unsigned char buf = val;
    int g_pwm_ctrl = led_dev.regs.g_pwm_ctrl;
    int status = gx_i2c_send(g_pwm_ctrl, 1, &buf, 1);
    if (status == 0)
        led_dev.freq = val;
    return status?-1:0;
}

int LedIS31FL3208AInit(int i2c_bus, int chip)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;
    led_dev.i2c_bus = i2c_bus;
    led_dev.chip = chip;

    if(init_flag != 0)
    {
      printf("LedIS31FL3208AInit already inited.\n");
      return 0;
    } else {
      init_flag = 1;
    }

    if (LedIS31FL3208AReset() < 0)
    {
      printf(" LedIS31FL3208AReset error\n");
      init_flag= 0;
      return -1;
    }

    if (LedIS31FL3208AEnable() < 0)
    {
      printf(" LedIS31FL3208AEnable error \n");
      init_flag= 0;
      return -1;
    }

    if (LedIS31FL3208ASetFrequency(LED_FREQUENCE_23K))
    {
      printf(" LedIS31FL3208ASetFrequency errr \n");
      init_flag= 0;
      return -1;
    }

    if(LedIS31FL3208ASetCurrent(CURRENT_LEAVEL_2) < 0)
    {
      printf(" LedIS31FL3208ASet_ready errr \n");
      init_flag= 0;
      return -1;
    }

    return 0;
}

int LedIS31FL3208ASelectChip(int i2c_bus, int chip)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;
    led_dev.i2c_bus = i2c_bus;
    led_dev.chip = chip;

    return 0;
}


int LedIS31FL3208ASetColors(int start_index_vir, int num, LED_COLOR *rbg)
{
    int status = 0;
    int cnt = led_dev.cnt;
    int reg = led_dev.regs.pwm;
    int i = 0;

    if(num < 0 || num > cnt)
    {
		num = cnt;
    }

    if(start_index_vir >= num){
      printf(" LedIS31FL3208ASetColors led mumber error\n");
      start_index_vir = num -1;
    }

    for (i=start_index_vir; i<num; i++){
      reg = led_dev.regs.pwm + i * 3;
      status |=gx_i2c_send(reg, 1, (unsigned char*)&rbg[i], 3);
      if(status != 0){
        printf(" LedIS31FL3208ASetColors write  reg=0x%x error status=%d\n",reg,status);
      }
    }

    status |=is31fl3208a_led_sync();
    if(status !=0)
      printf(" LedIS31FL3208ASetColors write is31fl3208a_led_sync error status=%d\n",status);

    return status?-1:0;
}


int LedIS31FL3208ASetLights(int start_index_vir, int num, LED_LIGHT *light)
{
    int i, j;
    int status = 0;
    int cnt = led_dev.cnt;
    int reg = led_dev.regs.ctrl;
    unsigned char buf[3 * IS31FL3208A_LED_MAX_NUM] = {0};

    if(num < 0 || num > cnt)
    {
      num = cnt;
    }

    if(start_index_vir >= num){
      printf(" LedIS31FL3208ASetLights led mumber error\n");
      start_index_vir = num -1;
    }

    if(num == (start_index_vir + 1)){
      for(j = start_index_vir; j < num; j++){
        memset(buf + 3 * j, light[0], 3);
      }
    }else{
      for(j = 0; j < num; j++){
        memset(buf + 3 * j, light[j], 3);
      }

    }

    for(i=start_index_vir * 3; i < num * 3; i++){
      reg = led_dev.regs.ctrl + i;
      if(currentbuffer[i] != buf[i]){
        if(buf[i] < 0 || buf[i] > 0x13){
          buf[i] = 0x12;
        }

        currentbuffer[i] = buf[i];
        status |=gx_i2c_send(reg, 1, &buf[i], 1);
        if(status != 0)
          printf(" LedIS31FL3208ASetLights write reg=0x%x error status=%d\n",reg,status);

        if(i == num * 3 - 1){
          status |=is31fl3208a_led_sync();
          if(status != 0 )
            printf(" LedIS31FL3208ASetLights write is31fl3208a_led_sync error status=%d\n",status);
        }
      }

    }

    return status?-1:0;
}

int LedIS31FL3208ASetSwitchs(int index_vir, int num, LED_SWITCH *val)
{
    int i, j;
    int status = 0;
    int cnt = led_dev.cnt;
    int reg = led_dev.regs.ctrl;
    unsigned char buf[3 * IS31FL3208A_LED_MAX_NUM] = {0};

    if(num < 0 || num > cnt)
    {
      num = cnt;
    }

    if(index_vir >= num){
      printf(" LedIS31FL3208ASetSwitchs led mumber error\n");
      index_vir = num -1;
    }

    if(num == (index_vir + 1)){
      for(j = index_vir; j < num; j++){
        memset(buf + 3 * j, val[0], 3);
      }
    }else{
      for(j = 0; j < num; j++){
        memset(buf + 3 * j, val[j], 3);
      }
    }

    for(i = index_vir * 3; i < num * 3; i++){
      reg = led_dev.regs.ctrl + i;
      if(currentbuffer[i] != buf[i]){
        if(buf[i] < 0 || buf[i] > 0x13){
          buf[i] = 0x12;
        }

        if(buf[i] == LED_SWITCH_ON){
          buf[i] = 0x12;
        }

        currentbuffer[i] = buf[i];

        status |=gx_i2c_send(reg, 1, &buf[i], 1);
        if(status != 0)
          printf(" LedIS31FL3208ASetSwitchs write reg=0x%x error status=%d\n",reg,status);

        if(i == num * 3 - 1){
          status |=is31fl3208a_led_sync();
          if(status != 0 )
            printf(" LedIS31FL3208ASetSwitchs write is31fl3208a_led_sync error status=%d\n",status);
        }
      }
    }

    return status?-1:0;

}


int LedIS31FL3208ASetColor(int index_vir, LED_COLOR rbg)
{
    return LedIS31FL3208ASetColors(index_vir, index_vir+1, &rbg);
}

int LedIS31FL3208ASetLight(int index_vir, LED_LIGHT light)
{
    return LedIS31FL3208ASetLights(index_vir, index_vir+1, &light);
}

int LedIS31FL3208ASetSwitch(int index_vir, LED_SWITCH val)
{
    return LedIS31FL3208ASetSwitchs(index_vir, index_vir+1, &val);
}


