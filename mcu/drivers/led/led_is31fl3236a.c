/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_is31fl3236a.c: LED Driver for IS31FL3236A
 *
 */

#include <common.h>
#include <string.h>
#include <driver/i2c.h>
#include <driver/led_is31fl3236a.h>

#define IS31FL3236A_LED_MAX_NUM  12

enum IS31FL3236A_LED_SSE_SET { //Software Shutdown Enable
    IS31FL3236A_LED_SSE_SHUTDOWN = 0,
    IS31FL3236A_LED_SSE_NORMAL,
};

enum IS31FL3236A_LED_GEN_SET { //Global Enable
    IS31FL3236A_LED_GEN_NORMAL = 0,
    IS31FL3236A_LED_GEN_SHUTDOWN_ALL,
};

struct regsister {
    int sse;//set shutdown or enable
    int pwm;//pwm set start register
    int sync; //sync write value;
    int ctrl;//enable or disable set start register
    int g_ctrl;//enable or disable all led(Global Enable)
    int g_pwm_ctrl;//set pwm output frequency
    int reset;//reset all register
};

struct led_table {
    int index_vir;
    int index_phy;
    int ena;
    LED_LIGHT light;
    LED_COLOR rbg;
};

static struct
{
    int i2c_bus;
    int chip;
    int cnt;
    LED_FREQUENCE freq;
    struct led_table tbl[IS31FL3236A_LED_MAX_NUM];
    struct regsister regs;
} led_dev = {
    .i2c_bus = 0,
    .chip = 0x3c,
    .cnt = IS31FL3236A_LED_MAX_NUM,
    .freq = LED_FREQUENCE_22K,
    .tbl = {{0}},
    .regs = {
        .sse = 0x0 ,
        .pwm = 0x1 ,
        .sync = 0x25,
        .ctrl = 0x26,
        .g_ctrl = 0x4a,
        .g_pwm_ctrl = 0x4b,
        .reset = 0x4f
    },
};

static void int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            short2char(data, addr);
            break;
        case 3:
            int24_2char(data, addr);
            break;
        case 4:
            int2char(data, addr);
            break;
    }
}

static int gx_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    int i2c_bus = led_dev.i2c_bus;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};
    int chip = led_dev.chip;

    addr2buf(addr_buf, addr, alen);

    msg[0].addr  = chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = chip;
    msg[1].flags = 0;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }
    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);
    return status!=0?-1:0;
}

static int is31fl3236a_led_sync(void)
{
    unsigned char val = 0x00;
    int sync_reg = led_dev.regs.sync;
    return gx_i2c_send(sync_reg, 1, &val, 1);
}

//=================================================================================================

int LedIS31FL3236ASetColors(int start_index_vir, int num, LED_COLOR *rbg)
{
    int i, j, status;
    int cnt = led_dev.cnt;
    struct led_table *tbl = led_dev.tbl;
    int reg = led_dev.regs.pwm;

    for (i = 0; i < cnt; i++) {
        if (tbl[i].index_vir == start_index_vir)
            break;
    }
    if (i == cnt)
        return -1;

    reg += tbl[i].index_phy * 3;
    status = gx_i2c_send(reg, 1, (unsigned char*)rbg, num * 3);
    if (status != 0)
        return -1;

    status = is31fl3236a_led_sync();
    if (status == 0) {
        for (j = 0; j < num; j++){
            tbl[i++].rbg = rbg[j];
        }
    }

    return status?-1:0;
}

int LedIS31FL3236ASetLights(int start_index_vir, int num, LED_LIGHT *light)
{
    int i, j, index, status;
    struct led_table *tbl = led_dev.tbl;
    int cnt = led_dev.cnt;
    unsigned char buf[3 * IS31FL3236A_LED_MAX_NUM] = {0};
    int reg = led_dev.regs.ctrl;
    for (i = 0; i < cnt; i++) {
        if (tbl[i].index_vir == start_index_vir)
            break;
    }
    if (i == cnt)
        return -1;

    reg += tbl[i].index_phy * 3;
    index = i;
    for (j = 0; j < num; j++, index++){
        memset(buf + 3 * index, light[j] | tbl[index].ena, 3);
    }
    status = gx_i2c_send(reg, 1, buf + 3 * i, 3 * num);
    if (status != 0)
        return -1;

    status = is31fl3236a_led_sync();
    if (status == 0) {
        for (j = 0; j < num; j++){
            tbl[i++].light = light[j];
        }
    }

    return status?-1:0;
}

int LedIS31FL3236ASetSwitchs(int index_vir, int num, LED_SWITCH *val)
{
    int i, j, index, status;
    int cnt = led_dev.cnt;
    struct led_table *tbl = led_dev.tbl;
    int reg = led_dev.regs.ctrl;
    unsigned char buf[3 * IS31FL3236A_LED_MAX_NUM] = {0};

    for (i = 0; i < cnt; i++) {
        if (tbl[i].index_vir == index_vir)
            break;
    }
    if (i == cnt)
        return -1;

    reg += tbl[i].index_phy * 3;
    index = i;
    for (j = 0; j < num; j++, index++){
        memset(buf + 3 * index, val[j], 3);
    }
    status = gx_i2c_send(reg, 1, buf + 3 * i, 3 * num);

    if (status != 0)
        return -1;

    status = is31fl3236a_led_sync();
    if (status == 0){
        for (j = 0; j < num; j++) {
            tbl[i++].ena = val[j];
        }
    }

    return status?-1:0;
}

//-------------------------------------------------------------------------------------------------

int LedIS31FL3236ASetColor(int index_vir, LED_COLOR rbg)
{
    return LedIS31FL3236ASetColors(index_vir, 1, &rbg);
}

int LedIS31FL3236ASetLight(int index_vir, LED_LIGHT light)
{
    return LedIS31FL3236ASetLights(index_vir, 1, &light);
}

int LedIS31FL3236ASetSwitch(int index_vir, LED_SWITCH val)
{
    return LedIS31FL3236ASetSwitchs(index_vir, 1, &val);
}

//-------------------------------------------------------------------------------------------------

int LedIS31FL3236AEnable(void)
{
    int status;
    unsigned char val = IS31FL3236A_LED_SSE_NORMAL;
    int reg = led_dev.regs.sse;
    status = gx_i2c_send(reg, 1, &val, 1);
    return status?-1:0;
}

int LedIS31FL3236ADisable(void)
{
    int status;
    unsigned char val = IS31FL3236A_LED_SSE_SHUTDOWN;
    int reg = led_dev.regs.sse;
    status = gx_i2c_send(reg, 1, &val, 1);
    return status?-1:0;
}

//-------------------------------------------------------------------------------------------------
//连续两次对4F寄存器操作复位有概率导致I2C卡死，改成手动对各个寄存器进行复位
int LedIS31FL3236AReset(void)
{
    int i;
    unsigned char val = 0x00;
    struct led_table * tbl = led_dev.tbl;
    int cnt = led_dev.cnt;
    int reset_reg = led_dev.regs.reset;
    int ret = 0;
    for (i = 0; i < cnt; i++) {
        tbl[i].index_phy = i;
        tbl[i].index_vir = i;
        tbl[i].ena = 0;
        memset(&tbl[i].rbg, 0, sizeof(tbl[i].rbg));
    }

    for (i = 0; i <= led_dev.regs.sync; i++) {
        reset_reg = i;
        val = 0;
        ret |= gx_i2c_send(reset_reg, 1, &val, 1);
    }
    for (i = led_dev.regs.ctrl; i < led_dev.regs.g_ctrl; i++) {
        reset_reg = i;
        val = 1;
        ret |= gx_i2c_send(reset_reg, 1, &val, 1);
    }

    reset_reg = led_dev.regs.g_ctrl;
    val = 0;
    ret |= gx_i2c_send(reset_reg, 1, &val, 1);

    reset_reg = led_dev.regs.sse;
    val = 1;
    ret |= gx_i2c_send(reset_reg, 1, &val, 1);
    return (ret != 0) ? -1 : 0;
}

int LedIS31FL3236ASetFrequency(LED_FREQUENCE val)
{
    unsigned char buf = val;
    int g_pwm_ctrl = led_dev.regs.g_pwm_ctrl;
    int status = gx_i2c_send(g_pwm_ctrl, 1, &buf, 1);
    if (status == 0)
        led_dev.freq = val;
    return status?-1:0;
}

int LedIS31FL3236AInit(int i2c_bus, int chip)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;
    led_dev.i2c_bus = i2c_bus;
    led_dev.chip = chip;
    if (LedIS31FL3236AReset() < 0)
        return -1;
    if (LedIS31FL3236AEnable() < 0)
        return -1;
    if (LedIS31FL3236ASetFrequency(LED_FREQUENCE_22K))
        return -1;
    return 0;
}

int LedIS31FL3236ASelectChip(int i2c_bus, int chip)
{
    if (i2c_bus < 0 || i2c_bus > 1)
        return -1;
    led_dev.i2c_bus = i2c_bus;
    led_dev.chip = chip;

    return 0;
}
