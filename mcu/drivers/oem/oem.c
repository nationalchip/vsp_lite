/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * oem.c: OEM API
 */

#include <common.h>
#include <base_addr.h>
#include <string.h>

#include <driver/oem.h>
#include <driver/clock.h>
#include <driver/spi_flash.h>
#include <driver/delay.h>


#define USER_DATA_OFFSET (OEM_OFFSET + sizeof(OEM_INFO))

#define LOG_TAG "[Driver-Oem]"

int OemWrite(OEM_INFO *oem_info)
{
    USER_DATA user_data;

    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return 1;
    }

    SpiFlashRead(USER_DATA_OFFSET, (unsigned char *)&user_data, sizeof(USER_DATA));

    SpiFlashErase(OEM_OFFSET, FLASH_BLOCK_SIZE);

    SpiFlashWrite(OEM_OFFSET, (unsigned char *)oem_info, sizeof(OEM_INFO));
    SpiFlashWrite(USER_DATA_OFFSET, (unsigned char *)&user_data, sizeof(USER_DATA));

    return 0;
}

int OemRead(OEM_INFO *oem_info)
{
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return 1;
    }

    SpiFlashRead(OEM_OFFSET, (unsigned char *)oem_info, sizeof(OEM_INFO));

    return 0;
}

int UserDataWrite(USER_DATA *user_data)
{
    OEM_INFO oem_info;

    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return 1;
    }

    SpiFlashRead(OEM_OFFSET, (unsigned char *)&oem_info, sizeof(OEM_INFO));

    SpiFlashErase(OEM_OFFSET, FLASH_BLOCK_SIZE);

    SpiFlashWrite(OEM_OFFSET, (unsigned char *)&oem_info, sizeof(OEM_INFO));
    SpiFlashWrite(USER_DATA_OFFSET, (unsigned char *)user_data, sizeof(USER_DATA));

    return 0;
}

int UserDataRead(USER_DATA *user_data)
{
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return 1;
    }

    SpiFlashRead(USER_DATA_OFFSET, (unsigned char *)user_data, sizeof(USER_DATA));

    return 0;
}

