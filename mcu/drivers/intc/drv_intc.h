/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * drv_intc.h: Interrupt Controller Driver
 *
 */

#ifndef __DRV_INTC_H__
#define __DRV_INTC_H__

#define NR_IRQS                     64
#define NR_FIQS                     64

/*
 * Base Address : interrupt
 */

#define INTC_IRQ_STATUS             0x00
#define INTC_IRQ_PENDSTATUS         0x10
#define INTC_IRQ_ENSET              0x20
#define INTC_IRQ_ENABLECLR          0x30
#define INTC_IRQ_ENABLEINT          0x40
#define INTC_IRQ_MASK               0x50	/* INTMASK register */

#define INTC_IRQ_STATUS_HI          0x04
#define INTC_IRQ_PENDSTATUS_HI      0x14
#define INTC_IRQ_ENSET_HI           0x24
#define INTC_IRQ_ENABLECLR_HI       0x34
#define INTC_IRQ_ENABLEINT_HI       0x44
#define INTC_IRQ_MASK_HI            0x54

#define INTX_IRQ_NSOURCE_03_00      0x60
#define INTX_IRQ_NSOURCE_07_04      0x64
#define INTX_IRQ_NSOURCE_11_08      0x68
#define INTX_IRQ_NSOURCE_15_12      0x6c
#define INTX_IRQ_NSOURCE_19_16      0x70
#define INTX_IRQ_NSOURCE_23_20      0x74
#define INTX_IRQ_NSOURCE_27_24      0x78
#define INTX_IRQ_NSOURCE_31_28      0x7c
#define INTX_IRQ_NSOURCE_35_32      0x80
#define INTX_IRQ_NSOURCE_39_36      0x84
#define INTX_IRQ_NSOURCE_43_40      0x88
#define INTX_IRQ_NSOURCE_47_44      0x8c
#define INTX_IRQ_NSOURCE_51_48      0x90
#define INTX_IRQ_NSOURCE_55_52      0x94
#define INTX_IRQ_NSOURCE_59_56      0x98
#define INTC_IRQ_NSOURCE_63_60      0x9c

#define INTC_FIQ_STATUS             0x100
#define INTC_FIQ_PENDSTATUS         0x110
#define INTC_FIQ_ENSET              0x120
#define INTC_FIQ_ENABLECLR          0x130
#define INTC_FIQ_ENABLEINT          0x140
#define INTC_FIQ_MASK               0x150	/* INTMASK register */

#define INTC_FIQ_STATUS_HI          0x104
#define INTC_FIQ_PENDSTATUS_HI      0x114
#define INTC_FIQ_ENSET_HI           0x124
#define INTC_FIQ_ENABLECLR_HI       0x134
#define INTC_FIQ_ENABLEINT_HI       0x144
#define INTC_FIQ_MASK_HI            0x154

#define INTX_FIQ_NSOURCE03_00       0x160
#define INTX_FIQ_NSOURCE07_04       0x164
#define INTX_FIQ_NSOURCE11_08       0x168
#define INTX_FIQ_NSOURCE15_12       0x16c
#define INTX_FIQ_NSOURCE19_16       0x170
#define INTX_FIQ_NSOURCE23_20       0x174
#define INTX_FIQ_NSOURCE27_24       0x178
#define INTX_FIQ_NSOURCE31_28       0x17c
#define INTX_FIQ_NSOURCE35_32       0x180
#define INTX_FIQ_NSOURCE39_36       0x184
#define INTX_FIQ_NSOURCE43_40       0x188
#define INTX_FIQ_NSOURCE47_44       0x18c
#define INTX_FIQ_NSOURCE51_48       0x190
#define INTX_FIQ_NSOURCE55_52       0x194
#define INTX_FIQ_NSOURCE59_56       0x198
#define INTC_FIQ_NSOURCE63_60       0x19c

typedef struct {
    unsigned int status[2];         // 0x00
    unsigned int _1[2];
    unsigned int pend[2];           // 0x10
    unsigned int _2[2];
    unsigned int enset[2];          // 0x20
    unsigned int _3[2];
    unsigned int enclear[2];        // 0x30
    unsigned int _4[2];
    unsigned int en[2];             // 0x40
    unsigned int _5[2];
    unsigned int mask[2];           // 0x50
    unsigned int _6[2];
    unsigned int source[16];
} INTC_REGS;

#endif  /* __DRV_INTC_H__ */
