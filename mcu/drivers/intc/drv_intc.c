/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * drv_intc.c: Interrupt Controller Driver
 *
 */

#include <common.h>
#include <types.h>
#include <stdio.h>

#include <base_addr.h>
#include <driver/irq.h>

#include "drv_intc.h"

static unsigned int irq_num[NR_IRQS] = {0};
static unsigned int fiq_num[NR_IRQS] = {0};
static unsigned int irq_channel[NR_IRQS] = {0};

//
// interrupt / fast interrupt control
//
#define IRQ_CHANNEL_NUM  (INTC_IRQ_NSOURCE_63_60 - INTX_IRQ_NSOURCE_03_00 + 4)

static void _request_irq(int irq)
{
	int channel;
	unsigned int value;
	unsigned int base_addr = MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_03_00;
	int i, j;

	// get free channel
	for (channel = 0; channel < IRQ_CHANNEL_NUM; channel++)
		if (irq_channel[channel] == 0xff)
			break;

	if (channel >= IRQ_CHANNEL_NUM) {
		printf("ERROR! File %s, Line %d, channel = %d\n", __FILE__, __LINE__, channel);
		return;
	}

	// join channel and irq
	irq_channel[channel] = irq;
	irq_num[irq] = channel;

	// write intc register
	i = channel / 4;
	j = channel % 4;
	value = readl(base_addr + i * 4);
	value &= ~(0xff << (j * 8));
	value |= (irq << (j * 8));
	writel(value, base_addr + i * 4);
}

void gx_irq_init(void)
{
	int i;

	for (i = 0; i < IRQ_CHANNEL_NUM; i++)
		irq_channel[i] = 0xff;
	for (i = 0; i < IRQ_CHANNEL_NUM / 4; i++)
		writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_03_00 + i * 4);

    _request_irq(MCU_IRQ_USB_WU);    /* 41 */
    _request_irq(MCU_IRQ_USB);       /* 42 */
    _request_irq(MCU_IRQ_USB_DMA);   /* 43 */

    _request_irq(MCU_IRQ_DW_UART0);  /* 00 */
    _request_irq(MCU_IRQ_DW_UART1);  /* 01 */
    _request_irq(MCU_IRQ_DW_I2C0);   /* 02 */
    _request_irq(MCU_IRQ_DW_I2C1);   /* 03 */
    _request_irq(MCU_IRQ_IRR);       /* 04 */
    _request_irq(MCU_IRQ_RTC);       /* 05 */

    _request_irq(MCU_IRQ_DW_SPI);    /* 08 */
    _request_irq(MCU_IRQ_WDT);       /* 09 */
    _request_irq(MCU_IRQ_COUNTER0);  /* 10 */
    _request_irq(MCU_IRQ_COUNTER1);  /* 11 */
    _request_irq(MCU_IRQ_COUNTER2);  /* 12 */
    _request_irq(MCU_IRQ_COUNTER3);  /* 13 */

    _request_irq(MCU_IRQ_GPIO0);     /* 16 */
    _request_irq(MCU_IRQ_GPIO1);     /* 17 */
    _request_irq(MCU_IRQ_GPIO2);     /* 18 */
    _request_irq(MCU_IRQ_DMA);       /* 19 */

    _request_irq(MCU_IRQ_A7_NORM);   /* 32 */
    _request_irq(MCU_IRQ_A7_FAST);   /* 33 */
    _request_irq(MCU_IRQ_SNPU);      /* 34 */
    _request_irq(MCU_IRQ_AUDIO_IN);  /* 35 */
    _request_irq(MCU_IRQ_A7_TO_MCU); /* 36 */
    _request_irq(MCU_IRQ_MTC);       /* 37 */
    _request_irq(MCU_IRQ_A7_WDT);    /* 38 */
    _request_irq(MCU_IRQ_DSP);       /* 39 */
    _request_irq(MCU_IRQ_AUDIO_OUT); /* 40 */

    /* enable all interrupts */
    writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_ENABLEINT);
    writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_ENABLEINT_HI);
    /* mask all interrupts */
    writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_MASK);
    writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_MASK_HI);

    writel(0x7, CK802_VIC_ISER);
    writel(0xc0 | (0x80 << 8) | (0x40 << 16), CK802_VIC_IPR0);
}

void gx_fiq_init(void)
{

}

void gx_disable_all_interrupt(void)
{
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_MASK);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_MASK_HI);

	writel(0x0, MCU_REG_BASE_INTC + INTC_IRQ_ENABLEINT);
	writel(0x0, MCU_REG_BASE_INTC + INTC_IRQ_ENABLEINT_HI);

	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_03_00);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_07_04);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_11_08);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_15_12);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_19_16);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_23_20);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_27_24);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_31_28);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_35_32);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_39_36);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_43_40);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_47_44);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_51_48);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_55_52);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTX_IRQ_NSOURCE_59_56);
	writel(0xFFFFFFFF, MCU_REG_BASE_INTC + INTC_IRQ_NSOURCE_63_60);
}

//=================================================================================================

SECTION_SRAM_TEXT void gx_mask_irq(unsigned int irq)
{
	unsigned int reg = 0;
	unsigned int channel = irq_num[irq];

	if (channel >= 32) {
		reg = readl(MCU_REG_BASE_INTC + INTC_IRQ_MASK_HI) | 1 << (channel - 32);
		writel(reg, MCU_REG_BASE_INTC + INTC_IRQ_MASK_HI);
	}
	else {
		reg = readl(MCU_REG_BASE_INTC + INTC_IRQ_MASK) | 1 << channel;
		writel(reg, MCU_REG_BASE_INTC + INTC_IRQ_MASK);
	}
}

SECTION_SRAM_TEXT void gx_unmask_irq(unsigned int irq)
{
	unsigned int reg = 0;
	unsigned int channel = irq_num[irq];

	if (channel >= 32) {
		reg = readl(MCU_REG_BASE_INTC + INTC_IRQ_MASK_HI) & ~(1 << (channel - 32));
		writel(reg, MCU_REG_BASE_INTC + INTC_IRQ_MASK_HI);
	}
	else {
		reg = readl(MCU_REG_BASE_INTC + INTC_IRQ_MASK) & ~(1 << channel);
		writel(reg, MCU_REG_BASE_INTC + INTC_IRQ_MASK);
	}
}

SECTION_SRAM_TEXT void gx_mask_fiq(unsigned int fiq)
{
	unsigned int reg = 0;
	unsigned int channel = fiq_num[fiq];

	if (channel >= 32) {
		reg = readl(MCU_REG_BASE_INTC + INTC_FIQ_MASK_HI) | 1 << (channel - 32);
		writel(reg, MCU_REG_BASE_INTC + INTC_FIQ_MASK_HI);
	}
	else {
		reg = readl(MCU_REG_BASE_INTC + INTC_FIQ_MASK) | 1 << channel;
		writel(reg, MCU_REG_BASE_INTC + INTC_FIQ_MASK);
	}
}

SECTION_SRAM_TEXT void gx_unmask_fiq(unsigned int fiq)
{
	unsigned int reg = 0;
	unsigned int channel = fiq_num[fiq];

	if (channel >= 32) {
		reg = readl(MCU_REG_BASE_INTC + INTC_FIQ_MASK_HI) & ~(1 << (channel - 32));
		writel(reg, MCU_REG_BASE_INTC + INTC_FIQ_MASK_HI);
	}
	else {
		reg = readl(MCU_REG_BASE_INTC + INTC_FIQ_MASK) & ~(1 << channel);
		writel(reg, MCU_REG_BASE_INTC + INTC_FIQ_MASK);
	}
}

//=================================================================================================
//
// interrupt handler
//
static struct {
	irq_handler_t handler;
	void *pdata;
} s_irq_info[NR_IRQS];

void gx_enable_irq(void)
{
    writel(0x1, CK802_VIC_ISER);
}

void gx_disable_irq(void)
{
    writel(0x1, CK802_VIC_ICER);
}

int gx_irqenabled(void)
{
    return readl(CK802_VIC_ISER) & 0x1;
}

void gx_request_irq(int irq, irq_handler_t handler, void *pdata)
{
	s_irq_info[irq_num[irq]].handler = handler;
	s_irq_info[irq_num[irq]].pdata = pdata;
	gx_unmask_irq(irq);
}

void gx_free_irq(int irq)
{
	gx_mask_irq(irq);
	s_irq_info[irq_num[irq]].handler = 0;
	s_irq_info[irq_num[irq]].pdata = 0;
}

SECTION_SRAM_TEXT void gx_irq_handler(void)
{
	int i = 0;
	unsigned int mask = 1;
	unsigned int status = 0;
	unsigned int irq = 0;

	status = *(volatile unsigned int*)(MCU_REG_BASE_INTC + INTC_IRQ_STATUS);

	for (i=0; status; ++i, mask<<=1) {
		if (status & mask) {
			status &= ~mask;
			irq = irq_channel[i];
			gx_mask_irq(irq);	/* If no handler, left it masked */
			if (s_irq_info[i].handler)
			{
				s_irq_info[i].handler(irq, s_irq_info[i].pdata);
				gx_unmask_irq(irq);
			}
		}
	}

	/* check status II for 64 INTs */
	if (NR_IRQS <= 32)
		return;

	status = *(volatile unsigned int*)(MCU_REG_BASE_INTC + INTC_IRQ_STATUS_HI);

	mask = 1;
	for (i=32; status; ++i, mask<<=1) {
		if (status & mask) {
			status &= ~mask;
			irq = irq_channel[i];
			gx_mask_irq(irq);	/* If no handler, left it masked */
			if (s_irq_info[i].handler)
			{
				s_irq_info[i].handler(irq, s_irq_info[i].pdata);
				gx_unmask_irq(irq);
			}
		}
	}
}

//=================================================================================================
//
// fast interrupt handler
//

static struct {
	fiq_handler_t handler;
	void *pdata;
} s_fiq_info[NR_IRQS];

void gx_enable_fiq(void)
{

}

void gx_disable_fiq(void)
{
}

void gx_request_fiq(int fiq, fiq_handler_t handler, void *pdata)
{
	s_fiq_info[fiq].handler = handler;
	s_fiq_info[fiq].pdata = pdata;
	gx_unmask_fiq(fiq);
}

void gx_free_fiq(int fiq)
{
	gx_mask_fiq(fiq);
	s_fiq_info[fiq].handler = NULL;
	s_fiq_info[fiq].pdata = NULL;
}

SECTION_SRAM_TEXT void gx_fiq_handler(void)
{
	int i;
	unsigned int mask;
	unsigned int status = readl(MCU_REG_BASE_INTC + INTC_FIQ_STATUS);

	for (i = 0, mask = 1; status != 0; ++i, mask <<= 1) {
		if (status & mask) {
			status &= ~mask;
			gx_mask_fiq(i);	/* If no handler, left it masked */
			if (s_fiq_info[i].handler) {
				s_fiq_info[i].handler(i, s_fiq_info[i].pdata);
				gx_unmask_fiq(i);
			}
		}
	}
}

//=================================================================================================

#define FIQ_TYPE        0x01
#define IRQ_TYPE        0x02
#define WAKE_TYPE       0x04

static struct {
    wiq_handler_t    handler;
    void            *pdata;
} s_wake_info;

void gx_request_wake(unsigned int wake_source, wiq_handler_t handler, void *pdata)
{
    s_wake_info.handler = handler;
    s_wake_info.pdata   = pdata;

    writel(WAKE_TYPE, CK802_VIC_IWER);
    writel(wake_source, MCU_REG_BASE_WAKE_SOURCE);
    __asm__ __volatile__ ("stop\n nop\n nop\n nop\n nop\n nop\n");
}

SECTION_SRAM_TEXT void gx_wake_handler(void)
{
    unsigned int status = readl(MCU_REG_BASE_WAKE_STATUS);
    writel(0x0, MCU_REG_BASE_WAKE_SOURCE);
    if (s_wake_info.handler)
        s_wake_info.handler(status, s_wake_info.pdata);
}

//=================================================================================================


SECTION_SRAM_TEXT void HandleUndef(void)
{
	while (1);
}

SECTION_SRAM_TEXT void HandleSWI(void)
{
	while (1);
}

SECTION_SRAM_TEXT void HandlePabort(void)
{
	while (1);
}


SECTION_SRAM_TEXT void HandleDabort(void)
{
	while (1);
}


