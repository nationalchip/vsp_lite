#ifndef __DW_DMAC_AXI_H__
#define __DW_DMAC_AXI_H__

#define DW_DMAC_MAX_CH  6

#define DMA_BASE                      0xa0800000
#define DMA_CFGREG                    (DMA_BASE + 0x10)
#define DMA_CHENREG                   (DMA_BASE + 0x18)
#define DMA_INTSTATUSREG              (DMA_BASE + 0x30)
#define DMA_INTSTATEREG               (DMA_BASE + 0x50)
#define DMA_RESETREG                  (DMA_BASE + 0x58)

#define DMA_CHXSAR(x)                 (DMA_BASE + 0x100 + (x << 8))
#define DMA_CHXDAR(x)                 (DMA_BASE + 0x108 + (x << 8))
#define DMA_CHXBLKTS(x)               (DMA_BASE + 0x110 + (x << 8))
#define DMA_CHXCTL(x)                 (DMA_BASE + 0x118 + (x << 8))
#define DMA_CHXCFG(x)                 (DMA_BASE + 0x120 + (x << 8))
#define DMA_CHXSTATUS(x)              (DMA_BASE + 0x130 + (x << 8))
#define DMA_CHXSWHSSRC(x)             (DMA_BASE + 0x138 + (x << 8))
#define DMA_CHXSWHSDST(x)             (DMA_BASE + 0x140 + (x << 8))
#define DMA_CHXINTSTATUSEN(x)         (DMA_BASE + 0x180 + (x << 8))
#define DMA_CHXINTSTATUS(x)           (DMA_BASE + 0x188 + (x << 8))
#define DMA_CHXINTSIGNALEN(x)         (DMA_BASE + 0x190 + (x << 8))
#define DMA_CHXINTCLEAR(x)            (DMA_BASE + 0x198 + (x << 8))

/* DMAC_CFG */
#define DMAC_EN_POS            0
#define DMAC_EN_MASK           (DMA_BIT(DMAC_EN_POS))

#define INT_EN_POS             1
#define INT_EN_MASK            (DMA_BIT(INT_EN_POS))

#define DMAC_CHAN_EN_SHIFT     0
#define DMAC_CHAN_EN_WE_SHIFT  8

#define DMAC_CHAN_SUSP_SHIFT     16
#define DMAC_CHAN_SUSP_WE_SHIFT  24

/* CH_CTL_H */
#define CH_CTL_L_ARLEN_EN_POS    38
#define CH_CTL_L_ARLEN_POS       39
#define CH_CTL_L_AWLEN_EN_POS    47
#define CH_CTL_L_AWLEN_POS       48



#endif
