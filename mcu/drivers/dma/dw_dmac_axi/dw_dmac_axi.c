#include <common.h>
#include <driver/dma.h>
#include <driver/delay.h>
#include <driver/dma_axi.h>
#include <base_addr.h>
#include <driver/irq.h>

#include "dw_dmac_axi.h"

//static DW_DMAC_CH_CONFIG channel_config[DW_DMAC_MAX_CH];

#define CPU_ADDR_TO_DMA(_addr) ({                                 \
	uint32_t __addr = (uint32_t)(_addr);                          \
	__addr - (__addr >= 0xa0000000 ? 0xa0000000 : 0x40000000);    \
})

static void (*dw_dma_callback[DW_DMAC_MAX_CH])(void *);
static void *dw_dma_callback_param[DW_DMAC_MAX_CH];

static int is_debug = 0;

typedef union {
	unsigned long long ll;
	struct {
		uint32_t low;
		uint32_t high;
	};
} DWunion;

SECTION_SRAM_TEXT
static inline unsigned long long lshift(unsigned long long u, int b)
{
	const DWunion uu = {.ll = u};
	const int bm = 32 - b;
	DWunion w;

	if (b == 0)
		return u;

	if (bm <= 0) {
		w.low = 0;
		w.high = (uint32_t) uu.low << -bm;
	} else {
		uint32_t carries = (uint32_t) uu.low >> bm;

		w.low = (uint32_t) uu.low << b;
		w.high = ((uint32_t) uu.high << b) | carries;
	}

	return w.ll;
}

SECTION_SRAM_TEXT
static inline unsigned long long dma_read_reg(unsigned int reg)
{
	unsigned long long value;
	value = *((volatile unsigned long long *)(reg));
	return value;
}

SECTION_SRAM_TEXT
static inline void dma_write_reg(unsigned int reg, unsigned long long value)
{
	*((volatile unsigned long long *)(reg)) = value;
	return ;
}

SECTION_SRAM_TEXT
static inline int dma_set_bit_value(unsigned long long *reg_value, int bit_start,
	unsigned long long mask,unsigned long set_value)
{
	unsigned long long temp=0;
	unsigned long long value = (unsigned long long)set_value;
	*reg_value &= ~mask;
	if(lshift(value, bit_start) > mask) {
		printf("error value\n");
		return -1;
	}
	temp = lshift(value, bit_start) & mask;
	*reg_value |= temp;
	return 0;
}

#if 0
static void dw_chan_clean(int ch)
{
	//CHAN_CLEAN(&priv->dw_dmac->CLEAR, |=,  0x01  << ch );
	//CHAN_CLEAN(&priv->dw_dmac->RAW,   &=, ~(0x01 << ch));
	//CHAN_CLEAN(&priv->dw_dmac->MASK,  |=,  0x101 << ch);
}


static void display_interrupt(int dma_channel)
{
	printf("channel %d isr int status en is %llx\n",dma_channel, dma_read_reg(DMA_CHXINTSTATUSEN(dma_channel)));
	printf("channel %d isr int status is %llx\n",dma_channel, dma_read_reg(DMA_CHXINTSTATUS(dma_channel)));
	printf("channel %d isr int signal en is %llx\n",dma_channel, dma_read_reg(DMA_CHXINTSIGNALEN(dma_channel)));
	printf("channel %d isr int clear is %llx\n",dma_channel, dma_read_reg(DMA_CHXINTCLEAR(dma_channel)));	
}
#endif

SECTION_SRAM_TEXT
static void dma_reset(void)
{
	unsigned long long temp;
	dma_write_reg(DMA_RESETREG, 0x01);
	while(1) {
		temp = dma_read_reg(DMA_RESETREG);
		if(0 == temp)
			break;
	}
}

SECTION_SRAM_TEXT
static void display_regs(int dma_channel)
{
	printf("DMA_CHENREG is %llx\n", dma_read_reg(DMA_CHENREG));
	printf("DMA_CHXINTSTATUS is %llx\n", dma_read_reg(DMA_CHXINTSTATUS(dma_channel)));
	printf("DMA_CHXCTL is %llx\n",dma_read_reg(DMA_CHXCTL(dma_channel)));
	printf("DMA_CHXCFG reg is %llx\n",dma_read_reg(DMA_CHXCFG(dma_channel)));
	printf("DMA_CHENREG reg is %llx\n",dma_read_reg(DMA_CHENREG));
	printf("DMA_CHXSAR is %llx\n",dma_read_reg(DMA_CHXSAR(dma_channel)));
	printf("DMA_CHXDAR is %llx\n",dma_read_reg(DMA_CHXDAR(dma_channel)));
}

SECTION_SRAM_TEXT
static void dw_dma_enable(void)
{
	unsigned long long val;

	val = dma_read_reg(DMA_CFGREG);
	val |= (INT_EN_MASK|DMAC_EN_MASK);
	dma_write_reg(DMA_CFGREG, val);
}

#if 0
static void dw_dma_disable(void)
{
	unsigned long long val;

	val = dma_read_reg(DMA_CFGREG);
	val &= ~INT_EN_MASK;
	dma_write_reg(DMA_CFGREG, val);
}
#endif

SECTION_SRAM_TEXT
static void dw_dma_channel_enable(int dma_channel)
{
	unsigned long long reg_value;

	reg_value = dma_read_reg(DMA_CHENREG);
	reg_value |= lshift(((1<<DMAC_CHAN_EN_SHIFT) |(1<<DMAC_CHAN_EN_WE_SHIFT)), dma_channel);
	dma_write_reg(DMA_CHENREG, reg_value);
}

SECTION_SRAM_TEXT
static void dw_dma_channel_suspend(int dma_channel)
{
	unsigned long long reg_value;

	reg_value = dma_read_reg(DMA_CHENREG);
	reg_value |= lshift(((1<<DMAC_CHAN_SUSP_SHIFT) |(1<<DMAC_CHAN_SUSP_WE_SHIFT)), dma_channel);
	dma_write_reg(DMA_CHENREG, reg_value);

}

SECTION_SRAM_TEXT
static void dw_dma_channel_disable(int dma_channel)
{
	unsigned long long reg_value;
	reg_value = dma_read_reg(DMA_CHENREG);
	reg_value &= ~lshift((1<<DMAC_CHAN_EN_SHIFT), dma_channel);
	reg_value |= lshift((1<<DMAC_CHAN_EN_WE_SHIFT), dma_channel);

	dma_write_reg(DMA_CHENREG, reg_value);

}

SECTION_SRAM_TEXT
static void dw_dma_enable_int(int dma_channel)
{
	unsigned long long reg_value;

	reg_value = dma_read_reg(DMA_CHXINTSIGNALEN(dma_channel));
	reg_value = DWAXIDMAC_IRQ_DMA_TRF;
	dma_write_reg(DMA_CHXINTSIGNALEN(dma_channel),reg_value);
}

SECTION_SRAM_TEXT
static void dw_dma_disable_int(int dma_channel)
{
	unsigned long long reg_value;

	reg_value = dma_read_reg(DMA_CHXINTSIGNALEN(dma_channel));
	reg_value &= ~(DWAXIDMAC_IRQ_DMA_TRF);
	dma_write_reg(DMA_CHXINTSIGNALEN(dma_channel),reg_value);
}

SECTION_SRAM_TEXT
static void dw_dma_enable_status(int dma_channel)
{
	unsigned long long reg_value;

	reg_value = dma_read_reg(DMA_CHXINTSTATUSEN(dma_channel));
	reg_value = DWAXIDMAC_IRQ_DMA_TRF | DWAXIDMAC_IRQ_SUSPENDED | DWAXIDMAC_IRQ_DISABLED;
	dma_write_reg(DMA_CHXINTSTATUSEN(dma_channel),reg_value);
}

SECTION_SRAM_TEXT
static void dw_dma_clear_int(int dma_channel)
{
	unsigned long long reg_value;

	reg_value = dma_read_reg(DMA_CHXINTCLEAR(dma_channel));
	reg_value = DWAXIDMAC_IRQ_ALL;
	dma_write_reg(DMA_CHXINTCLEAR(dma_channel),reg_value);
}

SECTION_SRAM_TEXT
static int dw_dma_complete(int irq, void *pdata)
{
	int channel;

	for (channel=0; channel<DW_DMAC_MAX_CH; channel++) {
		if(0 != dma_read_reg(DMA_CHXINTSTATUS(channel))) {
			dw_dma_clear_int(channel);
			dw_dma_disable_int(channel);
			if (dw_dma_callback[channel])
				dw_dma_callback[channel](dw_dma_callback_param[channel]);
		}
		//printf("channel %d isr int status is %llx\n",i, dma_read_reg(DMA_CHXINTSTATUS(i)));
	}
	//printf("channel %d dma_complete int\n",channel);
	//gx_free_irq(IRQ_NUM_DMA);
	return 0;
}

SECTION_SRAM_TEXT
int dw_dma_select_channel()
{
	int i = 0;
	unsigned long long  ch_enable=0;

	for(i=0;i<DW_DMAC_MAX_CH;i++) {
		ch_enable = (dma_read_reg(DMA_CHENREG)) & lshift(DMAC_EN_MASK, i);
		if(0 == ch_enable)
			return i;
	}
	printf("no enough channel\n");
	return -1;
}

SECTION_SRAM_TEXT
void dw_dma_register_complete_callback(int channel, void *func, void *param)
{
	dw_dma_callback[channel] = func;
	dw_dma_callback_param[channel] = param;
	return;
}

SECTION_SRAM_TEXT
int dw_dma_channel_config(int dma_channel, DW_DMAC_CH_CONFIG config)
{
	unsigned long long reg_value;
	int ret = 0;

	/////////////////////////////////////////////////////////////
	//now setting CHXCTL REG
	reg_value = dma_read_reg(DMA_CHXCTL(dma_channel));

	//set msize
	ret = dma_set_bit_value(&reg_value,CH_CTL_L_DST_MSIZE_POS,CH_CTL_L_DST_MSIZE_MASK,config.dst_msize);
	ret |= dma_set_bit_value(&reg_value,CH_CTL_L_SRC_MSIZE_POS,CH_CTL_L_SRC_MSIZE_MASK,config.src_msize);
	//set tr width
	ret |= dma_set_bit_value(&reg_value,CH_CTL_L_DST_WIDTH_POS,CH_CTL_L_DST_WIDTH_MASK,config.dst_trans_width);
	ret |= dma_set_bit_value(&reg_value,CH_CTL_L_SRC_WIDTH_POS,CH_CTL_L_SRC_WIDTH_MASK,config.src_trans_width);

	//set dst/src addr increase
	if(DWAXIDMAC_CH_CTL_L_INC == config.dst_addr_update) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_DST_INC_POS,CH_CTL_L_DST_INC_POS_MASK,0x0);
	} else if(DWAXIDMAC_CH_CTL_L_NOINC == config.dst_addr_update) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_DST_INC_POS,CH_CTL_L_DST_INC_POS_MASK,0x1);
	} else {
		printf("error addr dst inc param\n");
		ret |= -1;
	}
	if(DWAXIDMAC_CH_CTL_L_INC == config.src_addr_update) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_SRC_INC_POS,CH_CTL_L_SRC_INC_POS_MASK,0x0);
	} else if(DWAXIDMAC_CH_CTL_L_NOINC == config.src_addr_update) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_SRC_INC_POS,CH_CTL_L_SRC_INC_POS_MASK,0x1);
	} else {
		printf("error addr src inc param\n");
		ret |= -1;
	}

	//AXI master select //mem2mem is 0x1 0x01
	if(AXI_MASTER_1 == config.dst_master_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_DST,CH_CTL_L_DST_MASK,0x0);
	} else if(AXI_MASTER_2 == config.dst_master_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_DST,CH_CTL_L_DST_MASK,0x1);
	} else {
		printf("error addr dst inc param\n");
	}
	if(AXI_MASTER_1 == config.src_master_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_SRC,CH_CTL_L_SRC_MASK,0x0);
	} else if(AXI_MASTER_2 == config.src_master_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CTL_L_SRC,CH_CTL_L_SRC_MASK,0x1);
	} else {
		printf("error addr dst inc param\n");
	}
	dma_write_reg(DMA_CHXCTL(dma_channel),reg_value);

	////////////////////////////////////////////////////////////////
	//now setting CHXCFG REG
	reg_value = dma_read_reg(DMA_CHXCFG(dma_channel));
	//set dir and dataflow
	ret |= dma_set_bit_value(&reg_value,CH_CFG_L_TT_FC_POS,CH_CFG_L_TT_FC_MASK,config.flow_ctrl);

	//set src/dst handshaking mode.
	//hardware or software
	if(DWAXIDMAC_HS_SEL_HW == config.dst_hs_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CFG_L_HS_SEL_DST_POS,CH_CFG_L_HS_SEL_DST_POS_MASK,0x0);
	} else if(DWAXIDMAC_HS_SEL_SW == config.dst_hs_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CFG_L_HS_SEL_DST_POS,CH_CFG_L_HS_SEL_DST_POS_MASK,0x1);
	} else {
		printf("error dst handshake mode\n");
		ret |= -1;
	}
	if(DWAXIDMAC_HS_SEL_HW == config.src_hs_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CFG_L_HS_SEL_SRC_POS,CH_CFG_L_HS_SEL_SRC_POS_MASK,0x0);
	} else if(DWAXIDMAC_HS_SEL_SW == config.src_hs_select) {
		ret |= dma_set_bit_value(&reg_value,CH_CFG_L_HS_SEL_SRC_POS,CH_CFG_L_HS_SEL_SRC_POS_MASK,0x1);
	} else {
		printf("error src handshake mode\n");
		ret |= -1;
	}
	//set dst/src per
	ret |= dma_set_bit_value(&reg_value,CH_CFG_L_DST_PER_POS,CH_CFG_L_DST_PER_MASK,config.dst_per);
	ret |= dma_set_bit_value(&reg_value,CH_CFG_L_SRC_PER_POS,CH_CFG_L_SRC_PER_MASK,config.src_per);

	dma_write_reg(DMA_CHXCFG(dma_channel),reg_value);
	return ret;
}

SECTION_SRAM_TEXT
int dw_dma_xfer(void *dst, void *src, int len, int dma_channel)
{
	unsigned long long reg_value;

	dw_dma_enable();
	dw_dma_clear_int(dma_channel);
	dw_dma_enable_status(dma_channel);
	//dw_dma_enable_int(dma_channel);

	//set src/dst address
	reg_value = (unsigned long long)(CPU_ADDR_TO_DMA(src));
	dma_write_reg(DMA_CHXSAR(dma_channel),reg_value);

	reg_value = (unsigned long long)(CPU_ADDR_TO_DMA(dst));
	dma_write_reg(DMA_CHXDAR(dma_channel),reg_value);

	//set data len
	reg_value = (unsigned long long)(len-1);
	dma_write_reg(DMA_CHXBLKTS(dma_channel),reg_value);

	if(1 == is_debug)
		display_regs(dma_channel);
	dw_dma_channel_enable(dma_channel);

	return 0;
}

// the procedure(channel disable prior to transfer completion without suspend)
// follows the guide in the page 90 of the user manual
SECTION_SRAM_TEXT
static int dw_dmac_channel_abort(int dma_channel)
{
	unsigned long long reg_value;

	dw_dma_channel_suspend(dma_channel);

	//wait disable finished
	while(1) {
		reg_value = dma_read_reg(DMA_CHXINTSTATUS(dma_channel));
		if ((DWAXIDMAC_IRQ_SUSPENDED & reg_value) > 0)
			break;
	}

	//disable dma channel
	dw_dma_channel_disable(dma_channel);

	//wait disable finished
	while(1) {
		reg_value = dma_read_reg(DMA_CHXINTSTATUS(dma_channel));
		if ((DWAXIDMAC_IRQ_DISABLED & reg_value) > 0 )
			break;
	}

	dw_dma_clear_int(dma_channel);
	return 0;
}

SECTION_SRAM_TEXT
int dw_dma_wait_complete_timeout(int dma_channel, unsigned int timeout_ms)
{
	unsigned long long reg_value;
	unsigned int start;
	start = get_time_ms();
	while((get_time_ms() - start) < timeout_ms) {
		reg_value = dma_read_reg(DMA_CHXINTSTATUS(dma_channel));
		reg_value &= (DWAXIDMAC_IRQ_DMA_TRF);
		if (DWAXIDMAC_IRQ_DMA_TRF == reg_value) {
			//printf("dma finish\n");
			dw_dma_clear_int(dma_channel);
			return 0;
		}
	}
	printf("dma timeout and abort\n");
	dw_dmac_channel_abort(dma_channel);
	return -1;
}

SECTION_SRAM_TEXT
int dw_dma_wait_complete(int dma_channel)
{
	unsigned long long reg_value;

	while(1) {
		reg_value = dma_read_reg(DMA_CHXINTSTATUS(dma_channel));
		reg_value &= (DWAXIDMAC_IRQ_DMA_TRF);
		if (DWAXIDMAC_IRQ_DMA_TRF == reg_value) {
			//printf("dma finish\n");
			break;
		}
	}

	dw_dma_clear_int(dma_channel);
	return 0;
}

SECTION_SRAM_TEXT
int dw_dma_xfer_poll(void *dst, void *src, int len, int dma_channel)
{
	dw_dma_xfer(dst,src,len,dma_channel);
	dw_dma_wait_complete(dma_channel);

	return 0;
}

SECTION_SRAM_TEXT
int dw_dma_xfer_int(void *dst, void *src, int len, int dma_channel)
{
	unsigned long long reg_value;

	//dw_dma_clear_int(dma_channel);
	dw_dma_enable_int(dma_channel);
	dw_dma_enable_status(dma_channel);

	//gx_request_irq(IRQ_NUM_DMA, (irq_handler_t)dw_dma_complete, NULL);

	//set src/dst address
	reg_value = (unsigned long long)(CPU_ADDR_TO_DMA(src));
	dma_write_reg(DMA_CHXSAR(dma_channel),reg_value);

	reg_value = (unsigned long long)(CPU_ADDR_TO_DMA(dst));
	dma_write_reg(DMA_CHXDAR(dma_channel),reg_value);

	//set data len
	reg_value = (unsigned long long)(len-1);
	dma_write_reg(DMA_CHXBLKTS(dma_channel),reg_value);

	if(1 == is_debug)
		display_regs(dma_channel);

	dw_dma_enable();
	dw_dma_channel_enable(dma_channel);

	return 0;
}

SECTION_SRAM_TEXT
void dw_dmac_init(void)
{
	dma_reset();
	gx_request_irq(MCU_IRQ_DMA, (irq_handler_t)dw_dma_complete, NULL);
	is_debug = 0;
}

void dw_dmac_debug(int debug)
{
	is_debug = debug;
}

