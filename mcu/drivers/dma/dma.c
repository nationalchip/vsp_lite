/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 */

#include <stdio.h>
#include <string.h>
#include <driver/dma.h>

#define DMA_CTLR_MAX    4
static struct dma_controller *g_ctlr[DMA_CTLR_MAX];

int dma_ctlr_register(struct dma_controller *ctlr)
{
    int i = 0;

    if(!ctlr || !ctlr->xfer)
        return -1;

    for(;i < DMA_CTLR_MAX;  ++i){
        if(g_ctlr[i] == NULL){
            g_ctlr[i] = ctlr;
            ctlr->ctlr_id = i;
            break;
        }
    }

    return -(i >= DMA_CTLR_MAX);
}

int dma_ctlr_unregister(struct dma_controller *ctlr)
{
    int index;

    if(!ctlr)
        return -1;

    index = ctlr->ctlr_id;

    g_ctlr[index] = NULL;

    return 0;
}

struct dma_controller *get_dma_ctlr(int id, const char *name)
{
    static int is_inited = 0;

    if(unlikely(!is_inited)){
        is_inited = 1;
        dma_init();
    }

    if(id >= 0 && id < DMA_CTLR_MAX){
        if(g_ctlr[id])
            return g_ctlr[id];
    }

    if(name){
        int i = 0;

        for(; i < DMA_CTLR_MAX; ++i){
            if(!g_ctlr[i])
                continue;

            if(!strcmp(g_ctlr[i]->name,name))
                return g_ctlr[i];
        }
    }

    return NULL;
}

/**
 * [Dma Memcpy]
 * @param  dst      [Dma Memcpy 的目标地址]
 * @param  src      [Dma Memcpy 的源地址]
 * @param  len      [Dma Memcpy 的长度]
 * @param  llp_buf  [Dma Memcpy 时所需要的malloc内存]
 * @param  llp_size [Dma Memcpy 时所需要的malloc内存长度]
 * @return          [成功 返回memcopy的长度, 失败 -1]
 */
int dma_memcpy(void *dst, void *src, size_t len, void *llp_buf, int llp_size)
{
    struct dma_controller *ctlr;
    int _llp_size;
    struct dma_slave_config cfg = {
        .chan_id   = -1,
        .src_addr  = (dma_addr_t)src,
        .dst_addr  = (dma_addr_t)dst,
        .len       = len,
        .llp_buf   = llp_buf,
        .llp_size  = llp_size,
        .direction = DMA_MEM_TO_MEM,
        .timeout   = len << 1,
        .src_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES,
        .dst_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES,
        .src_maxburst   = 32,
        .dst_maxburst   = 32,
    };

    ctlr = get_dma_ctlr(-1, "CK_DW_DMAC");
    if(!ctlr)
        return -1;

    _llp_size = get_llp_size(len, DMA_SLAVE_BUSWIDTH_4_BYTES);
    if(llp_size < _llp_size){
        printf("ERROR: llp_buf too small, "
            "need size 0x%x, set size 0x%x\n", _llp_size, llp_size);
        return -1;
    }

    return ctlr->xfer(ctlr->priv, &cfg, 1);
}
