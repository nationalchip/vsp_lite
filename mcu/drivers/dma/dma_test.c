/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 */
#include <common.h>
#include <driver/dma.h>
#include <string.h>
#include <driver/delay.h>

#define LLP_IN_CK_SRAM  ((void*)0x400f0000)
#define LLP_IN_A7_SRAM  LLP_IN_CK_SRAM      //实际测试,将链表放在a7 sram dma无法工作
#define LLP_IN_DDR      ((void*)0x50500000)

static void test(const char *msg, void *dest, void *src, size_t len, void *llp_buf)
{
    unsigned long long time = 0;
    u32 speed = 0;
    u32 llp_size = get_llp_size(len, DMA_SLAVE_BUSWIDTH_4_BYTES);

    printf("%s dest = 0x%x, src = 0x%x, llp = 0x%x, len = 0x%x\n", msg, dest, src, llp_buf, len);

    time = get_time_us();
    dma_memcpy(dest, src, len, llp_buf, llp_size);
    time = get_time_us() - time;

    speed = (len * 1000) / time * 1000 ;

    printf(">> xfer len 0x%x use %lld us, speed = %d K/s\n", len, time, speed);
    printf(">> test %s\n", memcmp(dest,src,len) ? " fail" : "ok");
}

extern int CpuWakeup(void);
void dma_test(void)
{
    CpuWakeup();    // 启动uboot,使ddr可用
    mdelay(2000);

    /* test ck sram to ck sram */
    test("TEST CK_SRAM <==> CK_SRAM: ",(void*)0x400a0000, (void*)0x400c0000, 0x10000, LLP_IN_CK_SRAM);
    test("TEST CK_SRAM <==> CK_SRAM: ",(void*)0x400a0000, (void*)0x400c0000, 0x20000, LLP_IN_A7_SRAM);
    test("TEST CK_SRAM <==> CK_SRAM: ",(void*)0x400b0000, (void*)0x400a0000, 0x10000, LLP_IN_DDR);

    /* test ck sram to a7 sram */
    test("TEST CK_SRAM <==> A7_SRAM: ",(void*)0x400a0000, (void*)0xa0100000, 0x4000, LLP_IN_CK_SRAM);
    test("TEST CK_SRAM <==> A7_SRAM: ",(void*)0x400a0000, (void*)0xa0100000, 0x2000, LLP_IN_A7_SRAM);
    test("TEST CK_SRAM <==> A7_SRAM: ",(void*)0xa0100000, (void*)0x400a0000, 0x4000, LLP_IN_DDR);

    /* test ck sram to ddr */
    test("TEST CK_SRAM <==> DDR: ",(void*)0x400a0000, (void*)0x50000000, 0x10000, LLP_IN_CK_SRAM);
    test("TEST CK_SRAM <==> DDR: ",(void*)0x400a0000, (void*)0x50000000, 0x8000, LLP_IN_A7_SRAM);
    test("TEST CK_SRAM <==> DDR: ",(void*)0x50100000, (void*)0x400a0000, 0x4000, LLP_IN_DDR);

    /* test ddr to a7 sram */
    test("TEST DDR <==> A7_SRAM: ",(void*)0x50000000, (void*)0xa0100000, 0x4000, LLP_IN_CK_SRAM);
    test("TEST DDR <==> A7_SRAM: ",(void*)0x50000000, (void*)0xa0100000, 0x8000, LLP_IN_A7_SRAM);
    test("TEST DDR <==> A7_SRAM: ",(void*)0xa0100000, (void*)0x50100000, 0x4000, LLP_IN_DDR);

    /* test ddr to ddr */
    test("TEST DDR <==> DDR: ",(void*)0x50000000, (void*)0x50100000, 0x80000, LLP_IN_CK_SRAM);
    test("TEST DDR <==> DDR: ",(void*)0x50000000, (void*)0x50100000, 0x40000, LLP_IN_A7_SRAM);
    test("TEST DDR <==> DDR: ",(void*)0x50200000, (void*)0x50100000, 0x100000, LLP_IN_DDR);
    test("TEST DDR <==> DDR: ",(void*)0x50300000, (void*)0x50100000, 0x800, LLP_IN_DDR);

    /* test a7 sram to a7 sram */
    test("TEST A7_SRAM <==> A7_SRAM: ",(void*)0xa0100000, (void*)0xa0104000, 0x4000, LLP_IN_CK_SRAM);
    test("TEST A7_SRAM <==> A7_SRAM: ",(void*)0xa0100000, (void*)0xa0104000, 0x4000, LLP_IN_A7_SRAM);
    test("TEST A7_SRAM <==> A7_SRAM: ",(void*)0xa0104000, (void*)0xa0100000, 0x4000, LLP_IN_DDR);
}
