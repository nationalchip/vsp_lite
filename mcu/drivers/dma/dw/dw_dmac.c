/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 */

#include <common.h>
#include <base_addr.h>
#include <driver/delay.h>
#include <driver/dma.h>
#include "dw_dmac.h"

#define DRIVER_NAME "DW_DMAC"

struct dw_dmac_priv {
    dw_dma_regs_t *dw_dmac;
    uint32_t chan_num;
    uint32_t chan_mask;
    void *priv;
    uint32_t temp;
};

//#define  DMA_DBG
#ifdef   DMA_DBG

#define DMA_DEBUG_CFG          1
#define DMA_DEBUG_PARAM        0
#define DMA_DEBUG_INTERRUPT    0
#define DMA_DEBUG_OTHERS       1

#define dma_debug(_fg, _fmt,args...) do{ if(_fg) printf(_fmt,##args); }while(0)
#else
#define dma_debug(_fg, _fmt,args...)
#endif

#define assert_param(_param, _rtn) do{\
    if(!(_param)){\
        dma_debug(1, "ERROR: [%s] - %d...\n",__func__, __LINE__);\
        return _rtn;\
    }\
}while(0)

#define FREE(_PTR) do{ if(_PTR) { free(_PTR); _PTR = NULL; } }while(0)


#define CHAN_CLEAN(_PTR, _OP, _VAL) do{\
    *((_PTR)->XFER)     _OP _VAL;\
    *((_PTR)->BLOCK)    _OP _VAL;\
    *((_PTR)->SRC_TRAN) _OP _VAL;\
    *((_PTR)->DST_TRAN) _OP _VAL;\
    *((_PTR)->ERROR)    _OP _VAL;\
}while(0)

#define dw_dma_enable(_dma, _on) do{ (_dma)->CFG[0] = !!(_on); }while(0)

#define cpu_addr_to_dma(_addr) ({\
    uint32_t __addr = (uint32_t)(_addr);\
    __addr - (__addr >= 0xa0000000 ? 0xa0000000 : 0x40000000);\
})

#define bytes2width(_bytes) ({\
    int i;\
    u8 tb[] = {1, 2, 4, 8, 16 };\
    for(i=0; i < sizeof(tb); ++i){\
        if(tb[i] == (_bytes))\
            break;\
    }\
    i;\
})

#define bytes2burst(_bytes) ({\
    int i;\
    u8 tb[] = {1, 4, 8, 16, 32, 64, 128};\
    for(i=0; i < sizeof(tb); ++i){\
        if(tb[i] == (_bytes))\
            break;\
    }\
    i;\
})

/**
 *  由于a7 SRAM 要当成外设来操作,所以 Master Select需要设置为1
 */
#define addr2master(_addr) ({\
    int Master = 0;\
    u32 addr = (u32)(_addr);\
    /* 该区域为a7 sram的寻址范围 */ \
    if(addr > 0xa0000000 && addr < 0xa0110000)\
        Master = 1;\
    Master;\
})

static void display_interrupt(dw_dma_regs_t *dma)
{
    dma_debug(DMA_DEBUG_INTERRUPT, "\n\n*****************************************\n");

    dma_debug(DMA_DEBUG_INTERRUPT,"  [Raw]    xfer:%04x, block:%04x, src:%04x, "
        "dest:%04x, error:%04x\n",dma->RAW.XFER[0],dma->RAW.BLOCK[0],
        dma->RAW.SRC_TRAN[0],dma->RAW.DST_TRAN[0],dma->RAW.ERROR[0]);
    dma_debug(DMA_DEBUG_INTERRUPT,"  [Status] xfer:%04x, block:%04x, src:%04x, "
        "dest:%04x, error:%04x\n",dma->STATUS.XFER[0],dma->STATUS.BLOCK[0],
        dma->STATUS.SRC_TRAN[0],dma->STATUS.DST_TRAN[0],dma->STATUS.ERROR[0]);
    dma_debug(DMA_DEBUG_INTERRUPT,"  [Mask]   xfer:%04x, block:%04x, src:%04x, "
        "dest:%04x, error:%04x\n",dma->MASK.XFER[0],dma->MASK.BLOCK[0],
        dma->MASK.SRC_TRAN[0],dma->MASK.DST_TRAN[0],dma->MASK.ERROR[0]);
    dma_debug(DMA_DEBUG_INTERRUPT,"  [Clear]  xfer:%04x, block:%04x, src:%04x, "
        "dest:%04x, error:%04x\n",dma->CLEAR.XFER[0],dma->CLEAR.BLOCK[0],
        dma->CLEAR.SRC_TRAN[0],dma->CLEAR.DST_TRAN[0],dma->CLEAR.ERROR[0]);
    dma_debug(DMA_DEBUG_INTERRUPT,"  STATUS_INI %04X\n\n", dma->STATUS_INT[0]);
}

static void display_regs(const char *msg, dw_dma_regs_t *dma, int ch)
{
    dma_debug(DMA_DEBUG_CFG, "\n****************** %s ******************\n", msg);
    dma_debug(DMA_DEBUG_CFG, "\tchan:    %d\n", ch);
    dma_debug(DMA_DEBUG_CFG, "\tSAR:     0x%08x, DAR:     0x%08x\n",
        dma->CHAN[ch].SAR[0],dma->CHAN[ch].DAR[0]);
    dma_debug(DMA_DEBUG_CFG, "\tLLP:     0x%08x\n",
        dma->CHAN[ch].LLP[0]);
    dma_debug(DMA_DEBUG_CFG, "\tCTRL:    0x%08x  HI:      0x%08x\n",
        dma->CHAN[ch].CTL[0],dma->CHAN[ch].CTL[1]);
    dma_debug(DMA_DEBUG_CFG, "\tCFG:     0x%08x  HI:      0x%08x\n",
        dma->CHAN[ch].CFG[0],dma->CHAN[ch].CFG[1]);
    dma_debug(DMA_DEBUG_CFG, "\tSSTAT:   0x%08x, DSTAT:   0x%08x\n",
        dma->CHAN[ch].SSTAT[0], dma->CHAN[ch].DSTAT[0]);
    dma_debug(DMA_DEBUG_CFG, "\tSSTATAR: 0x%08x, DSTATAR: 0x%08x\n",
        dma->CHAN[ch].SSTATAR[0], dma->CHAN[ch].DSTATAR[0]);
    dma_debug(DMA_DEBUG_CFG, "\tSGR:     0x%08x, DSR:     0x%08x\n\n",
        dma->CHAN[ch].SGR[0], dma->CHAN[ch].DSR[0]);
}

static int dw_request_chan(struct dw_dmac_priv *priv)
{
    int i;
    for(i = 0; i < priv->chan_num; ++i){
        if((priv->chan_mask & (0x01 << i)) == 0){
            priv->chan_mask |= 0x01 << i;
            return i;
        }
    }

    return -1;
}

static int dw_unrequest_chan(struct dw_dmac_priv *priv, int chan)
{
    if(chan < 0 || chan >= priv->chan_num)
        return -1;

    priv->chan_mask &= ~(0x01 << chan);

    return 0;
}

static int dw_cyclic_start(struct dw_dmac_priv *priv, u32 start_flag, int timeout)
{
    dw_dma_regs_t *dma = priv->dw_dmac;
    int i, ret = 0;

    display_interrupt(dma);

    dw_dma_enable(dma, 1);
    dma->CH_EN[0] |= start_flag;

    for(i = 0; i < priv->chan_num; ++i){
        if(!(start_flag & 0x01 << i))
            continue;

        while((dma->RAW.XFER[0] & (0x01 << i) ) == 0){
            if(timeout < 0)
                continue;

            if(timeout == 0){
                dma_debug(DMA_DEBUG_OTHERS,
                    "Dma transfer chan %d timeout!\n", i);
                ret = -1;
                goto out;
            }
            --timeout;
            udelay(10);
        }
        display_regs("After transfer", dma, i);
    }

out:
    dw_dma_enable(dma, 0);
    display_interrupt(dma);

    i = 0;
    while(i < priv->chan_num){
        if(start_flag & 0x01 << i)
            dw_unrequest_chan(priv, i);
        ++i;
    }

    return ret;
}

static void dw_chan_clean(struct dw_dmac_priv *priv, int ch)
{
    CHAN_CLEAN(&priv->dw_dmac->CLEAR, |=,  0x01  << ch );
    CHAN_CLEAN(&priv->dw_dmac->RAW,   &=, ~(0x01 << ch));
    CHAN_CLEAN(&priv->dw_dmac->MASK,  |=,  0x101 << ch);
}

static int cfg_check(struct dw_dmac_priv *priv, struct dma_slave_config *cfg)
{
    size_t len;
    u32 need_size;
    /* 判断地址是否有效 */
    if(!cfg->llp_buf || !cfg->src_addr || !cfg->dst_addr || cfg->len < 0)
        return -1;

    if(cfg->chan_id < 0){
        cfg->chan_id = dw_request_chan(priv);
        if(cfg->chan_id < 0)
            return -1;
    }

    /* 判断为链表准备的缓存是否足够 */
    len = cfg->len / cfg->src_addr_width;
    need_size = (len/BLOCK_TS + !!(len%BLOCK_TS)) * sizeof(dw_lli_t);

    return -(need_size > cfg->llp_size);
}

static u32 get_dw_ctrl(u32 *reg_cfgx, struct dma_slave_config *cfg)
{
    u32 ctrl = DWC_CTLL_LLP_D_EN | DWC_CTLL_LLP_S_EN | DWC_CTLL_INT_EN;
    u32 CFGx_L = DWC_CFGL_CH_PRIOR(cfg->chan_id);     // set Channel priority.
    u32 CFGx_H = DWC_CFGH_FIFO_MODE;

    switch(cfg->direction){
    case DMA_MEM_TO_MEM:
        ctrl |= DWC_CTLL_FC_M2M | DWC_CTLL_SRC_INC | DWC_CTLL_DST_INC |
                DWC_CTLL_DMS(addr2master(cfg->dst_addr)) |
                DWC_CTLL_SMS(addr2master(cfg->src_addr));
        CFGx_L |= DWC_CFGL_HS_DST | DWC_CFGL_HS_SRC;
        break;
    case DMA_MEM_TO_DEV:
        ctrl |= DWC_CTLL_FC_M2P | DWC_CTLL_DST_FIX |
                DWC_CTLL_DMS(1) | DWC_CTLL_SMS(addr2master(cfg->src_addr));
        ctrl |= cfg->flags ? DWC_CTLL_SRC_FIX : DWC_CTLL_SRC_INC;
        CFGx_L |= DWC_CFGL_HS_SRC;
        CFGx_H |= DWC_CFGH_DST_PER(cfg->chan_id);
        break;
    case DMA_DEV_TO_MEM:
        ctrl |= DWC_CTLL_FC_P2M | DWC_CTLL_SRC_FIX |
                DWC_CTLL_SMS(1) | DWC_CTLL_DMS(addr2master(cfg->dst_addr));
        ctrl |= cfg->flags ? DWC_CTLL_DST_FIX : DWC_CTLL_DST_INC;

        CFGx_L |= DWC_CFGL_HS_DST;
        CFGx_H |= DWC_CFGH_SRC_PER(cfg->chan_id);
        break;
    case DMA_DEV_TO_DEV:
        ctrl |= DWC_CTLL_SRC_FIX | DWC_CTLL_FC_P2P |
                DWC_CTLL_DST_FIX | DWC_CTLL_DMS(1) | DWC_CTLL_SMS(1);
        CFGx_H |= DWC_CFGH_SRC_PER(cfg->chan_id) | DWC_CFGH_DST_PER(cfg->chan_id);
        break;
    default:
        return -1;
    }

    reg_cfgx[0] = CFGx_L;
    reg_cfgx[1] = CFGx_H;

    ctrl |= DWC_CTLL_DST_MSIZE(bytes2burst(cfg->dst_maxburst))|
            DWC_CTLL_SRC_MSIZE(bytes2burst(cfg->src_maxburst))|
            DWC_CTLL_DST_WIDTH(bytes2width(cfg->dst_addr_width))|
            DWC_CTLL_SRC_WIDTH(bytes2width(cfg->src_addr_width));

    dma_debug(DMA_DEBUG_OTHERS, "CFG: ctrl = 0x%x, "
        "cfg_l = 0x%x, cfg_h = 0x%x.\n", ctrl, CFGx_L, CFGx_H);

    return ctrl;
}

static int dw_cyclic_prepare(
    struct dw_dmac_priv *priv, dw_lli_t *llp, struct dma_slave_config *cfg)
{
    dw_lli_t *p_llp;
    int i, llp_cnt, bit_wide;
    u32 reg_cfgx[2] = {0};
    int dst_step, src_step, len;
    u16 step[] = { BLOCK_TS, -BLOCK_TS, 0, 0 };

    if(cfg_check(priv, cfg) < 0){
        dma_debug(DMA_DEBUG_OTHERS, "cfg_check: param error.\n");
        return -1;
    }

    dw_chan_clean(priv, cfg->chan_id);

    llp->sar = cpu_addr_to_dma(cfg->src_addr);
    llp->dar = cpu_addr_to_dma(cfg->dst_addr);
    llp->ctl[0] = get_dw_ctrl(reg_cfgx, cfg);

    /* config channel cfgx register */
    priv->dw_dmac->CHAN[cfg->chan_id].CFG[0] = reg_cfgx[0];
    priv->dw_dmac->CHAN[cfg->chan_id].CFG[1] = reg_cfgx[1];

    /* prepare list for transfer */
    p_llp = cfg->llp_buf;
    llp->llp = cpu_addr_to_dma(p_llp + 1);
    bit_wide = bytes2width(cfg->src_addr_width);
    i = (llp->ctl[0] >> 9) & 0x03;
    src_step = step[i] << bit_wide;
    i = (llp->ctl[0] >> 7) & 0x03;
    dst_step = step[i] << bit_wide;

    len = cfg->len >> bit_wide;
    llp_cnt = len / BLOCK_TS + !!(len % BLOCK_TS);

    for(i = 0; i < llp_cnt - 1; ++i, ++p_llp){
        p_llp->sar = llp->sar + src_step * i;
        p_llp->dar = llp->dar + dst_step * i;
        p_llp->ctl[0] = llp->ctl[0];
        p_llp->ctl[1] = BLOCK_TS ;
        p_llp->llp = cpu_addr_to_dma(p_llp + 1);
    }

    p_llp->sar = llp->sar + src_step * i;
    p_llp->dar = llp->dar + dst_step * i;
    p_llp->ctl[0] = llp->ctl[0] & (~(0x03 << 27));
    p_llp->llp = 0;
    p_llp->ctl[1] = (cfg->len >> bit_wide) % BLOCK_TS ? : BLOCK_TS;

    llp->ctl[1] = (llp_cnt == 1) ? p_llp->ctl[1] : BLOCK_TS;

    return 0;
}

static int dw_dma_transfer(void *drv_priv, struct dma_slave_config *cfg, int num_cfg)
{
    dw_dma_regs_t *dma;
    dw_lli_t llp[4];
    u32 start_flag = 0;
    int i = 0;

    if(!cfg)
        return -1;

    if(cfg->len == 0)
        return 0;

    dma = ((struct dw_dmac_priv*)drv_priv)->dw_dmac;

    for(i = 0; i < num_cfg; ++i){
        int chan;
        if(dw_cyclic_prepare(drv_priv, llp + i, cfg + i) < 0){
            dma_debug(DMA_DEBUG_OTHERS,"dw_cyclic_prepare error!\n");
            return -1;
        }

        chan = cfg[i].chan_id;
        start_flag |= 0x101 << chan;

        dma->CHAN[chan].SAR[0] = llp[i].sar;
        dma->CHAN[chan].DAR[0] = llp[i].dar;
        dma->CHAN[chan].CTL[0] = llp[i].ctl[0];
        dma->CHAN[chan].CTL[1] = llp[i].ctl[1];
        dma->CHAN[chan].LLP[0] = cpu_addr_to_dma(cfg[i].llp_buf);
    }

    if(dw_cyclic_start(drv_priv, start_flag, cfg->timeout) < 0)
        return -1;

    return cfg->len;
}

static void dw_dmac_clean(dw_dma_regs_t *dma)
{
    /* 清中断标记 */
    CHAN_CLEAN(&dma->CLEAR, =, ~0);
    CHAN_CLEAN(&dma->RAW,   =,  0);
    CHAN_CLEAN(&dma->MASK,  =,  0xffff);

    /* disable dma */
    dw_dma_enable(dma, 0);
}

static int dw_llp_size(size_t len, enum dma_slave_buswidth src_addr_width)
{
    len = (len + src_addr_width -1) / src_addr_width;
    return ((len / BLOCK_TS) + !!(len % BLOCK_TS)) * sizeof(dw_lli_t);
}

int dma_init(void)
{
    static struct dw_dmac_priv dma;
    static struct dma_controller ctlr;

    dma.dw_dmac   = (dw_dma_regs_t*)MCU_REG_BASE_DW_DMA;
    dma.chan_num  = 4;
    dma.chan_mask = 0;
    dw_dmac_clean(dma.dw_dmac);

    ctlr.num_chan = dma.chan_num;
    ctlr.priv = &dma;
    dma.priv = &ctlr;
    ctlr.xfer = dw_dma_transfer;
    ctlr.malloc_size = dw_llp_size;

    sprintf(ctlr.name, "CK_%s", DRIVER_NAME);

    if(dma_ctlr_register(&ctlr) < 0){
        dma_debug(DMA_DEBUG_OTHERS, "dma_ctlr_register error!\n");
        return -1;
    }

    return 0;
}

