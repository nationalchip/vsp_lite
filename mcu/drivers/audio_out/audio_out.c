/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * audio_out.c: MCU AudioOut HAL
 *              we use r0 to play PCM1IN, and r1 to play Buffer
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <base_addr.h>
#include <audio_out_regs.h>
#include <misc_regs.h>

#include <driver/dto.h>
#include <driver/audio_ref.h>
#include <driver/audio_out.h>
#include <driver/irq.h>
#include <driver/delay.h>
#include <driver/clock.h>

typedef struct {
    unsigned short gain;
    short          value_dB;
} AUDIO_OUT_DB_TABLE;

typedef struct {
    AUDIO_OUT_PLAY_CB play_callback;

    unsigned int fre_div;
    unsigned int fre_adjust;
    unsigned int fre_adjust_step;
    unsigned long long old_time;
    unsigned long long cur_time;

    void *priv;
} AUDIO_OUT_STATE;

typedef enum {
    PCM1IN_FIFO_FULL,
    PCM1IN_FIFO_EMPTY
} AUDIO_OUT_PCM1IN_TYPE;

static AUDIO_OUT_STATE s_audio_out_state;

static const AUDIO_OUT_DB_TABLE table_dB[] = {
    {16 , -18}, {18 , -17}, {20 , -16}, {23 , -15}, {25 , -14},
    {28 , -13}, {32 , -12}, {36 , -11}, {40 , -10}, {45 ,  -9},
    {51 ,  -8}, {57 ,  -7}, {64 ,  -6}, {72 ,  -5}, {81 ,  -4},
    {91 ,  -3}, {102,  -2}, {114,  -1}, {128,   0}, {144,   1},
    {162,   2}, {181,   3}, {203,   4}, {228,   5}, {256,   6},
    {288,   7}, {323,   8}, {362,   9}, {407,  10}, {460,  11},
    {512,  12}, {576,  13}, {646,  14}, {725,  15}, {814,  16},
    {913,  17}, {1023, 18}
};
//=================================================================================================
static int _AudioOutPcm1InAdjust(AUDIO_OUT_PCM1IN_TYPE type)
{
// 策略参数
#define TIME_WEIGHT             (10)
#define DEFAULT_FRE_ADJUST_STEP (2)

// 固定参数
#define PCM1IN_SAMPLES_NUM (32)
#define AUDIO_OUT_SYSCLK   (12288000)
#define FREQ_THRESHOLD     (1500)
#define CAL_MULTI          (1000)

    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    unsigned long long dto = DTO_FREQ_FROM_STEP(DtoGetStep(DTO_AUDIO_OUT));
    unsigned long long time_us = 0;
    unsigned long long frequence = 0;

    if (s_audio_out_state.fre_div <= 0)
        return -1;

    unsigned int sample_rate = dto/s_audio_out_state.fre_div;
    s_audio_out_state.cur_time = get_time_us();
    time_us = s_audio_out_state.cur_time - s_audio_out_state.old_time;

    unsigned long long time_weight = TIME_WEIGHT*PCM1IN_SAMPLES_NUM*1000/(sample_rate/1000);
    if ((time_us <= 0) || (time_us < time_weight))
        return 0;

    unsigned int samples_num = PCM1IN_SAMPLES_NUM;
    if (s_audio_out_state.fre_adjust_step == DEFAULT_FRE_ADJUST_STEP) {
        unsigned int fifo_cap = regs->audio_play_i2s_in_info.bits.i2s_in_full_cap;
        samples_num = (type == PCM1IN_FIFO_FULL) ? PCM1IN_SAMPLES_NUM-fifo_cap:fifo_cap;
    }

    frequence = (unsigned long long)samples_num*1000000*CAL_MULTI/time_us;
    frequence = frequence * s_audio_out_state.fre_div / CAL_MULTI;
    frequence = frequence * s_audio_out_state.fre_adjust_step;

    if (frequence > (FREQ_THRESHOLD*s_audio_out_state.fre_adjust_step)) {
    #if 0
        printf("So big frequence, cur dto:%lld, %s\n", dto, ((type == PCM1IN_FULL)?"Full":"Empty"));
    #endif
        return 0;
    }

    if (type == PCM1IN_FIFO_FULL) {
        dto += frequence;
        printf("\t Full, frequence: %lld, curDto:%lld, dstDto:%lld\n", frequence, dto-frequence, dto);
    } else if (type == PCM1IN_FIFO_EMPTY) {
        dto -= frequence;
        printf("\t Empty, frequence: %lld, curDto:%lld, dstDto:%lld\n", frequence, dto+frequence, dto);
    }
    DtoSetStep(DTO_AUDIO_OUT, DTO_STEP_FROM_FREQ(dto));

    s_audio_out_state.fre_adjust_step = 1;
    s_audio_out_state.old_time = get_time_us();

    return 0;
}

int _AudioOutISR(int irq, void *pdata)
{
    if (irq != MCU_IRQ_AUDIO_OUT)
        return -1;

    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    if (regs->audio_play_int.bits.r2_set_newpcm_int) {
        if (regs->audio_play_int_en.bits.r2_set_newpcm_int_en && s_audio_out_state.play_callback)
            s_audio_out_state.play_callback(regs->audio_play_route[1].buffer_sdc_addr.value, s_audio_out_state.priv);

        regs->audio_play_int.value = (1 << 1); // clear route[1] int
    }

    if (regs->audio_play_int_en.bits.i2s_in_pcm_read_err_int_en &&
            regs->audio_play_int.bits.i2s_in_pcm_read_err_int) {
        _AudioOutPcm1InAdjust(PCM1IN_FIFO_EMPTY);
        regs->audio_play_int.value = (1 << 7);
    }

    if (regs->audio_play_int_en.bits.i2s_in_pcm_read_err2_int_en &&
            regs->audio_play_int.bits.i2s_in_pcm_read_err2_int) {
        _AudioOutPcm1InAdjust(PCM1IN_FIFO_FULL);
        regs->audio_play_int.value = (1 << 6);
    }

    return 0;
}

int AudioRefInit(const AUDIO_REF_CONFIG *config)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    // Disable PCM Write
    regs->audio_play_ctrl.bits.pcm_w_en = 0;
    if (regs->audio_play_ctrl.bits.work_soft_rstn) {
        regs->audio_play_ctrl.bits.work_soft_rstn = 0;
    }

    // Channel Setting
    if (config->channel_num == 1) {
        regs->audio_play_ctrl.bits.pcm_w_ch_sel = 0;
        regs->audio_play_ctrl.bits.pcm_w_mono_en = 1;
        regs->audio_play_channel_sel.bits.write_channel_0_sel = 0;
        regs->audio_play_channel_sel.bits.write_channel_1_sel = 1;
    }
    else {
        regs->audio_play_ctrl.bits.pcm_w_ch_sel = 1;
        regs->audio_play_ctrl.bits.pcm_w_mono_en = 0;
        regs->audio_play_channel_sel.bits.write_channel_0_sel = 0;
        regs->audio_play_channel_sel.bits.write_channel_1_sel = 1;
    }

    regs->audio_play_ctrl.bits.pcm_w_endian = AUDIO_REF_PCM_ENDIAN_LITTLE;
    regs->audio_play_ctrl.bits.pcm_w_source_sel = AUDIO_REF_SOURCE_INTERNAL;
#ifndef CONFIG_GX8008B
    if (config->sample_rate == 8000)
        regs->audio_play_ctrl.bits.pcm_w_fs_sel = AUDIO_REF_SAMPLE_RATE_08KHZ;
    else if (config->sample_rate == 16000)
        regs->audio_play_ctrl.bits.pcm_w_fs_sel = AUDIO_REF_SAMPLE_RATE_16KHZ;
    else
        regs->audio_play_ctrl.bits.pcm_w_fs_sel = AUDIO_REF_SAMPLE_RATE_48KHZ;
#endif

    //regs->audio_play_pcm_w_num.value = config->frame_size / 128 * 64;
    regs->audio_play_pcm_w_buffer_saddr.value = (uint32_t)(config->buffer_addr);
    regs->audio_play_pcm_w_buffer_size.value = config->channel_size;
    regs->audio_play_ctrl.bits.pcm_w_en = 1;
    regs->audio_play_int.value = (1 << 16); // reg info fresh

    return 0;
}

//-------------------------------------------------------------------------------------------------

void AudioRefDone(void)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);
    regs->audio_play_ctrl.bits.pcm_w_en = 0;
}

//=================================================================================================

int AudioOutInit(const AUDIO_OUT_PCM1IN_CONFIG *pcm1in_config, const AUDIO_OUT_BUFFER_CONFIG *buffer_config)
{
    ClockEnable(CLOCK_AUDIO_PLAY);
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    // Setup Play Controller
    regs->audio_play_ctrl.bits.i2s_mute = 0;
    regs->audio_play_ctrl.bits.spdif_mute = 0;
    regs->audio_play_ctrl.bits.data_stop = 0;
    regs->audio_play_ctrl.bits.spdif_play_mode = 2;
    regs->audio_play_ctrl.bits.silent_end = 1;

    regs->audio_play_i2s_dacinfo.bits.play_channel_0_sel = 0;
    regs->audio_play_i2s_dacinfo.bits.play_channel_1_sel = 1;
    regs->audio_play_i2s_dacinfo.bits.mclk_sel = 0;
    regs->audio_play_i2s_dacinfo.bits.length_mode = 2;
    regs->audio_play_i2s_dacinfo.bits.data_format = 0;

    regs->audio_play_int.value = (1 << 21); // cpu reopen audio play
    regs->audio_play_ctrl.bits.work_soft_rstn = 0;

    // Route 0
    if (pcm1in_config) {
        s_audio_out_state.fre_div  = 0;
        s_audio_out_state.old_time = 0;
        s_audio_out_state.cur_time = 0;
        s_audio_out_state.fre_adjust_step = DEFAULT_FRE_ADJUST_STEP;

        regs->audio_play_i2s_in_info.bits.i2s_in_data_format = 0x0;
        regs->audio_play_i2s_in_info.bits.i2s_in_bclk_inv_en = 0x0;
        regs->audio_play_i2s_in_info.bits.i2s_in_lrclk_inv_en = 0x0;
        regs->audio_play_i2s_in_info.bits.i2s_in_pcm_fs = (AUDIO_PLAY_SAMPLE_RATE)pcm1in_config->sample_rate;
        regs->audio_play_i2s_in_info.bits.i2s_in_full_cap = 0x10;
        regs->audio_play_i2s_in_info.bits.i2s_in_data_length = 0x0;

        regs->audio_play_route[0].indata.bits.src_sel = 0;
        regs->audio_play_route[0].vol_ctrl.bits.vol_level = 0x80;

        switch(pcm1in_config->sample_rate) {
        case AUDIO_PLAY_SMAPLE_RATE_48000:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/48000;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_44100:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/44100;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_32000:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/32000;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_24000:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/24000;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_22050:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/22050;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_16000:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/16000;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_11025:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/11025;
            break;
        case AUDIO_PLAY_SAMPLE_RATE_8000:
            s_audio_out_state.fre_div = AUDIO_OUT_SYSCLK/8000;
            break;
        }

        if (pcm1in_config->clock_type == AUDIO_OUT_CLOCK_ASYNC) {
            s_audio_out_state.fre_adjust = 1;
            regs->audio_play_int_en.bits.i2s_in_pcm_read_err_int_en  = 1;
            regs->audio_play_int_en.bits.i2s_in_pcm_read_err2_int_en = 1;
            gx_request_irq(MCU_IRQ_AUDIO_OUT, _AudioOutISR, NULL);
        }
    }

    // Route 1
    if (buffer_config) {
        if (buffer_config->channel_type == AUDIO_OUT_CHANNEL_MONO) {
            regs->audio_play_channel_sel.bits.r2_channel_0_sel = 0;
            regs->audio_play_channel_sel.bits.r2_channel_1_sel = 0;
        }
        else {
            regs->audio_play_channel_sel.bits.r2_channel_0_sel = 1;
            regs->audio_play_channel_sel.bits.r2_channel_1_sel = 0;
        }

        regs->audio_play_route[1].indata.bits.src_sel = 1;
        regs->audio_play_route[1].indata.bits.pcm_bit_size = 0x4;
        regs->audio_play_route[1].indata.bits.pcm_store_mode = 1;
        regs->audio_play_route[1].indata.bits.pcm_endian = 0x0;
        regs->audio_play_route[1].indata.bits.channel_sel = buffer_config->channel_type;
        regs->audio_play_route[1].indata.bits.fs_mode = (AUDIO_PLAY_SAMPLE_RATE)buffer_config->sample_rate;
        regs->audio_play_route[1].vol_ctrl.bits.vol_level = 0x80;

        regs->audio_play_route[1].buffer_start_addr.value = (unsigned int)buffer_config->buffer_addr;
        regs->audio_play_route[1].buffer_size.value = buffer_config->buffer_size;

        regs->audio_play_ctrl.bits.r2_work_en = 1;
        regs->audio_play_ctrl.bits.r2_mix_en = 1;

        if (buffer_config->play_callback) {
            regs->audio_play_int_en.bits.r2_set_newpcm_int_en = 1;
            s_audio_out_state.play_callback = buffer_config->play_callback;
            gx_request_irq(MCU_IRQ_AUDIO_OUT, _AudioOutISR, NULL);
        }
    }

    regs->audio_play_ctrl.bits.i2s_data_stop = 0;
    AudioOutBoardInit();

    return 0;
}

int AudioOutDone()
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    if (!regs->audio_play_ctrl.bits.work_soft_rstn) {
        // audio out is working
        if (s_audio_out_state.play_callback)
            regs->audio_play_int.value = (1 << 1); // clear route[1] int

        if (s_audio_out_state.fre_adjust) {
            regs->audio_play_int.value = (1 << 6);
            regs->audio_play_int.value = (1 << 7);
        }

        regs->audio_play_int.value = (1 << 22); // cpu stop audio play request
        while (!regs->audio_play_int.bits.axi_state_idle);
        regs->audio_play_ctrl.bits.work_soft_rstn = 1;
    }

    gx_free_irq(MCU_IRQ_AUDIO_OUT);
    ClockDisable(CLOCK_AUDIO_PLAY | CLOCK_AUDIO_DAC);
    return 0;
}

//-------------------------------------------------------------------------------------------------

int AudioOutPlay(const AUDIO_OUT_ROUTE route, const short *frame, const unsigned int size)
{
    static bool firstboot = true;
    if(firstboot)
    {
        AudioOutExternCodecInit();
        firstboot=false;
    }
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    if (route == AUDIO_OUT_ROUTE_PCM1IN) {
        regs->audio_play_ctrl.bits.r1_mix_en  = 1;
        regs->audio_play_ctrl.bits.r1_work_en = 1;
        regs->audio_play_i2s_in_info.bits.i2s_in_work_en = 0x1;
        s_audio_out_state.old_time = get_time_us();
    }
    else if (route == AUDIO_OUT_ROUTE_BUFFER) {
        regs->audio_play_route[1].new_frame_start_addr.value = (unsigned int)frame;
        regs->audio_play_route[1].new_frame_end_addr.value = (unsigned int)frame + size - 1;

        int frame_pcm_len = size / sizeof(short);
        if (regs->audio_play_route[1].indata.bits.pcm_store_mode == 1)
            frame_pcm_len /= (regs->audio_play_route[1].indata.bits.channel_sel == AUDIO_OUT_CHANNEL_MONO ? 1 : 2);

        regs->audio_play_route[1].new_frame_pcm_len.value = frame_pcm_len;

        regs->audio_play_route[1].set_newframe_ctrl.value = 2;
    }

    return 0;
}

int AudioOutRefresh(const AUDIO_OUT_ROUTE route)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    if (route == AUDIO_OUT_ROUTE_BUFFER)
        regs->audio_play_route[1].set_newframe_ctrl.value = 2;

    return 0;
}

// TODO: Need verify
int AudioOutStop(const AUDIO_OUT_ROUTE route)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    regs->audio_play_ctrl.bits.i2s_data_stop = 1;

    if (!regs->audio_play_ctrl.bits.work_soft_rstn) {
        // audio out is working
        regs->audio_play_int.value = (1 << 22); // cpu stop audio play request
        while (!regs->audio_play_int.bits.axi_state_idle);
        regs->audio_play_ctrl.bits.work_soft_rstn = 1;
    }

    if (route == AUDIO_OUT_ROUTE_PCM1IN) {
        regs->audio_play_ctrl.bits.r1_mix_en  = 0;
        regs->audio_play_ctrl.bits.r1_work_en = 0;
        regs->audio_play_i2s_in_info.bits.i2s_in_work_en = 0x0;
    }
    else if (route == AUDIO_OUT_ROUTE_BUFFER) {
        regs->audio_play_route[1].new_frame_start_addr.value = 0x00;
        regs->audio_play_route[1].new_frame_end_addr.value   = 0x00;
        regs->audio_play_int_en.bits.r2_set_newpcm_int_en    = 0;
    }

    regs->audio_play_int.value = (1 << 21); // cpu reopen audio play
    regs->audio_play_ctrl.bits.work_soft_rstn = 0;
    regs->audio_play_ctrl.bits.i2s_data_stop  = 0;

    return 0;
}

int AudioOutSetStoreMode(const AUDIO_OUT_STORE_MODE mode)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    regs->audio_play_route[1].indata.bits.pcm_store_mode = mode;

    return 0;
}

int AudioOutSetVolume(const AUDIO_OUT_ROUTE route, const int volume)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    unsigned short gain = 0;
    for (int i = 0; i < sizeof(table_dB)/sizeof(AUDIO_OUT_DB_TABLE); i++) {
            if (table_dB[i].value_dB == volume) {
                gain = table_dB[i].gain;
                break;
            }
    }

    if (!gain)
        return -1;

    if (route == AUDIO_OUT_ROUTE_PCM1IN) {
        regs->audio_play_route[0].vol_ctrl.bits.vol_level = gain;
        regs->audio_play_route[0].set_newframe_ctrl.value = 2;
        return 0;
    }
    else if (route == AUDIO_OUT_ROUTE_BUFFER) {
        regs->audio_play_route[1].vol_ctrl.bits.vol_level = gain;
        return 0;
    }
    else
        return -1;
}

int AudioOutSetMute(const AUDIO_OUT_ROUTE route, const unsigned int mute)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    if (route == AUDIO_OUT_ROUTE_PCM1IN) {
        regs->audio_play_ctrl.bits.r1_mix_en = mute ? 0 : 1;
        return 0;
    }
    else if (route == AUDIO_OUT_ROUTE_BUFFER) {
        regs->audio_play_ctrl.bits.r2_mix_en = mute ? 0 : 1;
        return 0;
    }
    else
        return -1;

    return 0;
}

int AudioOutSetTrack(AUDIO_OUT_TRACK track)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);

    if (track == AUDIO_OUT_STEREO_TRACK) {
        regs->audio_play_i2s_dacinfo.bits.play_channel_0_sel = 0;
        regs->audio_play_i2s_dacinfo.bits.play_channel_1_sel = 1;
    } else if (track == AUDIO_OUT_LEFT_TRACK) {
        regs->audio_play_i2s_dacinfo.bits.play_channel_0_sel = 0;
        regs->audio_play_i2s_dacinfo.bits.play_channel_1_sel = 0;
    } else if (track == AUDIO_OUT_RIGHT_TRACK) {
        regs->audio_play_i2s_dacinfo.bits.play_channel_0_sel = 1;
        regs->audio_play_i2s_dacinfo.bits.play_channel_1_sel = 1;
    }

    return 0;
}

//-------------------------------------------------------------------------------------------------

int AudioOutGetRemainBufferSize(void)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);
    int remain_buffer_size = regs->audio_play_route[1].playing_frame_end_addr.value - regs->audio_play_route[0].buffer_sdc_addr.value + 1;
    if (regs->audio_play_route[1].new_frame_start_addr.value != regs->audio_play_route[0].playing_frame_start_addr.value)
        remain_buffer_size += regs->audio_play_route[1].new_frame_pcm_len.value * sizeof(short);
    else
        remain_buffer_size = regs->audio_play_route[1].playing_frame_end_addr.value - regs->audio_play_route[0].buffer_sdc_addr.value + 1;
    return remain_buffer_size;
}

unsigned int AudioOutGetSdcAddr(void)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);
    return regs->audio_play_route[1].buffer_sdc_addr.value;
}

//=================================================================================================
int AudioOutSetupDAC(void)
{
    ClockEnable(CLOCK_AUDIO_DAC);

    volatile AUDIO_CODEC_CONTROL *acc_reg = (volatile AUDIO_CODEC_CONTROL *)(MISC_REG_AUDIO_CODEC_CONTROL);

    // Reset
    acc_reg->audio_dac_rstn = 1;
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;   // software clock
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;

    // MClk
    acc_reg->value = ((0<<12)|(0xa8<<4)|(1<<2)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;

    // Read
    acc_reg->value = ((0<<12)|(1<<3)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;

    volatile AUDIO_CODEC_DATA    *acd_reg = (volatile AUDIO_CODEC_DATA *)(MISC_REG_AUDIO_CODEC_DATA);
    unsigned int acd_value = acd_reg->value & 0xfffffffe;

    // Left Channel
    acc_reg->value = ((0<<12)|((0x0|acd_value)<<4)|(1<<2)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;
    acc_reg->value = ((3<<12)|(0x76<<4)|(1<<2)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;

    // Right Channel
    acc_reg->value = ((0<<12)|((0x1|acd_value)<<4)|(1<<2)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;
    acc_reg->value = ((3<<12)|(0x76<<4)|(1<<2)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;

    // Bit, I2S Mode
    acc_reg->value = ((2<<12)|(0x38<<4)|(1<<2)|(1<<0));
    acc_reg->audio_dac_sfrclk = 1; acc_reg->audio_dac_sfrclk = 0;

    return 0;
}

int AudioOutAutoMuteDAC(int enable)
{
#ifdef CONFIG_GX8008B
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);
    volatile AUDIO_CODEC_CONFIG *acf_reg = (volatile AUDIO_CODEC_CONFIG *)(MISC_REG_AUDIO_CODEC_CONFIG);

    acf_reg->soft_source_sel = 0x1;
    acf_reg->pop_ctrl_mode   = 0x4;
    acf_reg->pdm_en          = 0x1;
    acf_reg->pdm_out         = 0x8000;

    regs->audio_play_dac_lowpower_para.bits.lp_en  = enable;
    regs->audio_play_dac_highpower_para.bits.hp_en = enable;

#endif
    return 0;
}

