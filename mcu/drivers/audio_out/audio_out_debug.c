/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * All rights reserved!
 *
 * drv_audio_out_debug.c: MCU AudioOut Driver Debug
 *
 */

#include <audio_out_regs.h>
#include <base_addr.h>

#include <stdio.h>

#include <driver/audio_out.h>

void AudioOutDumpRegs(AUDIO_OUT_DUMP_FLAGS flags)
{
    volatile AUDIO_OUT_REGS *regs = (volatile AUDIO_OUT_REGS *)(MCU_REG_BASE_AOUT);
    
    printf("==========================================================\n");
    if (flags.generic) {
        // AUDIO_PLAY_CTRL
        printf("[%08X]AUDIO_PLAY_CTRL:                        %08X\n", (uint32_t)&regs->audio_play_ctrl, regs->audio_play_ctrl.value);
        if (flags.detail) {
            printf("    r1_mix_en:                 %d\n", regs->audio_play_ctrl.bits.r1_mix_en);
            printf("    r2_mix_en:                 %d\n", regs->audio_play_ctrl.bits.r2_mix_en);
            printf("    r3_mix_en:                 %d\n", regs->audio_play_ctrl.bits.r3_mix_en);
            printf("    r1_work_en:                %d\n", regs->audio_play_ctrl.bits.r1_work_en);
            printf("    r2_work_en:                %d\n", regs->audio_play_ctrl.bits.r2_work_en);
            printf("    r3_work_en:                %d\n", regs->audio_play_ctrl.bits.r3_work_en);
            
            printf("    spd_play_mode:             0x%x\n", regs->audio_play_ctrl.bits.spdif_play_mode);
            printf("    data_stop:                 %d\n", regs->audio_play_ctrl.bits.data_stop);
            printf("    silent_end:                %d\n", regs->audio_play_ctrl.bits.silent_end);
            
            printf("    pcm_w_en:                  %d\n", regs->audio_play_ctrl.bits.pcm_w_en);
            printf("    pcm_w_endian:              %d\n", regs->audio_play_ctrl.bits.pcm_w_endian);
            printf("    pcm_w_channel_sel:         %d\n", regs->audio_play_ctrl.bits.pcm_w_ch_sel);
            printf("    pcm_w_mono_en:             %d\n", regs->audio_play_ctrl.bits.pcm_w_mono_en);
            printf("    pcm_w_fs_sel:              0x%x\n", regs->audio_play_ctrl.bits.pcm_w_fs_sel);
            printf("    pcm_w_source_sel:          %d\n", regs->audio_play_ctrl.bits.pcm_w_source_sel);
            
            printf("    i2s_mute:                  %d\n", regs->audio_play_ctrl.bits.i2s_mute);
            printf("    spd_mute:                  %d\n", regs->audio_play_ctrl.bits.spdif_mute);
            printf("    work_soft_rstn:            %d\n", regs->audio_play_ctrl.bits.work_soft_rstn);
        }
    }
    
    if (flags.i2s) {
        // AUDIO_PLAY_I2S_DACINFO
        printf("[%08X]AUDIO_PLAY_I2S_DACINFO:                 %08X\n", (uint32_t)&regs->audio_play_i2s_dacinfo, regs->audio_play_i2s_dacinfo.value);
        if (flags.detail) {
            printf("    data_format:               0x%x\n", regs->audio_play_i2s_dacinfo.bits.data_format);
            printf("    length_mode:               0x%x\n", regs->audio_play_i2s_dacinfo.bits.length_mode);
            printf("    mclk_sel:                  0x%x\n", regs->audio_play_i2s_dacinfo.bits.mclk_sel);
            printf("    play_channel_0_sel:        0x%x\n", regs->audio_play_i2s_dacinfo.bits.play_channel_0_sel);
            printf("    play_channel_1_sel:        0x%x\n", regs->audio_play_i2s_dacinfo.bits.play_channel_1_sel);
        }
    }
    
    printf("----------------------------------------------------------\n");
    
    if (flags.spdif) {
        // AUDIO_SPDIF_CL1
        printf("[%08X]AUDIO_SPDIF_CL1:                        %08X\n", (uint32_t)&regs->audio_spdif_cl1, regs->audio_spdif_cl1.value);
        
        // AUDIO_SPDIF_CL1
        printf("[%08X]AUDIO_SPDIF_CL2:                        %08X\n", (uint32_t)&regs->audio_spdif_cl2, regs->audio_spdif_cl2.value);
        
        // AUDIO_SPDIF_CL1
        printf("[%08X]AUDIO_SPDIF_CR1:                        %08X\n", (uint32_t)&regs->audio_spdif_cr1, regs->audio_spdif_cr1.value);
        
        // AUDIO_SPDIF_CL1
        printf("[%08X]AUDIO_SPDIF_CR2:                        %08X\n", (uint32_t)&regs->audio_spdif_cr2, regs->audio_spdif_cr2.value);
        
        // AUDIO_SPDIF_U
        printf("[%08X]AUDIO_SPDIF_U:                          %08X\n", (uint32_t)&regs->audio_spdif_u, regs->audio_spdif_u.value);
        if (flags.detail) {
            printf("    ul:                        0x%x\n", regs->audio_spdif_u.bits.ul);
            printf("    ur:                        0x%x\n", regs->audio_spdif_u.bits.ur);
        }
    }
    
    printf("----------------------------------------------------------\n");
    
    if (flags.generic) {
        // AUDIO_PLAY_CHANNEL_SEL
        printf("[%08X]AUDIO_PLAY_CHANNEL_SEL:                 %08X\n", (uint32_t)&regs->audio_play_channel_sel, regs->audio_play_channel_sel.value);
        if (flags.detail) {
            printf("    r1_channel_0_sel:          0x%x\n", regs->audio_play_channel_sel.bits.r1_channel_0_sel);
            printf("    r1_channel_1_sel:          0x%x\n", regs->audio_play_channel_sel.bits.r1_channel_1_sel);
            printf("    r2_channel_0_sel:          0x%x\n", regs->audio_play_channel_sel.bits.r2_channel_0_sel);
            printf("    r2_channel_1_sel:          0x%x\n", regs->audio_play_channel_sel.bits.r2_channel_1_sel);
            printf("    r3_channel_0_sel:          0x%x\n", regs->audio_play_channel_sel.bits.r3_channel_0_sel);
            printf("    r3_channel_1_sel:          0x%x\n", regs->audio_play_channel_sel.bits.r3_channel_1_sel);
            printf("    write_channel_0_sel:       0x%x\n", regs->audio_play_channel_sel.bits.write_channel_0_sel);
            printf("    write_channel_1_sel:       0x%x\n", regs->audio_play_channel_sel.bits.write_channel_1_sel);
        }
    }
    
    if (flags.pcm_w) {
        // AUDIO_PLAY_PCM_W_NUM
        printf("[%08X]AUDIO_PLAY_PCM_W_NUM:                   %08X\n", (uint32_t)&regs->audio_play_pcm_w_num, regs->audio_play_pcm_w_num.value);
        
        // AUDIO_PLAY_PCM_W_BUFFER_SADDR
        printf("[%08X]AUDIO_PLAY_PCM_W_BUFFER_SADDR:          %08X\n", (uint32_t)&regs->audio_play_pcm_w_buffer_saddr, regs->audio_play_pcm_w_buffer_saddr.value);
        
        // AUDIO_PLAY_PCM_W_BUFFER_SIZE
        printf("[%08X]AUDIO_PLAY_PCM_W_BUFFER_SIZE:           %08X\n", (uint32_t)&regs->audio_play_pcm_w_buffer_size, regs->audio_play_pcm_w_buffer_size.value);
        
        // AUDIO_PLAY_PCM_W_SDC_ADDR
        printf("[%08X]AUDIO_PLAY_PCM_W_SDC_ADDR:              %08X\n", (uint32_t)&regs->audio_play_pcm_w_sdc_addr, regs->audio_play_pcm_w_sdc_addr.value);
    }
    
    if (flags.generic) {
        // AUDIO_PLAY_INT_EN
        printf("[%08X]AUDIO_PLAY_INT_EN:                      %08X\n", (uint32_t)&regs->audio_play_int_en, regs->audio_play_int_en.value);
        
        // AUDIO_PLAY_INT_STATE
        printf("[%08X]AUDIO_PLAY_INT_STATE:                   %08X\n", (uint32_t)&regs->audio_play_int, regs->audio_play_int.value);
        if (flags.detail) {
            printf("    r1_set_newpcm_int:         0x%x\n", regs->audio_play_int.bits.r1_set_newpcm_int);
            printf("    r2_set_newpcm_int:         0x%x\n", regs->audio_play_int.bits.r2_set_newpcm_int);
            printf("    r3_set_newpcm_int:         0x%x\n", regs->audio_play_int.bits.r3_set_newpcm_int);
            
            printf("    i2s_in_pcm_read_err2_int:  0x%x\n", regs->audio_play_int.bits.i2s_in_pcm_read_err2_int);
            printf("    i2s_in_pcm_read_err_int:   0x%x\n", regs->audio_play_int.bits.i2s_in_pcm_read_err_int);
            printf("    pcm_w_done_int:            0x%x\n", regs->audio_play_int.bits.pcm_w_done_int);
            
            printf("    reg_info_fresh:            0x%x\n", regs->audio_play_int.bits.reg_info_fresh);
            
            printf("    axi_state_idle:            0x%x\n", regs->audio_play_int.bits.axi_state_idle);
            printf("    cpu_reopen_aup:            0x%x\n", regs->audio_play_int.bits.cpu_reopen_aup);
            printf("    cpu_stop_aup_req:          0x%x\n", regs->audio_play_int.bits.cpu_stop_aup_req);
        }
    }
    
    printf("----------------------------------------------------------\n");
    int i;
    for (i = 0; i < 3; i++) {
        
        if (i == 0 && flags.r1 == 0)
            continue;
        if (i == 1 && flags.r2 == 0)
            continue;
        if (i == 2 && flags.r3 == 0)
            continue;
        
        // AUDIO_PLAY_Rx_INDATA
        printf("[%08X]AUDIO_PLAY_R%d_INDATA:                   %08X\n", (uint32_t)&regs->audio_play_route[i].indata, i + 1, regs->audio_play_route[i].indata.value);
        if (flags.detail) {
            printf("    fs_mode:                   0x%x\n", regs->audio_play_route[i].indata.bits.fs_mode);
            printf("    channel_sel:               0x%x\n", regs->audio_play_route[i].indata.bits.channel_sel);
            printf("    pcm_endian:                0x%x\n", regs->audio_play_route[i].indata.bits.pcm_endian);
            printf("    r%d_pcm_store_mode:         0x%x\n", i + 1, regs->audio_play_route[i].indata.bits.pcm_store_mode);
            printf("    pcm_bit_size:              0x%x\n", regs->audio_play_route[i].indata.bits.pcm_bit_size);
            printf("    r%d_src_sel:                0x%x\n", i + 1, regs->audio_play_route[i].indata.bits.src_sel);
        }
        
        // AUDIO_PLAY_Rx_VOL_CTRL
        printf("[%08X]AUDIO_PLAY_R%d_VOL_CTRL:                 %08X\n", (uint32_t)&regs->audio_play_route[i].vol_ctrl, i + 1, regs->audio_play_route[i].vol_ctrl.value);
        
        // AUDIO_PLAY_Rx_BUFFER_START_ADDR
        printf("[%08X]AUDIO_PLAY_R%d_BUFFER_START_ADDR:        %08X\n", (uint32_t)&regs->audio_play_route[i].buffer_start_addr, i + 1, regs->audio_play_route[i].buffer_start_addr.value);
        
        // AUDIO_PLAY_Rx_BUFFER_SIZE
        printf("[%08X]AUDIO_PLAY_R%d_BUFFER_SIZE:              %08X\n", (uint32_t)&regs->audio_play_route[i].buffer_size, i + 1, regs->audio_play_route[i].buffer_size.value);
        
        // AUDIO_PLAY_Rx_BUFFER_SDC_ADDR
        printf("[%08X]AUDIO_PLAY_R%d_BUFFER_SDC_ADDR:          %08X\n", (uint32_t)&regs->audio_play_route[i].buffer_sdc_addr, i + 1, regs->audio_play_route[i].buffer_sdc_addr.value);
        
        // AUDIO_PLAY_Rx_NEW_FRAME_START_ADDR
        printf("[%08X]AUDIO_PLAY_R%d_NEW_FRAME_START_ADDR:     %08X\n", (uint32_t)&regs->audio_play_route[i].new_frame_start_addr, i + 1, regs->audio_play_route[i].new_frame_start_addr.value);
        
        // AUDIO_PLAY_Rx_NEW_FRAME_END_ADDR
        printf("[%08X]AUDIO_PLAY_R%d_NEW_FRAME_END_ADDR:       %08X\n", (uint32_t)&regs->audio_play_route[i].new_frame_end_addr, i + 1, regs->audio_play_route[i].new_frame_end_addr.value);
        
        // AUDIO_PLAY_Rx_PLAYING_FRAME_START_ADDR
        printf("[%08X]AUDIO_PLAY_R%d_PLAYING_FRAME_START_ADDR: %08X\n", (uint32_t)&regs->audio_play_route[i].playing_frame_start_addr, i + 1, regs->audio_play_route[i].playing_frame_start_addr.value);
        
        // AUDIO_PLAY_Rx_PLAYING_FRAME_END_ADDR
        printf("[%08X]AUDIO_PLAY_R%d_PLAYING_FRAME_END_ADDR:   %08X\n", (uint32_t)&regs->audio_play_route[i].playing_frame_end_addr, i + 1, regs->audio_play_route[i].playing_frame_end_addr.value);

        // AUDIO_PLAY_Rx_NEW_FRAME_PCM_LEN
        printf("[%08X]AUDIO_PLAY_R%d_NEW_FRAME_PCM_LEN:        %08X\n", (uint32_t)&regs->audio_play_route[i].new_frame_pcm_len, i + 1, regs->audio_play_route[i].new_frame_pcm_len.value);
        
        // AUDIO_PLAY_Rx_PLAYING_FRAME_PCM_LEN
        printf("[%08X]AUDIO_PLAY_R%d_PLAYING_FRAME_PCM_LEN:    %08X\n", (uint32_t)&regs->audio_play_route[i].playing_frame_pcm_len, i + 1, regs->audio_play_route[i].playing_frame_pcm_len.value);
        
        // AUDIO_PLAY_Rx_SET_NEWFRAME_CTRL
        printf("[%08X]AUDIO_PLAY_R%d_SET_NEWFRAME_CTRL:        %08X\n", (uint32_t)&regs->audio_play_route[i].set_newframe_ctrl, i + 1, regs->audio_play_route[i].set_newframe_ctrl.value);
        if (flags.detail) {
            printf("    r%d_set_newpcm_int:         0x%x\n", i + 1, regs->audio_play_route[i].set_newframe_ctrl.bits.r_set_newframe_start);
            printf("    r%d_set_newpcm_int:         0x%x\n", i + 1, regs->audio_play_route[i].set_newframe_ctrl.bits.r_set_newframe_finish);
        }
        
        printf("----------------------------------------------------------\n");
    }
    
    if (flags.spdif) {
        // AUDIO_SPDIF_CL3
        printf("[%08X]AUDIO_SPDIF_CL3:                        %08X\n", (uint32_t)&regs->audio_spdif_cl3, regs->audio_spdif_cl3.value);
        
        // AUDIO_SPDIF_CL4
        printf("[%08X]AUDIO_SPDIF_CL4:                        %08X\n", (uint32_t)&regs->audio_spdif_cl4, regs->audio_spdif_cl4.value);
        
        // AUDIO_SPDIF_CR3
        printf("[%08X]AUDIO_SPDIF_CR3:                        %08X\n", (uint32_t)&regs->audio_spdif_cr3, regs->audio_spdif_cr3.value);
        
        // AUDIO_SPDIF_CR4
        printf("[%08X]AUDIO_SPDIF_CR4:                        %08X\n", (uint32_t)&regs->audio_spdif_cr4, regs->audio_spdif_cr4.value);
        
        // AUDIO_SPDIF_CL5
        printf("[%08X]AUDIO_SPDIF_CL5:                        %08X\n", (uint32_t)&regs->audio_spdif_cl5, regs->audio_spdif_cl5.value);
        
        // AUDIO_SPDIF_CL6
        printf("[%08X]AUDIO_SPDIF_CL6:                        %08X\n", (uint32_t)&regs->audio_spdif_cl6, regs->audio_spdif_cl6.value);
        
        // AUDIO_SPDIF_CR5
        printf("[%08X]AUDIO_SPDIF_CR5:                        %08X\n", (uint32_t)&regs->audio_spdif_cr5, regs->audio_spdif_cr5.value);
        
        // AUDIO_SPDIF_CR6
        printf("[%08X]AUDIO_SPDIF_CR6:                        %08X\n", (uint32_t)&regs->audio_spdif_cr6, regs->audio_spdif_cr6.value);
    }
    
    printf("----------------------------------------------------------\n");
    
    if (flags.i2s) {
        // AUDIO_PLAY_I2S_IN_INFO
        printf("[%08X]AUDIO_PLAY_I2S_IN_INFO:                 %08X\n", (uint32_t)&regs->audio_play_i2s_in_info, regs->audio_play_i2s_in_info.value);
        if (flags.detail) {
            printf("    i2s_in_data_format:        0x%x\n", regs->audio_play_i2s_in_info.bits.i2s_in_data_format);
            printf("    i2s_in_bclk_inv_en:        %d\n", regs->audio_play_i2s_in_info.bits.i2s_in_bclk_inv_en);
            printf("    i2s_in_lrclk_inv_en:       %d\n", regs->audio_play_i2s_in_info.bits.i2s_in_lrclk_inv_en);
            
            printf("    i2s_in_pcm_fs:             0x%x\n", regs->audio_play_i2s_in_info.bits.i2s_in_pcm_fs);
            printf("    i2s_in_work_en:            %d\n", regs->audio_play_i2s_in_info.bits.i2s_in_work_en);
            printf("    i2s_in_full_cap:           0x%x\n", regs->audio_play_i2s_in_info.bits.i2s_in_full_cap);
            
            printf("    i2s_in_data_length:        0x%x\n", regs->audio_play_i2s_in_info.bits.i2s_in_data_length);
        }
    }
    
}
