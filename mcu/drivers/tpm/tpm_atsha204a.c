/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * tpm_atsha204a.c: Tpm Chip
 *
 */

#include <stdio.h>
#include <string.h>

#include <driver/tpm_atsha204a.h>
#include <driver/i2c.h>
#include <driver/delay.h>

static struct
{
    int i2c_bus;
    int chip_addr;
} s_tpm_dev = {
    .i2c_bus = 1,
    .chip_addr = 0x64,
};

//=================================================================================================

static void _short2char(unsigned char *data, unsigned short int a)
{
    data[0]  = (a & 0x0000FF00) >>  8;
    data[1]  = (a & 0x000000FF) >>  0;
}

static void _int24_2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0x00FF0000) >>  16;
    data[1]  = (a & 0x0000FF00) >>  8;
    data[2]  = (a & 0x000000FF) >>  0;
}

static void _int2char(unsigned char *data, unsigned int a)
{
    data[0]  = (a & 0xFF000000) >> 24;
    data[1]  = (a & 0x00FF0000) >> 16;
    data[2]  = (a & 0x0000FF00) >>  8;
    data[3]  = (a & 0x000000FF) >>  0;
}

static void _addr2buf(unsigned char *data, unsigned int addr, int alen)
{
    switch (alen) {
        case 1:
            data[0] = addr&0xff;
            break;
        case 2:
            _short2char(data, addr);
            break;
        case 3:
            _int24_2char(data, addr);
            break;
        case 4:
            _int2char(data, addr);
            break;
    }
}

static int _gx_i2c_send(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    int i2c_bus = s_tpm_dev.i2c_bus;
    int chip = s_tpm_dev.chip_addr;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};

    _addr2buf(addr_buf, addr, alen);

    msg[0].addr  = chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = chip;
    msg[1].flags =  0;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }

    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);

    return status ? -1 : 0;
}

static int _gx_i2c_recv(unsigned int addr, int alen, unsigned char *buf, int len)
{
    int status;
    int i2c_bus = s_tpm_dev.i2c_bus;
    int chip = s_tpm_dev.chip_addr;
    struct i2c_msg msg[2];
    void *i2c_dev = NULL;
    unsigned char addr_buf[4] = {0};

    _addr2buf(addr_buf, addr, alen);

    msg[0].addr  = chip;
    msg[0].flags = 0;
    msg[0].buf   = addr_buf;
    msg[0].len   = alen;

    msg[1].addr  = chip;
    msg[1].flags = I2C_M_RD | I2C_M_NO_RD_ACK;
    msg[1].buf   = buf;
    msg[1].len   = len;

    i2c_dev = gx_i2c_open(i2c_bus);
    if (!i2c_dev) {
        printf("open i2c dev failed.\n");
        return -1;
    }

    status = gx_i2c_transfer(i2c_dev, msg, 2);
    gx_i2c_close(i2c_dev);

    return status ? -1 : 0;
}

//=================================================================================================

/** \brief This function calculates CRC.
 *
 * \param[in] length number of bytes in buffer
 * \param[in] data pointer to data for which CRC should be calculated
 * \param[out] crc pointer to 16-bit CRC
 */
static void _TpmAtsha204aCalculateCRC(uint8_t length, uint8_t *data, uint8_t *crc) {
    uint8_t counter;
    uint16_t crc_register = 0;
    uint16_t polynom = 0x8005;
    uint8_t shift_register;
    uint8_t data_bit, crc_bit;

    for (counter = 0; counter < length; counter++) {
      for (shift_register = 0x01; shift_register > 0x00; shift_register <<= 1) {
         data_bit = (data[counter] & shift_register) ? 1 : 0;
         crc_bit = crc_register >> 15;
         crc_register <<= 1;
         if (data_bit != crc_bit)
            crc_register ^= polynom;
      }
    }
    crc[0] = (uint8_t) (crc_register & 0x00FF);
    crc[1] = (uint8_t) (crc_register >> 8);
}

static int _TpmAtsha204aWakeUp(void)
{
    unsigned char tx_buf[1] = {0};
    unsigned char rx_buf[4] = {0};

    _gx_i2c_send(0, 1, tx_buf, sizeof(tx_buf));

    mdelay(3);  // at least 2.5ms

    _gx_i2c_recv(0, 1, rx_buf, 4);

    // Verify status rx_buf.
    if ((rx_buf[2] != 0x33) || (rx_buf[3] != 0x43))
        return -1;

    return 0;
}

//-------------------------------------------------------------------------------------------------

int TpmAtsha204aInit(void)
{
    //set i2c bus speed to 100 kb/s
    gx_i2c_set_speed(s_tpm_dev.i2c_bus, 100);

    int ret;
    ret = _TpmAtsha204aWakeUp();
    if (ret)
    {
        printf("atsha204a init fail\n");
        return ret;
    }

    return ret;
}

int TpmAtsha204aDone(void)
{
    return 0;
}

//-------------------------------------------------------------------------------------------------

int TpmAtsha204aReadSN(unsigned char *read_SN)
{
    unsigned char tx_buf[8] = {0};

    tx_buf[0] = 0x07;
    tx_buf[1] = 0x02; // read cmd
    tx_buf[2] = 0x00;
    tx_buf[3] = 0x00;
    tx_buf[4] = 0x00;
    _TpmAtsha204aCalculateCRC(5 , &tx_buf[0], &tx_buf[5]);
    _gx_i2c_send(0x03, 1, tx_buf, 7);

    mdelay(1);
    _gx_i2c_recv(0, 1, read_SN, 7);

    if( read_SN[1] == 0x01 && read_SN[2] == 0x23)
        return 0;
    else
        return -1;
}

