/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * console.c: MCU CONSOLE driver
 *
 */
#include <common.h>
#include <driver/uart.h>
#include <board_config.h>

static int console_port = 0;

#define CONSOLE_PORT     CONFIG_SERIAL_PORT
#define CONSOLE_BAUDRATE CONFIG_SERIAL_BAUD_RATE

int ConsoleInit(void)
{
    console_port = CONSOLE_PORT;
    return UartInit(console_port, CONSOLE_BAUDRATE);
}

void ConsoleSendChar(int ch)
{
    UartSendByte(console_port, ch);
}

void ConsoleCompatibleSendChar(char ch)
{
    if (ch == '\n')
        UartSendByte(console_port, '\r');
    UartSendByte(console_port, ch);
}

int ConsoleTstc(void)
{
    return UartCanRecvByte(console_port);
}
