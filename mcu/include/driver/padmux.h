/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * padmux.h: pad multiplexing configure
 *
 */

#ifndef __PADMUX_H__
#define __PADMUX_H__

typedef struct {
	unsigned char pad_id;
	unsigned char pad_function;
} PADMUX_PAD_CONFIG;

int PadMuxInit(const PADMUX_PAD_CONFIG *config_table, int table_size);

int PadMuxSetFunction(int pad_id, int pad_function);
int PadMuxGetFunction(int pad_id);

#ifdef CONFIG_MCU_ENABLE_DEBUG
void PadMuxDumpRegs(void);
#else
#define PadMuxDumpRegs() while(0)
#endif

#endif  /* __PADMUX_H__ */
