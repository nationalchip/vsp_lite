/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * audio_ref.h: MCU AudioRef Driver HAL
 *
 */

#ifndef __AUDIO_REF_H__
#define __AUDIO_REF_H__

// Audio Ref Initialize and Done
typedef struct {
    short       *buffer_addr;
    unsigned int buffer_size;
    unsigned int frame_size;
    unsigned int channel_size;
    unsigned int channel_num;
    unsigned int sample_rate;
} AUDIO_REF_CONFIG;

int AudioRefInit(const AUDIO_REF_CONFIG *config);
void AudioRefDone(void);

#endif  /* __AUDIO_REF_H__ */
