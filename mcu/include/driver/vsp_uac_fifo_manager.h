/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_uac_fifo_manager.h : uac fifo manage
 *
 */

#ifndef __VSP_UAC_FIFO_MANAGER_H__
#define __VSP_UAC_FIFO_MANAGER_H__

#include <types.h>
#include <autoconf.h>

#if defined(CONFIG_VSP_UAC_WORKS_ON_SRAM) || defined(CONFIG_VSP_PLC_WORKS_ON_SRAM)
#define AIN_BUFFER_NUM CONFIG_VSP_SRAM_CONTEXT_NUM
#endif

#if defined(CONFIG_VSP_UAC_WORKS_ON_DDR) || defined(CONFIG_VSP_UAC_WORKS_ON_LINUX) || defined(CONFIG_VSP_PLC_WORKS_ON_DDR)
#define AIN_BUFFER_NUM CONFIG_VSP_DDR_CONTEXT_NUM
#endif

typedef int (*RD_CALLBACK)(unsigned int rd_addr,unsigned int size);

unsigned int AudioBuffMngrInit(void *aud_buff,unsigned int aud_buff_size,bool aout,unsigned int call_interval);

void AudioBuffMngrDone(unsigned int handle);

void AudioBuffProduceNotify(unsigned int handle, int do_check);
void * AudioBuffProduce(unsigned int handle,void *buff,unsigned int size);

int AudioBuffConsumeCheck(unsigned int handle, unsigned int want_len);
void * AudioBuffConsume(unsigned int handle,unsigned int size);
int AudioBuffConsumerRegister(unsigned int handle,RD_CALLBACK aud_consumer);
void AudioBuffConsumeStart(unsigned int handle);
void AudioBuffConsumeStop(unsigned int handle);

unsigned int AudioBuffTotalLen(unsigned int handle);
unsigned int AudioBuffUsedLen(unsigned int handle);
int AudioBuffUsedInfo(unsigned int handle);
int AudioBuffConsumeCheck(unsigned int handle, unsigned int want_len);

// 用于监测 uac fifo 的状态，可放在 tick 中调用
void AudioBuffPrintStatus(unsigned int handle);

#endif
