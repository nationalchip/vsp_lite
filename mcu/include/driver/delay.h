/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * delay.h: MCU delay
 *
 */

#ifndef __DELAY_H__
#define __DELAY_H__

void udelay(unsigned int us);
void mdelay(unsigned int ms);

unsigned long long get_time_us(void);
unsigned int get_time_ms(void);

void DelayInit(void);

#endif
