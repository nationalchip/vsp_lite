/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * uart_message.h: MCU uart communication protocol driver
 *
 */

#ifndef __MCU_UART_MESSAGE__
#define __MCU_UART_MESSAGE__


#define BT_CODE_ADDR_START 0x200000
#define BT_FLASH_LEN 253

//=================================================================================================
// PLC EVENT
typedef enum {
    UART_COMMAND_TYPE_GENERAL = 0,
    UART_COMMAND_TYPE_BT,
    UART_COMMAND_TYPE_8008,
} UART_COMMAND_TYPE;

typedef enum {
    UCI_GENERAL_ACK = 0,

    //command id tx
    UCI_NOTIFY_TX_WAKEUP_STATE = 0,
    UCI_NOTIFY_TX_MUSIC_STATE,
    UCI_NOTIFY_TX_CALLING_STATE,
    UCI_NOTIFY_TX_OPUS_PCM_DATA,
    UCI_NOTIFY_TX_ORIGIN_PCM_DATA,
    UCI_NOTIFY_TX_PCM_DATA_END,
    UCI_NOTIFY_TX_MTC,
    UCI_NOTIFY_TX_NAVI = 0x08,
    UCI_NOTIFY_TX_CANCEL,
    UCI_NOTIFY_TX_CARMOUNT_CONTROL,
    UCI_NOTIFY_TX_IMAGEINFO = 0x20,
    UCI_NOTIFY_TX_PREPARE_BURN_BT = 0x21,
    UCI_NOTIFY_TX_SET_MAC = 0x23,
    UCI_NOTIFY_TX_SET_FREQ = 0x24,
    UCI_NOTIFY_TX_GET_BKMAC = 0x25,
    UCI_NOTIFY_TX_GET_BKVER = 0x26,
    UCI_NOTIFY_TX_SET_BURN_BT = 0x27,
    UCI_NOTIFY_TX_GET_FREQ = 0x28,
    UCI_NOTIFY_TX_2MIC_OPUS_DATA = 0X30,

    //command id rx
    UCI_NOTIFY_RX_CONNECT_STATE = 0,
    UCI_NOTIFY_RX_WAV_STREAM_STATE = 0x03,
    UCI_NOTIFY_RX_MUSIC_STATE = 0x06,
    UCI_NOTIFY_RX_MIC_STATE = 0x07,
    UCI_NOTIFY_RX_LED_STATE = 0x0C,
    UCI_NOTIFY_RX_MTC_STATE = 0x0E,
    UCI_NOTIFY_RX_QUERY_IMAGEINFO = 0x20,
    UCI_NOTIFY_RX_PREPARE_BURN_BT = 0x21,
    UCI_NOTIFY_RX_TRIGGER_BURN_BT = 0x22,
} UART_COMMAND_ID;

typedef enum {
    UCA_WAKE_UP = 0,

    //music state
    UCA_MUSIC_PLAY = 0,
    UCA_MUSIC_PAUSE,
    UCA_MUSIC_PREV,
    UCA_MUSIC_NEXT,
    UCA_VOLUME_UP,
    UCA_VOLUME_DOWN,
    UCA_SONGNAME,

    //phone state
    UCA_PHONE_ANSWER = 0,
    UCA_PHONE_REJECT,

    //MTC STATE
    UCA_TEST_SUCCESS = 0,
    UCA_TEST_FAIL,
    UCA_TEST_DATA,

    //Query information
    UCA_QUERY_IMAGEINFO = 0,

    UCA_BACKNAVI = 0,
    UCA_STOPNAVI,

    UCA_CARMOUNT_CONTROL_OPEN = 0,
    UCA_CARMOUNT_CONTROL_CLOSE,

    UCA_CANCEL = 0,


    UCA_VOLUME_NUTE = 99,
    UCA_VOLUME_0 = 99,
    UCA_VOLUME_1,
    UCA_VOLUME_2,
    UCA_VOLUME_3,
    UCA_VOLUME_4,
    UCA_VOLUME_5,
    UCA_VOLUME_6,
    UCA_VOLUME_7,
    UCA_VOLUME_8,
    UCA_VOLUME_9,
    UCA_VOLUME_10,
    UCA_VOLUME_11,
    UCA_VOLUME_12,
    UCA_VOLUME_13,
    UCA_VOLUME_14,
    UCA_VOLUME_15,
    UCA_VOLUME_16,
    UCA_VOLUME_17,

} UART_COMMAND_ACTION;

//=================================================================================================
char UartGetPacket(void);

char UartSendCommand(UART_COMMAND_ID cmd, UART_COMMAND_ACTION data);

char UartSendCommandWithData(UART_COMMAND_ID cmd, unsigned char *cmdData, unsigned char dataLen);

void UartGetImageinfoVersion(unsigned char *image_info, unsigned char len);

char UartSendImageinfo(void);

char UartSendPrepareBurnBtResult(unsigned char res);

char UartSendWavEnd(void);

char UartSendWav(char *outbuff, unsigned int outbuff_length);

int UartGetCommandID(void);

int UartGetCommandState(void);

unsigned char *UartGetCommandData(void);

unsigned char UartGetCommandDataLength(void);

char UartSendCompressedAudio(char *outbuff, char length);

#endif
