/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * udc.h: USB Device Controller Driver API
 *
 */

#ifndef __UDC_H__
#define __UDC_H__

enum udc_speed {
    UDC_SPEED_HIGH,
    UDC_SPEED_FULL,
};

int UdcInit(enum udc_speed speed);
int UdcDone(void);

// Minimize udc power consumption when UDC is not used
int UdcDisable(void);

#endif /* __UDC_H__ */
