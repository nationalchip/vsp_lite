/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * cpu.h: MCU CPU Driver
 *
 */

#ifndef __MCU_CPU_H__
#define __MCU_CPU_H__

#include <autoconf.h>
#include <vsp_message.h>

#include <types.h>
#include <driver/irq.h>

//=================================================================================================

typedef void (*CPU_VSP_CALLBACK)(volatile unsigned int *message, void *priv);

int CpuSetVspCallback(CPU_VSP_CALLBACK callback, void *priv);
int CpuPostVspMessage(const uint32_t *message, const uint32_t size);

int CpuGetVspMessage(uint32_t *message, const uint32_t size);

//-------------------------------------------------------------------------------------------------
int CpuReboot(void);
int CpuSuspend(void);
int CpuResume(VSP_CPU_WAKE_UP_REASON reason);
int CpuWakeup(void);
int CpuInitializeDDR(void); // Only initialize DDR, and sleep A7
int CpuSuspendDDR(void);
int CpuResumeDDR(void);
int CpuWatchdogInit(void);

//=================================================================================================

#ifdef CONFIG_MCU_ENABLE_DEBUG
void CpuDumpRegs(int detail);
#else
#define CpuDumpRegs(x) while(0)
#endif

#endif  /* __MCU_CPU_H__ */
