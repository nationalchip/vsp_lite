/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * clock.h: MCU's Clock management API
 *
 */

#ifndef __CLOCK_H__
#define __CLOCK_H__

// CLK_CONFIG_GATE_1
#define APB2_0_CLK_EN           (0x1ULL << 1)
#define APB2_UART_CLK_EN        (0x1ULL << 2)
#define SNPU_CLK_EN             (0x1ULL << 3)
#define DSP_CLK_EN              (0x1ULL << 4)
#define SECURE_CLK_EN           (0x1ULL << 5)
#define AUDIO_PLAY_CLK_EN       (0x1ULL << 6)
#define LODAC_CLK_EN            (0x1ULL << 7)
/* 8 bit reserve */
#define SRL_AUDIO_PLAY_CLK_EN   (0x1ULL << 9)
#define CONTROLLER_SRAM_CLK_EN  (0x1ULL << 10)
#define MCU_SRAM_CLK_EN         (0x1ULL << 11)
#define AUDIO_PLAY_SRAM_CLK_EN  (0x1ULL << 12)
#define SNPU_SRAM_CLK_EN        (0x1ULL << 13)
#define DSP_SRAM_CLK_EN         (0x1ULL << 14)
#define AUDIO_IN_SRAM_CLK_EN    (0x1ULL << 15)
#define USB_SRAM_CLK_EN         (0x1ULL << 16)
#define USB_PHY_CLK_EN          (0x1ULL << 17)
/* 18 bit reserve */
#define AHB_USB_CLK_EN          (0x1ULL << 19)
#define AUDIO_IN_CLK_EN         (0x3fULL << 20)
/* 31 - 30 bit reserve */

// CLK_CONFIG_GATE_2
#define SCPU_CLK_EN             (0x1ULL << (32 + 12))
#define SCPU_AHB_CLK_EN         (0x1ULL << (32 + 14))
#define RTC_CLK_EN              (0x1ULL << (32 + 15))
#define COUNTER_CLK_EN          (0x1ULL << (32 + 16))
#define OTP_CLK_EN              (0x1ULL << (32 + 17))
#define REF_CLK_OUT0_EN         (0x1ULL << (32 + 18))
#define REF_CLK_OUT1_EN         (0x1ULL << (32 + 19))
#define UART1_REF_CLK_EN        (0x1ULL << (32 + 20))
#define SPI_MASTER_CLK_EN       (0x1ULL << (32 + 21))
#define SPI_FLASH_CLK_EN        (0x1ULL << (32 + 22))

// CLOCK TYPE
#define CLOCK_AUDIO_PLAY    ((SRL_AUDIO_PLAY_CLK_EN) | (AUDIO_PLAY_SRAM_CLK_EN))
#define CLOCK_AUDIO_DAC     (LODAC_CLK_EN)
#define CLOCK_AUDIO_IN      ((AUDIO_IN_CLK_EN) | (AUDIO_IN_SRAM_CLK_EN))
#define CLOCK_SECURE        (SECURE_CLK_EN)
#define CLOCK_DSP           ((DSP_CLK_EN) | (DSP_SRAM_CLK_EN))
#define CLOCK_UDC           ((USB_SRAM_CLK_EN) | (USB_PHY_CLK_EN))
#define CLOCK_SNPU          ((SNPU_CLK_EN) | (SNPU_SRAM_CLK_EN))
#define CLOCK_RTC           (RTC_CLK_EN)
#define CLOCK_COUNTER       (COUNTER_CLK_EN)
#define CLOCK_OTP           (OTP_CLK_EN)
#define CLOCK_REF_OUT0      (REF_CLK_OUT0_EN)
#define CLOCK_REF_OUT1      (REF_CLK_OUT1_EN)
#define CLOCK_UART1_REF     (UART1_REF_CLK_EN)
#define CLOCK_SPI_MASTER    (SPI_MASTER_CLK_EN)
#define CLOCK_SPI_FLASH     (SPI_FLASH_CLK_EN)

#define CLOCK_MODULES       ((CLOCK_AUDIO_PLAY) | (CLOCK_AUDIO_DAC) | (CLOCK_AUDIO_IN) \
                                | (CLOCK_SECURE) | (CLOCK_DSP) | (CLOCK_UDC) | (CLOCK_SNPU) \
                                | (CLOCK_OTP) | (CLOCK_REF_OUT0) |(CLOCK_REF_OUT1) | (CLOCK_SPI_MASTER))

void ClockEnable(uint64_t clock_type);
void ClockDisable(uint64_t clock_type);

void ClockInit(void);

// Wakeup Source
typedef enum {
    WAKEUP_SOURCE_AVAD  = 0x01,
    WAKEUP_SOURCE_RTC   = 0x02,
    WAKEUP_SOURCE_GPIO  = 0x04,
} WAKEUP_SOURCE;

// return the wakeup source which wakeup mcu
WAKEUP_SOURCE ClockSleep(WAKEUP_SOURCE wakeup_source);

#endif
