/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * imageinfo.h: MCU IMAGEINFO driver
 *
 */

#ifndef __IMAGEINFO_H__
#define __IMAGEINFO_H__

#include <common.h>

typedef struct {
    unsigned int   magic:32;
    unsigned int   imageinfo_version:32;
    unsigned int   image_version:32;
    unsigned int   image_crc32:32;
    unsigned int   imageinfo_crc32:32;
} IMAGE_INFO;

#define IMAGEINFO_MAGIC       0x494D4948 // "IMIH"
#define IMAGEINFO_CRC32_LEN   4
#define IMAGEINFO_SN_LEN      8
#define IMAGEINFO_SIZE        1024

int ImageInfoGetImageVersion(unsigned int *image_version);
int ImageInfoGetImageCRC32(unsigned int *crc32);
int ImageInfoInit(unsigned int offset);
void ImageInfoDone(void);

#endif /* __IMAGEINFO_H__ */

