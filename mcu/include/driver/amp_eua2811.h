/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * amp_eua2811.h: i2c Driver for eua2811
 *
 */

#ifndef __AMP_EUA2811_H__
#define __AMP_EUA2811_H__

int AmpEua2811Init(int i2c_bus, int chip);

#endif
