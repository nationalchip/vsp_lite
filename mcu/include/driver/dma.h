
/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 */

#ifndef __DMA_H__
#define __DMA_H__

#include <types.h>

#define BIT(n)                   (1 << n)
#define DMA_SUPPORTS_MEM_TO_MEM  BIT(0)
#define DMA_SUPPORTS_MEM_TO_DEV  BIT(1)
#define DMA_SUPPORTS_DEV_TO_MEM  BIT(2)
#define DMA_SUPPORTS_DEV_TO_DEV  BIT(3)
#define DMA_SUPPORTS_SPI         BIT(8)
#define BLOCK_TS                 0xFFF

enum dma_slave_buswidth {
    DMA_SLAVE_BUSWIDTH_UNDEFINED = 0,
    DMA_SLAVE_BUSWIDTH_1_BYTE    = 1,
    DMA_SLAVE_BUSWIDTH_2_BYTES   = 2,
    DMA_SLAVE_BUSWIDTH_3_BYTES   = 3,
    DMA_SLAVE_BUSWIDTH_4_BYTES   = 4,
    DMA_SLAVE_BUSWIDTH_8_BYTES   = 8,
    DMA_SLAVE_BUSWIDTH_16_BYTES  = 16,
    DMA_SLAVE_BUSWIDTH_32_BYTES  = 32,
    DMA_SLAVE_BUSWIDTH_64_BYTES  = 64,
};

enum dma_transfer_direction {
    DMA_MEM_TO_MEM,
    DMA_MEM_TO_DEV,
    DMA_DEV_TO_MEM,
    DMA_DEV_TO_DEV,
    DMA_TRANS_NONE,
};

/**
 * chan_id          [对应的dma 通道, -1 为自动申请]
 * src_addr         [dma 传输的源地址]
 * dst_addr         [dma 传输的目标地址]
 * len              [dma 传输的长度]
 * llp_buf          [dma 传输是需要存储链表的内存地址]
 * llp_size         [dma 传输是需要存储链表的内存地址长度]
 * timeout          [dma 传输的最到超时等待时间, -1为一直等待]
 * flags            [dma 传输使用的一些标志位]
 * direction        [dma 传输的方向]
 * src_addr_width   [dma 传输的源地址位宽]
 * dst_addr_width   [dma 传输的目标地址位宽]
 * src_maxburst     [dma 传输时源地址一个burst长度, 一般为fifo地址]
 * dst_maxburst     [dma 传输时目标地址一个burst长度, 一般为fifo地址]
 * device_fc        [dma 流控, 目前没有使用]
 */
struct dma_slave_config {
    int chan_id;
    dma_addr_t src_addr;
    dma_addr_t dst_addr;
    size_t len;
    void *llp_buf;
    u32 llp_size;
    int timeout;
    int flags;
    enum dma_transfer_direction direction;
    enum dma_slave_buswidth src_addr_width;
    enum dma_slave_buswidth dst_addr_width;
    u32 src_maxburst;
    u32 dst_maxburst;
    u32 device_fc;
};

struct dma_controller {
    char name[16];
    int ctlr_id;
    int num_chan;
    int (*xfer)(void *drv_priv, struct dma_slave_config *, int num_cfg);
    int (*malloc_size)(size_t len, enum dma_slave_buswidth src_width);
    void *priv;
};

extern int dma_init(void);
extern int dma_ctlr_register(struct dma_controller *ctlr);
extern int dma_ctlr_unregister(struct dma_controller *ctlr);
extern struct dma_controller *get_dma_ctlr(int id, const char *name);

/**
 * [get_llp_size 获取DmaMemcpy所需要malloc长度]
 * @param  len            [memcpy的长度]
 * @param  src_addr_width [DmaMemcpy 源地址使用的位宽, memcpy一般使用的是4bytes]
 * @return                [返回需要malloc的长度]
 */
static inline int get_llp_size(size_t len, enum dma_slave_buswidth src_addr_width)
{
    struct dma_controller *ctlr = get_dma_ctlr(-1, "CK_DW_DMAC");
    if(!ctlr || !ctlr->malloc_size)
        return 0;
    return ctlr->malloc_size(len, src_addr_width);
}

/**
 * [Dma Memcpy]
 * @param  dst      [Dma Memcpy 的目标地址]
 * @param  src      [Dma Memcpy 的源地址]
 * @param  len      [Dma Memcpy 的长度]
 * @param  llp_buf  [Dma Memcpy 时所需要的malloc内存]
 * @param  llp_size [Dma Memcpy 时所需要的malloc内存长度]
 * @return          [成功 返回memcopy的长度, 失败 -1]
 */
extern int dma_memcpy(void *dst, void *src, size_t len, void *llp_buf, int llp_size);

#endif
