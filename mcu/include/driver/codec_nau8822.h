/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * codec_nau8822.h: CODEC Driver for NAU8822
 *
 */

#ifndef __CODEC_NAU8822_H__
#define __CODEC_NAU8822_H__

int CodecNau8822Init(unsigned int i2c_bus);
int CodecNau8822Done(void);

#endif
