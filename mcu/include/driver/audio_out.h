/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * audio_out.h: MCU AudioOut Driver HAL
 *
 */

#ifndef __AUDIO_OUT_H__
#define __AUDIO_OUT_H__

#include <autoconf.h>

//=================================================================================================
// For Life cycle

typedef enum {
    AUDIO_OUT_SAMPLE_RATE_48000 = 0x00,
    AUDIO_OUT_SAMPLE_RATE_44100 = 0x01,
    AUDIO_OUT_SAMPLE_RATE_32000 = 0x02,
    AUDIO_OUT_SAMPLE_RATE_24000 = 0x03,
    AUDIO_OUT_SAMPLE_RATE_22050 = 0x04,
    AUDIO_OUT_SAMPLE_RATE_16000 = 0x05,
    AUDIO_OUT_SAMPLE_RATE_11025 = 0x06,
    AUDIO_OUT_SAMPLE_RATE_8000  = 0x07,
} AUDIO_OUT_SAMPLE_RATE;

typedef enum {
    AUDIO_OUT_CHANNEL_MONO      = 0x00,
    AUDIO_OUT_CHANNEL_STEREO    = 0x01,
} AUDIO_OUT_CHANNEL_TYPE;

typedef enum {
    AUDIO_OUT_CLOCK_SYNC  = 0x00,
    AUDIO_OUT_CLOCK_ASYNC = 0X01,
} AUDIO_OUT_CLOCK_TYPE;

typedef struct {
    AUDIO_OUT_SAMPLE_RATE sample_rate;
    AUDIO_OUT_CHANNEL_TYPE channel_type;
    AUDIO_OUT_CLOCK_TYPE clock_type;
} AUDIO_OUT_PCM1IN_CONFIG;

typedef int (*AUDIO_OUT_PLAY_CB)(unsigned int sdc_addr, void *priv);

typedef struct {
    AUDIO_OUT_SAMPLE_RATE sample_rate;
    AUDIO_OUT_CHANNEL_TYPE channel_type;
    const short *buffer_addr;           // buffer should be align 8
    unsigned int buffer_size;           // size should be times of 1024

    AUDIO_OUT_PLAY_CB play_callback;
} AUDIO_OUT_BUFFER_CONFIG;

int AudioOutInit(const AUDIO_OUT_PCM1IN_CONFIG *pcm1in_config,      // route 0
                 const AUDIO_OUT_BUFFER_CONFIG *buffer_config);     // route 1
int AudioOutDone(void);

//-------------------------------------------------------------------------------------------------
// For VSP Calls

typedef enum {
    AUDIO_OUT_ROUTE_PCM1IN = 0x0,     // Route 0
    AUDIO_OUT_ROUTE_BUFFER,
} AUDIO_OUT_ROUTE;

int AudioOutPlay(const AUDIO_OUT_ROUTE route, const short *frame, const unsigned int size);
int AudioOutStop(const AUDIO_OUT_ROUTE route);
int AudioOutRefresh(const AUDIO_OUT_ROUTE route);

int AudioOutSetVolume(const AUDIO_OUT_ROUTE route, const int volume);
int AudioOutSetMute(const AUDIO_OUT_ROUTE route, const unsigned int mute);

typedef enum {
    AUDIO_OUT_STEREO_TRACK,
    AUDIO_OUT_LEFT_TRACK,
    AUDIO_OUT_RIGHT_TRACK
} AUDIO_OUT_TRACK;

int AudioOutSetTrack(const AUDIO_OUT_TRACK track);

typedef enum {
    AUDIO_OUT_STORE_UNINTERLACED = 0,
    AUDIO_OUT_STORE_INTERLACED,
} AUDIO_OUT_STORE_MODE;

int AudioOutSetStoreMode(const AUDIO_OUT_STORE_MODE mode);

//-------------------------------------------------------------------------------------------------
// For UAC sync (Route Buffer Only)

int AudioOutGetRemainBufferSize(void);
unsigned int AudioOutGetSdcAddr(void);

//=================================================================================================
// For Boards

int AudioOutSetupDAC(void);
int AudioOutAutoMuteDAC(int enable);
int AudioOutBoardInit(void);
int AudioOutExternCodecInit(void);

//=================================================================================================

typedef union {
    unsigned int value;
    struct {
        unsigned generic:1;
        unsigned i2s:1;
        unsigned spdif:1;
        unsigned pcm_w:1;
        unsigned r1:1;
        unsigned r2:1;
        unsigned r3:1;
        unsigned detail:1;
    };
} AUDIO_OUT_DUMP_FLAGS;

#ifdef CONFIG_MCU_ENABLE_DEBUG
void AudioOutDumpRegs(AUDIO_OUT_DUMP_FLAGS flags);
#else
#define AudioOutDumpRegs(x) while(0)
#endif

#endif /* __AUDIO_OUT_H__ */

