/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_is31fl3236a.h: LED Driver for IS31FL3236A
 *
 */

#ifndef __LED_IS31FL3236A_H__
#define __LED_IS31FL3236A_H__

#include "led.h"

int LedIS31FL3236ASetColors(int start_index_vir, int num, LED_COLOR *colors);
int LedIS31FL3236ASetLights(int start_index_vir, int num, LED_LIGHT *lights);
int LedIS31FL3236ASetSwitchs(int index_vir, int num, LED_SWITCH *switchs);

int LedIS31FL3236ASetColor(int index_v, LED_COLOR color);
int LedIS31FL3236ASetLight(int index_vir, LED_LIGHT light);
int LedIS31FL3236ASetSwitch(int index_v, LED_SWITCH _switch);

int LedIS31FL3236AEnable(void);
int LedIS31FL3236ADisable(void);

int LedIS31FL3236AReset(void);
int LedIS31FL3236ASetFrequency(LED_FREQUENCE val);
int LedIS31FL3236AInit(int i2c_bus, int chip);
int LedIS31FL3236ASelectChip(int i2c_bus, int chip);

#endif  // __LED_IS31FL3236A_H__

