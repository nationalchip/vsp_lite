/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * snpu.h: Device Driver for SNPU
 *
 */

#ifndef __SNPU_H__
#define __SNPU_H__

int SnpuInit(void);
int SnpuLoadFirmware(void);
int SnpuDone(void);

typedef short SNPU_FLOAT;
int SnpuFloat32To16(unsigned int *in_data, SNPU_FLOAT *out_data, int data_num);
int SnpuFloat16To32(SNPU_FLOAT *in_data, unsigned int *out_data, int data_num);

typedef enum {
    SNPU_IDLE,
    SNPU_BUSY,
    SNPU_STALL,
} SNPU_STATE;

typedef int (*SNPU_CALLBACK)(int module_id, SNPU_STATE state, void *private_data);

typedef struct {
    int module_id;          // module id defined by programmer
    void *ops;              // ops_content in model.c
    void *data;             // cpu_content in model.c
    void *input;            // input in model.c
    void *output;           // output in model.c
    void *cmd;              // npu_content in model.c
    void *tmp_mem;          // tmp_content in model.c
} SNPU_TASK;

int SnpuRunTask(SNPU_TASK *task, SNPU_CALLBACK callback, void *private_data);

SNPU_STATE SnpuGetState(void);

#endif // __SNPU_H__
