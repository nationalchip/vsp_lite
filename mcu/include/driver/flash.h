/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * flash.h: internal spi nor/nand flash driver
 *
 */

#ifndef __FLASH_H__
#define __FLASH_H__

#include "autoconf.h"
#include <driver/spi_device.h>
/* 3 types of flash */
enum {
	NO_FLASH = 0,
	NOR_FLASH,
	SPI_FLASH,
	NAND_FLASH
};

enum spi_flash_info {
	SPI_FLASH_SIZE_CHIP,
	SPI_FLASH_SIZE_BLOCK,
	SPI_FLASH_SIZE_PAGE,
	SPI_FLASH_SIZE_SECTOR,
	SPI_FLASH_NUM_BLOCK,
	SPI_FLASH_NUM_PAGE,
	SPI_FLASH_NUM_SECTOR,
};

#define FLASH_TYPE_NUM (3)

/*
 * flash ops function
 */
struct flash_dev {
	struct flash_dev*  (*init) (struct spi_device *spi_device);
	int  (*readdata)(unsigned int addr, unsigned char *data, unsigned int len);
	int  (*chiperase) (void);
	int  (*erasedata)(unsigned int addr, unsigned int len);
	int  (*pageprogram)(unsigned int addr, unsigned char *data, unsigned int len);
	void (*sync)(void);
	void (*test)(int argc, char *argv[]);
	void (*calcblockrange)(unsigned int addr, unsigned int len, unsigned int *pstart, unsigned int *pend);
	int  (*badinfo)(void);
	int  (*pageprogram_yaffs2)(unsigned int addr, unsigned char *data, unsigned int len);
	int  (*readoob)(unsigned int addr, unsigned char *data, unsigned int len);
	int  (*writeoob)(unsigned int addr, unsigned char *data, unsigned int len);
	char *(*gettype)(void);
	int (*getsize)(enum spi_flash_info flash_info);
	int (*write_protect_mode)(void);
	int (*write_protect_status)(void);
	int (*write_protect_lock)(unsigned int addr);
	int (*write_protect_unlock)(void);
#ifdef CONFIG_MTD_TESTS
	int  (*readdata_noskip)(unsigned int addr, unsigned char *data, unsigned int len);
	void (*erasedata_noskip)(unsigned int addr, unsigned int len);
	int  (*pageprogram_noskip)(unsigned int addr, unsigned char *data, unsigned int len);
#endif
	int (*otp_lock)(void);
	int (*otp_status)(unsigned char *data);
	int (*otp_erase)(void);
	int (*otp_write)(unsigned int addr, unsigned char *data, unsigned int len);
	int (*otp_read)(unsigned int addr, unsigned char* data, unsigned int len);
	int (*otp_get_region)(unsigned int *region);
	int (*otp_set_region)(unsigned int region);
	int (*block_isbad)(unsigned int addr);
	int (*block_markbad)(unsigned int addr);
	int (*calc_phy_offset)(unsigned int start, unsigned int logic_offset, unsigned int *phy_offset);
};

/*
 * flash_ops interface function
 */
struct flash_dev *spi_flash_probe(unsigned int sample, unsigned int cs,
		unsigned int max_hz, unsigned int spi_mode);
int spi_flash_readdata(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
int spi_flash_chiperase (struct flash_dev *devp);
int spi_flash_erasedata(struct flash_dev *devp, unsigned int addr, unsigned int len);
int spi_flash_pageprogram(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
void spi_flash_sync(struct flash_dev *devp);
#ifdef CONFIG_MTD_TESTS
int spi_flash_readdata_noskip(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
void spi_flash_erasedata_noskip(struct flash_dev *devp, unsigned int addr, unsigned int len);
int spi_flash_pageprogram_noskip(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
#endif
void spi_flash_test(struct flash_dev *devp, int argc, char *argv[]);
void spi_flash_calcblockrange(struct flash_dev *devp, unsigned int addr, unsigned int len, unsigned int *pstart, unsigned int *pend);
int spi_flash_badinfo(struct flash_dev *devp);
int spi_flash_pageprogram_yaffs2(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
int spi_flash_readoob(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
int spi_flash_writeoob(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
char * spi_flash_gettype(struct flash_dev *devp);
int spi_flash_getsize(struct flash_dev *devp, enum spi_flash_info flash_info);
int spi_flash_write_protect_mode(struct flash_dev *devp);
int spi_flash_write_protect_status(struct flash_dev *devp);
int spi_flash_write_protect_lock(struct flash_dev *devp, unsigned long addr);
int spi_flash_write_protect_unlock(struct flash_dev *devp);
int spi_flash_block_isbad(struct flash_dev *devp, unsigned int addr);
int spi_flash_block_markbad(struct flash_dev *devp, unsigned int addr);
int spi_flash_otp_lock(struct flash_dev *devp);
int spi_flash_otp_status(struct flash_dev *devp, unsigned char *data);
int spi_flash_otp_erase(struct flash_dev *devp);
int spi_flash_otp_write(struct flash_dev *devp, unsigned int addr, unsigned char *data, unsigned int len);
int spi_flash_otp_read(struct flash_dev *devp, unsigned int addr, unsigned char* data, unsigned int len);
int spi_flash_otp_get_region(struct flash_dev *devp, unsigned int *region);
int spi_flash_otp_set_region(struct flash_dev *devp, unsigned int region);
int spi_flash_calc_phy_offset(struct flash_dev *devp, unsigned int phy_start, unsigned int logic_offset, unsigned int *phy_offset);
int spi_flash_logic_read(struct flash_dev *devp, unsigned int phy_start, unsigned int logic_offs, void *buf, unsigned int len);
int spi_flash_logic_program(struct flash_dev *devp, unsigned int phy_start, unsigned int logic_offs, void *buf, unsigned int len);
int spi_flash_area_erase(struct flash_dev *devp, unsigned int phy_start, unsigned int phy_end);
#endif
