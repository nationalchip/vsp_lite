/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * irq: MCU's IRQ Table
 *
 */

#ifndef __MCU_IRQ_H__
#define __MCU_IRQ_H__

void gx_disable_all_interrupt(void);

void gx_mask_irq(unsigned int irq);
void gx_unmask_irq(unsigned int irq);
int  gx_irq_status(unsigned int irq);
void gx_irq_wait(unsigned int irq);

typedef int (*irq_handler_t) (int irq, void *pdata);

void gx_irq_init(void);
void gx_mask_fiq(unsigned int fiq);
void gx_unmask_irq(unsigned int irq);

void gx_enable_irq(void);
void gx_disable_irq(void);
int  gx_irqenabled(void);

void gx_request_irq(int irq, irq_handler_t handler, void *pdata);
void gx_free_irq(int irq);

//=================================================================================================

// Fast Interrupt
void gx_fiq_init(void);
void gx_mask_fiq(unsigned int fiq);
void gx_unmask_fiq(unsigned int fiq);
int  gx_fiq_status(unsigned int fiq);
void gx_fiq_wait(unsigned int fiq);

// Fast Interrupt handler
typedef void (*fiq_handler_t) (int fiq, void *pdata);

void gx_enable_fiq(void);
void gx_clf(void);
void gx_request_fiq(int fiq, fiq_handler_t handler, void *pdata);
void gx_free_fiq(int fiq);
void hide_logo(void);

//=================================================================================================

// Wake Interrupt Handler
typedef int (*wiq_handler_t) (int status, void *pdata);
void gx_request_wake(unsigned int wake_source, wiq_handler_t handler, void *pdata);

#endif  /* __MCU_IRQ_H__ */
