/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * pmu.h: PMU Driver for HC18P11xS_L
 *
 */
#ifndef __PMU_H__
#define __PMU_H__

struct BQ25890Config {
    u32 ichg;   /* charge current       */
    u32 vreg;   /* regulation voltage       */
    u32 iterm;  /* termination current      */
    u32 iprechg;    /* precharge current        */
    u32 sysvmin;    /* minimum system voltage limit */
    u32 boostv; /* boost regulation voltage */
    u32 boosti; /* boost current limit      */
    u32 boostf; /* boost frequency      */
    u32 ilim_en;    /* enable ILIM pin      */
    u32 treg;   /* thermal regulation threshold */
    u32 enable;
};

struct BQ25890Info{
	unsigned int chrg_status;
	unsigned int online;
	unsigned int enable;
	unsigned int bat_fault;
	unsigned int temp;
	unsigned int Vbus;
	unsigned int chrg_I;
	unsigned int chrg_Imax;
	unsigned int chrg_V;
	unsigned int chrg_Vmax;
	unsigned int chrg_Iterm;
};

struct CW2015Info{
	unsigned int voltage;
	unsigned int capacity;
	unsigned int time;
};

int PmuPowerOff(void);
int PmuInit(int i2c_bus);
int PmuReadVoltage(unsigned short *voltage);


int BQ25890Init(int i2c_bus, struct BQ25890Config *config);
int BQ25890GetProperty(struct BQ25890Info *info);

int CW2015Init(int i2c_bus);
int CW2015GetProperty(struct CW2015Info *info);

void RT5037Init(int bus_id);
void RT5037BuckSet(unsigned char buck_num, unsigned int voltage);
void RT5037LdoSet(unsigned char ldo_num, unsigned int voltage);

#endif
