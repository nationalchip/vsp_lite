/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * irr.h: Infra-Red Remote
 *
 */

#ifndef __IRR_H__
#define __IRR_H__

void IrrSendInit(unsigned int port, unsigned int carrier_frequency);
void IrrSendCode(unsigned char addr, unsigned char code);
void IrrSendRepeat(void);

#endif /* __IRR_H__ */
