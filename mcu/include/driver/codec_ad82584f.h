/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * codec_ad82584f.h: CODEC Driver for Ad82584f
 *
 */

#ifndef __CODEC_AD82584F_H__
#define __CODEC_AD82584F_H__

int CodecAd82584fInit(unsigned int i2c_bus,unsigned int gpio_dsp_reset);
int CodecAd82584fDone(void);

#endif
