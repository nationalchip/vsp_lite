/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * codec_npcp215x.h: CODEC Driver for NPCP215X
 *
 */

#ifndef __CODEC_NPCP215X_H__
#define __CODEC_NPCP215X_H__

int CodecNpcp215xInit(unsigned int i2c_bus, unsigned int gpio_dsp_rst, unsigned int gpio_amp_pdn);
int CodecNpcp215xDone(void);

#endif
