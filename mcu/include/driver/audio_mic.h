/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * audio_mic.h: MCU Audio MIC Driver HAL
 *
 */

#ifndef __AUDIO_MIC_H__
#define __AUDIO_MIC_H__

#include <autoconf.h>
#include <types.h>

//=================================================================================================
// Initialize and Done
typedef enum {
    AUDIO_IN_BIT_16,
    AUDIO_IN_BIT_24,
} AUDIO_IN_BIT_SIZE;

typedef enum {
    AUDIO_IN_ENDIAN_BIG,
    AUDIO_IN_ENDIAN_LITTLE,
} AUDIO_IN_ENDIAN;

typedef int (*AUDIO_IN_RECORDER_CB)(int frame_index, void *priv);
typedef int (*AUDIO_IN_AVAD_CB)(uint8_t state, void *priv);

typedef struct {
    short       *buffer_addr;
    unsigned int buffer_size;
    unsigned int frame_size;
    unsigned int channel_size;
    unsigned int mic_channel_num;
    unsigned int ref_channel_num;
    unsigned int sample_rate;
    AUDIO_IN_RECORDER_CB record_callback;
    AUDIO_IN_AVAD_CB avad_callback;

    short       *mic_buffer_addr;
    short       *ref_buffer_addr;
    AUDIO_IN_BIT_SIZE bit_size;
    AUDIO_IN_ENDIAN endian;

    void *priv;
} AUDIO_IN_CONFIG;

int AudioInBoardInit(void); /* Called by AudioIn Init */
int AudioInInit(const AUDIO_IN_CONFIG *config, int enable_avad);

int AudioInBoardDone(void);
void AudioInDone(void);

//-------------------------------------------------------------------------------------------------
// AUDIO_IN_IZCT_REG
/**
 * Set level of ZCR
 * @param[in]   level (0: bypass)
 * @return      0:success, other:failure
 */
int AudioInSetZcrLevel(uint32_t level);

/**
 * Get level of ZCR
 * @return  level value
 */
uint32_t AudioInGetZcrLevel(void);

//-------------------------------------------------------------------------------------------------
// Agc

typedef enum {
    AUDIO_IN_AGC_ENERGY_THRESHOLD_LOW,
    AUDIO_IN_AGC_ENERGY_THRESHOLD_HIGH,
} AUDIO_IN_AGC_ENERGY_THRESHOLD_TYPE;

typedef enum {
    AUDIO_IN_AGC_SOFT_STATE_DONE,
    AUDIO_IN_AGC_SOFT_STATE_UP,
} AUDIO_IN_AGC_SOFT_STATE;

typedef enum {
    AUDIO_IN_AGC_MODE_ENABLE   = 0x01,
    AUDIO_IN_AGC_MODE_DISABLE  = 0x02,
    AUDIO_IN_AGC_MODE_SOFTWARE = 0x04,
} AUDIO_IN_AGC_MODE;

typedef enum {
    AIN_AGC_CH_0   = 0,
    AIN_AGC_CH_1   = 1,
    AIN_AGC_CH_2   = 2,
    AIN_AGC_CH_3   = 3,
    AIN_AGC_CH_4   = 4,
    AIN_AGC_CH_5   = 5,
    AIN_AGC_CH_6   = 6,
    AIN_AGC_CH_7   = 7,
    AIN_AGC_ECHO_0 = 8,
    AIN_AGC_ECHO_1 = 9,
} AUDIO_IN_AGC_CH_SEL;

typedef enum {
    AUDIO_IN_AGC_INT_EN_DISABLE,
    AUDIO_IN_AGC_INT_EN_ENABLE,
} AUDIO_IN_AGC_INTEN;

typedef int (*AUDIO_IN_AGC_CB)(AUDIO_IN_AGC_SOFT_STATE state, unsigned int agc_eng);

typedef struct {
    unsigned int              eng_low;
    unsigned int              eng_high;
    unsigned int              sample_num;
    AUDIO_IN_AGC_INTEN        int_en_up;
    AUDIO_IN_AGC_INTEN        int_en_down;
    AUDIO_IN_AGC_CH_SEL       channel_sel;
    AUDIO_IN_AGC_MODE         mode;
    AUDIO_IN_AGC_CB           agc_callback;
} AUDIO_IN_AGC_CONFIG;

int AudioInAgcGetEnable(void);
int AudioInAgcConfig(const AUDIO_IN_AGC_CONFIG *config);

int AudioInSetAgcEnergyThreshold(AUDIO_IN_AGC_ENERGY_THRESHOLD_TYPE type, unsigned int value);
unsigned int AudioInGetAgcEnergyThreshold(AUDIO_IN_AGC_ENERGY_THRESHOLD_TYPE type);
int AudioInSetAgcSampleNum(unsigned int sample_num);
unsigned int AudioInGetAgcSampleNum(void);
unsigned int AudioInGetAgcEnergy(void);

//-------------------------------------------------------------------------------------------------
// AUDIO_IN_ITL_USING & AUDIO_IN_ITU_USING
typedef enum {
#ifdef CONFIG_GX8010NRE
    AUDIO_IN_READ_ITPARAM_0,
    AUDIO_IN_READ_ITPARAM_1,
    AUDIO_IN_READ_ITPARAM_2,
    AUDIO_IN_READ_ITPARAM_3,
    AUDIO_IN_READ_ITPARAM_4,
    AUDIO_IN_READ_ITPARAM_5,
    AUDIO_IN_READ_ITPARAM_6,
    AUDIO_IN_READ_ITPARAM_7,
    AUDIO_IN_READ_ITPARAM_ECHO_0,
    AUDIO_IN_READ_ITPARAM_ECHO_1,
#else
    AUDIO_IN_READ_ITPARAM_0_1,
    AUDIO_IN_READ_ITPARAM_2_3,
    AUDIO_IN_READ_ITPARAM_4_5,
    AUDIO_IN_READ_ITPARAM_6_7
#endif
} AUDIO_IN_READ_ITPARAM_SEL;

typedef enum {
    AUDIO_IN_READ_ITPARAM_ITL,
    AUDIO_IN_READ_ITPARAM_ITU
} AUDIO_IN_READ_ITPARAM_TYPE;

typedef enum {
    AUDIO_IN_POWER_MODE_STANDBY,    // turn off all
    AUDIO_IN_POWER_MODE_ACTIVE,     // turn on all
    AUDIO_IN_POWER_MODE_AVAD        // turn off all except AVAD
} AUDIO_IN_POWER_MODE;

int AudioInSetPowerMode(AUDIO_IN_POWER_MODE power_mode);
int AudioInSetAvadParamUsing(AUDIO_IN_READ_ITPARAM_TYPE type, uint32_t value);
int AudioInSetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_TYPE type, uint32_t value); //replace API <<int AudioInSetAvadParamUsing(...)>>
uint32_t AudioInGetAvadParamUsing(AUDIO_IN_READ_ITPARAM_SEL sel, AUDIO_IN_READ_ITPARAM_TYPE type);
uint32_t AudioInGetDigitalVadParamUsing(AUDIO_IN_READ_ITPARAM_SEL sel, AUDIO_IN_READ_ITPARAM_TYPE type); //replace API <<uint32_t AudioInGetAvadParamUsing(...)>>
uint8_t AudioInGetAvadState(void);

typedef enum {
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_560, //mV
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_430,
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_310,
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_200,
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_97,
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_47,
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_22,
    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_11,

    AUDIO_IN_AVAD_THRESHOLD_VOLTAGE_INVALID = 0xffffffff
} AUDIO_IN_AVAD_THRESHOLD_VOLTAGE;

typedef enum {
    AUDIO_IN_AVAD_PREAMP_0, //dB
    AUDIO_IN_AVAD_PREAMP_20,
    AUDIO_IN_AVAD_PREAMP_22,
    AUDIO_IN_AVAD_PREAMP_24,
    AUDIO_IN_AVAD_PREAMP_26,
    AUDIO_IN_AVAD_PREAMP_28,
    AUDIO_IN_AVAD_PREAMP_30,
    AUDIO_IN_AVAD_PREAMP_32,
    AUDIO_IN_AVAD_PREAMP_34,
    AUDIO_IN_AVAD_PREAMP_36,
    AUDIO_IN_AVAD_PREAMP_38,
    AUDIO_IN_AVAD_PREAMP_40,
    AUDIO_IN_AVAD_PREAMP_42,
    AUDIO_IN_AVAD_PREAMP_44,
    AUDIO_IN_AVAD_PREAMP_46,
    AUDIO_IN_AVAD_PREAMP_48,
    AUDIO_IN_AVAD_PREAMP_50,
    AUDIO_IN_AVAD_PREAMP_52,
    AUDIO_IN_AVAD_PREAMP_54,
    AUDIO_IN_AVAD_PREAMP_56,
    AUDIO_IN_AVAD_PREAMP_58,
    AUDIO_IN_AVAD_PREAMP_60,

    AUDIO_IN_AVAD_PREAMP_INVALID = 0xffffffff
} AUDIO_IN_AVAD_PREAMP;

int AudioInSetAnalogVadParamUsing(AUDIO_IN_AVAD_THRESHOLD_VOLTAGE voltage, AUDIO_IN_AVAD_PREAMP preamp);
int AudioInGetAnalogVadParamUsing(AUDIO_IN_AVAD_THRESHOLD_VOLTAGE *voltage, AUDIO_IN_AVAD_PREAMP *preamp);
uint8_t AudioInGetVadState(void);

//-------------------------------------------------------------------------------------------------
//Mic

int AudioInSetMicEnable(unsigned char mask);
unsigned char AudioInGetMicEnable(void);

/**
 * Set gain of PGA
 * @param[in]   mic_gain   gain value(in dB, min: 0db)
 * @return      0:success, other:failure
 */
int AudioInSetMicGain(unsigned char mic_gain);

/**
 * Get gain of PGA
 * @return  gain value(in dB)
 */
unsigned char AudioInGetMicGain(void);

/**
 * Set gain of Channel
 * @param[in]   channel serial num (0 ~ 1)
 * @return      0:success, other:failure
 */
int AudioInSetMicChannelGain(int channel, unsigned int gain);

/**
 * Get gain of Channel
 * @param[in]   channel serial num (0 ~ 1)
 * @return      gain value, -1:failure
 */
uint32_t AudioInGetMicChannelGain(int channel);

typedef enum {
    AUDIO_IN_MIC_PRE_DC_FILTER_ENABLE   = (1 << 0),
    AUDIO_IN_MIC_PRE_DC_FILTER_DISABLE  = (1 << 1),
    AUDIO_IN_MIC_POST_DC_FILTER_ENABLE  = (1 << 2),
    AUDIO_IN_MIC_POST_DC_FILTER_DISABLE = (1 << 3),
} AUDIO_IN_MIC_DC_FILTER_ENABLE;

int  AudioInSetMicDcEnable(AUDIO_IN_MIC_DC_FILTER_ENABLE enable);
AUDIO_IN_MIC_DC_FILTER_ENABLE AudioInGetMicDcEnable(void);

int AudioInGetMicOverflow(void);

//-------------------------------------------------------------------------------------------------
//Ref

typedef enum {
    AUDIO_IN_REF_SOURCE_AMIC,
    AUDIO_IN_REF_SOURCE_DMIC,
    AUDIO_IN_REF_SOURCE_INTERNAL_LOOP,
    AUDIO_IN_REF_SOURCE_I2S_MASTER,     // Device as Master, LEO as Slave, PCM1INPUT
    AUDIO_IN_REF_SOURCE_I2S_SLAVE       // Device as Slave, LEO as Master, PCM0INPUT
} AUDIO_IN_REF_SOURCE;

int AudioInSetRefSource(AUDIO_IN_REF_SOURCE source, int left_channel, int right_channel);

unsigned char AudioInGetRefEnable(void);
int AudioInSetRefEnable(unsigned char mask);
/**
 * Set gain of REF
 * @param[in]   ref_gain   gain value(in dB, min: 0db)
 * @return      0:success, other:failure
 */
int AudioInSetRefGain(unsigned char ref_gain);

/**
 * Get gain of REF
 * @return  gain value(in dB)
 */
unsigned char AudioInGetRefGain(void);

/**
 * Set gain of Ref Channel
 * @param[in]   channel serial num (0 ~ 1)
 *              gain value(0 ~ 4095)
 * @return      0:success, other:failure
 */
int AudioInSetRefChannelGain(int channel, unsigned int gain);

/**
 * Get gain of Ref Channel
 * @param[in]   channel serial num (0 ~ 1)
 * @return      gain value, -1:failure
 */
uint32_t AudioInGetRefChannelGain(int channel);

typedef enum {
    AUDIO_IN_REF_PRE_DC_FILTER_ENABLE   = (1 << 0),
    AUDIO_IN_REF_PRE_DC_FILTER_DISABLE  = (1 << 1),
    AUDIO_IN_REF_POST_DC_FILTER_ENABLE  = (1 << 2),
    AUDIO_IN_REF_POST_DC_FILTER_DISABLE = (1 << 3),
} AUDIO_IN_REF_DC_FILTER_ENABLE;
int AudioInSetRefDcEnable(AUDIO_IN_REF_DC_FILTER_ENABLE enable);
AUDIO_IN_REF_DC_FILTER_ENABLE AudioInGetRefDcEnable(void);

//=================================================================================================

#ifdef CONFIG_MCU_ENABLE_DEBUG
void AudioInDumpRegs(int detail);
#else
#define AudioInDumpRegs(x) while(0)
#endif

#endif  /* __AUDIO_MIC_H__ */
