/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * misc.h: Miscellaneous board HAL API definition
 *
 */

#ifndef __MISC_H__
#define __MISC_H__

//=================================================================================================
// Board Init
void BoardInit(void);
void BoardDone(void);

void BoardSuspend(void);
void BoardResume(void);

//=================================================================================================
// Cache
void CacheInit(void);
void CacheDone(void);

//=================================================================================================
// LED
typedef union {
    unsigned int value;
    struct {
        unsigned r:8;           /* red */
        unsigned g:8;           /* green */
        unsigned b:8;           /* blue */
        unsigned a:8;           /* alpha, or level */
    } bits;
} LED_PIXEL;

int LedInit(void);
int LedSetPixel(unsigned int index, LED_PIXEL pixel);
int LedFlush(void);
int LedDone(void);

//=================================================================================================
// TPM
int TPMGetAuthorizeKey(void *digest, void *cipher);

//=================================================================================================
// Uart Process Result
typedef enum {
    UPR_SEND_WAV = 100,
    UPR_SEND_WAV_END,
    UPR_SEND_OPUS_DATA
} UART_PROCESS_RESULT;

//=================================================================================================

#endif // __MISC_H__
