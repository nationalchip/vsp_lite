/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * uart.h: MCU UART device driver
 *
 */

#ifndef __MCU_UART__
#define __MCU_UART__

#define UART_PORT0 0
#define UART_PORT1 1

//=================================================================================================
// Initialize and Done
int UartInit(int port, unsigned int baudrate);
int UartDone(int port);

int UartSetBaudrate(int port, unsigned int baudrate);

//-------------------------------------------------------------------------------------------------
// FIFO Related

typedef enum {
    UART_FIFO_SEND_THRESHOLD_EMPTY,      // empty
    UART_FIFO_SEND_THRESHOLD_2,          // 2 byte
    UART_FIFO_SEND_THRESHOLD_QUARTER,    // 25%
    UART_FIFO_SEND_THRESHOLD_HALF,       // 50%
} UART_FIFO_SEND_THRESHOLD;

typedef enum {
    UART_FIFO_RECV_THRESHOLD_ONE_BYTE,   // 1 byte
    UART_FIFO_RECV_THRESHOLD_QUARTER,    // 25%
    UART_FIFO_RECV_THRESHOLD_HALF,       // 50%
    UART_FIFO_RECV_THRESHOLD_FULL,       // Almost Full
} UART_FIFO_RECV_THRESHOLD;

int UartSetSendFIFOThreshold(int port, UART_FIFO_SEND_THRESHOLD threshold);
int UartSetRecvFIFOThreshold(int port, UART_FIFO_RECV_THRESHOLD threshold);

void UartFlushSendFIFO(int port);
void UartFlushRecvFIFO(int port);

//-------------------------------------------------------------------------------------------------
// Sync API
// Caller is hold until transfer is finished.

// Send
void UartSendByte(int port, unsigned char byte);
int UartTrySendByte(int port, unsigned char byte);
int UartCanSendByte(int port);
void UartSyncSendBuffer(int port, unsigned char *buffer, unsigned int length);

// Recv
unsigned char UartRecvByte(int port);
int UartTryRecvByte(int port, unsigned char *byte);
int UartCanRecvByte(int port);
unsigned int UartSyncRecvBuffer(int port, unsigned char *buffer, unsigned int length);

//-------------------------------------------------------------------------------------------------
// Async API Mode 1 (Interrupt Mode)

typedef int (*UART_CAN_SEND_CALLBACK)(int port, int length, void *priv);
typedef int (*UART_CAN_RECV_CALLBACK)(int port, int length, void *priv);

int UartStartAsyncSend(int port, UART_CAN_SEND_CALLBACK callback, void *priv);
int UartStopAsyncSend(int port);

int UartStartAsyncRecv(int port, UART_CAN_RECV_CALLBACK callback, void *priv);
int UartStopAsyncRecv(int port);

// No check FIFO status in these function
int UartSendBuffer(int port, unsigned char *buffer, int length);
int UartRecvBuffer(int port, unsigned char *buffer, int length);

//-------------------------------------------------------------------------------------------------
// Async API Mode 2 (DMA Mode)
//   In Leo, it is implemented by interrupt mode

typedef int (*UART_SEND_DONE_CALLBACK)(int port, void *priv);
typedef int (*UART_RECV_DONE_CALLBACK)(int port, void *priv);

int UartAsyncSendBuffer(int port, unsigned char *buffer, int length, UART_SEND_DONE_CALLBACK callback, void *priv);
int UartAsyncRecvBuffer(int port, unsigned char *buffer, int length, UART_RECV_DONE_CALLBACK callback, void *priv);

#endif /* __MCU_UART__ */
