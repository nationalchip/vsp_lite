/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_is31fl3208a.h: LED Driver for IS31FL3208A
 *
 */

#ifndef __LED_IS31FL3208A_H__
#define __LED_IS31FL3208A_H__

#include "led.h"

typedef enum {
    CURRENT_LEAVEL_0 = 0x00,//OUT=0
    CURRENT_LEAVEL_1 = 0x13,//OUT 7/12
    CURRENT_LEAVEL_2 = 0x12,//OUT 9/12
    CURRENT_LEAVEL_3 = 0x11,//OUT 11/12
    CURRENT_LEAVEL_4 = 0x10,//OUT MAX
} LED_CURRENT_LEAVEl;

typedef enum {
    LED_FREQUENCE_23K = 0,
    LED_FREQUENCE_3_POINT_45K,
} PWM_FREQUENCE;



int LedIS31FL3208ASetColors(int start_index_vir, int num, LED_COLOR *colors);
int LedIS31FL3208ASetLights(int start_index_vir, int num, LED_LIGHT *lights);
int LedIS31FL3208ASetSwitchs(int index_vir, int num, LED_SWITCH *val);


int LedIS31FL3208ASetColor(int index_v, LED_COLOR color);
int LedIS31FL3208ASetLight(int index_vir, LED_LIGHT light);
int LedIS31FL3208ASetSwitch(int index_v, LED_SWITCH _switch);

int LedIS31FL3208AEnable(void);
int LedIS31FL3208ADisable(void);

int LedIS31FL3208AReset(void);
int LedIS31FL3208ASetFrequency(LED_FREQUENCE val);
int LedIS31FL3208AInit(int i2c_bus, int chip);
int LedIS31FL3208ASelectChip(int i2c_bus, int chip);

int LedIS31FL3208ASetCurrent(int current);

#endif  // __LED_IS31FL3208A_H__

