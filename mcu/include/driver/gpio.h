/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * gpio.h: MCU GPIO driver
 *
 */

#ifndef __GPIO_H__
#define __GPIO_H__

//=================================================================================================

typedef enum {
    GPIO_DIRECTION_INPUT = 0,
    GPIO_DIRECTION_OUTPUT
} GPIO_DIRECTION;

GPIO_DIRECTION GpioGetDirection(unsigned int port);
int GpioSetDirection(unsigned int port, GPIO_DIRECTION direction);

//-------------------------------------------------------------------------------------------------

typedef enum {
    GPIO_LEVEL_LOW = 0,
    GPIO_LEVEL_HIGH
} GPIO_LEVEL;

GPIO_LEVEL GpioGetLevel(unsigned int port);
int GpioSetLevel(unsigned int port, GPIO_LEVEL level);

//-------------------------------------------------------------------------------------------------
// GPIO trigger function
typedef enum {
    GPIO_TRIGGER_EDGE_FALLING   = 0x01,
    GPIO_TRIGGER_EDGE_RISING    = 0x02,
    GPIO_TRIGGER_EDGE_BOTH      = 0x03,
    GPIO_TRIGGER_LEVEL_HIGH     = 0x04,
    GPIO_TRIGGER_LEVEL_LOW      = 0x08,
} GPIO_TRIGGER_EDGE;

typedef int (*GPIO_CALLBACK)(unsigned int port, void *pdata);

void GpioMaskTrigger(unsigned int port);
void GpioUnmaskTrigger(unsigned int port);
int GpioEnableTrigger(unsigned int port, GPIO_TRIGGER_EDGE edge, GPIO_CALLBACK callback, void *pdata);
int GpioDisableTrigger(unsigned int port);
void GpioSuspend(void);
void GpioResume(void);

//-------------------------------------------------------------------------------------------------
// GPIO PWM function
int GpioEnablePWM(unsigned int port, unsigned int freq, unsigned int duty);
int GpioDisablePWM(unsigned int port);

//=================================================================================================

#endif /* __GPIO_H__ */

