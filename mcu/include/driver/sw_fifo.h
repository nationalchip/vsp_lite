/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * sw_fifo.h: Soft Fifo Driver
 *
 */

#ifndef __SW_FIFO_H__
#define __SW_FIFO_H__

typedef struct {
    unsigned char *data;
    unsigned int   size;
    unsigned int   pread;
    unsigned int   pwrite;
} SW_FIFO;

int          SwFifoInit   (SW_FIFO *pfifo, void *buffer, unsigned int size);
unsigned int SwFifoUserGet(SW_FIFO *pfifo, void *buf, unsigned int len);
unsigned int SwFifoUserPut(SW_FIFO *pfifo, void *buf, unsigned int len);
unsigned int SwFifoLen    (SW_FIFO *pfifo);
unsigned int SwFifoGetRptr(SW_FIFO *pfifo);
unsigned int SwFifoGetWptr(SW_FIFO *pfifo);
void         SwFifoFree   (SW_FIFO *pfifo);
unsigned int SwFifoFreeLen(SW_FIFO *pfifo);
unsigned int SwFifoProduce(SW_FIFO *pfifo, unsigned int len);
unsigned int SwFifoConsume(SW_FIFO *pfifo, unsigned int len);

#endif  /* __SW_FIFO_H__ */
