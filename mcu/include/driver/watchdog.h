/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * watchdog.h: watchdog Driver HAL
 *
 */
#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

#include <driver/irq.h>

void WatchdogInit(uint16_t reset_timeout_ms,
        uint16_t level_timeout_ms, irq_handler_t irq_handler);
void WatchdogPing(void);
void WatchdogDone(void);

#endif  /* __WATCHDOG_H__ */
