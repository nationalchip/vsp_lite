/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led_is31fl3236a.h: LED Driver for IS31FL3236A
 *
 */

#ifndef __LED_AW9523B_H__
#define __LED_AW9523B_H__

#include "led.h"

void LedAW9523BInit(void);
int LedAW9523BSetColor(int index, LED_COLOR rgb);
int LedAW9523BReset(void);
int LedAW9523BReadID(unsigned char *read_ID);
int LedAW9523BSelectChip(int i2c_bus, int chip);

#endif
