/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * usb_gadget.h: usb gadget
 *
 */

#ifndef __USB_GADGET_H__
#define __USB_GADGET_H__

#include <vsp_ioctl.h>

//=================================================================================================

typedef enum {
    UAC2_CHANNEL_MONO   = 0x01,
    UAC2_CHANNEL_STEREO = 0x03,
    UAC2_CHANNEL_THREE  = 0x07,
    UAC2_CHANNEL_FOUR   = 0x0F,
    UAC2_CHANNEL_FIVE   = 0x1F,
    UAC2_CHANNEL_SIX    = 0x3F,
    UAC2_CHANNEL_SEVEN  = 0x7F,
    UAC2_CHANNEL_EIGHT  = 0xFF,
} UAC2_CHANNEL_NUM;

#define UAC2_CONNECT_STATE_BIT    0
#define UAC2_PLAYBACK_STATE_BIT   1
#define UAC2_RECORD_STATE_BIT     2
#define UAC2_HID_STATE_BIT        3

typedef struct {
    UAC2_CHANNEL_NUM channel_mask;
    int sample_rate;        // 48000, 16000
} UAC2_CHANNEL_CONFIG;

typedef struct  {
    const char *sound_card_name_in_windows;  //
    const char *manufacturer_name;  //
    const char *product_name;       //
    const char *serial_number;      //
} UAC2_DEVICE_DESCRIPTION;

enum UAC2_CONTROL_DIRECTION {
    UAC2_CONTROL_DOWNSTREAM,
    UAC2_CONTROL_UPSTREAM,
};

enum notify_status_type {
    NOTIFY_STATUS_DEVICE,
    NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM,
    NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM,
    NOTIFY_STATUS_FUNCTION_HID,
};

typedef int (*UAC2_NOTIFY_CALLBACK)(enum notify_status_type type, unsigned int state);
typedef unsigned int (*UAC2_AOUTGETSDCADDR_CALLBACK)(void);

typedef int (*UAC2_VOLUME_CALLBACK)(enum UAC2_CONTROL_DIRECTION dir, unsigned int volume);
typedef int (*UAC2_MUTE_CALLBACK)(enum UAC2_CONTROL_DIRECTION dir, int mute);
typedef int (*UAC2_AUTO_GAIN_CONTROL_CALLBACK)(enum UAC2_CONTROL_DIRECTION dir, int open);
typedef int (*UAC2_BASS_BOOST_CALLBACK)(enum UAC2_CONTROL_DIRECTION dir, int open);

typedef struct {
    unsigned int aout_buff_handle;
    unsigned int ain_buff_handle;

    UAC2_VOLUME_CALLBACK            volume_callback;
    UAC2_MUTE_CALLBACK              mute_callback;
    UAC2_AUTO_GAIN_CONTROL_CALLBACK auto_gain_control_callback;
    UAC2_BASS_BOOST_CALLBACK        bass_boost_callback;

    UAC2_NOTIFY_CALLBACK notify_callback;
    UAC2_AOUTGETSDCADDR_CALLBACK audio_out_get_sdc_addr_callback;
} UAC2_CALLBACKS;

int UsbCompositeInit(const UAC2_CHANNEL_CONFIG *audio_in_config,
        const UAC2_CHANNEL_CONFIG *audio_out_config,
        const UAC2_DEVICE_DESCRIPTION *device_description,
        const UAC2_CALLBACKS *callbacks,
        void *private_data);
int UsbCompositeDone(void);

// ------------------------------ gadget uac --------------------------------------

// ds : downstream(playback) us : upstream(capture)
struct uac_control_info {
    short volume_ds_cur;
    short volume_ds_min;
    short volume_ds_max;

    short volume_us_cur;
    short volume_us_min;
    short volume_us_max;

    // 1 : mute , 0 : not mute
    char mute_ds;
    char mute_us;
};

int UacGetControlInfo(struct uac_control_info *info);
unsigned int Uac2GetState(void);
unsigned int/*UAC2_STATE*/ Uac2HidGetState(void);

// ------------------------------ gadget hid --------------------------------------

int HidOpen(void);
int HidClose(int handle);
int HidWrite(int handle, unsigned char *buffer, unsigned int count);
int HidRead(int handle, unsigned char *buffer, unsigned int count);

// ------------------------------ gadget Serial --------------------------------------
typedef struct {
       unsigned int total_len;
       unsigned int used_len;
       unsigned int free_len;
} GS_FIFO_INFO;

typedef struct  {
       GS_FIFO_INFO write_fifo;
       GS_FIFO_INFO read_fifo;
} GS_INFO;

int  GsOpen(void);
void GsClose(void);
int  GsWrite(const char *buf, int count);
int  GsRead(char *buf, int count);
int  GsInfo(GS_INFO *info); // get read/write fifo info

// ------------------------------ Mass Storage --------------------------------------

/*
 * 作用：用于初始化 mass storage 所操作的 flash 空间
 *
 * partition_start             : 分区起始地址
 * partition_size              : 分区总长度
 * reserved_size_for_badblocks : 允许坏块占用的空间，nor flash 配置为 0, nand 根据需要分区大小进行配置
 *
 * 实际可用空间为 partition_size - reserved_size_for_badblocks
 *
 * return : 0 for success, other for faild
 */
int MassstorageInit(int partition_start, int partition_size, int reserved_size_for_badblocks);

/*
 * 释放 MassstorageInit 时创建的资源
 */
int MassstorageDone(void);

/**
 * 作用：处理 mass storage 的命令的交互
 *
 * 用法：可用于不能阻塞的上下文，但是需要经常调用该接口以完成命令交互
 *
 * 该函数在没有收到主机发送的命令时会立即返回，在收到命令时会在命令处理完返回
 */
void MassstorageProcess(void);

// ------------------------------ Upgrade dedicated --------------------------------------

/*
 * 如下 Init 是提供了一个单独的 Configuration （一个 Configuration 是很多接口的集合），
 * 跟 UAC2 这个 configuration 独立
 */
int UsbslaveDownloaderInit(void);
int UsbslaveDownloaderDone(void);

/*
 * usb dma buff 的配置
 *
 * 要求：
 *   地址要求：至少 4 字节对齐
 *   长度要求：至少 512 字节，并且是 512 字节的整数倍
 */
int UsbslaveInit(void);
int UsbslaveDestroy(void);

// 用于模拟 rom 从 usb 接收 boot 文件的 stage1
int UsbslaveEnterDownloadMode(void);

// 无协议的 usb 读写，上层可基于如下 api 去封装协议
int UsbslaveRead (void *buff, unsigned int len);
int UsbslaveWrite(void *buff, unsigned int len);
int UsbslaveReadNoblock(void *buff, unsigned int len);

// ------------------------------ usb like serial --------------------------------------

int  UsbLSOpen(void);
void UsbLSClose(void);

//=================================================================================================

#endif  // __UDC_UAC2_H__
