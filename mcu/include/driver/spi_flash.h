/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * spi_flash.h: SPI Flash API
 *
 */

#ifndef __SPI_FLASH_H__
#define __SPI_FLASH_H__

int SpiFlashInitPure(void *dma_buffer, int dma_bufsize);
int SpiFlashInit(void *dma_buffer, int dma_bufsize);
int SpiFlashRead(unsigned int addr, unsigned char *data, unsigned int len);
int SpiFlashReadPure(unsigned int addr, unsigned char *data, unsigned int len);
int SpiFlashWrite(unsigned int addr, unsigned char *data, unsigned int len);
int SpiFlashProgram(unsigned int addr, unsigned char *data, unsigned int len);
int SpiFlashLock(void);
int SpiFlashUnlock(void);
int SpiFlashErase(unsigned int addr, unsigned int len);
int SpiFlashChiperase(void);
int SpiFlashDone(void);

#endif  /* __SPI_FLASH_H__ */
