/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led.h: LED device related definition
 *
 */

#ifndef __LED_H__
#define __LED_H__

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} LED_COLOR;

typedef enum {
    LED_SWITCH_OFF = 0,
    LED_SWITCH_ON,
} LED_SWITCH;

typedef enum {
    LED_FREQUENCE_3K = 0,
    LED_FREQUENCE_22K,
} LED_FREQUENCE;

typedef enum {
    LED_LIGHT_0 = 0x06,
    LED_LIGHT_1 = 0x04,
    LED_LIGHT_2 = 0x02,
    LED_LIGHT_3 = 0x00,
    LED_LIGHT_4 = 0x12,
} LED_LIGHT;

#endif /* __LED_H__ */

