/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led.h: LED Driver for SN3193
 *
 */

#ifndef __LED_SN3193_H__
#define __LED_SN3193_H__

#include "led.h"

int LedSN3193Init(int i2c_bus, int i2c_chip);
int LedSN3193Done(void);
int LedSN3193SetColor(LED_COLOR rbg);

#endif
