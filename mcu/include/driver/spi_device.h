/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * spi_device.h: spi device API
 *
 */

#ifndef __SPI_DEVICE_H_
#define __SPI_DEVICE_H_

#include <common.h>

#define SPI_DMA       0x800
#define SPI_PHASE     (1 << 6)   /* SPI时钟相位*/
#define SPI_SCPOL     (1 << 7)   /* SPI时钟极性*/

enum {
	CS_DISABLE = 0,
	CS_ENABLE
};

struct spi_priv {
	uint16_t ctrlr0;
	uint16_t baudr;
};

struct spi_device {
	uint32_t        sample_dly;
	uint32_t        freq;
	uint32_t        mode;
	int             (*set_cs)(int enable);
	struct spi_priv priv;
};

int spi_device_init(struct spi_device* spi_device);
int spi_xfer(const struct spi_device* spi_device, const uint8_t *tx, uint8_t *rx, uint32_t len);
int spi_write_then_read(const struct spi_device* spi_device,
		const uint8_t *txbuf, unsigned int n_tx,
		void *rxbuf, unsigned int n_rx);

#endif /* __SPI_DEVICE_H_ */
