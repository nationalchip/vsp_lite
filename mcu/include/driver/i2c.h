#ifndef __I2C__
#define __I2C__

#define I2CON_W_ACK      (1 << 1) //write ack
#define I2CON_R_ACK      (1 << 2) //read ack
#define I2CON_SI         (1 << 3) //operatin finish
#define I2CON_STO        (1 << 4) //stop
#define I2CON_STA        (1 << 5) //start
#define I2CON_I2EN       (1 << 6) //module enable
#define I2CON_I2IE       (1 << 7) //interrupt enable

#define EAGAIN           11    /* Try again later */

enum {
	I2CON = 0x0,
	I2DATA = 0x4,
	I2DIV = 0x8,
};

enum {
	I2C_READ = 0,
	I2C_WRITE
};

enum i2c_slave_callback_event {
	I2C_SLAVE_RECEIVE_DATA,
	I2C_SLAVE_REQUESTED_DATA,
	I2C_SLAVE_STOP
};

struct i2c_msg {
	unsigned short addr;
	unsigned short flags;

#define I2C_M_RD           0x0001
#define I2C_M_TEN          0x0010
#define I2C_M_RECV_LEN     0x0400
#define I2C_M_NO_RD_ACK    0x0800
#define I2C_M_IGNORE_NAK   0x1000
#define I2C_M_REV_DIR_ADDR 0x2000
#define I2C_M_NOSTART      0x4000

	unsigned short len;
	unsigned char        *buf;
};

struct i2c_slave_callback_param {
	unsigned char data;
	unsigned int  rx_index;
	unsigned int  tx_index;
	enum i2c_slave_callback_event event;
};

typedef int (*i2c_slave_cb_t)(struct i2c_slave_callback_param *);

/* i2c master*/
void *gx_i2c_open(unsigned int id);
int gx_i2c_set_speed(unsigned int id, unsigned int speed);
int gx_i2c_transfer(void *dev, struct i2c_msg *msgs, int num);
int gx_i2c_tx(int bus_id, unsigned int devid, unsigned int reg_address, unsigned char *tx_data, unsigned int count);
int gx_i2c_rx(int bus_id, unsigned int devid, unsigned int reg_address, unsigned char *rx_data, unsigned int count);
int gx_i2c_close(void *dev);

/* i2c slave*/
void *gx_i2c_slave_open(unsigned int id, unsigned int devid, i2c_slave_cb_t callback);
int gx_i2c_slave_close(void *dev);
#endif

