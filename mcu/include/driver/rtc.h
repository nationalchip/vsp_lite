/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * rtc.h: MCU's RTC Driver
 *
 */

#ifndef __RTC_H__
#define __RTC_H__

//=================================================================================================

typedef struct {
    // Date
    unsigned short  year;
    unsigned char   month;
    unsigned char   month_day;
    unsigned char   week_day;
    // Time
    unsigned char   hour;
    unsigned char   minute;
    unsigned char   second;
    unsigned int    microsecond;
} RTC_DATE_TIME;

typedef enum {
    RTC_ALARM_MASK_MICROSECOND  = 1 << 0,
    RTC_ALARM_MASK_MILLISECOND  = 1 << 1,
    RTC_ALARM_MASK_SECOND       = 1 << 2,
    RTC_ALARM_MASK_MINUTE       = 1 << 3,
    RTC_ALARM_MASK_HOUR         = 1 << 4,
    RTC_ALARM_MASK_WEEK_DAY     = 1 << 5,
    RTC_ALARM_MASK_MONTH_DAY    = 1 << 6,
    RTC_ALARM_MASK_MONTH        = 1 << 7,
    RTC_ALARM_MASK_YEAR         = 1 << 8,
} RTC_ALARM_MASK;

typedef enum {
    RTC_ITEM_TICK1,
    RTC_ITEM_TICK2,
    RTC_ITEM_ALARM1,
    RTC_ITEM_ALARM2,
} RTC_ITEM;

typedef int (*RTC_CALLBACK)(RTC_ITEM item, void *priv);
typedef int (*RTC_ISR)(int irq, void *priv);

//=================================================================================================

void RtcInit(void);
void RtcDone(void);

void RtcReset(void);

int RtcSetCallback(RTC_ITEM item, RTC_CALLBACK callback, void *priv);
int RtcSetISR(RTC_ISR isr, void *priv);

//-------------------------------------------------------------------------------------------------

void RtcStartTime(const RTC_DATE_TIME *date_time);
void RtcStopTime(void);
void RtcGetTime(RTC_DATE_TIME *date_time);

//-------------------------------------------------------------------------------------------------

void RtcStartTick(RTC_ITEM tick, const unsigned int duration_us);
void RtcStopTick(RTC_ITEM tick);

//-------------------------------------------------------------------------------------------------

void RtcStartAlarm(RTC_ITEM alarm, const RTC_DATE_TIME *date_time, RTC_ALARM_MASK mask);
void RtcStopAlarm(RTC_ITEM alarm);

void RtcGetAlarmTime(RTC_ITEM alarm, RTC_DATE_TIME *date_time, RTC_ALARM_MASK *mask);

//=================================================================================================

#endif  /* __RTC_H__ */
