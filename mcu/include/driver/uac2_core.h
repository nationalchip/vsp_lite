/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * uac_core.h: vsp uac core
 *
 */

#ifndef __UAC2_CORE_H__
#define __UAC2_CORE_H__

// uac buff manage
unsigned int get_r_ptr(void);
unsigned int get_w_ptr(void);
unsigned int set_w_ptr(unsigned int addr);
unsigned int get_wr_distance(void);
unsigned int get_next_buff(void);

// uac core

enum uac_clock_sync_policy {
    UAC_CLOCK_SYNC_THROUGH_NONE,
    UAC_CLOCK_SYNC_THROUGH_PLAYBACK,
    UAC_CLOCK_SYNC_THROUGH_CAPTURE,
    UAC_CLOCK_SYNC_THROUGH_SOF,
    UAC_CLOCK_SYNC_THROUGH_PLAYBACK_WITH_AUDIOOUT_RUNNING, // uac 占用 audio play 的单个通道，且依赖 audio play 一直运行才能做时钟同步
};

#define PLAYBACK_DEFAULT_DROP_PKT_TIME_MS 100

/*
 * uac_core_config: uac core 配置参数
 *
 * @policy: 调频策略
 *
 * @playback_enable: 下行支持(usb host -> usb slave)
 * @aout_buff: 存储下行数据用的 buff
 * @aout_buff_len: buff 大小
 * @playback_sample_rate: 下行采样率
 * @playback_channel_num: 下行通道数
 * @playback_bitwidth: 位宽
 * @playback_pkt_drop_time_ms: 为了保证下行流的稳定，刚开始一段时间下行的数据会被丢掉，通过该值配置被丢掉数据的时间
 *
 * @capture_enable: 上行支持(usb slave -> usb host)
 * @capture_sample_rate: 上行采样率
 * @capture_channel_num: 上行通道数
 * @capture_bitwidth: 位宽
 */
typedef struct uac_core_config {
    enum uac_clock_sync_policy policy;

    int           playback_enable;
    void         *aout_buff;
    unsigned int  aout_buff_len;
    unsigned int  playback_sample_rate;
    unsigned int  playback_channel_num;
    unsigned int  playback_bitwidth;
    unsigned int  playback_pkt_drop_time_ms;

    int           capture_enable;
    unsigned int  capture_sample_rate;
    unsigned int  capture_channel_num;
    unsigned int  capture_bitwidth;
} uac_core_config_t;

struct downstream_fifo_info {
    unsigned int cycle_times;
    unsigned int position;
};


typedef int (*playback_cbk_t)(unsigned int w_ptr);

struct uac_core_data_update_info {
    unsigned int version;

    unsigned int playback_buff_addr;
    unsigned int playback_buff_len;
};

struct uac_core_data_update_cbk {
    playback_cbk_t playback;
};

/*
 * uac_core_init - Init basic uac core data structure
 */
void uac_core_init(uac_core_config_t *config);

void uac_core_task (void);
void uac_core_playback_callback (unsigned int recv_size);
void uac_core_capture_callback  (unsigned int send_size);
void uac_core_put_data          (unsigned int data_size); // like uac_core_playback_callback

/*
 * uac clock sync policy enable/disable
 */
void uac_core_stop(void);
void uac_core_restart(void);

/*
 * return : 0 for success, -1 for failed
 */

int uac_core_get_downstream_fifo_info(struct downstream_fifo_info *info);

// 通知 uac core 状态变化
void uac_core_playback_start_callback(void);
void uac_core_playback_stop_callback(void);
void uac_core_capture_start_callback(void);
void uac_core_capture_stop_callback(void);

// ----------- 订阅 uac 上下行时读写指针的变化，通过回调 -----------

/*
 * uac_core_data_update_get_info：获取 uac 上下行 buff 的信息（地址和长度）
 */
int uac_core_data_update_get_info(struct uac_core_data_update_info *info);

/*
 * uac_core_data_update_callback_register：注册回调函数，用于获取 uac 上下行的读写指针，建议回调中增加圈数的统计
 */
int uac_core_data_update_callback_register(struct uac_core_data_update_cbk *cbk);

// uac upstream lost packet detect
extern unsigned int       uac_up_left_pkt_num;
extern unsigned int       uac_up_lost_pkt, uac_up_lost_pkt_num, uac_up_lost_pkt_total;
extern unsigned long long uac_up_lost_pkt_time;

#endif
