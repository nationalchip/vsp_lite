#ifndef __MULTICORE_LOCK_H__
#define __MULTICORE_LOCK_H__

typedef enum {
    MULTICORE_LOCK_OTP = (1 << 0),
} MULTICORE_LOCK;

int MultiCoreTryLock(MULTICORE_LOCK lock);
int MultiCoreLock(MULTICORE_LOCK lock);
int MultiCoreUnlock(MULTICORE_LOCK lock);

#endif
