/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * console.h: MCU Console device driver
 *
 */

#ifndef __MCU_CONSOLE__
#define __MCU_CONSOLE__

// Console can be UART or USB Slave, defined by Configuration Macro.
int ConsoleInit(void);
int ConsoleDone(void);

//-------------------------------------------------------------------------------------------------
// All console API is async call

void ConsoleSendChar(char c);
void ConsoleCompatibleSendChar(char c);
int ConsoleTstc(void);

#endif /* __MCU_CONSOLE__ */

