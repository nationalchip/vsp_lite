/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * oem.h: MCU's OEM API
 *
 */

#ifndef __OEM_H__
#define __OEM_H__

#ifndef FLASH_TOTAL_SIZE
#define FLASH_TOTAL_SIZE 0x200000
#endif

#ifndef FLASH_BLOCK_SIZE
#define FLASH_BLOCK_SIZE (64 * 1024)
#endif

#define USER_DATA_OFFSET (OEM_OFFSET + sizeof(OEM_INFO))

#define OEM_SIZE        (sizeof(OEM_INFO))
#define OEM_CRC32_LEN   4
#define OEM_SN_LEN      24
#define OEM_MAGIC       0x4F454D48 // "OEMH"
#define OEM_VERSION     0x20191015

#define USER_DATA_MAX_LEN 256

typedef struct {
    unsigned int    len;
    unsigned char   data[USER_DATA_MAX_LEN];
    unsigned int    user_data_crc32;
} __attribute__((packed)) USER_DATA;

typedef struct {
    unsigned int    magic;
    unsigned int    oem_version;
    unsigned char   hardware_version[32];
    unsigned char   vendor_id[4];
    unsigned char   product_id[4];
    unsigned char   sn[OEM_SN_LEN];
    unsigned char   reserved[64];
    unsigned int    oem_crc32;
} __attribute__((packed)) OEM_INFO;

int OemWrite(OEM_INFO *oem_info);
int OemRead(OEM_INFO *oem_info);

int UserDataWrite(USER_DATA *user_data);
int UserDataRead(USER_DATA *user_data);

#endif /* __OEM_H__ */

