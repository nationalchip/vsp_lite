/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * dsp.h: MCU DSP Driver
 *
 */

#ifndef __MCU_DSP_H__
#define __MCU_DSP_H__

#include <autoconf.h>
#include <types.h>
#include <vsp_message.h>

//=================================================================================================

typedef int (*DSP_VSP_CALLBACK)(volatile VSP_MSG_DSP_MCU *message, void *priv);
void DspSetVspCallback(DSP_VSP_CALLBACK callback, void *priv);

int DspPostVspMessage(const uint32_t *message, const uint32_t size);
int DspSendVspMessage(const uint32_t *request, const uint32_t size,
                      DSP_VSP_CALLBACK callback, void *priv, unsigned int timeout_ms);

//-------------------------------------------------------------------------------------------------
int DspInit(void);
int DspDone(void);
void DspReset(void);
int DspLoadPTCMSection(const uint8_t *src, uint32_t length);
int DspLoadDTCMSection(const uint8_t *src, uint32_t length);
int DspLoadSRAMSection(uint8_t *dst, const uint8_t *src, uint32_t length);
int DspRun(void);
int DspStop(void);

//=================================================================================================
#ifdef CONFIG_MCU_ENABLE_DEBUG
void DspDumpRegs(const int detail);
#else
#define DspDumpRegs(x) while(0)
#endif


#endif  /* __MCU_DSP_H__ */
