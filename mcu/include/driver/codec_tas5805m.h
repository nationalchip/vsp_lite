/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * codec_tas5805m.h: CODEC Driver for TAS5805M
 *
 */

#ifndef __CODEC_TAS5805M_H__
#define __CODEC_TAS5805M_H__

int CodecTas5805mInit(unsigned int i2c_bus);
int CodecTas5805mfDone(void);

#endif
