/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * tpm_atsha204a.h: Tpm Chip
 *
 */

#ifndef __TPM_ATSHA204A_H__
#define __TPM_ATSHA204A_H__

int TpmAtsha204aInit(void);
int TpmAtsha204aDone(void);

int TpmAtsha204aReadSN(unsigned char *read_SN);

#endif  /* __TPM_ATSHA204A_H__ */
