/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * otp.h: MCU's OTP API
 *
 */

#ifndef __OTP_H__
#define __OTP_H__

typedef enum {
    OTP_MODE_READONLY,
    OTP_MODE_READWRITE,
} OTP_MODE;

#define CHIP_VERSION_A 'A'
#define CHIP_VERSION_B 'B'
#define CHIP_VERSION_C 'C'

int OtpInit(OTP_MODE mode);
int OtpDone(void);

// Section 0, Locked by NationalChip
#define OTP_CHIP_NAME_LENGTH (12)
int OtpGetChipName(char *chip_name, unsigned int buf_len);
int OtpGetPublicID(unsigned long long *public_id);
int OtpGetChipVersion(void);

// Section 1
// Section 2
// Section 3

// Section 4
#define OTP_USER_DATA_LENGTH (64)
int OtpSetUserData(unsigned int index, char *data, unsigned int length);
int OtpGetUserData(unsigned int index, char *data, unsigned int length);

int OtpLock(void);
int OtpGetLock(unsigned int *lock);

// Section 5

#endif /* __OTP_H__ */
