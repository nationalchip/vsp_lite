/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * dto.h: DTO API
 *
 */

#ifndef __DTO_H__
#define __DTO_H__

#include <soc_config.h>

typedef enum {
    DTO_MCU = 0,
    DTO_APB,
    DTO_UART,
    DTO_SNPU,
    DTO_DSP,
    DTO_MTC,
    DTO_AUDIO_OUT,
    DTO_AUDIO_IN,
    DTO_SRAM
} DTO_TYPE;

unsigned int DtoGetStep(DTO_TYPE dto_type);
int DtoSetStep(DTO_TYPE dto_type, unsigned int dto_step);

#define DTO_FREQ_FROM_STEP(step) ({                                 \
    unsigned long long value = CONFIG_DTO_INPUT;                    \
    value = value * step + (1 << 29) - 1;                           \
    unsigned int result = (unsigned int)(value >> 30);              \
    result;                                                         \
})

#define DTO_STEP_FROM_FREQ(freq) ({                                 \
    unsigned long long value = freq;                                \
    value = (value << 30) + CONFIG_DTO_INPUT / 2;                   \
    unsigned int result = (unsigned int)(value / CONFIG_DTO_INPUT); \
    result = result & 0x3FFFFFFF;                                   \
    result;                                                         \
})

#endif /* __DTO_H__ */
