/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * led.h: LED Driver for SN3193
 *
 */

#ifndef __LED_SGM31324_H__
#define __LED_SGM31324_H__

#include "led.h"

typedef enum{
    SGM31324_LED_BLINK_ALWAYSON,
    SGM31324_LED_BLINK_ALWAYSOFF,
    SGM31324_LED_BLINK_SLOWLY,
    SGM31324_LED_BLINK_NORMAL,
    SGM31324_LED_BLINK_FAST
} SGM31324_LED_BLINK_MODE;

typedef enum {
    SGM31324_LED_MODE_ALWAYS_OFF,
    SGM31324_LED_MODE_ALWAYS_ON,
    SGM31324_LED_MODE_CHANNEL_1,
    SGM31324_LED_MODE_CHANNEL_2
} SGM31324_LED_WORK_MODE;

typedef enum {
    SGM31324_TIME_SLOT_MULTIPLE_X8,
    SGM31324_TIME_SLOT_MULTIPLE_X16,
    SGM31324_TIME_SLOT_MULTIPLE_X32,
    SGM31324_TIME_SLOT_MULTIPLE_X1
} SGM31324_TIME_SLOT_MULTIPLE;

typedef struct {
    unsigned int                flash_source_period;            //  Blink period (128ms-16510ms)
    unsigned int                flash_rise_time_slot;           //  Base time of dark to bright (1500ns-180000ns)
    unsigned int                flash_fall_time_slot;           //  Base time of bright to dark (1500ns-180000ns)
    SGM31324_TIME_SLOT_MULTIPLE time_slot_multiple;             //  the multiple of base time to true time
    unsigned int                thousandth_on_time_channel_1;   //  Proportion of the bright time in blink period (0-1000)
    unsigned int                thousandth_on_time_channel_2;   //  Proportion of the bright time in blink period (0-1000)
    SGM31324_LED_WORK_MODE      switch_mode_led_1;              //  ALWAYS_OFF,ALWAYS_ON,CHANNEL_1,CHANNEL_2
    SGM31324_LED_WORK_MODE      switch_mode_led_2;              //  ALWAYS_OFF,ALWAYS_ON,CHANNEL_1,CHANNEL_2
    SGM31324_LED_WORK_MODE      switch_mode_led_3;              //  ALWAYS_OFF,ALWAYS_ON,CHANNEL_1,CHANNEL_2
    unsigned int                current_led_1;                  //  (125nA-24000nA)
    unsigned int                current_led_2;                  //  (125nA-24000nA)
    unsigned int                current_led_3;                  //  (125nA-24000nA)
    unsigned int                auto_flash_flag;                //  0:disable auto flash;1:Enable auto flash
    unsigned int                register_control;               //  This value will be used when (register_control & 0x80) > 0
    unsigned int                on_off_led_all;                 //  Take all led ON/OFF
} SGM31324_CONFIG;

int LedSGM31324SetConfig(SGM31324_CONFIG *config);
int LedSGM31324GetConfig(SGM31324_CONFIG *config);
int LedSGM31324SetBlink(LED_COLOR rgb, SGM31324_LED_BLINK_MODE mode);
int LedSGM31324Init(int i2c_bus, int i2c_chip);
int LedSGM31324Done(void);
int LedSGM31324SetColor(LED_COLOR rbg);

#endif
