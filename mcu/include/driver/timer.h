/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * delay.h: MCU timer
 *
 */

#ifndef __TIMER_H__
#define __TIMER_H__

typedef int (*TIMER_CALLBACK)(void *private_data);

int TimerInit(unsigned long long period_us);
void TimerDone(void);

int TimerRegisterCallback_us(TIMER_CALLBACK callback, void *private_data, unsigned long long interval_us);
int TimerRegisterCallback(TIMER_CALLBACK callback, void *private_data, unsigned int interval_ms);
int TimerUnregisterCallback(TIMER_CALLBACK callback);

#endif
