/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * padmux.h: pad multiplexing configure
 *
 */

#ifndef __BOOT_PADMUX_H__
#define __BOOT_PADMUX_H__

typedef struct {
    unsigned char pad_id;
    unsigned char pad_function;
} PADMUX_PAD_CONFIG;

int BootPadMuxInit(const PADMUX_PAD_CONFIG *config_table, int table_size);

int BootPadMuxSetFunction(int pad_id, int pad_function);
int BootPadMuxGetFunction(int pad_id);

#endif  /* __PADMUX_H__ */
