/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * boot_gpio.h: Lite GPIO driver for BOOT
 *
 */

#ifndef __BOOT_GPIO_H__
#define __BOOT_GPIO_H__

//=================================================================================================

typedef enum {
    GPIO_DIRECTION_INPUT = 0,
    GPIO_DIRECTION_OUTPUT
} GPIO_DIRECTION;

GPIO_DIRECTION BootGpioGetDirection(unsigned int port);
int BootGpioSetDirection(unsigned int port, GPIO_DIRECTION direction);

//-------------------------------------------------------------------------------------------------

typedef enum {
    GPIO_LEVEL_LOW = 0,
    GPIO_LEVEL_HIGH
} GPIO_LEVEL;

GPIO_LEVEL BootGpioGetLevel(unsigned int port);
int BootGpioSetLevel(unsigned int port, GPIO_LEVEL level);

//=================================================================================================

#endif /* __BOOT_GPIO_H__ */
