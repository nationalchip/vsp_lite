/*
 * This file is part of the EasyFlash Library.
 *
 * Copyright (c) 2015-2019, Armink, <armink.ztl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Function: Portable interface for each platform.
 * Created on: 2015-01-16
 */

#include <easyflash.h>
//#include <stdarg.h>
#include <driver/irq.h>
#include <driver/spi_flash.h>

#define LOG_TAG "[EasyFlash] "
/* default environment variables set for user */
static const ef_env default_env_set[] = {
//    {"test",     "Hello World!", 0},
//    {"username", "armin", 0},
//    {"password", "123", 0},
//    {"vendor", "NationalChip", 0},
};

/**
 * Flash port for hardware initialize.
 *
 * @param default_env default ENV set for user
 * @param default_env_size default ENV size
 *
 * @return result
 */
EfErrCode ef_port_init(ef_env const **default_env, size_t *default_env_size) {
    EfErrCode result = EF_NO_ERR;

    *default_env = default_env_set;
    *default_env_size = sizeof(default_env_set) / sizeof(default_env_set[0]);


    if (SpiFlashInitPure(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        result = EF_ENV_INIT_FAILED;
    }

    return result;
}

/**
 * Read data from flash.
 * @note This operation's units is word.
 *
 * @param addr flash address
 * @param buf buffer to store read data
 * @param size read bytes size
 *
 * @return result
 */
EfErrCode ef_port_read(uint32_t addr, uint32_t *buf, size_t size) {
    EfErrCode result = EF_NO_ERR;

    /* You can add your code under here. */
    if (SpiFlashInitPure(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        result = EF_ENV_INIT_FAILED;
    }

    SpiFlashReadPure(addr, (unsigned char *)buf, size);
    return result;
}

/**
 * Erase data on flash.
 * @note This operation is irreversible.
 * @note This operation's units is different which on many chips.
 *
 * @param addr flash address
 * @param size erase bytes size
 *
 * @return result
 */
EfErrCode ef_port_erase(uint32_t addr, size_t size) {
    EfErrCode result = EF_NO_ERR;

    /* make sure the start address is a multiple of EF_ERASE_MIN_SIZE */
    EF_ASSERT(addr % EF_ERASE_MIN_SIZE == 0);

    /* You can add your code under here. */
    if (SpiFlashInitPure(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        result = EF_ENV_INIT_FAILED;
    }
    SpiFlashErase(addr, size);

    return result;
}
/**
 * Write data to flash.
 * @note This operation's units is word.
 * @note This operation must after erase. @see flash_erase.
 *
 * @param addr flash address
 * @param buf the write data buffer
 * @param size write bytes size
 *
 * @return result
 */
EfErrCode ef_port_write(uint32_t addr, const uint32_t *buf, size_t size) {
    EfErrCode result = EF_NO_ERR;

    /* You can add your code under here. */
    if (SpiFlashInitPure(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        result = EF_ENV_INIT_FAILED;
    }
    SpiFlashWrite(addr, (unsigned char *)buf, size);


    return result;
}

/**
 * lock the ENV ram cache
 */
void ef_port_env_lock(void) {

    /* You can add your code under here. */
    gx_disable_irq();
}

/**
 * unlock the ENV ram cache
 */
void ef_port_env_unlock(void) {

    /* You can add your code under here. */
    gx_enable_irq();
}



#define PRINTF_BUFF_SIZE 256

#ifdef CONFIG_MCU_ENABLE_USB_PRINTF
#define USB_PRINT_BUFF_SIZE (2 * PRINTF_BUFF_SIZE)
static char tmp_buff[USB_PRINT_BUFF_SIZE];

static void gs_puts(const char *str)
{
    int  n = strlen(str);
    int  i = 0;

    for (i = 0; n > 0 && (i < sizeof(tmp_buff)); n--) {
        if (*str == '\n')
            tmp_buff[i++] = '\r';
        tmp_buff[i++] = *str;

        str++;
    }

    GsWrite(tmp_buff, i);
}
#endif

/**
 * This function is print flash debug info.
 *
 * @param file the file which has call this function
 * @param line the line number which has call this function
 * @param format output format
 * @param ... args
 *
 */
void ef_log_debug(const char *file, const long line, const char *format, ...) {
#ifdef PRINT_DEBUG
#ifdef CONFIG_MCU_ENABLE_PRINTF
    char buf[PRINTF_BUFF_SIZE];

    va_list args;

    /* args point to the first variable parameter */
    va_start(args, format);

    /* You can add your code under here. */
    printf(LOG_TAG"<%ld:%s>", line, file);
    vsprintf(buf, format, args);
    va_end(args);

#ifdef CONFIG_MCU_ENABLE_UART_PRINTF
    puts(buf);
#endif

#ifdef CONFIG_MCU_ENABLE_USB_PRINTF
    gs_puts(buf);
#endif

#endif
#endif

}

/**
 * This function is print flash routine info.
 *
 * @param format output format
 * @param ... args
 */
void ef_log_info(const char *format, ...) {
#ifdef PRINT_DEBUG

#ifdef CONFIG_MCU_ENABLE_PRINTF
    char buf[PRINTF_BUFF_SIZE];

    va_list args;

    /* args point to the first variable parameter */
    va_start(args, format);

    /* You can add your code under here. */
    vsprintf(buf, format, args);
    va_end(args);

#ifdef CONFIG_MCU_ENABLE_UART_PRINTF
    puts(buf);
#endif

#ifdef CONFIG_MCU_ENABLE_USB_PRINTF
    gs_puts(buf);
#endif

#endif
#endif
}
/**
 * This function is print flash non-package info.
 *
 * @param format output format
 * @param ... args
 */
void ef_print(const char *format, ...) {
#ifdef PRINT_DEBUG

#ifdef CONFIG_MCU_ENABLE_PRINTF
    char buf[PRINTF_BUFF_SIZE];

    va_list args;

    /* args point to the first variable parameter */
    va_start(args, format);

    /* You can add your code under here. */
    vsprintf(buf, format, args);
    va_end(args);

#ifdef CONFIG_MCU_ENABLE_UART_PRINTF
    puts(buf);
#endif

#ifdef CONFIG_MCU_ENABLE_USB_PRINTF
    gs_puts(buf);
#endif

#endif
#endif
}
