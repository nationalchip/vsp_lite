/*
 * (C) Copyright 2000-2009
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __COMMON_H__
#define __COMMON_H__

//=================================================================================================

#ifndef __ASSEMBLY__		/* put C only stuff in this section */

typedef volatile unsigned long	vu_long;
typedef volatile unsigned short vu_short;
typedef volatile unsigned char	vu_char;

unsigned int BIG_TO_LITTLE(unsigned int value);
unsigned int ENDIAN_SWITCH(unsigned int value);
unsigned short ENDIAN_SWITCH16(unsigned short value);

#define readb(addr) \
	    ({ unsigned char __v = (*(volatile unsigned char *) (addr)); __v; })
#define readw(addr) \
	    ({ unsigned short __v = (*(volatile unsigned short *) (addr)); __v; })
#define readl(addr) \
	    ({ unsigned int __v = (*(volatile unsigned int *) (addr)); __v; })

#define writeb(b,addr) (void)((*(volatile unsigned char *) (addr)) = (b))
#define writew(b,addr) (void)((*(volatile unsigned short *) (addr)) = (b))
#define writel(b,addr) (void)((*(volatile unsigned int *) (addr)) = (b))

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define ROUND(a,b)		(((a) + (b) - 1) & ~((b) - 1))
#define DIV_ROUND(n,d)		(((n) + ((d)/2)) / (d))
#define DIV_ROUND_UP(n,d)	(((n) + (d) - 1) / (d))
#define roundup(x, y)		((((x) + ((y) - 1)) / (y)) * (y))

#define ALIGN(x,a)		__ALIGN_MASK((x),(typeof(x))(a)-1)
#define __ALIGN_MASK(x,mask)	(((x)+(mask))&~(mask))

#define uswap_16(x) \
	((((x) & 0xff00) >> 8) | \
	 (((x) & 0x00ff) << 8))
#define uswap_32(x) \
	((((x) & 0xff000000) >> 24) | \
	 (((x) & 0x00ff0000) >>  8) | \
	 (((x) & 0x0000ff00) <<  8) | \
	 (((x) & 0x000000ff) << 24))
#define _uswap_64(x, sfx) \
	((((x) & 0xff00000000000000##sfx) >> 56) | \
	 (((x) & 0x00ff000000000000##sfx) >> 40) | \
	 (((x) & 0x0000ff0000000000##sfx) >> 24) | \
	 (((x) & 0x000000ff00000000##sfx) >>  8) | \
	 (((x) & 0x00000000ff000000##sfx) <<  8) | \
	 (((x) & 0x0000000000ff0000##sfx) << 24) | \
	 (((x) & 0x000000000000ff00##sfx) << 40) | \
	 (((x) & 0x00000000000000ff##sfx) << 56))
#if defined(__GNUC__)
# define uswap_64(x) _uswap_64(x, ull)
#else
# define uswap_64(x) _uswap_64(x, )
#endif

#define __LITTLE_ENDIAN 1
#define __BYTE_ORDER __LITTLE_ENDIAN


# define cpu_to_le16(x)		(x)
# define cpu_to_le32(x)		(x)
# define cpu_to_le64(x)		(x)
# define le16_to_cpu(x)		(x)
# define le32_to_cpu(x)		(x)
# define le64_to_cpu(x)		(x)
# define cpu_to_be16(x)		uswap_16(x)
# define cpu_to_be32(x)		uswap_32(x)
# define cpu_to_be64(x)		uswap_64(x)
# define be16_to_cpu(x)		uswap_16(x)
# define be32_to_cpu(x)		uswap_32(x)
# define be64_to_cpu(x)		uswap_64(x)

//=================================================================================================

#include <errno.h>
#include <stdio.h>
#include <types.h>
#include <autoconf.h>
//#include <command.h>
//#include <console.h>
//#include <environment.h>

#ifdef DEBUG
#define _DEBUG	1
#else
#define _DEBUG	0
#endif

#ifdef CONFIG_SPL_BUILD
#define _SPL_BUILD	1
#else
#define _SPL_BUILD	0
#endif

#define SECTION_SRAM_TEXT    __attribute__((section(".sram.text")))
#define SECTION_SRAM_RODATA  __attribute__((section(".sram.rodata")))
#define SECTION_XIP_TEXT     __attribute__((section(".xip.text")))
#define SECTION_XIP_RODATA   __attribute__((section(".xip.rodata")))

/* Define this at the top of a file to add a prefix to debug messages */
#ifndef pr_fmt
#define pr_fmt(fmt) fmt
#endif

/*
 * Output a debug text when condition "cond" is met. The "cond" should be
 * computed by a preprocessor in the best case, allowing for the best
 * optimization.
 */
#define debug_cond(cond, fmt, args...)			\
	do {						\
		if (cond)				\
			printf(pr_fmt(fmt), ##args);	\
	} while (0)

/* Show a message if not in SPL */
#define warn_non_spl(fmt, args...)			\
	debug_cond(!_SPL_BUILD, fmt, ##args)

/*
 * An assertion is run-time check done in debug mode only. If DEBUG is not
 * defined then it is skipped. If DEBUG is defined and the assertion fails,
 * then it calls panic*( which may or may not reset/halt U-Boot (see
 * CONFIG_PANIC_HANG), It is hoped that all failing assertions are found
 * before release, and after release it is hoped that they don't matter. But
 * in any case these failing assertions cannot be fixed with a reset (which
 * may just do the same assertion again).
 */
void __assert_fail(const char *assertion, const char *file, unsigned line,
		   const char *function);
#define assert(x) \
	({ if (!(x) && _DEBUG) \
		__assert_fail(#x, __FILE__, __LINE__, __func__); })

#define error(fmt, args...) do {					\
		printf("ERROR: " pr_fmt(fmt) "\nat %s:%d/%s()\n",	\
			##args, __FILE__, __LINE__, __func__);		\
} while (0)

#ifndef BUG
#define BUG() do { \
	printf("BUG: failure at %s:%d/%s()!\n", __FILE__, __LINE__, __FUNCTION__); \
	panic("BUG!"); \
} while (0)
#define BUG_ON(condition) do { if (unlikely((condition)!=0)) BUG(); } while(0)
#endif /* BUG */

typedef void (interrupt_handler_t)(void *);

//=================================================================================================


/* common/main.c */
void	main_loop	(void);
int run_command(const char *cmd, int flag);
int run_command_repeatable(const char *cmd, int flag);

/**
 * Run a list of commands separated by ; or even \0
 *
 * Note that if 'len' is not -1, then the command does not need to be nul
 * terminated, Memory will be allocated for the command in that case.
 *
 * @param cmd	List of commands to run, each separated bu semicolon
 * @param len	Length of commands excluding terminator if known (-1 if not)
 * @param flag	Execution flags (CMD_FLAG_...)
 * @return 0 on success, or != 0 on error.
 */
int run_command_list(const char *cmd, int len, int flag);

int	env_init     (void);
void	env_relocate (void);
int	envmatch     (uchar *, int);

/* Avoid unfortunate conflict with libc's getenv() */
char	*getenv	     (const char *);
int	getenv_f     (const char *name, char *buf, unsigned len);
ulong getenv_ulong(const char *name, int base, ulong default_val);

/**
 * getenv_hex() - Return an environment variable as a hex value
 *
 * Decode an environment as a hex number (it may or may not have a 0x
 * prefix). If the environment variable cannot be found, or does not start
 * with hex digits, the default value is returned.
 *
 * @varname:		Variable to decode
 * @default_val:	Value to return on error
 */
ulong getenv_hex(const char *varname, ulong default_val);

/*
 * Read an environment variable as a boolean
 * Return -1 if variable does not exist (default to true)
 */
int getenv_yesno(const char *var);
int	saveenv	     (void);
int	setenv	     (const char *, const char *);
int setenv_ulong(const char *varname, ulong value);
int setenv_hex(const char *varname, ulong value);

/**
 * setenv_addr - Set an environment variable to an address in hex
 *
 * @varname:	Environment variable to set
 * @addr:	Value to set it to
 * @return 0 if ok, 1 on error
 */
static inline int setenv_addr(const char *varname, const void *addr)
{
	return setenv_hex(varname, (ulong)addr);
}


#ifdef CONFIG_AUTO_COMPLETE
int env_complete(char *var, int maxv, char *cmdv[], int maxsz, char *buf);
#endif
int get_env_id (void);


/* lib/qsort.c */
void qsort(void *base, size_t nmemb, size_t size,
	   int(*compar)(const void *, const void *));
int strcmp_compar(const void *, const void *);

void	fputs(int file, const char *s);
void	fputc(int file, const char c);
int	ftstc(int file);
int	fgetc(int file);

#endif /* __ASSEMBLY__ */


#define ROUND(a,b)		(((a) + (b) - 1) & ~((b) - 1))

//=================================================================================================

/*
 * check_member() - Check the offset of a structure member
 *
 * @structure:	Name of structure (e.g. global_data)
 * @member:	Name of member (e.g. baudrate)
 * @offset:	Expected offset in bytes
 */
#define check_member(structure, member, offset) _Static_assert( \
	offsetof(struct structure, member) == offset, \
	"`struct " #structure "` offset for `" #member "` is not " #offset)

//=================================================================================================

#include <vsprintf.h>


void qsort(void  *base,
	   size_t nel,
	   size_t width,
	   int (*comp)(const void *, const void *));

//=================================================================================================

/*
 * Global Data Flags
 *
 * Note: The low 16 bits are expected for common code.  If your arch
 *       really needs to add your own, use the high 16bits.
 */
#define GD_FLG_RELOC		0x0001	/* Code was relocated to RAM */
#define GD_FLG_DEVINIT		0x0002	/* Devices have been initialized */
#define GD_FLG_SILENT		0x0004	/* Silent mode */
#define GD_FLG_POSTFAIL		0x0008	/* Critical POST test failed */
#define GD_FLG_POSTSTOP		0x0010	/* POST seqeunce aborted */
#define GD_FLG_LOGINIT		0x0020	/* Log Buffer has been initialized */
#define GD_FLG_DISABLE_CONSOLE	0x0040	/* Disable console (in & out) */
#define GD_FLG_ENV_READY	0x0080	/* Environment imported into hash table */
#define GD_FLG_ENV_DEFAULT	0x02000 /* Default variable flag	   */


typedef	struct	global_data {
	unsigned long	flags;
	unsigned long	env_addr;	/* Address  of Environment struct */
	unsigned long	env_valid;	/* Checksum of Environment valid? */

	phys_size_t	ram_size;	/* RAM size */
	unsigned long	start_addr_sp;	/* start_addr_stackpointer */

	unsigned long malloc_ptr;	/* current address */
	unsigned long malloc_base;	

	//void		**jt;		/* jump table */
	char		env_buf[32];	/* buffer for getenv() before reloc. */

} gd_t;

#define DECLARE_GLOBAL_DATA_PTR     extern gd_t *gd

/* crc32.c */
uint32_t crc32 (uint32_t crc, const unsigned char/*Bytef*/ *p, unsigned int/*uInt*/ len);

#endif	/* __COMMON_H__ */
