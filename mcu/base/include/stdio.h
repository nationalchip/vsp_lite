/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * stdio.h: MCU libc standard I/O
 *
 */

#ifndef __STDIO_H__
#define __STDIO_H__

#include <autoconf.h>

// stdarg
typedef void *va_list;

// size of memory storing the data with specified type
// data size must be multiple of sizeof(int) : 4 bytes
#define __va_rounded_size(TYPE)  \
  (((sizeof (TYPE) + sizeof (int) - 1) / sizeof (int)) * sizeof (int))

// stdarg standard macros
#define va_start(AP, LASTARG)   (AP = (va_list) __builtin_next_arg(LASTARG))
#define va_end(AP)
#define va_arg(AP, TYPE)        (AP = (va_list) ((char *) (AP) + __va_rounded_size (TYPE)), *((TYPE *) (void *) ((char *) (AP) - __va_rounded_size(TYPE))))

/* function prototypes */

//int sprintf(char *buf, const char *fmt, ...) __attribute__ ((format(printf, 2, 3)));
int sprintf(char * buf, const char *fmt, ...)
		__attribute__ ((format (__printf__, 2, 3)));
int vsprintf(char *buf, const char *fmt, va_list args);

#ifdef CONFIG_MCU_ENABLE_PRINTF
int printf(const char *fmt, ...);
#else
#define printf(x...) while(0)
#endif

void puts(const char *t);

void heap_init(void *startptr, unsigned int size);
void *malloc(unsigned int size);
void *calloc(unsigned int nmemb, unsigned int elem_size);

void free(void *ptr);
void *realloc(void *p, unsigned int size);
void *page_malloc(unsigned int size);
void page_free(void *p);

void hole_init(void *startptr, unsigned int size);
void *hole_malloc(unsigned int size);
void hole_free(void *ptr);
void *hole_realloc(void *p, unsigned int size);

void tmp_heap_init(void *startptr, unsigned int size);
void *tmp_malloc(unsigned int size);
void tmp_free(void *ptr);
void *tmp_realloc(void *ptr, unsigned int size);

#endif  /* __STDIO_H__ */
