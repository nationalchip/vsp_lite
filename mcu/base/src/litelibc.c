/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * fakelibc.c: fake libc
 *
 */

// ctype.c
#include "stdio.h"
#include "string.h"

//#define ENABLE_MALLOC

//=================================================================================================

int isspace(int ch)
{
	return (ch == '\r' || ch == '\n' || ch == ' ' || ch == '\t') ? 1 : 0;
}

int isdigit(int ch)
{
	return (ch >= '0' && ch <= '9') ? 1 : 0;
}

int isupper(int ch)
{
	return (ch >= 'A' && ch <= 'Z') ? 1 : 0;
}

int toupper(int ch)
{
	return (ch >= 'a' && ch <= 'z') ? ch - 'a' + 'A' : ch;
}

int tolower(int ch)
{
	return (ch >= 'A' && ch <= 'Z') ? ch - 'A' + 'a' : ch;
}

/*
 * add by chenxy
 */
int isxdigit(int ch)
{
	return ((ch >= '0' && ch <= '9')
			|| (ch >= 'a' && ch <= 'f')
			|| (ch >= 'A' && ch <= 'F')) ? 1 : 0;
}
int islower(int ch)
{
	return (ch >= 'a' && ch <= 'z') ? 1 : 0;
}

//=================================================================================================

#ifdef ENABLE_MALLOC

#define LEN32_BYTE_ALIGN (1 << 5)  //ck610 must addr 32bytes align, len 32bytes align
#define PAGE_ALIGN       (1 << 12)

#define MULI_MEM

#ifndef MULI_MEM
static int heap_inited = 0;

#include "q_malloc.h"
static struct qm_block* mem_block;
#define malloc_init(p, s) qm_malloc_init(p, s)
#define pkg_malloc(s)     qm_malloc(mem_block, (s))
#define pkg_realloc(p, s) qm_realloc(mem_block, (p), (s))
#define pkg_free(p)       qm_free(mem_block, (p))

void heap_init(void *startptr, unsigned int size)
{
	if (heap_inited)
		return;
	mem_block = malloc_init(startptr, size);
	if (mem_block == 0){
		printf("Too much pkg memory demanded: %d\n", size);
	}
	heap_inited = 1;
}

static void *malloc_basic(unsigned int size)
{
	return pkg_malloc(size);
}

static void free_basic(void *ptr)
{
	pkg_free(ptr);
}

void *malloc(unsigned int size)
{
	unsigned char *node;
	unsigned char *tmp, *ret;
	unsigned int ssize = sizeof(void *) + size + LEN32_BYTE_ALIGN;
	unsigned int *p;

	node = malloc_basic(ssize);
	if(node == NULL){
		return NULL;
	}

	tmp = node + sizeof(void *);

	ret = (unsigned char *)((unsigned int)(tmp + LEN32_BYTE_ALIGN - 1) & (~(unsigned int)( LEN32_BYTE_ALIGN - 1)));
	p   = (unsigned int *) (ret - 4);
	*p  = (unsigned int)node;

	return ret;
}

void free(void *ptr)
{
	if(ptr){
		void *tmp = (void *)(*(unsigned int *)((unsigned int)ptr - 4));
		if(tmp) free_basic(tmp);
	}
}

void *realloc(void *ptr, unsigned int size)
{
	void *p;

	if (ptr) {
		p = malloc(size);
		if (p)
			memcpy(p,ptr,size); //may be read overflow
		free(ptr);
		return p;
	}
	return malloc(size);
}

#else   // MULI_MEM -------------------------------------------------------------------------------

#include "q_malloc.h"
static int count = 0;
struct heap_block {
	struct qm_block* block;
	unsigned char *startptr;
	unsigned int size;
} mem_block[8];

void heap_init(void *startptr, unsigned int size)
{
	int i;

	for (i = 0; i < count; i++)
		if (mem_block[i].startptr == startptr)
			return;

	if (i == count && i < 8)  {
		mem_block[i].block = qm_malloc_init(startptr, size);
		if (mem_block[i].block == 0){
			printf("Too much pkg memory demanded: %d\n", size);
		}
		mem_block[i].startptr = startptr;
		mem_block[i].size     = size;
		count++;
	}
}

static void *malloc_basic(unsigned int size)
{
	int i;
	void *p;

	for (i=0; i < count; i++) {
		if ((p = qm_malloc(mem_block[i].block, size)))
			return p;
	}

	return NULL;
}

static void free_basic(void *ptr)
{
	int i;

	for (i=0; i < count; i++) {
		if ( (unsigned int)ptr >= (unsigned int)mem_block[i].startptr &&
				(unsigned int)ptr <= (unsigned int)(mem_block[i].startptr + mem_block[i].size))
		{
			qm_free(mem_block[i].block, ptr);
			break;
		}
	}
}

void *malloc(unsigned int size)
{
	unsigned char *node;
	unsigned char *tmp, *ret;
	unsigned int ssize = sizeof(void *) + size + LEN32_BYTE_ALIGN;
	unsigned int *p;

	node = malloc_basic(ssize);
	if(node == NULL){
		return NULL;
	}
	tmp = node + sizeof(void *);
	ret = (unsigned char *)((unsigned int)(tmp + LEN32_BYTE_ALIGN - 1) & (~(unsigned int)( LEN32_BYTE_ALIGN - 1)));
	p   = (unsigned int *) (ret - 4);
	*p  = (unsigned int)node;

	return ret;
}

void free(void *ptr)
{
	if(ptr){
		void *tmp = (void *)(*(unsigned int *)((unsigned int)ptr - 4));
		if(tmp) free_basic(tmp);
	}
}

void *realloc(void *ptr, unsigned int size)
{
	void *p;

	if (ptr) {
		p = malloc(size);
		if (p)
			memcpy(p,ptr,size); //may be read overflow
		free(ptr);
		return p;
	}
	return malloc(size);
}

void *page_malloc(unsigned int size)
{
	unsigned char *node;
	unsigned char *tmp, *ret;
	unsigned int ssize = sizeof(void *) + size + PAGE_ALIGN;
	unsigned int *p;

	node = malloc_basic(ssize);
	if(node == NULL){
		return NULL;
	}
	tmp = node + sizeof(void *);
	ret = (unsigned char *)((unsigned int)(tmp + PAGE_ALIGN - 1) & (~(unsigned int)(PAGE_ALIGN - 1)));
	p   = (unsigned int *) (ret - 4);
	*p  = (unsigned int)node;

	return ret;
}

void page_free(void *ptr)
{
	if(ptr){
		void *tmp = (void *)(*(unsigned int *)((unsigned int)ptr - 4));
		if(tmp) free_basic(tmp);
	}
}

static int hole_block_num = 0;
#define hole_pkg_malloc(s)     qm_malloc(mem_block[hole_block_num].block, (s))
#define hole_pkg_realloc(p, s) qm_realloc(mem_block[hole_block_num].block, (p), (s))
#define hole_pkg_free(p)       qm_free(mem_block[hole_block_num].block, (p))

void hole_init(void *startptr, unsigned int size)
{
	int i;
	for (i = 0; i < count; i++)
		if (mem_block[i].startptr == startptr)
			return;

	if (i == count && i < 8)  {
		hole_block_num = i;
		mem_block[i].block = qm_malloc_init(startptr, size);
		if (mem_block[i].block == 0){
			printf("Too much pkg memory demanded: %d\n", size);
		}
		mem_block[i].startptr = startptr;
		mem_block[i].size     = size;
		count++;
	}
}

void *hole_malloc(unsigned int size)
{
	unsigned char *node;
	unsigned char *tmp, *ret;
	unsigned int ssize = sizeof(void *) + size + LEN32_BYTE_ALIGN;
	unsigned int *p;

	node = hole_pkg_malloc(ssize);
	if(node == NULL){
		return NULL;
	}
	tmp = node + sizeof(void *);
	ret = (unsigned char *)((unsigned int)(tmp + LEN32_BYTE_ALIGN - 1) & (~(unsigned int)( LEN32_BYTE_ALIGN - 1)));
	p   = (unsigned int *) (ret - 4);
	*p  = (unsigned int)node;

	return ret;
}

void hole_free(void *ptr)
{
	if(ptr){
		void *tmp = (void *)(*(unsigned int *)((unsigned int)ptr - 4));
		if(tmp) hole_pkg_free(tmp);
	}
}

void *hole_realloc(void *ptr, unsigned int size)
{
	void *p;

	if (ptr) {
		p = hole_malloc(size);
		if (p)
			memcpy(p,ptr,size); //may be read overflow
		hole_free(ptr);
		return p;
	}
	return hole_malloc(size);
}

static int tmp_mem_initd = 0;
static struct qm_block *tmp_mem_block;
#define tmp_pkg_malloc(s)     qm_malloc(tmp_mem_block, (s))
#define tmp_pkg_realloc(p, s) qm_realloc(tmp_mem_block, (p), (s))
#define tmp_pkg_free(p)       qm_free(tmp_mem_block, (p))

void tmp_heap_init(void *startptr, unsigned int size)
{
	if (tmp_mem_initd)
		return;
	tmp_mem_block = qm_malloc_init(startptr, size);
	if (tmp_mem_block == 0){
		printf("Too much pkg memory demanded: %d\n", size);
	}
	tmp_mem_initd = 1;
}

void *tmp_malloc(unsigned int size)
{
	unsigned char *node;
	unsigned char *tmp, *ret;
	unsigned int ssize = sizeof(void *) + size + LEN32_BYTE_ALIGN;
	unsigned int *p;

	node = tmp_pkg_malloc(ssize);
	if(node == NULL){
		return NULL;
	}
	tmp = node + sizeof(void *);
	ret = (unsigned char *)((unsigned int)(tmp + LEN32_BYTE_ALIGN - 1) & (~(unsigned int)( LEN32_BYTE_ALIGN - 1)));
	p   = (unsigned int *) (ret - 4);
	*p  = (unsigned int)node;

	return ret;
}

void tmp_free(void *ptr)
{
	if(ptr){
		void *tmp = (void *)(*(unsigned int *)((unsigned int)ptr - 4));
		if(tmp)
			tmp_pkg_free(tmp);
	}
}

void *tmp_realloc(void *ptr, unsigned int size)
{
	void *p;

	if (ptr) {
		p = tmp_malloc(size);
		if (p)
			memcpy(p,ptr,size); //may be read overflow
		tmp_free(ptr);
		return p;
	}
	return tmp_malloc(size);
}

#endif  // MULI_MEM -------------------------------------------------------------------------------

void *calloc(unsigned int nmemb, unsigned int elem_size)
{
	size_t size = nmemb * elem_size;
	void *ptr;

	ptr = malloc(size);
	memset(ptr, '\0', size);

	return ptr;
}

#endif // Disable Malloc

//=================================================================================================
//
// stdlib
//

unsigned int atoi(const char *str)
{
	const char *cp;
	unsigned int data = 0;

	if (str[0] == '0' && (str[1] == 'X' || str[1] == 'x'))
		return htoi(str + 2);

	for (cp = str, data = 0; *cp != 0; ++cp) {
		if (*cp >= '0' && *cp <= '9')
			data = data * 10 + *cp - '0';
		else
			break;
	}

	return data;
}

unsigned int htoi(const char *str)
{
	const char *cp;
	unsigned int data = 0, bdata = 0;

	for (cp = str, data = 0; *cp != 0; ++cp) {
		if (*cp >= '0' && *cp <= '9')
			bdata = *cp - '0';
		else if (*cp >= 'A' && *cp <= 'F')
			bdata = *cp - 'A' + 10;
		else if (*cp >= 'a' && *cp <= 'f')
			bdata = *cp - 'a' + 10;
		else
			break;
		data = (data << 4) | bdata;
	}

	return data;
}

//=================================================================================================
#include <driver/console.h>

void puts(const char *t)
{
    while (*t) ConsoleCompatibleSendChar(*t++);
}

void putnstr( const char* str, int n)
{
	while (n && *str != '\0') {
		ConsoleCompatibleSendChar(*str) ;
		str++;
		n--;
	}
}

#define PRINTF_BUFF_SIZE 256

#ifdef CONFIG_MCU_ENABLE_USB_PRINTF
#include <driver/usb_gadget.h>

#define USB_PRINT_BUFF_SIZE (2 * PRINTF_BUFF_SIZE)
static char tmp_buff[USB_PRINT_BUFF_SIZE];

static void gs_puts(const char *str)
{
	int  n = strlen(str);
	int  i = 0;

	for (i = 0; n > 0 && (i < sizeof(tmp_buff)); n--) {
		if (*str == '\n')
			tmp_buff[i++] = '\r';
		tmp_buff[i++] = *str;

		str++;
	}

	GsWrite(tmp_buff, i);
}
#endif

#ifdef CONFIG_MCU_ENABLE_PRINTF
int printf(const char *fmt, ...)
{
	char buf[PRINTF_BUFF_SIZE];

	va_list args;
	int i;

	va_start(args, fmt);
	i = vsprintf(buf, fmt, args);	/* hopefully i < sizeof(buf) */
	va_end(args);

#ifdef CONFIG_MCU_ENABLE_UART_PRINTF
	puts(buf);
#endif

#ifdef CONFIG_MCU_ENABLE_USB_PRINTF
	gs_puts(buf);
#endif

	return i;
}
#endif
