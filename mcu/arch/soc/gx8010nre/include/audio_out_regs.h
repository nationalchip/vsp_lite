/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 NationalChip Co., Ltd
 * All rights reserved!
 *
 * audio_out_regs.h: MCU Audio Out Registers
 *
 */

#ifndef __AUDIO_OUT_REGS_H__
#define __AUDIO_OUT_REGS_H__

//=================================================================================================

typedef enum {
    AUDIO_REF_PCM_ENDIAN_LITTLE = 0x00,
    AUDIO_REF_PCM_ENDIAN_BIG    = 0x01,
} AUDIO_REF_PCM_ENDIAN;

typedef enum pcm_w_channel_sel {
    AUDIO_REF_STEREO = 0x0,
    AUDIO_REF_MONO = 0x1
} AUDIO_REF_MONO_ENABLE;

typedef enum pcm_w_fs_sel{
    AUDIO_REF_SAMPLE_RATE_16KHZ   = 0x00,
    AUDIO_REF_SAMPLE_RATE_08KHZ   = 0x01,
    AUDIO_REF_SAMPLE_RATE_48KHZ   = 0x02,
    AUDIO_REF_SAMPLE_RATE_RESERVED,
} AUDIO_REF_SAMPLE_RATE;

typedef enum {
    AUDIO_REF_SOURCE_INTERNAL = 0x00,
    AUDIO_REF_SOURCE_EXTERNAL = 0x01,
} AUDIO_REF_SOURCE;

/* AUDIO_PLAY_CTRL */
typedef union {
    unsigned int value;
    struct {
        unsigned r1_mix_en:1;
        unsigned r2_mix_en:1;
        unsigned r3_mix_en:1;
        unsigned :1;
        unsigned r1_work_en:1;
        unsigned r2_work_en:1;
        unsigned r3_work_en:1;
        unsigned :1;
        unsigned spdif_play_mode:2;
        unsigned data_stop:1;
        unsigned i2s_data_stop:1;
        unsigned silent_end:1;
        unsigned r1_frame_over_en:1;
        unsigned r2_frame_over_en:1;
        unsigned r3_frame_over_en:1;
        unsigned pcm_w_en:1;
        AUDIO_REF_PCM_ENDIAN pcm_w_endian:1;
        unsigned pcm_w_ch_sel:1;
        AUDIO_REF_MONO_ENABLE pcm_w_mono_en:1;
        AUDIO_REF_SAMPLE_RATE pcm_w_fs_sel:2;
        AUDIO_REF_SOURCE pcm_w_source_sel:1;
        unsigned :6;
        unsigned i2s_mute:1;
        unsigned spdif_mute:1;
        unsigned work_soft_rstn:1;
    } bits;
} AUDIO_PLAY_CTRL;

/* AUDIO_PLAY_I2S_DACINFO */
typedef union {
    unsigned int value;
    struct {
        unsigned data_format:2;
        unsigned length_mode:2;
        unsigned mclk_sel:2;
        unsigned :18;
        unsigned play_channel_0_sel:4;
        unsigned play_channel_1_sel:4;
    } bits;
} AUDIO_PLAY_I2S_DACINFO;

/* AUDIO_SPDIF_C */
typedef union {
    unsigned int value;
    struct {
        unsigned channel_c:32;
    } bits;
} AUDIO_SPDIF_C;

/* AUDIO_SPDIF_U */
typedef union {
    unsigned int value;
    struct {
        unsigned ul:1;
        unsigned ur:1;
        unsigned :30;
    } bits;
} AUDIO_SPDIF_U;

/* AUDIO_PLAY_CHANNEL_SEL */
typedef union {
    unsigned int value;
    struct {
        unsigned r1_channel_0_sel:4;
        unsigned r1_channel_1_sel:4;
        unsigned r2_channel_0_sel:4;
        unsigned r2_channel_1_sel:4;
        unsigned r3_channel_0_sel:4;
        unsigned r3_channel_1_sel:4;
        unsigned write_channel_0_sel:4;
        unsigned write_channel_1_sel:4;
    } bits;
} AUDIO_PLAY_CHANNEL_SEL;

/* AUDIO_PLAY_PCM_W_NUM */
typedef union {
    unsigned int value;
    struct {
        unsigned pcm_w_num:32;
    } bits;
} AUDIO_PLAY_PCM_W_NUM;

/* AUDIO_PLAY_PCM_W_BUFFER_SADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned pcm_w_buffer_saddr:32;
    } bits;
} AUDIO_PLAY_PCM_W_BUFFER_SADDR;

/* AUDIO_PLAY_PCM_W_BUFFER_SIZE */
typedef union {
    unsigned int value;
    struct {
        unsigned pcm_w_buffer_size:32;
    } bits;
} AUDIO_PLAY_PCM_W_BUFFER_SIZE;

/* AUDIO_PLAY_PCM_W_SDC_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned pcm_w_sdc_addr:32;
    } bits;
} AUDIO_PLAY_PCM_W_SDC_ADDR;

/* AUDIO_PLAY_INT_EN */
typedef union {
    unsigned int value;
    struct {
        unsigned r1_set_newpcm_int_en:1;
        unsigned r2_set_newpcm_int_en:1;
        unsigned r3_set_newpcm_int_en:1;
        unsigned :3;
        unsigned i2s_in_pcm_read_err2_int_en:1;
        unsigned i2s_in_pcm_read_err_int_en:1;
        unsigned pcm_w_done_int_en:1;
        unsigned :23;
    } bits;
} AUDIO_PLAY_INT_EN;

/* AUDIO_PLAY_INT */
typedef union {
    unsigned int value;
    struct {
        unsigned r1_set_newpcm_int:1;
        unsigned r2_set_newpcm_int:1;
        unsigned r3_set_newpcm_int:1;
        unsigned :3;
        unsigned i2s_in_pcm_read_err2_int:1;
        unsigned i2s_in_pcm_read_err_int:1;
        unsigned pcm_w_done_int:1;
        unsigned r1_frame_over:1;
        unsigned r2_frame_over:1;
        unsigned r3_frame_over:1;
        unsigned :4;
        unsigned reg_info_fresh:1;
        unsigned :3;
        unsigned axi_state_idle:1;
        unsigned cpu_reopen_aup:1;
        unsigned cpu_stop_aup_req:1;
        unsigned :9;
    } bits;
} AUDIO_PLAY_INT;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AUDIO_PLAY_SMAPLE_RATE_48000 = 0x00,
    AUDIO_PLAY_SAMPLE_RATE_44100 = 0x01,
    AUDIO_PLAY_SAMPLE_RATE_32000 = 0x02,
    AUDIO_PLAY_SAMPLE_RATE_24000 = 0x03,
    AUDIO_PLAY_SAMPLE_RATE_22050 = 0x04,
    AUDIO_PLAY_SAMPLE_RATE_16000 = 0x05,
    AUDIO_PLAY_SAMPLE_RATE_11025 = 0x06,
    AUDIO_PLAY_SAMPLE_RATE_8000 = 0x07,
} AUDIO_PLAY_SAMPLE_RATE;

/* AUDIO_PLAY_R_INDATA */
typedef union {
    unsigned int value;
    struct {
        AUDIO_PLAY_SAMPLE_RATE fs_mode:3;
        unsigned channel_sel:1;
        unsigned pcm_endian:2;
        unsigned :1;
        unsigned pcm_store_mode:1;
        unsigned pcm_bit_size:3;
        unsigned :1;
        unsigned src_sel:2;
        unsigned :18;
    } bits;
} AUDIO_PLAY_R_INDATA;

/* AUDIO_PLAY_R_VOL_CTRL */
typedef union {
    unsigned int value;
    struct {
        unsigned vol_level:10;
        unsigned :22;
    } bits;
} AUDIO_PLAY_R_VOL_CTRL;

/* AUDIO_PLAY_R_BUFFER_START_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned r_buffer_saddr:32;
    } bits;
} AUDIO_PLAY_R_BUFFER_START_ADDR;

/* AUDIO_PLAY_R_BUFFER_SIZE */
typedef union {
    unsigned int value;
    struct {
        unsigned r_buffer_size:32;
    } bits;
} AUDIO_PLAY_R_BUFFER_SIZE;

/* AUDIO_PLAY_R_BUFFER_SDC_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned r_buffer_read_addr:32;
    } bits;
} AUDIO_PLAY_R_BUFFER_SDC_ADDR;

/* AUDIO_PLAY_R_NEW_FRAME_START_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned r_new_frame_start_addr:32;
    } bits;
} AUDIO_PLAY_R_NEW_FRAME_START_ADDR;

/* AUDIO_PLAY_R_NEW_FRAME_END_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned r_new_frame_end_addr:32;
    } bits;
} AUDIO_PLAY_R_NEW_FRAME_END_ADDR;

/* AUDIO_PLAY_R_PLAYING_FRAME_START_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned r_playing_frame_start_addr:32;
    } bits;
} AUDIO_PLAY_R_PLAYING_FRAME_START_ADDR;

/* AUDIO_PLAY_R_PLAYING_FRAME_END_ADDR */
typedef union {
    unsigned int value;
    struct {
        unsigned r_playing_frame_end_addr:32;
    } bits;
} AUDIO_PLAY_R_PLAYING_FRAME_END_ADDR;

/* AUDIO_PLAY_R_NEW_FRAME_PCM_LEN */
typedef union {
    unsigned int value;
    struct {
        unsigned r_new_frame_sample_len:16;
        unsigned :16;
    } bits;
} AUDIO_PLAY_R_NEW_FRAME_PCM_LEN;

/* AUDIO_PLAY_R_PLAYING_FRAME_PCM_LEN */
typedef union {
    unsigned int value;
    struct {
        unsigned r_playing_frame_sample_len:16;
        unsigned :16;
    } bits;
} AUDIO_PLAY_R_PLAYING_FRAME_PCM_LEN;

/* AUDIO_PLAY_R_SET_NEWFRAME_CTRL */
typedef union {
    unsigned int value;
    struct {
        unsigned r_set_newframe_start:1;
        unsigned r_set_newframe_finish:1;
        unsigned :30;
    } bits;
} AUDIO_PLAY_R_SET_NEWFRAME_CTRL;

typedef struct {
    AUDIO_PLAY_R_INDATA                     indata;                     // 0x0040
    AUDIO_PLAY_R_VOL_CTRL                   vol_ctrl;
    AUDIO_PLAY_R_BUFFER_START_ADDR          buffer_start_addr;
    AUDIO_PLAY_R_BUFFER_SIZE                buffer_size;
    AUDIO_PLAY_R_BUFFER_SDC_ADDR            buffer_sdc_addr;            // 0x0050
    AUDIO_PLAY_R_NEW_FRAME_START_ADDR       new_frame_start_addr;
    AUDIO_PLAY_R_NEW_FRAME_END_ADDR         new_frame_end_addr;
    AUDIO_PLAY_R_PLAYING_FRAME_START_ADDR   playing_frame_start_addr;
    AUDIO_PLAY_R_PLAYING_FRAME_END_ADDR     playing_frame_end_addr;     // 0x0060
    AUDIO_PLAY_R_NEW_FRAME_PCM_LEN          new_frame_pcm_len;
    AUDIO_PLAY_R_PLAYING_FRAME_PCM_LEN      playing_frame_pcm_len;
    AUDIO_PLAY_R_SET_NEWFRAME_CTRL          set_newframe_ctrl;
} AUDIO_PLAY_ROUTE;

//-------------------------------------------------------------------------------------------------

/* AUDIO_PLAY_I2S_IN_INFO */
typedef union {
    unsigned int value;
    struct {
        unsigned i2s_in_data_format:2;
        unsigned i2s_in_bclk_inv_en:1;
        unsigned i2s_in_lrclk_inv_en:1;
        unsigned i2s_in_pcm_fs:3;
        unsigned i2s_in_work_en:1;
        unsigned i2s_in_full_cap:5;
        unsigned i2s_in_data_length:3;
        unsigned :16;
    } bits;
} AUDIO_PLAY_I2S_IN_INFO;

typedef struct {
    AUDIO_PLAY_CTRL                 audio_play_ctrl;                // 0x0000
    AUDIO_PLAY_I2S_DACINFO          audio_play_i2s_dacinfo;
    AUDIO_SPDIF_C                   audio_spdif_cl1;
    AUDIO_SPDIF_C                   audio_spdif_cl2;
    AUDIO_SPDIF_C                   audio_spdif_cr1;                // 0x0010
    AUDIO_SPDIF_C                   audio_spdif_cr2;
    AUDIO_SPDIF_U                   audio_spdif_u;
    AUDIO_PLAY_CHANNEL_SEL          audio_play_channel_sel;
    AUDIO_PLAY_PCM_W_NUM            audio_play_pcm_w_num;           // 0x0020
    AUDIO_PLAY_PCM_W_BUFFER_SADDR   audio_play_pcm_w_buffer_saddr;
    AUDIO_PLAY_PCM_W_BUFFER_SIZE    audio_play_pcm_w_buffer_size;
    AUDIO_PLAY_PCM_W_SDC_ADDR       audio_play_pcm_w_sdc_addr;
    AUDIO_PLAY_INT_EN               audio_play_int_en;              // 0x0030
    AUDIO_PLAY_INT                  audio_play_int;
    unsigned int RESERVED[2];
    
    AUDIO_PLAY_ROUTE                audio_play_route[3];
    
    AUDIO_SPDIF_C                   audio_spdif_cl3;                // 0x00D0
    AUDIO_SPDIF_C                   audio_spdif_cl4;
    AUDIO_SPDIF_C                   audio_spdif_cr3;
    AUDIO_SPDIF_C                   audio_spdif_cr4;
    AUDIO_SPDIF_C                   audio_spdif_cl5;                // 0x00E0
    AUDIO_SPDIF_C                   audio_spdif_cl6;
    AUDIO_SPDIF_C                   audio_spdif_cr5;
    AUDIO_SPDIF_C                   audio_spdif_cr6;
    
    AUDIO_PLAY_I2S_IN_INFO          audio_play_i2s_in_info;         // 0x00F0
}AUDIO_OUT_REGS;

//=================================================================================================

#endif /* __AUDIO_OUT_REGS_H__ */
