/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * cpu_regs.h: CPU registers fields
 *
 */

#ifndef __CPU_REGS_H__
#define __CPU_REGS_H__

#include <types.h>
#include "base_addr.h"

//=================================================================================================
// Any Register Offsets are based on the MCU_REG_BASE_CONFIG

#define REG_OFFSET_LOW_POWER            (0x0150)

#define REG_OFFSET_CK_A7_INT_ENABLE     (0x0208)

#define REG_OFFSET_A7_ENABLE            (0x0260)
#define REG_OFFSET_A7_POWER_CTRL        (0x0264)
#define REG_OFFSET_CK_GATE2             (0x0068)

#define REG_OFFSET_A7_CK_INT            (0x0268)
#define REG_OFFSET_A7_CK_CLR            (0x026c)
#define REG_OFFSET_A7_CK_ENABLE         (0x0270)
#define REG_OFFSET_CK_A7_INT            (0x0274)
#define REG_OFFSET_CK_A7_CLR            (0x0278)
#define REG_OFFSET_CK_A7_ENABLE         (0x027c)

#define REG_OFFSET_COMMON_REG_DATA00    (0x0280)        // A7 -> CK
#define REG_OFFSET_COMMON_REG_DATA01    (0x0284)
#define REG_OFFSET_COMMON_REG_DATA02    (0x0288)
#define REG_OFFSET_COMMON_REG_DATA03    (0x028c)
#define REG_OFFSET_COMMON_REG_DATA04    (0x0290)
#define REG_OFFSET_COMMON_REG_DATA05    (0x0294)
#define REG_OFFSET_COMMON_REG_DATA06    (0x0298)
#define REG_OFFSET_COMMON_REG_DATA07    (0x029c)

#define REG_OFFSET_COMMON_REG_DATA08    (0x02a0)        // CK -> A7
#define REG_OFFSET_COMMON_REG_DATA09    (0x02a4)
#define REG_OFFSET_COMMON_REG_DATA10    (0x02a8)
#define REG_OFFSET_COMMON_REG_DATA11    (0x02ac)
#define REG_OFFSET_COMMON_REG_DATA12    (0x02b0)
#define REG_OFFSET_COMMON_REG_DATA13    (0x02b4)
#define REG_OFFSET_COMMON_REG_DATA14    (0x02b8)
#define REG_OFFSET_COMMON_REG_DATA15    (0x02bc)

#define REG_OFFSET_COMMON_REG_DATA16    (0x02c0)
#define REG_OFFSET_COMMON_REG_DATA17    (0x02c4)
#define REG_OFFSET_COMMON_REG_DATA18    (0x02c8)
#define REG_OFFSET_COMMON_REG_DATA19    (0x02cc)
#define REG_OFFSET_COMMON_REG_DATA20    (0x02d0)
#define REG_OFFSET_COMMON_REG_DATA21    (0x02d4)
#define REG_OFFSET_COMMON_REG_DATA22    (0x02d8)
#define REG_OFFSET_COMMON_REG_DATA23    (0x02dc)

#define REG_OFFSET_COMMON_REG_DATA24    (0x02e0)
#define REG_OFFSET_COMMON_REG_DATA25    (0x02e4)
#define REG_OFFSET_COMMON_REG_DATA26    (0x02e8)
#define REG_OFFSET_COMMON_REG_DATA27    (0x02ec)
#define REG_OFFSET_COMMON_REG_DATA28    (0x02f0)
#define REG_OFFSET_COMMON_REG_DATA29    (0x02f4)
#define REG_OFFSET_COMMON_REG_DATA30    (0x02f8)
#define REG_OFFSET_COMMON_REG_DATA31    (0x02fc)

// Some Extra Common Registers
#define REG_OFFSET_COMMON_REG_DATA32    (0x00c4)        // PM_RESUME_ADDR
#define REG_OFFSET_COMMON_REG_DATA33    (0x0174)        // PM_REQUEST_TYPE
#define REG_OFFSET_COMMON_REG_DATA34    (0x017c)

// These macros are used by VSP
#define VSP_A7_CK_REG_OFFSET            (REG_OFFSET_COMMON_REG_DATA00)
#define VSP_A7_CK_REG_SIZE              (8)
#define VSP_CK_A7_REG_OFFSET            (REG_OFFSET_COMMON_REG_DATA08)
#define VSP_CK_A7_REG_SIZE              (8)

#define VSP_CK_A7_WAKEUP_REASON         (MCU_REG_BASE_CONFIG + REG_OFFSET_COMMON_REG_DATA31)

#define VSP_A7_CK_PM_REQUEST_TYPE       (MCU_REG_BASE_CONFIG + REG_OFFSET_COMMON_REG_DATA33)
#define VSP_A7_CK_PM_RESUME_ADDR        (MCU_REG_BASE_CONFIG + REG_OFFSET_COMMON_REG_DATA32)

#define MULTICORE_LOCK_MCU_LOCK         (MCU_REG_BASE_CONFIG + REG_OFFSET_COMMON_REG_DATA28)
#define MULTICORE_LOCK_CPU_LOCK         (MCU_REG_BASE_CONFIG + REG_OFFSET_COMMON_REG_DATA29)

#define VSP_A7_DDR_RSP_REG              (VSP_A7_CK_PM_RESUME_ADDR)

//=================================================================================================

typedef union {
    uint32_t value;
    struct {
    unsigned arm_power_config:1;
    unsigned :31;
    } bits;
} LOW_POWER;

typedef union {
    uint32_t value;
    struct {
        unsigned a7_enable:1;                       // 使能 A7 域的模块
        unsigned :31;
    } bits;
} A7_ENABLE;

typedef union {
    uint32_t value;
    struct {
        unsigned pd2_power_ctrl:1;                  // 进行隔离
        unsigned :31;
    } bits;
} A7_POWER_CTRL;

typedef union {
    uint32_t value;
    struct {
        unsigned audio_play:1;
        unsigned :3;
        unsigned vsp_flag:1;                        // 我们只需用第四位
        unsigned :25;
        unsigned cpu_halt:1;
        unsigned :1;
    } bits;
} A7_CK_INT;

typedef union {
    uint32_t value;
    struct {
        unsigned ck_dw_uart1_int:1;
        unsigned ck_dw_uart2_int:1;
        unsigned ck_dw_i2c1_int:1;
        unsigned ck_dw_i2c2_int:1;
        unsigned ck_dw_spi_int:1;
        unsigned ck_count1_int:1;
        unsigned ck_eport1_int:1;
        unsigned ck_dma_int:1;
        unsigned snpu_int:1;
        unsigned audio_int:1;
        unsigned mtc_int:1;
        unsigned dsp_int:1;
        unsigned audio_play_int:1;
        unsigned usb_slave_wuintreq:1;
        unsigned usb_slave_int_req:1;
        unsigned usb_dma_int_req:1;
        unsigned vsp_flag:1;
        unsigned :15;
    } bits;
} CK_A7_INT;

typedef struct {
    A7_CK_INT a7_ck_int;
    A7_CK_INT a7_ck_clear;
    A7_CK_INT a7_ck_enable;
    CK_A7_INT ck_a7_int;
    CK_A7_INT ck_a7_clear;
    CK_A7_INT ck_a7_enable;
} CPU_INT_REGS;

typedef union {
    uint32_t value;
    struct {
        unsigned dto_pll_clk_gate_en:1;
        unsigned :31;
    } bits;
} CK_GATE2;

//=================================================================================================

#endif /* __CPU_REGS_H__ */
