/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * base_addr.h: Base Address for GX8010NRE
 *
 */

#ifndef __BASE_ADDR_H__
#define __BASE_ADDR_H__

//=================================================================================================

// AHB  (AHB Clock)
#define MCU_BUS_BASE_A7_SRAM     0xa0100000

// AHB->APB0 (APB0 Clock)
#define MCU_BUS_BASE_APB0        0xa0200000
#define     MCU_REG_BASE_IRR         0xa0204000
#define     MCU_REG_BASE_I2C0        0xa0205000
#define     MCU_REG_BASE_I2C1        0xa0206000
#define     MCU_REG_BASE_RTC         0xa0209000
#define     MCU_REG_BASE_COUNTER     0xa020a000
#define     MCU_REG_BASE_WDT         0xa020b000

// AHB->APB1 (APB1 Clock)
#define MCU_BUS_BASE_APB1        0xa0300000
#define     MCU_REG_BASE_SPI1        0xa0303000
#define     MCU_REG_BASE_GPIO1       0xa0305000
#define     MCU_REG_BASE_GPIO2       0xa0306000
#define     MCU_REG_BASE_GPIO3       0xa0307000
#define     MCU_REG_BASE_UART0       0xa0308000
#define     MCU_REG_BASE_UART1       0xa0309000
#define     MCU_REG_BASE_CONFIG      0xa030a000
#define     MCU_REG_BASE_AOUT_CODEC  0xa030a1a4
#define     MCU_REG_BASE_INTC_SHARED 0xa030a208
#define     MCU_REG_BASE_WAKE_SOURCE 0xa030a210
#define     MCU_REG_BASE_WAKE_STATUS 0xa030a214
#define     MCU_REG_BASE_CPU_ISOLATE 0xa030a264

#define MCU_REG_BASE_INTC        0xa0500000
#define MCU_REG_BASE_DW_DMA      0xa0800000
#define MCU_REG_BASE_USB_SLAVE   0xa0900000
#define MCU_REG_BASE_SECURE_CTRL 0xa0a00000
#define     MCU_REG_BASE_OTP         0xa0a80000

// AHB->APB2 (APB2 Clock)
#define MCU_BUS_BASE_APB2        0xa1000000
#define     MCU_REG_BASE_SNPU        0xa1000000
#define     MCU_REG_BASE_AIN         0xa1100000
#define     MCU_REG_BASE_AOUT        0xa1200000
#define     MCU_REG_BASE_DSP         0xa1300000

//=================================================================================================

#define MCU_REG_BASE_CK802       0xe0000000
/* GXMCU CK802 */
/* timer */
#define CK802_CORET_CSR         (MCU_REG_BASE_CK802 + 0xe010)
#define CK802_CORET_RVR         (MCU_REG_BASE_CK802 + 0xe014)
#define CK802_CORET_CVR         (MCU_REG_BASE_CK802 + 0xe018)
#define CK802_CORET_CALIB       (MCU_REG_BASE_CK802 + 0xe01c)
/* intc */
#define CK802_VIC_ISER          (MCU_REG_BASE_CK802 + 0xe100)
#define CK802_VIC_IWER          (MCU_REG_BASE_CK802 + 0xe140)
#define CK802_VIC_ICER          (MCU_REG_BASE_CK802 + 0xe180)
#define CK802_VIC_ISPR          (MCU_REG_BASE_CK802 + 0xe200)
#define CK802_VIC_ICPR          (MCU_REG_BASE_CK802 + 0xe280)
#define CK802_VIC_IPR0          (MCU_REG_BASE_CK802 + 0xe400)
#define CK802_VIC_IPR7          (MCU_REG_BASE_CK802 + 0xe41c)
/* cache */
#define CK802_CER               (MCU_REG_BASE_CK802 + 0xf000)
#define CK802_CIR               (MCU_REG_BASE_CK802 + 0xf004)
#define CK802_CRCR0             (MCU_REG_BASE_CK802 + 0xf008)
#define CK802_CRCR1             (MCU_REG_BASE_CK802 + 0xf00c)
#define CK802_CRCR2             (MCU_REG_BASE_CK802 + 0xf010)
#define CK802_CRCR3             (MCU_REG_BASE_CK802 + 0xf014)

/* GXMCU CONFIG */
#define MCU_CONFIG_BASE_ADDR     0x08000000
#define MCU_AP_CMD              (MCU_CONFIG_BASE_ADDR + 0x0)
#define MCU_AP_CMD_EN           (MCU_CONFIG_BASE_ADDR + 0x4)
#define MCU_SP_STATUS           (MCU_CONFIG_BASE_ADDR + 0X8)
#define MCU_SP_STATUS_EN        (MCU_CONFIG_BASE_ADDR + 0x0c)
#define MCU_COSE_BAE            (MCU_CONFIG_BASE_ADDR + 0x10)
#define MCU_PARAM_BASE          (MCU_CONFIG_BASE_ADDR + 0x14)
#define MCU_PUB_ID_LO           (MCU_CONFIG_BASE_ADDR + 0x18)
#define MCU_PUB_ID_HI           (MCU_CONFIG_BASE_ADDR + 0x1c)
#define MCU_SKETCH0             (MCU_CONFIG_BASE_ADDR + 0x20)
#define MCU_SKETCH2             (MCU_CONFIG_BASE_ADDR + 0x24)
#define MCU_CW_WEN              (MCU_CONFIG_BASE_ADDR + 0x40)
#define MCU_CW_ADDR             (MCU_CONFIG_BASE_ADDR + 0x44)
#define MCU_CW_DATA_LO          (MCU_CONFIG_BASE_ADDR + 0x48)
#define MCU_CW_DAT_HI           (MCU_CONFIG_BASE_ADDR + 0x4c)
#define MCU_RETIRE_PC           (MCU_CONFIG_BASE_ADDR + 0x50)
#define MCU_PROT_SIZE           (MCU_CONFIG_BASE_ADDR + 0x54)
#define MCU_CTIM_CALIB          (MCU_CONFIG_BASE_ADDR + 0x58)
#define MCU_DSK_0               (MCU_CONFIG_BASE_ADDR + 0x60)
#define MCU_DSK_1               (MCU_CONFIG_BASE_ADDR + 0x64)
#define MCU_DSK_2               (MCU_CONFIG_BASE_ADDR + 0x68)
#define MCU_DSK_3               (MCU_CONFIG_BASE_ADDR + 0x6c)
#define MCU_DSK_4               (MCU_CONFIG_BASE_ADDR + 0x70)
#define MCU_DSK_5               (MCU_CONFIG_BASE_ADDR + 0x74)

#define MCU_AP_CMD_JTAG_FLAG            (1 << 16)
#define MCU_AP_CMD_ENCRYPT_FLAG         (1 << 17)
#define MCU_AP_CMD_MCU_PC_FLAG          (1 << 18)
#define MCU_AP_CMD_ACPU_INT_MASK        (0xffff)
#define MCU_AP_CMD_EN_ACPU_INT_MASK     (0xffff)
#define MCU_SP_STATUS_MASK              (0xffff)
#define MCU_SP_STATUS_EN_MASK           (0xffff)

#define MCU_INTERRUPT_0                 (1 << 0)
#define MCU_INTERRUPT_1                 (1 << 1)
#define MCU_INTERRUPT_2                 (1 << 2)
#define MCU_INTERRUPT_3                 (1 << 3)

//=================================================================================================
// IRQ Table

#define MCU_IRQ_DW_UART0        0
#define MCU_IRQ_DW_UART1        1
#define MCU_IRQ_DW_I2C0         2
#define MCU_IRQ_DW_I2C1         3
#define MCU_IRQ_IRR             4
#define MCU_IRQ_RTC             5

#define MCU_IRQ_DW_SPI          8
#define MCU_IRQ_WDT             9
#define MCU_IRQ_COUNTER0        10
#define MCU_IRQ_COUNTER1        11
#define MCU_IRQ_COUNTER2        12
#define MCU_IRQ_COUNTER3        13

#define MCU_IRQ_GPIO0           16
#define MCU_IRQ_GPIO1           17
#define MCU_IRQ_GPIO2           18
#define MCU_IRQ_DMA             19

#define MCU_IRQ_A7_NORM         32
#define MCU_IRQ_A7_FAST         33
#define MCU_IRQ_SNPU            34
#define MCU_IRQ_AUDIO_IN        35
#define MCU_IRQ_A7_TO_MCU       36
#define MCU_IRQ_MTC             37
#define MCU_IRQ_A7_WDT          38
#define MCU_IRQ_DSP             39
#define MCU_IRQ_AUDIO_OUT       40
#define MCU_IRQ_USB_WU          41
#define MCU_IRQ_USB             42
#define MCU_IRQ_USB_DMA         43

//=================================================================================================
// Address Offset

#define MCU_TO_DEV(x) ((unsigned int)x >= 0x40000000 ?\
		(void *)((unsigned int)x - 0x40000000) : (void *)((unsigned int)x - 0x20000000))
#define DEV_TO_MCU(x) ((void *)((unsigned int)x + 0x40000000))

#endif
