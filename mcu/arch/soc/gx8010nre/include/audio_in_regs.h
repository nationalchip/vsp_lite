/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * audio_in_regs.h: MCU AudioIn Register Fields
 *
 */

#ifndef __AUDIO_IN_REGS_H__
#define __AUDIO_IN_REGS_H__

#include <types.h>

//=================================================================================================
// Register Fields
//-------------------------------------------------------------------------------------------------

typedef enum {
    AUDIO_IN_INPUT_SOURCE_AMIC,  /* Analog Microphone */
    AUDIO_IN_INPUT_SOURCE_DMIC,  /* Digital Microphone */
    AUDIO_IN_INPUT_SOURCE_I2S,   /* I2S or TDM Input */
    AUDIO_IN_INPUT_SOURCE_TEST
} AUDIO_IN_INPUT_SOURCE;

typedef enum {
    AUDIO_IN_INPUT_GAIN_1,       /*  0dB */
    AUDIO_IN_INPUT_GAIN_2,       /*  6dB */
    AUDIO_IN_INPUT_GAIN_4,       /* 12dB */
    AUDIO_IN_INPUT_GAIN_8,       /* 18dB */
    AUDIO_IN_INPUT_GAIN_16,      /* 24dB */
    AUDIO_IN_INPUT_GAIN_32,      /* 30dB */
    AUDIO_IN_INPUT_GAIN_64,      /* 36dB */
    AUDIO_IN_INPUT_GAIN_128,     /* 42dB */
    AUDIO_IN_INPUT_GAIN_256,     /* 48dB */
    AUDIO_IN_INPUT_GAIN_512,     /* 54dB */
} AUDIO_IN_INPUT_GAIN;

typedef enum {
    AUDIO_IN_INPUT_FILTER_I2S,
    AUDIO_IN_INPUT_FILTER_DMIC,
    AUDIO_IN_INPUT_FILTER_AMIC,
    AUDIO_IN_INPUT_FILTER_TEST
} AUDIO_IN_INPUT_FILTER;

/* AUDIO_IN_SOURCE_CTRL */
typedef union {
    uint32_t value;
    struct {
        AUDIO_IN_INPUT_SOURCE source_sel:2;
        AUDIO_IN_INPUT_FILTER filter_mag_en:2;
        AUDIO_IN_INPUT_GAIN magnif_sel:4;
        unsigned i2s_channel_sel:3;
        unsigned :1;
        unsigned channel_work_en:8;
        unsigned i2sout_sys_gate_enable:1;
        unsigned else_rst_auto_enable:1;
        unsigned else_gate_auto_enable:1;
        unsigned cic_clk_inv_en:1;
        unsigned mclk_out_sel:1;
        unsigned else_sys_gate_enable:1;
        unsigned i2sin_sys_gate_enable:1;
        unsigned dmic_sys_gate_enable:1;
        unsigned i2sout_test_mode:1;
        unsigned i2sout_soft_rstn:1;
        unsigned dmic_soft_rstn:1;
        unsigned i2s_soft_rstn:1;
    } bits;
} AUDIO_IN_SOURCE_CTRL;

//-------------------------------------------------------------------------------------------------
// AUDIO_IN_CHANNEL_SEL
typedef union {
    uint32_t value;
    struct {
        unsigned channel_0_sel:4;
        unsigned channel_1_sel:4;
        unsigned channel_2_sel:4;
        unsigned channel_3_sel:4;
        unsigned channel_4_sel:4;
        unsigned channel_5_sel:4;
        unsigned channel_6_sel:4;
        unsigned channel_7_sel:4;
    } bits;
} AUDIO_IN_CHANNEL_SEL;

//-------------------------------------------------------------------------------------------------
// AUDIO_IN_AVAD_CTRL
typedef enum {
    AIN_AVAD_FRAME_LENGTH_5MS,
    AIN_AVAD_FRAME_LENGTH_10MS,
    AIN_AVAD_FRAME_LENGTH_20MS,
} AIN_AVAD_FRAME_LENGTH;

// Interval Threshold Value
typedef enum {
    AIN_AVAD_ITV_HARDWARE = 0x00,
    AIN_AVAD_ITV_USER = 0x01,
} AIN_AVAD_ITV_TYPE;

typedef enum {
    AIN_AVAD_READ_ITPARAM_0,
    AIN_AVAD_READ_ITPARAM_1,
    AIN_AVAD_READ_ITPARAM_2,
    AIN_AVAD_READ_ITPARAM_3,
    AIN_AVAD_READ_ITPARAM_4,
    AIN_AVAD_READ_ITPARAM_5,
    AIN_AVAD_READ_ITPARAM_6,
    AIN_AVAD_READ_ITPARAM_7,
    AIN_AVAD_READ_ITPARAM_ECHO_0,
    AIN_AVAD_READ_ITPARAM_ECHO_1,
} AIN_AVAD_READ_ITPARAM_SEL;

typedef enum {
    AIN_AVAD_ECHO_AVAD_MODE_EXTERNAL = 0x00,
    AIN_AVAD_ECHO_AVAD_MODE_INTERNAL = 0x01,
} AIN_AVAD_ECHO_AVAD_MODE;

typedef enum {
    AIN_AVAD_AVAD_MODE_EXTERNAL = 0x00,
    AIN_AVAD_AVAD_MODE_INTERNAL = 0x01,
} AIN_AVAD_AVAD_MODE;

/* AUDIO_IN_AVAD_CTRL */
typedef union {
    uint32_t value;
    struct {
        AIN_AVAD_FRAME_LENGTH frame_mode:2;
        AIN_AVAD_ITV_TYPE it_param_sel:1;
        unsigned zcr_bypass:1;
        AIN_AVAD_READ_ITPARAM_SEL read_itpara_sel:4;
        unsigned set_itpara_sel:4;
        unsigned :4;
        unsigned ch_avad_en:8;
        unsigned echo_ch_avad_en:2;
        unsigned :1;
        AIN_AVAD_ECHO_AVAD_MODE echo_avad_mode:1;
        AIN_AVAD_AVAD_MODE avad_mode:1;
        unsigned avad_bypass:1;
        unsigned amic_avad_rstn_inv_en:1;
        unsigned avad_soft_rstn:1;
    }bits;
} AUDIO_IN_AVAD_CTRL;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_READ_STATE */
typedef union {
    uint32_t value;
    struct {
        unsigned avad_state:8;
        unsigned frame_full_0_1:1;
        unsigned :5;
        unsigned echo_avad_state:2;
        unsigned channel_data_valid_state:8;
        unsigned ref_channel_data_valid_state:2;
        unsigned :6;
    } bits;
} AUDIO_IN_READ_STATE;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_ITL_REG */
typedef union {
    uint32_t value;
    struct {
        unsigned itl_reg:32;
    } bits;
} AUDIO_IN_ITL_REG;

/* AUDIO_IN_ITU_REG */
typedef union {
    uint32_t value;
    struct {
        unsigned itu_reg:32;
    } bits;
} AUDIO_IN_ITU_REG;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_ITL_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned itl_using:32;
    } bits;
} AUDIO_IN_ITL_USING;

/* AUDIO_IN_ITU_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned itu_using:32;
    } bits;
} AUDIO_IN_ITU_USING;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AIN_I2S_ADC_CLK_MODE_SLAVE         = 0x00,
    AIN_I2S_ADC_CLK_MODE_MASTER        = 0x01
} AIN_I2S_ADC_CLK_MODE;

typedef enum bclk_mode {
    AIN_I2S_BIT_CLK_MODE_64FS,
    AIN_I2S_BIT_CLK_MODE_256FS,
} AIN_I2S_BIT_CLK_MODE;

typedef enum {
    AIN_I2S_LR_CLK_MODE_HALF,
    AIN_I2S_LR_CLK_MODE_256FS,
} AIN_I2S_LR_CLK_MODE;

/* AUDIO_IN_I2S_GENCLK */
typedef union {
    uint32_t value;
    struct {
        AIN_I2S_ADC_CLK_MODE adc_clk_mode:1;
        AIN_I2S_BIT_CLK_MODE bit_clk_sel:1;
        AIN_I2S_LR_CLK_MODE lr_clk_sel:1;
        unsigned bit_clk_o_inv_en:1;
        unsigned :28;
    } bits;
} AUDIO_IN_I2S_GENCLK;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AIN_I2S_DATA_FORMAT_I2S,
    AIN_I2S_DATA_FORMAT_LEFT_JUSTIFY,
    AIN_I2S_DATA_FORMAT_RIGHT_JUSTIFY,
    AIN_I2S_DATA_FORMAT_TDM,
} AIN_I2S_DATA_FORMAT;

typedef enum {
    AIN_I2S_PCM_LENGTH_24BIT,
    AIN_I2S_PCM_LENGTH_20BIT,
    AIN_I2S_PCM_LENGTH_16BIT,
    AIN_I2S_PCM_LENGTH_32BIT_ALL,
    AIN_I2S_PCM_LENGTH_32BIT_MID16,
    AIN_I2S_PCM_LENGTH_32BIT_LEFT16,
    AIN_I2S_PCM_LENGTH_32BIT_RIGHT16,
} AIN_I2S_PCM_LENGTH;

typedef enum { // TDM mode work
    AIN_I2S_FSYNC_MODE_LONG            = 0x00,
    AIN_I2S_FSYNC_MODE_SHORT           = 0x01,
} AIN_I2S_FSYNC_MODE;

typedef enum {
    AIN_I2S_SOURCE_SEL_OUT_ADC,
    AIN_I2S_SOURCE_SEL_AUDIO_PLAY,
    AIN_I2S_SOURCE_SEL_OUT_I2S
} AIN_I2S_SOURCE_SEL;

/* AUDIO_IN_I2S_ADCINFO */
typedef union {
    uint32_t value;
    struct {
        AIN_I2S_DATA_FORMAT data_format:2;
        unsigned :3;
        unsigned bit_clk_i_inv_en:1;
        unsigned second_sdata_en:1;
        AIN_I2S_FSYNC_MODE fsync_mode:1;
        unsigned lrclk_i_inv_en:1;
        AIN_I2S_PCM_LENGTH pcm_length:3;
        unsigned :18;
        AIN_I2S_SOURCE_SEL i2s_source_sel:2;
    } bits;
} AUDIO_IN_I2S_ADCINFO;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AIN_PCM_WRITE_MODE_PINGPANG = 0x00,
    AIN_PCM_WRITE_MODE_LOOP     = 0x01,
} AIN_PCM_WRITE_MODE;

typedef enum {
    AIN_PCM_ENDIAN_LITTLE = 0x00,
    AIN_PCM_ENDIAN_BIG    = 0x01
} AIN_PCM_ENDIAN;

typedef enum pcm_output_fr{
    AIN_PCM_SAMPLE_RATE_16KHZ   = 0x00,
    AIN_PCM_SAMPLE_RATE_08KHZ   = 0x01,
    AIN_PCM_SAMPLE_RATE_48KHZ   = 0x02,
    AIN_PCM_SAMPLE_RATE_RESERVED,
} AIN_PCM_SAMPLE_RATE;

/* AUDIO_IN_W_PCM_CTRL */
typedef union {
    uint32_t value;
    struct {
        AIN_PCM_WRITE_MODE write_mode:1;
        AIN_PCM_ENDIAN pcm_endian:1;
        unsigned w_echo_channel_max_sel:2;
        unsigned write_channel_max_sel:3;
        unsigned :1;
        AIN_PCM_SAMPLE_RATE pcm_fs_sel:2;
        unsigned :21;
        unsigned write_soft_rstn:1;
    } bits;
} AUDIO_IN_W_PCM_CTRL;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_PCM_NUM */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_num:32;
    } bits;
} AUDIO_IN_PCM_NUM;

/* AUDIO_IN_PCM_FRAME0_SADDR */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_frame0_saddr:32;
    } bits;
} AUDIO_IN_PCM_FRAME0_SADDR;

/* AUDIO_IN_PCM_FRAME1_SADDR */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_frame1_saddr:32;
    } bits;
} AUDIO_IN_PCM_FRAME1_SADDR;

/* AUDIO_IN_PCM_FRAME_SIZE */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_frame_size:32;
    } bits;
} AUDIO_IN_PCM_FRAME_SIZE;

/* AUDIO_IN_PCM_BUFFER_SADDR */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_buffer_saddr:32;
    } bits;
} AUDIO_IN_PCM_BUFFER_SADDR;

/* AUDIO_IN_PCM_BUFFER_SIZE */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_buffer_size:32;
    } bits;
} AUDIO_IN_PCM_BUFFER_SIZE;

/* AUDIO_IN_PCM_SDC_ADDR */
typedef union {
    uint32_t value;
    struct {
        unsigned pcm_sdc_addr:32;
    } bits;
} AUDIO_IN_PCM_SDC_ADDR;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_INT_EN */
typedef union {
    uint32_t value;
    struct {
        unsigned avad_done_int:1;
        unsigned pcm_done_int:1;
        unsigned axi_idle_int:1;
        unsigned agc_down_done:1;
        unsigned agc_up_done:1;
        unsigned agc_skip_done:1;
        unsigned :26;
    } bits;
} AUDIO_IN_INT_EN;

/* AUDIO_IN_INT */
typedef union {
    uint32_t value;
    struct {
        unsigned avad_done_int:1;
        unsigned pcm_done_int:1;
        unsigned axi_idle_int:1;
        unsigned agc_down_done:1;
        unsigned agc_up_done:1;
        unsigned agc_skip_done:1;
        unsigned :2;
        unsigned reg_info_fresh:1;
        unsigned cpu_stop_ain_req:1;
        unsigned cpu_rerun_ain_req:1;
        unsigned :21;
    } bits;
} AUDIO_IN_INT_STATE;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AIN_ECHO_SOURCE_SEL_AMIC,
    AIN_ECHO_SOURCE_SEL_DMIC,
    AIN_ECHO_SOURCE_SEL_I2S,
} AIN_ECHO_SOURCE_SEL;

typedef enum {
    AIN_ECHO_FILTER_I2S,
    AIN_ECHO_FILTER_DMIC,
    AIN_ECHO_FILTER_AMIC,
    AIN_ECHO_FILTER_TEST
} AIN_ECHO_FILTER;

/* AUDIO_IN_EHCO_SOURCE_CTRL */
typedef union {
    uint32_t value;
    struct {
        AIN_ECHO_SOURCE_SEL echo_source_sel:2;
        AIN_ECHO_FILTER echo_filter_mag_en:2;
        unsigned echo_magnif_sel:4;
        unsigned echo_work_en:2;
        unsigned :6;
        unsigned echo_channel_0_sel:4;
        unsigned echo_channel_1_sel:4;
        unsigned :8;
    } bits;
} AUDIO_IN_ECHO_SOURCE_CTRL;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_IMX_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned imx_using:32;
    } bits;
} AUDIO_IN_IMX_USING;

/* AUDIO_IN_IMN_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned imn_using:32;
    } bits;
} AUDIO_IN_IMN_USING;

/* AUDIO_IN_ENG_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned eng_using:32;
    } bits;
} AUDIO_IN_ENG_USING;

/* AUDIO_IN_ZCR0_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned zcr0_using:32;
    } bits;
} AUDIO_IN_ZCR0_USING;

/* AUDIO_IN_ZCR_USING */
typedef union {
    uint32_t value;
    struct {
        unsigned zcr_using:32;
    } bits;
} AUDIO_IN_ZCR_USING;

/* AUDIO_IN_IZCT_REG */
typedef union {
    uint32_t value;
    struct {
        unsigned izct_reg:32;
    } bits;
} AUDIO_IN_IZCT_REG;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AIN_DC_FILTER_SEL_INTERNAL,
    AIN_DC_FILTER_SEL_SOFT,
    AIN_DC_FILTER_SEL_BYPASS,
} AIN_DC_FILTER_SEL;

typedef enum {
    AIN_DC_ECHO_FILTER_SEL_INTERNAL,
    AIN_DC_ECHO_FILTER_SEL_SOFT,
    AIN_DC_ECHO_FILTER_SEL_BYPASS,
} AIN_DC_ECHO_FILTER_SEL;

typedef enum {
    AIN_DC_SET_OFFSET_SEL_ALL,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_0,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_1,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_2,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_3,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_4,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_5,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_6,
    AIN_DC_SET_OFFSET_SEL_CHANNEL_7,
    AIN_DC_SET_OFFSET_SEL_ECHO_0,
    AIN_DC_SET_OFFSET_SEL_ECHO_1,
} AIN_DC_SET_OFFSET_SEL;

/* AUDIO_IN_DC_CTRL */
typedef union {
    uint32_t value;
    struct {
        AIN_DC_FILTER_SEL dc_filter_sel:2;
        AIN_DC_ECHO_FILTER_SEL echo_dc_filter_sel:2;
        AIN_DC_SET_OFFSET_SEL set_dc_offset_sel:4;
        unsigned post_dc_filter_en:1;
        unsigned echo_post_dc_filter_en:1;
        unsigned :22;
    } bits;
} AUDIO_IN_DC_CTRL;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_DC_OFFSET */
typedef union {
    uint32_t value;
    struct {
        unsigned dc_offset:32;
    } bits;
} AUDIO_IN_DC_OFFSET;

/* AUDIO_IN_PCM_VOL_CTRL 0 - 4 */
typedef union {
    uint32_t value;
    struct {
        unsigned ch0_vol_level:12;
        unsigned :4;
        unsigned ch1_vol_level:12;
        unsigned :4;
    } bits;
} AUDIO_IN_PCM_VOL_CTRL;

/* AUDIO_IN_ITL_INTER */
typedef union {
    uint32_t value;
    struct {
        unsigned itl_inter:32;
    } bits;
} AUDIO_IN_ITL_INTER;

/* AUDIO_IN_ITU_INTER */
typedef union {
    uint32_t value;
    struct {
        unsigned itu_inter:32;
    } bits;
} AUDIO_IN_ITU_INTER;

//-------------------------------------------------------------------------------------------------

typedef enum {
    AIN_AGC_SOFT,
    AIN_AGC_HARD,
} AIN_AGC_UPDATA_MODE;

typedef enum {
    AIN_AGC_AMIC_AGC_STEP_2DB,
    AIN_AGC_AMIC_AGC_STEP_4DB,
    AIN_AGC_AMIC_AGC_STEP_6DB,
    AIN_AGC_AMIC_AGC_STEP_8DB,
    AIN_AGC_AMIC_AGC_STEP_10DB,
    AIN_AGC_AMIC_AGC_STEP_12DB,
} AIN_AGC_AMIC_AGC_STEP;

/* AUDIO_IN_AGC_CTRL */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_ch_sel:4;
        AIN_AGC_UPDATA_MODE agc_updata_mode:1;
        unsigned :3;
        unsigned min_agc:8;
        unsigned max_agc:8;
        AIN_AGC_AMIC_AGC_STEP amic_agc_step:3;
        unsigned :2;
        unsigned agc_sys_gate_rst_auto:1;
        unsigned agc_sys_gate_enable:1;
        unsigned agc_soft_rstn:1;
    } bits;
} AUDIO_IN_AGC_CTRL;

//-------------------------------------------------------------------------------------------------

/* AUDIO_IN_AGC_FILTER_PARA */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_para_a:16;
        unsigned agc_para_n:16;
    } bits;
} AUDIO_IN_AGC_FILTER_PARA;

/* AUDIO_IN_AGC_SAMPLE_NUM */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_sample_num:32;
    } bits;
} AUDIO_IN_AGC_SAMPLE_NUM;

/* AUDIO_IN_AGC_ENG_HIGH */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_eng_high:32;
    } bits;
} AUDIO_IN_AGC_ENG_HIGH;

/* AUDIO_IN_AGC_ENG_LOW */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_eng_low:32;
    } bits;
} AUDIO_IN_AGC_ENG_LOW;

/* AUDIO_IN_AGC_ENG */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_eng:32;
    } bits;
} AUDIO_IN_AGC_ENG;

/* AUDIO_IN_AGC_UPDATE_CTRL */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_updata_amic_en:8;
        unsigned agc_updata_group_en:2;
        unsigned :22;
    } bits;
} AUDIO_IN_AGC_UPDATE_CTRL;

/* AUDIO_IN_AGC_STATE */
typedef union {
    uint32_t value;
    struct {
        unsigned agc_pga_gain:4;
        unsigned agc_boost_gain:2;
        unsigned agc_boost_gain_by_pass:1;
        unsigned agc_boost_gain_pd:1;
        unsigned agc_magnif_sel:4;
        unsigned agc_echo_magnif_sel:4;
        unsigned filter_overflow:1;
        unsigned :15;
    } bits;
} AUDIO_IN_AGC_STATE;

/* AUDIO_IN_GXADC_CTRL */
typedef union {
    uint32_t value;
    struct {
        unsigned gxadc_mic_en_after_avad:8;
        unsigned pga_gain_after_avad:4;
        unsigned boost_gain_after_avad:2;
        unsigned gxadc_open_auto:1;
        unsigned :17;
    } bits;
} AUDIO_IN_GXADC_CTRL;

/* AUDIO_IN_AVAD_PARA */
typedef union {
    uint32_t value;
    struct {
        unsigned avad_para:16;
        unsigned :16;
    } bits;
} AUDIO_IN_AVAD_PARA;

//==============================GXADC==============================//
/* AUDIO_IN_ADC_BIAS_GAIN */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_cm_bias_pd:8;
        unsigned adc_micbias_pd:8;
        unsigned adc_boost_gain_pd:8;
        unsigned adc_boost_gain_bypass:8;
    } bits;
} AUDIO_IN_ADC_BIAS_GAIN;

/* AUDIO_IN_ADC_PGA_PD */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_pga_pd:8;
        unsigned adc_pga_all_bypass:8;
        unsigned adc_pd:8;
        unsigned adc_vad_pd:8;
    } bits;
} AUDIO_IN_ADC_PGA_PD;

/* AUDIO_IN_ADC_LDO_PD */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_ldo_pd:1;
        unsigned :31;
    } bits;
} AUDIO_IN_ADC_LDO_PD;

/* AUDIO_IN_ADC_BOOST_GAIN */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_boost_gain:16;
        unsigned :16;
    } bits;
} AUDIO_IN_ADC_BOOST_GAIN;

/* AUDIO_IN_ADC_PGA_GAIN */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_pga_gain:32;
    } bits;
} AUDIO_IN_ADC_PGA_GAIN;

/* AUDIO_IN_ADC_SET */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_set:32;
    } bits;
} AUDIO_IN_ADC_SET;

/* AUDIO_IN_ADC_OUT_INV */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_out_inv:8;
        unsigned :24;
    } bits;
} AUDIO_IN_ADC_OUT_INV;

/* AUDIO_IN_ADC_CLK_INV */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_clk_inv:1;
        unsigned :31;
    } bits;
} AUDIO_IN_ADC_CLK_INV;

/* AUDIO_IN_ADC_SEL_BIAS */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_sel_bias:8;
        unsigned :24;
    } bits;
} AUDIO_IN_ADC_SEL_BIAS;

/* AUDIO_IN_ADC_SEL_EXCOM */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_sel_excom:1;
        unsigned :31;
    } bits;
} AUDIO_IN_ADC_SEL_EXCOM;

/* AUDIO_IN_ADC_SEL_EXVBG */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_sel_exvbg:1;
        unsigned :31;
    } bits;
} AUDIO_IN_ADC_SEL_EXVBG;

/* AUDIO_IN_ADC_VAD_SH */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_vad_sh:16;
        unsigned :16;
    } bits;
} AUDIO_IN_ADC_VAD_SH;

/* AUDIO_IN_ADC_LDO_SET */
typedef union {
    unsigned int value;
    struct {
        unsigned ldo_voltage:3;
        unsigned chopper_clk:3;
        unsigned chopper_en:1;
        unsigned bandgap_pd:1;
        unsigned :24;
    } bits;
} AUDIO_IN_ADC_LDO_SET;

/* AUDIO_IN_ADC_RSTN */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_rstn:1;
        unsigned :31;
    } bits;
} AUDIO_IN_ADC_RSTN;

/* AUDIO_IN_ADC_AVAD_STATUS */
typedef union {
    unsigned int value;
    struct {
        unsigned adc_avad_status:10;
        unsigned :22;
    } bits;
} AUDIO_IN_ADC_AVAD_STATUS;

/* AUDIO_IN_ADC_VAD_RESTORE */
typedef union {
    unsigned int value;
    struct {
        unsigned vad_restore:8;
        unsigned :24;
    } bits;
} AUDIO_IN_ADC_VAD_RESTORE;

/* AUDIO_IN_ADC_ANA_BIAS_PD */
typedef union {
    unsigned int value;
    struct {
        unsigned ana_bias_pd:8;
        unsigned :24;
    } bits;
} AUDIO_IN_ADC_ANA_BIAS_PD;

//=================================================================================================

typedef struct {
    AUDIO_IN_SOURCE_CTRL        source_ctrl;           // 0x0000
    AUDIO_IN_CHANNEL_SEL        channel_sel;
    AUDIO_IN_AVAD_CTRL          avad_ctrl;
    AUDIO_IN_READ_STATE         read_state;
    AUDIO_IN_ITL_REG            itl_reg;               // 0x0010
    AUDIO_IN_ITU_REG            itu_reg;
    AUDIO_IN_ITL_USING          itl_using;
    AUDIO_IN_ITU_USING          itu_using;
    AUDIO_IN_I2S_GENCLK         i2s_genclk;            // 0x0020
    AUDIO_IN_I2S_ADCINFO        i2s_adcinfo;
    AUDIO_IN_W_PCM_CTRL         w_pcm_ctrl;
    AUDIO_IN_PCM_NUM            pcm_num;
    AUDIO_IN_PCM_FRAME0_SADDR   pcm_frame0_saddr;      // 0x0030
    AUDIO_IN_PCM_FRAME1_SADDR   pcm_frame1_saddr;
    AUDIO_IN_PCM_FRAME_SIZE     pcm_frame_size;
    AUDIO_IN_PCM_BUFFER_SADDR   pcm_buffer_saddr;
    AUDIO_IN_PCM_BUFFER_SIZE    pcm_buffer_size;       // 0x0040
    AUDIO_IN_PCM_SDC_ADDR       pcm_sdc_addr;
    AUDIO_IN_INT_EN             int_en;
    AUDIO_IN_INT_STATE          int_state;
    AUDIO_IN_ECHO_SOURCE_CTRL   echo_source_ctrl;      // 0x0050
    AUDIO_IN_IMX_USING          imx_using;
    AUDIO_IN_IMN_USING          imn_using;
    AUDIO_IN_ENG_USING          eng_using;
    AUDIO_IN_ZCR0_USING         zcr0_using;            // 0x0060
    AUDIO_IN_ZCR_USING          zcr_using;
    AUDIO_IN_IZCT_REG           izct_reg;
    AUDIO_IN_DC_CTRL            dc_ctrl;
    AUDIO_IN_DC_OFFSET          dc_offset;             // 0x0070

    AUDIO_IN_PCM_VOL_CTRL       pcm_vol_ctrl[5];       // 0X0080

    AUDIO_IN_ITL_INTER          itl_inter;
    AUDIO_IN_ITU_INTER          itu_inter;
    AUDIO_IN_AGC_CTRL           agc_ctrl;              // 0x0090
    AUDIO_IN_AGC_FILTER_PARA    agc_filter_para;
    AUDIO_IN_AGC_SAMPLE_NUM     agc_sample_num;
    AUDIO_IN_AGC_ENG_HIGH       agc_eng_high;
    AUDIO_IN_AGC_ENG_LOW        agc_eng_low;           // 0x00A0
    AUDIO_IN_AGC_ENG            agc_eng;
    AUDIO_IN_AGC_UPDATE_CTRL    agc_update_ctrl;
    AUDIO_IN_AGC_STATE          agc_state;
    AUDIO_IN_GXADC_CTRL         gxadc_ctrl;            // 0x00B0
    AUDIO_IN_AVAD_PARA          avad_para;
    unsigned int                RESERVED[2];

    AUDIO_IN_ADC_BIAS_GAIN      adc_bias_gain;         // 0x00C0
    AUDIO_IN_ADC_PGA_PD         adc_pga_pd;
    AUDIO_IN_ADC_LDO_PD         adc_ldo_pd;
    AUDIO_IN_ADC_BOOST_GAIN     adc_boost_gain;
    AUDIO_IN_ADC_PGA_GAIN       adc_pga_gain;          // 0x00D0
    AUDIO_IN_ADC_SET            adc_set;
    AUDIO_IN_ADC_OUT_INV        adc_out_inv;
    AUDIO_IN_ADC_CLK_INV        adc_clk_inv;
    AUDIO_IN_ADC_SEL_BIAS       adc_sel_bias;          // 0x00E0
    AUDIO_IN_ADC_SEL_EXCOM      adc_sel_excom;
    AUDIO_IN_ADC_SEL_EXVBG      adc_sel_exvbg;
    AUDIO_IN_ADC_VAD_SH         adc_vad_sh;
    AUDIO_IN_ADC_LDO_SET        adc_ldo_set;           // 0x00F0
    AUDIO_IN_ADC_RSTN           adc_rstn;
    AUDIO_IN_ADC_AVAD_STATUS    adc_avad_status;
    AUDIO_IN_ADC_VAD_RESTORE    adc_vad_restore;
    AUDIO_IN_ADC_ANA_BIAS_PD    adc_ana_bias_pd;       // 0x0100
} AUDIO_IN_REGS;

#endif /* __AUDIO_IN_REGS_H__ */
