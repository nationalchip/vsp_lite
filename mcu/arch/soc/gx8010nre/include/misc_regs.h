/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * misc_regs.h: MCU Config Register Fields
 *
 */

#ifndef __MISC_REGS_H__
#define __MISC_REGS_H__

#include <types.h>
#include "base_addr.h"

//=================================================================================================

#define MCU_REG_BASE_RESET        (MCU_REG_BASE_CONFIG + 0x0)
#define MCU_REG_BASE_CLOCK        (MCU_REG_BASE_CONFIG + 0x18)

#define MISC_REG_DTO1_CONFIG           (MCU_REG_BASE_CONFIG + 0x28) // CPU
#define MISC_REG_DTO2_CONFIG           (MCU_REG_BASE_CONFIG + 0x2C) // AP2_0
#define MISC_REG_DTO3_CONFIG           (MCU_REG_BASE_CONFIG + 0x30) // APB2
#define MISC_REG_DTO4_CONFIG           (MCU_REG_BASE_CONFIG + 0x34) // SNPU
#define MISC_REG_DTO5_CONFIG           (MCU_REG_BASE_CONFIG + 0x38) // DSP
#define MISC_REG_DTO6_CONFIG           (MCU_REG_BASE_CONFIG + 0x3C) // SECURE
#define MISC_REG_DTO7_CONFIG           (MCU_REG_BASE_CONFIG + 0x40) // AUDIO SRL
#define MISC_REG_DTO8_CONFIG           (MCU_REG_BASE_CONFIG + 0x44) // AUDIO IN
#define MISC_REG_DTO9_CONFIG           (MCU_REG_BASE_CONFIG + 0x48) // SRAM SDC

#define MISC_REG_PLL1_CONFIG               (MCU_REG_BASE_CONFIG + 0xC0) // Audio PLL

#define MISC_REG_PLL3_CONFIG               (MCU_REG_BASE_CONFIG + 0xC8) // DTO PLL
#define MISC_REG_PLL4_CONFIG               (MCU_REG_BASE_CONFIG + 0xCC) // ARM
#define MISC_REG_PLL9_CONFIG               (MCU_REG_BASE_CONFIG + 0xE0) // DDR
#define MISC_REG_USB4_CONFIG               (MCU_REG_BASE_CONFIG + 0x110) // USB SLAVE

#define MISC_REG_PIN_FUNCTION_SEL_0    (MCU_REG_BASE_CONFIG + 0x140)
#define MISC_REG_PIN_FUNCTION_SEL_1    (MCU_REG_BASE_CONFIG + 0x144)
#define MISC_REG_PIN_FUNCTION_SEL_2    (MCU_REG_BASE_CONFIG + 0x148)
#define MISC_REG_PIN_FUNCTION_SEL_3    (MCU_REG_BASE_CONFIG + 0x14C)

#define MISC_REG_DW_SPI_CONFIG_ADDR     (MCU_REG_BASE_CONFIG + 0x168)

#define MISC_REG_SOURCE_SEL             (MCU_REG_BASE_CONFIG + 0x170)
#define MISC_REG_CLOCK_DIV_CONFIG2      (MCU_REG_BASE_CONFIG + 0x178)

#define MISC_REG_CHIP_ID                (MCU_REG_BASE_CONFIG + 0x184)

#define MISC_REG_AUDIO_CODEC_DATA       (MCU_REG_BASE_CONFIG + 0x1a0)
#define MISC_REG_AUDIO_CODEC_CONTROL    (MCU_REG_BASE_CONFIG + 0x1a4)

#define MISC_REG_AUDIO_ADC              (MCU_REG_BASE_CONFIG + 0x210)

#define MISC_REG_BOOT_MODE              (MCU_REG_BASE_CONFIG + 0x1f8)

//=================================================================================================

// MEPG_CLD_RST_NORM
// MEPG_CLD_RST_1SET
// MEPG_CLD_RST_1CLR
typedef union {
    uint32_t value;
    struct {
        unsigned snpu_cold_rst_n:1;
        unsigned audio_play_cold_rst_n:1;
        unsigned audio_in_cold_rst_n:1;
        unsigned dsp_cold_rst_n:1;
        unsigned dsp_debug_cold_rst_n:1;
        unsigned dsp_ocd_cold_rst_n:1;
        unsigned otp_cold_rst_n:1;
        unsigned mtc_cold_rst_n:1;
        unsigned secure_cold_rst_n:1;
        unsigned :1;
        unsigned usb_slave_cold_rst_n:1;
        unsigned ncpureset_a7_cold_rst_n:1;
        unsigned ndbgreset_a7_cold_rst_n:1;
        unsigned nscureset_a7_cold_rst_n:1;
        unsigned nporeset_a7_cold_rst_n:1;
        unsigned nsocareset_a7_cold_rst_n:1;
        unsigned nsocdbgreset_a7_cold_rst_n:1;
        unsigned clock_hresetn_a7_cold_rst_n:1;
        unsigned nrst_raw_a7_cold_rst_n:1;
        unsigned :1;
        unsigned sdc_cold_rst_n:1;
        unsigned :11;
    } bits;
} MEPG_CLD_RST;

// MEPG_HOT_RST_NORM
// MEPG_HOT_RST_1SET
// MEPG_HOT_RST_1CLR
typedef union {
    uint32_t value;
    struct {
        unsigned snpu_hot_rst_n:1;
        unsigned audio_play_hot_rst_n:1;
        unsigned audio_in_hot_rst_n:1;
        unsigned dsp_hot_rst_n:1;
        unsigned dsp_debug_hot_rst_n:1;
        unsigned dsp_ocd_hot_rst_n:1;
        unsigned otp_hot_rst_n:1;
        unsigned mtc_hot_rst_n:1;
        unsigned secure_hot_rst_n:1;
        unsigned :1;
        unsigned usb_slave_hot_rst_n:1;
        unsigned ncpureset_a7_hot_rst_n:1;
        unsigned ndbgreset_a7_hot_rst_n:1;
        unsigned nscureset_a7_hot_rst_n:1;
        unsigned nporeset_a7_hot_rst_n:1;
        unsigned nsocareset_a7_hot_rst_n:1;
        unsigned nsocdbgreset_a7_hot_rst_n:1;
        unsigned clock_hresetn_a7_hot_rst_n:1;
        unsigned nrst_raw_a7_hot_rst_n:1;
        unsigned :1;
        unsigned sdc_hot_rst_n:1;
        unsigned :11;
    } bits;
} MEPG_HOT_RST;

typedef struct {
    MEPG_CLD_RST mepg_cld_rst_norm;
    MEPG_CLD_RST mepg_cld_rst_1set;
    MEPG_CLD_RST mepg_cld_rst_1clr;
    MEPG_HOT_RST mepg_hot_rst_norm;
    MEPG_HOT_RST mepg_hot_rst_1set;
    MEPG_HOT_RST mepg_hot_rst_1clr;
} RESET_REGS;

//=================================================================================================

typedef union {
    uint32_t value;
    struct {
        unsigned :1;
        unsigned clock_apb2_0_en:1;
        unsigned clock_apb2_uart_en:1;
        unsigned clock_snpu_en:1;
        unsigned clock_dsp_en:1;
        unsigned clock_secure_ctrl_en:1;
        unsigned clock_audio_play_en:1;
        unsigned clock_audio_lodac_en:1;
        unsigned clock_audio_in_en:1;
        unsigned clock_srl_audio_play_en:1;
        unsigned clock_sram_sdc_controller_en:1;
        unsigned sram_sdc_controller_en:1;
        unsigned clock_sram_sdc_apu_en:1;
        unsigned clock_sram_sdc_snpu_en:1;
        unsigned clock_sram_sdc_dsp_en:1;
        unsigned clock_sram_sdc_audio_in_en:1;
        unsigned clock_sram_sdc_usb_en:1;
        unsigned clock_usbslave_phy_en:1;
        unsigned :1;
        unsigned clock_ahb_usb_en:1;
        unsigned audio_in_clk_en:1;
        unsigned :11;
    } bits;
} MEPG_CLK_INHIBIT;

typedef union {
    uint32_t value;
    struct {
        unsigned :4;                            //  0 -  3
        unsigned cpu_clk_div_ratio_ahb:4;       //  4 -  7
        unsigned arm_clk_div_ratio_scpu:4;      //  8 - 11
        unsigned :4;                            // 12 - 15
        unsigned audio_in_clk_div_ratio:6;      // 16 - 21
        unsigned audio_in_clk_div_load_en:1;    // 22
        unsigned audio_in_clk_div_rstn:4;       // 23
        unsigned audio_lodac_clk_div_ratio:6;   // 24 - 29
        unsigned audio_lodac_clk_div_load_en:1; // 30
        unsigned audio_lodac_clk_div_rstn:1;    // 31
    } bits;
} CLOCK_DIV_CONFIG;

typedef union {
    uint32_t value;
    struct {
        unsigned usbphy_clk_div_ratio:10;   //  0 -  9
        unsigned usbphy_clk_div_load_en:1;  // 10
        unsigned usbphy_clk_div_rstn:1;     // 11
        unsigned :20;
    } bits;
} CLOCK_DIV_CONFIG2;

typedef union {
    uint32_t value;
    struct {
        unsigned step:30;
        unsigned load:1;
        unsigned rst:1;
    } bits;
} DTO_CONFIG;

typedef struct {
    MEPG_CLK_INHIBIT mepg_clk_inhibit_norm;
    MEPG_CLK_INHIBIT mepg_clk_inhibit_1set;
    MEPG_CLK_INHIBIT mepg_clk_inhibit_1clr;
    DTO_CONFIG dto[16];
} CLOCK_REGS;

//=================================================================================================

// PLL...

typedef union {
    uint32_t value;
    struct {
        unsigned _00:1;
        unsigned _01:1;
        unsigned _02:1;
        unsigned _03:1;
        unsigned _04:1;
        unsigned _05:1;
        unsigned _06:1;
        unsigned _07:1;
        unsigned _08:1;
        unsigned _09:1;
        unsigned _10:1;
        unsigned _11:1;
        unsigned _12:1;
        unsigned _13:1;
        unsigned _14:1;
        unsigned _15:1;
        unsigned _16:1;
        unsigned _17:1;
        unsigned _18:1;
        unsigned _19:1;
        unsigned _20:1;
        unsigned _21:1;
        unsigned _22:1;
        unsigned _23:1;
        unsigned _24:1;
        unsigned _25:1;
        unsigned _26:1;
        unsigned _27:1;
        unsigned _28:1;
        unsigned _29:1;
        unsigned _30:1;
        unsigned _31:1;
    };
} PIN_FUNCTION_SEL;

//=================================================================================================
typedef union {
    uint32_t value;
    struct {
        unsigned clock_scpu_source:1;               // 0, Clock start; 1, DTO Clock
        unsigned clock_apb2_peripheral_source:1;    // 0, clock_start; 1, clock_DTO_APB2_0
        unsigned clock_apb2_uart_source:1;          // 0, clock_start; 1, clock_DTO_uart
        unsigned clock_snpu_source:1;               // 0, clock_start; 1, clock_DTO_snpu
        unsigned :1;
        unsigned clock_secure_ctrl_source:1;        // 0, clock_start; 1, clock_DIV_pixel_double
        unsigned clock_audio_lodec_source:1;        // 0, clock_start; 1, clock_DIV_lodec
        unsigned clock_audio_in_source1:1;          // 0, audio_clock_DIV; 1, audio_clock_DTO
        unsigned clock_audio_in_source2:1;          // 0, clock_start; 1, audio_in_mux_out
        unsigned clock_srl_audio_play_source:1;     // 0, clock_start; 1, clock_DTO_audio_play
        unsigned clock_sdc_source:1;                // 0, clock_start; 1, clock_DTO_sram
        unsigned clock_usbslave_phy_source:1;       // 0, clock_start; 1, clock_usb_phy_raw
        unsigned clock_scpu_usb_source:1;           // 0, clock_start; 1, clock_DTO_usb_source
        unsigned :3;
        unsigned clock_dsp_source:1;                // 0, clock_start; 1, clock_dsp_raw
        unsigned clock_dsp_pll_source:1;            // 0, audio_DIV_dsp; 1, DTO_dsp_clock
        unsigned :12;
        unsigned arm_clk_config_source:1;           // 0, clock_start; 1, clock_pll_DTO_ARM
        unsigned arm_clk_source_scpu:1;             // 0, PLL_ARM 由 ARM 配置; 1, PLL_ARM 由 CK 配置
    } bits;
} SOURCE_SEL;

typedef union {
    uint32_t value;
    struct {
        unsigned chip_id_version_h:14;
        unsigned chip_id_name:16;
        unsigned chip_id_version_l:2;
    } bits;
} CHIP_ID;

//=================================================================================================

typedef union {
    uint32_t value;
    struct {
        unsigned audio_codec_data:8;
        unsigned :24;
    };
} AUDIO_CODEC_DATA;

typedef union {
    uint32_t value;
    struct {
        unsigned audio_dac_rstn:1;                  // 音频 DA SFR 接口的复位使能，0 有效
        unsigned audio_dac_sfrclk:1;                // 音频 DA SFR 接口的时钟输入
        unsigned mc_load:1;                         // 音频 DA SFR 接口写使能，1 有效
        unsigned mc_readstr:1;                      // 音频 DA SFR 读使能，1 有效
        unsigned audio_dac_datain:8;                // 音频 DA SFR 接口控制数据
        unsigned audio_dac_addr:8;                  // 音频 DA SFR 接口控制地址
        unsigned :12;
    };
} AUDIO_CODEC_CONTROL;

//=================================================================================================

typedef union {
    uint32_t value;
    struct {
        unsigned scpu_tsvalue_31_00:32;
    };
} TIME_COUNT0;

typedef union {
    uint32_t value;
    struct {
        unsigned scpu_tsvalue_63_32:32;
    };
} TIME_COUNT1;

//=================================================================================================

typedef union {
    uint32_t value;
    struct {
        unsigned :6;               //  0 ~  5
        unsigned usb_pll_en:1;     //  6
        unsigned :21;              //  7 ~ 27
        unsigned usb_phy_reset:1;  // 28
        unsigned :3;               // 29 ~ 31
    } bits;
} USB4_CONFIG;

//=================================================================================================
enum {
    BOOT_MODE_UART       = 0x01,
    BOOT_MODE_USB_HIGH   = 0x01,
    BOOT_MODE_USB_FULL   = 0x03,
    BOOT_MODE_SPI_NOR_3B = 0x01,
    BOOT_MODE_SPI_NOR_4B = 0x11,
    BOOT_MODE_SPI_NAND   = 0x04,
};

typedef union {
    uint32_t value;
    struct {
        unsigned int uart_boot:1;
        unsigned int :3;
        unsigned int usb_boot:2;
        unsigned int :2;
        unsigned int flash_boot:8;
        unsigned int :16;
    };
} BOOT_MODE;

#endif /* __MISC_REGS_H__ */
