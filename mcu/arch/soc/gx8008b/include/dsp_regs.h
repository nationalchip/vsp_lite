/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 *
 * dsp_regs.h: MCU's DSP Registers
 *
 */

#ifndef __DSP_REGS_H__
#define __DSP_REGS_H__

#include <types.h>

typedef union {
    uint32_t value;
    struct {
        unsigned :4;
        unsigned vsp_flag:1;
        unsigned :27;
    };
} MCU_DSP_INT;

typedef union {
    uint32_t value;
    struct {
        unsigned :4;
        unsigned vsp_flag:1;
        unsigned :26;
        unsigned p_wait_mode:1;
    };
} DSP_MCU_INT;

typedef union {
    uint32_t value;
    struct {
        unsigned runstall:1;
        unsigned :31;
    };
} DSP_RUNSTALL;

typedef union {
    uint32_t value;
    struct {
        unsigned gpio_in:32;
    };
} DSP_GPIO_IN;

typedef union {
    uint32_t value;
    struct {
        unsigned is_dsp_sleep:1;
        unsigned :31;
    };
} PWAIT_MODE;

typedef union {
    uint32_t value;
    struct {
        unsigned dsp_reg:32;
    };
} DSP_REG;

typedef union {
    uint32_t value;
    struct {
        unsigned mcu_reg:32;
    };
} MCU_REG;

typedef union {
    uint32_t value;
    struct {
        unsigned ocd_halt_on_reset:1;
        unsigned :31;
    };
} DSP_OCDHALT;

typedef union {
    uint32_t value;
    struct {
        unsigned xocdmode:1;
        unsigned :31;
    };
} DSP_XOCDMODE;

typedef struct {
    MCU_DSP_INT         mcu_dsp_int;
    MCU_DSP_INT         mcu_dsp_en;
    MCU_DSP_INT         mcu_dsp_clear;
    DSP_MCU_INT         dsp_mcu_int;
    DSP_MCU_INT         dsp_mcu_en;
    DSP_MCU_INT         dsp_mcu_clear;
    DSP_RUNSTALL        dsp_runstall;
    DSP_GPIO_IN         dsp_gpio_in;
    PWAIT_MODE          pwrite_mode;
    DSP_REG             dsp_reg[10];
    MCU_REG             mcu_reg[10];
    DSP_OCDHALT         dsp_ocdhalt;
    DSP_XOCDMODE        dsp_xocdmode;
} DSP_REGS;

#endif  /* __DSP_REGS_H__ */
