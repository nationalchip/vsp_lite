/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_upgrade.c: vsp upgrade function
 *
 */

#include <stdio.h>
#include <common.h>

#include <cpu_regs.h>
#include <base_addr.h>
#include <misc_regs.h>
#include <board_config.h>
#include <driver/misc.h>
#include <driver/uart.h>
#include <driver/delay.h>
#include <driver/irq.h>
#include <driver/usb_gadget.h>
#include <driver/clock.h>
#include <driver/cpu.h>

#define STAGE1_SRAM_BASE  CONFIG_STAGE1_DRAM_BASE
#define JUMP_2_ADDR(addr) ((void(*)(void))(addr))()
#define STAGE1_LEN        (32 * 1024)


static void _VspWaitArmPoweroff(void)
{
    volatile CPU_INT_REGS *cpu_int_regs = (volatile CPU_INT_REGS *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_CK_INT);
    volatile A7_ENABLE *a7_enable = (volatile A7_ENABLE *)(MCU_REG_BASE_CONFIG + REG_OFFSET_A7_ENABLE);

    if (a7_enable->bits.a7_enable) {
        printf("wait a7 power off!\n");

        while((cpu_int_regs->a7_ck_int.bits.cpu_halt != 1) || (cpu_int_regs->a7_ck_enable.bits.cpu_halt != 1))
            continue;

        cpu_int_regs->a7_ck_enable.bits.cpu_halt = 0;
        cpu_int_regs->a7_ck_clear.bits.cpu_halt = 1;

        CpuSuspend();

        printf("a7 power off!\n");
    }
}

void _SerialDown(unsigned int base_addr, unsigned int max_len)
{
    unsigned int i;
    unsigned int length=0;
    unsigned char dat;

    for(i = 0; i < 4; i++)
    {
        length >>= 8;
        length |= ((unsigned int)UartRecvByte(CONFIG_SERIAL_PORT)) << 24;
    }
    if(length > max_len / 4)
        length = max_len / 4;
    for(i = 0; i < (length << 2); i++)
    {
        dat = UartRecvByte(CONFIG_SERIAL_PORT);
        *(volatile unsigned char *)(base_addr + i) = dat;
    }
    UartSendByte(CONFIG_SERIAL_PORT, 'F');
}

static void RunBootFileStage1(void)
{
    unsigned int addr;
    addr = *(unsigned int *)STAGE1_SRAM_BASE;

    __asm__ __volatile__("psrclr ee,ie");
    gx_disable_all_interrupt();
    CacheDone();

    JUMP_2_ADDR(*(unsigned int *)STAGE1_SRAM_BASE);
}

void VspUpgrade(void)
{
    char c = ' ';
    int ret = 0;
    unsigned long long start;
    volatile BOOT_MODE *mode = (volatile BOOT_MODE *)MISC_REG_BOOT_MODE;

    _VspWaitArmPoweroff();

    ClockEnable(CLOCK_MODULES);
    CacheInit();
    DelayInit();
    gx_irq_init();
    mode->flash_boot = 0;
    mode->uart_boot  = 0;
    mode->usb_boot   = 0;

    UartInit(CONFIG_SERIAL_PORT, 57600);
    start = get_time_us();
    while((get_time_us() - start) < 100000){
        UartSendByte(CONFIG_SERIAL_PORT, 0x4d); // 'M'
        UartSendByte(CONFIG_SERIAL_PORT, 0xf8);
        ret = UartTryRecvByte(CONFIG_SERIAL_PORT, (unsigned char *)&c);
        if (c == 'Y')
            break;
    }
    if ((ret == 0) && (c == 'Y')){
        _SerialDown(STAGE1_SRAM_BASE, STAGE1_LEN);
        mode->uart_boot = BOOT_MODE_UART;
    }else{
        UartInit(CONFIG_SERIAL_PORT, CONFIG_SERIAL_BAUD_RATE);
        UsbslaveDownloaderInit();
        UsbslaveInit();
        UsbslaveEnterDownloadMode();
        mode->usb_boot = BOOT_MODE_USB_HIGH;
    }
    RunBootFileStage1();
}
