/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 * vsp_mode_idle.c: VSP IDLE mode
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <types.h>

#include <board_config.h>

#include <driver/dsp.h>
#include <driver/cpu.h>
#include <driver/pmu.h>
#include <cpu_regs.h>
#include "vsp_mode.h"
#include "vsp_mode_idle_led.h"

#include "vsp_buffer.h"

#include "common/vsp_led_player.h"

#define LOG_TAG "[IDLE]"

//=================================================================================================

static int _IdleModeDspCallback(volatile VSP_MSG_DSP_MCU *request, void *priv)
{
    if (request->header.type == VMT_DM_SET_PARAM_DONE) {

        // INFO: If you want to handle the SET_PARAM, put your code here

        VSP_MSG_MCU_CPU message;
        message.header.type = VMT_MC_SET_PARAM_DONE;
        message.header.size = 0;
        message.header.magic = request->header.magic;
        message.header.param = request->header.param;
        CpuPostVspMessage(&message.header.value, message.header.size + 1);
    }
    return 0;
}

//=================================================================================================

static int _IdleModeProcPmRequest(volatile VSP_MSG_CPU_MCU *request)
{
    switch (request->header.param) {
        case VSP_PM_REQUEST_POWEROFF:
#ifdef CONFIG_BOARD_HAS_PMU
            PmuPowerOff();
#endif
            break;
        case VSP_PM_REQUEST_REBOOT:
            CpuReboot();
            break;
        default:
            return -1;
    }
    return 0;
}

static int _IdleModeProcLoadDSPFirmware(volatile VSP_MSG_CPU_MCU *request)
{
    unsigned int ptcm_addr = (unsigned int)request->load_dsp.ptcm_addr + CONFIG_STAGE1_DRAM_BASE;
    unsigned int dtcm_addr = (unsigned int)request->load_dsp.dtcm_addr + CONFIG_STAGE1_DRAM_BASE;
    unsigned int sram_addr = (unsigned int)request->load_dsp.sram_addr + CONFIG_STAGE1_DRAM_BASE;
    unsigned int ddr_addr  = (unsigned int)DEV_TO_MCU(request->load_dsp.ddr_addr);

    unsigned int ptcm_size = request->load_dsp.ptcm_size;
    unsigned int dtcm_size = request->load_dsp.dtcm_size;
    unsigned int sram_size = request->load_dsp.sram_size;
    unsigned int ddr_size  = request->load_dsp.ddr_size;

    unsigned int sram_base = (unsigned int)DEV_TO_MCU(request->load_dsp.sram_base);

    printf(LOG_TAG"Load DSP Firmware from:\n");
    printf(LOG_TAG"DSP SRAM base:      0x%08x\n", sram_base);
    printf(LOG_TAG"DSP PTCM addr|size: 0x%08x|%d\n", ptcm_addr, ptcm_size);
    printf(LOG_TAG"DSP DTCM addr|size: 0x%08x|%d\n", dtcm_addr, dtcm_size);
    printf(LOG_TAG"DSP SRAM addr|size: 0x%08x|%d\n", sram_addr, sram_size);
    printf(LOG_TAG"DSP DDR  addr|size: 0x%08x|%d\n", ddr_addr , ddr_size);

    if (sram_base < CONFIG_DSP_SRAM_BASE) {
        printf("DSP SRAM section is in MCU region: %08X\n", sram_base);
        return -1;
    }

    DspInit();
    DspLoadPTCMSection((unsigned char *)ptcm_addr, ptcm_size);
    DspLoadDTCMSection((unsigned char *)dtcm_addr, dtcm_size);
    DspLoadSRAMSection((unsigned char *)sram_base, (unsigned char *)sram_addr, sram_size);
#ifdef CONFIG_DSP_ENABLE_DDR
    unsigned int ddr_base  = (unsigned int)DEV_TO_MCU(request->load_dsp.ddr_base);
    DspLoadSRAMSection((unsigned char *)ddr_base , (unsigned char *)ddr_addr , ddr_size);
#endif
    DspRun();

    VSP_MSG_MCU_CPU response;

    response.header.type = VMT_MC_LOAD_DSP_DONE;
    response.header.size = 0;
    response.header.magic = request->header.magic;
    response.header.param = 0;
    CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1);
    printf(LOG_TAG"Succeed to Load DSP!\n");
    return 0;
}

static int _IdleModeProcSetVspBuffer(volatile VSP_MSG_CPU_MCU *request)
{
    VSP_BUFFER_CONFIG buffer_config;
    buffer_config.mic_buffer = request->set_buffer_vsp.mic_buffer;
    buffer_config.ref_buffer = request->set_buffer_vsp.ref_buffer;
    buffer_config.ctx_buffer = request->set_buffer_vsp.ctx_buffer;
    buffer_config.log_buffer = request->set_buffer_vsp.log_buffer;
    buffer_config.ctx_header = request->set_buffer_vsp.ctx_header;
    buffer_config.tmp_buffer = request->set_buffer_vsp.tmp_buffer;
    buffer_config.total_size = request->set_buffer_vsp.total_size;

    printf(LOG_TAG"Set VSP DDR Buffer as: \n");
    printf(LOG_TAG"MIC Buffer: 0x%08X\n", (unsigned int)buffer_config.mic_buffer);
    printf(LOG_TAG"REF Buffer: 0x%08X\n", (unsigned int)buffer_config.ref_buffer);
    printf(LOG_TAG"CTX Buffer: 0x%08X\n", (unsigned int)buffer_config.ctx_buffer);
    printf(LOG_TAG"CTX Header: 0x%08X\n", (unsigned int)buffer_config.ctx_header);
    printf(LOG_TAG"LOG Buffer: 0x%08X\n", (unsigned int)buffer_config.log_buffer);
    printf(LOG_TAG"TMP Buffer: 0x%08X\n", (unsigned int)buffer_config.tmp_buffer);

    if (VspSetDDRBuffer(&buffer_config)) {
        printf(LOG_TAG"Buffer is not accepted!\n");
        return -1;
    }

    VSP_MSG_MCU_CPU response;
    response.header.type = VMT_MC_SET_BUFFER_DONE;
    response.header.size = 0;
    response.header.magic = request->header.magic;
    response.header.param = request->header.param;

    CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1);

    return 0;
}

static int _IdleModeProcSetLedBuffer(volatile VSP_MSG_CPU_MCU *request)
{
    printf(LOG_TAG"Set LED DDR Buffer as: \n");
    printf(LOG_TAG"LED Queue:  0x%08X\n", (unsigned int)request->set_buffer_led.queue_buffer);
    printf(LOG_TAG"LED Frame:  0x%08X\n", (unsigned int)request->set_buffer_led.frame_buffer);

    if (VspLedSetCpuQueue(DEV_TO_MCU(request->set_buffer_led.queue_buffer),
                          DEV_TO_MCU(request->set_buffer_led.frame_buffer))) {
        printf(LOG_TAG"LED Queue is not accepted!\n");
        return -1;
    }
    VSP_MSG_MCU_CPU response;
    response.header.type = VMT_MC_SET_BUFFER_DONE;
    response.header.size = 0;
    response.header.magic = request->header.magic;
    response.header.param = request->header.param;

    CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1);

    return 0;
}

static int _IdleModeProcSetCmdBuffer(volatile VSP_MSG_CPU_MCU *request)
{
    printf(LOG_TAG"Set CMD DDR Buffer as: \n");
    printf(LOG_TAG"CMD Queue :  0x%08X\n", (unsigned int)request->set_buffer_cmd.cmd_buffer);
    printf(LOG_TAG"CMD Header:  0x%08X\n", (unsigned int)request->set_buffer_cmd.cmd_header);

    VSP_CMD_BUFFER_CONFIG config;
    config.cmd_buffer = request->set_buffer_cmd.cmd_buffer;
    config.cmd_header = request->set_buffer_cmd.cmd_header;
    config.total_size = request->set_buffer_cmd.total_size;

    if (VspSetDDRCmdBuffer(&config)) {
        printf(LOG_TAG"CMD DDR Buffer is not accepted!\n");
        return -1;
    }

    VSP_MSG_MCU_CPU response;
    response.header.type = VMT_MC_SET_BUFFER_DONE;
    response.header.size = 0;
    response.header.magic = request->header.magic;
    response.header.param = request->header.param;

    CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1);

    return 0;
}


static int _IdleModeProcSetBuffer(volatile VSP_MSG_CPU_MCU *request)
{
    switch (request->header.param) {
        case VSP_BUFFER_VSP:
            return _IdleModeProcSetVspBuffer(request);

        case VSP_BUFFER_LED:
            return _IdleModeProcSetLedBuffer(request);

        case VSP_BUFFER_CMD:
            return _IdleModeProcSetCmdBuffer(request);

        default:
            return -1;
    }
}

static int _IdleModeProcSetParam(volatile VSP_MSG_CPU_MCU *request)
{
    // INFO: If you want set mcu's param before DSP,
    //          write code here


    // Send the param to DSP
    VSP_MSG_MCU_DSP message;
    message.header.type  = VMT_MD_SET_PARAM;
    message.header.param = request->header.param;
    message.header.magic = request->header.magic;
    message.header.size  = sizeof(VMB_MD_SET_PARAM) / sizeof(unsigned int);
    message.set_param.param_addr = request->set_param.param_addr;
    message.set_param.param_size = request->set_param.param_size;

    DspPostVspMessage((unsigned int *)&message, message.header.size + 1);

    return 0;
}

static int _IdleModeProcEnterUpgrade(volatile VSP_MSG_CPU_MCU *request)
{
    VSP_MSG_MCU_CPU response;

    response.header.type  = VMT_MC_ENTER_UPGRADE_DONE;
    response.header.size  = 0;
    response.header.magic = request->header.magic;
    response.header.param = request->header.param;

    CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1);

    VspExit(VSP_EXIT_TO_ENTER_UPGRADE);
    return 0;
}
//=================================================================================================

static const VSP_LED_TASK s_idle_led_task = {
    .mode = VSP_LED_MODE_MASTER,
    .origin = 0,
    .frame_num = sizeof(vsp_mode_idle_led_buffer) / sizeof(vsp_mode_idle_led_buffer[0]) / 12,
    .pixel_num = 12,
    .frames = (void *)vsp_mode_idle_led_buffer,
};

static int _IdleModeInit(VSP_MODE_TYPE prev_mode)
{
    printf(LOG_TAG"Init IDLE mode\n");
    DspSetVspCallback(_IdleModeDspCallback, NULL);

    VspLedPlay(&s_idle_led_task);

    return 0;
}

static void _IdleModeDone(VSP_MODE_TYPE next_mode)
{
    VspLedStop();
    DspSetVspCallback(NULL, NULL);
    printf(LOG_TAG"Exit IDLE mode\n");
}

static int _IdleModeProc(volatile VSP_MSG_CPU_MCU *request)
{
    switch (request->header.type)
    {
        case VMT_CM_PM_REQUEST:
            return _IdleModeProcPmRequest(request);

        case VMT_CM_LOAD_DSP:
            return _IdleModeProcLoadDSPFirmware(request);

        case VMT_CM_SET_BUFFER:
            return _IdleModeProcSetBuffer(request);

        case VMT_CM_SET_PARAM:
            return _IdleModeProcSetParam(request);

        case VMT_CM_ENTER_UPGRADE:
            return _IdleModeProcEnterUpgrade(request);
        default:
            return -1;
    }
}

static void _IdleModeTick(void)
{
    VspLedFlush();
}

//-------------------------------------------------------------------------------------------------

const VSP_MODE_INFO vsp_idle_mode_info = {
    .type = VSP_MODE_IDLE,
    .init = _IdleModeInit,
    .done = _IdleModeDone,
    .proc = _IdleModeProc,
    .tick = _IdleModeTick,
};
