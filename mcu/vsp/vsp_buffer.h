/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_buffer.h: VSP I/O Buffer and Context Management
 *
 */

#ifndef __VSP_BUFFER_H__
#define __VSP_BUFFER_H__

#include <autoconf.h>

#include <vsp_context.h>
#include <vsp_command.h>

#include <driver/audio_mic.h>
#include <driver/audio_ref.h>
#include <driver/snpu.h>

#ifdef CONFIG_VSP_SAMPLE_RATE_8K
#define VSP_SAMPLE_RATE     8000
#endif
#ifdef CONFIG_VSP_SAMPLE_RATE_16K
#define VSP_SAMPLE_RATE     16000
#endif
#ifdef CONFIG_VSP_SAMPLE_RATE_48K
#define VSP_SAMPLE_RATE     48000
#endif
#ifndef VSP_SAMPLE_RATE
#error "Unknown Sample Rate"
#endif

#ifdef CONFIG_VSP_FRAME_LENGTH_2MS
#define VSP_FRAME_LENGTH    2
#endif
#ifdef CONFIG_VSP_FRAME_LENGTH_3MS
#define VSP_FRAME_LENGTH    3
#endif
#ifdef CONFIG_VSP_FRAME_LENGTH_4MS
#define VSP_FRAME_LENGTH    4
#endif
#ifdef CONFIG_VSP_FRAME_LENGTH_10MS
#define VSP_FRAME_LENGTH    10
#endif
#ifdef CONFIG_VSP_FRAME_LENGTH_16MS
#define VSP_FRAME_LENGTH    16
#endif
#ifndef VSP_FRAME_LENGTH
#error "Unknown Frame Length"
#endif

#ifdef CONFIG_VSP_FRAME_BIT_16
#define VSP_FRAME_BIT_SIZE  (AUDIO_IN_BIT_16)
typedef short SAMPLE;
#endif

#ifdef CONFIG_VSP_FRAME_BIT_24
#define VSP_FRAME_BIT_SIZE  (AUDIO_IN_BIT_24)
typedef int SAMPLE;
#endif

// size of a channel and a frame
#define VSP_FRAME_SIZE      (VSP_SAMPLE_RATE * VSP_FRAME_LENGTH / 1000 )  // sample number

//=================================================================================================

typedef struct {
    unsigned int        mic_num;                // channel number
    unsigned int        ref_num;                // channel number
    unsigned int        out_num;                // channel number

    unsigned int        frame_length;           // millisecond
    unsigned int        frame_num;              // frame number per channel
    unsigned int        sample_rate;            // sample rate;

    unsigned int        context_size;           // context size;
    unsigned int        context_num;            // context number;

    unsigned int        log_buffer_size;        // log buffer size;
    unsigned int        tmp_buffer_size;        // buffer size;
    unsigned int        ext_buffer_size;        // buffer size;
} VSP_BUFFER_SPEC;

typedef struct {
    VSP_CONTEXT context;
#if CONFIG_VSP_OUT_NUM > 0
    SAMPLE out_buffer[VSP_FRAME_SIZE * CONFIG_VSP_OUT_NUM * CONFIG_VSP_FRAME_NUM_PER_CONTEXT];
#endif
#ifdef CONFIG_VSP_VPA_FEATURES_DIM
    float features[CONFIG_VSP_VPA_FEATURES_DIM * CONFIG_VSP_FRAME_NUM_PER_CONTEXT];
#endif
#ifdef CONFIG_VPA_SNPU_BUFFER_SIZE
    SNPU_FLOAT snpu_buffer[CONFIG_VPA_SNPU_BUFFER_SIZE];
#endif
#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    unsigned int ext_buffer[(CONFIG_VSP_VPA_EXT_BUFFER_SIZE * 1024) / 4];
#endif
} __attribute__ ((aligned (8))) VSP_CONTEXT_BUFFER;

//=================================================================================================
// For STANDBY/UAC SRAM/PLC/CODEC workmodes
const VSP_BUFFER_SPEC *VspGetSRAMBufferSpec(void);

// Initialize the buffer info
int VspInitSRAMBuffer(void);

// Get a buffer configuration for Audio In settings
int VspGetSRAMAudioInConfig(AUDIO_IN_CONFIG *config);

// Get context header
VSP_CONTEXT_HEADER * VspGetSRAMContextHeader(void);

// Get a context with its related buffer
int VspGetSRAMContext(unsigned int index, VSP_CONTEXT **pp_context, unsigned int *size);

//-------------------------------------------------------------------------------------------------

int VspGetSRAMAudioRefConfig(AUDIO_REF_CONFIG *config);

SAMPLE * VspGetSRAMOutPutAddr(void);     // output buffer first pointer, get first context, and return 
unsigned int VspGetSRAMMicBufferLength(void);   // mic buffer length
unsigned int VspGetSRAMBufferLength(void);  // sizeof context buffer

//=================================================================================================
// For ACTIVE/BYPASS/FEED/UAC Linux|DDR modes

// Initialize the buffer info in DDR
typedef struct {
    SAMPLE              *mic_buffer;
    SAMPLE              *ref_buffer;
    VSP_CONTEXT_BUFFER *ctx_buffer;
    char               *log_buffer;
    VSP_CONTEXT_HEADER *ctx_header;
    void               *tmp_buffer;
    unsigned int        total_size;
} VSP_BUFFER_CONFIG;

int VspSetDDRBuffer(const VSP_BUFFER_CONFIG *config);
int VspSetStaticDDRBuffer(unsigned int start_addr);

//-------------------------------------------------------------------------------------------------

const VSP_BUFFER_SPEC *VspGetDDRBufferSpec(void);

int VspInitDDRBuffer(void);

// Get buffer configuration for Audio In / Ref Settings
int VspGetDDRAudioInConfig(AUDIO_IN_CONFIG *config);
int VspGetDDRAudioRefConfig(AUDIO_REF_CONFIG *config);

// Get context header
VSP_CONTEXT_HEADER * VspGetDDRContextHeader(void);

// Get a context with its related buffer
int VspGetDDRContext(unsigned int index, VSP_CONTEXT **pp_context, unsigned int *size);

//-------------------------------------------------------------------------------------------------

SAMPLE * VspGetDDROutPutAddr(void);     // output buffer first pointer, get first context, and return
unsigned int VspGetDDRMicBufferLength(void);   // mic buffer length
unsigned int VspGetDDRBufferLength(void);  // sizeof context buffer

//=================================================================================================
// For COMBO workmodes

// Initialize the buffer info
int VspInitComboBuffer(void);

// Get buffer configuration for Audio In / Ref Settings
int VspGetComboAudioInConfig(AUDIO_IN_CONFIG *config);

//=================================================================================================

#ifdef CONFIG_MCU_ENABLE_DEBUG
void VspDumpContext(VSP_CONTEXT *context);
void VspDumpContextHeader(VSP_CONTEXT_HEADER *context_header);
#else
#define VspDumpContext(c) while(0)
#define VspDumpContextHeader(c) while(0)
#endif

//=================================================================================================

typedef struct {
    unsigned int cmd_num;
    unsigned int cmd_size;
    unsigned int cmd_data_size;
} VSP_CMD_BUFFER_SPEC;

typedef struct {
    VSP_COMMAND command;
#if CONFIG_VSP_COMMAND_DATA_SIZE
    unsigned int  cmd_data[CONFIG_VSP_COMMAND_DATA_SIZE * 1024];
#endif
} __attribute__ ((aligned (8))) VSP_COMMAND_BUFFER;

typedef struct {
    VSP_COMMAND_BUFFER *cmd_buffer;
    VSP_COMMAND_HEADER *cmd_header;
    void               *cmd_data;
    unsigned int        total_size;
} VSP_CMD_BUFFER_CONFIG;

const VSP_CMD_BUFFER_SPEC *VspGetDDRCmdBufferSpec(void);
int VspSetDDRCmdBuffer(const VSP_CMD_BUFFER_CONFIG *config);
int VspInitCmdDDRBuffer(void);
int VspGetDDRCmd(unsigned int index, VSP_COMMAND **command, unsigned int *size);
int VspGetDDRNextCmd(VSP_COMMAND **command, unsigned int *size);
VSP_COMMAND_HEADER * VspGetDDRCommandHeader(void);

#endif /* __VSP_BUFFER_H__ */
