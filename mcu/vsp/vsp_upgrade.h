/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_upgrade.h: vsp upgrade function
 *
 */

#ifndef _VSP_UPGRADE_
#define _VSP_UPGRADE_

void VspUpgrade(void);

#endif
