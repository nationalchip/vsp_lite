/* Voice Signal Preprocess
 * Copyright (C) 2001-2018 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_mode.c: vsp mode
 *
 */

#include <types.h>
#include <autoconf.h>

#include "vsp_mode.h"

extern const VSP_MODE_INFO vsp_idle_mode_info;
extern const VSP_MODE_INFO vsp_boot_mode_info;
extern const VSP_MODE_INFO vsp_active_mode_info;
extern const VSP_MODE_INFO vsp_standby_mode_info;
extern const VSP_MODE_INFO vsp_bypass_mode_info;
extern const VSP_MODE_INFO vsp_modem_mode_info;
extern const VSP_MODE_INFO vsp_test_mode_info;

// workmodes for GX8008
extern const VSP_MODE_INFO vsp_codec_mode_info;
extern const VSP_MODE_INFO vsp_uac_mode_info;
extern const VSP_MODE_INFO vsp_plc_mode_info;
extern const VSP_MODE_INFO vsp_factory_mode_info;

static const VSP_MODE_INFO *s_vsp_mode_list[] = {
    // IDLE mode should be always put at the first position
    &vsp_idle_mode_info,
#ifdef CONFIG_VSP_HAS_BOOT_MODE
    &vsp_boot_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_ACTIVE_MODE
    &vsp_active_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_STANDBY_MODE
    &vsp_standby_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_MODEM_MODE
    &vsp_modem_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_BYPASS_MODE
    &vsp_bypass_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_TEST_MODE
    &vsp_test_mode_info,
#endif
// workmodes for GX8008
#ifdef CONFIG_VSP_HAS_CODEC_MODE
    &vsp_codec_mode_info,
#endif

#ifdef CONFIG_VSP_HAS_UAC_MODE
    &vsp_uac_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_PLC_MODE
    &vsp_plc_mode_info,
#endif
#ifdef CONFIG_VSP_HAS_FACTORY_MODE
    &vsp_factory_mode_info,
#endif
};

static int s_current_index;
static int s_vsp_loop;
static VSP_EXIT_REASON vsp_exit_reason;

//=================================================================================================

VSP_MODE_TYPE VspInitMode(VSP_MODE_TYPE type)
{
    s_vsp_loop = 1;
    s_current_index = 0;    // 0 Always is IDLE mode
    for (int i = 0; i < sizeof(s_vsp_mode_list) / sizeof(VSP_MODE_INFO *); i++) {
        if (s_vsp_mode_list[i]->type == type) {
            s_current_index = i;
            break;
        }
    }
    s_vsp_mode_list[s_current_index]->init(VSP_MODE_IDLE);
    return s_vsp_mode_list[s_current_index]->type;
}

VSP_MODE_TYPE VspSwitchMode(VSP_MODE_TYPE type)
{
    if (s_vsp_mode_list[s_current_index]->type == type)
        return type;

    int required_index = -1;
    for (int i = 0; i < sizeof(s_vsp_mode_list) / sizeof(VSP_MODE_INFO *); i++) {
        if (s_vsp_mode_list[i]->type == type) {
            required_index = i;
            break;
        }
    }

    if (required_index != -1) {
        // Exit current mode
        s_vsp_mode_list[s_current_index]->done(type);

        // Init the required mode
        if (s_vsp_mode_list[required_index]->init(s_vsp_mode_list[s_current_index]->type) == 0) {
            s_current_index = required_index;
        }
        else {  // Force to IDLE mode
            s_vsp_mode_list[s_current_index]->init(s_vsp_mode_list[s_current_index]->type);
            s_current_index = 0;
        }
    }

    return s_vsp_mode_list[s_current_index]->type;
}

VSP_MODE_TYPE VspGetCurrentMode(void)
{
    return s_vsp_mode_list[s_current_index]->type;
}

//=================================================================================================

int VspModeTaskProcess(volatile VSP_MSG_CPU_MCU *request)
{
    if (s_vsp_mode_list[s_current_index]->proc)
        return s_vsp_mode_list[s_current_index]->proc(request);
    else
        return -1;
}

int VspModeTick(void)
{
    if (s_vsp_mode_list[s_current_index]->tick)
        s_vsp_mode_list[s_current_index]->tick();

    return s_vsp_loop;
}

void VspExit(VSP_EXIT_REASON reason)
{
    s_vsp_mode_list[s_current_index]->done(VSP_MODE_IDLE);
    s_current_index = 0;
    s_vsp_loop = 0;
    vsp_exit_reason = reason;
}

VSP_EXIT_REASON VspGetExitReason(void)
{
    return vsp_exit_reason;
}
