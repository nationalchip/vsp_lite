/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_mode.h: vsp mode
 *
 */

#ifndef __VSP_MODE_H__
#define __VSP_MODE_H__

#include <vsp_message.h>

typedef int  (*VSP_MODE_INIT)(VSP_MODE_TYPE prev_mode);
typedef void (*VSP_MODE_DONE)(VSP_MODE_TYPE next_mode);
typedef int  (*VSP_MODE_PROC)(volatile VSP_MSG_CPU_MCU *request);
typedef void (*VSP_MODE_TICK)(void);

typedef enum {
    VSP_EXIT_TO_LOAD_MCU,
    VSP_EXIT_TO_ENTER_UPGRADE
} VSP_EXIT_REASON;

typedef struct {
    VSP_MODE_TYPE   type;
    VSP_MODE_INIT   init;
    VSP_MODE_DONE   done;
    VSP_MODE_PROC   proc;
    VSP_MODE_TICK   tick;
} VSP_MODE_INFO;

VSP_MODE_TYPE VspInitMode(VSP_MODE_TYPE mode);
VSP_MODE_TYPE VspSwitchMode(VSP_MODE_TYPE mode);
VSP_MODE_TYPE VspGetCurrentMode(void);

int VspModeTaskProcess(volatile VSP_MSG_CPU_MCU *request);
int VspModeTick(void);

void VspExit(VSP_EXIT_REASON reason);

VSP_EXIT_REASON VspGetExitReason(void);

#endif /* __VSP_MODE_H__ */
