/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_mode_uac.c: VSP UAC mode implements a USB UAC/HID/UART device, which can:
 *      1) Receive REF signal from HOST with USB UAC device;
 *      2) Capture MIC signal from Audio In;
 *      3) Process MIC and REF signal with AEC, BF, KWS;
 *      4) Send KWS to HOST by USB HID device;
 *      5) Send OUT signal after AEC and BF to HOST by USB UAC device;
 *      6) Wakeup HOST by IRR sender or GPIO pulse;
 *      7) Drive the LED ring (optional), frames are sent through UART.
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <base_addr.h>

#include <board_config.h>

#include <driver/audio_mic.h>
#include <driver/audio_ref.h>
#include <driver/audio_out.h>
#include <driver/cpu.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/usb_gadget.h>
#include <driver/vsp_uac_fifo_manager.h>
#include <driver/spi_flash.h>
#include <driver/misc.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/uac2_core.h>

#include <vsp_firmware.h>
#include <vsp_ioctl.h>

#include "common/vsp_led_player.h"
#include "common/vsp_vpa.h"
#include "common/vsp_plc_event.h"

#include "vsp_mode.h"
#include "vsp_buffer.h"

#include <common.h>
#include <list.h>

#define LOG_TAG "[UAC]"

// Configuration Check
#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM

#if CONFIG_VSP_UAC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF > CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL / CONFIG_VSP_FRAME_NUM_PER_CONTEXT
#error "UAC Context Offset is great than Context Num"
#endif

#else

#if CONFIG_VSP_UAC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF > CONFIG_VSP_DDR_FRAME_NUM_PER_CHANNEL / CONFIG_VSP_FRAME_NUM_PER_CONTEXT
#error "UAC Context Offset is great than Context Num"
#endif

#endif

#ifdef CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_16K
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 16000
#elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_32K)
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 32000
#elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_48K)
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 48000
#elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_64K)
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 64000
#elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_80K)
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 80000
#elif defined (CONFIG_VSP_UAC_MODE_CAPTURE_SAMPLE_RATE_96K)
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 96000
#else
#define VSP_UAC_MODE_CAPTURE_SAMPLE_RATE 16000
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
#ifdef CONFIG_VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE_8K
#define VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE 8000
#elif defined (CONFIG_VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE_16K)
#define VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE 16000
#elif defined (CONFIG_VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE_32K)
#define VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE 32000
#elif defined (CONFIG_VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE_48K)
#define VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE 48000
#endif
#endif // #ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK

#ifdef CONFIG_MCU_ENABLE_CPU
#define VPA_FLASH_OFFSET CONFIG_VSP_UAC_LOAD_DSP_FROM
#else
#define VPA_FLASH_OFFSET DSP_FW_OFFSET
#endif
//=================================================================================================

typedef struct {
    unsigned char vad;
    unsigned char key;
} VSP_UAC_HID_VAD_TABLE;

typedef struct {
    unsigned char kws;
    unsigned char key;
} VSP_UAC_HID_KWS_TABLE;

typedef enum {
    VSP_UAC_SUB_MODE_STATE_IDLE         = 0x00,
    VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_N = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_N = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT | 1 << UAC2_PLAYBACK_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_Y = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT | 1 << UAC2_RECORD_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_Y = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT | 1 << UAC2_PLAYBACK_STATE_BIT | 1 << UAC2_RECORD_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_KWS_ENABLE   = 0x10,
    VSP_UAC_SUB_MODE_STATE_MUTE         = 0x20,
    VSP_UAC_SUB_MODE_STATE_VAD          = 0x30,
} VSP_UAC_SUB_MODE_STATE;

typedef void (*VSP_UAC_SUB_MODE_PROC)(void);

typedef struct {
    VSP_UAC_SUB_MODE_STATE  state; // sub-mode in UAC MODE
    LED_PIXEL               pixel;
    VSP_UAC_SUB_MODE_PROC   proce;
} VSP_UAC_SUB_MODE_INFO;

//=================================================================================================

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
# ifdef CONFIG_VSP_UAC_MODE_PLAYBACK_AUDIO_BUFF_STORE_TIME_MS
# define AUDIO_BUFF_STORE_TIME_MS CONFIG_VSP_UAC_MODE_PLAYBACK_AUDIO_BUFF_STORE_TIME_MS //ms
# else
# define AUDIO_BUFF_STORE_TIME_MS 50 //ms
# endif
#define AUDIO_OUT_BUFFER_SIZE (VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE / 1000 * CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER * (CONFIG_VSP_UAC_MODE_PLAYBACK_BIT_WIDTH / 8) * AUDIO_BUFF_STORE_TIME_MS)
#else
#define AUDIO_OUT_BUFFER_SIZE (8192)
#endif
static char s_audio_out_buffer[AUDIO_OUT_BUFFER_SIZE] __attribute__((aligned(8)));

static struct {
    unsigned int    audio_in_handle;
    unsigned int    audio_out_handle;
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID
    int             hid_handle;
#endif
    char            hid_key[8];
    unsigned char   mute_enable;
    unsigned char   kws_value;
    unsigned char   kws_enable;
    unsigned char   vad_value;
    unsigned char   vad_enable;
    unsigned char   sub_mode_last_index;
    unsigned        reserved:2;
    unsigned int    led_state;            // 0: uninitialized; 1: initialized
    unsigned int    uac_state;            // 0: uninitialized; 1: initialized
    unsigned int    ctx_last_index;
} s_uac_state = {0};

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID
static const VSP_UAC_HID_VAD_TABLE s_uac_hid_vad_table[] = {
    {  0, 0x14}, // vad == 0       -> 'q'
    {  1, 0x1A}, // vad == 1       -> 'w'
};

static const VSP_UAC_HID_KWS_TABLE s_uac_hid_kws_table[] = {
    {100, 0x04}, // 你好小乐        -> 'a'
    {103, 0x05}, // 小宝开灯        -> 'b'
    {104, 0x06}, // 小宝关灯        -> 'c'
    {105, 0x07}, // 调亮一点        -> 'd'
    {106, 0x08}, // 调暗一点        -> 'e'
    {107, 0x09}, // 再亮一点        -> 'f'
    {108, 0x0A}, // 再暗一点        -> 'g'
    {109, 0x0B}, // 打开暖灯        -> 'h'
    {110, 0x0C}, // 打开白灯        -> 'i'
    {111, 0x0D}, // 最低亮度        -> 'j'
    {112, 0x0E}, // 最高亮度        -> 'k'
};
#endif

static void _UacSubModeVadProce(void)
{
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID
    // Send hid
    for (unsigned int i = 0; i < sizeof(s_uac_hid_vad_table) / sizeof(VSP_UAC_HID_VAD_TABLE); ++i) {
        if(s_uac_state.vad_value == s_uac_hid_vad_table[i].vad) {
            s_uac_state.hid_key[2] = s_uac_hid_vad_table[i].key;
            HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);

            s_uac_state.hid_key[2] = 0x00;
            HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);
            break;
        }
    }
#endif
    s_uac_state.vad_enable = 0;
}

static void _UacSubModeKwsProce(void)
{
    if (s_uac_state.kws_value) {
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_IRR
        IrrSendCode(0x12, 0x34);
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID
        // Send hid
        for (unsigned int i = 0; i < sizeof(s_uac_hid_kws_table) / sizeof(VSP_UAC_HID_KWS_TABLE); ++i) {
            if(s_uac_state.kws_value == s_uac_hid_kws_table[i].kws) {
                s_uac_state.hid_key[2] = s_uac_hid_kws_table[i].key;
                HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);

                s_uac_state.hid_key[2] = 0x00;
                HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);
                break;
            }
        }
#endif
        s_uac_state.kws_value = 0;
    }
}

static const VSP_UAC_SUB_MODE_INFO s_uac_sub_mode_list[] = {
    {VSP_UAC_SUB_MODE_STATE_IDLE        , .pixel.bits = {  0,   0,   0,   0}, NULL},                 // idle
    {VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_N, .pixel.bits = {  0,   0, 255, 255}, NULL},                 // no_aplay and no_record    -> blue
    {VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_N, .pixel.bits = {  0, 255, 255, 255}, NULL},                 // aplay and no_record       -> cyan
    {VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_Y, .pixel.bits = {255,   0, 255, 255}, NULL},                 // no_aplay and record       -> purple
    {VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_Y, .pixel.bits = {  0, 255,   0, 255}, NULL},                 // aplay and record          -> green
    {VSP_UAC_SUB_MODE_STATE_VAD         , .pixel.bits = {  0,   0,   0,   0}, _UacSubModeVadProce},  // vad                       -> NULL
    {VSP_UAC_SUB_MODE_STATE_KWS_ENABLE  , .pixel.bits = {255,   0,   0, 255}, _UacSubModeKwsProce},  // kws enable                -> red blink
    {VSP_UAC_SUB_MODE_STATE_MUTE        , .pixel.bits = {255, 255,   0, 255}, NULL},                 // mute                      -> yellow
};

//=================================================================================================
// Mute Button

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_MUTE_BTN

static int _UacMuteButtonCallback(unsigned int port, void *pdata)
{
    s_uac_state.mute_enable = !s_uac_state.mute_enable;

    PLC_EVENT plc_event;
    plc_event.event_id = UAC_UP_STREAM_OFF;
    VspTriggerPlcEvent(plc_event);
    return 0;
}

#endif

//=================================================================================================

#ifndef CONFIG_VSP_UAC_WITHOUT_DSP
static int _UacDspCallback(volatile VSP_MSG_DSP_MCU *request, void *priv)
{
    if (request->header.type == VMT_DM_PROCESS_DONE) {
        if (request->header.param == VSP_PROCESS_ACTIVE || request->header.param == VSP_PROCESS_TALKING) {
            VSP_CONTEXT *context = DEV_TO_MCU(request->process_done.context_addr);
            SAMPLE *out_buffer = DEV_TO_MCU(context->out_buffer);

#ifdef CONFIG_VSP_PLC_ENABLE_SYS_EVENT
            PLC_EVENT plc_event;
            plc_event.event_id = DSP_PROCESS_DONE_EVENT_ID;
            plc_event.ctx_index = context->ctx_index;
            VspTriggerPlcEvent(plc_event); // Notify to send wav to bluetooth
#endif

            if (!s_uac_state.mute_enable) {
#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM
                AudioBuffProduce(s_uac_state.audio_in_handle, out_buffer, VspGetSRAMMicBufferLength() * sizeof(short) / sizeof(SAMPLE));
#else   // DDR + Linux
#ifndef CONFIG_VSP_UAC_MODE_ENABLE_CAPTURE_FROM_CPU
                AudioBuffProduce(s_uac_state.audio_in_handle, out_buffer, VspGetDDRMicBufferLength() * sizeof(short) / sizeof(SAMPLE));
#endif
#ifdef CONFIG_VSP_UAC_WORKS_ON_LINUX
#ifdef CONFIG_VSP_UAC_POST_CONTEXT_TO_CPU_BY_DSP
                // Send Context to CPU
                VSP_MSG_MCU_CPU message;
                message.header.type = VMT_MC_PROCESS_DONE;
                message.header.size = sizeof(VMB_MC_PROCESS_DONE) / sizeof(unsigned int);
                message.header.magic = request->header.magic;
                message.header.param = request->header.param;
                message.process_done.context_addr = request->process_done.context_addr;
                message.process_done.context_size = request->process_done.context_size;
                CpuPostVspMessage(&message.header.value, message.header.size + 1);
#endif
#endif
#endif
            }

            if (context->vad != s_uac_state.vad_value) {
                if ((context->ctx_index - s_uac_state.ctx_last_index) > 2) {
                    s_uac_state.vad_value = context->vad;
                    s_uac_state.vad_enable = 1;
                    s_uac_state.ctx_last_index = context->ctx_index;
                }
            }

            if (s_uac_state.kws_enable)
                s_uac_state.kws_enable--;

            if (context->kws) {
                s_uac_state.kws_value = context->kws;
                s_uac_state.kws_enable = 5; // Control led blink duration of 5 * 30ms

                PLC_EVENT plc_event = {.event_id = s_uac_state.kws_value, .ctx_index = context->ctx_index};
                VspTriggerPlcEvent(plc_event);
            }
        }
    }
    return 0;
}
#endif

//=================================================================================================

static int _UacUsbAudioPlayCallback(unsigned int read_addr, unsigned int size)
{
#ifdef CONFIG_VSP_UAC_MODE_PLAYBACK_THROUGH_AUDIOOUT
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (SAMPLE *)read_addr, size);
#endif
    return 0;
}

//-------------------------------------------------------------------------------------------------

static struct usb_status usb_dev_status, usb_hid_status;
static struct usb_status usb_uac_us_status, usb_uac_ds_status;

#if defined(CONFIG_VSP_UAC_MODE_PRODUCE_UAC_DOWNSTREAM_CONTROL_MSG) || defined(CONFIG_VSP_UAC_MODE_PRODUCE_UAC_UPSTREAM_CONTROL_MSG)
typedef enum {
    UAC_CTRL_DIR_DOWNSTREAM,
    UAC_CTRL_DIR_UPSTREAM,
} uac_ctrl_dir_t;

typedef enum {
    UAC_CTRL_VOLUME,
    UAC_CTRL_MUTE,
    UAC_CTRL_AUTO_GAIN_CONTROL,
    UAC_CTRL_BASS_BOOST,
} uac_ctrl_type_t;

typedef union {
    int  volume;
    int  mute;
    int  auto_gain_control;
    int  bass_boost;
} uac_ctrl_value_t;

#define UAC_CTRL_INFO_NUMBER 20
struct uac_ctrl_info {
    uac_ctrl_dir_t   dir;
    uac_ctrl_type_t  type;
    uac_ctrl_value_t value;

    int in_use;
    struct list_head node;
};

static struct uac_ctrl_info uci[UAC_CTRL_INFO_NUMBER];
static LIST_HEAD(uac_ctrl_head);

static int get_uci(void)
{
    int i;

    for (i = 0; i < ARRAY_SIZE(uci); i++) {
        if (!uci[i].in_use) {
            uci[i].in_use = 1;
            return i;
        }
    }

    return -1;
}

static void put_uci(int index)
{
    if (index < 0)
        return ;

    if (uci[index].in_use)
        uci[index].in_use = 0;

    return ;
}

static int get_uci_index(struct uac_ctrl_info *info)
{
    if (!info)
        return -1;

    return info - &uci[0];
}

static int get_avalible_uci_num(void)
{
    int i;
    int total = 0;

    for (i = 0; i < ARRAY_SIZE(uci); i++)
        if (uci[i].in_use)
            total++;

    return total;
}

#if 0
static char *type_to_str(uac_ctrl_type_t type)
{
    switch (type) {
        case UAC_CTRL_VOLUME:
            return "volume";
        case UAC_CTRL_MUTE:
            return "mute";
        case UAC_CTRL_AUTO_GAIN_CONTROL:
            return "agc";
        case UAC_CTRL_BASS_BOOST:
            return "bass boost";
        default:
            return "unknow";
    }
}

static void list_used_uci(void)
{
    int i;
    int total = 0;

    for (i = 0; i < ARRAY_SIZE(uci); i++) {
        if (uci[i].in_use) {
            printf ("index : %2d %s %s %d\n", i, uci[i].dir == UAC_CTRL_DIR_DOWNSTREAM ? "ds" : "us", type_to_str(uci[i].type), uci[i].value.volume);
            total++;
        }
    }
    printf ("total : %d\n", total);
}
#endif

static int uci_produce(uac_ctrl_dir_t dir, uac_ctrl_type_t type, uac_ctrl_value_t value)
{
    int index;

    index = get_uci();
    if (index < 0)
        return -1;

    INIT_LIST_HEAD(&uci[index].node);
    uci[index].dir   = dir;
    uci[index].type  = type;
    uci[index].value = value;
    list_add_tail(&uci[index].node, &uac_ctrl_head);

    return 0;
}

static int uci_consume(struct uac_ctrl_info *info)
{
    struct uac_ctrl_info *_info;

    if (!info)
        return -1;

    if (list_empty(&uac_ctrl_head))
        return -1;

    _info = list_first_entry(&uac_ctrl_head, struct uac_ctrl_info, node);
    *info = *_info;
    list_del(&_info->node);
    put_uci(get_uci_index(_info));

    return 0;
}

static void _UacCtrlInfoTranslate(struct uac_ctrl_info *info, VSP_IOC_UAC_INFO *INFO)
{
    INFO->uac_ctrl_info_valid = 1;
    INFO->uac_ctrl_dir = (info->dir == UAC_CTRL_DIR_DOWNSTREAM) ? VSP_UAC_CTRL_DIR_DOWNSTREAM : VSP_UAC_CTRL_DIR_UPSTREAM;

    switch (info->type) {
        case UAC_CTRL_VOLUME:
            INFO->uac_ctrl_type         = VSP_UAC_CTRL_TYPE_VOLUME;
            INFO->uac_ctrl_value.volume = info->value.volume;
            break;
        case UAC_CTRL_MUTE:
            INFO->uac_ctrl_type         = VSP_UAC_CTRL_TYPE_MUTE;
            INFO->uac_ctrl_value.enable = info->value.mute;
            break;
        case UAC_CTRL_AUTO_GAIN_CONTROL:
            INFO->uac_ctrl_type         = VSP_UAC_CTRL_TYPE_AUTO_GAIN_CONTROL;
            INFO->uac_ctrl_value.enable = info->value.auto_gain_control;
            break;
        case UAC_CTRL_BASS_BOOST:
            INFO->uac_ctrl_type         = VSP_UAC_CTRL_TYPE_BASS_BOOST;
            INFO->uac_ctrl_value.enable = info->value.bass_boost;
            break;
        default:
            INFO->uac_ctrl_type         = VSP_UAC_CTRL_TYPE_UNKNOW;
            break;
    }

    return ;
}

// called in isr context
static void _UacStatusInfoTranslate(VSP_IOC_UAC_INFO *info)
{
    info->usb_dev_status    = usb_dev_status;
    info->usb_uac_us_status = usb_uac_us_status;
    info->usb_uac_ds_status = usb_uac_ds_status;
    info->usb_hid_status    = usb_hid_status;

    if (usb_dev_status.changed == 1)
        usb_dev_status.changed = 0;

    if (usb_uac_us_status.changed == 1)
        usb_uac_us_status.changed = 0;

    if (usb_uac_ds_status.changed == 1)
        usb_uac_ds_status.changed = 0;

    if (usb_hid_status.changed == 1)
        usb_hid_status.changed = 0;

    return ;
}
#endif

static int _UacVolumeCtrlCallback(enum UAC2_CONTROL_DIRECTION dir, unsigned int volume)
{
    switch (dir) {
        case UAC2_CONTROL_DOWNSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_DOWNSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.volume = volume;
                uci_produce(UAC_CTRL_DIR_DOWNSTREAM, UAC_CTRL_VOLUME, value);
            }
#else
            {
                PLC_EVENT plc_event = {.event_id = UAC_DOWN_SET_VOLUME, .uac_volume = volume};
                VspTriggerPlcEvent(plc_event);
                AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, volume);
            }
#endif
            break;

        case UAC2_CONTROL_UPSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_UPSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.volume = volume;
                uci_produce(UAC_CTRL_DIR_UPSTREAM, UAC_CTRL_VOLUME, value);
            }
#else
            // not support
#endif
            break;

        default:
            break;
    }
    return 0;
}

static int _UacMuteCtrlCallback(enum UAC2_CONTROL_DIRECTION dir, int mute)
{
    //printf(LOG_TAG"_UacMuteCtrlCallback %d\n", mute);
    //s_uac_state.mute_enable = !s_uac_state.mute_enable; // switch to sub-mute mode

    switch (dir) {
        case UAC2_CONTROL_DOWNSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_DOWNSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.mute = mute;
                uci_produce(UAC_CTRL_DIR_DOWNSTREAM, UAC_CTRL_MUTE, value);
            }
#else
            AudioOutSetMute(AUDIO_OUT_ROUTE_BUFFER, mute);
#endif
            break;

        case UAC2_CONTROL_UPSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_UPSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.mute = mute;
                uci_produce(UAC_CTRL_DIR_UPSTREAM, UAC_CTRL_MUTE, value);
            }
#else
            // not support
#endif
            break;

        default:
            break;
    }
    return 0;
}

int _UacAGCCtrlCallback(enum UAC2_CONTROL_DIRECTION dir, int open)
{
    switch (dir) {
        case UAC2_CONTROL_DOWNSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_DOWNSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.auto_gain_control = open;
                uci_produce(UAC_CTRL_DIR_DOWNSTREAM, UAC_CTRL_AUTO_GAIN_CONTROL, value);
            }
#else
            // not support now
#endif
            break;

        case UAC2_CONTROL_UPSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_UPSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.auto_gain_control = open;
                uci_produce(UAC_CTRL_DIR_UPSTREAM, UAC_CTRL_AUTO_GAIN_CONTROL, value);
            }
#else
            // not support now
#endif
            break;

        default:
            break;
    }
    return 0;
}

int _UacBassBoostCtrlCallback(enum UAC2_CONTROL_DIRECTION dir, int open)
{
    switch (dir) {
        case UAC2_CONTROL_DOWNSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_DOWNSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.bass_boost = open;
                uci_produce(UAC_CTRL_DIR_DOWNSTREAM, UAC_CTRL_BASS_BOOST, value);
            }
#else
            // not support now
#endif
            break;

        case UAC2_CONTROL_UPSTREAM:
#ifdef CONFIG_VSP_UAC_MODE_PRODUCE_UAC_UPSTREAM_CONTROL_MSG
            {
                uac_ctrl_value_t value;
                value.bass_boost = open;
                uci_produce(UAC_CTRL_DIR_UPSTREAM, UAC_CTRL_BASS_BOOST, value);
            }
#else
            // not support now
#endif
            break;

        default:
            break;
    }
    return 0;
}

static int _UacConnectNotifyCallback(enum notify_status_type type, unsigned int state)
{
    switch (type) {
        case NOTIFY_STATUS_DEVICE:
            usb_dev_status.changed   = 1;
            usb_dev_status.st.status = state; // enum udc_device_status
            break;
        case NOTIFY_STATUS_FUNCTION_UAC_UPSTREAM:
            usb_uac_us_status.changed   = 1;
            usb_uac_us_status.st.enable = state; //state : 0/1
            break;
        case NOTIFY_STATUS_FUNCTION_UAC_DOWNSTREAM:
            usb_uac_ds_status.changed   = 1;
            usb_uac_ds_status.st.enable = state; //state : 0/1
            break;
        case NOTIFY_STATUS_FUNCTION_HID:
            usb_hid_status.changed   = 1;
            usb_hid_status.st.enable = state; //state : 0/1
            break;
        default:
            break;
    }

    return 0;
}

static unsigned int _UacGetSdcAddrCallback(void)
{
    return AudioOutGetSdcAddr();
}

static const UAC2_DEVICE_DESCRIPTION _uac2_device_description = {
    .sound_card_name_in_windows = CONFIG_VSP_UAC_NAME_IN_WINDOWS,
    .manufacturer_name          = CONFIG_VSP_UAC_MANUFACTURER_NAME,
    .product_name               = CONFIG_VSP_UAC_PRODUCT_NAME,
    .serial_number              = CONFIG_VSP_UAC_SERIAL_NUMBER,
};

static UAC2_CALLBACKS s_uac2_callback = {
    .volume_callback                 = _UacVolumeCtrlCallback,
    .mute_callback                   = _UacMuteCtrlCallback,
    .auto_gain_control_callback      = _UacAGCCtrlCallback,
    .bass_boost_callback             = _UacBassBoostCtrlCallback,

    .notify_callback                 = _UacConnectNotifyCallback,
    .audio_out_get_sdc_addr_callback = _UacGetSdcAddrCallback,
};

//=================================================================================================

#if defined (CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID) && defined (CONFIG_VSP_UAC_MODE_HID_PROTOCOL_HUACHUANGSHIXUN)
struct uac_mode_arm_msg {
    int in_use;
    VSP_PARAM_UAC_MODE_MSG msg;
    struct list_head node;
};

#define MAX_UAC_MODE_MSG_NUMBER 10
static struct uac_mode_arm_msg umam[MAX_UAC_MODE_MSG_NUMBER];
static LIST_HEAD(uac_mode_arm_msg_head);

static int parse_uac_mode_arm_msg(VSP_PARAM_UAC_MODE_MSG *msg)
{
    unsigned char hid_content[HID_MSG_MAX_PACKET_SIZE];
    unsigned int  send_len = 0;
    int ret;

    if (!msg)
        return -1;

    //printf ("type : %d id : %d\n", msg->type, msg->content.hid);
    switch (msg->type) {
        case UAC_MODE_MSG_TYPE_HID:
            switch (msg->hid.type) {
                // report id : 1
                case UAC_MODE_MSG_HID_VOLUME_UP:
                    hid_content[0] = UAC_MODE_MSG_HID_VOLUME_UP_REPORT_ID; // report id
                    hid_content[1] = 1 << 0;
                    hid_content[2] = UAC_MODE_MSG_HID_VOLUME_UP_REPORT_ID; // report id
                    hid_content[3] = 0;
                    send_len = 4;
                    break;
                case UAC_MODE_MSG_HID_VOLUME_DOWN:
                    hid_content[0] = UAC_MODE_MSG_HID_VOLUME_DOWN_REPORT_ID; // report id
                    hid_content[1] = 1 << 1;
                    hid_content[2] = UAC_MODE_MSG_HID_VOLUME_DOWN_REPORT_ID; // report id
                    hid_content[3] = 0;
                    send_len = 4;
                    break;

                // report id : 2
                case UAC_MODE_MSG_HID_HOOKSWITH:
                    hid_content[0] = UAC_MODE_MSG_HID_HOOKSWITH_REPORT_ID; // report id
                    hid_content[1] = 1 << 0;
                    hid_content[2] = UAC_MODE_MSG_HID_HOOKSWITH_REPORT_ID; // report id
                    hid_content[3] = 0;
                    send_len = 4;
                    break;
                case UAC_MODE_MSG_HID_PHONEMUTE:
                    hid_content[0] = UAC_MODE_MSG_HID_PHONEMUTE_REPORT_ID; // report id
                    hid_content[1] = 1 << 1;
                    hid_content[2] = UAC_MODE_MSG_HID_PHONEMUTE_REPORT_ID; // report id
                    hid_content[3] = 0;
                    send_len = 4;
                    break;

                // report id : 7
                case UAC_MODE_MSG_HID_PRIVATE:

                    {
#define HID_MSG_MAX_SEND_SIZE_EACH_PKT (HID_MSG_MAX_PACKET_SIZE - 2)
                        // 如果单次要发送的数据量比较大，则将该包拆分成多次传输
                        unsigned int left, offset, consume_size;

                        left = msg->hid.len;
                        offset = 0;

                        while (left) {
                            if (left >= HID_MSG_MAX_SEND_SIZE_EACH_PKT)
                                consume_size = HID_MSG_MAX_SEND_SIZE_EACH_PKT;
                            else
                                consume_size = left;

                            hid_content[0] = UAC_MODE_MSG_HID_PRIVATE_REPORT_ID;
                            hid_content[1] = consume_size;
                            memcpy(&hid_content[2], &msg->hid.content[offset], consume_size);

                            offset += consume_size;
                            left   -= consume_size;

                            send_len = consume_size + 2;
                            ret = HidWrite(s_uac_state.hid_handle, hid_content, send_len);
                            if (ret != send_len) {
                                printf ("hid write failed in hid private, ret : %d expect : %d\n", ret, send_len);
                                break;
                            }
                        }
                        send_len = 0; // already finish
                    }

                    break;

                default:
                    break;
            }

            if (send_len) {
                ret = HidWrite(s_uac_state.hid_handle, hid_content, send_len);
                if (ret != send_len)
                    printf ("hid write failed, ret : %d, expect : %d\n", ret, send_len);
            }
            break;
        case UAC_MODE_MSG_TYPE_NONE:
        default:
            break;
    }

    return 0;
}

static struct uac_mode_arm_msg* get_arm_msg(void)
{
    int i;

    for  (i = 0; i < ARRAY_SIZE(umam); i++) {
        if (!umam[i].in_use) {
            umam[i].in_use = 1;
            return &umam[i];
        }
    }

    return NULL;
}

static void put_arm_msg(struct uac_mode_arm_msg* msg)
{
    if ((msg - &umam[0] < 0) || (msg - &umam[0] >= ARRAY_SIZE(umam)))
        return ;

    memset(msg, 0, sizeof(struct uac_mode_arm_msg));

    return ;
}

// called in ISR context
static int uac_mode_arm_msg_produce(VSP_PARAM_UAC_MODE_MSG *msg)
{
    struct uac_mode_arm_msg *msg_r;

    if (!msg)
        return -1;

    msg_r = get_arm_msg();
    if (!msg_r) {
        printf ("%s, get msg failed\n", __func__);
        return -1;
    }

    INIT_LIST_HEAD(&msg_r->node);
    msg_r->msg = *msg;
    list_add_tail(&msg_r->node, &uac_mode_arm_msg_head);

    return 0;
}

// called in non-interrupted context
static int uac_mode_arm_msg_consume(VSP_PARAM_UAC_MODE_MSG *msg)
{
    struct uac_mode_arm_msg *msg_r;

    if (!msg)
        return -1;

    if (list_empty(&uac_mode_arm_msg_head))
        return -1;

    gx_disable_irq();
    msg_r = list_first_entry(&uac_mode_arm_msg_head, struct uac_mode_arm_msg, node);
    *msg = msg_r->msg;
    list_del(&msg_r->node);
    put_arm_msg(msg_r);
    gx_enable_irq();

    return 0;
}

static int uac_mode_arm_msg_is_available(void)
{
    if (list_empty(&uac_mode_arm_msg_head))
        return 0;

    return 1;
}

// hid downstream message
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_HID_DOWNSTREAM
struct uac_mode_hid_msg_record {
    int in_use;
    struct list_head node;

    struct hid_msg msg;
};

#define UAC_MODE_HID_MSG_RNUMBER 10
static struct uac_mode_hid_msg_record umhmr[UAC_MODE_HID_MSG_RNUMBER];
static LIST_HEAD(uac_mode_hid_msg_head);

static struct uac_mode_hid_msg_record* get_hid_msg(void)
{
    int i;

    for  (i = 0; i < ARRAY_SIZE(umhmr); i++) {
        if (!umhmr[i].in_use) {
            umhmr[i].in_use = 1;
            return &umhmr[i];
        }
    }

    return NULL;
}

static void put_hid_msg(struct uac_mode_hid_msg_record* msg)
{
    if ((msg - &umhmr[0] < 0) || (msg - &umhmr[0] >= ARRAY_SIZE(umhmr)))
        return ;

    memset(msg, 0, sizeof(struct uac_mode_hid_msg_record));

    return ;
}

// called in non-interrupted context
static int uac_mode_hid_msg_produce(struct hid_msg *msg)
{
    struct uac_mode_hid_msg_record *msg_r;

    if (!msg)
        return -1;

    gx_disable_irq();
    msg_r = get_hid_msg();
    if (!msg_r) {
        gx_enable_irq();
        printf ("%s, get msg failed\n", __func__);
        return -1;
    }

    INIT_LIST_HEAD(&msg_r->node);
    msg_r->msg = *msg;
    list_add_tail(&msg_r->node, &uac_mode_hid_msg_head);
    gx_enable_irq();

    return 0;
}

// called in ISR context
static int uac_mode_hid_msg_consume(struct hid_msg *msg)
{
    struct uac_mode_hid_msg_record *msg_r;

    if (!msg)
        return -1;

    if (list_empty(&uac_mode_hid_msg_head))
        return -1;

    msg_r = list_first_entry(&uac_mode_hid_msg_head, struct uac_mode_hid_msg_record, node);
    *msg = msg_r->msg;
    list_del(&msg_r->node);
    put_hid_msg(msg_r);

    return 0;
}

#if 0
static int uac_mode_hid_msg_is_available(void)
{
    if (list_empty(&uac_mode_hid_msg_head))
        return 0;

    return 1;
}
#endif

static unsigned int hid_msg_id3_value = -1;
static unsigned int hid_msg_id4_value = -1;
static unsigned int hid_msg_id5_value = -1;
static unsigned int hid_msg_id6_value = -1;

static void parse_uac_mode_hid_msg_in_huachaungshixun_protocol(void)
{
    int ret;
    unsigned char hid_buf[HID_MSG_MAX_PACKET_SIZE];
    unsigned char report_id, value;
    unsigned int pkt_size;
    struct hid_msg msg;

    while (1) {
        ret = HidRead(s_uac_state.hid_handle, hid_buf, 1);
        if (ret <= 0)
            break;

        report_id = hid_buf[0];
        memset (&msg, 0, sizeof(struct hid_msg));

        switch (report_id) {
            case HID_MSG_RING_REPORT_ID:

                ret = HidRead(s_uac_state.hid_handle, hid_buf, 1);
                if (ret <= 0)
                    break;
                value = hid_buf[0];

                if (value != hid_msg_id3_value) {
                    msg.valid = 1;
                    msg.ph.msg_type = HID_MSG_RING;
                    msg.ph.value    = value;

                    hid_msg_id3_value = value;
                }
                break;

            case HID_MSG_MUTE_REPORT_ID:

                ret = HidRead(s_uac_state.hid_handle, hid_buf, 1);
                if (ret <= 0)
                    break;
                value = hid_buf[0];

                if (value != hid_msg_id4_value) {
                    msg.valid = 1;
                    msg.ph.msg_type = HID_MSG_MUTE;
                    msg.ph.value    = value;

                    hid_msg_id4_value = value;
                }
                break;

            case HID_MSG_OFFHOOK_REPORT_ID:

                ret = HidRead(s_uac_state.hid_handle, hid_buf, 1);
                if (ret <= 0)
                    break;
                value = hid_buf[0];

                if (value != hid_msg_id5_value) {
                    msg.valid = 1;
                    msg.ph.msg_type = HID_MSG_OFFHOOK;
                    msg.ph.value    = value;

                    hid_msg_id5_value = value;
                }
                break;

            case HID_MSG_HOLD_REPORT_ID:

                ret = HidRead(s_uac_state.hid_handle, hid_buf, 1);
                if (ret <= 0)
                    break;
                value = hid_buf[0];

                if (value != hid_msg_id6_value) {
                    msg.valid = 1;
                    msg.ph.msg_type = HID_MSG_HOLD;
                    msg.ph.value    = value;

                    hid_msg_id6_value = value;
                }
                break;

            case HID_MSG_PRIVATE_REPORT_ID:

                ret = HidRead(s_uac_state.hid_handle, hid_buf, 1);
                if (ret <= 0)
                    break;
                pkt_size = hid_buf[0];

                ret = HidRead(s_uac_state.hid_handle, hid_buf, pkt_size);
                if (ret != pkt_size)
                    break;

                memcpy(msg.ph.content, hid_buf, pkt_size);
                msg.ph.len = pkt_size;

                msg.valid = 1;
                msg.ph.msg_type = HID_MSG_PRIVATE;

                break;
            default:
                break;
        }

        if (msg.valid)
            uac_mode_hid_msg_produce(&msg);
    }
}

static void parse_uac_mode_hid_msg(void)
{
    parse_uac_mode_hid_msg_in_huachaungshixun_protocol();

    return ;
}
#endif
#endif

//=================================================================================================

// 临时使用，等 audio in 提供 API 后，再去除
#include <audio_in_regs.h>
#include <base_addr.h>

static unsigned int audioin_intr_interval;
static unsigned int audioin_stable_intr_times;
static unsigned int audioin_callback_intr_interval;
static unsigned int trigger_time;

static int _UacCalculateAudioinCallbackIntrInterval(void)
{
    int i = 1;

    while (1) {
        if ((audioin_intr_interval * i) / (VSP_FRAME_LENGTH * CONFIG_VSP_FRAME_NUM_PER_CONTEXT) == 1)
            break;
        i++;
    }

    return i;
}

static int _UacCalculateAudioinStableIntrTimes(void)
{
    int i = 1;

    while (1) {
        if ((i * audioin_intr_interval) % (VSP_FRAME_LENGTH * CONFIG_VSP_FRAME_NUM_PER_CONTEXT) == 0)
            break;
        i++;
    }

    return i;
}

/*
 * 功能简介：
 *      UAC 只有上行时，为了能实时监控本地可用数据量，需要周期性的确认 context buff 中可用数据量，
 *   但是这个周期性的操作不能由 count 实现的定时器来做，而只能由借助于 audio in/out 模块来做，
 *   这是因为此时只有 audio in/out 的时钟频率跟 usb 消耗数据的速度是同步的。
 */
static int _UacPrepare(void)
{
    volatile AUDIO_IN_REGS *regs = (volatile AUDIO_IN_REGS *)(MCU_REG_BASE_AIN);
    unsigned int pcm_value = regs->pcm_num.value;

#define AUDIO_IN_PCM_ALIGN_SIZE 64
    pcm_value = (pcm_value + (AUDIO_IN_PCM_ALIGN_SIZE - 1)) / AUDIO_IN_PCM_ALIGN_SIZE * AUDIO_IN_PCM_ALIGN_SIZE;
    if (pcm_value % (VSP_SAMPLE_RATE / 1000)) {
        printf ("%s, <Warning> audio in interrupt time interval is not accurate\n", __func__);
	return -1;
    }

    audioin_intr_interval          = pcm_value / (VSP_SAMPLE_RATE / 1000);
    audioin_stable_intr_times      = _UacCalculateAudioinStableIntrTimes();
    audioin_callback_intr_interval = _UacCalculateAudioinCallbackIntrInterval();
    trigger_time = (audioin_stable_intr_times + (audioin_callback_intr_interval - 1)) / audioin_callback_intr_interval;

#if 0
    printf ("%s, audioin intr time : %d, ain period times : %d, send interval : %d, trigger time : %d\n",
            __func__, audioin_intr_interval, audioin_stable_intr_times, audioin_callback_intr_interval, trigger_time);
#endif
    return 0;
}

static int _UacHandleCaptureCallback(void)
{
    static int count;

    if (trigger_time > 0) {
        if (count == trigger_time) {
        AudioBuffProduceNotify(s_uac_state.audio_in_handle, 1);
            count = 0;
        } else
        AudioBuffProduceNotify(s_uac_state.audio_in_handle, 0);

        count++;
    }

    return 0;
}

static int _UacModeProcProcess(volatile VSP_MSG_CPU_MCU *request)
{
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_CAPTURE_FROM_CPU
    VSP_CONTEXT *ctx_addr;
    unsigned int ctx_size;
    unsigned context_index = request->process.context_size;
    if (context_index > 0) {
        context_index = context_index - 1;
        VspGetDDRContext(context_index, &ctx_addr, &ctx_size);

        SAMPLE *out_buffer = DEV_TO_MCU(ctx_addr->out_buffer);
        if (!s_uac_state.mute_enable) {
            AudioBuffProduce(s_uac_state.audio_in_handle, out_buffer, VspGetDDRMicBufferLength() * sizeof(short) / sizeof(SAMPLE));
        }
    }
#endif

    return 0;
}

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK_FORWARD_CPU
static volatile int mcu_send_cmd_count = 0;
static int aout_buff_len;

static int mcu_send_cmd(void)
{
        VSP_COMMAND    *command_addr;
        unsigned int    command_size;

        struct downstream_fifo_info ds_fifo_info;
        VSP_IOC_UAC_INFO tmp_buf_info;

        uac_core_get_downstream_fifo_info(&ds_fifo_info);
        memset(&tmp_buf_info, 0, sizeof(VSP_IOC_UAC_INFO));
        tmp_buf_info.total_size    = aout_buff_len;
        tmp_buf_info.cycle_num     = ds_fifo_info.cycle_times;
        tmp_buf_info.write_offset  = ds_fifo_info.position;

        _UacStatusInfoTranslate(&tmp_buf_info);

#if defined (CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID) && defined (CONFIG_VSP_UAC_MODE_HID_PROTOCOL_HUACHUANGSHIXUN) && defined (CONFIG_VSP_UAC_MODE_ENABLE_HID_DOWNSTREAM)
        struct hid_msg   msg;

        memset(&msg, 0, sizeof(struct hid_msg));
        uac_mode_hid_msg_consume(&msg);
        tmp_buf_info.hid_msg = msg;
#endif

#if defined(CONFIG_VSP_UAC_MODE_PRODUCE_UAC_DOWNSTREAM_CONTROL_MSG) || defined(CONFIG_VSP_UAC_MODE_PRODUCE_UAC_UPSTREAM_CONTROL_MSG)
        if (get_avalible_uci_num()) {
            struct uac_ctrl_info info = {0};

            uci_consume(&info);
            _UacCtrlInfoTranslate(&info, &tmp_buf_info);
        }
#endif

        gx_disable_irq();
        if(VspGetDDRNextCmd(&command_addr, &command_size)) {
            gx_enable_irq();
            return 0;
        }

        command_addr->cmd_id = VCI_UAC_INFO;

        memcpy((void *)DEV_TO_MCU(command_addr->cmd_data), &tmp_buf_info, sizeof(VSP_IOC_UAC_INFO));

        VSP_MSG_MCU_CPU response;

        response.header.type = VMT_MC_SEND_COMMAND;
        response.header.size = sizeof(VMB_MC_SEND_COMMAND) / sizeof(unsigned int);
        response.header.magic = 0;
        response.header.param = 0;
        response.send_command.command_addr = (void *)MCU_TO_DEV(command_addr);
        response.send_command.command_size = command_size;

        while (CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1) == -1);
        gx_enable_irq();

        return 0;
}
#endif

static int _UacModeTask(void)
{
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK_FORWARD_CPU
    if (mcu_send_cmd_count > 0) {
        mcu_send_cmd();

        gx_disable_irq();
        mcu_send_cmd_count--;
        gx_enable_irq();
    }
#endif

    return 0;
}

static int _UacAudioInRecordCallback(int frame_index, void *priv)
{
    if (frame_index > CONFIG_VSP_UAC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF) {
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = frame_index - (CONFIG_VSP_UAC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF + 1);

#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM
        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);
#else
        VspGetDDRContext(context_index, &ctx_addr, &ctx_size);
#endif

        ctx_addr->mic_mask      = AudioInGetMicEnable();
        ctx_addr->ref_mask      = 0xFFFF;
        ctx_addr->frame_index   = context_index * CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
        ctx_addr->ctx_index     = context_index;
        ctx_addr->vad           = 0;
        ctx_addr->kws           = 0;
        ctx_addr->mic_gain      = AudioInGetMicGain();
        ctx_addr->ref_gain      = AudioInGetRefGain();
        ctx_addr->direction     = -1;

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK_FORWARD_CPU
#ifdef CONFIG_VSP_UAC_POST_CONTEXT_TO_CPU_BY_AUDIOIN
        if (!s_uac_state.mute_enable) {
        // Send Context to CPU
            VSP_MSG_MCU_CPU message;
            message.header.type = VMT_MC_PROCESS_DONE;
            message.header.size = sizeof(VMB_MC_PROCESS_DONE) / sizeof(unsigned int);
            message.header.param = VSP_PROCESS_ACTIVE;
            message.header.magic = context_index;
            message.process_done.context_addr = (void *)MCU_TO_DEV(ctx_addr);
            message.process_done.context_size = ctx_size;
            CpuPostVspMessage(&message.header.value, message.header.size + 1);

            _UacHandleCaptureCallback();
        }
#endif

        _UacHandleCaptureCallback();

        mcu_send_cmd_count++;
#else
        // Send a context to DSP
        VSP_MSG_MCU_DSP message;
        message.header.type = VMT_MD_PROCESS;

        PLC_STATUS plc_status = {.vpa_mode = PLC_STATUS_VPA_ACTIVE};
        VspGetPlcStatus(PST_SWITCH_VPA_MODE, &plc_status);
        if (plc_status.vpa_mode == PLC_STATUS_VPA_TALKING) {
            message.header.param = VSP_PROCESS_TALKING;
        } else if (plc_status.vpa_mode == PLC_STATUS_VPA_ACTIVE) {
            message.header.param = VSP_PROCESS_ACTIVE;
        }
        message.header.magic = context_index;
        message.header.size = sizeof(VMB_MD_PROCESS) / sizeof(unsigned int);
        message.process.context_addr = (void *)MCU_TO_DEV(ctx_addr);
        message.process.context_size = ctx_size;
        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);

        _UacHandleCaptureCallback();
#endif

    }

    return 0;
}

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
static void _UacAudioBuffInit(AUDIO_OUT_BUFFER_CONFIG *config)
{
    if (!config) {
        printf ("%s, invalid config pointer\n", __func__);
        return ;
    }

    config->buffer_addr = (const short *)((unsigned int)s_audio_out_buffer - 0x40000000);
    config->buffer_size = AUDIO_OUT_BUFFER_SIZE;


    switch (CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER) {
        case 1:
            config->channel_type = AUDIO_OUT_CHANNEL_MONO;
            break;
        case 2:
            config->channel_type = AUDIO_OUT_CHANNEL_STEREO;
            break;
        default:
            printf ("%s unsupport channel num %u\n", __func__, CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER);
            break;
    }

    switch (VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE) {
        case 8000:
            config->sample_rate = AUDIO_OUT_SAMPLE_RATE_8000;
            break;
        case 16000:
            config->sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
            break;
        case 32000:
            config->sample_rate = AUDIO_OUT_SAMPLE_RATE_32000;
            break;
        case 44100:
            config->sample_rate = AUDIO_OUT_SAMPLE_RATE_44100;
            break;
        case 48000:
            config->sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
            break;
        default:
            printf ("%s unsupport sample rate %u\n", __func__, VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE);
            break;
    }

    return ;
}
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_CAPTURE
#define UAC_CAPTURE_CHANNEL_NUM ((CONFIG_VSP_OUT_INTERLACED_NUM * VSP_SAMPLE_RATE) / VSP_UAC_MODE_CAPTURE_SAMPLE_RATE)
#endif
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
#define UAC_PLAYBACK_CHANNEL_NUM CONFIG_VSP_UAC_MODE_PLAYBACK_CHANNEL_NUMBER
#endif
static void _UacConfig(uac_core_config_t *config, UAC2_CHANNEL_CONFIG *playback, UAC2_CHANNEL_CONFIG *capture)
{
    memset (config, 0, sizeof(uac_core_config_t));
    config->policy = UAC_CLOCK_SYNC_THROUGH_NONE;

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
    config->playback_enable      = 1;
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK_FORWARD_CPU
    VSP_CONTEXT *ctx_addr;
    unsigned int ctx_size;
    VspGetDDRContext(0, &ctx_addr, &ctx_size);
    VSP_CONTEXT_HEADER *ctx_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_addr->ctx_header);

    config->aout_buff            = ctx_header->tmp_buffer;
    config->aout_buff_len        = ctx_header->tmp_buffer_size;

    aout_buff_len                = config->aout_buff_len;
#else
    config->aout_buff            = s_audio_out_buffer;
    config->aout_buff_len        = AUDIO_OUT_BUFFER_SIZE;
#endif

    config->playback_pkt_drop_time_ms = PLAYBACK_DEFAULT_DROP_PKT_TIME_MS;
    config->playback_sample_rate      = playback->sample_rate;
    config->playback_channel_num      = UAC_PLAYBACK_CHANNEL_NUM;
    config->playback_bitwidth         = 16;
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_CAPTURE
    config->capture_enable      = 1;
    config->capture_sample_rate = capture->sample_rate;
    config->capture_channel_num = UAC_CAPTURE_CHANNEL_NUM;
    config->capture_bitwidth    = 16;
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK_FORWARD_CPU
    config->policy = UAC_CLOCK_SYNC_THROUGH_CAPTURE;
#else
    if (config->playback_enable) {
#ifdef CONFIG_VSP_UAC_MODE_PLAYBACK_THROUGH_AUDIOOUT
        config->policy = UAC_CLOCK_SYNC_THROUGH_PLAYBACK;
#else
        config->policy = UAC_CLOCK_SYNC_THROUGH_CAPTURE;
#endif
    } else if (config->capture_enable) {
        config->policy = UAC_CLOCK_SYNC_THROUGH_CAPTURE;
    }
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_DOWNSTREAM_THROUGH_LS
    if (config->policy == UAC_CLOCK_SYNC_THROUGH_NONE)
        config->policy = UAC_CLOCK_SYNC_THROUGH_SOF;

    config->aout_buff            = s_audio_out_buffer;
#endif

    return ;
}

//=================================================================================================

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_UPGRADE
static u8 xfer_buff[64];
#endif

static int _UacModeInit(VSP_MODE_TYPE prev_mode)
{
    printf(LOG_TAG"Init UAC mode\n");

    unsigned int start = get_time_ms();
    memset(&s_uac_state, 0, sizeof(s_uac_state));

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_IRR
    IrrSendInit(CONFIG_BOARD_GPIO_IRR);
#endif
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_MUTE_BTN
    if (GpioEnableTrigger(CONFIG_BOARD_GPIO_MUTE_BTN, GPIO_TRIGGER_EDGE_FALLING, _UacMuteButtonCallback, NULL)) {
        printf(LOG_TAG"GpioEnableTrigger Failed !\n");
        return -1;
    }
#endif

#ifndef CONFIG_VSP_UAC_WITHOUT_DSP
#if defined(CONFIG_VSP_UAC_WORKS_ON_SRAM) || defined(CONFIG_VSP_UAC_WORKS_ON_DDR)
    // Load VPA
    if (VspLoadVpa(VPA_FLASH_OFFSET, s_audio_out_buffer, AUDIO_OUT_BUFFER_SIZE)) {
        printf(LOG_TAG"LoadDSP Failed !\n");
        return -1;
    }
#endif
#endif

#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM
    // Prepare Buffer for SRAM mode
    VspInitSRAMBuffer();
#endif
#ifdef CONFIG_VSP_UAC_WORKS_ON_DDR
    // Prepare Buffer for DDR without Linux mode
    CpuWakeup();
    mdelay(1000);
    VspSetStaticDDRBuffer(CONFIG_VSP_UAC_DDR_START_ADDR);
    VspInitDDRBuffer();
#endif
#ifdef CONFIG_VSP_UAC_WORKS_ON_LINUX
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK_FORWARD_CPU
    if (VspInitCmdDDRBuffer()) {
        printf(LOG_TAG"Failed to initialize Cmd Buffer!\n");
        return -1;
    }
#endif
    VspInitDDRBuffer();
#endif

    // UAC Buffer Initialization
    // Audio Play <- UAC Output, 2 Channels
    s_uac_state.audio_out_handle = AudioBuffMngrInit(s_audio_out_buffer, AUDIO_OUT_BUFFER_SIZE, 1, 16);
    AudioBuffConsumerRegister(s_uac_state.audio_out_handle, (RD_CALLBACK)_UacUsbAudioPlayCallback);
    s_uac2_callback.aout_buff_handle = s_uac_state.audio_out_handle;

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
# ifdef CONFIG_VSP_UAC_MODE_PLAYBACK_THROUGH_AUDIOOUT
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    _UacAudioBuffInit(&buffer_config);
    AudioOutInit(NULL, &buffer_config);
# endif
#endif

    // Audio Out -> UAC Input
#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM
    s_uac_state.audio_in_handle = AudioBuffMngrInit(VspGetSRAMOutPutAddr(), VspGetSRAMMicBufferLength() * sizeof(short) / sizeof(SAMPLE), 0, VspGetSRAMBufferLength());
#else
    s_uac_state.audio_in_handle = AudioBuffMngrInit(VspGetDDROutPutAddr(), VspGetDDRMicBufferLength(), 0, VspGetDDRBufferLength());
#endif

    s_uac2_callback.ain_buff_handle = s_uac_state.audio_in_handle;

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_MASS_STORAGE
    MassstorageInit(CONFIG_VSP_UAC_MODE_MASS_STORAGE_PARTITION_ADDR,
            CONFIG_VSP_UAC_MODE_MASS_STORAGE_PARTITION_SIZE,
            CONFIG_VSP_UAC_MODE_MASS_STORAGE_RESERVED_SIZE_FOR_BADBLOCK);
#endif

    // Start UAC
    UAC2_CHANNEL_CONFIG vsp_to_uac2_channel_config = {0};
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_CAPTURE
    vsp_to_uac2_channel_config.sample_rate = VSP_UAC_MODE_CAPTURE_SAMPLE_RATE;
    vsp_to_uac2_channel_config.channel_mask = (1 << UAC_CAPTURE_CHANNEL_NUM) - 1;
#endif

    UAC2_CHANNEL_CONFIG uac2_to_ref_channel_config = {0};
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK
    uac2_to_ref_channel_config.sample_rate = VSP_UAC_MODE_PLAYBACK_SAMPLE_RATE;
    uac2_to_ref_channel_config.channel_mask = (1 << UAC_PLAYBACK_CHANNEL_NUM) - 1;
#endif

    UsbCompositeInit(&vsp_to_uac2_channel_config, &uac2_to_ref_channel_config,
            &_uac2_device_description, &s_uac2_callback,
            NULL);

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID
    // Start HID
    s_uac_state.hid_handle = HidOpen();
#endif

#if defined(CONFIG_VSP_UAC_MODE_ENABLE_LED_PLAYER) || defined(CONFIG_VSP_UAC_USING_GADGET_SERIAL)
    GsOpen();
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_DOWNSTREAM_THROUGH_LS
    UsbLSOpen();
#endif

#ifndef CONFIG_VSP_UAC_WITHOUT_DSP
    mdelay(50); // wait LOAD_DSP to initialize
#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM
    if (VspInitializeVpa(VspGetSRAMContextHeader(), _UacDspCallback, NULL)) {
        printf(LOG_TAG"Failed to initialize VPA for SRAM!\n");
        return -1;
    }
#else
    if (VspInitializeVpa(VspGetDDRContextHeader(), _UacDspCallback, NULL)) {
        printf(LOG_TAG"Failed to initialize VPA for DDR!\n");
        return -1;
    }
#endif
#endif

    // Start Audio In
    // Audio In should be put at the tail.
    AUDIO_IN_CONFIG audio_in_config;
#ifdef CONFIG_VSP_UAC_WORKS_ON_SRAM
    VspGetSRAMAudioInConfig(&audio_in_config);
#else
    VspGetDDRAudioInConfig(&audio_in_config);
#endif
    audio_in_config.record_callback = _UacAudioInRecordCallback;

    if (AudioInInit(&audio_in_config, 0)) {
        printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
        return -1;
    }

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_AGC
    AudioInAgcConfig(AUDIO_IN_AGC_CONFIG_STATE_ENABLE, NULL);
#endif

#if defined(CONFIG_VSP_UAC_MODE_ENABLE_CAPTURE) && !defined(CONFIG_VSP_UAC_MODE_ENABLE_PLAYBACK)
    // In recorded-only scenes
    if (_UacPrepare()) {
        printf ("UAC Mode Init Failed, Please Change Your Config In Menuconfig To Ensure That The Following Equation Holds \n");
        printf ("((((\"Frame Number in a Context\" * \"Frame Length selects\"  * \"Sample Rate selects\" * 2 / 1000) / 128 * 32) + (64 - 1)) / 64 * 64) %% (\"Sample Rate selects\" / 1000) == 0\n");
        return -1;
    }
#else
    _UacPrepare();
#endif

    s_uac_state.uac_state = 1;

    uac_core_config_t config;
    _UacConfig(&config, &uac2_to_ref_channel_config, &vsp_to_uac2_channel_config);
    uac_core_init(&config);

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_UPGRADE
    UsbslaveInit();
#endif

    VspInitializePlcEvent();
    printf(LOG_TAG"Init UAC mode done with %d ms\n", get_time_ms() - start);
    return 0;
}

//-------------------------------------------------------------------------------------------------

static void _UacModeDone(VSP_MODE_TYPE next_mode)
{
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_UPGRADE
    UsbslaveDestroy();
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID
    HidClose(s_uac_state.hid_handle);
#endif

#if defined(CONFIG_VSP_UAC_MODE_ENABLE_LED_PLAYER) || defined(CONFIG_VSP_UAC_USING_GADGET_SERIAL)
    GsClose();
#endif
    UsbCompositeDone();

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_MASS_STORAGE
    MassstorageDone();
#endif

    AudioInDone();

    AudioBuffMngrDone(s_uac_state.audio_in_handle);
    AudioBuffMngrDone(s_uac_state.audio_out_handle);

    VspCleanupVpa();

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_MUTE_BTN
    GpioDisableTrigger(CONFIG_BOARD_GPIO_MUTE_BTN);
#endif

    memset(&s_uac_state, 0, sizeof(s_uac_state));
    printf(LOG_TAG"Exit UAC mode\n");
}

//=================================================================================================

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_LED_PLAYER

static VSP_LED_QUEUE s_vsp_led_queue = {
    .version = VSP_LED_VERSION,
    .read = 0,
    .write = 0,
    .frame_num = CONFIG_VSP_LED_QUEUE_LENGTH,
    .pixel_num = CONFIG_VSP_LED_NUM
};

#define VSP_LED_FRAME_LENGTH (CONFIG_VSP_LED_NUM * sizeof(VSP_LED_PIXEL) + sizeof(VSP_LED_FRAME_HEADER))
static unsigned char s_vsp_led_frame[CONFIG_VSP_LED_QUEUE_LENGTH][VSP_LED_FRAME_LENGTH];

static int _UacGsFetchLedFrame(void)
{
    int read_cnt = VSP_LED_FRAME_LENGTH;
    char read_buf[VSP_LED_FRAME_LENGTH];

    read_cnt = GsRead(read_buf, read_cnt);

    if (read_cnt == VSP_LED_FRAME_LENGTH && ((s_vsp_led_queue.write + 1) % s_vsp_led_queue.frame_num) != s_vsp_led_queue.read) {
        memcpy(s_vsp_led_frame + s_vsp_led_queue.write, read_buf, VSP_LED_FRAME_LENGTH);
        s_vsp_led_queue.write = (s_vsp_led_queue.write + 1) % s_vsp_led_queue.frame_num;
    }

    return 0;
}

static const VSP_LED_TASK s_uac_led_task = {
    .mode = VSP_LED_MODE_SLAVE,
};

#endif

//-------------------------------------------------------------------------------------------------
static void _UacUpdateUI(void)
{
    unsigned int usb_current_state = 0;

    if (s_uac_state.vad_enable) {
        usb_current_state = VSP_UAC_SUB_MODE_STATE_VAD;
    }
    else if (s_uac_state.kws_enable) {
        usb_current_state = VSP_UAC_SUB_MODE_STATE_KWS_ENABLE;
    }
    else if (s_uac_state.mute_enable) {
        usb_current_state = VSP_UAC_SUB_MODE_STATE_MUTE;
    }
    else {
        usb_current_state = Uac2GetState();
    }

    int sub_mode_current_index = -1;

    for (unsigned int i = 0; i < sizeof(s_uac_sub_mode_list) / sizeof(VSP_UAC_SUB_MODE_INFO); ++i) {
        if(usb_current_state == s_uac_sub_mode_list[i].state) {
            sub_mode_current_index = i;
            break;
        }
    }

    if (sub_mode_current_index != -1) {
        // switch to current mode
        if (sub_mode_current_index != s_uac_state.sub_mode_last_index) {
            s_uac_state.sub_mode_last_index = sub_mode_current_index;
        }
    } else {
        // switch to sub-idle mode
        s_uac_state.sub_mode_last_index = 0;
    }

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_LED_PLAYER
    _UacGsFetchLedFrame();
    VspLedFlush();
#else
    LedSetPixel(0, s_uac_sub_mode_list[s_uac_state.sub_mode_last_index].pixel);
    LedFlush();
#endif

    if (s_uac_sub_mode_list[s_uac_state.sub_mode_last_index].proce)
        s_uac_sub_mode_list[s_uac_state.sub_mode_last_index].proce();
}

static void _UacModeTick(void)
{
    VspPlcEventTick();
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_UPGRADE
    int  len;

    memset(xfer_buff, 0, sizeof(xfer_buff));
    len = UsbslaveReadNoblock(xfer_buff, sizeof(xfer_buff));
    if (len > 0) {
        if (strcmp((char *)xfer_buff, "upgrade") == 0) {
            printf ("Received upgrade request\n");
            VspExit(VSP_EXIT_TO_ENTER_UPGRADE);
        }
    }
#endif

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_MASS_STORAGE
    MassstorageProcess();
#endif

    if (s_uac_state.uac_state && !s_uac_state.led_state) {
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_LED_PLAYER
        VspLedSetCpuQueue(&s_vsp_led_queue, (void *)s_vsp_led_frame);
        VspLedPlay(&s_uac_led_task);
#else
        LedInit();
#endif
        s_uac_state.led_state = 1;
    }

    _UacUpdateUI();

    if (!s_uac_state.uac_state && s_uac_state.led_state) {
#ifdef CONFIG_VSP_UAC_MODE_ENABLE_LED_PLAYER
        VspLedStop();
#else
        LedDone();
#endif
        s_uac_state.led_state = 0;
    }

#if defined (CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID) && defined (CONFIG_VSP_UAC_MODE_HID_PROTOCOL_HUACHUANGSHIXUN)
    VSP_PARAM_UAC_MODE_MSG msg;

    if (uac_mode_arm_msg_is_available()) {
        uac_mode_arm_msg_consume(&msg);
        parse_uac_mode_arm_msg(&msg);
    }

#ifdef CONFIG_VSP_UAC_MODE_ENABLE_HID_DOWNSTREAM
    parse_uac_mode_hid_msg();
#endif
#endif

    uac_core_task();
    AudioBuffPrintStatus(0);

#ifdef CONFIG_VSP_PLC_ENABLE_SYS_EVENT
    if (usb_dev_status.changed == 1) {
        usb_dev_status.changed = 0;
        if (usb_dev_status.st.status == 3) {

        } else if (usb_dev_status.st.status == 0) {
            PLC_EVENT plc_event = {.event_id = UAC_DOWN_STREAM_OFF, .ctx_index = 0};
            VspTriggerPlcEvent(plc_event);
        }
    }
    if (usb_uac_ds_status.changed == 1) {
        usb_uac_ds_status.changed = 0;
        if (usb_uac_ds_status.st.enable == 1) {
            PLC_EVENT plc_event = {.event_id = UAC_DOWN_STREAM_ON, .ctx_index = 0};
            VspTriggerPlcEvent(plc_event);
        } else {
            PLC_EVENT plc_event = {.event_id = UAC_DOWN_STREAM_OFF, .ctx_index = 0};
            VspTriggerPlcEvent(plc_event);
        }
    }

#endif

    _UacModeTask();
}

#if defined (CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID) && defined (CONFIG_VSP_UAC_MODE_HID_PROTOCOL_HUACHUANGSHIXUN)
static int _UacModeProcSetParam(volatile VSP_MSG_CPU_MCU *request)
{
    VSP_PARAM_UAC_MODE_MSG *msg;

    if ((request->set_param.param_addr == NULL) || (request->set_param.param_size != sizeof(VSP_PARAM_UAC_MODE_MSG)))
        return -1;

    msg = (VSP_PARAM_UAC_MODE_MSG *)DEV_TO_MCU(request->set_param.param_addr);
    switch (msg->type) {
        case UAC_MODE_MSG_TYPE_HID:
            uac_mode_arm_msg_produce(msg);
            break;
        case UAC_MODE_MSG_TYPE_NONE:
        default:
            break;
    }

    VSP_MSG_MCU_CPU message;
    message.header.type = VMT_MC_SET_PARAM_DONE;
    message.header.size = sizeof(VMB_MC_SET_PARAM_DONE) / sizeof(unsigned int);
    message.header.magic = request->header.magic;
    message.header.param = request->header.param;
    message.set_param_done.param_addr = request->set_param.param_addr;
    message.set_param_done.param_size = request->set_param.param_size;
    CpuPostVspMessage(&message.header.value, message.header.size + 1);

    return 0;
}
#endif

static int _UacModeProc(volatile VSP_MSG_CPU_MCU *request)
{
    switch (request->header.type) {
        case VMT_CM_PROCESS:
            return _UacModeProcProcess(request);
        case VMT_CM_SET_PARAM:
#if defined (CONFIG_VSP_UAC_MODE_ENABLE_SEND_HID) && defined (CONFIG_VSP_UAC_MODE_HID_PROTOCOL_HUACHUANGSHIXUN)
            return _UacModeProcSetParam(request);
#else
            break;
#endif
        default:
            return -1;
    }
    return 0;

}
//=================================================================================================

const VSP_MODE_INFO vsp_uac_mode_info = {
    .type = VSP_MODE_UAC,
    .init = _UacModeInit,
    .done = _UacModeDone,
    .proc = _UacModeProc,
    .tick = _UacModeTick,
};
