/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_null.c
 *
 */

#include <common.h>
#include <driver/uart.h>
#include <vsp_context.h>
#include "vsp_hook.h"
#include "../vsp_buffer.h"

#define LOG_TAG "[HOOK]"

//=================================================================================================
#define DSP_PROCESS_DONE_EVENT_ID     (98)  // Used to notify board to send wav to bluetooth

static void UartWrite(int port, unsigned char *buf, int len)
{
    int i = 0;
    for(i = 0; i < len; i++)
        UartSendByte(0, buf[i]);
}

static void UartSendMsg(unsigned char msg)
{
    unsigned char send_msg[] = {0x55, 0x55, 0x08, 0x01, 0x00, 0x00, 0xAA, 0xAA};
    send_msg[4] = msg;
    send_msg[5] = send_msg[3]^send_msg[4];
    UartWrite(0, send_msg, send_msg[2]);
}

// Hook Event Process
static int g_vad = 0;
static int g_count_vad_end = 0;
static int g_count_vad_start = 0;
static int g_last_vad = 0;
static int _HookProcessUart(unsigned int event_id, unsigned int ctx_index)
{
    if(event_id >= 100)
    {
        UartSendMsg(event_id);
    }
    
    if(event_id == DSP_PROCESS_DONE_EVENT_ID)
    {
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size = 0;
        VspGetSRAMContext(ctx_index, &ctx_addr, &ctx_size);

        int vad = ctx_addr->vad;
        if(g_vad != vad)
        {
            g_count_vad_end = 0;
            g_count_vad_start = 0;
        }

        if(vad == 0)
        {
            g_count_vad_end++;
        }

        if(vad == 1)
        {
            g_count_vad_start++;
        }

        if(g_count_vad_end > 5)
        {
            if(g_last_vad == 1)
            {
                g_last_vad = 0;
                UartSendMsg(0x91);
                printf("vad end\n");
            }
        }

        if(g_count_vad_start > 5)
        {
            if(g_last_vad == 0)
            {
                g_last_vad = 1;
                UartSendMsg(0x90);
                printf("vad start\n");
            }
        }

        g_vad = vad;
    }

    return 0;
}

int HookProcessInit(void)
{
    UartInit(0, 115200);
    return 0;
}

int HookEventResponse(PLC_EVENT plc_event)
{
    _HookProcessUart(plc_event.event_id, plc_event.ctx_index);
    return 0;
}

int HookProcessTick(void) { return 0;}

