/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_bluetooth_v1_0.c
 *
 */

#include <stdio.h>
#include <string.h>

#include <autoconf.h>
#include <vsp_message.h>
#include <vsp_context.h>
#include <vsp_ext.h>
#include "../common/vsp_vpa.h"

#include <driver/misc.h>
#include <driver/irr.h>

#include "../vsp_buffer.h"
#include "../common/vsp_plc_event.h"
#include "vsp_hook.h"

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uart.h>
#include <driver/uart_message.h>
#include <driver/imageinfo.h>
#include <driver/oem.h>
#include <driver/irq.h>
#include <driver/timer.h>
#include <driver/watchdog.h>
#include <driver/usb_gadget.h>
#include <driver/sw_fifo.h>

#include "../common/vsp_agc.h"

#define LOG_TAG "[HOOK]"

//=================================================================================================
// 0:BT connect; 1:BT connected，BLE unconnected; 2:BLE connected，BT unconnected; 3:BT & BLE connected;
// 0x11:Genie APP connected;
static int s_connect_state = -1;
// 0:stop wav stream; 1:start pcm wav stream; 2:start opus wav stream; 3:start 2-channel opus stream;
// 0x10:BT phone ring started; 0x11:BT phone ring ended;
static int s_wav_stream_state = 0;
// 0: pause; 1: play
static int s_music_state = -1;
// 0: close; 1: open
static int s_mic_state = -1;
// led state, default;
static int s_led_state = -1;
// MTC;
static int s_mtc_flag = 0;
// 0: invalid; 
static int s_plc_event_ctx_index = 0;
#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL
static SW_FIFO s_pre_roll_fifo;
#endif
//=================================================================================================

#define BT_CONNECT_TIMEOUT_MAX (4000/60) // 4 seconds
static int s_bt_connect_timeout = 0;
static char _UartGetState(void)
{
    switch (UartGetCommandID()) {
        case UCI_NOTIFY_RX_CONNECT_STATE:
        {
            s_connect_state = UartGetCommandState();
            if (s_connect_state == 0x11) { // Genie APP connected
                s_bt_connect_timeout = BT_CONNECT_TIMEOUT_MAX;
            }
            break;
        }
        case UCI_NOTIFY_RX_WAV_STREAM_STATE:
        {
            int wav_state = UartGetCommandState();
            PLC_STATUS plc_status = {.vpa_mode = PLC_STATUS_VPA_ACTIVE};
            if (wav_state == 0x11) {// Phone ring start
                s_plc_event_ctx_index = 0;
                plc_status.vpa_mode = PLC_STATUS_VPA_TALKING;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Disable AGC
                AUDIO_IN_AGC_CONFIG config = {
                    .mode         = AUDIO_IN_AGC_MODE_DISABLE,
                };
                VspAudioInInitAgcConfig(&config);
                AudioInSetMicGain(30); // 恢复板级中设置的MIC和REF增益
                AudioInSetRefGain(2);
#endif
            }
            else if (wav_state == 0x01) { // Start pcm wave (BT sco connected)
                s_plc_event_ctx_index = 0;
                if (s_wav_stream_state == 0x11) {
                    break; // Do not switch plc status
                }
                else {
                    plc_status.vpa_mode = PLC_STATUS_VPA_TALKING;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Disable AGC
                AUDIO_IN_AGC_CONFIG config = {
                    .mode         = AUDIO_IN_AGC_MODE_DISABLE,
                };
                VspAudioInInitAgcConfig(&config);
                AudioInSetMicGain(30); // 恢复板级中设置的MIC和REF增益
                AudioInSetRefGain(2);
#endif
                }
            }
            else if (wav_state == 0x00) { // Stop sending speech
                s_plc_event_ctx_index = 0;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Enable AGC
                VspInitGainControl(NULL);
                VspAudioInInitAgcConfig(NULL);
                VspAudionInSetAgcSmoothParam(50);
#endif
            }
            else if (wav_state == 0x03) { // Start 2mic opus
                s_plc_event_ctx_index = 0;
                plc_status.vpa_mode = PLC_STATUS_VPA_TEST;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Disable AGC
                AUDIO_IN_AGC_CONFIG config = {
                    .mode         = AUDIO_IN_AGC_MODE_DISABLE,
                };
                VspAudioInInitAgcConfig(&config);
                AudioInSetMicGain(30); // 恢复板级中设置的MIC和REF增益
                AudioInSetRefGain(2);
#endif
            }
            else if (wav_state == 0x02) {
                s_plc_event_ctx_index = 0;
            }

            VspSetPlcStatus(PST_SWITCH_VPA_MODE, &plc_status);
            s_wav_stream_state = wav_state;
            break;
        }
        case UCI_NOTIFY_RX_MUSIC_STATE:
        {
            s_music_state = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_MIC_STATE:
        {
            s_mic_state = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_LED_STATE:
        {
           s_led_state = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_MTC_STATE:
        {
            s_mtc_flag = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_QUERY_IMAGEINFO:
        {
            if (UartGetCommandState() == UCA_QUERY_IMAGEINFO){
                UartSendImageinfo();
            }
            break;
        }
        default:
            break;
    }

    return 0;
}

static int _HookProcessUartSendWav(char *wav_addr, unsigned int wav_length)
{
    return UartSendWav(wav_addr, wav_length);
}

static int _HookProcessUartSendWavEnd(void)
{
    return UartSendWavEnd();
}

static int _HookProcessUartSendOpusData(char *data_addr, unsigned int data_length)
{
    if (s_wav_stream_state == 0x03) {
        return UartSendCommandWithData(UCI_NOTIFY_TX_2MIC_OPUS_DATA, (unsigned char *)data_addr, data_length);
    }
    else {
        return UartSendCompressedAudio(data_addr, data_length);
    }
}

//=================================================================================================
// Hook Event Process
#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA)
#define DEINTERLACE_BUFF_LEN  1024
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN];
# endif
static int _PlcUartSendOutputData(int value)
{
#ifdef CONFIG_VSP_CF_BT_V1_0_ENABLE_UART_SEND_OUTPUT_DATA
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    
    VspGetSRAMContext(s_plc_event_ctx_index, &ctx_buffer, &ctx_size);
    VSP_CONTEXT_HEADER *context_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_buffer->ctx_header);
    switch (value) {
        case UPR_SEND_WAV:
            {
                if (s_plc_event_ctx_index == 0) break;
                int frame_length       = context_header->frame_length * context_header->sample_rate / 1000;
                int context_sample_num = frame_length * context_header->frame_num_per_context;
#if defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA
                short *send_addr = (short *)DEV_TO_MCU(ctx_buffer->out_buffer);
#elif defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_MIC_CHANNLE_DATA
                short *send_addr = (((short *)DEV_TO_MCU(context_header->mic_buffer)) + context_sample_num * current_context_index);
#endif

#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA)
                if (context_sample_num > DEINTERLACE_BUFF_LEN) {
                    printf(LOG_TAG"s_deinterlace_buff is smaller\n");
                    return -1;
                }

                //Deinterlace
                int out_num = context_header->out_num;
                for(int i = 0; i < context_sample_num; i++) {
                    s_deinterlace_buff[i] = send_addr[out_num*i];
                }
                _HookProcessUartSendWav((char *)s_deinterlace_buff, context_sample_num * sizeof(short));
#else
                _HookProcessUartSendWav((char *)send_addr, context_sample_num * sizeof(short));
#endif
                break;
            }
        case UPR_SEND_OPUS_DATA:
            {
                if (s_plc_event_ctx_index == 0) break;
#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL
                unsigned char buf[2*60*2], get_len;
                get_len = (SwFifoLen(&s_pre_roll_fifo) >= sizeof(buf))? sizeof(buf): sizeof(buf)/2;
                SwFifoUserGet(&s_pre_roll_fifo, buf, get_len);
                _HookProcessUartSendOpusData((char *)buf, get_len);
                break;
#else
                VSP_EXT_BUFFER *ext_buffer = DEV_TO_MCU((ctx_buffer->ext_buffer));
                VSP_CODEC_DATA_HEADER *codec_header = &(ext_buffer->buffer.codec_data.codec_header);
                unsigned int output_addr = (unsigned int) DEV_TO_MCU((ext_buffer->buffer.codec_data.data));
                unsigned int frame_num = codec_header->frame_num;
                unsigned int output_length = 0;
                for (int i = 0; i < frame_num; i++) {
                    output_length += ext_buffer->buffer.codec_data.codec_size[i];
                }
                if (output_length) {
                    if (((s_wav_stream_state == 0x03) && (output_length != (frame_num*40*2)))
                        || ((s_wav_stream_state == 0x02) && (output_length != (frame_num*40)))) {
                        // Filter invalid packet length
                        break;
                    }
                    printf(LOG_TAG"frame_num: %d, output_length: %d\n", frame_num, output_length);
                    _HookProcessUartSendOpusData((char *)output_addr, output_length);
                }
                break;
#endif
            }
        case UPR_SEND_WAV_END:
            s_plc_event_ctx_index = 0;
            // _HookProcessUartSendWavEnd();
            break;
        default:
            break;
    }

#endif

    return 0;
}

#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL

static unsigned int s_wakeWordIndexOffset = 0, s_wakeWordIndexCount = 0;

static void _HookWakeWordIndexSet(void)
{
    long long beginSampleIndex = 0, endSampleIndex = 0;

    // Get WWE sample index at the end of ext_buffer
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    VspGetSRAMContext(s_plc_event_ctx_index, &ctx_buffer, &ctx_size);
    if (s_plc_event_ctx_index == 0) return;
    VSP_EXT_BUFFER *ext_buffer = DEV_TO_MCU((ctx_buffer->ext_buffer));
    VSP_CONTEXT_HEADER *context_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_buffer->ctx_header);
    unsigned char* output_addr = (unsigned char *) DEV_TO_MCU((ext_buffer->buffer.codec_data.data));
    beginSampleIndex = *(long long *)(output_addr + context_header->ext_buffer_size/2);
    endSampleIndex = *(long long *)(output_addr + context_header->ext_buffer_size/2 + sizeof(beginSampleIndex));
    printf(LOG_TAG"beginSampleIndex=%lld, endSampleIndex=%lld\n", beginSampleIndex, endSampleIndex);

    // Update local sample index and discard the extra data except 500ms pre-roll before the wake word is detected
    #define SAMPLES_PER_MS (16)
    #define PRE_ROLL_MS (500)
    #define BYTES_PER_SAMPLE (2)
    #define OPUS_FRAME_MS (20)
    #define PRE_ROLL_SAMPLES (SAMPLES_PER_MS*PRE_ROLL_MS)
    #define OPUS_FRAME_SAMPLES (SAMPLES_PER_MS*OPUS_FRAME_MS)
    if (beginSampleIndex && endSampleIndex) {
        s_wakeWordIndexCount = (unsigned int)(endSampleIndex&0xFFFFFFFF) - (unsigned int)(beginSampleIndex&0xFFFFFFFF);
        unsigned int fifoIndexCount = SwFifoLen(&s_pre_roll_fifo) / BYTES_PER_SAMPLE * SAMPLES_PER_MS;
        if (fifoIndexCount > (s_wakeWordIndexCount + PRE_ROLL_SAMPLES)) {
            unsigned int consume_index = (fifoIndexCount - s_wakeWordIndexCount - PRE_ROLL_SAMPLES) / OPUS_FRAME_SAMPLES; // Aligned to 20ms(OPUS frame length).
            s_wakeWordIndexOffset = (fifoIndexCount - s_wakeWordIndexCount) % OPUS_FRAME_SAMPLES; // Total pre-roll sampleIndex in fifo
            s_wakeWordIndexOffset = (s_wakeWordIndexOffset != 0)? (OPUS_FRAME_SAMPLES - s_wakeWordIndexOffset + PRE_ROLL_SAMPLES) : PRE_ROLL_SAMPLES;
            SwFifoConsume(&s_pre_roll_fifo, consume_index * 40); // Discard extra data except 500ms pre-roll, 40bytes data per 20ms opus frame.
        }
        else {
            s_wakeWordIndexOffset = fifoIndexCount - s_wakeWordIndexCount;
        }
    }
    printf(LOG_TAG"s_wakeWordIndexOffset=%d, s_wakeWordIndexCount=%d\n", s_wakeWordIndexOffset, s_wakeWordIndexCount);
}

static void _HookPrerollPut(void)
{
    if (s_plc_event_ctx_index == 0) return;
    
    // Put data to the end of fifo.
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    VspGetSRAMContext(s_plc_event_ctx_index, &ctx_buffer, &ctx_size);
    VSP_EXT_BUFFER *ext_buffer = DEV_TO_MCU((ctx_buffer->ext_buffer));
    VSP_CODEC_DATA_HEADER *codec_header = &(ext_buffer->buffer.codec_data.codec_header);
    unsigned int output_addr = (unsigned int) DEV_TO_MCU((ext_buffer->buffer.codec_data.data));
    unsigned int frame_num = codec_header->frame_num;
    unsigned int output_length = 0;
    for (int i = 0; i < frame_num; i++) {
        output_length += ext_buffer->buffer.codec_data.codec_size[i];
    }
    if (output_length) {
        SwFifoUserPut(&s_pre_roll_fifo, (unsigned char *)output_addr, output_length);
        // Discard the front data of fifo when the free size is not enough.
        if (SwFifoFreeLen(&s_pre_roll_fifo) < (2*60)) {
            SwFifoConsume(&s_pre_roll_fifo, 2*60);
        }
    }
}
#endif

static int _HookProcessUart(unsigned int event_id)
{
    if(event_id > 99) printf(LOG_TAG"event_id = %d\n",event_id);
    int result = -1;
    static unsigned int send_wav_flag = 0;

    if (s_mic_state == 0) {
        send_wav_flag = 0;
        return -1;
    }

    switch(event_id) {
        case 105:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_VOLUME_UP);
            break;
        case 106:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_VOLUME_DOWN);
            break;
        case 103:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_PAUSE);
            break;
        case 104:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_PLAY);
            break;
        case 113:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_NEXT);
            break;
        case 112:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_PREV);
            break;
        case 114:
            UartSendCommand(UCI_NOTIFY_TX_CALLING_STATE, UCA_PHONE_ANSWER);
            break;
        case 115:
            UartSendCommand(UCI_NOTIFY_TX_CALLING_STATE, UCA_PHONE_REJECT);
            break;
        case 101:
        case 100:
        {
#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL
            _HookWakeWordIndexSet();
            if (s_bt_connect_timeout == 0) { // 天猫精灵APP连接4秒后才做唤醒处理
                unsigned char cmdData[9] = {0};
                cmdData[0] = UCA_WAKE_UP;
                memcpy(cmdData + 1, (unsigned char *)&s_wakeWordIndexOffset, 4);
                memcpy(cmdData + 5, (unsigned char *)&s_wakeWordIndexCount, 4);
                UartSendCommandWithData(UCI_NOTIFY_TX_WAKEUP_STATE, cmdData, sizeof(cmdData));
            }
            break;
#else
            if (s_bt_connect_timeout == 0) { // 天猫精灵APP连接4秒后才做唤醒处理
                UartSendCommand(UCI_NOTIFY_TX_WAKEUP_STATE, UCA_WAKE_UP);
            }
#endif
        }
            //s_wav_stream_state = 1;
        case DSP_PROCESS_DONE_EVENT_ID: // event : DSP callback
            {
#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL
                _HookPrerollPut();
#endif
                if (s_bt_connect_timeout > 0) {
                    s_bt_connect_timeout--;
                }
                if (s_wav_stream_state == 1) {
                    send_wav_flag = 1;
                    result = UPR_SEND_WAV;
                } else if ((s_wav_stream_state == 2) || (s_wav_stream_state == 3)) {
                    send_wav_flag = 1;
                    result =  UPR_SEND_OPUS_DATA;
                } else if (send_wav_flag) {
                    send_wav_flag = 0;
                    result =  UPR_SEND_WAV_END;
                }
                break;
            }
        default:
            break;
    }

    _PlcUartSendOutputData(result);
    return result;
}

static void _RebootToFactoryFirmware(void)
{
    extern int _start_boot_switch_;
    *(volatile unsigned int *)&_start_boot_switch_ = 0x42555831; // 写"BUX1"到寄存器

    // Watchdog reset
    WatchdogInit(1, 1, NULL);
    while(1) {}
}

//=================================================================================================
int HookEventResponse(PLC_EVENT plc_event)
{
    _HookProcessUart(plc_event.event_id);
    s_plc_event_ctx_index = plc_event.ctx_index;
    return 0;
}

#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL
// Buffer for 16kHz/16bit opus encoded voice pre-roll
static unsigned int s_pre_roll_buffer[2*CONFIG_VSP_CF_BT_VOICE_PRE_ROLL_TIME/4];
#endif

int HookProcessInit(void)
{
#ifdef CONFIG_VSP_CF_BT_ENABLE_VOICE_PRE_ROLL
    SwFifoInit(&s_pre_roll_fifo, s_pre_roll_buffer, sizeof(s_pre_roll_buffer));
#endif
    return 0;
}

int HookProcessTick(void)
{
    if (UartGetPacket() == 0) {
        _UartGetState();
    }

    return 0;
}

