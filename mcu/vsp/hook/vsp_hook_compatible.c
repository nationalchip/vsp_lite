/* Voice Signal Preprocess
 * copyright (c) 2001-2020 nationalchip co., ltd
 * all rights reserved!
 *
 * vsp_hook_compatible.c
 *
 */


#include <autoconf.h>

#include "vsp_hook.h"

#include <stdio.h>
#include <string.h>
#include <base_addr.h>

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/misc.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uart.h>
#include "../vsp_mode.h"
#include "../vsp_buffer.h"
//#include "vsp_plc_event.h"
#include "mad.h"
#include "vsp_ext.h"
#include <vsp_context.h>
#define LOG_TAG "[PLC_EVENT]"

//=================================================================================================
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_VOICE_BROADCAST

typedef struct {
    unsigned char  event_id;    // Defined in plc_json
    unsigned       reserved:24; // Reserver for future
    unsigned int   wav_offset;  // The wav offset address in flash, the base address is CONFIG_COMPATIBLE_VSP_PLC_LOAD_EVENT_CONFIG_FROM
    unsigned int   wav_size;
} PLC_EVENT_INFO;

static struct {
    PLC_EVENT_INFO event_info[CONFIG_COMPATIBLE_VSP_PLC_EVENT_CONFIG_NUM];
    unsigned char  event_id;
    unsigned char vad;
} s_plc_event = {.event_id = 0};

#if defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_WAV_VOICE_BROADCAST
//=================================================================================================
// Read from flash, to prepare for event response
static unsigned char s_wav_buff[CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE * 2] __attribute__((aligned(8))); // Need 8 bytes aligned

static struct {
    unsigned int   refresh_flag;
    unsigned int   total_frames;
    unsigned int   play_frame_index;
    unsigned int   play_wav_addr;
    unsigned int   wav_offset; // The wav offset address in flash, the base address is CONFIG_COMPATIBLE_VSP_PLC_LOAD_EVENT_CONFIG_FROM
} s_plc_event_play_wav = {.play_wav_addr = (unsigned int)s_wav_buff};

#elif defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_MP3_VOICE_BROADCAST
//=================================================================================================
// Decode MP3
#define MP3_BUFFER_SIZE 4096
#define PCM_BUFFER_SIZE CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE
typedef struct {
    unsigned char mp3_buffer[MP3_BUFFER_SIZE * 2] __attribute__((aligned(8)));
    unsigned char pcm_buffer[PCM_BUFFER_SIZE * 2] __attribute__((aligned(8)));
    unsigned int sample_rate;// The sample rate of mp3
    unsigned int mp3_size;   // The length of mp3
    unsigned int pcm_size;   // The length of pcm
    unsigned int index;      // Decode index
    unsigned int mp3_offset; // The mp3 offset address in flash
    unsigned int read_size;  // The data size that is decoded
    unsigned int frame_size; // Decode frame size
    unsigned int refresh_flag; // 1-->>need to ready data tp play
} MP3_INFO;
static MP3_INFO g_mp3_info;
#endif

#else

# if defined CONFIG_VSP_OUT_INTERLACED
#define DEINTERLACE_BUFF_LEN  1024
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN];
# endif

static struct {
    unsigned char  event_id;
    unsigned char vad;
} s_plc_event = {.event_id = 0};

#endif
//=================================================================================================
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_VOICE_BROADCAST
static int _PlcEventLoadConfig(void)
{
    printf("_PlcEventLoadConfig\n");
    ssize_t offset = CONFIG_COMPATIBLE_VSP_PLC_LOAD_EVENT_CONFIG_FROM;
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return -1;
    }

    return SpiFlashRead(offset, (unsigned char *)(s_plc_event.event_info), CONFIG_COMPATIBLE_VSP_PLC_EVENT_CONFIG_NUM * sizeof(PLC_EVENT_INFO));
}

static int _PlcEventLoadVoiceData(unsigned int voice_data_offset, void* voice_data_addr, unsigned int voice_data_len)
{
    ssize_t offset = CONFIG_COMPATIBLE_VSP_PLC_LOAD_EVENT_CONFIG_FROM;
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return -1;
    }

    int ret = SpiFlashRead(offset + voice_data_offset, (unsigned char *)voice_data_addr, voice_data_len);

    return ret;
}

static int _PlcEventAudioOutCallback(unsigned int sdc_addr, void *priv)
{
#if defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_WAV_VOICE_BROADCAST
    if (s_plc_event_play_wav.play_frame_index < s_plc_event_play_wav.total_frames) {
        s_plc_event_play_wav.refresh_flag = 1;
    }
#elif defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_MP3_VOICE_BROADCAST
    g_mp3_info.refresh_flag = 1; // For play mp3
#else
#error "Unknown Voice Type"
#endif

    return 0;
}

//-------------------------------------------------------------------------------------------------

#if defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_WAV_VOICE_BROADCAST
static int _PlcEventPlayWavByFrameInit(int wav_offset, unsigned  int wav_size)
{
    s_plc_event_play_wav.total_frames     = wav_size / CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE;
    s_plc_event_play_wav.wav_offset       = wav_offset;
    s_plc_event_play_wav.play_frame_index = 0;
    s_plc_event_play_wav.refresh_flag     = 1;

    return 0;
}

static int _PlcEventPlayWavByFrame(void)
{
    if (!s_plc_event_play_wav.refresh_flag) return 0;
    s_plc_event_play_wav.refresh_flag = 0;

    unsigned int play_buffer_offset = 0;
    if (s_plc_event_play_wav.play_frame_index % 2 == 0) {
        play_buffer_offset = 0;
    } else {
        play_buffer_offset = CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE;
    }
    void *play_buffer       = (void *)((unsigned int)s_wav_buff + play_buffer_offset);
    unsigned int wav_offset = s_plc_event_play_wav.wav_offset + s_plc_event_play_wav.play_frame_index * CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE;
    s_plc_event_play_wav.play_frame_index += 1;

    _PlcEventLoadVoiceData(wav_offset, play_buffer, CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE);
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)play_buffer_offset, (CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE / 1024 * 1024));

    return 0;
}

static int _PlcEventProcWav(int wav_offset, unsigned int wav_size)
{
    if (!wav_size) return 0;

    AUDIO_OUT_BUFFER_CONFIG buffer_config = {
#ifdef CONFIG_COMPATIBLE_VSP_PLC_WAV_BROADCAST_SAMPLE_RATE_16K
        .sample_rate = AUDIO_OUT_SAMPLE_RATE_16000,
#elif defined CONFIG_COMPATIBLE_VSP_PLC_WAV_BROADCAST_SAMPLE_RATE_8K
        .sample_rate = AUDIO_OUT_SAMPLE_RATE_8000,
#endif
        .channel_type = AUDIO_OUT_CHANNEL_MONO,
        .buffer_addr = (const short *)MCU_TO_DEV(s_plc_event_play_wav.play_wav_addr),
        .buffer_size = CONFIG_COMPATIBLE_VSP_PLC_WAV_FRAME_SIZE * 2 / 1024 * 1024,
        .play_callback = _PlcEventAudioOutCallback
    };
    AudioOutInit(NULL, &buffer_config);
    AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, 0);

    printf (LOG_TAG"[wav] sample_rate:[%d], channels:%d\n", buffer_config.sample_rate, buffer_config.channel_type);
    _PlcEventPlayWavByFrameInit(wav_offset, wav_size);
    _PlcEventPlayWavByFrame();

    return 0;
}
#elif defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_MP3_VOICE_BROADCAST
static void _LoadMp3Data(unsigned int flash_offset, unsigned char *mp3_buffer, unsigned int mp3_size)
{
    _PlcEventLoadVoiceData(flash_offset, mp3_buffer, mp3_size);
}

static enum mad_flow _PlcEventDecodeMp3Input(void *data, struct mad_stream *stream)
{
    int ret = 0;
    MP3_INFO *mp3_info = data;

    if (s_plc_event.event_id != 0) return MAD_FLOW_STOP;

    int unproc_data_len = stream->bufend - stream->next_frame;
    if ( (mp3_info->read_size + mp3_info->frame_size - unproc_data_len) <= mp3_info->mp3_size) {
        unsigned char * mp3_buffer = &mp3_info->mp3_buffer[mp3_info->index % 2 * MP3_BUFFER_SIZE];
        _LoadMp3Data(mp3_info->mp3_offset + mp3_info->read_size - unproc_data_len, mp3_buffer, mp3_info->frame_size);
        mad_stream_buffer(stream, mp3_buffer, mp3_info->frame_size);

        mp3_info->read_size += mp3_info->frame_size - unproc_data_len;
        mp3_info->index     += 1;
        ret = MAD_FLOW_CONTINUE;
    } else if (mp3_info->read_size < mp3_info->mp3_size) { // Decode unprocessed data in the last frame
        unsigned char * mp3_buffer = &mp3_info->mp3_buffer[mp3_info->index % 2 * MP3_BUFFER_SIZE];
        _LoadMp3Data(mp3_info->mp3_offset + mp3_info->read_size - unproc_data_len, mp3_buffer, mp3_info->mp3_size - mp3_info->read_size + unproc_data_len);
        mad_stream_buffer(stream, mp3_buffer, mp3_info->mp3_size - mp3_info->read_size + unproc_data_len);

        mp3_info->read_size += mp3_info->mp3_size - mp3_info->read_size;
        mp3_info->index     += 1;
        ret = MAD_FLOW_CONTINUE;
    } else {
        ret = MAD_FLOW_STOP;
    }

    return ret;
}

static inline signed int _Scale(mad_fixed_t sample)
{
    /* round */
    sample += (1L << (MAD_F_FRACBITS - 16));

    /* clip */
    if (sample >= MAD_F_ONE)
        sample = MAD_F_ONE - 1;
    else if (sample < -MAD_F_ONE)
        sample = -MAD_F_ONE;

    /* quantize */
    return sample >> (MAD_F_FRACBITS + 1 - 16);
}

static enum mad_flow _PlcEventDecodeMp3Output(void *data, struct mad_header const *header, struct mad_pcm *pcm)
{
    static int write_ptr = 0;
    static int frame_page_index = 1;
    unsigned int channels, samples, sample_rate;
    MP3_INFO *mp3_info = data;
    short *pcm_buffer = (short *)&mp3_info->pcm_buffer[0];

    mad_fixed_t *left_ch  = pcm->samples[0];
    mad_fixed_t *right_ch = pcm->samples[1];
    mp3_info->sample_rate = pcm->samplerate;
    channels = pcm->channels;
    samples  = pcm->length;

    if (mp3_info->pcm_size == 0) {
        write_ptr = 0;
        frame_page_index = 1;
        if (8000 == mp3_info->sample_rate)       sample_rate = AUDIO_OUT_SAMPLE_RATE_8000;
        else if (11025 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_11025;
        else if (16000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
        else if (22050 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_22050;
        else if (24000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_24000;
        else if (32000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_32000;
        else if (44100 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_44100;
        else if (48000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
        else {
            printf (LOG_TAG"Don't support this sample_rate:%d\n", pcm->samplerate);
            return MAD_FLOW_STOP;
        }

        printf (LOG_TAG"[mp3] sample_rate:%d[%d], channels:%d\n", mp3_info->sample_rate, sample_rate, channels);
        AUDIO_OUT_BUFFER_CONFIG buffer_config = {
            .sample_rate = sample_rate,
            .channel_type = channels <= 1 ? AUDIO_OUT_CHANNEL_MONO : AUDIO_OUT_CHANNEL_STEREO,
            .buffer_addr = (const short *)MCU_TO_DEV(mp3_info->pcm_buffer),
            .buffer_size = PCM_BUFFER_SIZE * 2 / 1024 * 1024,
            .play_callback = _PlcEventAudioOutCallback
        };
        AudioOutInit(NULL, &buffer_config);
        AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, -13);
    }

    while (samples--) {
        while (!mp3_info->refresh_flag) mdelay(1);

        pcm_buffer[write_ptr++] = _Scale(*left_ch++);
        mp3_info->pcm_size += 2;

        if (channels == 2) {
            pcm_buffer[write_ptr++] = _Scale(*right_ch++);
            mp3_info->pcm_size += 2;
        }

        if (write_ptr > PCM_BUFFER_SIZE) write_ptr = 0;

        int offset = 0;
        if (mp3_info->pcm_size > frame_page_index * PCM_BUFFER_SIZE) {
            mp3_info->refresh_flag = 0;
            if (frame_page_index % 2) {
                offset = 0;
            } else {
                offset = PCM_BUFFER_SIZE;
            }
            AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)offset, PCM_BUFFER_SIZE);
            frame_page_index++;
        }
    }

    // Play not played data in the last frame
    int offset = 0;
    if (mp3_info->read_size == mp3_info->mp3_size) {
        if (frame_page_index % 2) {
            offset = 0;
        } else {
            offset = PCM_BUFFER_SIZE;
        }
        AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)offset, PCM_BUFFER_SIZE);
        frame_page_index++;
    }

    return MAD_FLOW_CONTINUE;
}

static enum mad_flow _PlcEventDecodeMp3Error(void *data, struct mad_stream *stream, struct mad_frame *frame)
{
#if 0
    MP3_INFO *mp3_info = data;
    printf("[%s] decoding error 0x%04x (%s) at byte offset %u\n",
           __FUNCTION__,
           stream->error, mad_stream_errorstr(stream),
           stream->this_frame - mp3_info->mp3_buffer);
#endif

    return MAD_FLOW_CONTINUE;
}

static int _PlcEventProcMp3(int mp3_offset, unsigned int mp3_size)
{
    if (!mp3_size) return 0;

    g_mp3_info.mp3_size   = mp3_size;
    g_mp3_info.pcm_size   = 0;
    g_mp3_info.index      = 0;
    g_mp3_info.mp3_offset = mp3_offset;
    g_mp3_info.read_size  = 0;
    g_mp3_info.frame_size = MP3_BUFFER_SIZE;
    g_mp3_info.refresh_flag = 1;

    struct mad_decoder decoder;
    mad_decoder_init(&decoder, &g_mp3_info, _PlcEventDecodeMp3Input, 0, 0, _PlcEventDecodeMp3Output, _PlcEventDecodeMp3Error, 0);
    mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);
    mad_decoder_finish(&decoder);

    return 0;
}

#endif
#endif

static int _PlcEventProcVoiceBroadcast(unsigned char event_id)
{
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_VOICE_BROADCAST
    for (int i = 0; i < CONFIG_COMPATIBLE_VSP_PLC_EVENT_CONFIG_NUM; i++) {
        if (s_plc_event.event_info[i].event_id == event_id) {
#if defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_WAV_VOICE_BROADCAST
            _PlcEventProcWav(s_plc_event.event_info[i].wav_offset, s_plc_event.event_info[i].wav_size);
#elif defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_MP3_VOICE_BROADCAST
            _PlcEventProcMp3(s_plc_event.event_info[i].wav_offset, s_plc_event.event_info[i].wav_size);
#else
#error "Unknown Voice Type"
#endif
            break;
        }
    }
#endif
    return 0;
}

static int _PlcEventProcLed(unsigned char event_id)
{
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_LED_RESPONSE
    BoardProcessLed(event_id);
#endif
    return 0;
}

static int _PlcUartSendOutputData(int value)
{
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_UART_SEND_OUTPUT_DATA
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    int current_context_index = 0;
    int max_index_in_channel  = 0;
    static int index = 0;
    VspGetSRAMContext(current_context_index, &ctx_buffer, &ctx_size);
    VSP_CONTEXT_HEADER *context_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_buffer->ctx_header);
    int context_num_per_channel = context_header->frame_num_per_channel / context_header->frame_num_per_context;
    switch (value) {
    case UPR_SEND_WAV:
        {
            if (index == 0) {
                for (int i = 0; i < context_num_per_channel; i++) {
                    VspGetSRAMContext(i, &ctx_buffer, &ctx_size);
                    if (ctx_buffer->ctx_index > max_index_in_channel) max_index_in_channel = ctx_buffer->ctx_index;
                }
                VspGetSRAMContext(max_index_in_channel, &ctx_buffer, &ctx_size);
                current_context_index = max_index_in_channel % context_num_per_channel;
                index = max_index_in_channel + 1;
            } else {
                VspGetSRAMContext(index, &ctx_buffer, &ctx_size);
                current_context_index = index % context_num_per_channel;
                index ++;
            }

            int frame_length       = context_header->frame_length * context_header->sample_rate / 1000;
            int context_sample_num = frame_length * context_header->frame_num_per_context;
#if defined CONFIG_COMPATIBLE_VSP_PLC_MODE_UART_SEND_OUTPUT_CHANNLE_DATA
            short *send_addr = (short *)DEV_TO_MCU(ctx_buffer->out_buffer);
#elif defined CONFIG_COMPATIBLE_VSP_PLC_MODE_UART_SEND_MIC_CHANNLE_DATA
            short *send_addr = (((short *)DEV_TO_MCU(context_header->mic_buffer)) + context_sample_num * current_context_index);
#endif

#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_COMPATIBLE_VSP_PLC_MODE_UART_SEND_OUTPUT_CHANNLE_DATA)
            if (context_sample_num > DEINTERLACE_BUFF_LEN) {
                printf(LOG_TAG"s_deinterlace_buff is smaller\n");
                return -1;
            }

            //Deinterlace
            int out_num = context_header->out_num;
            for(int i = 0; i < context_sample_num; i++) {
                s_deinterlace_buff[i] = send_addr[out_num*i];
            }
            BoardProcessUartSendWav((char *)s_deinterlace_buff, context_sample_num * sizeof(short));
#else
            BoardProcessUartSendWav((char *)send_addr, context_sample_num * sizeof(short));
#endif
            break;
        }
    case UPR_SEND_OPUS_DATA:
        {
            int frame_num_per_context = context_header->frame_num_per_context;
            int frame_num_per_channel = context_header->frame_num_per_channel;
            max_index_in_channel = 0;
            if (index == 0) {
                for (int i = 0; i < context_num_per_channel; i++) {
                    VspGetSRAMContext(i, &ctx_buffer, &ctx_size);
                    if (ctx_buffer->ctx_index > max_index_in_channel) max_index_in_channel = ctx_buffer->ctx_index;
                }
                VspGetSRAMContext(max_index_in_channel, &ctx_buffer, &ctx_size);
                current_context_index = max_index_in_channel * frame_num_per_context % frame_num_per_channel;
                index = max_index_in_channel + 1;
            } else {
                VspGetSRAMContext(index, &ctx_buffer, &ctx_size);
                current_context_index = index * frame_num_per_context % frame_num_per_channel;
                index ++;
            }

            VSP_EXT_BUFFER *ext_buffer = DEV_TO_MCU((ctx_buffer->ext_buffer));
            unsigned int output_addr = (unsigned int) DEV_TO_MCU((ext_buffer->buffer.codec_data.data));
            unsigned int output_length = 0;
            for (int i = 0; i < 3; i++) {
                output_length += ext_buffer->buffer.codec_data.codec_size[i];
            }
            BoardProcessUartSendOpusData((char *)output_addr, output_length);
            break;
        }
    case UPR_SEND_WAV_END:
        index = 0;
        // BoardProcessUartSendWavEnd();
        break;
    default:
        break;
    }

#endif

    return 0;
}

static int _PlcEventProcUart(unsigned char event_id)
{
    int result = -1;
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_UART_RESPONSE

#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_SEND_VAD
    result = BoardProcessUart(event_id, s_plc_event.vad);
#else
    result = BoardProcessUart(event_id);
#endif
#endif

    _PlcUartSendOutputData(result);

    return 0;
}

static int _PlcEventProcIrr(unsigned char event_id)
{
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_IRR_RESPONSE
    BoardProcessIrr(event_id);
#endif
    return 0;
}

static int _PlcEventResponse(unsigned char event_id)
{
    if (event_id == 0) return 0;
    s_plc_event.event_id = 0;

    _PlcEventProcVoiceBroadcast(event_id);
    _PlcEventProcLed(event_id);
    _PlcEventProcUart(event_id);
    _PlcEventProcIrr(event_id);

    return 0;
}

//=================================================================================================

static int _VspTriggerPlcEvent(unsigned char event_id, unsigned char vad)
{
    if (event_id > 99)
        printf("[g800]_event_id = %d\n", event_id); //190906, dcj change here
    s_plc_event.event_id = event_id;
    s_plc_event.vad = vad;
    return 0;
}

#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_VOICE_BROADCAST_FROM_HOST
static int _VspCaptureBoradEvent(void)
{
    int event_id = BoardProcessGetHostEventId();
    if (event_id != 0) s_plc_event.event_id = event_id;
    return 0;
}
#endif

static int _VspInitializePlcEvent(void)
{
#if ((defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_IRR_RESPONSE) || (defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_LED_RESPONSE) \
     || (defined CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_UART_RESPONSE))
    BoardProcessInit();
#endif

#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_VOICE_BROADCAST
    _PlcEventLoadConfig();
#endif

    return 0;
}

static void _VspPlcEventTick(void)
{
#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_WAV_VOICE_BROADCAST
    // Play audio by frame
    _PlcEventPlayWavByFrame();
#endif

#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_UART_RESPONSE
    BoardProcessTick();
#endif

#ifdef CONFIG_COMPATIBLE_VSP_PLC_MODE_ENABLE_VOICE_BROADCAST_FROM_HOST
    _VspCaptureBoradEvent();
#endif

    _PlcEventResponse(s_plc_event.event_id);
}

//=================================================================================================
// Hook Event Process
int HookProcessInit(void)
{
    _VspInitializePlcEvent();
    return 0;
}

int HookEventResponse(PLC_EVENT plc_event)
{
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    VspGetSRAMContext(plc_event.ctx_index, &ctx_buffer, &ctx_size);


    _VspTriggerPlcEvent(plc_event.event_id, ctx_buffer->vad);


    return 0;
}

int HookProcessTick(void)
{
    _VspPlcEventTick();
    return 0;
}

