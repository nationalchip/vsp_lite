/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_null.c
 *
 */

#include "vsp_hook.h"

#define LOG_TAG "[HOOK]"

//=================================================================================================
// Hook Event Process
int HookProcessInit(void) { return 0;}

int HookEventResponse(PLC_EVENT plc_event) { return 0;}

int HookProcessTick(void) { return 0;}

