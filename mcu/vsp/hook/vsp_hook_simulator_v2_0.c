/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_null.c
 *
 */

#include <stdio.h>
#include <string.h>
#include <autoconf.h>
#include <vsp_message.h>
#include <vsp_context.h>
#include <vsp_ext.h>
#include <driver/misc.h>
#include <driver/irr.h>
#include "../vsp_buffer.h"
#include "vsp_hook.h"
#include <board_config.h>
#include <driver/audio_out.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uart.h>
#include "../common/uart_message_v2.h"

#define LOG_TAG "[HOOK]"

typedef enum {
    MSG_NTF_VAD_START     = NEW_MSG(MSG_TYPE_NTF, 0x85),
    MSG_NTF_VAD_END         = NEW_MSG(MSG_TYPE_NTF, 0x86),
    MSG_NTF_USR_CMD         = NEW_MSG(MSG_TYPE_NTF, 0xa0),
} SIMULATOR_UART_MSG_ID;

//=================================================================================================
#define DSP_PROCESS_DONE_EVENT_ID     (98)
static int g_audio_sent_fmt = UPR_SEND_OPUS_DATA;
/*static void  UartMessageSend(MSG_PACK *pack)
{
    pack->msg_header.crc32 = crc32(0, (unsigned char *)(&(pack->msg_header)), 10);

    unsigned char *p = (unsigned char *)pack;
    for (int i = 0; i < 14; i++) 
    {
        UartSendByte(0, *(p+i));
    }

    for (int i = 0; i < pack->len; i++)
    {
        UartSendByte(0, *(pack->body_addr+i));
    }
}*/

static void _Cmd_Send(unsigned int cmd_id)
{
    MSG_PACK msg_pack;
    msg_pack.msg_header.magic     = 0x12345678;
    msg_pack.msg_header.cmd       = MSG_NTF_USR_CMD + cmd_id;
    msg_pack.msg_header.flags     = 0;
    msg_pack.msg_header.length    = 0;
    msg_pack.body_addr            = NULL;
    msg_pack.port                 = UART_PORT0;
    msg_pack.len                  = 0;
    UartMessageAsyncSend(&msg_pack);
    //UartMessageSend(&msg_pack);
}

static void _Vad_Send(unsigned int start)
{
    MSG_PACK msg_pack;
    msg_pack.msg_header.magic     = 0x12345678;
    if(start == 0)
    {
        msg_pack.msg_header.cmd       = MSG_NTF_VAD_END;
    }
    else
    {
        msg_pack.msg_header.cmd       = MSG_NTF_VAD_START;
    }
    msg_pack.msg_header.flags     = 0;
    msg_pack.msg_header.length    = 0;
    msg_pack.body_addr            = NULL;
    msg_pack.port                 = UART_PORT0;
    msg_pack.len                  = 0;
    UartMessageAsyncSend(&msg_pack);
    //UartMessageSend(&msg_pack);
}

static int _UartAudioFmtChange(MSG_PACK * pack, void *priv)
{
    UartSendByte(0, 'A');
    if(pack->len > 0)
    {
        UartSendByte(0, 'B');
        unsigned char fmt = pack->body_addr[0];
        if(fmt == 0x00)
        {
            g_audio_sent_fmt = UPR_SEND_WAV;
        }
        else if(fmt == 0x02)
        {
            g_audio_sent_fmt = UPR_SEND_OPUS_DATA;
        }
    }
    
    return 0;
}

static int _HookProcessUartSendWav(char *wav_addr, unsigned int wav_length)
{
    MSG_PACK msg_pack;
    msg_pack.msg_header.magic     = 0x12345678;
    msg_pack.msg_header.cmd       = MSG_NTF_PCM_DATA;
    msg_pack.msg_header.flags     = 0;
    msg_pack.msg_header.length    = wav_length;
    msg_pack.body_addr            = (unsigned char*)wav_addr;
    msg_pack.port                 = UART_PORT0;
    msg_pack.len                  = wav_length;
    UartMessageAsyncSend(&msg_pack);
    //UartMessageSend(&msg_pack);
    return 0;
}

static int _HookProcessUartSendWavEnd(void)
{
    MSG_PACK msg_pack;
    msg_pack.msg_header.magic     = 0x12345678;
    msg_pack.msg_header.cmd       = MSG_NTF_PCM_DONE;
    msg_pack.msg_header.flags     = 0;
    msg_pack.msg_header.length    = 0;
    msg_pack.body_addr            = NULL;
    msg_pack.port                 = UART_PORT0;
    msg_pack.len                  = 0;
    UartMessageAsyncSend(&msg_pack);
    //UartMessageSend(&msg_pack);
    return 0;
}

static int _HookProcessUartSendOpusData(char *data_addr, unsigned int data_length)
{
    MSG_PACK msg_pack;
    msg_pack.msg_header.magic     = 0x12345678;
    msg_pack.msg_header.cmd       = MSG_NTF_PCM_DATA;
    msg_pack.msg_header.flags     = 0;
    msg_pack.msg_header.length    = data_length;
    msg_pack.body_addr            = (unsigned char*)data_addr;
    msg_pack.port                 = UART_PORT0;
    msg_pack.len                  = data_length;
    UartMessageAsyncSend(&msg_pack);
    //UartMessageSend(&msg_pack);
    return 0;
}

//=================================================================================================
// Hook Event Process
#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA)
#define DEINTERLACE_BUFF_LEN  1024
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN];
# endif
static int _PlcUartSendOutputData(int value)
{
#ifdef CONFIG_VSP_CF_BT_V1_0_ENABLE_UART_SEND_OUTPUT_DATA
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    int current_context_index = 0;
    int max_index_in_channel  = 0;
    static int index = 0;
    VspGetSRAMContext(current_context_index, &ctx_buffer, &ctx_size);
    VSP_CONTEXT_HEADER *context_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_buffer->ctx_header);
    int context_num_per_channel = context_header->frame_num_per_channel / context_header->frame_num_per_context;
    switch (value) {
        case UPR_SEND_WAV:
            {
                if (index == 0) {
                    for (int i = 0; i < context_num_per_channel; i++) {
                        VspGetSRAMContext(i, &ctx_buffer, &ctx_size);
                        if (ctx_buffer->ctx_index > max_index_in_channel) max_index_in_channel = ctx_buffer->ctx_index;
                    }
                    VspGetSRAMContext(max_index_in_channel, &ctx_buffer, &ctx_size);
                    current_context_index = max_index_in_channel % context_num_per_channel;
                    index = max_index_in_channel + 1;
                } else {
                    VspGetSRAMContext(index, &ctx_buffer, &ctx_size);
                    current_context_index = index % context_num_per_channel;
                    index ++;
                }

                int frame_length       = context_header->frame_length * context_header->sample_rate / 1000;
                int context_sample_num = frame_length * context_header->frame_num_per_context;
#if defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA
                short *send_addr = (short *)DEV_TO_MCU(ctx_buffer->out_buffer);
#elif defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_MIC_CHANNLE_DATA
                short *send_addr = (((short *)DEV_TO_MCU(context_header->mic_buffer)) + context_sample_num * current_context_index);
#endif

#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA)
                if (context_sample_num > DEINTERLACE_BUFF_LEN) {
                    printf(LOG_TAG"s_deinterlace_buff is smaller\n");
                    return -1;
                }

                //Deinterlace
                int out_num = context_header->out_num;
                for(int i = 0; i < context_sample_num; i++) {
                    s_deinterlace_buff[i] = send_addr[out_num*i];
                }
                _HookProcessUartSendWav((char *)s_deinterlace_buff, context_sample_num * sizeof(short));
#else
                _HookProcessUartSendWav((char *)send_addr, context_sample_num * sizeof(short));
#endif
                break;
            }
        case UPR_SEND_OPUS_DATA:
            {
                int frame_num_per_context = context_header->frame_num_per_context;
                int frame_num_per_channel = context_header->frame_num_per_channel;
                max_index_in_channel = 0;
                if (index == 0) {
                    for (int i = 0; i < context_num_per_channel; i++) {
                        VspGetSRAMContext(i, &ctx_buffer, &ctx_size);
                        if (ctx_buffer->ctx_index > max_index_in_channel) max_index_in_channel = ctx_buffer->ctx_index;
                    }
                    VspGetSRAMContext(max_index_in_channel, &ctx_buffer, &ctx_size);
                    current_context_index = max_index_in_channel * frame_num_per_context % frame_num_per_channel;
                    index = max_index_in_channel + 1;
                } else {
                    VspGetSRAMContext(index, &ctx_buffer, &ctx_size);
                    current_context_index = index * frame_num_per_context % frame_num_per_channel;
                    index ++;
                }

                VSP_EXT_BUFFER *ext_buffer = DEV_TO_MCU((ctx_buffer->ext_buffer));
                unsigned int output_addr = (unsigned int) DEV_TO_MCU((ext_buffer->buffer.codec_data.data));
                unsigned int output_length = 0;
                for (int i = 0; i < 3; i++) {
                    output_length += ext_buffer->buffer.codec_data.codec_size[i];
                }
                if (output_length)
                    _HookProcessUartSendOpusData((char *)output_addr, output_length);
                break;
            }
        case UPR_SEND_WAV_END:
            index = 0;
             _HookProcessUartSendWavEnd();
            break;
        default:
            break;
    }

#endif

    return 0;
}

// Hook Event Process
static int g_vad = 0;
static int g_count_vad_end = 0;
static int g_count_vad_start = 0;
static int g_last_vad = 0;
static int g_enable_vad = 0;
static int g_send_wav = -1;
static int _HookProcessUart(unsigned int event_id, unsigned int ctx_index)
{
    int result = -1;
    
    if(event_id >= 100)
    {
        unsigned int cmd_id = event_id - 100;
        _Cmd_Send(cmd_id);

        if(event_id == 100)
        {
            g_enable_vad = 1;
            g_count_vad_end = 0;
            g_count_vad_start = 0;
            g_send_wav = 1;
            _Vad_Send(1);
        }
    }
    
    if(event_id == DSP_PROCESS_DONE_EVENT_ID)
    {
        if(g_enable_vad)
        {
            VSP_CONTEXT *ctx_addr;
            unsigned int ctx_size = 0;
            VspGetSRAMContext(ctx_index, &ctx_addr, &ctx_size);

            int vad = ctx_addr->vad;
            if(g_vad != vad)
            {
                g_count_vad_end = 0;
                g_count_vad_start = 0;
            }

            if(vad == 0)
            {
                g_count_vad_end++;
            }

            if(vad == 1)
            {
                g_count_vad_start++;
            }

            if(g_count_vad_end > 5)
            {
                if(g_last_vad == 1)
                {
                    g_last_vad = 0;
                    g_enable_vad = 0;
                    g_send_wav = 0;
                    _Vad_Send(0);
                    //printf("vad end\n");
                }
            }

            if(g_count_vad_start > 5)
            {
                if(g_last_vad == 0)
                {
                    g_last_vad = 1;
                    //printf("vad start\n");
                }
            }

            g_vad = vad;
        }

        if(g_send_wav == 1) 
            result = g_audio_sent_fmt;//UPR_SEND_OPUS_DATA;//UPR_SEND_WAV;
        if(g_send_wav == 0)
        {
            result = UPR_SEND_WAV_END;
            g_send_wav = -1;
        }

        _PlcUartSendOutputData(result);
    }

    return 0;
}

static unsigned char g_fmt_buffer[4] = {0};
static int _UartProcessInit(void)
{
    UART_MSG_INIT_CONFIG config;
    config.port = UART_PORT0;
    config.magic = 0x12345678;
    config.baudrate = 2000000;
    config.reinit_flag = 1;

    UartMessageAsyncInit(&config);

    UART_MSG_REGIST registe;
    
    registe.msg_id = MSG_NTF_AUDIO_CODE_FORMAT;
    registe.port = config.port;
    registe.msg_pack_callback = (MSG_PACK_CALLBACK) _UartAudioFmtChange;
    registe.msg_buffer = g_fmt_buffer;
    registe.msg_buffer_length = sizeof(g_fmt_buffer);
    registe.msg_buffer_offset = 0;
    registe.priv = NULL;
    UartMessageAsyncRegist(&registe);

    return 0;
}


////////////////////////////////////////////////////////////////////////////////////
int HookProcessInit(void) 
{ 
    _UartProcessInit();
    return 0;
}

int HookEventResponse(PLC_EVENT plc_event)
{
    _HookProcessUart(plc_event.event_id, plc_event.ctx_index);
    return 0;
}

int HookProcessTick(void)
{
    UartMessageAsyncTick();
    return 0;
}

