/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_oneshort_v1_0.c
 *
 */

#include <stdio.h>
#include <types.h>

#include "vsp_hook.h"
#include "../common/uart_message_v2.h"
#include "../vsp_buffer.h"

#include <autoconf.h>
#include <board_config.h>
#include <vsp_context.h>

#define LOG_TAG "[HOOK]"

#define _SERIAL_PORT_ 0
#define _BAUDRATE_ 500000

#define _EVENT_CLEAN_KWS_ 300


typedef struct {
    unsigned int pcm_send_flag;
    unsigned int pcm_send_count;
    unsigned int pcm_send_threshold;

    unsigned int kws_send_flag;
    unsigned int kws_value;


} Bd_Handle;
static Bd_Handle s_handle = {
    .pcm_send_flag = 0,
    .pcm_send_count = 0,
    .pcm_send_threshold = 85
};

static unsigned int s_cur_kws_value = 0;
static MSG_PACK s_res_kws_value = {
    .port = _SERIAL_PORT_,
    .msg_header.magic = MSG_HOST_MAGIC,
    .msg_header.cmd = MSG_RSP_KWS_VALUE,
    .msg_header.flags = 1,
    .body_addr = (unsigned char *)&s_cur_kws_value,
    .len = sizeof(s_cur_kws_value)
};
static MSG_PACK s_ntf_kws_value = {
    .port = _SERIAL_PORT_,
    .msg_header.magic = MSG_HOST_MAGIC,
    .msg_header.cmd = MSG_NTF_KWS_VALUE,
    .msg_header.flags = 1,
    .body_addr = (unsigned char *)&s_cur_kws_value,
    .len = sizeof(s_cur_kws_value)
};
static MSG_PACK s_ntf_pcm_data = {
    .port = _SERIAL_PORT_,
    .msg_header.magic = MSG_HOST_MAGIC,
    .msg_header.cmd = MSG_NTF_PCM_DATA,
    .msg_header.flags = 0,
    .body_addr = NULL,
    .len = 0
};

static int _uartRecvCallback(MSG_PACK * pack, void *priv)
{
    if (pack->msg_header.cmd == MSG_REQ_KWS_VALUE) {
        s_res_kws_value.msg_header.seq = pack->msg_header.seq;
        UartMessageAsyncSend(&s_res_kws_value);

        PLC_EVENT plc_event = {.event_id = _EVENT_CLEAN_KWS_};
        VspTriggerPlcEvent(plc_event);
    }

    return 0;
}

//=================================================================================================
// Hook Event Process
int HookProcessInit(void) {

    UartMessageAsyncDone();
    UART_MSG_INIT_CONFIG init_config = {
        .magic = MSG_HOST_MAGIC,
        .port = _SERIAL_PORT_,
        .baudrate = _BAUDRATE_,
        .reinit_flag = 1
    };
    UartMessageAsyncInit(&init_config);

    UART_MSG_REGIST regist_info_1 = {
        .msg_id = MSG_REQ_KWS_VALUE,
        .port = init_config.port,
        .msg_buffer = NULL,
        .msg_buffer_length = 0,
        .msg_pack_callback = _uartRecvCallback,
        .priv = "start"

    };
    UartMessageAsyncRegist(&regist_info_1);

    return 0;
}

#define _SEND_OUTPUT_ 1

#if _SEND_OUTPUT_
#define DEINTERLACE_BUFF_LEN  1024
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN];
#endif

int HookEventResponse(PLC_EVENT plc_event) {

    if (plc_event.event_id >= 100 && plc_event.event_id < 300)
        s_cur_kws_value = plc_event.event_id;

    printf(LOG_TAG"event_id = %d\n", plc_event.event_id);
    switch (plc_event.event_id) {
    case 100:
        UartMessageAsyncSend(&s_ntf_kws_value);
        s_handle.pcm_send_flag = 1;
        s_handle.pcm_send_count = s_handle.pcm_send_threshold;
        break;

    case DSP_PROCESS_DONE_EVENT_ID:
        {
            if (s_handle.pcm_send_count)
                s_handle.pcm_send_count--;
            else
                break;

            VSP_CONTEXT *ctx_buffer;
            unsigned int ctx_size;

            VspGetSRAMContext(plc_event.ctx_index, &ctx_buffer, &ctx_size);
            VSP_CONTEXT_HEADER *context_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_buffer->ctx_header);
            int frame_num_per_context = context_header->frame_num_per_context;
            int frame_num_per_channel = context_header->frame_num_per_channel;
            int frame_length       = context_header->frame_length * context_header->sample_rate / 1000;
            int context_sample_num = frame_length * context_header->frame_num_per_context;
# if _SEND_OUTPUT_ // out put channel
            short *send_addr = (short *)DEV_TO_MCU(ctx_buffer->out_buffer);
# else // mic 0 channel
            short *send_addr = (((short *)DEV_TO_MCU(context_header->mic_buffer)) + \
                                context_sample_num * (plc_event.ctx_index % (frame_num_per_channel / frame_num_per_context)));
# endif

#if 0// if defined (CONFIG_VSP_OUT_INTERLACED) out put channel && INTERLACED
            if (context_sample_num > DEINTERLACE_BUFF_LEN) {
                printf(LOG_TAG"s_deinterlace_buff is smaller\n");
                return -1;
            }

            //Deinterlace
            int out_num = context_header->out_num;
            for(int i = 0; i < context_sample_num; i++) {
                s_deinterlace_buff[i] = send_addr[out_num*i];
            }
            s_ntf_pcm_data.body_addr = (unsigned char*)s_deinterlace_buff;
            s_ntf_pcm_data.len = context_sample_num * sizeof(short);
# else
            s_ntf_pcm_data.body_addr = (unsigned char*)send_addr;
            s_ntf_pcm_data.len = context_sample_num * sizeof(short);
# endif

            printf("%d 0x%x %d\n", plc_event.ctx_index, s_ntf_pcm_data.body_addr, s_ntf_pcm_data.len);
            UartMessageAsyncSend(&s_ntf_pcm_data);
            break;
            break;
        }

    case _EVENT_CLEAN_KWS_:
        s_cur_kws_value = 0;
    default:
        break;

    }

    return 0;
}

int HookProcessTick(void) {
    UartMessageAsyncTick();
    return 0;
}

