/* Voice Signal Preprocess
 * Copyright (C) 2001-2021 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_8008c+bk3266_conference_v1_0.c
 *
 */

#include <stdio.h>
#include <string.h>

#include <autoconf.h>
#include <vsp_message.h>
#include <vsp_context.h>
#include <vsp_ext.h>
#include "../common/vsp_vpa.h"

#include <driver/misc.h>
#include <driver/irr.h>

#include "../vsp_buffer.h"
#include "../common/vsp_plc_event.h"
#include "vsp_hook.h"

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uart.h>
#include <driver/uart_message.h>
#include <driver/imageinfo.h>
#include <driver/oem.h>
#include <driver/irq.h>
#include <driver/timer.h>
#include <driver/watchdog.h>
#include <driver/usb_gadget.h>
#include <driver/uac2_core.h>

#include "../common/vsp_agc.h"

#define LOG_TAG "[HOOK]"


//==========================
// uac
static unsigned int uac_data_cycle;
static unsigned int uac_data_playback_pos_w;
struct uac_core_data_update_cbk ucd_cbk;

struct uac_core_data_update_info uac_data_info = {
    .version = 0xFF01,
};

//=================================================================================================
#define __mic 1
#define __uac 0
#define __out 0
#define __uac_out 0

#if __mic
# define _SAMPLE_RATE               16000
# define _BIT_WIDTH                 16
# define _CHANNEL_NUMBER            1
#endif

#if __uac
# define _SAMPLE_RATE               16000
# define _BIT_WIDTH                 16
# define _CHANNEL_NUMBER            1
#endif

#if __out
# define _SAMPLE_RATE               16000
# define _BIT_WIDTH                 16
# define _CHANNEL_NUMBER            1
#endif

#if __uac_out
# define _SAMPLE_RATE               48000
# define _BIT_WIDTH                 16
# define _CHANNEL_NUMBER            2
#endif


// 为方便双声道播放时对齐数据 AUDIO_BUFF_STORE_TIME_MS 应设为 AUDIO_BUFF_STORE_TIME_MS 的 1/2
// AUDIO_BUFF_STORE_TIME_MS 应设为context对应音频时长
// AUDIO_BUFF_STORE_TIME_MS 为uac模式配置，可在make menuconfig 的CONFIG_VSP_UAC_MODE_PLAYBACK_AUDIO_BUFF_STORE_TIME_MS选项配置
# define AUDIO_BUFF_STORE_TIME_MS   (VSP_FRAME_LENGTH * CONFIG_VSP_FRAME_NUM_PER_CONTEXT)//ms
# define AUDIO_OUT_BUFFER_SIZE (_CHANNEL_NUMBER * _SAMPLE_RATE / 1000 * (_BIT_WIDTH / 8) * AUDIO_BUFF_STORE_TIME_MS * 2)
static char s_audio_out_buffer[AUDIO_OUT_BUFFER_SIZE] __attribute__((aligned(1024))) = {0};
static unsigned int s_ping_pang = 1;

#define DEINTERLACE_BUFF_LEN  (_SAMPLE_RATE / 1000 * (_BIT_WIDTH / 8) * AUDIO_BUFF_STORE_TIME_MS)
#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V1_0_UART_SEND_OUTPUT_CHANNLE_DATA)
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN];
#endif

static unsigned char *uac_data_buffer = NULL;
static unsigned int s_uac_flag = 0;


#if 0 // 以查询方式获取uac下行数据
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN] __attribute__((aligned(32))) = {0};
static unsigned int s_buff_w = 0;

// 数据量足够时返回0和指针参数，数据量不够时，返回-1
static int _GetUacPlaybackData(void *p) {
    static unsigned int pos_r = 0;
    static unsigned int cycle = 0;

    unsigned char *buff = (unsigned char *)uac_data_info.playback_buff_addr;
    unsigned int buff_len = uac_data_info.playback_buff_len;
    unsigned int pos_w = uac_data_playback_pos_w;

    if (cycle < uac_data_cycle) {
//        printf("cycle err %d\n", uac_data_cycle - cycle);
        cycle = uac_data_cycle - 1;
    }



    unsigned int data_size = (pos_w + buff_len - pos_r) % buff_len;
//    printf("-%d\n", data_size);
    if (data_size < 128) {
        return -1;
    }

#if 1
    if (data_size > 1024 && s_uac_flag == 0) {
        PLC_EVENT plc_event = {.event_id = 234, .ctx_index = 0};
        VspTriggerPlcEvent(plc_event);
    }

    for (int i = 0; i < data_size; i ++) {
        unsigned int index_f = (pos_r + i) >= buff_len ? (pos_r + i - buff_len) : (pos_r + i);
        unsigned int index_t = (s_buff_w + i) >= sizeof(s_deinterlace_buff) ? (s_buff_w + i - sizeof(s_deinterlace_buff)) : (s_buff_w + i);

        ((unsigned char *)s_deinterlace_buff)[index_t] = buff[index_f];
    }
    pos_r = (pos_r + data_size) % buff_len;
    s_buff_w = (s_buff_w + data_size) % sizeof(s_deinterlace_buff);
#endif

    cycle = uac_data_cycle;
    return -1;

}
#endif



int HookEventResponse(PLC_EVENT plc_event)
{

    if (plc_event.event_id == DSP_PROCESS_DONE_EVENT_ID && plc_event.ctx_index > 20) {

#if __mic
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = plc_event.ctx_index;

        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);

        VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
        int frame_length       = ctx_header->frame_length * ctx_header->sample_rate / 1000;
            int context_sample_num = frame_length * ctx_header->frame_num_per_context;
            int context_num_per_channel = ctx_header->frame_num_per_channel / ctx_header->frame_num_per_context;
            int current_context_index = ctx_addr->ctx_index % context_num_per_channel;
            unsigned int current_mic_addr = (unsigned int)(ctx_header->mic_buffer + context_sample_num * current_context_index);
            void *play_addr = (void *)(current_mic_addr - (unsigned int)ctx_header->mic_buffer);
            int   play_size = (ctx_header->out_buffer_size / ctx_header->out_num) / 8 * 8;

            AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, play_addr, play_size);

#endif

#if __out
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = plc_event.ctx_index;

        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);

        VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
        void *play_addr = (void *)((unsigned int)ctx_addr->out_buffer - (unsigned int)ctx_header->ctx_buffer);
        int   play_size = (ctx_header->out_buffer_size / ctx_header->out_num) / 8 * 8;

        AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, play_addr, play_size);
#endif

#if __uac_out
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = plc_event.ctx_index;

        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);

        VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();

        unsigned char *out_addr = (unsigned char *)DEV_TO_MCU(ctx_addr->out_buffer);
        int            out_size = (ctx_header->out_buffer_size / ctx_header->out_num) / 8 * 8;
        memcpy(s_audio_out_buffer + (s_ping_pang * out_size), out_addr, out_size);


        unsigned char *uac_addr = uac_data_buffer;
        memcpy(s_audio_out_buffer + (AUDIO_OUT_BUFFER_SIZE / 2) + (s_ping_pang * DEINTERLACE_BUFF_LEN), uac_addr, DEINTERLACE_BUFF_LEN);

        AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, s_ping_pang * out_size, (AUDIO_OUT_BUFFER_SIZE / 4)); // 双声道播放时，偏移地址和数据量依然按单声道计算

        s_ping_pang = s_ping_pang ? 0 : 1;
#endif

    }
    return 0;
}


static int uac_playback_cbk(unsigned int w_pos)
{
    static unsigned int w_pos_before;
    unsigned int buff_size;

    if (w_pos_before == w_pos)
        return -1;

    buff_size = uac_data_info.playback_buff_len;
    if (w_pos > buff_size)
        return -1;

    if (w_pos == 0)
        uac_data_cycle++;

    uac_data_playback_pos_w = w_pos;
    w_pos_before = w_pos;

    // 在中断内获取uac数据
    unsigned int pos_w = uac_data_playback_pos_w;
    unsigned char *buff = (unsigned char *)uac_data_info.playback_buff_addr;

    if (pos_w < DEINTERLACE_BUFF_LEN) {
        uac_data_buffer =  ((unsigned char *)buff) + DEINTERLACE_BUFF_LEN;
    } else {
        uac_data_buffer =  (unsigned char *)buff;
    }
    return 0;
}

static int _AudioPlayCallback(unsigned int read_addr, void *priv)
{
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, 0, AUDIO_OUT_BUFFER_SIZE);
    return 0;
}

static unsigned int s_audio_out_handle;

int HookProcessInit(void)
{
    // 用于获取uac下行数据
    static int init = 0;
    if (!init) {
        ucd_cbk.playback = uac_playback_cbk;
        uac_core_data_update_get_info(&uac_data_info);
        uac_core_data_update_callback_register(&ucd_cbk);
        init = 1;
    }
    printf("pos [%d] %d/%d\n", uac_data_cycle, uac_data_playback_pos_w, uac_data_info.playback_buff_len);

#if __mic
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    memset((void *)&buffer_config, 0, sizeof(AUDIO_OUT_BUFFER_CONFIG));
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
    buffer_config.channel_type = AUDIO_OUT_CHANNEL_MONO;
    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
    buffer_config.buffer_addr = (const short *) ctx_header->mic_buffer;
    buffer_config.buffer_size = (ctx_header->mic_buffer_size + 1023) / 1024 * 1024;
    AudioOutInit(NULL, &buffer_config);
#endif

#if __uac
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    memset((void *)&buffer_config, 0, sizeof(AUDIO_OUT_BUFFER_CONFIG));

    buffer_config.buffer_addr = (const short *)((unsigned int)uac_data_info.playback_buff_addr - 0x40000000);
    buffer_config.buffer_size = AUDIO_OUT_BUFFER_SIZE;

    buffer_config.channel_type = AUDIO_OUT_CHANNEL_MONO;
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
    buffer_config.play_callback = _AudioPlayCallback;

    AudioOutInit(NULL, &buffer_config);

    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, 0, AUDIO_OUT_BUFFER_SIZE);
#endif

#if __out
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    memset((void *)&buffer_config, 0, sizeof(AUDIO_OUT_BUFFER_CONFIG));
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
    buffer_config.channel_type = AUDIO_OUT_CHANNEL_MONO;
    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
    buffer_config.buffer_addr = (const short *) ctx_header->ctx_buffer;
    buffer_config.buffer_size = (ctx_header->ctx_size * ctx_header->ctx_num + 1023) / 1024 * 1024;
    AudioOutInit(NULL, &buffer_config);

#endif

#if __uac_out
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    memset((void *)&buffer_config, 0, sizeof(AUDIO_OUT_BUFFER_CONFIG));

    buffer_config.buffer_addr = (const short *)((unsigned int)s_audio_out_buffer - 0x40000000);
    buffer_config.buffer_size = AUDIO_OUT_BUFFER_SIZE / _CHANNEL_NUMBER; //此处应是单个声道对应的buffer长度

    buffer_config.channel_type = AUDIO_OUT_CHANNEL_STEREO;
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;

    AudioOutInit(NULL, &buffer_config);

    AudioOutSetStoreMode(AUDIO_OUT_STORE_UNINTERLACED);

    memset((void *)uac_data_info.playback_buff_addr, 0, AUDIO_OUT_BUFFER_SIZE / _CHANNEL_NUMBER);
#endif
    return 0;
}

int HookProcessTick(void)
{
    //_GetUacPlaybackData(NULL);

    return 0;
}

