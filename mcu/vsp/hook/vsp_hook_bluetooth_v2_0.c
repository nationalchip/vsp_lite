/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook_bluetooth_V2_0.c
 *
 */

#include <stdio.h>
#include <string.h>

#include <autoconf.h>
#include <vsp_message.h>
#include <vsp_context.h>
#include <vsp_ext.h>
#include "../common/vsp_vpa.h"

#include <driver/misc.h>
#include <driver/irr.h>

#include "../vsp_buffer.h"
#include "../common/vsp_plc_event.h"
#include "vsp_hook.h"

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uart.h>
#include <driver/uart_message.h>
#include <driver/imageinfo.h>
#include <driver/oem.h>
#include <driver/irq.h>
#include <driver/timer.h>
#include <driver/watchdog.h>
#include <driver/usb_gadget.h>

#include "../common/vsp_agc.h"

#define LOG_TAG "[HOOK]"

//=================================================================================================
// 0:BT connect; 1:BT connected，BLE unconnected; 2:BLE connected，BT unconnected; 3:BT & BLE connected;
// 0x11:Genie APP connected;
static int s_connect_state = -1;
// 0:stop wav stream; 1:start pcm wav stream; 2:start opus wav stream; 3:start 2-channel opus stream;
// 0x10:BT phone ring started; 0x11:BT phone ring ended;
static int s_wav_stream_state = 0;
// 0: pause; 1: play
static int s_music_state = -1;
// 0: close; 1: open
static int s_mic_state = -1;
// led state, default;
static int s_led_state = -1;
// MTC;
static int s_mtc_flag = 0;
// 0: invalid; 
static int s_plc_event_ctx_index = 0;
//=================================================================================================

#define USER_DATA_VALID_FLAG        0x5A
#define PARSE_INT_BIG_ENDIAN(p)     (((*(p))<<24) | (*(p+1)<<16) | (*(p+2)<<8) | (*(p+3)))
#define BURN3266_FIRMWARE_SIZE      (1024*1024UL)
#define BURN3266_FIRMWARE_MAC_OFFSET  0xFD000 // Offset of the BT mac in firmware
#define BURN3266_GENIE_OFFSET_TO_MAC 0xC00 // 三元组存储位置为0xFDC00

static unsigned int s_prepare_burn3266_flag = 0;
static unsigned int s_burn3266_unfinish_flag = 0;
static unsigned int s_burn3266_firmware_address = 0; // BK3266 firmware address in spiflash
static unsigned char s_burn3266_mac[6] = {0};
static unsigned char s_burn3266_tlv[256] = {0};

static int _UartTimeoutRecv(int port, unsigned char *buf, unsigned int len, unsigned int timeout_ms)
{
    unsigned int delay_cnt = 0, index = 0;

    do {
        if ((UartTryRecvByte(port, buf+index) == 0)) {
            index++;
            if (index == len) {
                return 1;
            }
        }
        else {
            mdelay(1);
            delay_cnt++;
        }
    }while (delay_cnt < timeout_ms);

    return 0;
}

static int _Reboot3266(void)
{
    #if 0
    const unsigned char tx_cmd[]={0x01,0xe0,0xfc,0x02,0x0e,0xa5};
    UartSyncSendBuffer(UART_PORT0, (unsigned char *)tx_cmd, sizeof(tx_cmd));
    #else
    // Reset BT
    GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_LOW);
    mdelay(100);
    GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_HIGH);
    #endif

    return 1;
}

static void _EndBurn3266(void)
{
    USER_DATA  user_data = {0};

    // Write burn3266 state to flash
    UserDataRead(&user_data);
    // data valid flag
    user_data.data[0] = USER_DATA_VALID_FLAG;
    // burn3266 state
    user_data.data[1] = 0x00;
    UserDataWrite(&user_data);
    s_prepare_burn3266_flag = 0;

    UartSetBaudrate(UART_PORT0, CONFIG_SERIAL_BAUD_RATE);
}

static int _Burn3266LedToggle(void *private_data)
{
    static int toggle_cnt = 0;

    if (toggle_cnt%2) {
        GpioSetLevel(CONFIG_BOARD_GPIO_LED01, GPIO_LEVEL_HIGH);
    }
    else {
        GpioSetLevel(CONFIG_BOARD_GPIO_LED01, GPIO_LEVEL_LOW);
    }
    toggle_cnt++;

    return 0;
}

static unsigned int _Burn3266Crc32Table(unsigned int index)
{
    unsigned int c = index;
    unsigned int poly = 0xEDB88320UL;
    unsigned int k = 0;

    for (k = 0; k < 8; k++) {
        c = (c & 1)? (poly ^ (c >> 1)) : (c >> 1);
    }

    return c;
}

static unsigned int _Burn3266Crc32(unsigned int crc, const unsigned char* buf, unsigned int len)
{
    unsigned int run_times = 0;

    while (len >= 8) {
        run_times = 8;
        while (run_times--) {
            crc = _Burn3266Crc32Table((crc ^ (*buf++)) & 0xff ) ^ (crc >> 8);
        }
        len -= 8;
    }

    if (len != 0) {
        do {
            crc = _Burn3266Crc32Table((crc ^ (*buf++)) & 0xff ) ^ (crc >> 8);
        }while (--len);
    }

    return crc;
}

static int _Burn3266VerifyFirmware(void)
{
    const unsigned char tx_cmd[] = {0x01,0xe0,0xfc,0x09,0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    const unsigned char rx_cmd[] = {0x04,0x0e,0x08,0x01,0xe0,0xfc,0x10,0x00,0x00,0x00,0x00};
    unsigned char tx_buf[4096] = {0};
    unsigned char rx_buf[11] = {0};
    unsigned int index = 0, j = 0, firmware_crc32 = 0, recv_crc32 = 0;
    unsigned int start_addr = 0, end_addr = BURN3266_FIRMWARE_SIZE-1;

    memcpy(tx_buf, tx_cmd, sizeof(tx_cmd));
    memcpy(tx_buf+5, &start_addr, sizeof(start_addr));
    memcpy(tx_buf+9, &end_addr, sizeof(end_addr));
    UartSyncSendBuffer(UART_PORT0, tx_buf, sizeof(tx_cmd));
    if (!_UartTimeoutRecv(UART_PORT0, rx_buf, sizeof(rx_cmd), 6*1000)){
        return 0;
    }
    if (memcmp(rx_cmd, rx_buf, 7) != 0) {
        return 0;
    }

    memcpy((unsigned char*)&recv_crc32, rx_buf + 7, 4);
    firmware_crc32 = 0xFFFFFFFF;
    for (index = 0; index < BURN3266_FIRMWARE_SIZE; index += sizeof(tx_buf)) {
        SpiFlashRead(s_burn3266_firmware_address + index, tx_buf, sizeof(tx_buf));
        if (index == BURN3266_FIRMWARE_MAC_OFFSET) {
            // 修改MAC
            for (j = 0; j < sizeof(s_burn3266_mac); j++) {
                tx_buf[j] = s_burn3266_mac[sizeof(s_burn3266_mac)-1-j];
            }
            // 修改三元组
            if ((s_burn3266_tlv[0] == 0x01) && (s_burn3266_tlv[1] != 0)) {
                memcpy(tx_buf+BURN3266_GENIE_OFFSET_TO_MAC, s_burn3266_tlv+2, s_burn3266_tlv[1]);
            }
        }
        firmware_crc32 = _Burn3266Crc32(firmware_crc32, (const unsigned char *)tx_buf, sizeof(tx_buf));
        printf("-");
    }
    if (recv_crc32 != firmware_crc32) {
        //printf("recv_crc32: 0x%08X, firmware_crc32: 0x%08X\n", recv_crc32, firmware_crc32);
        return 0;
    }

    return 1;
}

static int _Burn3266WriteFirmware(void)
{
    #define  FLASH_SECTOR_SIZE     4096
    const unsigned char tx_cmd[] = {0x01,0xe0,0xfc,0xff,0xf4,0x05,0x10,0x07};
    const unsigned char rx_cmd[] = {0x04,0x0e,0xff,0x01,0xe0,0xfc,0xf4,0x06,0x00,0x07,0x00,0x00,0x00,0x00,0x00};
    unsigned char tx_buf[FLASH_SECTOR_SIZE+32] = {0};
    unsigned char rx_buf[64] = {0};
    int i = 0, j = 0, write_addr = 0;

    for (i = 0; i < (BURN3266_FIRMWARE_SIZE/FLASH_SECTOR_SIZE); i++) {
        memcpy(tx_buf, tx_cmd, sizeof(tx_cmd));
        memcpy(tx_buf+8, &write_addr, sizeof(write_addr));
        SpiFlashRead(s_burn3266_firmware_address + i*FLASH_SECTOR_SIZE, tx_buf+12, FLASH_SECTOR_SIZE);
        if ((i*FLASH_SECTOR_SIZE) == BURN3266_FIRMWARE_MAC_OFFSET) {
            // 恢复MAC
            for (j = 0; j < sizeof(s_burn3266_mac); j++) {
                tx_buf[j+12] = s_burn3266_mac[sizeof(s_burn3266_mac)-1-j];
            }
            // 恢复三元组
            if ((s_burn3266_tlv[0] == 0x01) && (s_burn3266_tlv[1] != 0)) {
                memcpy(tx_buf+12+BURN3266_GENIE_OFFSET_TO_MAC, s_burn3266_tlv+2, s_burn3266_tlv[1]);
            }
        }
        UartSyncSendBuffer(UART_PORT0, tx_buf, 12+FLASH_SECTOR_SIZE);

        if (!_UartTimeoutRecv(UART_PORT0, rx_buf, sizeof(rx_cmd), 3*1000)){
            return 0;
        }
        if ((memcmp(rx_cmd, rx_buf, sizeof(rx_cmd)-sizeof(write_addr)) != 0)
         || (memcmp((unsigned char *)&write_addr, rx_buf+sizeof(rx_cmd)-sizeof(write_addr), sizeof(write_addr)) != 0)) {
            return 0;
        }
        printf("=");
        write_addr += FLASH_SECTOR_SIZE;
    }

    return 1;
}

static int _Burn3266EraseallFlash(void)
{
    const unsigned char tx_cmd[] = {0x01, 0xe0, 0xfc, 0xff, 0xf4, 0x02, 0x00, 0x0a, 0x10};
    const unsigned char rx_cmd[] = {0x04,0x0e,0xff,0x01,0xe0,0xfc,0xf4,0x03,0x00,0x0a,0x00,0x05};
    unsigned char rx_buf[64]={0};

    UartSyncSendBuffer(UART_PORT0, (unsigned char *)tx_cmd, sizeof(tx_cmd));
    if (_UartTimeoutRecv(UART_PORT0, rx_buf, sizeof(rx_cmd), 20*1000)) {
        if (memcmp(rx_cmd, rx_buf, 7) == 0) {
            if (rx_buf[10] == 0) {
                return 1;
            }
        }
    }

    return 0;
}

static int _Burn3266UnlockFlash(void)
{
    const unsigned char tx_cmd[] = {0x01, 0xe0, 0xfc, 0xff, 0xf4, 0x03, 0x00, 0x0d, 0x01, 0x00};
    const unsigned char rx_cmd[] = {0x04, 0x0e, 0xff, 0x01, 0xe0, 0xfc, 0xf4, 0x04, 0x00, 0x0d, 0x00, 0x01, 0x00};
    unsigned char rx_buf[64] = {0};

    UartSyncSendBuffer(UART_PORT0, (unsigned char *)tx_cmd, sizeof(tx_cmd));
    if (_UartTimeoutRecv(UART_PORT0, rx_buf, sizeof(rx_cmd), 2*1000)) {
        if (memcmp(rx_cmd, rx_buf, sizeof(rx_cmd)) == 0) {
            return 1;
        }
    }

    return 0;
}

static int _Burn3266SetBaudrate(unsigned int baudrate)
{
    const unsigned char tx_cmd[] = {0x01,0xe0,0xfc,0x06,0x0f,0x80,0x84,0x1e,0x00,0x16};
    const unsigned char rx_cmd[] = {0x04,0x0e,0x09,0x01,0xe0,0xfc,0x0f,0x80,0x84,0x1e,0x00,0x01};
    unsigned char tx_buf[10] = {0};
    unsigned char rx_buf[12] = {0};

    memcpy(tx_buf, tx_cmd, sizeof(tx_cmd));
    memcpy(tx_buf+5, &baudrate, 4);
    UartSyncSendBuffer(UART_PORT0, (unsigned char *)tx_buf, sizeof(tx_buf));
    mdelay(10);
    UartFlushRecvFIFO(UART_PORT0);
    UartSyncSendBuffer(UART_PORT0, (unsigned char *)tx_buf, sizeof(tx_buf));

    UartSetBaudrate(UART_PORT0, baudrate);
    if (_UartTimeoutRecv(UART_PORT0, rx_buf, sizeof(rx_cmd), 1000)) {
        if ((memcmp(rx_cmd, rx_buf, 7) == 0)
         && (memcmp(tx_buf+5, rx_buf+7, 4) == 0)){
            return 1;
        }
    }

    return 0;
}

static int _Burn3266StayInRom(void)
{
    const unsigned char tx_cmd[] = {0x01, 0xe0, 0xfc, 0x02, 0xaa, 0x55};
    const unsigned char rx_cmd[] = {0x04,0x0e,0x05,0x01,0xe0,0xfc,0xaa,0x55};
    unsigned char rx_buf[64] = {0};
    int try_cnt = 0;

    while (try_cnt < 2000) {
        UartSyncSendBuffer(UART_PORT0, (unsigned char *)tx_cmd, sizeof(tx_cmd));
        if (_UartTimeoutRecv(UART_PORT0, rx_buf, sizeof(rx_cmd), 5)) {
            if (memcmp(rx_buf, rx_cmd, sizeof(rx_cmd)) == 0) {
                return 1;
            }
        }
        try_cnt++;
    }

    return 0;
}

static void _Burn3266(void)
{
    printf(LOG_TAG"_Burn3266 started\n");
    TimerRegisterCallback(_Burn3266LedToggle, NULL, 100);

    gx_disable_all_interrupt();
    UartSetBaudrate(UART_PORT0, 115200);

    // Reset BT
    GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_LOW);
    mdelay(100);
    GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_HIGH);

    // Stay in rom process
    printf(LOG_TAG"_Burn3266StayInRom ");
    if (!_Burn3266StayInRom()){
        goto failure_exit;
    }
    printf("done.\n");
    mdelay(50);
    UartFlushRecvFIFO(UART_PORT0);

    // Set baudrate
    printf(LOG_TAG"_Burn3266SetBaudrate ");
    if (!_Burn3266SetBaudrate(2*1000*1000)){
        printf("failed!\n");
        UartSetBaudrate(UART_PORT0, 115200);
        //goto failure_exit;
    }
    printf("done.\n");
    mdelay(50);
    UartFlushRecvFIFO(UART_PORT0);

    // Unlock flash
    printf(LOG_TAG"_Burn3266UnlockFlash ");
    if (!_Burn3266UnlockFlash()) {
        goto failure_exit;
    }
    printf("done.\n");
    mdelay(500);
    UartFlushRecvFIFO(UART_PORT0);

    // Eraseall flash
    printf(LOG_TAG"_Burn3266EraseallFlash ");
    if (!_Burn3266EraseallFlash()) {
        goto failure_exit;
    }
    printf("done.\n");
    mdelay(500);
    UartFlushRecvFIFO(UART_PORT0);

    // Write firmware
    printf(LOG_TAG"_Burn3266WriteFirmware ");
    if (!_Burn3266WriteFirmware()) {
        goto failure_exit;
    }
    printf("done.\n");

    // Verify firmware
    printf(LOG_TAG"_Burn3266VerifyFirmware ");
    if (!_Burn3266VerifyFirmware()) {
        goto failure_exit;
    }
    printf("done.\n");

    _Reboot3266();
    _EndBurn3266();
    TimerUnregisterCallback(_Burn3266LedToggle);
    // Light off LED
    GpioSetLevel(CONFIG_BOARD_GPIO_LED01, GPIO_LEVEL_HIGH);
    printf(LOG_TAG"_Burn3266 completed!\n");
    // Reboot
    //gx_disable_all_interrupt();
    WatchdogInit(1, 1, NULL);
    while(1){}

failure_exit:
    printf("failed!\n");
    TimerUnregisterCallback(_Burn3266LedToggle);
    // Lighting LED
    GpioSetLevel(CONFIG_BOARD_GPIO_LED01, GPIO_LEVEL_LOW);
    while (1) {}
}

// Check if there is other tlv data to save
static void _PrepareSaveTlv(unsigned char *p_tlv, unsigned char data_len)
{
    memset(s_burn3266_tlv, 0, sizeof(s_burn3266_tlv));
    if ((data_len == 0) || (p_tlv[1] == 0) || (p_tlv[1] > (data_len-2))) return;

    switch(p_tlv[0])
    {
        case 0x01:
            // Type: 0x01三元组数据，格式: P:1234;M:1234567890abcdeffedcba1234567890;S:11:22:33:44:55:66;
            if ((strstr((const char *)&p_tlv[2], "P:") != NULL) \
              && (strstr((const char *)&p_tlv[2], "M:") != NULL) \
              && (strstr((const char *)&p_tlv[2], "S:") != NULL))
            {
                memcpy(s_burn3266_tlv, p_tlv, p_tlv[1]+2);
                printf(LOG_TAG"save tlv: %s\n", &s_burn3266_tlv[2]);
            }
        break;
        default:
        break;
    }
}

// Halt DSP to disable flash access
static void _HaltDsp(void)
{
    AudioInDone();
    VspSuspendVpa();
}

// Copy from vsp_mode_plc.c
#ifndef CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF
#define CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF 1
#endif
static int _PlcAudioInRecordCallback(int frame_index, void *priv)
{
    if (frame_index > CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF) {
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = frame_index - (CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF + 1);
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);
#else
        VspGetDDRContext(context_index, &ctx_addr, &ctx_size);
#endif
        ctx_addr->mic_mask      = AudioInGetMicEnable();
        ctx_addr->ref_mask      = 0xFFFF;
        ctx_addr->frame_index   = context_index * CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
        ctx_addr->ctx_index     = context_index;
        ctx_addr->vad           = 0;
        ctx_addr->kws           = 0;
        ctx_addr->mic_gain      = AudioInGetMicGain();
        ctx_addr->ref_gain      = AudioInGetRefGain();
        ctx_addr->direction     = -1;

        // Send a context to DSP
        VSP_MSG_MCU_DSP message;
        message.header.type = VMT_MD_PROCESS;

        PLC_STATUS plc_status = {.vpa_mode = PLC_STATUS_VPA_ACTIVE};
        VspGetPlcStatus(PST_SWITCH_VPA_MODE, &plc_status);
        if (plc_status.vpa_mode == PLC_STATUS_VPA_TALKING) {
            message.header.param = VSP_PROCESS_TALKING;
        } else if (plc_status.vpa_mode == PLC_STATUS_VPA_ACTIVE) {
            message.header.param = VSP_PROCESS_ACTIVE;
        }

        message.header.magic = context_index;
        message.header.size = sizeof(VMB_MD_PROCESS) / sizeof(unsigned int);
        message.process.context_addr = (void *)MCU_TO_DEV(ctx_addr);
        message.process.context_size = ctx_size;
        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
    }

    return 0;
}

// Resume DSP
static void _ResumeDsp(void)
{
    VspResumeVpa();
    // Retart Audio In
    // Copy from vsp_mode_plc.c
    AUDIO_IN_CONFIG audio_in_config;
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    VspGetSRAMAudioInConfig(&audio_in_config);
#else
    VspGetDDRAudioInConfig(&audio_in_config);
#endif
    audio_in_config.record_callback = _PlcAudioInRecordCallback;
    if (AudioInInit(&audio_in_config, 0)) {
        printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
    }
}

// Check if can burn 3266
static void _PrepareBurn3266(int cmd_id)
{
    unsigned char *p_para = UartGetCommandData();
    unsigned char para_len = UartGetCommandDataLength();
    unsigned int i, rx_crc32, calc_crc32;
    unsigned char buf[4096] = {0};

    s_prepare_burn3266_flag = 0;
    if (para_len < (sizeof(s_burn3266_firmware_address)+sizeof(rx_crc32)+sizeof(s_burn3266_mac))) return;
    // 3266 firmware address check
    s_burn3266_firmware_address = PARSE_INT_BIG_ENDIAN(p_para);
    if ((s_burn3266_firmware_address+BURN3266_FIRMWARE_SIZE) <= FLASH_TOTAL_SIZE) {
        _HaltDsp();
        // Firmware crc32 check
        rx_crc32 = PARSE_INT_BIG_ENDIAN(p_para + 4);
        calc_crc32 = 0;
        for (i = 0; i < BURN3266_FIRMWARE_SIZE; i += sizeof(buf)) {
            SpiFlashRead(s_burn3266_firmware_address+i, buf, sizeof(buf));
            calc_crc32 = crc32(calc_crc32, (const unsigned char *)buf, sizeof(buf));
        }
        if ((calc_crc32 != 0xa738ea1c)  /* the firmware is all 0x00 */
        && (calc_crc32 != 0x956bac74)  /* the firmware is all 0xFF */
        && (calc_crc32 == rx_crc32)) {
            // MAC check
            memcpy(s_burn3266_mac, p_para+8, sizeof(s_burn3266_mac));
            if ((s_burn3266_mac[0] == 0xFF)
            && (s_burn3266_mac[1] == 0xFF)
            && (s_burn3266_mac[2] == 0xFF)
            && (s_burn3266_mac[3] == 0xFF)
            && (s_burn3266_mac[4] == 0xFF)
            && (s_burn3266_mac[5] == 0xFF)){
                s_prepare_burn3266_flag = 0;
            }
            else {
                _PrepareSaveTlv(p_para+8+sizeof(s_burn3266_mac), \
                    para_len - (sizeof(s_burn3266_firmware_address)+sizeof(rx_crc32)+sizeof(s_burn3266_mac)));
                s_prepare_burn3266_flag = 1;
            }
        }
        _ResumeDsp();
    }

    if(cmd_id == UCI_NOTIFY_TX_PREPARE_BURN_BT) {
        UartSendPrepareBurnBtResult(s_prepare_burn3266_flag);
    }
}

static void _TriggerBurn3266(void)
{
    USER_DATA  user_data = {0};

    if (!s_prepare_burn3266_flag) return ;

    _HaltDsp();

    // Write burn3266 state to flash
    UserDataRead(&user_data);
    user_data.data[0] = USER_DATA_VALID_FLAG; // data valid flag
    user_data.data[1] = 0x01; // burn3266 state
    user_data.data[2] = (s_burn3266_firmware_address >> 24) & 0xFF; // 3266 firmware address
    user_data.data[3] = (s_burn3266_firmware_address >> 16) & 0xFF;
    user_data.data[4] = (s_burn3266_firmware_address >> 8) & 0xFF;
    user_data.data[5] = s_burn3266_firmware_address & 0xFF;
    memcpy(user_data.data+6, s_burn3266_mac, sizeof(s_burn3266_mac)); // MAC
    if ((s_burn3266_tlv[0] == 0x01) && (s_burn3266_tlv[1] != 0)) {
        memcpy(user_data.data+6+sizeof(s_burn3266_mac), s_burn3266_tlv, s_burn3266_tlv[1]+2); // 三元组
    }
    UserDataWrite(&user_data);

    // Start to burn3266
    _Burn3266();
}

void _HookCheckBurn3266(void)
{
    USER_DATA  user_data = {0};

    // Read burn3266 state from flash
    UserDataRead(&user_data);
    if ((user_data.data[0] == USER_DATA_VALID_FLAG)
    && (user_data.data[1] == 0x01)) { // burn3266 state
        s_burn3266_firmware_address = PARSE_INT_BIG_ENDIAN(user_data.data+2);
        memcpy(s_burn3266_mac, user_data.data+6, sizeof(s_burn3266_mac)); // MAC
        if ((user_data.data[6+sizeof(s_burn3266_mac)] == 0x01)
        && (user_data.data[6+sizeof(s_burn3266_mac)+1] != 0)) {
            memcpy(s_burn3266_tlv, &user_data.data[6+sizeof(s_burn3266_mac)], user_data.data[6+sizeof(s_burn3266_mac)+1]+2); // 三元组
        }
        s_burn3266_unfinish_flag = 1;
    }
}

#define BT_CONNECT_TIMEOUT_MAX (4000/60) // 4 seconds
static int s_bt_connect_timeout = 0;
static char _UartGetState(void)
{
    switch (UartGetCommandID()) {
        case UCI_NOTIFY_RX_CONNECT_STATE:
        {
            s_connect_state = UartGetCommandState();
            if (s_connect_state == 0x11) { // Genie APP connected
                s_bt_connect_timeout = BT_CONNECT_TIMEOUT_MAX;
            }
            break;
        }
        case UCI_NOTIFY_RX_WAV_STREAM_STATE:
        {
            int wav_state = UartGetCommandState();
            PLC_STATUS plc_status = {.vpa_mode = PLC_STATUS_VPA_ACTIVE};
            if (wav_state == 0x11) {// Phone ring start
                s_plc_event_ctx_index = 0;
                plc_status.vpa_mode = PLC_STATUS_VPA_TALKING;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Disable AGC
                AUDIO_IN_AGC_CONFIG config = {
                    .mode         = AUDIO_IN_AGC_MODE_DISABLE,
                };
                VspAudioInInitAgcConfig(&config);
                AudioInSetMicGain(30); // 恢复板级中设置的MIC和REF增益
                AudioInSetRefGain(2);
#endif
            }
            else if (wav_state == 0x01) { // Start pcm wave (BT sco connected)
                s_plc_event_ctx_index = 0;
                if (s_wav_stream_state == 0x11) {
                    break; // Do not switch plc status
                }
                else {
                    plc_status.vpa_mode = PLC_STATUS_VPA_TALKING;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Disable AGC
                AUDIO_IN_AGC_CONFIG config = {
                    .mode         = AUDIO_IN_AGC_MODE_DISABLE,
                };
                VspAudioInInitAgcConfig(&config);
                AudioInSetMicGain(30); // 恢复板级中设置的MIC和REF增益
                AudioInSetRefGain(2);
#endif
                }
            }
            else if (wav_state == 0x00) { // Stop sending speech
                s_plc_event_ctx_index = 0;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Enable AGC
                VspInitGainControl(NULL);
                VspAudioInInitAgcConfig(NULL);
                VspAudionInSetAgcSmoothParam(50);
#endif
            }
            else if (wav_state == 0x03) { // Start 2mic opus
                s_plc_event_ctx_index = 0;
                plc_status.vpa_mode = PLC_STATUS_VPA_TEST;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
                // Disable AGC
                AUDIO_IN_AGC_CONFIG config = {
                    .mode         = AUDIO_IN_AGC_MODE_DISABLE,
                };
                VspAudioInInitAgcConfig(&config);
                AudioInSetMicGain(30); // 恢复板级中设置的MIC和REF增益
                AudioInSetRefGain(2);
#endif
            }

            VspSetPlcStatus(PST_SWITCH_VPA_MODE, &plc_status);
            s_wav_stream_state = wav_state;
            break;
        }
        case UCI_NOTIFY_RX_MUSIC_STATE:
        {
            s_music_state = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_MIC_STATE:
        {
            s_mic_state = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_LED_STATE:
        {
           s_led_state = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_MTC_STATE:
        {
            s_mtc_flag = UartGetCommandState();
            break;
        }
        case UCI_NOTIFY_RX_QUERY_IMAGEINFO:
        {
            if (UartGetCommandState() == UCA_QUERY_IMAGEINFO){
                UartSendImageinfo();
            }
            break;
        }
        case UCI_NOTIFY_RX_PREPARE_BURN_BT:
        {
            _PrepareBurn3266(UCI_NOTIFY_RX_PREPARE_BURN_BT);
            break;
        }
        case UCI_NOTIFY_RX_TRIGGER_BURN_BT:
        {
            _TriggerBurn3266();
            break;
        }
        default:
            break;
    }

    return 0;
}

static int _HookProcessUartSendWav(char *wav_addr, unsigned int wav_length)
{
    return UartSendWav(wav_addr, wav_length);
}

static int _HookProcessUartSendWavEnd(void)
{
    return UartSendWavEnd();
}

static int _HookProcessUartSendOpusData(char *data_addr, unsigned int data_length)
{
    if (s_wav_stream_state == 0x03) {
        return UartSendCommandWithData(UCI_NOTIFY_TX_2MIC_OPUS_DATA, (unsigned char *)data_addr, data_length);
    }
    else {
        return UartSendCompressedAudio(data_addr, data_length);
    }
}

//=================================================================================================
// Hook Event Process
#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V2_0_UART_SEND_OUTPUT_CHANNLE_DATA)
#define DEINTERLACE_BUFF_LEN  1024
static short s_deinterlace_buff[DEINTERLACE_BUFF_LEN];
# endif
static int _PlcUartSendOutputData(int value)
{
#ifdef CONFIG_VSP_CF_BT_V2_0_ENABLE_UART_SEND_OUTPUT_DATA
    VSP_CONTEXT *ctx_buffer;
    unsigned int ctx_size;
    
    VspGetSRAMContext(s_plc_event_ctx_index, &ctx_buffer, &ctx_size);
    VSP_CONTEXT_HEADER *context_header = (VSP_CONTEXT_HEADER *)DEV_TO_MCU(ctx_buffer->ctx_header);
    switch (value) {
        case UPR_SEND_WAV:
            {
                if (s_plc_event_ctx_index == 0) break;
                int frame_length       = context_header->frame_length * context_header->sample_rate / 1000;
                int context_sample_num = frame_length * context_header->frame_num_per_context;
#if defined CONFIG_VSP_CF_BT_V2_0_UART_SEND_OUTPUT_CHANNLE_DATA
                short *send_addr = (short *)DEV_TO_MCU(ctx_buffer->out_buffer);
#elif defined CONFIG_VSP_CF_BT_V2_0_UART_SEND_MIC_CHANNLE_DATA
                short *send_addr = (((short *)DEV_TO_MCU(context_header->mic_buffer)) + context_sample_num * current_context_index);
#endif

#if defined (CONFIG_VSP_OUT_INTERLACED) && (defined CONFIG_VSP_CF_BT_V2_0_UART_SEND_OUTPUT_CHANNLE_DATA)
                if (context_sample_num > DEINTERLACE_BUFF_LEN) {
                    printf(LOG_TAG"s_deinterlace_buff is smaller\n");
                    return -1;
                }

                //Deinterlace
                int out_num = context_header->out_num;
                for(int i = 0; i < context_sample_num; i++) {
                    s_deinterlace_buff[i] = send_addr[out_num*i];
                }
                _HookProcessUartSendWav((char *)s_deinterlace_buff, context_sample_num * sizeof(short));
#else
                _HookProcessUartSendWav((char *)send_addr, context_sample_num * sizeof(short));
#endif
                break;
            }
        case UPR_SEND_OPUS_DATA:
            {
                if (s_plc_event_ctx_index == 0) break;
                VSP_EXT_BUFFER *ext_buffer = DEV_TO_MCU((ctx_buffer->ext_buffer));
                VSP_CODEC_DATA_HEADER *codec_header = &(ext_buffer->buffer.codec_data.codec_header);
                unsigned int output_addr = (unsigned int) DEV_TO_MCU((ext_buffer->buffer.codec_data.data));
                unsigned int frame_num = codec_header->frame_num;
                unsigned int output_length = 0;
                for (int i = 0; i < frame_num; i++) {
                    output_length += ext_buffer->buffer.codec_data.codec_size[i];
                }
                if (output_length) {
                    if (((s_wav_stream_state == 0x03) && (output_length != (frame_num*40*2)))
                        || ((s_wav_stream_state == 0x02) && (output_length != (frame_num*40)))) {
                        // Filter invalid packet length
                        break;
                    }
                    printf(LOG_TAG"frame_num: %d, output_length: %d\n", frame_num, output_length);
                    _HookProcessUartSendOpusData((char *)output_addr, output_length);
                }
                break;
            }
        case UPR_SEND_WAV_END:
            s_plc_event_ctx_index = 0;
            // _HookProcessUartSendWavEnd();
            break;
        default:
            break;
    }

#endif

    return 0;
}

static int _HookProcessUart(unsigned int event_id)
{
    if(event_id > 99) printf(LOG_TAG"event_id = %d\n",event_id);
    int result = -1;
    static unsigned int send_wav_flag = 0;

    if (s_mic_state == 0) {
        send_wav_flag = 0;
        return -1;
    }

    switch(event_id) {
        case 105:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_VOLUME_UP);
            break;
        case 106:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_VOLUME_DOWN);
            break;
        case 103:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_PAUSE);
            break;
        case 104:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_PLAY);
            break;
        case 113:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_NEXT);
            break;
        case 112:
            UartSendCommand(UCI_NOTIFY_TX_MUSIC_STATE, UCA_MUSIC_PREV);
            break;
        case 114:
            UartSendCommand(UCI_NOTIFY_TX_CALLING_STATE, UCA_PHONE_ANSWER);
            break;
        case 115:
            UartSendCommand(UCI_NOTIFY_TX_CALLING_STATE, UCA_PHONE_REJECT);
            break;
        case 101:
        case 100:
        {
            if (s_bt_connect_timeout == 0) { // 天猫精灵APP连接4秒后才做唤醒处理
                UartSendCommand(UCI_NOTIFY_TX_WAKEUP_STATE, UCA_WAKE_UP);
            }
        }
            //s_wav_stream_state = 1;
        case DSP_PROCESS_DONE_EVENT_ID: // event : DSP callback
            {
                if (s_bt_connect_timeout > 0) {
                    s_bt_connect_timeout--;
                }
                if (s_wav_stream_state == 1) {
                    send_wav_flag = 1;
                    result = UPR_SEND_WAV;
                } else if ((s_wav_stream_state == 2) || (s_wav_stream_state == 3)) {
                    send_wav_flag = 1;
                    result =  UPR_SEND_OPUS_DATA;
                } else if (send_wav_flag) {
                    send_wav_flag = 0;
                    result =  UPR_SEND_WAV_END;
                }
                break;
            }
        default:
            break;
    }

    _PlcUartSendOutputData(result);
    return result;
}

static void _RebootToFactoryFirmware(void)
{
    extern int _start_boot_switch_;
    *(volatile unsigned int *)&_start_boot_switch_ = 0x42555831; // 写"BUX1"到寄存器

    // Watchdog reset
    WatchdogInit(1, 1, NULL);
    while(1) {}
}

//=================================================================================================
int HookEventResponse(PLC_EVENT plc_event)
{
    _HookProcessUart(plc_event.event_id);
    s_plc_event_ctx_index = plc_event.ctx_index;
    return 0;
}

int HookProcessInit(void)
{
    _HookCheckBurn3266();
    return 0;
}

int HookProcessTick(void)
{
    if (UartGetPacket() == 0) {
        _UartGetState();
    }

    if (s_burn3266_unfinish_flag) {
        _Burn3266();
        s_burn3266_unfinish_flag = 0;
    }

    return 0;
}


