/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_hook.h
 *
 */

#ifndef __VSP_HOOK_H__
#define __VSP_HOOK_H__

#include "../common/vsp_plc_event.h"

//=================================================================================================
// Hook Event Process
int HookProcessInit(void);
int HookEventResponse(PLC_EVENT plc_event);
int HookProcessTick(void);

#endif // __VSP_HOOK_H__
