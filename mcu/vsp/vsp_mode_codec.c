/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_mode_codec.c: VSP CODEC mode, as its name, the mode play an act as an Audio Codec Chip,
 *  in the mode:
 *      # We receive audio data from HOST through ADC then play out through DAC,
 *          and send processed audio data to HOST via IIS,
 *          and send VAD/KWS signal to HOST via UART and DAC
 *      # As an alternative, we also support receiving audio from ADC, but needn't playback.
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <base_addr.h>

#include <driver/audio_mic.h>
#include <driver/audio_ref.h>
#include <driver/audio_out.h>
#include <driver/cpu.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/uart.h>
#include <driver/misc.h>

#include <vsp_firmware.h>

#include "common/vsp_plc_event.h"
#include "common/vsp_vpa.h"
#include "vsp_mode.h"
#include "vsp_buffer.h"

#ifdef CONFIG_MCU_ENABLE_CPU
#define VPA_FLASH_OFFSET CONFIG_VSP_CODEC_LOAD_DSP_FROM
#else
#define VPA_FLASH_OFFSET DSP_FW_OFFSET
#endif

#define LOG_TAG "[CODEC]"

static struct {
    unsigned char   kws_value;
    unsigned char   vad_value;
} s_codec_state = {0};

//=================================================================================================

static int _CodecAudioInRecordCallback(int frame_index, void *priv)
{
    // Delay a frame to make sure REF is fullfilled or algorithm may visit data across context
    if (frame_index > 0) {
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = frame_index - 1;
        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);

        ctx_addr->mic_mask      = AudioInGetMicEnable();
        ctx_addr->ref_mask      = 0xFFFF;
        ctx_addr->frame_index   = context_index * CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
        ctx_addr->ctx_index     = context_index;
        ctx_addr->vad           = 0;
        ctx_addr->kws           = 0;
        ctx_addr->mic_gain      = AudioInGetMicGain();
        ctx_addr->ref_gain      = AudioInGetRefGain();
        ctx_addr->direction     = -1;

        // Send a context to DSP
        VSP_MSG_MCU_DSP message;
        message.header.type = VMT_MD_PROCESS;
        message.header.param = VSP_PROCESS_ACTIVE;
        message.header.magic = context_index;
        message.header.size = sizeof(VMB_MD_PROCESS) / sizeof(unsigned int);
        message.process.context_addr = (void *)MCU_TO_DEV(ctx_addr);
        message.process.context_size = ctx_size;

        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
    }

    return 0;
}

static int _CodecAudioInAvadCallback(uint8_t state, void *priv)
{
    return 0;
}

//-------------------------------------------------------------------------------------------------

static int _CodecDspCallback(volatile VSP_MSG_DSP_MCU *request, void *priv)
{
    if (request->header.type == VMT_DM_PROCESS_DONE) {
        if (request->header.param == VSP_PROCESS_ACTIVE) {

            VSP_CONTEXT *context = (VSP_CONTEXT *)DEV_TO_MCU(request->process_done.context_addr);

            VSP_CONTEXT *ctx_buffer;
            unsigned int ctx_size;
            VspGetSRAMContext(context->ctx_index, &ctx_buffer, &ctx_size);

            if (context->kws){
                s_codec_state.kws_value = context->kws;
                PLC_EVENT plc_event = {.event_id = s_codec_state.kws_value};
                VspTriggerPlcEvent(plc_event);
            }

#ifdef CONFIG_VSP_CODEC_OUT_ROUTE_I2S
            VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#if defined CONFIG_VSP_CODEC_SOURCE_FROM_MIC0
            int frame_length       = ctx_header->frame_length * ctx_header->sample_rate / 1000;
            int context_sample_num = frame_length * ctx_header->frame_num_per_context;
            int context_num_per_channel = ctx_header->frame_num_per_channel / ctx_header->frame_num_per_context;
            int current_context_index = context->ctx_index % context_num_per_channel;
            unsigned int current_mic_addr = (unsigned int)(ctx_header->mic_buffer + context_sample_num * current_context_index);
            void *play_addr = (void *)(current_mic_addr - (unsigned int)ctx_header->mic_buffer);
#elif defined CONFIG_VSP_CODEC_SOURCE_FROM_OUTPUT0
            void *play_addr = (void *)((unsigned int)ctx_buffer->out_buffer - (unsigned int)ctx_header->ctx_buffer);
#endif
            int   play_size = (ctx_header->out_buffer_size / ctx_header->out_num) / 8 * 8;

            AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, play_addr, play_size);
#endif
         }
    }
    return 0;
}

//=================================================================================================

static int _CodecModeInit(VSP_MODE_TYPE prev_mode)
{
    printf(LOG_TAG"Init CODEC_A mode\n");
    unsigned int start = get_time_ms();

    // Initialize Audio In Buffer
    VspInitSRAMBuffer();
    AUDIO_IN_CONFIG audio_in_config;
    VspGetSRAMAudioInConfig(&audio_in_config);

    // Load VPA
    if (VspLoadVpa(VPA_FLASH_OFFSET, DEV_TO_MCU(audio_in_config.buffer_addr), audio_in_config.buffer_size)) {
        printf(LOG_TAG"LoadDSP Failed !\n");
        return -1;
    }
    mdelay(100);
    if (VspInitializeVpa(VspGetSRAMContextHeader(), _CodecDspCallback, NULL)) {
        printf(LOG_TAG"Failed to initialize VPA for SRAM!\n");
        return -1;
    }

    // Initialize Audio Playback
#ifdef CONFIG_VSP_CODEC_OUT_ROUTE_I2S
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    memset((void *)&buffer_config, 0, sizeof(AUDIO_OUT_BUFFER_CONFIG));
#if defined(CONFIG_VSP_SAMPLE_RATE_8K)
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_8000;
#elif defined(CONFIG_VSP_SAMPLE_RATE_16K)
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
#elif defined(CONFIG_VSP_SAMPLE_RATE_48K)
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
#else
#error "OUT sample rate is not supported!"
#endif

    buffer_config.channel_type = AUDIO_OUT_CHANNEL_MONO;

    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#if defined CONFIG_VSP_CODEC_SOURCE_FROM_MIC0
    buffer_config.buffer_addr = (const short *) ctx_header->mic_buffer;
    buffer_config.buffer_size = (ctx_header->mic_buffer_size + 1023) / 1024 * 1024;
#elif defined CONFIG_VSP_CODEC_SOURCE_FROM_OUTPUT0
    buffer_config.buffer_addr = (const short *) ctx_header->ctx_buffer;
    buffer_config.buffer_size = (ctx_header->ctx_size * ctx_header->ctx_num + 1023) / 1024 * 1024;
#endif
#define _BUFFER_CONFIG_ &buffer_config
#else
#define _BUFFER_CONFIG_ NULL
#endif

#ifdef CONFIG_VSP_CODEC_ENABLE_PCM1IN_PLAYER
    AUDIO_OUT_PCM1IN_CONFIG pcm1in_config;
    pcm1in_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
    pcm1in_config.channel_type = AUDIO_OUT_CHANNEL_STEREO;
    pcm1in_config.clock_type = AUDIO_OUT_CLOCK_SYNC;
#define _PCM1IN_CONFIG_ &pcm1in_config
#else
#define _PCM1IN_CONFIG_ NULL
#endif
    AudioOutInit(_PCM1IN_CONFIG_, _BUFFER_CONFIG_);

    // Start Audio Capture
    audio_in_config.record_callback = _CodecAudioInRecordCallback;
    audio_in_config.avad_callback = _CodecAudioInAvadCallback;
    if (AudioInInit(&audio_in_config, 0)) {
        printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
        return -1;
    }

    VspInitializePlcEvent();
    printf(LOG_TAG"Init CODEC mode done with %d ms\n", get_time_ms() - start);
    return 0;
}

static void _CodecModeDone(VSP_MODE_TYPE next_mode)
{
    AudioInDone();
    VspCleanupVpa();
#if defined(CONFIG_VSP_CODEC_OUT_ROUTE_I2S) || defined(CONFIG_VSP_CODEC_ENABLE_PCM1IN_PLAYER)
    AudioOutDone();
#endif
    printf(LOG_TAG"Exit CODEC_A mode\n");
}

static void _CodecModeTick(void)
{
    VspPlcEventTick();
}

//-------------------------------------------------------------------------------------------------

const VSP_MODE_INFO vsp_codec_mode_info = {
    .type = VSP_MODE_CODEC,
    .init = _CodecModeInit,
    .done = _CodecModeDone,
    .proc = NULL,
    .tick = _CodecModeTick,
};
