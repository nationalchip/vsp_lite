/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * vsp_buffer.c: VSP buffer
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <base_addr.h>

#include <vsp_command.h>

#include "vsp_buffer.h"

#define LOG_TAG "[BUFFER]"

#if defined(CONFIG_VSP_BUFFER_HAS_SRAM_MIC) || defined(CONFIG_VSP_BUFFER_HAS_SRAM_REF)
//=================================================================================================
// For STANDBY/UAC/PLC/CODEC workmodes

// Check some parameters
#if (VSP_FRAME_SIZE * CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL) % 128 != 0
#error "Channel Buffer Size SHOULD be times of 1024!"
#endif

#if CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL % CONFIG_VSP_FRAME_NUM_PER_CONTEXT != 0
#error "SRAM Frame Number of Channel SHOULD be times of Frame Number of Context!"
#endif

//-------------------------------------------------------------------------------------------------

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_MIC
// Microphone Input Buffer in SRAM
static SAMPLE s_mic_buffer[VSP_FRAME_SIZE * CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL * CONFIG_MIC_ARRAY_MIC_NUM] __attribute__((aligned(8)));
#endif

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_REF
// Ref Input Buffer in SRAM
static SAMPLE s_ref_buffer[VSP_FRAME_SIZE * CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL * CONFIG_VSP_REF_NUM] __attribute__((aligned(8)));
#endif

// Context Info
static VSP_CONTEXT_HEADER s_context_header __attribute__((aligned(8)));

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_CTX
// Context and its related buffer
static VSP_CONTEXT_BUFFER s_context_buffer[CONFIG_VSP_SRAM_CONTEXT_NUM] __attribute__((aligned(8)));
#endif

static const VSP_BUFFER_SPEC sc_sram_buffer_spec = {
#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_MIC
    .mic_num = CONFIG_MIC_ARRAY_MIC_NUM,
#else
    .mic_num = 0,
#endif
#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_REF
    .ref_num = CONFIG_VSP_REF_NUM,
#else
    .ref_num = 0,
#endif

    .frame_length = VSP_FRAME_LENGTH,
    .frame_num = CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL,
    .sample_rate = VSP_SAMPLE_RATE,

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_CTX
    .out_num = CONFIG_VSP_OUT_NUM,
    .context_size = sizeof(VSP_CONTEXT_BUFFER),
    .context_num = CONFIG_VSP_SRAM_CONTEXT_NUM,
#else
    .out_num = 0,
    .context_size = 0,
    .context_num = 0,
#endif

#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    .ext_buffer_size = CONFIG_VSP_VPA_EXT_BUFFER_SIZE * 1024,
#else
    .ext_buffer_size = 0,
#endif
    .log_buffer_size = 0,
    .tmp_buffer_size = 0,
};

static const unsigned int sc_sram_channel_size = VSP_FRAME_LENGTH * CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL * VSP_SAMPLE_RATE * sizeof(SAMPLE) / 1000;

//-------------------------------------------------------------------------------------------------

const VSP_BUFFER_SPEC *VspGetSRAMBufferSpec(void)
{
    return &sc_sram_buffer_spec;
}

int VspInitSRAMBuffer(void)
{
    // Initialize Context Header
    s_context_header.version = VSP_CONTEXT_VERSION;

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_MIC
    s_context_header.mic_num = CONFIG_MIC_ARRAY_MIC_NUM;
    s_context_header.mic_buffer = (SAMPLE *)MCU_TO_DEV(s_mic_buffer);
    s_context_header.mic_buffer_size = sizeof(s_mic_buffer);
#else
    s_context_header.mic_num = 0;
    s_context_header.mic_buffer = NULL;
    s_context_header.mic_buffer_size = 0;
#endif

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_REF
    s_context_header.ref_num = CONFIG_VSP_REF_NUM;
    s_context_header.ref_buffer = (SAMPLE *)MCU_TO_DEV(s_ref_buffer);
    s_context_header.ref_buffer_size = sizeof(s_ref_buffer);
#else
    s_context_header.ref_num = 0;
    s_context_header.ref_buffer = NULL;    // NULL
    s_context_header.ref_buffer_size = 0;
#endif

    s_context_header.out_num = CONFIG_VSP_OUT_NUM;
#ifdef CONFIG_VSP_OUT_INTERLACED
    s_context_header.out_interlaced = 1;
#else
    s_context_header.out_interlaced = 0;
#endif
    s_context_header.out_buffer_size = CONFIG_VSP_FRAME_NUM_PER_CONTEXT * VSP_FRAME_SIZE * CONFIG_VSP_OUT_NUM * sizeof(SAMPLE);

    s_context_header.frame_num_per_context = CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
    s_context_header.frame_num_per_channel = CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL;
    s_context_header.frame_length = VSP_FRAME_LENGTH;
    s_context_header.sample_rate = VSP_SAMPLE_RATE;
    s_context_header.ref_offset_to_mic = 0;

#ifdef CONFIG_VSP_VPA_FEATURES_DIM
    s_context_header.features_dim_per_frame = CONFIG_VSP_VPA_FEATURES_DIM;
#else
    s_context_header.features_dim_per_frame = 0;
#endif

#ifdef CONFIG_VPA_SNPU_BUFFER_SIZE
    s_context_header.snpu_buffer_size = CONFIG_VPA_SNPU_BUFFER_SIZE * sizeof(SNPU_FLOAT);
#else
    s_context_header.snpu_buffer_size = 0;
#endif

    s_context_header.tmp_buffer = NULL;    // NULL
    s_context_header.tmp_buffer_size = 0;

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_CTX
    s_context_header.ctx_buffer = MCU_TO_DEV(s_context_buffer);
    s_context_header.ctx_size = sizeof(VSP_CONTEXT_BUFFER);
    s_context_header.ctx_num = CONFIG_VSP_SRAM_CONTEXT_NUM;
#else
    s_context_header.ctx_buffer = NULL;
    s_context_header.ctx_size = 0;
    s_context_header.ctx_num = 0;
#endif

#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    s_context_header.ext_buffer_size = CONFIG_VSP_VPA_EXT_BUFFER_SIZE * 1024;
#else
    s_context_header.ext_buffer_size = 0;
#endif

    return 0;
}

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_MIC
int VspGetSRAMAudioInConfig(AUDIO_IN_CONFIG *config)
{
    config->buffer_addr = MCU_TO_DEV(s_mic_buffer);
    config->buffer_size = sizeof(s_mic_buffer);
    config->frame_size = sc_sram_buffer_spec.frame_length * sc_sram_buffer_spec.sample_rate * CONFIG_VSP_FRAME_NUM_PER_CONTEXT * sizeof(SAMPLE) / 1000;
    config->bit_size = VSP_FRAME_BIT_SIZE;
    config->channel_size = sc_sram_channel_size;
    config->mic_channel_num = sc_sram_buffer_spec.mic_num;
    config->ref_channel_num = sc_sram_buffer_spec.ref_num;
    config->sample_rate = sc_sram_buffer_spec.sample_rate;
    config->record_callback = NULL;
    config->avad_callback = NULL;
    return 0;
}
#endif

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_REF
int VspGetSRAMAudioRefConfig(AUDIO_REF_CONFIG *config)
{
    config->buffer_addr = MCU_TO_DEV(s_ref_buffer);
    config->buffer_size = sizeof(s_ref_buffer);
    config->frame_size = sc_sram_buffer_spec.frame_length * sc_sram_buffer_spec.sample_rate * CONFIG_VSP_FRAME_NUM_PER_CONTEXT * sizeof(SAMPLE) / 1000;
    config->channel_size = sc_sram_channel_size;
    config->channel_num = sc_sram_buffer_spec.ref_num;
    config->sample_rate = sc_sram_buffer_spec.sample_rate;
    
    return 0;
}
#endif

VSP_CONTEXT_HEADER * VspGetSRAMContextHeader(void)
{
    return &s_context_header;
}

#ifdef CONFIG_VSP_BUFFER_HAS_SRAM_CTX
int VspGetSRAMContext(unsigned int index, VSP_CONTEXT **context, unsigned int *size)
{
    VSP_CONTEXT_BUFFER *context_buffer  = &s_context_buffer[index % CONFIG_VSP_SRAM_CONTEXT_NUM];
    context_buffer->context.ctx_header  = MCU_TO_DEV(&s_context_header);

#if CONFIG_VSP_OUT_NUM > 0
    context_buffer->context.out_buffer  = MCU_TO_DEV(&context_buffer->out_buffer);
#else
    context_buffer->context.out_buffer  = NULL;
#endif
#ifdef CONFIG_VSP_VPA_FEATURES_DIM
    context_buffer->context.features    = MCU_TO_DEV(&context_buffer->features);
#else
    context_buffer->context.features    = NULL;
#endif
#ifdef CONFIG_VPA_SNPU_BUFFER_SIZE
    context_buffer->context.snpu_buffer = MCU_TO_DEV(&context_buffer->snpu_buffer);
#else
    context_buffer->context.snpu_buffer = NULL;
#endif
#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    context_buffer->context.ext_buffer  = MCU_TO_DEV(&context_buffer->ext_buffer);
#else
    context_buffer->context.ext_buffer  = NULL;
#endif

    *context = &context_buffer->context;
    *size = sizeof(VSP_CONTEXT_BUFFER);
    
    return 0;
}

//-------------------------------------------------------------------------------------------------

SAMPLE * VspGetSRAMOutPutAddr(void)
{
#if CONFIG_VSP_OUT_NUM > 0
    return (SAMPLE *)s_context_buffer[0].out_buffer;
#else
    return NULL;
#endif
}

unsigned int VspGetSRAMMicBufferLength(void)
{
#ifdef CONFIG_VSP_OUT_INTERLACED
    return s_context_header.out_buffer_size / CONFIG_VSP_OUT_NUM * CONFIG_VSP_OUT_INTERLACED_NUM;
#else
    return s_context_header.out_buffer_size;
#endif
}

unsigned int VspGetSRAMBufferLength(void)
{
    return sizeof(VSP_CONTEXT_BUFFER);
}
#endif

#endif  // defined(CONFIG_VSP_BUFFER_HAS_SRAM_MIC) || defined(CONFIG_VSP_BUFFER_HAS_SRAM_REF) @ Line 22

//=================================================================================================
// For ACTIVE/BYPASS/FEED/UAC Linux|DDR modes

#if (VSP_FRAME_SIZE * CONFIG_VSP_DDR_FRAME_NUM_PER_CHANNEL) % 128 != 0
#error "Error DDR Buffer Configuration!"
#endif

#if CONFIG_VSP_DDR_FRAME_NUM_PER_CHANNEL % CONFIG_VSP_FRAME_NUM_PER_CONTEXT != 0
#error "DDR Frame Number of Channel SHOULD be times of Frame Number of Context!"
#endif

static VSP_BUFFER_CONFIG s_ddr_buffer_info = {0};

static const VSP_BUFFER_SPEC sc_ddr_buffer_spec = {
    .mic_num = CONFIG_MIC_ARRAY_MIC_NUM,
    .ref_num = CONFIG_VSP_REF_NUM,
    .out_num = CONFIG_VSP_OUT_NUM,
    .frame_length = VSP_FRAME_LENGTH,
    .frame_num = CONFIG_VSP_DDR_FRAME_NUM_PER_CHANNEL,
    .sample_rate = VSP_SAMPLE_RATE,
    .log_buffer_size = 1024,
    .context_size = sizeof(VSP_CONTEXT_BUFFER),
    .context_num = CONFIG_VSP_DDR_CONTEXT_NUM,
    .tmp_buffer_size = CONFIG_VSP_DDR_TEMP_BUFFER_SIZE * 4 * 1024,
#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    .ext_buffer_size = CONFIG_VSP_VPA_EXT_BUFFER_SIZE * 1024,
#else
    .ext_buffer_size = 0,
#endif
};

static const unsigned int sc_ddr_channel_size = VSP_FRAME_LENGTH * CONFIG_VSP_DDR_FRAME_NUM_PER_CHANNEL * VSP_SAMPLE_RATE / 1000 * sizeof(SAMPLE);

//-------------------------------------------------------------------------------------------------

int VspSetDDRBuffer(const VSP_BUFFER_CONFIG *config)
{
    unsigned int total_size = sc_ddr_channel_size * (sc_ddr_buffer_spec.mic_num + sc_ddr_buffer_spec.ref_num);
    total_size += sc_ddr_buffer_spec.context_size * sc_ddr_buffer_spec.context_num;
    total_size += sizeof(VSP_CONTEXT_HEADER) + sc_ddr_buffer_spec.log_buffer_size;
    total_size += sc_ddr_buffer_spec.tmp_buffer_size;

    if (total_size != config->total_size) {
        printf(LOG_TAG"Buffer size doesn't match! We need %d, but %d provided! \n", total_size, config->total_size);
        return -1;
    }

    // Save configuration
    s_ddr_buffer_info.mic_buffer    = DEV_TO_MCU(config->mic_buffer);
    s_ddr_buffer_info.ref_buffer    = DEV_TO_MCU(config->ref_buffer);
    s_ddr_buffer_info.ctx_buffer    = DEV_TO_MCU(config->ctx_buffer);
    s_ddr_buffer_info.log_buffer    = DEV_TO_MCU(config->log_buffer);
    s_ddr_buffer_info.ctx_header    = DEV_TO_MCU(config->ctx_header);
    s_ddr_buffer_info.tmp_buffer    = DEV_TO_MCU(config->tmp_buffer);
    s_ddr_buffer_info.total_size    = config->total_size;

    return 0;
}

int VspSetStaticDDRBuffer(unsigned int start_addr)
{
    unsigned int mic_buffer_size = sc_ddr_channel_size * sc_ddr_buffer_spec.mic_num;
    unsigned int ref_buffer_size = sc_ddr_channel_size * sc_ddr_buffer_spec.ref_num;
    unsigned int ctx_buffer_size = sc_ddr_buffer_spec.context_size * sc_ddr_buffer_spec.context_num;
    unsigned int log_buffer_size = sc_ddr_buffer_spec.log_buffer_size;
    unsigned int ctx_header_size = sizeof(VSP_CONTEXT_HEADER);
    unsigned int tmp_buffer_size = sc_ddr_buffer_spec.tmp_buffer_size;

    VSP_BUFFER_CONFIG config;
    config.mic_buffer = (void *)start_addr;
    config.ref_buffer = (void *)((unsigned int)config.mic_buffer + mic_buffer_size);
    config.ctx_buffer = (void *)((unsigned int)config.ref_buffer + ref_buffer_size);
    config.log_buffer = (void *)((unsigned int)config.ctx_buffer + ctx_buffer_size);
    config.ctx_header = (void *)((unsigned int)config.log_buffer + log_buffer_size);
    config.tmp_buffer = (void *)((unsigned int)config.ctx_header + ctx_header_size);
    config.total_size = mic_buffer_size + ref_buffer_size + ctx_buffer_size + log_buffer_size + ctx_header_size + tmp_buffer_size;

    return VspSetDDRBuffer(&config);
}

//-------------------------------------------------------------------------------------------------

// Get DDR Buffer Info
const VSP_BUFFER_SPEC *VspGetDDRBufferSpec(void)
{
    return &sc_ddr_buffer_spec;
}

// Initialize the buffer info in DDR
int VspInitDDRBuffer(void)
{
    VSP_CONTEXT_HEADER *ctx_header = s_ddr_buffer_info.ctx_header;
    if (!ctx_header) {
        printf("DDR Buffer is not initialized!\n");
        return -1;
    }

    // Initialize the Context Header
    ctx_header->version = VSP_CONTEXT_VERSION;
    ctx_header->mic_num = sc_ddr_buffer_spec.mic_num;
    ctx_header->ref_num = sc_ddr_buffer_spec.ref_num;
    ctx_header->out_num = sc_ddr_buffer_spec.out_num;

#ifdef CONFIG_VSP_OUT_INTERLACED
    ctx_header->out_interlaced = 1;
#else
    ctx_header->out_interlaced = 0;
#endif
    ctx_header->frame_num_per_context = CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
    ctx_header->frame_num_per_channel = sc_ddr_buffer_spec.frame_num;
    ctx_header->frame_length = sc_ddr_buffer_spec.frame_length;
    ctx_header->sample_rate = sc_ddr_buffer_spec.sample_rate;
    ctx_header->ref_offset_to_mic = 0;

#ifdef CONFIG_VSP_VPA_FEATURES_DIM
    ctx_header->features_dim_per_frame = CONFIG_VSP_VPA_FEATURES_DIM;
#else
    ctx_header->features_dim_per_frame = 0;
#endif

    ctx_header->ctx_buffer = MCU_TO_DEV(s_ddr_buffer_info.ctx_buffer);
    ctx_header->ctx_num = sc_ddr_buffer_spec.context_num;
    ctx_header->ctx_size = sizeof(VSP_CONTEXT_BUFFER);

#ifdef CONFIG_VPA_SNPU_BUFFER_SIZE
    ctx_header->snpu_buffer_size = CONFIG_VPA_SNPU_BUFFER_SIZE * sizeof(SNPU_FLOAT);
#else
    ctx_header->snpu_buffer_size = 0;
#endif
#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    ctx_header->ext_buffer_size = CONFIG_VSP_VPA_EXT_BUFFER_SIZE * 1024;
#else
    ctx_header->ext_buffer_size = 0;
#endif
    ctx_header->out_buffer_size = CONFIG_VSP_FRAME_NUM_PER_CONTEXT * VSP_FRAME_SIZE \
                                                    * sizeof(SAMPLE) * CONFIG_VSP_OUT_NUM;


    ctx_header->mic_buffer = MCU_TO_DEV(s_ddr_buffer_info.mic_buffer);
    ctx_header->mic_buffer_size = sc_ddr_channel_size * sc_ddr_buffer_spec.mic_num;

    ctx_header->ref_buffer = MCU_TO_DEV(s_ddr_buffer_info.ref_buffer);
    ctx_header->ref_buffer_size = sc_ddr_channel_size * sc_ddr_buffer_spec.ref_num;

    ctx_header->tmp_buffer = MCU_TO_DEV(s_ddr_buffer_info.tmp_buffer);
    ctx_header->tmp_buffer_size = sc_ddr_buffer_spec.tmp_buffer_size;
    
    return 0;
}

// Get buffer configuration for Audio In / Ref Settings
int VspGetDDRAudioInConfig(AUDIO_IN_CONFIG *config)
{
    if (!s_ddr_buffer_info.mic_buffer) {
        printf(LOG_TAG"DDR Buffer is not initialized!\n");
        return -1;
    }
    config->buffer_addr = MCU_TO_DEV(s_ddr_buffer_info.mic_buffer);
    config->buffer_size = sc_ddr_channel_size * sc_ddr_buffer_spec.mic_num;
    config->frame_size = sc_ddr_buffer_spec.frame_length * sc_ddr_buffer_spec.sample_rate * CONFIG_VSP_FRAME_NUM_PER_CONTEXT * sizeof(SAMPLE) / 1000;
    config->bit_size = VSP_FRAME_BIT_SIZE;
    config->channel_size = sc_ddr_channel_size;
    config->mic_channel_num = sc_ddr_buffer_spec.mic_num;
    config->ref_channel_num = sc_ddr_buffer_spec.ref_num;
    config->sample_rate = sc_ddr_buffer_spec.sample_rate;
    config->record_callback = NULL;
    config->avad_callback = NULL;
    
    return 0;
}

int VspGetDDRAudioRefConfig(AUDIO_REF_CONFIG *config)
{
    if (!s_ddr_buffer_info.ref_buffer) {
        printf(LOG_TAG"DDR Buffer is not initialized!\n");
        return -1;
    }
    config->buffer_addr = MCU_TO_DEV(s_ddr_buffer_info.ref_buffer);
    config->buffer_size = sc_ddr_channel_size * sc_ddr_buffer_spec.ref_num;
    config->frame_size  = sc_ddr_buffer_spec.frame_length * sc_ddr_buffer_spec.sample_rate * CONFIG_VSP_FRAME_NUM_PER_CONTEXT * sizeof(SAMPLE) / 1000;
    config->channel_size = sc_ddr_channel_size;
    config->channel_num = sc_ddr_buffer_spec.ref_num;
    config->sample_rate = sc_ddr_buffer_spec.sample_rate;
    
    return 0;
}

// Get a context with its related buffer
int VspGetDDRContext(unsigned int index, VSP_CONTEXT **context, unsigned int *size)
{
    VSP_CONTEXT_BUFFER *context_buffer  = &s_ddr_buffer_info.ctx_buffer[index % sc_ddr_buffer_spec.context_num];

    context_buffer->context.ctx_header  = MCU_TO_DEV(s_ddr_buffer_info.ctx_header);

#if CONFIG_VSP_OUT_NUM > 0
    context_buffer->context.out_buffer  = MCU_TO_DEV(context_buffer->out_buffer);
#else
    context_buffer->context.out_buffer  = NULL;
#endif
#ifdef CONFIG_VSP_VPA_FEATURES_DIM
    context_buffer->context.features    = MCU_TO_DEV(&context_buffer->features);
#else
    context_buffer->context.features    = NULL;
#endif
#ifdef CONFIG_VPA_SNPU_BUFFER_SIZE
    context_buffer->context.snpu_buffer = MCU_TO_DEV(&context_buffer->snpu_buffer);
#else
    context_buffer->context.snpu_buffer = NULL;
#endif
#ifdef CONFIG_VSP_VPA_EXT_BUFFER_SIZE
    context_buffer->context.ext_buffer  = MCU_TO_DEV(&context_buffer->ext_buffer);
#else
    context_buffer->context.ext_buffer  = NULL;
#endif
    *context = &context_buffer->context;
    *size = sizeof(VSP_CONTEXT_BUFFER);
    
    return 0;
}

VSP_CONTEXT_HEADER * VspGetDDRContextHeader(void)
{
    return s_ddr_buffer_info.ctx_header;
}

//-------------------------------------------------------------------------------------------------

SAMPLE * VspGetDDROutPutAddr(void)
{
#if CONFIG_VSP_OUT_NUM > 0
    return (SAMPLE *)s_ddr_buffer_info.ctx_buffer[0].out_buffer;
#else
    return NULL;
#endif
}

unsigned int VspGetDDRMicBufferLength(void)
{
    return s_ddr_buffer_info.ctx_header->out_buffer_size;
}

unsigned int VspGetDDRBufferLength(void)
{
    return sizeof(VSP_CONTEXT_BUFFER);
}

//=================================================================================================

// Initialize the buffer info
int VspInitComboBuffer(void)
{
    if (VspInitCmdDDRBuffer()) {
        printf(LOG_TAG"Failed to initialize Cmd Buffer!\n");
        return -1;
    }

    if (VspInitDDRBuffer() || VspInitSRAMBuffer())
        return -1;

    VSP_CONTEXT_HEADER *ddr_ctx_header  = VspGetDDRContextHeader();
    VSP_CONTEXT_HEADER *sram_ctx_header = VspGetSRAMContextHeader();

    ddr_ctx_header->frame_num_per_channel   = sram_ctx_header->frame_num_per_channel;

    ddr_ctx_header->mic_buffer              = sram_ctx_header->mic_buffer;
    ddr_ctx_header->mic_buffer_size         = sram_ctx_header->mic_buffer_size;

    ddr_ctx_header->ref_buffer              = sram_ctx_header->ref_buffer;
    ddr_ctx_header->ref_buffer_size         = sram_ctx_header->ref_buffer_size;

    return 0;
}

// Get buffer configuration for Audio In / Ref Settings
int VspGetComboAudioInConfig(AUDIO_IN_CONFIG *config)
{
    return VspGetSRAMAudioInConfig(config);
}

int VspGetComboSRAMContext(unsigned int index, VSP_CONTEXT **pp_context, unsigned int *size)
{
    return -1;
}

int VspGetComboDDRContext(unsigned int index, VSP_CONTEXT **pp_context, unsigned int *size)
{
    return -1;
}

//=================================================================================================

#ifdef CONFIG_MCU_ENABLE_DEBUG

void VspDumpContext(VSP_CONTEXT *context)
{
    printf(LOG_TAG"===== VSP_CONTEXT:%08X ===========================================\n", (unsigned int)context);
    printf(LOG_TAG" Mask: %04x:%04x ", context->mic_mask, context->ref_mask);
    printf(       "| Index: %u:%u ", context->frame_index, context->ctx_index);
    printf(       "| VAD: %d ", context->vad);
    printf(       "| KWS: %d ", context->kws);
    printf(       "| GAIN: %d|%d ", context->mic_gain, context->ref_gain);
    printf(       "| DIR: %d ", context->direction);
    printf(       "|\n");
}

void VspDumpContextHeader(VSP_CONTEXT_HEADER *context_header)
{
    printf(LOG_TAG"===== VSP_CONTEXT_HEADER:%08X ====================================\n", (unsigned int)context_header);
    printf(LOG_TAG" version:                %08X\n",    context_header->version);

    printf(LOG_TAG" mic_num:                %u\n",      context_header->mic_num);
    printf(LOG_TAG" ref_num:                %u\n",      context_header->ref_num);
    printf(LOG_TAG" out_num:                %u\n",      context_header->out_num);
    printf(LOG_TAG" out_interlaced:         %u\n",      context_header->out_interlaced);
    printf(LOG_TAG" frame_num_per_context:  %u\n",      context_header->frame_num_per_context);
    printf(LOG_TAG" frame_num_per_channel:  %u\n",      context_header->frame_num_per_channel);
    printf(LOG_TAG" frame_length:           %u\n",      context_header->frame_length);
    printf(LOG_TAG" sample_rate:            %u\n",      context_header->sample_rate);

    printf(LOG_TAG" ref_offset_to_mic:      %u\n",      context_header->ref_offset_to_mic);

    printf(LOG_TAG" features_dim_per_frame: %u\n",      context_header->features_dim_per_frame);
    // CTX buffer for GLOBAL
    printf(LOG_TAG" ctx_buffer:             %08X\n",    (unsigned int)context_header->ctx_buffer);
    printf(LOG_TAG" ctx_num:                %u\n",      context_header->ctx_num);
    printf(LOG_TAG" ctx_size:               %u\n",      context_header->ctx_size);
    // SNPU, EXTRA and OUT buffer for CONTEXT
    printf(LOG_TAG" snpu_buffer_size:       %u\n",      context_header->snpu_buffer_size);
    printf(LOG_TAG" ext_buffer_size:        %u\n",      context_header->ext_buffer_size);
    printf(LOG_TAG" out_buffer_size:        %u\n",      context_header->out_buffer_size);
    // MIC buffer for GLOBAL
    printf(LOG_TAG" mic_buffer:             %08X\n",    (unsigned int)context_header->mic_buffer);
    printf(LOG_TAG" mic_buffer_size:        %u\n",      context_header->mic_buffer_size);
    // REF buffer for GLOBAL
    printf(LOG_TAG" ref_buffer:             %08X\n",    (unsigned int)context_header->ref_buffer);
    printf(LOG_TAG" ref_buffer_size:        %u\n",      context_header->ref_buffer_size);
    // TMP buffer for GLOBAL
    printf(LOG_TAG" tmp_buffer:             %08X\n",    (unsigned int)context_header->tmp_buffer);
    printf(LOG_TAG" tmp_buffer_size:        %u\n",      context_header->tmp_buffer_size);
}

#endif

//=================================================================================================

static const VSP_CMD_BUFFER_SPEC cmd_buffer_spec = {
    .cmd_num        = CONFIG_VSP_COMMAND_NUMBER,
    .cmd_size       = sizeof(VSP_COMMAND_BUFFER),
    .cmd_data_size  = CONFIG_VSP_COMMAND_DATA_SIZE * 4 * 1024
};

const VSP_CMD_BUFFER_SPEC *VspGetDDRCmdBufferSpec(void)
{
    return &cmd_buffer_spec;
}

#if CONFIG_VSP_COMMAND_NUMBER

static VSP_CMD_BUFFER_CONFIG s_ddr_cmd_buffer_info = {0};
static int g_command_index = -1;

int VspSetDDRCmdBuffer(const VSP_CMD_BUFFER_CONFIG *config)
{
    unsigned int total_size = cmd_buffer_spec.cmd_size * cmd_buffer_spec.cmd_num + sizeof(VSP_COMMAND_HEADER);

    if (total_size != config->total_size) {
        printf(LOG_TAG"CMD Buffer size doesn't match! We need %d, but %d provided! \n", total_size, config->total_size);
        return -1;
    }

    s_ddr_cmd_buffer_info.cmd_buffer = DEV_TO_MCU(config->cmd_buffer);
    s_ddr_cmd_buffer_info.cmd_header = DEV_TO_MCU(config->cmd_header);
    s_ddr_cmd_buffer_info.total_size = config->total_size;

    return 0;
}

int VspInitCmdDDRBuffer(void)
{
    VSP_COMMAND_HEADER *cmd_header = s_ddr_cmd_buffer_info.cmd_header;
    if (!cmd_header) {
        printf("Cmd DDR Buffer is not initialized!\n");
        return -1;
    }

    cmd_header->version = VSP_COMMAND_VERSION;
    cmd_header->cmd_buffer = s_ddr_cmd_buffer_info.cmd_buffer;
    cmd_header->cmd_num = CONFIG_VSP_COMMAND_NUMBER;

    g_command_index = 0;
    return 0;
}

int VspGetDDRCmd(unsigned int index, VSP_COMMAND **command, unsigned int *size)
{
    if(g_command_index == -1) return -1;

    VSP_COMMAND_BUFFER *command_buffer = &s_ddr_cmd_buffer_info.cmd_buffer[index % cmd_buffer_spec.cmd_num];

    command_buffer->command.cmd_header = MCU_TO_DEV(s_ddr_cmd_buffer_info.cmd_header);
#if CONFIG_VSP_COMMAND_DATA_SIZE
    command_buffer->command.cmd_data   = MCU_TO_DEV(command_buffer->cmd_data);
#else
    command_buffer->command.cmd_data   = NULL;
#endif
    *command = &command_buffer->command;
    *size = sizeof(VSP_COMMAND_BUFFER);

    return 0;
}

int VspGetDDRNextCmd(VSP_COMMAND **command, unsigned int *size)
{
    if(g_command_index == -1) return -1;

    VSP_COMMAND_BUFFER *command_buffer  = &s_ddr_cmd_buffer_info.cmd_buffer[g_command_index % cmd_buffer_spec.cmd_num];

    command_buffer->command.cmd_header = MCU_TO_DEV(s_ddr_cmd_buffer_info.cmd_header);
    command_buffer->command.cmd_index  = g_command_index;
#if CONFIG_VSP_COMMAND_DATA_SIZE
    command_buffer->command.cmd_data   = MCU_TO_DEV(command_buffer->cmd_data);
#else
    command_buffer->command.cmd_data   = NULL;
#endif

    *command = &command_buffer->command;
    *size = sizeof(VSP_COMMAND_BUFFER);

    g_command_index++;
    return 0;
}

VSP_COMMAND_HEADER * VspGetDDRCommandHeader(void)
{
    return s_ddr_cmd_buffer_info.cmd_header;
}

//=================================================================================================

#else

int VspSetDDRCmdBuffer(const VSP_CMD_BUFFER_CONFIG *config){return 0;}
int VspInitCmdDDRBuffer(void){return 0;}
int VspGetDDRCmd(unsigned int index, VSP_COMMAND **command, unsigned int *size){return -1;}
int VspGetDDRNextCmd(VSP_COMMAND **command, unsigned int *size){return -1;}
VSP_COMMAND_HEADER * VspGetDDRCommandHeader(void){return NULL;}

#endif

