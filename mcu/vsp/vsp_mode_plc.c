/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_mode_plc.c:
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <base_addr.h>

#include <board_config.h>

#include <driver/audio_mic.h>
#include <driver/audio_ref.h>
#include <driver/audio_out.h>
#include <driver/cpu.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/usb_gadget.h>
#include <driver/vsp_uac_fifo_manager.h>
#include <driver/spi_flash.h>
#include <driver/misc.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uac2_core.h>
#include <driver/watchdog.h>

#include <vsp_firmware.h>
#include <vsp_param.h>

#include "vsp_mode.h"
#include "vsp_buffer.h"

#include "common/vsp_led_player.h"
#include "common/vsp_vpa.h"
#include "common/vsp_plc_event.h"
#include "common/vsp_uart_proxy.h"
#include "common/vsp_agc.h"

#define LOG_TAG "[PLC]"

// Configuration Check
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM

#if CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF > CONFIG_VSP_SRAM_FRAME_NUM_PER_CHANNEL / CONFIG_VSP_FRAME_NUM_PER_CONTEXT
#error "PLC Context Offset is great than Context Num"
#endif

#else

#if CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF > CONFIG_VSP_DDR_FRAME_NUM_PER_CHANNEL / CONFIG_VSP_FRAME_NUM_PER_CONTEXT
#error "PLC Context Offset is great than Context Num"
#endif

#endif

#ifdef CONFIG_MCU_ENABLE_CPU
#define VPA_FLASH_OFFSET CONFIG_VSP_PLC_LOAD_DSP_FROM
#else
#define VPA_FLASH_OFFSET DSP_FW_OFFSET
#endif
//=================================================================================================
#define FLASH_OFFSET_VPA_CONFIGE VPB_PART_2
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_STANDBY
// Timeout Count
#define AVAD_TIMEOUT_COUNT      (2000) // The unit is milliseconds
#define DVAD_TIMEOUT_COUNT      (2000) // The unit is milliseconds
#define KWS_TIMEOUT_COUNT       (9000) // The unit is milliseconds

typedef enum {
    VSP_STANDBY_STATE_WAIT,         // AudioIn wait for the AVAD signal
    VSP_STANDBY_STATE_AVAD,         // Process Audio for VAD signal
    VSP_STANDBY_STATE_DVAD,         // Process Audio for KWS signal
    VSP_STANDBY_STATE_WAKUP,        // Wait for CPU resume
} VSP_STANDBY_STATE;

static struct {
    VSP_STANDBY_STATE       state;
    int                     count_down; // Count Down
    SNPU_TASK               kws_task;
} s_standby_state = {0};
#endif

typedef struct {
    unsigned int work_flage; //0:no work;1;write;2:read.
    void *source_addr;
    unsigned int source_size;
    unsigned int flash_offset;
} FLASH_BRIDGE_HANDLE;

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_FLASH_BRIDGE
static FLASH_BRIDGE_HANDLE s_flash_bridge_handle = {.flash_offset = USER_STORAGE_OFFSET};
#endif
//=================================================================================================

#ifdef CONFIG_VSP_PLC_ESR_WAKEUP_AFTER_KWS
// Major Wake Up Kws Value
static struct {
    unsigned char process_flag;       // 1: open esr; 0: close esr
    unsigned int  process_count_down;
} s_esr_state = {0};
#endif

//=================================================================================================

typedef struct {
    unsigned char vad;
    unsigned char key;
} VSP_UAC_HID_VAD_TABLE;

typedef struct {
    unsigned char kws;
    unsigned char key;
} VSP_UAC_HID_KWS_TABLE;

typedef enum {
    VSP_UAC_SUB_MODE_STATE_IDLE         = 0x00,
    VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_N = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_N = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT | 1 << UAC2_PLAYBACK_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_Y = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT | 1 << UAC2_RECORD_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_Y = 1 << UAC2_CONNECT_STATE_BIT | 1 << UAC2_HID_STATE_BIT | 1 << UAC2_PLAYBACK_STATE_BIT | 1 << UAC2_RECORD_STATE_BIT,
    VSP_UAC_SUB_MODE_STATE_KWS_ENABLE   = 0x10,
    VSP_UAC_SUB_MODE_STATE_VAD          = 0x30,
} VSP_UAC_SUB_MODE_STATE;

typedef void (*VSP_UAC_SUB_MODE_PROC)(void);

typedef struct {
    VSP_UAC_SUB_MODE_STATE  state; // sub-mode in UAC MODE
    LED_PIXEL               pixel;
    VSP_UAC_SUB_MODE_PROC   proce;
} VSP_UAC_SUB_MODE_INFO;

//=================================================================================================

#define AUDIO_OUT_BUFFER_SIZE (3072 * 3)
static char s_audio_out_buffer[AUDIO_OUT_BUFFER_SIZE+256] __attribute__((aligned(8)));

static struct {
    unsigned int    audio_in_handle;
    unsigned int    audio_out_handle;
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
    int             hid_handle;
#endif
    int             gs_handle;
    char            hid_key[8];
    unsigned char   kws_value;
    unsigned char   kws_enable;
    unsigned char   vad_value;
    unsigned char   vad_enable;
    unsigned char   sub_mode_last_index;
    unsigned        reserved:2;
    unsigned int    led_state;            // 0: uninitialized; 1: initialized
    unsigned int    uac_state;            // 0: uninitialized; 1: initialized
    unsigned int    ctx_last_index;
} s_uac_state = {0};

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
static const VSP_UAC_HID_VAD_TABLE s_uac_hid_vad_table[] = {
    {  0, 0x14}, // vad == 0       -> 'q'
    {  1, 0x1A}, // vad == 1       -> 'w'
};

static const VSP_UAC_HID_KWS_TABLE s_uac_hid_kws_table[] = {
    {100, 0x04}, //      -> 'a'
    {101, 0x05}, //      -> 'b'
    {102, 0x06}, //      -> 'c'
    {103, 0x07}, //      -> 'd'
    {104, 0x08}, //      -> 'e'
    {105, 0x09}, //      -> 'f'
    {106, 0x0A}, //      -> 'g'
    {107, 0x0B}, //      -> 'h'
    {108, 0x0C}, //      -> 'i'
    {109, 0x0D}, //      -> 'j'
    {110, 0x0E}, //      -> 'k'
    {111, 0x0F}, //      -> 'l'
    {112, 0x10}, //      -> 'm'
};
#endif

//=================================================================================================

static void _UacSubModeVadProce(void)
{
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
    // Send hid
    for (unsigned int i = 0; i < sizeof(s_uac_hid_vad_table) / sizeof(VSP_UAC_HID_VAD_TABLE); ++i) {
        if(s_uac_state.vad_value == s_uac_hid_vad_table[i].vad) {
            s_uac_state.hid_key[2] = s_uac_hid_vad_table[i].key;
            HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);
            s_uac_state.hid_key[2] = 0x00;
            HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);
            break;
        }
    }
#endif
    s_uac_state.vad_enable = 0;
}

static void _UacSubModeKwsProce(void)
{
    if (s_uac_state.kws_value) {
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_IRR
        IrrSendCode(0x12, 0x34);
#endif

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
        // Send hid
        for (unsigned int i = 0; i < sizeof(s_uac_hid_kws_table) / sizeof(VSP_UAC_HID_KWS_TABLE); ++i) {
            if(s_uac_state.kws_value == s_uac_hid_kws_table[i].kws) {
                s_uac_state.hid_key[2] = s_uac_hid_kws_table[i].key;
                HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);
                s_uac_state.hid_key[2] = 0x00;
                HidWrite(s_uac_state.hid_handle, (unsigned char *)s_uac_state.hid_key, 8);
                break;
            }
        }
#endif
        s_uac_state.kws_value = 0;
    }
}

static const VSP_UAC_SUB_MODE_INFO s_uac_sub_mode_list[] = {
    {VSP_UAC_SUB_MODE_STATE_IDLE        , .pixel.bits = {  0,   0,   0,   0}, NULL},                 // idle
    {VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_N, .pixel.bits = {  0,   0, 255, 255}, NULL},                 // no_aplay and no_record    -> blue
    {VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_N, .pixel.bits = {  0, 255, 255, 255}, NULL},                 // aplay and no_record       -> cyan
    {VSP_UAC_SUB_MODE_STATE_PLAY_N_REC_Y, .pixel.bits = {255,   0, 255, 255}, NULL},                 // no_aplay and record       -> purple
    {VSP_UAC_SUB_MODE_STATE_PLAY_Y_REC_Y, .pixel.bits = {  0, 255,   0, 255}, NULL},                 // aplay and record          -> green
    {VSP_UAC_SUB_MODE_STATE_VAD         , .pixel.bits = {  0,   0,   0,   0}, _UacSubModeVadProce},  // vad                       -> NULL
    {VSP_UAC_SUB_MODE_STATE_KWS_ENABLE  , .pixel.bits = {255,   0,   0, 255}, _UacSubModeKwsProce},  // kws enable                -> red blink
};

//=================================================================================================
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_FLASH_BRIDGE
static int _PlcAudioInRecordCallback(int frame_index, void *priv);
static int _FlashBridgeProcess(void)
{
    int result = -1;
    unsigned int work_flage = s_flash_bridge_handle.work_flage;
    s_flash_bridge_handle.work_flage = 0;

    if (work_flage == 0 || s_flash_bridge_handle.source_size > FLASH_BLOCK_SIZE || s_flash_bridge_handle.flash_offset == 0) result = -1;
    else {
        AudioInDone();
        VspSuspendVpa();
        mdelay(10);

        if (SpiFlashInit(NULL, 0) != 0) {
            printf(LOG_TAG"Flash init error!\n");
            result = -1;
        }

        if (work_flage == 1) {  // write
            result =  SpiFlashErase(s_flash_bridge_handle.flash_offset, FLASH_BLOCK_SIZE);
            if (result != -1)
                result = SpiFlashWrite(s_flash_bridge_handle.flash_offset, s_flash_bridge_handle.source_addr, s_flash_bridge_handle.source_size);
            if (result != -1)
                result = SpiFlashRead(s_flash_bridge_handle.flash_offset, s_flash_bridge_handle.source_addr, s_flash_bridge_handle.source_size) == -1 ? -1 : 0;
        } else if (work_flage == 2) { // read
            result = SpiFlashRead(s_flash_bridge_handle.flash_offset, s_flash_bridge_handle.source_addr, s_flash_bridge_handle.source_size) == -1 ? -1 : 1;
        }

        // Start Audio In
        // Audio In should be put at the tail.
        AUDIO_IN_CONFIG audio_in_config;
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
        VspGetSRAMAudioInConfig(&audio_in_config);
#else
        VspGetDDRAudioInConfig(&audio_in_config);
#endif
        audio_in_config.record_callback = _PlcAudioInRecordCallback;
        if (AudioInInit(&audio_in_config, 0)) {
            printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
            result = -1;
        }
    }

    printf (LOG_TAG"Mic Gain is %d dB\n", AudioInGetMicGain());
    printf (LOG_TAG"Ref Gain is %d dB\n", AudioInGetRefGain());
    VSP_MSG_MCU_DSP message;
    message.header.type = VMT_MD_REQUEST_FLASH_BRIDGE_DONE;
    message.header.param = result;
    message.header.size = sizeof(VMB_MD_RESPONSE_FLASH_BRIDGE) / sizeof(unsigned int);
    message.response_flash_bridge.addr = (void *)MCU_TO_DEV(s_flash_bridge_handle.source_addr);
    message.response_flash_bridge.size = s_flash_bridge_handle.source_size;
    DspPostVspMessage((unsigned int *)&message.header.value, message.header.size + 1);

    return result; // result == -1 : error
}
#endif

static int _PlcDspCallback(volatile VSP_MSG_DSP_MCU *request, void *priv)
{
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_WATCHDOG
    WatchdogPing();
#endif
#ifdef CONFIG_VSP_PLC_ESR_WAKEUP_AFTER_KWS
    if (s_esr_state.process_flag) {
        s_esr_state.process_count_down++;
    }
#endif
    if (request->header.type == VMT_DM_PROCESS_DONE) {
        if (request->header.param == VSP_PROCESS_ACTIVE || request->header.param == VSP_PROCESS_TALKING || request->header.param == VSP_PROCESS_TEST) {
            VSP_CONTEXT *context = DEV_TO_MCU(request->process_done.context_addr);
            SAMPLE *out_buffer = DEV_TO_MCU(context->out_buffer);

            PLC_EVENT plc_event;
            plc_event.event_id = DSP_PROCESS_DONE_EVENT_ID;
            plc_event.ctx_index = context->ctx_index;
            VspTriggerPlcEvent(plc_event); // Notify to send wav to bluetooth

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CODEC_OUT_I2S
            VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#if defined CONFIG_VSP_PLC_MODE_CODEC_OUT_I2S_SOURCE_FROM_MIC0
            int frame_length       = ctx_header->frame_length * ctx_header->sample_rate / 1000;
            int context_sample_num = frame_length * ctx_header->frame_num_per_context;
            int context_num_per_channel = ctx_header->frame_num_per_channel / ctx_header->frame_num_per_context;
            int current_context_index = context->ctx_index % context_num_per_channel;
            unsigned int current_mic_addr = (unsigned int)(ctx_header->mic_buffer + context_sample_num * current_context_index);
            void *play_addr = (void *)(current_mic_addr - (unsigned int)ctx_header->mic_buffer);
#elif defined CONFIG_VSP_PLC_MODE_CODEC_OUT_I2S_SOURCE_FROM_OUTPUT0
            void *play_addr = (void *)((unsigned int)context->out_buffer - (unsigned int)ctx_header->ctx_buffer);
#endif
            int   play_size = (ctx_header->out_buffer_size / ctx_header->out_num) / 8 * 8;

            AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, play_addr, play_size);
#endif

#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
            AudioBuffProduce(s_uac_state.audio_in_handle, out_buffer, VspGetSRAMMicBufferLength());
#else   // DDR + Linux
            AudioBuffProduce(s_uac_state.audio_in_handle, out_buffer, VspGetDDRMicBufferLength());
#ifdef CONFIG_VSP_PLC_WORKS_ON_LINUX
            // Send Context to CPU
            VSP_MSG_MCU_CPU message;
            message.header.type = VMT_MC_PROCESS_DONE;
            message.header.size = sizeof(VMB_MC_PROCESS_DONE) / sizeof(unsigned int);
            message.header.magic = request->header.magic;
            message.header.param = request->header.param;
            message.process_done.context_addr = request->process_done.context_addr;
            message.process_done.context_size = request->process_done.context_size;
            CpuPostVspMessage(&message.header.value, message.header.size + 1);
#endif
#endif

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_STANDBY
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
            VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#else
            VSP_CONTEXT_HEADER *ctx_header = VspGetDDRContextHeader();
#endif

            int context_length = ctx_header->frame_length * ctx_header->frame_num_per_context;
            if (s_standby_state.state == VSP_STANDBY_STATE_AVAD) {
                if (context->vad) { // if VAD, switch to DVAD
                    s_standby_state.state = VSP_STANDBY_STATE_DVAD;
                    s_standby_state.count_down = DVAD_TIMEOUT_COUNT / context_length;
                }
                else {  // Count down
                    s_standby_state.count_down--;
                    if (!s_standby_state.count_down) {
                        s_standby_state.state = VSP_STANDBY_STATE_WAIT;
                    }
                }
            }
            else if (s_standby_state.state == VSP_STANDBY_STATE_DVAD) {
                if (context->vad) {
                    s_standby_state.count_down = DVAD_TIMEOUT_COUNT / context_length;
                    if (context->kws) {
                        s_standby_state.state = VSP_STANDBY_STATE_WAKUP;
                    }
                }
                else {  // Count down
                    s_standby_state.count_down--;
                    if (!s_standby_state.count_down) {
                        s_standby_state.state = VSP_STANDBY_STATE_AVAD;
                        s_standby_state.count_down = AVAD_TIMEOUT_COUNT / context_length;
                    }
                }
            }
            else if (s_standby_state.state == VSP_STANDBY_STATE_WAKUP) {
                if (context->vad) {
                    s_standby_state.count_down = KWS_TIMEOUT_COUNT / context_length;
                    s_standby_state.state = VSP_STANDBY_STATE_WAKUP;
                }
                else {  // Count down
                    s_standby_state.count_down--;
                    if (!s_standby_state.count_down) {
                        s_standby_state.state = VSP_STANDBY_STATE_AVAD;
                        s_standby_state.count_down = AVAD_TIMEOUT_COUNT / context_length;
                    }
                }
            }
#endif
            if (context->vad != s_uac_state.vad_value) {
                if ((context->ctx_index - s_uac_state.ctx_last_index) > 2) {
                    s_uac_state.vad_value = context->vad;
                    s_uac_state.vad_enable = 1;
                    s_uac_state.ctx_last_index = context->ctx_index;
                }
            }

            if (s_uac_state.kws_enable)
                s_uac_state.kws_enable--;

#ifdef CONFIG_VSP_PLC_ESR_WAKEUP_AFTER_KWS
            if (context->kws) {
                if(context->kws == CONFIG_VSP_MAJOR_KWS_VALUE) {
                    s_esr_state.process_flag = 1;
                }
                if (s_esr_state.process_flag || context->kws == CONFIG_VSP_MAJOR_KWS_VALUE) {
                    s_esr_state.process_count_down = 0;

                    s_uac_state.kws_value = context->kws;
                    s_uac_state.kws_enable = 10; // Control led blink duration of 10 * 30ms

                    PLC_EVENT plc_event = {.event_id = s_uac_state.kws_value};
                    VspTriggerPlcEvent(plc_event);
                }
            }
#else
            if (context->kws) {
                s_uac_state.kws_value = context->kws;
                s_uac_state.kws_enable = 10; // Control led blink duration of 10 * 30ms

                PLC_EVENT plc_event = {.event_id = s_uac_state.kws_value};
                VspTriggerPlcEvent(plc_event);
            }
#endif
        }
    }

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_FLASH_BRIDGE
    if (request->header.type == VMT_DM_REQUEST_FLASH_BRIDGE) {
        s_flash_bridge_handle.source_addr = DEV_TO_MCU(request->request_flash_bridge.addr);
        s_flash_bridge_handle.source_size = request->request_flash_bridge.size;
        s_flash_bridge_handle.work_flage = request->request_flash_bridge.cmd + 1;
    }
#endif
    return 0;
}

//=================================================================================================

static const UAC2_DEVICE_DESCRIPTION _uac2_device_description = {
    .sound_card_name_in_windows = CONFIG_VSP_UAC_NAME_IN_WINDOWS,
    .manufacturer_name          = CONFIG_VSP_UAC_MANUFACTURER_NAME,
    .product_name               = CONFIG_VSP_UAC_PRODUCT_NAME,
    .serial_number              = CONFIG_VSP_UAC_SERIAL_NUMBER,
};

static UAC2_CALLBACKS s_uac2_callback = {
    .volume_callback                 = NULL,
    .mute_callback                   = NULL,
    .auto_gain_control_callback      = NULL,
    .bass_boost_callback             = NULL,

    .notify_callback                 = NULL,
    .audio_out_get_sdc_addr_callback = NULL,
};

//=================================================================================================

static int _PlcAudioInRecordCallback(int frame_index, void *priv)
{
    if (frame_index > CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF) {
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = frame_index - (CONFIG_VSP_PLC_CONTEXT_OFFSET_BETWEEN_MIC_AND_REF + 1);
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);
#else
        VspGetDDRContext(context_index, &ctx_addr, &ctx_size);
#endif
        ctx_addr->mic_mask      = AudioInGetMicEnable();
        ctx_addr->ref_mask      = 0xFFFF;
        ctx_addr->frame_index   = context_index * CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
        ctx_addr->ctx_index     = context_index;
        ctx_addr->vad           = 0;
        ctx_addr->kws           = 0;
#ifdef CONFIG_VSP_PLC_ENABLE_AGC
        if (AudioInAgcGetEnable()) {
            ctx_addr->mic_gain      = VspAgcGetMicGain();
            ctx_addr->ref_gain      = VspAgcGetRefGain();
        }
        else {
            ctx_addr->mic_gain      = AudioInGetMicGain();
            ctx_addr->ref_gain      = AudioInGetRefGain();
        }
#else
        ctx_addr->mic_gain      = AudioInGetMicGain();
        ctx_addr->ref_gain      = AudioInGetRefGain();
#endif
        ctx_addr->direction     = -1;

        // Send a context to DSP
        VSP_MSG_MCU_DSP message;
        message.header.type = VMT_MD_PROCESS;

        PLC_STATUS plc_status = {.vpa_mode = PLC_STATUS_VPA_ACTIVE};
        VspGetPlcStatus(PST_SWITCH_VPA_MODE, &plc_status);
        if (plc_status.vpa_mode == PLC_STATUS_VPA_TALKING) {
            message.header.param = VSP_PROCESS_TALKING;
        } else if (plc_status.vpa_mode == PLC_STATUS_VPA_ACTIVE) {
            message.header.param = VSP_PROCESS_ACTIVE;
        } else if (plc_status.vpa_mode == PLC_STATUS_VPA_TEST) {
            message.header.param = VSP_PROCESS_TEST;
        }

        message.header.magic = context_index;
        message.header.size = sizeof(VMB_MD_PROCESS) / sizeof(unsigned int);
        message.process.context_addr = (void *)MCU_TO_DEV(ctx_addr);
        message.process.context_size = ctx_size;
        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
    }

#ifdef CONFIG_VSP_PLC_ENABLE_AGC
    if (AudioInAgcGetEnable()) {
        VspAudioInAgcAdjustGain();
    }
#endif
    return 0;
}
//=================================================================================================

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_WATCHDOG
static int _PlcWatchDogLevelCallback(int tmp, void *priv)
{
    AudioInDone();
    VspCleanupVpa();

    while(1) {
#if defined(CONFIG_VSP_PLC_WORKS_ON_SRAM) || defined(CONFIG_VSP_PLC_WORKS_ON_DDR)
        // Load VPA from Flash
        if (VspLoadVpaFromFlash(VPA_FLASH_OFFSET, s_audio_out_buffer, AUDIO_OUT_BUFFER_SIZE)) {
            printf(LOG_TAG"LoadDSP Failed !\n");
            continue;
        }
#endif
        mdelay(200); // wait LOAD_DSP to initialize
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
        if (VspInitializeVpa(VspGetSRAMContextHeader(), _PlcDspCallback, NULL)) {
            printf(LOG_TAG"Failed to initialize VPA for SRAM!\n");
            continue;
        }
#else
        if (VspInitializeVpa(VspGetDDRContextHeader(), _PlcDspCallback, NULL)) {
            printf(LOG_TAG"Failed to initialize VPA for DDR!\n");
            continue;
        }
#endif
        // Start Audio In
        // Audio In should be put at the tail.
        AUDIO_IN_CONFIG audio_in_config;
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
        VspGetSRAMAudioInConfig(&audio_in_config);
#else
        VspGetDDRAudioInConfig(&audio_in_config);
#endif
        audio_in_config.record_callback = _PlcAudioInRecordCallback;
        if (AudioInInit(&audio_in_config, 0)) {
            printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
            continue;
        }
        break;
    }

    WatchdogPing();

    return 0;
}
#endif

//=================================================================================================

static int _UacInit(void)
{
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CAPTURE
#define UAC_CAPTURE_CHANNEL_NUM  CONFIG_VSP_OUT_INTERLACED_NUM
#define UAC_PLAYBACK_CHANNEL_NUM 2

    UAC2_CHANNEL_CONFIG vsp_to_uac2_channel_config;
    vsp_to_uac2_channel_config.channel_mask = (1 << UAC_CAPTURE_CHANNEL_NUM) - 1;
    vsp_to_uac2_channel_config.sample_rate = 16000;

    UAC2_CHANNEL_CONFIG uac2_to_ref_channel_config;
    uac2_to_ref_channel_config.channel_mask = (1 << UAC_PLAYBACK_CHANNEL_NUM) - 1;
    uac2_to_ref_channel_config.sample_rate = 48000;

    UsbCompositeInit(&vsp_to_uac2_channel_config, &uac2_to_ref_channel_config,
            &_uac2_device_description, &s_uac2_callback,
            NULL);
#else
    UsbCompositeInit(NULL, NULL, &_uac2_device_description, NULL, NULL);
#endif

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
    // Start HID
    s_uac_state.hid_handle = HidOpen();
#endif

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CAPTURE
    uac_core_config_t config;

    memset(&config, 0, sizeof(uac_core_config_t));
    config.policy = UAC_CLOCK_SYNC_THROUGH_CAPTURE;

    config.capture_enable      = 1;
    config.capture_sample_rate = vsp_to_uac2_channel_config.sample_rate;
    config.capture_channel_num = UAC_CAPTURE_CHANNEL_NUM;
    config.capture_bitwidth    = 16;

    uac_core_init(&config);
#endif

    return 0;
}

static int _CodecInit(void)
{
    // Initialize Audio Playback
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CODEC
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CODEC_OUT_I2S
    AUDIO_OUT_BUFFER_CONFIG buffer_config;
    memset((void *)&buffer_config, 0, sizeof(AUDIO_OUT_BUFFER_CONFIG));
#if defined(CONFIG_VSP_SAMPLE_RATE_8K)
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_8000;
#elif defined(CONFIG_VSP_SAMPLE_RATE_16K)
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
#elif defined(CONFIG_VSP_SAMPLE_RATE_48K)
    buffer_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
#else
#error "OUT sample rate is not supported!"
#endif

    buffer_config.channel_type = AUDIO_OUT_CHANNEL_MONO;

    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#if defined CONFIG_VSP_PLC_MODE_CODEC_OUT_I2S_SOURCE_FROM_MIC0
    buffer_config.buffer_addr = (const short *) ctx_header->mic_buffer;
    buffer_config.buffer_size = (ctx_header->mic_buffer_size + 1023) / 1024 * 1024;
#elif defined CONFIG_VSP_PLC_MODE_CODEC_OUT_I2S_SOURCE_FROM_OUTPUT0
    buffer_config.buffer_addr = (const short *) ctx_header->ctx_buffer;
    buffer_config.buffer_size = (ctx_header->ctx_size * ctx_header->ctx_num + 1023) / 1024 * 1024;
#endif
#define _BUFFER_CONFIG_ &buffer_config
#else
#define _BUFFER_CONFIG_ NULL
#endif

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CODEC_PCM1IN_PLAYER
    AUDIO_OUT_PCM1IN_CONFIG pcm1in_config;
    pcm1in_config.sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
    pcm1in_config.channel_type = AUDIO_OUT_CHANNEL_STEREO;
    pcm1in_config.clock_type = AUDIO_OUT_CLOCK_SYNC;
#define _PCM1IN_CONFIG_ &pcm1in_config
#else
#define _PCM1IN_CONFIG_ NULL
#endif
    AudioOutInit(_PCM1IN_CONFIG_, _BUFFER_CONFIG_);
#endif
    return 0;
}

//=================================================================================================

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_STANDBY
static int _StandbyAudioInAvadCallback(uint8_t state, void *priv)
{
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#else
    VSP_CONTEXT_HEADER *ctx_header = VspGetDDRContextHeader();
#endif

    int context_length = ctx_header->frame_length * ctx_header->frame_num_per_context;
    BoardResume();
    s_standby_state.state      = VSP_STANDBY_STATE_AVAD;
    s_standby_state.count_down = AVAD_TIMEOUT_COUNT / context_length;
    return 0;
}

static int _StandbyInitAudioIn(void)
{
    AUDIO_IN_CONFIG audio_in_config;
    VspGetSRAMAudioInConfig(&audio_in_config);
    audio_in_config.record_callback = _PlcAudioInRecordCallback;
    audio_in_config.avad_callback = _StandbyAudioInAvadCallback;
    if (AudioInInit(&audio_in_config, 1)) {
        printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
        return -1;
    }
    return 0;
}
#endif

//=================================================================================================
static int _PlcModeInit(VSP_MODE_TYPE prev_mode)
{
    printf(LOG_TAG"Init PLC mode\n");

    unsigned int start = get_time_ms();
    memset(&s_uac_state, 0, sizeof(s_uac_state));

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_WATCHDOG
    WatchdogInit(CONFIG_VSP_PLC_WATCHDOG_RESET_TIMEOUT, CONFIG_VSP_PLC_WATCHDOG_LEVEL_TIMEOUT, _PlcWatchDogLevelCallback);
#endif


#if defined(CONFIG_VSP_PLC_WORKS_ON_SRAM) || defined(CONFIG_VSP_PLC_WORKS_ON_DDR)
    // Load VPA
    if (VspLoadVpa(VPA_FLASH_OFFSET, s_audio_out_buffer, AUDIO_OUT_BUFFER_SIZE)) {
        printf(LOG_TAG"LoadDSP Failed !\n");
        return -1;
    }
#endif

#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    // Prepare Buffer for SRAM mode
    VspInitSRAMBuffer();
#endif
#ifdef CONFIG_VSP_PLC_WORKS_ON_DDR
    // Prepare Buffer for DDR without Linux mode
    CpuWakeup();
    mdelay(1000);
    VspSetStaticDDRBuffer(CONFIG_VSP_PLC_DDR_START_ADDR);
    VspInitDDRBuffer();
#endif
#ifdef CONFIG_VSP_PLC_WORKS_ON_LINUX
    VspInitDDRBuffer();
#endif

    // UAC Buffer Initialization
    // Audio Out -> UAC Input
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    s_uac_state.audio_in_handle = AudioBuffMngrInit(VspGetSRAMOutPutAddr(), VspGetSRAMMicBufferLength(), 0, VspGetSRAMBufferLength());
#else
    s_uac_state.audio_in_handle = AudioBuffMngrInit(VspGetDDROutPutAddr(), VspGetDDRMicBufferLength(), 0, VspGetDDRBufferLength());
#endif

    s_uac2_callback.ain_buff_handle = s_uac_state.audio_in_handle;

    // Start UAC
    _UacInit();

    _CodecInit();

    mdelay(50); // wait LOAD_DSP to initialize
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    if (VspInitializeVpa(VspGetSRAMContextHeader(), _PlcDspCallback, NULL)) {
        printf(LOG_TAG"Failed to initialize VPA for SRAM!\n");
        return -1;
    }
#else
    if (VspInitializeVpa(VspGetDDRContextHeader(), _PlcDspCallback, NULL)) {
        printf(LOG_TAG"Failed to initialize VPA for DDR!\n");
        return -1;
    }
#endif

    // Start Audio In
    // Audio In should be put at the tail.
    AUDIO_IN_CONFIG audio_in_config;
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    VspGetSRAMAudioInConfig(&audio_in_config);
#else
    VspGetDDRAudioInConfig(&audio_in_config);
#endif
    audio_in_config.record_callback = _PlcAudioInRecordCallback;
    if (AudioInInit(&audio_in_config, 0)) {
        printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
        return -1;
    }
    printf (LOG_TAG"Mic Gain is %d dB\n", AudioInGetMicGain());
    printf (LOG_TAG"Ref Gain is %d dB\n", AudioInGetRefGain());

    s_uac_state.uac_state = 1;

    VspInitializePlcEvent();
    PLC_EVENT plc_event = {.event_id = VSP_WAKE_UP_EVENT_ID};
    VspTriggerPlcEvent(plc_event);
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_STANDBY
    s_standby_state.state = VSP_STANDBY_STATE_WAIT;
#endif

#ifdef CONFIG_VSP_PLC_ENABLE_LOAD_VPA_CONFIG_FROM_FLASH
    VspLoadVpaConfigFromFlash(FLASH_OFFSET_VPA_CONFIGE, (unsigned char *)s_audio_out_buffer);
#endif

#ifdef CONFIG_VSP_PLC_ENABLE_AGC
    // Enable AGC
    VspInitGainControl(NULL);
    VspAudioInInitAgcConfig(NULL);
    VspAudionInSetAgcSmoothParam(50);
#endif
    printf(LOG_TAG"Init PLC mode done with %d ms\n", get_time_ms() - start);
    return 0;
}

//-------------------------------------------------------------------------------------------------

static void _PlcModeDone(VSP_MODE_TYPE next_mode)
{
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_WATCHDOG
    WatchdogDone();
#endif

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
    HidClose(s_uac_state.hid_handle);
#endif

    UsbCompositeDone();

    AudioInDone();

    AudioBuffMngrDone(s_uac_state.audio_in_handle);
    AudioBuffMngrDone(s_uac_state.audio_out_handle);

    VspCleanupVpa();

    memset(&s_uac_state, 0, sizeof(s_uac_state));
    printf(LOG_TAG"Exit PLC mode\n");
}

//=================================================================================================

static void _UacUpdateUI(void)
{
    unsigned int usb_current_state = 0;

    if (s_uac_state.vad_enable) {
        usb_current_state = VSP_UAC_SUB_MODE_STATE_VAD;
    }
    else if (s_uac_state.kws_enable) {
        usb_current_state = VSP_UAC_SUB_MODE_STATE_KWS_ENABLE;
    }
    else {
        usb_current_state = Uac2GetState();
    }

    int sub_mode_current_index = -1;

    for (unsigned int i = 0; i < sizeof(s_uac_sub_mode_list) / sizeof(VSP_UAC_SUB_MODE_INFO); ++i) {
        if(usb_current_state == s_uac_sub_mode_list[i].state) {
            sub_mode_current_index = i;
            break;
        }
    }

    if (sub_mode_current_index != -1) {
        // switch to current mode
        if (sub_mode_current_index != s_uac_state.sub_mode_last_index) {
            s_uac_state.sub_mode_last_index = sub_mode_current_index;
        }
    } else {
        // switch to sub-idle mode
        s_uac_state.sub_mode_last_index = 0;
    }

    LedSetPixel(0, s_uac_sub_mode_list[s_uac_state.sub_mode_last_index].pixel);
    LedFlush();

    if (s_uac_sub_mode_list[s_uac_state.sub_mode_last_index].proce)
        s_uac_sub_mode_list[s_uac_state.sub_mode_last_index].proce();
}

#ifdef CONFIG_VSP_PLC_MODE_WAKEUP_GPIO
static int _ButtonGpioCallbackStandby(unsigned int port, void *pdata)
{

    s_standby_state.state      = VSP_STANDBY_STATE_WAIT;
    s_standby_state.count_down = 0;

    GpioDisableTrigger(CONFIG_VSP_PLC_MODE_WAKEUP_GPIO_PORT);
    return 0;
}
static int _ButtonGpioCallbackWakeup(unsigned int port, void *pdata)
{
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#else
    VSP_CONTEXT_HEADER *ctx_header = VspGetDDRContextHeader();
#endif

    int context_length = ctx_header->frame_length * ctx_header->frame_num_per_context;
    BoardResume();
    s_standby_state.state      = VSP_STANDBY_STATE_AVAD;
    s_standby_state.count_down = DVAD_TIMEOUT_COUNT / context_length;

    GpioDisableTrigger(CONFIG_VSP_PLC_MODE_WAKEUP_GPIO_PORT);

    if (GpioEnableTrigger(CONFIG_VSP_PLC_MODE_WAKEUP_GPIO_PORT, GPIO_TRIGGER_EDGE_FALLING, _ButtonGpioCallbackStandby, NULL)) {
        printf(LOG_TAG"GpioEnableTrigger Failed !\n");
        return -1;
    }

    return 0;
}
#endif

static void _PlcModeTick(void)
{
    VspPlcEventTick();
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_FLASH_BRIDGE
    _FlashBridgeProcess();
#endif

#ifdef CONFIG_VSP_PLC_ESR_WAKEUP_AFTER_KWS
#ifdef CONFIG_VSP_PLC_WORKS_ON_SRAM
    VSP_CONTEXT_HEADER *ctx_header = VspGetSRAMContextHeader();
#else
    VSP_CONTEXT_HEADER *ctx_header = VspGetDDRContextHeader();
#endif

    int context_length = ctx_header->frame_length * ctx_header->frame_num_per_context;
    if ((s_esr_state.process_count_down * context_length) >= CONFIG_VSP_PLC_ESR_TIMEOUT_COUNT) {
        s_esr_state.process_flag       = 0;
        s_esr_state.process_count_down = 0;

        PLC_EVENT plc_event = {.event_id = ESR_GOODBYE_EVENT_ID};
        VspTriggerPlcEvent(plc_event);
    }
#endif
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_STANDBY
    if (s_standby_state.state == VSP_STANDBY_STATE_WAIT) {
        WAKEUP_SOURCE wakeup_source = 0;
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_WATCHDOG
        WatchdogDone();
#endif
        BoardSuspend();
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_SEND_HID
        HidClose(s_uac_state.hid_handle);
#endif
        UsbCompositeDone();
        AudioInDone();
        VspSuspendVpa();

        _StandbyInitAudioIn();
#ifdef CONFIG_VSP_PLC_MODE_WAKEUP_AVAD
        wakeup_source |= WAKEUP_SOURCE_AVAD;
#endif

#ifdef CONFIG_VSP_PLC_MODE_WAKEUP_GPIO
        if (GpioEnableTrigger(CONFIG_VSP_PLC_MODE_WAKEUP_GPIO_PORT, GPIO_TRIGGER_EDGE_FALLING, _ButtonGpioCallbackWakeup, NULL)) {
            printf(LOG_TAG"GpioEnableTrigger Failed !\n");
            return -1;
        }
        wakeup_source |= WAKEUP_SOURCE_GPIO;
#endif
        // Sleep MCU Here!
        printf(LOG_TAG"Sleep...");
        ClockSleep(wakeup_source);

        gx_disable_irq();
        _UacInit();
        gx_enable_irq();

        VspResumeVpa();
#ifdef CONFIG_VSP_PLC_MODE_ENABLE_WATCHDOG
        WatchdogInit(CONFIG_VSP_PLC_WATCHDOG_RESET_TIMEOUT, CONFIG_VSP_PLC_WATCHDOG_LEVEL_TIMEOUT, _PlcWatchDogLevelCallback);
#endif
        printf("Wake!\n");
    }
#endif

    if (s_uac_state.uac_state && !s_uac_state.led_state) {
        LedInit();
        s_uac_state.led_state = 1;
    }

    _UacUpdateUI();

    if (!s_uac_state.uac_state && s_uac_state.led_state) {
        LedDone();
        s_uac_state.led_state = 0;
    }

#ifdef CONFIG_VSP_PLC_MODE_ENABLE_CAPTURE
    uac_core_task();
    AudioBuffPrintStatus(0);
#endif
}

//=================================================================================================

const VSP_MODE_INFO vsp_plc_mode_info = {
    .type = VSP_MODE_PLC,
    .init = _PlcModeInit,
    .done = _PlcModeDone,
    .proc = NULL,
    .tick = _PlcModeTick
};

