/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_main_mini.c: MCU main routine for 8008/8008b
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>

#include <base_addr.h>

#include <driver/console.h>
#include <driver/irq.h>
#include <driver/delay.h>
#include <driver/timer.h>
#include <driver/dsp.h>
#include <driver/misc.h>
#include <driver/pmu.h>
#include <driver/clock.h>
#include <driver/rtc.h>
#include <driver/otp.h>
#include <driver/usb_gadget.h>
#include <driver/udc.h>
#include <driver/dma_axi.h>
#include <driver/imageinfo.h>
#include <vsp_message.h>

#include "vsp_mode.h"
#include "vsp_buffer.h"
#include "vsp_upgrade.h"

#include "common/vsp_uart_proxy.h"
#include "common/uart_message_v2.h"

#define LOG_TAG "[MAIN]"

//=================================================================================================
static void _SystemInit(void)
{
    CacheInit();
    gx_irq_init();
    BoardInit();
    ConsoleInit();

    DelayInit();

    GsOpen();

    // Print out system information
    printf("Voice Signal Preprocess\n");
    printf("Copyright (C) 2001-2019 NationalChip Co., Ltd\n");
    printf("ALL RIGHTS RESERVED!\n");
    printf("Hello MCU!\n");
    printf("MCU <-> DSP Message Version: [%0X]\n", VSP_MESSAGE_VERSION_BTW_MCU_DSP);
    printf("Board Model:[%s]\n", CONFIG_BOARD_MODEL);
    printf("MCU Version:[%s]\n", MCU_VERSION);
    printf("Build Date: [%s]\n", BUILD_TIME);

    // Print out chip information
    OtpInit(OTP_MODE_READONLY);
    char chip_name[OTP_CHIP_NAME_LENGTH + 1] = {0};
    OtpGetChipName(chip_name, OTP_CHIP_NAME_LENGTH);
    printf("CHIP NAME:  [%s]\n", chip_name);

    unsigned long long public_id = 0;
    OtpGetPublicID(&public_id);
    printf("PUBLIC ID:  [%016llx]\n", public_id);
    OtpDone();

    ImageInfoInit(IMAGE_INFO_OFFSET);

#ifdef CONFIG_GX8008B
    dw_dmac_init();
#endif
    TimerInit(10);

#ifndef CONFIG_MCU_UDC_ENABLED
    UdcDisable();
#endif

#ifdef CONFIG_VSP_UART_PROXY_ENABLE
    VspUartProxy();
#endif

}

//-------------------------------------------------------------------------------------------------

static void _SystemDone(void)
{
    DspDone();
    GsClose();
    CacheDone();
    TimerDone();
    BoardDone();
    gx_disable_all_interrupt();
}

//=================================================================================================

int main(int argc, char **argv)
{
    /* clear BSS section */
    extern int _start_bss_ , _end_bss_;
    memset(&_start_bss_, 0, (uint32_t)&_end_bss_ - (uint32_t)&_start_bss_);

    // In cold boot
    ClockInit();
    _SystemInit();

#if defined(CONFIG_VSP_INIT_WORKMODE_BOOT)
    VspInitMode(VSP_MODE_BOOT);
#elif defined(CONFIG_VSP_INIT_WORKMODE_UAC)
    VspInitMode(VSP_MODE_UAC);
#elif defined(CONFIG_VSP_INIT_WORKMODE_CODEC)
    VspInitMode(VSP_MODE_CODEC);
#elif defined(CONFIG_VSP_INIT_WORKMODE_PLC)
    VspInitMode(VSP_MODE_PLC);
#elif defined(CONFIG_VSP_INIT_WORKMODE_FACTORY)
    VspInitMode(VSP_MODE_FACTORY);
#else
    VspInitMode(VSP_MODE_IDLE);
#endif

    while(VspModeTick()) {
#ifdef CONFIG_VSP_UART_PROXY_ENABLE
        UartMessageAsyncTick();
#endif
    }
    _SystemDone();

    return 0;
}
