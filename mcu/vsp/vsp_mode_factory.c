/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_mode_factory.c: VSP Factory mode
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <base_addr.h>

#include <vsp_param.h>
#include <board_config.h>

#include <driver/audio_mic.h>
#include <driver/audio_ref.h>
#include <driver/audio_out.h>
#include <driver/cpu.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/misc.h>
#include <driver/uart.h>
#include <driver/usb_gadget.h>
#include <driver/otp.h>
#include <driver/oem.h>
#include <driver/timer.h>
#include <driver/gpio.h>

#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
#include <driver/uart_message.h>
#else
#include <driver/message.h>
#endif
#include "common/vsp_vpa.h"

#include "vsp_mode.h"
#include "vsp_buffer.h"
#include "vsp_mode_factory_wav.h"

#define MUSIC_BUFFER ((const short *)vsp_mode_factory_speaker_test_wav_buffer)
#define MUSIC_LENGTH ((vsp_mode_factory_speaker_test_wav_buffer_len - 48)  / 1024 * 1024)

#ifdef CONFIG_VSP_VPA_FACTORY_ENABLE_MIC_CALC_SNR
#if CONFIG_VSP_FACTORY_TEST_MIC_AND_REF_TMIE < CONFIG_VSP_VPA_FACTORY_SNR_SIGNAL_SHIFT_TIME
#error "The time you test mic must not be less than the time you test snr!"
#endif
#endif

#define FACTORY_TEST_MIC_REF_PREFORMANCE_MAX_COUNT  (CONFIG_VSP_FACTORY_TEST_MIC_AND_REF_TMIE)
#define FACTORY_TEST_MIC_CONSISTENCY_THRESHOLD  4

#define FACTORY_TEST_SPEAKER_PLAY_TIME      (2*1000) // millisecond

#ifdef CONFIG_MCU_ENABLE_CPU
#define VPA_FLASH_OFFSET CONFIG_VSP_FACTORY_LOAD_DSP_FROM
#else
#define VPA_FLASH_OFFSET DSP_FACTORY_FW_OFFSET
#endif

#define LOG_TAG "[Factory]"

//=================================================================================================

typedef void (*VSP_FACTORY_INIT_FUNC)(void);
typedef void (*VSP_FACTORY_DONE_FUNC)(void);
typedef void (*VSP_FACTORY_PROC_FUNC)(void);
typedef struct {
    char *name;
    unsigned char tag;
    VSP_FACTORY_INIT_FUNC init;
    VSP_FACTORY_DONE_FUNC done;
    VSP_FACTORY_PROC_FUNC function;
} VSP_FACTORY_TEST_SUITE;

typedef struct {
    int level;     // The dB of amplitude
    int amplitude; // The signal of amplitude
    int frequency; // The frequency at maximum energy
    int snr;       // The snr of the microphone board
} VSP_FACTORY_PERFORMANCE;

//=================================================================================================

static void _TestLedInit(void)
{
    printf (LOG_TAG"Test Led Init\n");
    LedInit();
}

static void _TestLedFunction(void)
{
    printf (LOG_TAG"Test Led Function\n");
    LED_PIXEL led_pixel;
    led_pixel.value = 0xffff;
    LedSetPixel(0, led_pixel);
    mdelay(400);

    led_pixel.value = 0xff00ff;
    LedSetPixel(0, led_pixel);
    mdelay(400);

    led_pixel.value = 0xff0000ff;
    LedSetPixel(0, led_pixel);
    mdelay(400);
}

static void _TestLedDone(void)
{
    printf (LOG_TAG"Test Led Done\n\n\n");
    LedDone();
}

//-------------------------------------------------------------------------------------------------
static volatile int g_play_count = 0;
static int _FactoryTestSpeakerCallback(unsigned int sdc_addr, void *priv)
{
    g_play_count++;
    unsigned int music_length = MUSIC_LENGTH;
    AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, 0);
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, 0, music_length);

    return 0;
}

static void _TestSpeakerInit(void)
{
    printf (LOG_TAG"Test Speaker Init\n");

    const unsigned char *music_buffer = (unsigned char *)MUSIC_BUFFER;
    unsigned int music_length         = MUSIC_LENGTH;
    AUDIO_OUT_BUFFER_CONFIG buffer_config = {
        .sample_rate  = AUDIO_OUT_SAMPLE_RATE_8000,
        .channel_type = AUDIO_OUT_CHANNEL_MONO,
        .buffer_addr  = (const short *)MCU_TO_DEV(music_buffer + 48),
        .buffer_size  = music_length,
        .play_callback = _FactoryTestSpeakerCallback
    };
    g_play_count = 0;

    AudioOutInit(NULL, &buffer_config);
}

static void _TestSpeakerFunction(void)
{
    printf (LOG_TAG"Test Speaker Function\n");
    unsigned int music_length = MUSIC_LENGTH;
    AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, 0);
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, 0, music_length);
}

static void _TestSpeakerDone(void)
{
    while(FACTORY_TEST_SPEAKER_PLAY_TIME/200 > g_play_count);
    AudioOutDone();
    printf (LOG_TAG"Test Speaker Done\n\n\n");
}

//-------------------------------------------------------------------------------------------------

static int _TestMicAudioInRecordCallback(int frame_index, void *priv)
{
    // Delay a frame to make sure REF is fullfilled or algorithm may visit data across context
    if (frame_index > 0) {
        VSP_CONTEXT *ctx_addr;
        unsigned int ctx_size;
        unsigned context_index = frame_index - 1; // prev_context
#ifndef CONFIG_MCU_ENABLE_CPU
        VspGetSRAMContext(context_index, &ctx_addr, &ctx_size);
#else
        VspGetDDRContext(context_index, &ctx_addr, &ctx_size);
#endif

        ctx_addr->mic_mask      = AudioInGetMicEnable();
        ctx_addr->ref_mask      = 0xFFFF;
        ctx_addr->frame_index   = context_index * CONFIG_VSP_FRAME_NUM_PER_CONTEXT;
        ctx_addr->ctx_index     = context_index;
        ctx_addr->vad           = 0;
        ctx_addr->kws           = 0;
        ctx_addr->mic_gain      = AudioInGetMicGain();
        ctx_addr->ref_gain      = AudioInGetRefGain();
        ctx_addr->direction     = -1;

        // Send a context to DSP
        VSP_MSG_MCU_DSP message;
        message.header.type = VMT_MD_PROCESS;
        message.header.param = VSP_PROCESS_ACTIVE;
        message.header.magic = context_index;
        message.header.size = sizeof(VMB_MD_PROCESS) / sizeof(unsigned int);
        message.process.context_addr = (void *)MCU_TO_DEV(ctx_addr);
        message.process.context_size = ctx_size;

        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
    }

    return 0;
}

static int g_context_index = 0; // Record the number of context has been  processed
static int _TestMicDspCallback(volatile VSP_MSG_DSP_MCU *request, void *priv)
{
    if (request->header.type == VMT_DM_PROCESS_DONE) {
        if (request->header.param == VSP_PROCESS_ACTIVE) {
            //VSP_CONTEXT *context = DEV_TO_MCU(request->process_done.context_addr);
            //if (context->ctx_index % FACTORY_TEST_MIC_PREFORMANCE_MAX_COUNT == 0) g_context_index++;
            g_context_index++;
        }
    }

    return 0;
}

static void _TestMicAndRefInit(void)
{
    printf (LOG_TAG"Test Mic and Ref Init\n");
#ifndef CONFIG_MCU_ENABLE_CPU
    if (VspInitializeVpa(VspGetSRAMContextHeader(), _TestMicDspCallback, NULL)) {
#else
    if (VspInitializeVpa(VspGetDDRContextHeader(), _TestMicDspCallback, NULL)) {
#endif
        printf(LOG_TAG"Failed to initialize VPA!\n");
        return;
    }

    AUDIO_IN_CONFIG audio_in_config;
    AUDIO_REF_CONFIG audio_ref_config;
#ifndef CONFIG_MCU_ENABLE_CPU
    if (VspGetSRAMAudioInConfig(&audio_in_config) || VspGetSRAMAudioRefConfig(&audio_ref_config)) {
#else
    if (VspGetDDRAudioInConfig(&audio_in_config) || VspGetDDRAudioRefConfig(&audio_ref_config)) {
#endif
        printf(LOG_TAG"Can't get Audio Configuration!\n");
        return;
    }

    audio_in_config.record_callback = _TestMicAudioInRecordCallback;
    audio_in_config.avad_callback = NULL;
    if (AudioInInit(&audio_in_config, 0)) {
        printf(LOG_TAG"Failed to initialize the Audio In Device!\n");
        return;
    }

#ifdef CONFIG_VSP_FACTORY_ENABLE_RECORD_AND_PLAY_BY_SELF
    const unsigned char *music_buffer = (unsigned char *)MUSIC_BUFFER;
    unsigned int music_length         = MUSIC_LENGTH;
    AUDIO_OUT_BUFFER_CONFIG buffer_config = {
        .sample_rate  = AUDIO_OUT_SAMPLE_RATE_8000,
        .channel_type = AUDIO_OUT_CHANNEL_MONO,
        .buffer_addr  = (const short *)MCU_TO_DEV(music_buffer + 48),
        .buffer_size  = music_length,
        .play_callback = _FactoryTestSpeakerCallback
    };
    AudioOutInit(NULL, &buffer_config);
#endif
}

static void _TestMicAndRefFunction(void)
{
#define TEST_FREQ 1000
    printf (LOG_TAG"Test Mic and Ref Function\n");
    int i;
    int k = 1; // Need initialize 1
    int mic_test_result = 1;
    int ref_test_result = 1;
    int mic_fail_count = 0;
    int ref_fail_count = 0;

#ifdef CONFIG_VSP_FACTORY_ENABLE_RECORD_AND_PLAY_BY_SELF
    unsigned int music_length = MUSIC_LENGTH;
    AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, 0);
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, 0, music_length);  //Play test signal
    mdelay(500);
#endif

    VSP_FACTORY_PERFORMANCE mic_performance[CONFIG_MIC_ARRAY_MIC_NUM];
    VSP_FACTORY_PERFORMANCE ref_performance[CONFIG_VSP_REF_NUM];
    while (k <= FACTORY_TEST_MIC_REF_PREFORMANCE_MAX_COUNT) {
        if (k < g_context_index) {
            for (; k < g_context_index; k++) {
                VSP_CONTEXT *ctx_addr = NULL;
                unsigned int ctx_size = 0;
#ifndef CONFIG_MCU_ENABLE_CPU
                VspGetSRAMContext(k, &ctx_addr, &ctx_size);
#else
                VspGetDDRContext(k, &ctx_addr, &ctx_size);
#endif
                float *features = DEV_TO_MCU(ctx_addr->features);
                for (i = 0; i < CONFIG_MIC_ARRAY_MIC_NUM; i++) {
                    mic_performance[i].level     = features[i * 4 + 0];
                    mic_performance[i].amplitude = features[i * 4 + 1] * 1000000;
                    mic_performance[i].frequency = features[i * 4 + 2];
                    mic_performance[i].snr       = features[i * 4 + 3];
                }
                // Check microphone consistency
                for (i = 1; i < CONFIG_MIC_ARRAY_MIC_NUM; i++) {
                    int level_difference     = mic_performance[i].level - mic_performance[i - 1].level;
                    int frequency_difference = mic_performance[i].frequency - mic_performance[i - 1].frequency;
                    int threshold            = FACTORY_TEST_MIC_CONSISTENCY_THRESHOLD;
                    if (level_difference < threshold && level_difference > -threshold
                        && frequency_difference == 0
                        && mic_performance[i-1].amplitude > 0 && mic_performance[i].amplitude > 0
                        && mic_performance[i-1].frequency == TEST_FREQ && mic_performance[i].frequency == TEST_FREQ) {
                        // printf(LOG_TAG" Test M%d and M%d ---> Pass\n", i - 1, i);
                    } else {
                        printf (LOG_TAG"M%d: [L:%d dB, A:0.%06d, F:%d Hz, S:%d]\n"
                                , i-1
                                , mic_performance[i-1].level
                                , mic_performance[i-1].amplitude
                                , mic_performance[i-1].frequency
                                , mic_performance[i-1].snr);
                        printf (LOG_TAG"M%d: [L:%d dB, A:0.%06d, F:%d Hz, S:%d]\n"
                                , i
                                , mic_performance[i].level
                                , mic_performance[i].amplitude
                                , mic_performance[i].frequency
                                , mic_performance[i].snr);
                        printf (LOG_TAG"[%d] Test M%d and M%d ---> Failed\n", k, i - 1, i);
                        mic_fail_count++;
                    }
                }

                for (i = 0; i < CONFIG_VSP_REF_NUM; i++) {
                    int mic_num   = CONFIG_MIC_ARRAY_MIC_NUM;
                    ref_performance[i].level     = features[mic_num*4 + i * 4 + 0];
                    ref_performance[i].amplitude = features[mic_num*4 + i * 4 + 1] * 1000000;
                    ref_performance[i].frequency = features[mic_num*4 + i * 4 + 2];
                }
                // Check the reference channel
                for (i = 1; i < CONFIG_VSP_REF_NUM; i++) {
                    if (ref_performance[i-1].amplitude > 0 && ref_performance[i].amplitude > 0
                        && ref_performance[i-1].frequency == TEST_FREQ && ref_performance[i].frequency == TEST_FREQ) {
                        // printf(LOG_TAG" Test R%d and R%d ---> Pass\n", i - 1, i);
                    } else {
                        printf (LOG_TAG"R%d: [L:%d dB, A:0.%06d, F:%d Hz]\n"
                                , i-1
                                , ref_performance[i-1].level
                                , ref_performance[i-1].amplitude
                                , ref_performance[i-1].frequency);
                        printf (LOG_TAG"R%d: [L:%d dB, A:0.%06d, F:%d Hz]\n"
                                , i
                                , ref_performance[i].level
                                , ref_performance[i].amplitude
                                , ref_performance[i].frequency);
                        printf(LOG_TAG"[%d] Test R%d and R%d ---> Failed\n", k, i - 1, i);
                        ref_fail_count++;
                    }
                }
            }
        }
        mdelay(VSP_FRAME_LENGTH);
    }

    if (mic_fail_count > 5) mic_test_result = 0;
    if (ref_fail_count > 5) ref_test_result = 0;

    printf("Mic Test Result : %s\n", mic_test_result ? "Mic Pass" : "Mic Failed");
    printf("Ref Test Result : %s\n", ref_test_result ? "Ref Pass" : "Ref Failed");

#ifdef CONFIG_MCU_ENABLE_CPU
    VSP_COMMAND    *command_addr;
    unsigned int    command_size;
    if(VspGetDDRNextCmd(&command_addr, &command_size))
        return;

    command_addr->cmd_id = 233;
    command_size = sizeof("Mic Test Result : Mic Failed") + 1;
    memcpy((void *)DEV_TO_MCU(command_addr->cmd_data), (mic_test_result ? "Mic Test Result : Mic Pass  " : "Mic Test Result : Mic Failed"), 1 + sizeof("Mic Test Result : Mic Failed"));
    VSP_MSG_MCU_CPU response;

    response.header.type = VMT_MC_SEND_COMMAND;
    response.header.size = sizeof(VMB_MC_SEND_COMMAND) / sizeof(unsigned int);
    response.header.magic = 0;
    response.header.param = 0;

    response.send_command.command_addr = (void *)MCU_TO_DEV(command_addr);
    response.send_command.command_size = command_size;

    CpuPostVspMessage((const unsigned int *)&response, response.header.size + 1);
#endif

    g_context_index = 0;
}

static void _TestMicAndRefDone(void)
{
    AudioInDone();
#ifdef CONFIG_VSP_FACTORY_ENABLE_RECORD_AND_PLAY_BY_SELF
    AudioOutDone();
#endif
    VspCleanupVpa();
    printf (LOG_TAG"Test Mic and Ref Done\n\n\n");
}

//--------------------------------------------------------------------------------------------

static void _TestUartInit(void)
{
    printf (LOG_TAG"Test Uart Init\n");
    UartInit(UART_PORT0, 460800);
}

static void _TestUartFunction(void)
{
#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
    int times = 2000;
    int tmp_id = 0;
    int tmp_state = 0;
    UartSendCommand(UCI_NOTIFY_TX_MTC, UCA_TEST_DATA);
    while (times--) {
        UartGetPacket();
        tmp_id = UartGetCommandID();
        tmp_state = UartGetCommandState();
        if ((tmp_id == UCI_NOTIFY_RX_MTC_STATE) && (tmp_state == UCA_TEST_SUCCESS)) {
            printf("Uart Test Result : Uart Pass\n");
            return;
        }
    }
    if (times <= 0) printf("Uart Test Result : Uart Failed\n");
    return;
#endif
    printf("Uart Test Result : Uart Failed\n");
}

static void _TestUartDone(void)
{
    printf(LOG_TAG"Test Uart Done\n\n\n");
}

static void _TestSnInit(void)
{
    OtpInit(OTP_MODE_READONLY);
}

static void _TestSnDone(void)
{
    printf(LOG_TAG"Test Sn Done\n\n\n");
}

static void _TestSnFunction(void)
{
    unsigned long long public_id = 0;
    OtpGetPublicID(&public_id);
    printf("PUBLIC ID: %016llx\n", public_id);
}

static void _TestOTPInit(void)
{
    OtpInit(OTP_MODE_READONLY);
}

static void _TestOTPDone(void)
{
    unsigned int lock = 0;
    OtpGetLock(&lock);
    printf("OTP Test Result : %s\n", lock ? "OTP Pass" : "OTP Failed");
}

static void _TestOTPFunction(void)
{
    printf(LOG_TAG"Test OTP Done\n\n\n");
}

static void _TestFinishInit(void)
{
}

static void _TestFinishDone(void)
{
}

static void _TestFinishFunction(void)
{
    printf("Finish Test Result : Finish Pass\n");
}

static void _ReadOemInit(void) {}

static void _ReadOemDone(void) {}

static void _ReadOemFunction(void)
{
    OEM_INFO oem_info;

    OemRead(&oem_info);

    printf("ROEM");
    UartSendBuffer(UART_PORT0, (unsigned char *)(&oem_info), sizeof(OEM_INFO));
    printf("RMEO");
}

static void _WriteOemInit(void) {}

static void _WriteOemDone(void) {}

static void _WriteOemFunction(void)
{
    OEM_INFO oem_info;

    printf("WOEM");
    UartRecvBuffer(UART_PORT0, (unsigned char *)(&oem_info), sizeof(OEM_INFO));
    printf("WMEO");

    OemWrite(&oem_info);
}

static void _ReadUserInit(void) {}

static void _ReadUserDone(void) {}

static void _ReadUserFunction(void)
{
    USER_DATA user_data;

    UserDataRead(&user_data);

    printf("RUSER");
    UartSendBuffer(UART_PORT0, (unsigned char *)(&user_data), sizeof(USER_DATA));
    printf("RRESU");
}

static void _WriteUserInit(void) {}

static void _WriteUserDone(void) {}

static void _WriteUserFunction(void)
{
    USER_DATA user_data;

    printf("WUSER");
    UartRecvBuffer(UART_PORT0, (unsigned char *)(&user_data), sizeof(USER_DATA));
    printf("WRESU");

    UserDataWrite(&user_data);
}

static void _TestHostInit(void) {}

static void _TestHostDone(void) {}

#ifndef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
static int StreamIsEmpty(void)
{
    return !UartCanRecvByte(UART_PORT0);
}

static int StreamRead(unsigned char *buf, int len)
{
    for (int i = 0; i < len; i++)
        buf[i] = UartRecvByte(UART_PORT0);
    return len;
}

static int StreamWrite(const unsigned char *buf, int len)
{
    for (int i = 0; i < len ; i++)
        UartSendByte(UART_PORT0, buf[i]);
    return len;
}

typedef enum {
    MSG_NTF_UART_GET_HOST_CONDITION = NEW_MSG(MSG_TYPE_NTF, 0x0E),
    MSG_NTF_UART_HOST_CONDITION     = NEW_MSG(MSG_TYPE_NTF, 0x0F),
} UART_MSG_ID;

static int _HostTestTimerCallback(void *private_data)
{
    *(int*)(private_data) += 1;
    if (*(int*)(private_data) == 3) {
        printf("Host Test Result : Host Failed\n");
        return 0;
    }
    return 0;
}
#endif

static void _TestHostFunction(void) {
#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
    int times = 2000;
    int tmp_id = 0;
    int tmp_state = 0;
    UartSendCommand(UCI_NOTIFY_TX_MTC, UCA_TEST_DATA);
    while (times--) {
        UartGetPacket();
        tmp_id = UartGetCommandID();
        tmp_state = UartGetCommandState();
        if ((tmp_id == UCI_NOTIFY_RX_MTC_STATE) && (tmp_state == UCA_TEST_SUCCESS)) {
            printf("Host Test Result : Host Pass\n");
            return;
        }
    }
    if (times <= 0) printf("Host Test Result : Host Failed\n");
    return;
#else

    MessageInit((STREAM_READ)StreamRead, (STREAM_WRITE)StreamWrite, (STREAM_EMPTY)StreamIsEmpty);

    MESSAGE notify_recv;
    int host_condition = 0;
    notify_recv.body = (unsigned char *)&host_condition;
    notify_recv.bodylen = 4;
    MESSAGE notify_send;
    notify_send.cmd = MSG_NTF_UART_GET_HOST_CONDITION;
    notify_send.seq = 0;
    notify_send.flags = 0;
    notify_send.body = NULL;
    notify_send.bodylen = 0;
    MessageSend(&notify_send);

    int timer_flag = 0;
    TimerInit(1000);
    TimerRegisterCallback(_HostTestTimerCallback, (void*)(&timer_flag), 5000);

    while (MessageReceive(&notify_recv) < 0);
    TimerDone();

    if (timer_flag > 2) return;

    if (notify_recv.cmd == MSG_NTF_UART_HOST_CONDITION && *((int*)(notify_recv.body)) == 1) {
        printf("Host Test Result : Host Pass\n");
        return;
    }
    MessageDone();
    printf("Host Test Result : Host Failed\n");
#endif
}

static char extended_test_data[1024] = {0};

static void _ExtenedTestInit(void)
{
    printf(LOG_TAG"Extended test init.\n");
}

static void _ExtenedTestDone(void)
{
    memset(extended_test_data, 0, sizeof(extended_test_data));

    printf(LOG_TAG"Extended test done.\n\n\n");
}

#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0

static void _TestGetBkMac(void)
{
    printf(LOG_TAG"Extended Test: GET_BKMAC\n");
    UartSendCommand(UCI_NOTIFY_TX_GET_BKMAC, 0);
    int times = 1000*500;
    int tmp_id = 0;
    unsigned char bkMac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    while (times--) {
        if (UartGetPacket() == 0) {
            tmp_id = UartGetCommandID();
            if ((tmp_id == UCI_NOTIFY_TX_GET_BKMAC)) {
                memcpy(bkMac, UartGetCommandData(), sizeof(bkMac));

                printf("GET_BKMAC Test Result: Pass %02x:%02x:%02x:%02x:%02x:%02x\n", \
                    bkMac[0],bkMac[1],bkMac[2],bkMac[3],bkMac[4],bkMac[5]);
                return ;
            }
        }
        else {
            udelay(2);
        }
    }

    printf("GET_BKMAC Test Result: GET_BKMAC Failed\n");
}

static void _TestSetMac(char *cmd_data)
{
    unsigned char i, mac[6] = {0};
    char *s = NULL;

    printf(LOG_TAG"Extended Test: MAC\n");
    s = cmd_data;
    // Set mac cmd format: MAC:AA:BB:CC:DD:EE:FF
    for (i = 0; i < sizeof(mac); i++) {
        s = strstr(s, ":");
        if (s != NULL) {
            s++;
            mac[i] = (unsigned char)htoi(s);
        }
    }

    UartSendCommandWithData(UCI_NOTIFY_TX_SET_MAC, mac, sizeof(mac));
    int times = 1000*500;
    int tmp_id = 0;
    int tmp_state = 0;
    while (times--) {
        if (UartGetPacket() == 0) {
            tmp_id = UartGetCommandID();
            tmp_state = UartGetCommandState();
            if ((tmp_id == UCI_NOTIFY_TX_SET_MAC) && (tmp_state == 1)) {
                printf("MAC Test Result: Pass\n");
                printf("MAC Test Result: Finish Pass\n");
#ifdef CONFIG_BOARD_GPIO_RESET_BT
                // Reset BT
                mdelay(20);
                GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_LOW);
                mdelay(100);
                GpioSetLevel(CONFIG_BOARD_GPIO_RESET_BT, GPIO_LEVEL_HIGH);
                // 注意：有些板子bk3266软件在启动后会复位8008
#endif
                return;
            }
        }
        else {
            udelay(2);
        }
    }

    printf("MAC Test Result: MAC Failed\n");
}

static void _TestSetFreq(char *cmd_data)
{
    unsigned char ff[2] = {0};
    unsigned short freq = 0;
    char *s = NULL;

    printf(LOG_TAG"Extended Test: TX_FREQ\n");
    // Set frequency cmd format: TX_FREQ:894
    s = strstr(cmd_data, ":");
    if (s != NULL) {
        freq = (unsigned short)atoi(s+1);
        ff[0] = (freq >> 8) & 0xFF;
        ff[1] = freq & 0xFF;
        UartSendCommandWithData(UCI_NOTIFY_TX_SET_FREQ, ff, sizeof(ff));
        int times = 1000*500;
        int tmp_id = 0;
        int tmp_state = 0;
        while (times--) {
            if (UartGetPacket() == 0) {
                tmp_id = UartGetCommandID();
                tmp_state = UartGetCommandState();
                if ((tmp_id == UCI_NOTIFY_TX_SET_FREQ) && (tmp_state == 1)) {
                    printf("TX_FREQ Test Result: TX_FREQ Pass\n");
                    return;
                }
            }
            else {
                udelay(2);
            }
        }
    }
    printf("TX_FREQ Test Result: TX_FREQ Failed\n");
}
#endif

static void _ExtenedTestFunction(void)
{
    char *cmd = NULL;

    printf(LOG_TAG"Extended test data: %s\n", extended_test_data);

    cmd = strstr(extended_test_data, "TX_FREQ");
    if (cmd != NULL) {
#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
        _TestSetFreq(cmd);
#endif
    }

    cmd = strstr(extended_test_data, "MAC");
    if (cmd != NULL) {
#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
        _TestSetMac(cmd);
#endif
    }

    cmd = strstr(extended_test_data, "GET_BKMA");
    if (cmd != NULL) {
#ifdef CONFIG_VSP_UART_MESSAGE_ENABLE_V1_0
        _TestGetBkMac();
#endif
    }
}

//-------------------------------------------------------------------------------------------------

static VSP_FACTORY_TEST_SUITE g_factory_test_suite[] = {
    {"Test Led",         '1', _TestLedInit,       _TestLedDone,       _TestLedFunction},
    {"Test Mic And Ref", '2', _TestMicAndRefInit, _TestMicAndRefDone, _TestMicAndRefFunction},
    {"Test Speaker",     '3', _TestSpeakerInit,   _TestSpeakerDone,   _TestSpeakerFunction},
    {"Test Uart",        '4', _TestUartInit,      _TestUartDone,      _TestUartFunction},
    {"Test SN",          '5', _TestSnInit,        _TestSnDone,        _TestSnFunction},
    {"Test OTP",         '6', _TestOTPInit,       _TestOTPDone,       _TestOTPFunction},
    {"Test Finish",      '7', _TestFinishInit,    _TestFinishDone,    _TestFinishFunction},
    {"Test Host",        '8', _TestHostInit,      _TestHostDone,      _TestHostFunction},

    {"Cmd Read OEM",    'a', _ReadOemInit,   _ReadOemDone,   _ReadOemFunction},
    {"Cmd Write OEM",   'b', _WriteOemInit,  _WriteOemDone,  _WriteOemFunction},
    {"Cmd Read User",   'c', _ReadUserInit,  _ReadUserDone,  _ReadUserFunction},
    {"Cmd Write User",  'd', _WriteUserInit, _WriteUserDone, _WriteUserFunction},

    {"Extended test",   'x', _ExtenedTestInit, _ExtenedTestDone, _ExtenedTestFunction},
};

static int _FactoryMenu(void)
{
    int items_num = sizeof(g_factory_test_suite) / sizeof(VSP_FACTORY_TEST_SUITE);
    printf (LOG_TAG"------- Factory Menu Start -------\n");
    printf (LOG_TAG" Have %d Test Items\n", items_num);
    for (int i = 0; i < items_num; i++) {
        printf (LOG_TAG" %c %s\n", g_factory_test_suite[i].tag, g_factory_test_suite[i].name);
    }
    printf (LOG_TAG"------- Factory Menu End -------\n\n\n");
    return 0;
}

static int _FactoryFunction(unsigned char c)
{
    for (int i = 0; i < sizeof(g_factory_test_suite) / sizeof(VSP_FACTORY_TEST_SUITE); i++) {
        if (c == g_factory_test_suite[i].tag) {
            g_factory_test_suite[i].init();
            g_factory_test_suite[i].function();
            g_factory_test_suite[i].done();
            _FactoryMenu();
            return 0;
        }
    }

    printf (LOG_TAG"Input Error Cmd\n\n\n");
    _FactoryMenu();
    return 0;
}

//=================================================================================================
#ifdef CONFIG_MCU_ENABLE_CPU
static char s_cmd = 0;
#endif
static void _FactoryUartRecvCmd(void)
{
    char c = 0;
#ifdef CONFIG_MCU_ENABLE_UART_PRINTF
    if (0 == UartTryRecvByte(UART_PORT1, (unsigned char *)(&c))) {
        printf (LOG_TAG" Get Input: %c\n", c);
        _FactoryFunction(c);
    }
#else
    if (0 != GsRead(&c,1)) {
        if (c == 'x') {
            // Extended test
            GsRead(extended_test_data, sizeof(extended_test_data));
        }
        printf (LOG_TAG" Get Input: %c\n", c);
        _FactoryFunction(c);
    }
#endif

#ifdef CONFIG_MCU_ENABLE_CPU
    if (s_cmd != 0) {
        printf (LOG_TAG" Get Input: %c\n", s_cmd);
        _FactoryFunction(s_cmd);
        s_cmd = 0;
    }
#endif

}

//=================================================================================================

static const UAC2_DEVICE_DESCRIPTION udc_desc = {
    .manufacturer_name = "NationalChip",
    .product_name = "NationalChip ACM Device",
    .serial_number = "88156088",
};

#define AUDIO_OUT_BUFFER_SIZE (1024)
static char s_audio_out_buffer[AUDIO_OUT_BUFFER_SIZE+256] __attribute__((aligned(8)));
static int _FactoryModeInit(VSP_MODE_TYPE prev_mode)
{
    printf(LOG_TAG"Init Factory mode\n");

    UsbCompositeInit(NULL, NULL, &udc_desc, NULL, NULL);

#ifndef CONFIG_MCU_ENABLE_CPU
    // Load VPA
    if (VspLoadVpa(VPA_FLASH_OFFSET, s_audio_out_buffer, AUDIO_OUT_BUFFER_SIZE)) {
        printf(LOG_TAG"LoadDSP Failed !\n");
        return -1;
    }
    if (VspInitSRAMBuffer()) {
#else
    if (VspInitDDRBuffer()) {
#endif
        printf(LOG_TAG"Failed to initialize Buffer!\n");
        return -1;
    }

    VspInitCmdDDRBuffer();

    mdelay(50);
    _FactoryMenu();
    return 0;
}

static void _FactoryModeDone(VSP_MODE_TYPE next_mode) // when this be called, must make sure every test task was done.
{
    UsbCompositeDone();

    VspCleanupVpa();
    printf(LOG_TAG"Exit FACTORY mode\n");
}

static void _FactoryModeTick(void)
{
    _FactoryUartRecvCmd();
}

static int _FactoryModeProcSetParam(volatile VSP_MSG_CPU_MCU *request)
{
    VSP_PARAM *vsp_param = (VSP_PARAM *)DEV_TO_MCU(request->set_param.param_addr);
    unsigned int vsp_param_size = request->set_param.param_size;
    switch (vsp_param->type) {
        case VSP_PARAM_TYPE_CMD:
#ifdef CONFIG_MCU_ENABLE_CPU
            s_cmd = (char)(vsp_param->param.cmd.buff_addr);

            VSP_MSG_MCU_CPU message;
            message.header.type = VMT_MC_SET_PARAM_DONE;
            message.header.size = sizeof(VMB_MC_SET_PARAM_DONE) / sizeof(unsigned int);
            message.header.magic = request->header.magic;
            message.header.param = request->header.param;
            message.set_param_done.param_addr = NULL;
            message.set_param_done.param_size = 0;
            CpuPostVspMessage(&message.header.value, message.header.size + 1);
#endif
            break;
        default:
            break;
    }

    return 0;
}

static int _FactoryModeProc(volatile VSP_MSG_CPU_MCU *request)
{
    switch (request->header.type) {
        case VMT_CM_SET_PARAM:
            return _FactoryModeProcSetParam(request);
        default:
            return -1;
    }
}

//-------------------------------------------------------------------------------------------------

const VSP_MODE_INFO vsp_factory_mode_info = {
    .type = VSP_MODE_FACTORY,
    .init = _FactoryModeInit,
    .done = _FactoryModeDone,
    .proc = _FactoryModeProc,
    .tick = _FactoryModeTick,
};
