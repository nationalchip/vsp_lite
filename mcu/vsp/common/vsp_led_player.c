/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * vsp_led_player.h: VSP LED Player
 *
 */

#include <autoconf.h>

#include <stdio.h>

#include <driver/timer.h>
#include <driver/misc.h>

#include "vsp_led_player.h"

#ifdef CONFIG_VSP_LED_NUM

typedef struct {
    VSP_LED_FRAME_HEADER    header;
    VSP_LED_PIXEL           pixel[];
} VSP_LED_FRAME;

static struct {
    const VSP_LED_TASK     *task;           // current task
    unsigned int            started;
    // for mode SLAVE
    VSP_LED_QUEUE          *cpu_queue;      // the queue from CPU
    void                   *cpu_frame;      // the frame from CPU
    // for mode MASTER
    VSP_LED_QUEUE           mcu_queue;
    // working settings
    volatile VSP_LED_QUEUE *playing_queue;
    volatile void          *playing_frame;
    unsigned int            origin;
    unsigned int            played_count;
    unsigned int            notified;
    // frame states
    int                     current_time;       // current time
    int                     current_frame_duration;
    int                     current_setup_duration;
    int                     current_setup_distance[CONFIG_VSP_LED_NUM][4];
    VSP_LED_PIXEL           current_setup_init_pixel[CONFIG_VSP_LED_NUM];
    VSP_LED_PIXEL           current_frame_done_pixel[CONFIG_VSP_LED_NUM];
} s_player_state = {0};

static volatile VSP_LED_PIXEL s_led_pixels[CONFIG_VSP_LED_NUM] = {{0}};

//=================================================================================================

static void _LedPlayerSetCurrentFrame(unsigned int index)
{
    VSP_LED_FRAME *current_frame = (VSP_LED_FRAME *)((unsigned int)s_player_state.playing_frame + (sizeof(VSP_LED_FRAME_HEADER) + s_player_state.playing_queue->pixel_num * sizeof(VSP_LED_PIXEL)) * index);
    s_player_state.current_setup_duration = current_frame->header.bits.setup;
    s_player_state.current_frame_duration = current_frame->header.bits.hold + s_player_state.current_setup_duration;
    
    // Clear Pixel Buffer;
    for (int i = 0; i < CONFIG_VSP_LED_NUM; i++) {
        s_player_state.current_frame_done_pixel[i].value = 0;
        s_player_state.current_setup_init_pixel[i].value = s_led_pixels[i].value;
    }
    
    // Fetch Next Frame Pixels
    int valid_pixel_num = CONFIG_VSP_LED_NUM < s_player_state.playing_queue->pixel_num ? CONFIG_VSP_LED_NUM : s_player_state.playing_queue->pixel_num;
    for (int i = 0; i < valid_pixel_num; i++)
        s_player_state.current_frame_done_pixel[(i + s_player_state.origin) % CONFIG_VSP_LED_NUM].value = current_frame->pixel[i].value;
    
    // Calulate the Distance between tar
    for (int i = 0; i < CONFIG_VSP_LED_NUM; i++) {
        s_player_state.current_setup_distance[i][0] = s_player_state.current_frame_done_pixel[i].bits.r - s_player_state.current_setup_init_pixel[i].bits.r;
        s_player_state.current_setup_distance[i][1] = s_player_state.current_frame_done_pixel[i].bits.g - s_player_state.current_setup_init_pixel[i].bits.g;
        s_player_state.current_setup_distance[i][2] = s_player_state.current_frame_done_pixel[i].bits.b - s_player_state.current_setup_init_pixel[i].bits.b;
        s_player_state.current_setup_distance[i][3] = s_player_state.current_frame_done_pixel[i].bits.a - s_player_state.current_setup_init_pixel[i].bits.a;
    }
    
    s_player_state.notified = 0;
    s_player_state.current_time = 0;
}

static int _LedTimerCallback(void *data)
{
    int current_time = s_player_state.current_time + 1000 / CONFIG_VSP_LED_REFRESH_FREQUENCE;
    int valid_pixel_num = CONFIG_VSP_LED_NUM < s_player_state.playing_queue->pixel_num ?
                          CONFIG_VSP_LED_NUM : s_player_state.playing_queue->pixel_num;
    
    if (current_time <= s_player_state.current_frame_duration) {
        if (current_time < s_player_state.current_setup_duration) {
            // in setup phase
            for (int i = 0; i < CONFIG_VSP_LED_NUM; i++) {
                s_led_pixels[i].bits.r = s_player_state.current_setup_init_pixel[i].bits.r + s_player_state.current_setup_distance[i][0] * current_time / s_player_state.current_setup_duration;
                s_led_pixels[i].bits.g = s_player_state.current_setup_init_pixel[i].bits.g + s_player_state.current_setup_distance[i][1] * current_time / s_player_state.current_setup_duration;
                s_led_pixels[i].bits.b = s_player_state.current_setup_init_pixel[i].bits.b + s_player_state.current_setup_distance[i][2] * current_time / s_player_state.current_setup_duration;
                s_led_pixels[i].bits.a = s_player_state.current_setup_init_pixel[i].bits.a + s_player_state.current_setup_distance[i][3] * current_time / s_player_state.current_setup_duration;
            }
        }
        else {
            // in hold phase
            for (int i = 0; i < valid_pixel_num; i++) {
                s_led_pixels[i].value = s_player_state.current_frame_done_pixel[i].value;
            }
        }
        s_player_state.current_time = current_time;
    }
    else {
        for (int i = 0; i < valid_pixel_num; i++) {
            s_led_pixels[i].value = s_player_state.current_frame_done_pixel[i].value;
        }
        // notify client a frame is done
        if (s_player_state.task->frame_done && !s_player_state.notified) {
            s_player_state.task->frame_done(s_player_state.playing_queue->read, s_player_state.task->priv_data);
            s_player_state.notified = 1;
        }
        
        // fetch next frame
        if (s_player_state.task->mode == VSP_LED_MODE_MASTER) {
            // Master Mode
            unsigned int next_index = (s_player_state.playing_queue->read + 1) % s_player_state.playing_queue->frame_num;
            if (next_index == 0) {  // one loop is finished
                s_player_state.played_count ++;
                if (s_player_state.played_count == s_player_state.task->loop_times && s_player_state.task->loop_times) {
                    if (s_player_state.task->task_done)
                        s_player_state.task->task_done(s_player_state.task->priv_data);

                    VspLedStop();
                }
            }
            _LedPlayerSetCurrentFrame(next_index);
            s_player_state.playing_queue->read = next_index;
        }
        else {
            // Slave Mode
            if (s_player_state.playing_queue->read == s_player_state.playing_queue->write) {
                if (s_player_state.task->task_done)
                    s_player_state.task->task_done(s_player_state.task->priv_data);
            }
            else {
                _LedPlayerSetCurrentFrame(s_player_state.playing_queue->read);
                if (s_player_state.playing_queue->read != s_player_state.playing_queue->write)
                    s_player_state.playing_queue->read = (s_player_state.playing_queue->read + 1) % s_player_state.playing_queue->frame_num;
            }
        }
    }

    return 0;
}

//=================================================================================================

int VspLedSetCpuQueue(VSP_LED_QUEUE *queue, void *frame)
{
    if (queue && frame && queue->version == VSP_LED_VERSION) {
        s_player_state.cpu_queue = queue;
        s_player_state.cpu_frame = frame;
        return 0;
    }
    return -1;
}

int VspLedPlay(const VSP_LED_TASK *task)
{
    s_player_state.task = 0;
    s_player_state.started = 0;
    if (task->mode == VSP_LED_MODE_SLAVE) {
        if(s_player_state.cpu_queue && s_player_state.cpu_frame) {
            s_player_state.task = task;
            s_player_state.played_count = 0;
            s_player_state.origin = task->origin;
            
            s_player_state.playing_queue = s_player_state.cpu_queue;
            s_player_state.playing_frame = s_player_state.cpu_frame;
            
            // printf("LED QUEUE: %d %d \n", s_player_state.playing_queue->frame_num, s_player_state.playing_queue->pixel_num);
            
            if (s_player_state.playing_queue->read != s_player_state.playing_queue->write)
                _LedPlayerSetCurrentFrame(s_player_state.playing_queue->read);
            else {
                // Let current_time > current_frame_duration
                s_player_state.current_time = 1000;
                s_player_state.current_frame_duration = 10;
            }
        }
        else {
            return -1;
        }
    }
    else if (task->mode == VSP_LED_MODE_MASTER) {
        // initialize the mcu queue
        s_player_state.task = task;
        s_player_state.mcu_queue.frame_num = task->frame_num;
        s_player_state.mcu_queue.pixel_num = task->pixel_num;
        s_player_state.mcu_queue.read = 0;
        s_player_state.played_count = 0;
        s_player_state.origin = task->origin;

        s_player_state.playing_queue = &s_player_state.mcu_queue;
        s_player_state.playing_frame = task->frames;

        _LedPlayerSetCurrentFrame(s_player_state.mcu_queue.read);
    }
    else {
        return -1;
    }

    return TimerRegisterCallback(_LedTimerCallback, &s_player_state, 1000 / CONFIG_VSP_LED_REFRESH_FREQUENCE);
}

int VspLedStop(void)
{
    TimerUnregisterCallback(_LedTimerCallback);
    s_player_state.task = 0;
    return 0;
}

int VspLedClear(void)
{
    for (int i = 0; i < CONFIG_VSP_LED_NUM; i++) {
        s_led_pixels[i].value = 0;
    }
    return 0;
}

int VspLedFlush(void)
{
    if (s_player_state.task && !s_player_state.started) {
        LedInit();
        s_player_state.started = 1;
    }

    for (int i = 0; i < CONFIG_VSP_LED_NUM; i++) {
        LED_PIXEL pixel;
        pixel.bits.r = s_led_pixels[i].bits.r;
        pixel.bits.g = s_led_pixels[i].bits.g;
        pixel.bits.b = s_led_pixels[i].bits.b;
        pixel.bits.a = s_led_pixels[i].bits.a;
        LedSetPixel(i, pixel);
    }

    LedFlush();

    if (!s_player_state.task && s_player_state.started) {
        LedDone();
        s_player_state.started = 0;
    }

    return 0;
}

//=================================================================================================

#else

int VspLedSetCpuQueue(VSP_LED_QUEUE *queue, void *frame){return 0;}
int VspLedIsQueueSet(void){return 0;}

int VspLedPlay(const VSP_LED_TASK *task){return 0;}
int VspLedStop(void){return 0;}

int VspLedClear(void){return 0;}

int VspLedFlush(void){return 0;}

#endif
