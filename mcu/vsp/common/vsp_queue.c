/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 *
 * vsp_queue.c: a Circular Queue using array
 *
 */

#include "vsp_queue.h"

void VspQueueInit(VSP_QUEUE *queue, unsigned char *buffer, int size, int member_size)
{
    queue->buffer = buffer;
    queue->size = size - size % member_size;
    queue->member_size = member_size;
    queue->tail = 0;
    queue->head = 0;
}

int VspQueuePut(VSP_QUEUE *queue, const unsigned char *value)
{
    if (((queue->tail + queue->member_size) % queue->size) == queue->head)
        return 0;

    for (int i = 0; i < queue->member_size; i++) {
        queue->buffer[(queue->tail + i) % queue->size] = value[i];
    }
    queue->tail = (queue->tail + queue->member_size) % queue->size;
    return 1;
}

int VspQueueGet(VSP_QUEUE *queue, unsigned char *value)
{
    if (queue->head == queue->tail)
        return 0;

    for (int i = 0; i < queue->member_size; i++) {
        value[i] = queue->buffer[(queue->head + i) % queue->size];
    }
    queue->head = (queue->head + queue->member_size) % queue->size;
    return 1;
}

int VspQueueGetCapacity(VSP_QUEUE *queue)
{
    return queue->size / queue->member_size;
}

int VspQueueIsEmpty(VSP_QUEUE *queue)
{
    return (queue->head == queue->tail);
}

int VspQueueIsFull(VSP_QUEUE *queue)
{
    return (((queue->tail + queue->member_size) % queue->size) == queue->head);
}

#ifdef __VSP_QUEUE_SUPPORT_BUFFER__
int VspQueuePutBuffer(VSP_QUEUE *queue, const unsigned int *buffer, const int count)
{
    if (VspQueueGetFreeSize(queue) < count * queue->member_size)
        return 0;

    int frontFreeSize = queue->size - queue->tail;
    if (frontFreeSize > count * queue->member_size) {
        for (int i = 0; i < count * queue->member_size; i++)
            queue->buffer[queue->tail + i] = buffer[i];
        queue->tail += count * queue->member_size;
    } else {
        for (int i = 0; i < frontFreeSize; i++)
            queue->buffer[queue->tail + i] = buffer[i];
        for (int i = 0; i < count * queue->member_size - frontFreeSize; i++)
            queue->buffer[i] = buffer[frontFreeSize + i];
        queue->tail = count * queue->member_size - frontFreeSize;
    }
    return count * queue->member_size;
}

int VspQueueGetBuffer(VSP_QUEUE *queue, unsigned int *buffer, int count)
{
    if (VspQueueGetDataSize(queue) < count * queue->member_size)
        return 0;

    int frontDataSize = queue->size - queue->head;
    if (frontDataSize > count * queue->member_size) {
        for (int i = 0; i < count * queue->member_size; i++)
            buffer[i] = queue->buffer[queue->head + i];
        queue->head += count * queue->member_size;
    } else {
        for (int i = 0; i < frontDataSize; i++)
            buffer[i] = queue->buffer[queue->head + i];
        for (int i = 0; i < count * queue->member_size - frontDataSize; i++)
            buffer[frontDataSize + i] = queue->buffer[i];
        queue->head = count * queue->member_size - frontDataSize;
    }
    return count * queue->member_size;
}

int VspQueuePeekBuffer(VSP_QUEUE *queue, unsigned int *buffer, const int size)
{
    if (VspQueueGetDataSize(queue) < size)
        return 0;

    int frontDataSize = queue->size - queue->head;
    if (frontDataSize > size) {
        for (int i = 0; i < size; i++)
            buffer[i] = queue->buffer[queue->head + i];
    } else {
        for (int i = 0; i < frontDataSize; i++)
            buffer[i] = queue->buffer[queue->head + i];
        for (int i = 0; i < size - frontDataSize; i++)
            buffer[frontDataSize + i] = queue->buffer[i];
    }
    return size;
}

int VspQueueGetFreeSize(VSP_QUEUE *queue)
{
    return (queue->head > queue->tail) ? (queue->head - queue->tail - 1)
    : (queue->size - queue->tail + queue->head - 1);
}

int VspQueueGetDataSize(VSP_QUEUE *queue)
{
    return (queue->tail >= queue->head) ? (queue->tail - queue->head)
    : (queue->size - queue->head + queue->tail);
}

#endif
