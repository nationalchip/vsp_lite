/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * uart_message_v2.c: MCU uart communication protocol
 *
 */

#include <string.h>
#include <common.h>
#include <errno.h>
#include <misc_regs.h>

#include <driver/uart.h>
#include <driver/delay.h>
#include "uart_message_v2.h"
#include "vsp_queue.h"

#define LOG_TAG "[MSG2.0]"

#define BSWAP_32(x) \
    (uint32_t)((((uint32_t)(x) & 0xff000000) >> 24) | \
              (((uint32_t)(x) & 0x00ff0000) >> 8) | \
              (((uint32_t)(x) & 0x0000ff00) << 8) | \
              (((uint32_t)(x) & 0x000000ff) << 24) \
             )

#define BSWAP_16(x) \
    ((short)( \
        (((short)(x) & (short)0x00ffU) << 8 ) | \
        (((short)(x) & (short)0xff00U) >> 8 ) ))

#define UART_RECV_QUENE_LEN 8
#define UART_SEND_QUENE_LEN 8
#define UART_MSG_REGIST_MAXNUM 32

//=================================================================================================
static int _UartMessageAsyncFindMagicCallback(int port, void* priv);
static int _UartMessageAsyncRecvHeadDoneCallback(int port, void* priv);
static int _UartMessageAsyncSendDoneCallback(int port, void *priv);

typedef struct {
    unsigned int    magic;
    unsigned int    tmp_magic; // Use for check magic
    unsigned int    port;
    int             init_flag;
    unsigned int    baudrate;
    unsigned char   send_sequence;
    unsigned char   recv_sequence;
    unsigned int    send_ing;
    MSG_PACK        cur_recv_pack;
    MSG_PACK        cur_send_pack;
    unsigned char   uart_send_pack_queue_buffer[UART_SEND_QUENE_LEN * sizeof(MSG_PACK)];
    VSP_QUEUE       uart_send_pack_queue;
} MESSAGE_HANDLE;

static MESSAGE_HANDLE s_msg_handle[2];

static UART_MSG_REGIST s_uart_msg_regist_array[UART_MSG_REGIST_MAXNUM];

static unsigned char s_uart_recv_pack_queue_buffer[UART_RECV_QUENE_LEN * sizeof(MSG_PACK)] = {0};
VSP_QUEUE s_uart_recv_pack_queue;

static MESSAGE_HANDLE *_GetMessageHandle(unsigned char port)
{
    if (port == UART_PORT0)
        return (MESSAGE_HANDLE *)&s_msg_handle[0];
    else if (port == UART_PORT1)
        return (MESSAGE_HANDLE *)&s_msg_handle[1];
    else
        return NULL;
}

static int _CheckCrc(unsigned char *data, int len, unsigned int crc_stand)
{
    return crc_stand == crc32(0, data, len);
}

int UartMessageAsyncRegist(UART_MSG_REGIST *uart_msg_regist)
{
    for (int i = 0; i < sizeof(s_uart_msg_regist_array) / sizeof(UART_MSG_REGIST); i++) {
        if (uart_msg_regist->msg_id == s_uart_msg_regist_array[i].msg_id) {
            s_uart_msg_regist_array[i].msg_id = 0;
            s_uart_msg_regist_array[i].port = -1;
            s_uart_msg_regist_array[i].msg_buffer = NULL;
            s_uart_msg_regist_array[i].msg_buffer_length = 0;
            s_uart_msg_regist_array[i].msg_buffer_offset = 0;
            s_uart_msg_regist_array[i].priv = NULL;
            s_uart_msg_regist_array[i].msg_pack_callback = NULL;
        }
    }

    for (int i = 0; i < sizeof(s_uart_msg_regist_array) / sizeof(UART_MSG_REGIST); i++) {
        if (s_uart_msg_regist_array[i].msg_id == 0) {
            s_uart_msg_regist_array[i].msg_id = uart_msg_regist->msg_id;
            s_uart_msg_regist_array[i].port = uart_msg_regist->port;
            s_uart_msg_regist_array[i].msg_buffer           = uart_msg_regist->msg_buffer;
            s_uart_msg_regist_array[i].msg_buffer_length    = uart_msg_regist->msg_buffer_length;
            s_uart_msg_regist_array[i].msg_buffer_offset    = uart_msg_regist->msg_buffer_offset;
            s_uart_msg_regist_array[i].priv                 = uart_msg_regist->priv;
            s_uart_msg_regist_array[i].msg_pack_callback   = uart_msg_regist->msg_pack_callback;
            return 0;
        }
    }
    return -1;
}

int UartMessageAsyncLogout(UART_MSG_REGIST *uart_msg_regist)
{
    for (int i = 0; i < sizeof(s_uart_msg_regist_array) / sizeof(UART_MSG_REGIST); i++) {
        if (uart_msg_regist->msg_id == s_uart_msg_regist_array[i].msg_id) {
            uart_msg_regist->port         = s_uart_msg_regist_array[i].port;
            uart_msg_regist->msg_buffer         = s_uart_msg_regist_array[i].msg_buffer;
            uart_msg_regist->msg_buffer_length  = s_uart_msg_regist_array[i].msg_buffer_length;
            uart_msg_regist->msg_buffer_offset  = s_uart_msg_regist_array[i].msg_buffer_offset;
            uart_msg_regist->priv               = s_uart_msg_regist_array[i].priv;
            uart_msg_regist->msg_pack_callback = s_uart_msg_regist_array[i].msg_pack_callback;

            s_uart_msg_regist_array[i].msg_id = 0;
            s_uart_msg_regist_array[i].port = -1;
            s_uart_msg_regist_array[i].msg_buffer = NULL;
            s_uart_msg_regist_array[i].msg_buffer_length = 0;
            s_uart_msg_regist_array[i].msg_buffer_offset = 0;
            s_uart_msg_regist_array[i].priv = NULL;
            s_uart_msg_regist_array[i].msg_pack_callback = NULL;
            return 0;
        }
    }

    return -1;
}

int UartMessageAsyncTick(void)
{
    MSG_PACK uart_pack;
    if (VspQueueGet(&s_uart_recv_pack_queue, (unsigned char *)&uart_pack) == 0) return -1; //没有待处理的pack

    for (int i = 0; i < sizeof(s_uart_msg_regist_array) / sizeof(UART_MSG_REGIST); i++) {
        if (s_uart_msg_regist_array[i].msg_id == uart_pack.msg_header.cmd && s_uart_msg_regist_array[i].port == uart_pack.port) {
            return s_uart_msg_regist_array[i].msg_pack_callback(&uart_pack, s_uart_msg_regist_array[i].priv);
        }
    }

    return -1; //没有注册回调
}

static unsigned char s_uart_common_recv_buff[256];
static unsigned int s_uart_common_recv_buff_offset = 0;
static unsigned int s_uart_common_recv_buff_length = sizeof(s_uart_common_recv_buff);

static int _UartMessagePrepareRecv(MSG_PACK* pack)
{

    for (int i = 0; i < sizeof(s_uart_msg_regist_array) / sizeof(UART_MSG_REGIST); i++) {
        if (s_uart_msg_regist_array[i].msg_id == pack->msg_header.cmd) {
            if (pack->msg_header.length > s_uart_msg_regist_array[i].msg_buffer_length) break;

            if (s_uart_msg_regist_array[i].msg_buffer_offset + pack->msg_header.length > s_uart_msg_regist_array[i].msg_buffer_length) {
                s_uart_msg_regist_array[i].msg_buffer_offset = 0;
            }

            if (s_uart_msg_regist_array[i].msg_buffer == NULL) break;
            pack->body_addr = s_uart_msg_regist_array[i].msg_buffer + s_uart_msg_regist_array[i].msg_buffer_offset;
            s_uart_msg_regist_array[i].msg_buffer_offset += pack->msg_header.length;
            return 0;
        }
    }

    if (pack->msg_header.length > s_uart_common_recv_buff_length) {
        pack->body_addr = NULL;
        return 0;
    }

    if (s_uart_common_recv_buff_offset + pack->msg_header.length > s_uart_common_recv_buff_length) {
        s_uart_common_recv_buff_offset = 0;
    }
    pack->body_addr = s_uart_common_recv_buff + s_uart_common_recv_buff_offset;
    s_uart_common_recv_buff_offset += pack->msg_header.length;
    s_uart_common_recv_buff_offset %= s_uart_common_recv_buff_length;

    return 0;
}

static int _UartMessageAsyncFindMagicCallback(int port, void* priv)
{
    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(port);
    if(msg_handle == NULL) return -1;

    if (msg_handle->init_flag == 0) return -1;

    memset(((unsigned char *)&msg_handle->cur_recv_pack) + 4, 0, sizeof(msg_handle->cur_recv_pack) - 4);
    msg_handle->cur_recv_pack.port = msg_handle->port;

    unsigned int tmp = *(unsigned char *)&(msg_handle->cur_recv_pack.msg_header.magic);
    msg_handle->tmp_magic = (msg_handle->tmp_magic >> 8) | (tmp << 24);

    if (msg_handle->tmp_magic == msg_handle->magic) {
        msg_handle->cur_recv_pack.msg_header.magic = msg_handle->tmp_magic;
        UartAsyncRecvBuffer(msg_handle->port, ((unsigned char*)(&(msg_handle->cur_recv_pack.msg_header))) + 4, sizeof(MESSAGE_HEADER) - 4,
                            (UART_RECV_DONE_CALLBACK)_UartMessageAsyncRecvHeadDoneCallback, priv);
    } else {
        msg_handle->cur_recv_pack.msg_header.magic = BSWAP_32(msg_handle->tmp_magic);
        UartAsyncRecvBuffer(msg_handle->port, (unsigned char *)&(msg_handle->cur_recv_pack.msg_header.magic), 1,
                            (UART_RECV_DONE_CALLBACK)_UartMessageAsyncFindMagicCallback, priv);
    }

    return 0;
}

static int _UartMessageAsyncRecvDoneCallback(int port, void* priv)
{
    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(port);
    if(msg_handle == NULL) return -1;

    msg_handle->cur_recv_pack.len = msg_handle->cur_recv_pack.msg_header.length - (msg_handle->cur_recv_pack.msg_header.flags ? 4 : 0);

    if ((msg_handle->cur_recv_pack.msg_header.flags == 0) || \
        _CheckCrc(msg_handle->cur_recv_pack.body_addr, msg_handle->cur_recv_pack.msg_header.length - 4, \
                *(msg_handle->cur_recv_pack.body_addr + msg_handle->cur_recv_pack.msg_header.length - 1) << 24 | \
                *(msg_handle->cur_recv_pack.body_addr + msg_handle->cur_recv_pack.msg_header.length - 2) << 16 | \
                *(msg_handle->cur_recv_pack.body_addr + msg_handle->cur_recv_pack.msg_header.length - 3) << 8 | \
                *(msg_handle->cur_recv_pack.body_addr + msg_handle->cur_recv_pack.msg_header.length - 4) )) {

        VspQueuePut(&s_uart_recv_pack_queue, (unsigned char *)&msg_handle->cur_recv_pack);
    } else {;
//        printf(LOG_TAG"!!!recv body_crc32 err.\n");
    }
    memset(&msg_handle->cur_recv_pack, 0, sizeof(MSG_PACK));
    msg_handle->cur_recv_pack.port = msg_handle->port;

    UartAsyncRecvBuffer(msg_handle->port, (unsigned char *)&(msg_handle->cur_recv_pack.msg_header.magic),
                        1, (UART_RECV_DONE_CALLBACK)_UartMessageAsyncFindMagicCallback, priv);
    return 0;
}

static int _UartMessageAsyncRecvHeadDoneCallback(int port, void* priv)
{
    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(port);
    if(msg_handle == NULL) return -1;

    if ((!((msg_handle->cur_recv_pack.msg_header.cmd & 0xff00) == 0x200)) && (msg_handle->cur_recv_pack.msg_header.seq != ((msg_handle->recv_sequence + 1) % 256))) {
//        printf(LOG_TAG"!!! seq distance: %d\n", s_cur_recv_pack.msg_header.seq - s_recv_sequence);
    }
    msg_handle->recv_sequence = msg_handle->cur_recv_pack.msg_header.seq;

    if (_CheckCrc((unsigned char *)&(msg_handle->cur_recv_pack.msg_header), 10, msg_handle->cur_recv_pack.msg_header.crc32) \
        && _UartMessagePrepareRecv(&msg_handle->cur_recv_pack) == 0) {
        if ( msg_handle->cur_recv_pack.msg_header.length == 0) {
            VspQueuePut(&s_uart_recv_pack_queue, (unsigned char *)&msg_handle->cur_recv_pack);
            memset(&msg_handle->cur_recv_pack, 0, sizeof(MSG_PACK));
            msg_handle->cur_recv_pack.port = msg_handle->port;
            UartAsyncRecvBuffer(msg_handle->port, (unsigned char *)&(msg_handle->cur_recv_pack.msg_header.magic),
                                1, (UART_RECV_DONE_CALLBACK)_UartMessageAsyncFindMagicCallback, priv);
            return 0;
        }

        UartAsyncRecvBuffer(msg_handle->port, msg_handle->cur_recv_pack.body_addr, msg_handle->cur_recv_pack.msg_header.length,
                            (UART_RECV_DONE_CALLBACK)_UartMessageAsyncRecvDoneCallback, priv);
    } else {
        memset(&msg_handle->cur_recv_pack, 0, sizeof(MSG_PACK));
        msg_handle->cur_recv_pack.port = msg_handle->port;
        UartAsyncRecvBuffer(msg_handle->port, (unsigned char *)&(msg_handle->cur_recv_pack.msg_header.magic),
                        1, (UART_RECV_DONE_CALLBACK)_UartMessageAsyncFindMagicCallback, priv);
    }
    return 0;
}


static unsigned char s_uart_common_send_buff[256];
static unsigned int s_uart_common_send_buff_offset = 0;
static unsigned int s_uart_common_send_buff_length = sizeof(s_uart_common_send_buff);
static unsigned int s_uart_common_send_buff_hold[2] = {sizeof(s_uart_common_send_buff), sizeof(s_uart_common_send_buff)};

static int _UartMessageAsyncSendCheck(unsigned char port)
{
    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(port);
    if(msg_handle == NULL) return -1;

    if (VspQueueIsEmpty(&msg_handle->uart_send_pack_queue)) {
        msg_handle->send_ing = 0;
        return -1;
    }
    msg_handle->send_ing = 1;

    VspQueueGet(&msg_handle->uart_send_pack_queue, (unsigned char *)&msg_handle->cur_send_pack);

    // 发送头
    UartAsyncSendBuffer(msg_handle->port, (unsigned char *)&msg_handle->cur_send_pack.msg_header, sizeof(MESSAGE_HEADER),
                        (UART_SEND_DONE_CALLBACK) _UartMessageAsyncSendDoneCallback,
                        &msg_handle->cur_send_pack);

    return 0;
}

static int  _UartMessageAsyncSendDoneCallback(int port, void *priv)
{
    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(port);
    if(msg_handle == NULL) return -1;

    static unsigned int send_body_len[2] = {0};
    static unsigned int body_crc32 = 0;

    if (msg_handle->cur_send_pack.len) { // 如果有body,发送body
        send_body_len[port] = msg_handle->cur_send_pack.len;

        UartAsyncSendBuffer(msg_handle->port, msg_handle->cur_send_pack.body_addr, msg_handle->cur_send_pack.len,
                            (UART_SEND_DONE_CALLBACK) _UartMessageAsyncSendDoneCallback,
                            &msg_handle->cur_send_pack);
        body_crc32 = crc32(0, msg_handle->cur_send_pack.body_addr, msg_handle->cur_send_pack.len);
        msg_handle->cur_send_pack.len = 0;
        return 0;
    } else if (msg_handle->cur_send_pack.msg_header.flags) {
        UartAsyncSendBuffer(msg_handle->port, (unsigned char *)&body_crc32, 4,
                            (UART_SEND_DONE_CALLBACK) _UartMessageAsyncSendDoneCallback,
                            &msg_handle->cur_send_pack);
        msg_handle->cur_send_pack.msg_header.flags = 0;
        return 0;
    } else if ((msg_handle->cur_send_pack.body_addr >= s_uart_common_send_buff) && (msg_handle->cur_send_pack.body_addr < (s_uart_common_send_buff + s_uart_common_send_buff_length))) {
        s_uart_common_send_buff_hold[0] += send_body_len[port]; //如果发送的body是存在内部buff中的，清除占用
        send_body_len[port] = 0;
    }

    _UartMessageAsyncSendCheck(port);
    return 0;
}

int UartMessageAsyncSend(MSG_PACK *pack)
{
    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(pack->port);
    if(msg_handle == NULL) return -1;

    if (msg_handle->init_flag == 0) return -1;

    if (pack->len && pack->body_addr == NULL) return -1;

    if (VspQueueIsFull(&msg_handle->uart_send_pack_queue)) return -1;

    // 小数据
    if ((pack->len != 0) && (pack->len <= 32) \
        && (((((s_uart_common_send_buff_offset + pack->len) > s_uart_common_send_buff_length) ? \
            pack->len : (s_uart_common_send_buff_offset + pack->len)) - 1 \
            ) < (s_uart_common_send_buff_hold[0] % s_uart_common_send_buff_length) \
           )) { //common buffer未占用的空间足够

        if ((s_uart_common_send_buff_offset + pack->len) > s_uart_common_send_buff_length)
            s_uart_common_send_buff_offset = 0;

        for (int i = 0; i < pack->len; i++)
            s_uart_common_send_buff[s_uart_common_send_buff_offset + i] = pack->body_addr[i];

        pack->body_addr = s_uart_common_send_buff + s_uart_common_send_buff_offset;
        s_uart_common_send_buff_offset += pack->len;
        s_uart_common_send_buff_hold[1] = s_uart_common_send_buff_offset;
    } // 将body中的小数据暂存到内部buff

    pack->msg_header.length = pack->len + (pack->msg_header.flags ? 4 : 0);

    if (pack->msg_header.magic == 0) pack->msg_header.magic = msg_handle->magic;

    if (!((pack->msg_header.cmd & 0xff00) == 0x200)) pack->msg_header.seq = msg_handle->send_sequence++ % 256;

    pack->msg_header.crc32 = crc32(0, (unsigned char *)(&(pack->msg_header)), 10);

    VspQueuePut(&msg_handle->uart_send_pack_queue, (unsigned char *)pack);
    if (!msg_handle->send_ing) _UartMessageAsyncSendCheck(msg_handle->port);

    return 0;
}

int UartMessageAsyncInit(UART_MSG_INIT_CONFIG *config)
{
    if (config == NULL) return -1;

    MESSAGE_HANDLE *msg_handle = _GetMessageHandle(config->port);
    if(msg_handle == NULL) return -1;

    msg_handle->port = config->port;

    if (msg_handle->init_flag && (config->reinit_flag == 0)) return -1; // 重新初始化需要特殊标注

    if (config->magic != 0) {
        msg_handle->magic = config->magic;
    } else {
        msg_handle->magic = MSG_HOST_MAGIC;
    }

    msg_handle->baudrate = config->baudrate;

    if (UartInit(msg_handle->port, msg_handle->baudrate)) return -1;

    // VspQueueInit can not call again.
    if (s_msg_handle[0].init_flag == 0 && s_msg_handle[1].init_flag == 0) {
        VspQueueInit(&s_uart_recv_pack_queue, s_uart_recv_pack_queue_buffer, UART_RECV_QUENE_LEN * sizeof(MSG_PACK), sizeof(MSG_PACK));
    }

    if (msg_handle->init_flag == 0) {
        VspQueueInit(&msg_handle->uart_send_pack_queue, msg_handle->uart_send_pack_queue_buffer, UART_SEND_QUENE_LEN * sizeof(MSG_PACK), sizeof(MSG_PACK));
    }

    // 8008`s UartAsyncRecvBuffer need to be call again when reinit, but 8008c`s need not and can not call again.
#ifdef CONFIG_GX8008B
    if (msg_handle->init_flag == 1) return 0;
#endif
    UartAsyncRecvBuffer(msg_handle->port, (unsigned char *)&(msg_handle->cur_recv_pack.msg_header.magic),
                        1, (UART_RECV_DONE_CALLBACK)_UartMessageAsyncFindMagicCallback, NULL);

    msg_handle->init_flag = 1;
    return 0;
}

int UartMessageAsyncDone(void)
{
    s_msg_handle[0].init_flag = 0;
    s_msg_handle[1].init_flag = 0;

    memset(&s_uart_msg_regist_array, 0, sizeof(s_uart_msg_regist_array));
    memset(&s_uart_recv_pack_queue, 0, sizeof(s_uart_recv_pack_queue));
    memset(&s_msg_handle, 0, sizeof(s_msg_handle));
    return 0;
}


