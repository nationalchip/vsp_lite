/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_vpa.h: API for Voice Process Algorithm
 *
 */

#ifndef __VSP_VPA_H__
#define __VSP_VPA_H__

#include <driver/dsp.h>

#include <vsp_context.h>

int VspInitializeVpa(VSP_CONTEXT_HEADER *ctx_header, DSP_VSP_CALLBACK callback, void *priv);
int VspCleanupVpa(void);
int VspSuspendVpa(void);
int VspResumeVpa(void);
int VspLoadVpaConfigFromFlash(unsigned int config_offset, unsigned char *tmp_buffer);

int VspLoadVpa(unsigned int offset, void *dma_buffer, unsigned int dma_size);

#endif /* __VSP_VPA_H__ */
