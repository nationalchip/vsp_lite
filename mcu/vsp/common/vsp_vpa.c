/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_vpa.c: API for Voice Process Algorithm
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <base_addr.h>
#include <soc_config.h>
#include <misc_regs.h>

#include <driver/snpu.h>
#include <driver/dsp.h>
#include <driver/spi_flash.h>
#include <driver/delay.h>
#include <driver/misc.h>
#include <driver/otp.h>
#include <driver/irq.h>
#include <driver/uart.h>

#include <vsp_param.h>
#include <vsp_firmware.h>
#include <vsp_message.h>

#include "vsp_vpa.h"

#define LOG_TAG "[VPA]"

#define DSP_CALLBACK_TIMEOUT (1000)

static DSP_VSP_CALLBACK s_dsp_callback = NULL;

//=================================================================================================

#ifdef CONFIG_VSP_VPA_NEED_SNPU
static int _SnpuCallback(int module_id, SNPU_STATE state, void *priv)
{
    VSP_MSG_MCU_DSP response;
    response.header.type  = VMT_MD_RUN_SNPU_TASK_DONE;
    response.header.magic = 0;
    response.header.param = 0;
    response.header.size  = sizeof(response.run_snpu_task_done) / sizeof(unsigned int);
    response.run_snpu_task_done.module_id    = module_id;
    response.run_snpu_task_done.state        = state;
    response.run_snpu_task_done.private_data = priv;
    return DspPostVspMessage((unsigned int *)&response, response.header.size + 1);
}
#endif

//=================================================================================================

static int _DspDefaultCallback(volatile VSP_MSG_DSP_MCU *request, void *priv)
{
    if (request->header.type == VMT_DM_RUN_SNPU_TASK) {
#ifdef CONFIG_VSP_VPA_NEED_SNPU
        SNPU_TASK run_snpu_task;
        run_snpu_task.module_id = request->run_snpu_task.module_id;
        run_snpu_task.ops       = request->run_snpu_task.ops;
        run_snpu_task.data      = request->run_snpu_task.data;
        run_snpu_task.input     = request->run_snpu_task.input;
        run_snpu_task.output    = request->run_snpu_task.output;
        run_snpu_task.cmd       = request->run_snpu_task.cmd;
        run_snpu_task.tmp_mem   = request->run_snpu_task.tmp_mem;
        return SnpuRunTask(&run_snpu_task, _SnpuCallback, request->run_snpu_task.priv_data);
#else
        return 0;
#endif
    }
#ifdef CONFIG_DSP_ENABLE_HOST_PRINTF
    else if (request->header.type == VMT_DM_LOG) {
        printf(LOG_TAG"%s", DEV_TO_MCU(request->log.addr));
        return 0;
    }
#endif
#ifdef CONFIG_BOARD_HAS_TPM
    else if (request->header.type == VMT_DM_REQUEST_AUTHORIZE_KEY) {
        void *digest = (void *)DEV_TO_MCU(request->request_authorize_key.input_buffer);
        void *cipher = (void *)DEV_TO_MCU(request->request_authorize_key.output_buffer);
        if (0 == TPMGetAuthorizeKey(digest, cipher)) {
            VSP_MSG_MCU_DSP message;
            message.header.type = VMT_MD_REQUEST_AUTHORIZE_KEY_DONE;
            DspPostVspMessage(&message.header.value, message.header.size + 1);
        }
    }
#endif

    if (s_dsp_callback)
        return s_dsp_callback(request, priv);
    return 0;
}

//=================================================================================================

#define VPA_OTP_KEY_BUFFER_LENGTH 56
static unsigned char g_otp_key_buffer[VPA_OTP_KEY_BUFFER_LENGTH];
static VSP_PARAM g_vsp_param;
static void _VspPostOtpKey(void)
{
    OtpGetUserData(0, (char *)g_otp_key_buffer, sizeof(g_otp_key_buffer));

    g_vsp_param.version = 0;
    g_vsp_param.type    = VSP_PARAM_TYPE_OTP_KEY;
    g_vsp_param.param.key.buff_addr = (unsigned int)MCU_TO_DEV(g_otp_key_buffer);
    g_vsp_param.param.key.buff_size = sizeof(g_otp_key_buffer);

    // Send a message to DSP
    VSP_MSG_MCU_DSP message;
    message.header.type  = VMT_MD_SET_PARAM;
    message.header.param = 0;
    message.header.magic = 0;
    message.header.size  = sizeof(VMB_CM_SET_PARAM) / sizeof(unsigned int);
    message.set_param.param_addr = (void *)MCU_TO_DEV(&g_vsp_param);
    message.set_param.param_size = sizeof(g_vsp_param);

    DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
}

//=================================================================================================

static int _DspInitializeCallback(volatile VSP_MSG_DSP_MCU *response, void *priv)
{
    VSP_MSG_MCU_DSP *request = (VSP_MSG_MCU_DSP *)priv;
    if (response->header.type != VMT_DM_HELLO_DONE) {
        //printf(LOG_TAG"Mismatch Message Type: DSP %x, MCU %x\n", response->header.type, VMT_DM_HELLO_DONE);
        return 0;
    }
    if (response->header.magic != request->header.magic) {
        printf(LOG_TAG"Mismatch Magic Code: Request %x, Response %x\n", request->header.magic, response->header.magic);
        return -1;
    }
    if (response->hello_done.message_version != VSP_MESSAGE_VERSION_BTW_MCU_DSP) {
        printf(LOG_TAG"Mismatch Message Version: DSP %x, MCU %x\n", response->hello_done.message_version, VSP_MESSAGE_VERSION_BTW_MCU_DSP);
        return -3;
    }
    if (response->header.param != 0) {
        printf(LOG_TAG"DSP Returns Error Code: %d\n", response->header.param);
        return -4;
    }
    return 0;
}

int VspInitializeVpa(VSP_CONTEXT_HEADER *ctx_header, DSP_VSP_CALLBACK callback, void *priv)
{
#ifdef CONFIG_VSP_VPA_NEED_SNPU
    if (SnpuInit())
        return -1;
    if (SnpuLoadFirmware())
        return -2;
#endif

    DspSetVspCallback(NULL, NULL);
    VSP_MSG_MCU_DSP message;
    message.header.type = VMT_MD_HELLO;
    message.header.magic = get_time_ms();
    message.header.param = 0;
    message.header.size = sizeof(VMB_MD_HELLO) / sizeof(unsigned int);
    message.hello.message_version = VSP_MESSAGE_VERSION_BTW_MCU_DSP;
    message.hello.context_header_addr = MCU_TO_DEV(ctx_header);
    message.hello.context_header_size = sizeof(VSP_CONTEXT_HEADER);
    if (DspSendVspMessage((unsigned int *)&message, message.header.size + 1,
                          _DspInitializeCallback, &message, DSP_CALLBACK_TIMEOUT))
        return -3;

    s_dsp_callback = callback;
    DspSetVspCallback(_DspDefaultCallback, priv);
    _VspPostOtpKey();

    return 0;
}

//=================================================================================================

static int _DspCleanupCallback(volatile VSP_MSG_DSP_MCU *response, void *priv)
{
    VSP_MSG_MCU_DSP *request = (VSP_MSG_MCU_DSP *)priv;
    if (response->header.type != VMT_DM_CLEANUP_DONE) {
        printf(LOG_TAG"Mismatch Message Type: DSP %x, MCU %x\n", response->header.type, VMT_DM_CLEANUP_DONE);
        return -1;
    }
    if (response->header.magic != request->header.magic) {
        printf(LOG_TAG"Mismatch Magic Code: Request %x, Response %x\n", request->header.magic, response->header.magic);
        return -1;
    }
    return 0;
}

int VspCleanupVpa(void)
{
    int result = 0;

#ifdef CONFIG_VSP_VPA_NEED_SNPU
    result |= SnpuDone();
#endif

    DspSetVspCallback(NULL, NULL);
    VSP_MSG_MCU_DSP message;
    message.header.type = VMT_MD_CLEANUP;
    message.header.magic = get_time_ms();
    message.header.param = 0;
    message.header.size = 0;
    result |= DspSendVspMessage((unsigned int *)&message, message.header.size + 1,
                                _DspCleanupCallback, &message, DSP_CALLBACK_TIMEOUT);

    return result;
}

//=================================================================================================

int VspSuspendVpa(void)
{
    int result = 0;

    VSP_MSG_MCU_DSP message;
    message.header.type = VMT_MD_HALT;
    message.header.magic = get_time_ms();
    message.header.param = 0;
    message.header.size = 0;
    DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
    mdelay(100); // Need larger than context_length

#ifdef CONFIG_VSP_VPA_NEED_SNPU
    SnpuDone();
#endif

    return result;
}

int VspResumeVpa(void)
{
    int result = 0;

#ifdef CONFIG_VSP_VPA_NEED_SNPU
    gx_disable_irq();
    SnpuInit();
    gx_enable_irq();
#endif

    return result;
}


//=================================================================================================
static void _VspDumpFirmwareDspHeader(VSP_FIRMWARE_DSP_HEADER dsp_header)
{
    printf(LOG_TAG" dsp_header.magic = %x\n", dsp_header.magic);
    printf(LOG_TAG" dsp_header.message_version = %x\n", dsp_header.message_version);
    printf(LOG_TAG" dsp_header.xip_size = %d\n", dsp_header.xip_size);
    printf(LOG_TAG" dsp_header.ptcm_size = %d\n", dsp_header.ptcm_size);
    printf(LOG_TAG" dsp_header.dtcm_size = %d\n", dsp_header.dtcm_size);
    printf(LOG_TAG" dsp_header.sram_size = %d\n", dsp_header.sram_size);
    printf(LOG_TAG" dsp_header.sram_base = %x\n", dsp_header.sram_base);
}

//=================================================================================================
static int serial_port = UART_PORT0;
static int _VspUartRecvImage(unsigned char *buf, unsigned int len)
{
    unsigned char *p = NULL;
    int i = 0;
    int size = 0;
    unsigned int check_sum = 0, new_check_sum = 0;

    p = (unsigned char *)&check_sum;
    for(i = 0; i < sizeof(check_sum); i++){
        *p++ = UartRecvByte(serial_port);
    }
    UartSyncRecvBuffer(serial_port, buf, len);
    size = len;
    p = buf;
    for(i = 0; i < size; i++) {
        new_check_sum += p[i];
    }
    if (new_check_sum == check_sum) {
        return 0;
    } else {
        return -1;
    }
}

static int _VspLoadVpaFromUart(void)
{
    VSP_FIRMWARE_DSP_HEADER dsp_header;

    // Init serial port for vpa loading
    UartInit(serial_port, 3000000);

    // Init dsp
    DspInit();

    UartSyncSendBuffer(serial_port, (unsigned char *)"DSP", 3);

    if(!_VspUartRecvImage((unsigned char *)&dsp_header, sizeof(VSP_FIRMWARE_DSP_HEADER))) {
        UartSyncSendBuffer(serial_port, (unsigned char *)"H", 1);
    } else {
        return -1;
    }

    if (dsp_header.message_version != VSP_MESSAGE_VERSION_BTW_MCU_DSP)
    {
        printf(LOG_TAG"The VPA is not compatiale with us!\n");
        return -1;
    }
    if ((unsigned int)DEV_TO_MCU(dsp_header.sram_base) < CONFIG_DSP_SRAM_BASE) {
        printf("DSP SRAM section is in MCU region: %08X\n", DEV_TO_MCU(dsp_header.sram_base));
        return -1;
    }

    // Load ptcm from uart
    void *ptcm_buffer = DEV_TO_MCU(dsp_header.sram_base);
    if (!_VspUartRecvImage((unsigned char *)ptcm_buffer, dsp_header.ptcm_size)) {
        UartSyncSendBuffer(serial_port, (unsigned char *)"PTCM", 4);
    } else {
        return -1;
    }
    DspLoadPTCMSection(ptcm_buffer, dsp_header.ptcm_size);

    // Load dtcm from uart
    void *dtcm_buffer = DEV_TO_MCU(dsp_header.sram_base);
    if (!_VspUartRecvImage((unsigned char *)dtcm_buffer, dsp_header.dtcm_size)) {
        UartSyncSendBuffer(serial_port, (unsigned char *)"DTCM", 4);
    } else {
        return -1;
    }
    DspLoadDTCMSection(dtcm_buffer, dsp_header.dtcm_size);

    // Load sram from uart
    void *sram_buffer = DEV_TO_MCU(dsp_header.sram_base);
    if (!_VspUartRecvImage((unsigned char *)sram_buffer, dsp_header.sram_size)) {
        UartSyncSendBuffer(serial_port, (unsigned char *)"SRAM", 4);
    } else {
        return -1;
    }

    // Run dsp
    DspRun();

    _VspDumpFirmwareDspHeader(dsp_header);

    return 0;
}

static int _VspLoadVpaFromFlash(unsigned int offset, void *dma_buffer, unsigned int dma_size)
{
    VSP_FIRMWARE_DSP_HEADER dsp_header;

    if (SpiFlashInit(dma_buffer, dma_size) != 0) {
        printf(LOG_TAG"Failed to initialize SPI Flash Driver!\n");
        return -1;
    }
    SpiFlashRead(offset, (unsigned char *)&dsp_header, sizeof(VSP_FIRMWARE_DSP_HEADER));

    _VspDumpFirmwareDspHeader(dsp_header);

    if (dsp_header.message_version != VSP_MESSAGE_VERSION_BTW_MCU_DSP) {
        printf(LOG_TAG"The VPA is not compatiale with us!\n");
        return -1;
    }

    if ((unsigned int)DEV_TO_MCU(dsp_header.sram_base) < CONFIG_DSP_SRAM_BASE) {
        printf("DSP SRAM section is in MCU region: %08X\n", DEV_TO_MCU(dsp_header.sram_base));
        return -1;
    }

    // Init dsp
    DspInit();

    // Load dsp firmware
    uint32_t ptcm_offset = offset + sizeof(VSP_FIRMWARE_DSP_HEADER) + dsp_header.xip_size;
    uint32_t dtcm_offset = ptcm_offset + dsp_header.ptcm_size;
    uint32_t sram_offset = dtcm_offset + dsp_header.dtcm_size;
#ifdef CONFIG_MCU_ENABLE_XIP
    // Load ptcm from flash
    void *ptcm_buffer = (void *)(MCU_FLASH_XIP_BASE + ptcm_offset);
    DspLoadPTCMSection(ptcm_buffer, dsp_header.ptcm_size);

    // Load dtcm from flash
    void *dtcm_buffer = (void *)(MCU_FLASH_XIP_BASE + dtcm_offset);
    DspLoadDTCMSection(dtcm_buffer, dsp_header.dtcm_size);

    // Load sram from flash
    void *sram_buffer = DEV_TO_MCU(dsp_header.sram_base);
    DspLoadSRAMSection((unsigned char *)sram_buffer, (const unsigned char *)(MCU_FLASH_XIP_BASE + sram_offset), dsp_header.sram_size);
#else
    // Load ptcm from flash
    void *ptcm_buffer = DEV_TO_MCU(dsp_header.sram_base);
    SpiFlashRead(ptcm_offset, (unsigned char *)ptcm_buffer, dsp_header.ptcm_size);
    DspLoadPTCMSection(ptcm_buffer, dsp_header.ptcm_size);

    // Load dtcm from flash
    void *dtcm_buffer = DEV_TO_MCU(dsp_header.sram_base);
    SpiFlashRead(dtcm_offset, (unsigned char *)dtcm_buffer, dsp_header.dtcm_size);
    DspLoadDTCMSection(dtcm_buffer, dsp_header.dtcm_size);

    // Load sram from flash
    void *sram_buffer = DEV_TO_MCU(dsp_header.sram_base);
    SpiFlashRead(sram_offset, (unsigned char *)sram_buffer, dsp_header.sram_size);

    SpiFlashDone();
#endif

    // Run dsp
    DspRun();

    return 0;
}

int VspLoadVpa(unsigned int offset, void *dma_buffer, unsigned int dma_size)
{
    volatile BOOT_MODE *mode = (volatile BOOT_MODE *)MISC_REG_BOOT_MODE;

    if(mode->uart_boot == BOOT_MODE_UART) {
#ifdef CONFIG_GX8008B
        serial_port = mode->uart_boot_port;
#endif
        return _VspLoadVpaFromUart(); // Load VPA from Uart
    } else {
        return _VspLoadVpaFromFlash(offset, dma_buffer, dma_size); // Load VPA from Flash
    }
}

int VspLoadVpaConfigFromFlash(unsigned int config_offset, unsigned char *tmp_buffer)
{
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return -1;
    }

    VSP_VPA_CONFIG_HEADER vpa_config_header;
    SpiFlashRead(config_offset, (unsigned char *)(&vpa_config_header), sizeof(VSP_VPA_CONFIG_HEADER));

    if (vpa_config_header.magic == 0x1234 && vpa_config_header.kws_number != 0 && vpa_config_header.kws_number != 0xff) {

        unsigned int kws_list_size = vpa_config_header.kws_number * sizeof(VSP_KWS_SOURCE);
        SpiFlashRead(config_offset + sizeof(VSP_VPA_CONFIG_HEADER), (unsigned char *)(tmp_buffer), kws_list_size);

        VSP_PARAM vsp_param;
        vsp_param.type = VSP_PARAM_TYPE_KWS_CONFIG;
        vsp_param.param.kws_config.buff_addr = (unsigned int)MCU_TO_DEV(tmp_buffer);
        vsp_param.param.kws_config.buff_size = kws_list_size;

        VSP_MSG_MCU_DSP message;
        message.header.type  = VMT_MD_SET_PARAM;
        message.header.param = 0;
        message.header.magic = 0;
        message.header.size  = sizeof(VMB_CM_SET_PARAM) / sizeof(unsigned int);
        message.set_param.param_addr = (void *)MCU_TO_DEV(&vsp_param);
        message.set_param.param_size = sizeof(vsp_param);

        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
        mdelay(100);

        unsigned int vma_config_size = vpa_config_header.vma_config_length;
        SpiFlashRead(config_offset + vpa_config_header.vma_config_offset, (unsigned char *)(tmp_buffer), vma_config_size);

        memset((void *)&vsp_param, 0, sizeof(vsp_param));
        vsp_param.type = VSP_PARAM_TYPE_VMA_CONFIG;
        vsp_param.param.vma_config.buff_addr = (unsigned int)MCU_TO_DEV(tmp_buffer);
        vsp_param.param.vma_config.buff_size = vma_config_size;

        message.header.type  = VMT_MD_SET_PARAM;
        message.header.param = 0;
        message.header.magic = 0;
        message.header.size  = sizeof(VMB_CM_SET_PARAM) / sizeof(unsigned int);
        message.set_param.param_addr = (void *)MCU_TO_DEV(&vsp_param);
        message.set_param.param_size = sizeof(vsp_param);

        DspPostVspMessage((unsigned int *)&message, message.header.size + 1);
        mdelay(10);
    }
    return 0;
}

