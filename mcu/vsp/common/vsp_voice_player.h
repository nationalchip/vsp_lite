/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_voice_player.h: vsp voice player, data from flash or uart
 *
 */

#ifndef __VSP_VOICE_PLAYER_H__
#define __VSP_VOICE_PLAYER_H__

#include <driver/audio_out.h>
#include "vsp_plc_event.h"

typedef struct {
    AUDIO_OUT_SAMPLE_RATE   sample_rate;    // range : 0x01 - 0x07 : 48000 44100 32000 24000 22050 16000 11025 8000
    unsigned int            volume;         // default 0: 0dB, -18 ~ 18 dB
    unsigned int            channel_type;   // 1:STEREO and 0:MONO ,support MONO only for now
    unsigned int            sample_bit;     // support 16:16bit for now
} UART_VOICE_PLAY_CONFIG;

typedef int (*UART_PLAYER_FLOW_CONTOL)(void *priv);

typedef struct {
    UART_PLAYER_FLOW_CONTOL Uart_Player_Flow_Control; //Its will be called in main loop, until it return 1
    void *priv;
} VSP_VOICE_PLAYER_INIT_CONFIG;

int VspVoicePlayerInit(VSP_VOICE_PLAYER_INIT_CONFIG *config);

int VspVoicePlayerSetVolume(int volume); //volume == [-18 ~ 18] : -18dB ~ 18dB; volume < -18 : mute:
int VspVoicePlayerGetVolume(void); // return volume

int VspVoicePlayerMute(unsigned int mute); // mute == 1 : mute;

int VspVoicePlayerPlay(int offset, unsigned int size, PLC_RESURCE_TYPE type); // offset: input data's address in flash

int VspVoicePlayerTick(void *priv); // Its need be called in tick_loop.

#endif /* __VSP_VOICE_PLAYER_H__ */
