/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_agc.c:
 */

#include <autoconf.h>
#include <stdio.h>
#include <string.h>
#include <driver/audio_mic.h>
#include <driver/audio_ref.h>
#include "vsp_agc.h"
#include "../vsp_buffer.h"

#define LOG_TAG "[AGC]"

#define MAX_GAIN    CONFIG_VSP_AGC_MAX_GAIN
#define MIN_GAIN    CONFIG_VSP_AGC_MIN_GAIN

#ifdef CONFIG_VSP_AGC_FUNCTION_ENABLE

static VSP_AGC_HANDLE s_agc_handle = {{0},{0},{0},0};

int VspAudionInSetAgcSmoothParam(unsigned int threshold)
{
    s_agc_handle.agc_smooth.smooth_threshold = threshold;
    s_agc_handle.agc_smooth.smooth_count     = 0;
    return 0;
}

unsigned char VspAgcGetMicGain(void)
{
    return s_agc_handle.prev_agc_ctrl.mic_gain;
}

unsigned char VspAgcGetRefGain(void)
{
    return s_agc_handle.prev_agc_ctrl.ref_gain;
}

int VspInitGainControl(VSP_AGC_HANDLE *agc_handle)
{
    if (agc_handle) {
        memcpy((void*)&s_agc_handle, (void*)agc_handle, sizeof(VSP_AGC_HANDLE));
    } else {
        // The default value will keep tha agc dose not modify the default gain vaule
        s_agc_handle.curr_agc_ctrl.mic_gain = AudioInGetMicGain();
        s_agc_handle.curr_agc_ctrl.ref_gain = AudioInGetRefGain();
        s_agc_handle.agc_smooth.smooth_threshold = 100; // 30 * 3 * 32 ms
        s_agc_handle.agc_smooth.smooth_count     = 0;
        s_agc_handle.gain_width                  = 2;
    }
    return 0;
}

int VspAudioInAgcAdjustGain(void)
{
    // Need be carefull to increase the gain
    unsigned int agc_eng    = AudioInGetAgcEnergy();
    unsigned int up_agc_eng = 7577 * 4; // 0.08 * 4
    unsigned int threshold  = s_agc_handle.agc_smooth.smooth_threshold;
    if (agc_eng < up_agc_eng) {
        // smooth
        if (s_agc_handle.agc_smooth.smooth_count > threshold) {
            s_agc_handle.agc_smooth.smooth_count = 0;
            int mic_gain = AudioInGetMicGain();
            int ref_gain = AudioInGetRefGain();
            mic_gain += s_agc_handle.gain_width;
            ref_gain += s_agc_handle.gain_width;

#ifdef CONFIG_VSP_ENABLE_ADJUST_REF_GAIN
            int max_gain = mic_gain>ref_gain ? mic_gain:ref_gain;
#else
            int max_gain = mic_gain;
#endif
            if( max_gain > MAX_GAIN) {
                // gain is out of bound
            } else {
                s_agc_handle.curr_agc_ctrl.mic_gain = mic_gain;
                s_agc_handle.curr_agc_ctrl.ref_gain = ref_gain;
            }
        }
        else {
            s_agc_handle.agc_smooth.smooth_count++;
        }
    }
    else {
        s_agc_handle.agc_smooth.smooth_count = 0;
    }

    // prev context gain
    s_agc_handle.prev_agc_ctrl.mic_gain = AudioInGetMicGain();
    s_agc_handle.prev_agc_ctrl.ref_gain = AudioInGetRefGain();
    if (s_agc_handle.curr_agc_ctrl.mic_gain != AudioInGetMicGain()) {
        printf (LOG_TAG"mic_gain:%d\n", s_agc_handle.curr_agc_ctrl.mic_gain);
        AudioInSetMicGain(s_agc_handle.curr_agc_ctrl.mic_gain);
#ifdef CONFIG_VSP_ENABLE_ADJUST_REF_GAIN
        AudioInSetRefGain(s_agc_handle.curr_agc_ctrl.ref_gain);
#endif
    }

    return 0;
}

static int _AudioInAgcSoftCallback(AUDIO_IN_AGC_SOFT_STATE state, unsigned int agc_eng)
{
    if (AUDIO_IN_AGC_SOFT_STATE_DONE == state) { // Need reduce the mic gain
        s_agc_handle.agc_smooth.smooth_count = 0;

        int mic_gain = AudioInGetMicGain();
        int ref_gain = AudioInGetRefGain();
        mic_gain -= s_agc_handle.gain_width;
        ref_gain -= s_agc_handle.gain_width;

#ifdef CONFIG_VSP_ENABLE_ADJUST_REF_GAIN
        int min_gain = mic_gain>ref_gain ? ref_gain:mic_gain;
#else
        int min_gain = mic_gain;
#endif
        if(min_gain < MIN_GAIN ) {
            // gain is out of bound
        } else {
            s_agc_handle.curr_agc_ctrl.mic_gain = mic_gain;
            s_agc_handle.curr_agc_ctrl.ref_gain = ref_gain;
        }
    }
    return 0;
}

int VspAudioInInitAgcConfig(AUDIO_IN_AGC_CONFIG *acg_config)
{
    if (acg_config){
        AudioInAgcConfig(acg_config);
    } else {

        AUDIO_IN_AGC_CONFIG config = {
            .eng_low      = 7577,    // 0.08
            .eng_high     = 7577*14, // 0.08*14;
            .sample_num   = VSP_FRAME_SIZE,
            .int_en_up    = 0,      // disable agc up interrupt
            .int_en_down  = 1,      // enable agc down interrupt
#ifdef CONFIG_GX8008B
            .channel_sel  = AIN_AGC_CH_0,
#else
            .channel_sel  = AIN_AGC_CH_7,
#endif
            .mode         = AUDIO_IN_AGC_MODE_ENABLE | AUDIO_IN_AGC_MODE_SOFTWARE,
            .agc_callback = _AudioInAgcSoftCallback,
        };
        AudioInAgcConfig(&config);
    }
    return 0;
}
#endif

