/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_voice_player.c : Vsp voice player driver, data from flash
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/spi_flash.h>
#include <driver/misc.h>
#include <driver/delay.h>

#include <mad.h>

#include "uart_message_v2.h"
#include "vsp_voice_player.h"

#define LOG_TAG "[VSP_VOICE_PLAYER]"

#ifdef CONFIG_VSP_ENABLE_UART_WAV_PLAYER
static unsigned int s_uart_data_ready = 0;
static unsigned int s_uart_playing = 0;
#endif

static struct {
    int volume;
    unsigned int mute;
} s_player_handle;

#if (defined CONFIG_VSP_ENABLE_WAV_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
//=================================================================================================
// Read from flash, to prepare for event response
static unsigned char s_wav_buff[CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE * 2] __attribute__((aligned(8))); // Need 8 bytes aligned

static struct {
    unsigned int   refresh_flag;
    unsigned int   total_frames;
    unsigned int   play_frame_index;
    unsigned int   play_wav_addr;
    unsigned int   wav_offset; // The wav offset address in flash, the base address is CONFIG_VSP_PLC_VOICE_PLAYER_LOAD_EVENT_CONFIG_FROM
} s_vsp_event_play_wav = {.play_wav_addr = (unsigned int)s_wav_buff};


typedef struct
{
    unsigned int riff_id;
    unsigned int riff_size;
    unsigned int riff_format;
} RIFF_HEADER;

typedef struct
{
    unsigned int    format_id;
    unsigned int    format_size;
    unsigned short  format_tag;
    unsigned short  channels;
    unsigned int    sample_rate;
    unsigned int    bytes_rate;
    unsigned short  block_align;
    unsigned short  bits;
} WAVE_FORMAT;

typedef struct
{
    unsigned int    fact_id;
    unsigned int    fact_size;
    unsigned int    fact_data;
} WAVE_FACT;

typedef struct
{
    unsigned char  sub_list_id[4];
    unsigned char  sub_list_size[4];
    unsigned char  sub_list_Data[14];
} WAV_SUB_LIST;

typedef struct
{
    unsigned char    list_id[4];
    unsigned char    list_size[4];
    unsigned char    list_typ[4];
    WAV_SUB_LIST     sub_list;
} WAVE_LIST;

typedef struct
{
    unsigned int data_tag;
    unsigned int data_size;
} WAVE_DATA_INFO;

typedef struct
{
    RIFF_HEADER riff_header;
    WAVE_FORMAT wave_format;
} WAVE_BASE_HEADER;
#endif

#if (defined CONFIG_VSP_ENABLE_MP3_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
//=================================================================================================
// Decode MP3
#define MP3_BUFFER_SIZE 4096
#define PCM_BUFFER_SIZE CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE
typedef struct {
    unsigned char mp3_buffer[MP3_BUFFER_SIZE * 2] __attribute__((aligned(8)));
    unsigned char pcm_buffer[PCM_BUFFER_SIZE * 2] __attribute__((aligned(8)));
    unsigned int sample_rate;// The sample rate of mp3
    unsigned int mp3_size;   // The length of mp3
    unsigned int pcm_size;   // The length of pcm
    unsigned int index;      // Decode index
    unsigned int mp3_offset; // The mp3 offset address in flash
    unsigned int read_size;  // The data size that is decoded
    unsigned int frame_size; // Decode frame size
    unsigned int refresh_flag; // 1-->>need to ready data tp play
} MP3_INFO;
static MP3_INFO g_mp3_info;
#endif

//=================================================================================================

static int _VspVoicePlayerLoadVoiceData(unsigned int voice_data_offset, void* voice_data_addr, unsigned int voice_data_len)
{
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return -1;
    }
    int ret = SpiFlashRead(voice_data_offset, (unsigned char *)voice_data_addr, voice_data_len);
    return ret;
}

static int _VspVoicePlayerAudioOutCallback(unsigned int sdc_addr, void *priv)
{
#ifdef CONFIG_VSP_ENABLE_UART_WAV_PLAYER
    s_uart_data_ready = 0;
#endif
#if (defined CONFIG_VSP_ENABLE_WAV_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
    if (s_vsp_event_play_wav.play_frame_index < s_vsp_event_play_wav.total_frames) {
        s_vsp_event_play_wav.refresh_flag = 1;
    }
#endif

#if (defined CONFIG_VSP_ENABLE_MP3_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
    g_mp3_info.refresh_flag = 1; // For play mp3
#endif

    return 0;
}

//-------------------------------------------------------------------------------------------------

#if (defined CONFIG_VSP_ENABLE_WAV_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
static int _VspVoicePlayerGetWavInfo(const unsigned char *wav_addr, unsigned int *wav_header_len, unsigned int *sample_rate, unsigned short *channel_type)
{
    WAVE_BASE_HEADER wave_base_header;

    if (NULL == wav_addr) return -1;
    memcpy((void *)&wave_base_header, (void *)wav_addr, sizeof(WAVE_BASE_HEADER));
    wav_addr += sizeof(WAVE_BASE_HEADER);

#ifdef CONFIG_MCU_ENABLE_DEBUG
    printf ("riff_id    :0x%x\n", wave_base_header.riff_header.riff_id);
    printf ("riff_size  :%d\n",   wave_base_header.riff_header.riff_size);
    printf ("format_id  :0x%x\n", wave_base_header.wave_format.format_id);
    printf ("format_size:0x%x\n", wave_base_header.wave_format.format_size);
    printf ("format_tag :0x%x\n", wave_base_header.wave_format.format_tag);
    printf ("channels   :0x%x\n", wave_base_header.wave_format.channels);
    printf ("sample_rate:%d\n",   wave_base_header.wave_format.sample_rate);
    printf ("bytes_rate :0x%x\n", wave_base_header.wave_format.bytes_rate);
    printf ("block_align:0x%x\n", wave_base_header.wave_format.block_align);
    printf ("bits       :0x%x\n", wave_base_header.wave_format.bits);
#endif

    unsigned int wav_sample_rate = wave_base_header.wave_format.sample_rate;
    if (8000 == wav_sample_rate)       *sample_rate = AUDIO_OUT_SAMPLE_RATE_8000;
    else if (11025 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_11025;
    else if (16000 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
    else if (22050 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_22050;
    else if (24000 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_24000;
    else if (32000 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_32000;
    else if (44100 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_44100;
    else if (48000 == wav_sample_rate) *sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
    else {
        printf (LOG_TAG"Don't support this sample_rate:%d\n", wav_sample_rate);
        return -1;
    }
    *channel_type   = wave_base_header.wave_format.channels <= 1 ? AUDIO_OUT_CHANNEL_MONO : AUDIO_OUT_CHANNEL_STEREO;
    *wav_header_len = sizeof(WAVE_BASE_HEADER);

    unsigned int chunk_id = 0;
    memcpy(&chunk_id, wav_addr, sizeof(int));
    if(chunk_id == 0x61746164) {
        *wav_header_len += sizeof(WAVE_DATA_INFO);
        return 0;
    }
    else if(chunk_id == 0x5453494c) {
        wav_addr += sizeof(WAVE_LIST);
        *wav_header_len += sizeof(WAVE_LIST);
        unsigned int tmp_chunk_id = 0;
        memcpy(&tmp_chunk_id, wav_addr, sizeof(int));
        if(tmp_chunk_id == 0x61746164) {
            *wav_header_len += sizeof(WAVE_DATA_INFO);
            return 0;
        }
        else if(tmp_chunk_id == 0x74636166) {
            *wav_header_len += (sizeof(WAVE_FACT) + sizeof(WAVE_DATA_INFO));
            return 0;
        }
    }
    else if(chunk_id == 0x74636166) {
        wav_addr += sizeof(WAVE_FACT);
        *wav_header_len += sizeof(WAVE_FACT);
        unsigned int tmp_chunk_id = 0;
        memcpy(&tmp_chunk_id, wav_addr, sizeof(int));
        if(tmp_chunk_id == 0x61746164) {
            *wav_header_len += sizeof(WAVE_DATA_INFO);
            return 0;
        }
        else if(tmp_chunk_id == 0x5453494c) {
            *wav_header_len += (sizeof(WAVE_LIST) + sizeof(WAVE_DATA_INFO));
            return 0;
        }
    }
    else {
        printf("Unparseable wav file header information!\n");
        return -1;
    }
    return 0;
}

static int _VspVoicePlayerPlayWavByFrameInit(int wav_offset, unsigned  int wav_size)
{
    s_vsp_event_play_wav.total_frames     = wav_size / CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE;
    s_vsp_event_play_wav.wav_offset       = wav_offset;
    s_vsp_event_play_wav.play_frame_index = 0;
    s_vsp_event_play_wav.refresh_flag     = 1;

    return 0;
}

static int _VspVoicePlayerPlayWavByFrame(void)
{
    if (!s_vsp_event_play_wav.refresh_flag) return 0;
    s_vsp_event_play_wav.refresh_flag = 0;

    unsigned int play_buffer_offset = 0;
    if (s_vsp_event_play_wav.play_frame_index % 2 == 0) {
        play_buffer_offset = 0;
    } else {
        play_buffer_offset = CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE;
    }
    void *play_buffer       = (void *)((unsigned int)s_wav_buff + play_buffer_offset);
    unsigned int wav_offset = s_vsp_event_play_wav.wav_offset + s_vsp_event_play_wav.play_frame_index * CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE;
    s_vsp_event_play_wav.play_frame_index += 1;

    _VspVoicePlayerLoadVoiceData(wav_offset, play_buffer, CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE);
    AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)play_buffer_offset, (CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE / 1024 * 1024));

    return 0;
}

#define MAX_WAV_HEADER_SIZE 90
int _VspVoicePlayerProcWav(int wav_offset, unsigned int wav_size)
{
    if (!wav_size) return 0;
    unsigned int sample_rate      = 0;
    unsigned short channel_type   = 0;
    unsigned int wav_header_len   = 0;
    unsigned char wav_header_buffer[MAX_WAV_HEADER_SIZE] = {0};

    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return -1;
    }
    SpiFlashRead(wav_offset, (unsigned char *)wav_header_buffer, MAX_WAV_HEADER_SIZE);

    if(0 == _VspVoicePlayerGetWavInfo(wav_header_buffer, &wav_header_len, &sample_rate, &channel_type)) {
        AUDIO_OUT_BUFFER_CONFIG buffer_config = {
            .sample_rate  = sample_rate,
            .channel_type = channel_type,
            .buffer_addr  = (const short *)MCU_TO_DEV(s_vsp_event_play_wav.play_wav_addr),
            .buffer_size  = CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE * 2 / 1024 * 1024,
            .play_callback = _VspVoicePlayerAudioOutCallback
        };
        AudioOutInit(NULL, &buffer_config);
        AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, s_player_handle.volume);
        AudioOutSetMute(AUDIO_OUT_ROUTE_BUFFER,s_player_handle.mute);

        printf (LOG_TAG"[wav] sample_rate:[%d], channels:%d\n", buffer_config.sample_rate, buffer_config.channel_type);
        _VspVoicePlayerPlayWavByFrameInit(wav_offset + wav_header_len, wav_size - wav_header_len);
        _VspVoicePlayerPlayWavByFrame();
        return 0;
    } else {
        printf (LOG_TAG"[error]Parse wav header failed!\n");
        return -1;
    }

}
#endif

#if (defined CONFIG_VSP_ENABLE_MP3_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
static void _LoadMp3Data(unsigned int flash_offset, unsigned char *mp3_buffer, unsigned int mp3_size)
{
    _VspVoicePlayerLoadVoiceData(flash_offset, mp3_buffer, mp3_size);
}

static enum mad_flow _VspVoicePlayerDecodeMp3Input(void *data, struct mad_stream *stream)
{
    int ret = 0;
    MP3_INFO *mp3_info = data;

    PLC_STATUS plc_status_result;
    VspGetPlcStatus(PST_NEW_VOICE_PLAY_TASK, &plc_status_result);
    if (plc_status_result.new_voice_play_task)
        return MAD_FLOW_STOP;

    int unproc_data_len = stream->bufend - stream->next_frame;
    if ( (mp3_info->read_size + mp3_info->frame_size - unproc_data_len) <= mp3_info->mp3_size) {
        unsigned char * mp3_buffer = &mp3_info->mp3_buffer[mp3_info->index % 2 * MP3_BUFFER_SIZE];
        _LoadMp3Data(mp3_info->mp3_offset + mp3_info->read_size - unproc_data_len, mp3_buffer, mp3_info->frame_size);
        mad_stream_buffer(stream, mp3_buffer, mp3_info->frame_size);

        mp3_info->read_size += mp3_info->frame_size - unproc_data_len;
        mp3_info->index     += 1;
        ret = MAD_FLOW_CONTINUE;
    } else if (mp3_info->read_size < mp3_info->mp3_size) { // Decode unprocessed data in the last frame
        unsigned char * mp3_buffer = &mp3_info->mp3_buffer[mp3_info->index % 2 * MP3_BUFFER_SIZE];
        _LoadMp3Data(mp3_info->mp3_offset + mp3_info->read_size - unproc_data_len, mp3_buffer, mp3_info->mp3_size - mp3_info->read_size + unproc_data_len);
        mad_stream_buffer(stream, mp3_buffer, mp3_info->mp3_size - mp3_info->read_size + unproc_data_len);

        mp3_info->read_size += mp3_info->mp3_size - mp3_info->read_size;
        mp3_info->index     += 1;
        ret = MAD_FLOW_CONTINUE;
    } else {
        ret = MAD_FLOW_STOP;
    }

    return ret;
}

static inline signed int _Scale(mad_fixed_t sample)
{
    /* round */
    sample += (1L << (MAD_F_FRACBITS - 16));

    /* clip */
    if (sample >= MAD_F_ONE)
        sample = MAD_F_ONE - 1;
    else if (sample < -MAD_F_ONE)
        sample = -MAD_F_ONE;

    /* quantize */
    return sample >> (MAD_F_FRACBITS + 1 - 16);
}

static enum mad_flow _VspVoicePlayerDecodeMp3Output(void *data, struct mad_header const *header, struct mad_pcm *pcm)
{
    static int write_ptr = 0;
    static int frame_page_index = 1;
    unsigned int channels, samples, sample_rate;
    MP3_INFO *mp3_info = data;
    short *pcm_buffer = (short *)&mp3_info->pcm_buffer[0];

    mad_fixed_t *left_ch  = pcm->samples[0];
    mad_fixed_t *right_ch = pcm->samples[1];
    mp3_info->sample_rate = pcm->samplerate;
    channels = pcm->channels;
    samples  = pcm->length;

    if (mp3_info->pcm_size == 0) {
        write_ptr = 0;
        frame_page_index = 1;
        if (8000 == mp3_info->sample_rate)       sample_rate = AUDIO_OUT_SAMPLE_RATE_8000;
        else if (11025 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_11025;
        else if (16000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_16000;
        else if (22050 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_22050;
        else if (24000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_24000;
        else if (32000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_32000;
        else if (44100 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_44100;
        else if (48000 == mp3_info->sample_rate) sample_rate = AUDIO_OUT_SAMPLE_RATE_48000;
        else {
            printf (LOG_TAG"Don't support this sample_rate:%d\n", pcm->samplerate);
            return MAD_FLOW_STOP;
        }

        printf (LOG_TAG"[mp3] sample_rate:%d[%d], channels:%d\n", mp3_info->sample_rate, sample_rate, channels);
        AUDIO_OUT_BUFFER_CONFIG buffer_config = {
            .sample_rate = sample_rate,
            .channel_type = channels <= 1 ? AUDIO_OUT_CHANNEL_MONO : AUDIO_OUT_CHANNEL_STEREO,
            .buffer_addr = (const short *)MCU_TO_DEV(mp3_info->pcm_buffer),
            .buffer_size = PCM_BUFFER_SIZE * 2 / 1024 * 1024,
            .play_callback = _VspVoicePlayerAudioOutCallback
        };
        AudioOutInit(NULL, &buffer_config);
        AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, s_player_handle.volume);
        AudioOutSetMute(AUDIO_OUT_ROUTE_BUFFER,s_player_handle.mute);
    }

    while (samples--) {
        while (!mp3_info->refresh_flag) mdelay(1);

        pcm_buffer[write_ptr++] = _Scale(*left_ch++);
        mp3_info->pcm_size += 2;

        if (channels == 2) {
            pcm_buffer[write_ptr++] = _Scale(*right_ch++);
            mp3_info->pcm_size += 2;
        }

        if (write_ptr > PCM_BUFFER_SIZE) write_ptr = 0;

        int offset = 0;
        if (mp3_info->pcm_size > frame_page_index * PCM_BUFFER_SIZE) {
            mp3_info->refresh_flag = 0;
            if (frame_page_index % 2) {
                offset = 0;
            } else {
                offset = PCM_BUFFER_SIZE;
            }
            AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)offset, PCM_BUFFER_SIZE);
            frame_page_index++;
        }
    }

    // Play not played data in the last frame
    int offset = 0;
    if (mp3_info->read_size == mp3_info->mp3_size) {
        if (frame_page_index % 2) {
            offset = 0;
        } else {
            offset = PCM_BUFFER_SIZE;
        }
        AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)offset, PCM_BUFFER_SIZE);
        frame_page_index++;
    }

    return MAD_FLOW_CONTINUE;
}

static enum mad_flow _VspVoicePlayerDecodeMp3Error(void *data, struct mad_stream *stream, struct mad_frame *frame)
{
#if 0
    MP3_INFO *mp3_info = data;
    printf("[%s] decoding error 0x%04x (%s) at byte offset %u\n",
            __FUNCTION__,
            stream->error, mad_stream_errorstr(stream),
            stream->this_frame - mp3_info->mp3_buffer);
#endif

    return MAD_FLOW_CONTINUE;
}

//==============================================================

int _VspVoicePlayerProcMp3(int mp3_offset, unsigned int mp3_size)
{
    if (!mp3_size) return 0;

    PLC_STATUS plc_status_result = {.new_voice_play_task = 0};
    VspSetPlcStatus(PST_NEW_VOICE_PLAY_TASK, &plc_status_result);

    g_mp3_info.mp3_size   = mp3_size;
    g_mp3_info.pcm_size   = 0;
    g_mp3_info.index      = 0;
    g_mp3_info.mp3_offset = mp3_offset;
    g_mp3_info.read_size  = 0;
    g_mp3_info.frame_size = MP3_BUFFER_SIZE;
    g_mp3_info.refresh_flag = 1;

    struct mad_decoder decoder;
    mad_decoder_init(&decoder, &g_mp3_info, _VspVoicePlayerDecodeMp3Input, 0, 0, _VspVoicePlayerDecodeMp3Output, _VspVoicePlayerDecodeMp3Error, 0);
    mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);
    mad_decoder_finish(&decoder);

    return 0;
}

#endif

#ifdef CONFIG_VSP_ENABLE_UART_WAV_PLAYER
static UART_PLAYER_FLOW_CONTOL _FlowControl = NULL;
static void *s_flow_control_priv = NULL;
static int s_flow_control_ready = 0;
static int _MsgPackCallback_UartPlayPcm(MSG_PACK * pack, void *priv)
{
    if (pack->msg_header.cmd == MSG_NTF_PCM_DATA \
        && (pack->len % 8) == 0 && ((unsigned int)pack->body_addr) % 8 == 0 \
        && s_uart_playing == 1 && s_flow_control_ready == 1 && s_uart_data_ready == 0) {

        s_uart_data_ready = 1;
        s_flow_control_ready = 0;
        AudioOutPlay(AUDIO_OUT_ROUTE_BUFFER, (short *)(pack->body_addr - s_wav_buff), pack->len);
    }

    if (pack->msg_header.cmd == MSG_NTF_PCM_START && pack->len == sizeof(UART_VOICE_PLAY_CONFIG)) {
            UART_VOICE_PLAY_CONFIG *config = (UART_VOICE_PLAY_CONFIG *)pack->body_addr;

            AUDIO_OUT_BUFFER_CONFIG buffer_config = {
                .sample_rate = config->sample_rate,
                .channel_type = AUDIO_OUT_CHANNEL_MONO,
                .buffer_addr = (const short *)MCU_TO_DEV(s_vsp_event_play_wav.play_wav_addr),
                .buffer_size = CONFIG_VSP_VOICE_PLAYER_WAV_FRAME_SIZE * 2 / 1024 * 1024,
                .play_callback = _VspVoicePlayerAudioOutCallback
            };
            AudioOutInit(NULL, &buffer_config);
            AudioOutSetVolume(AUDIO_OUT_ROUTE_BUFFER, config->volume);

            s_uart_data_ready = 0;
            s_flow_control_ready = 0;
            s_uart_playing = 1;
            return 0;
    }

    if (pack->msg_header.cmd == MSG_NTF_PCM_DONE) {
        s_uart_playing = 0;
    }

    return 0;
}
#endif

int VspVoicePlayerInit(VSP_VOICE_PLAYER_INIT_CONFIG *config)
{
    s_player_handle.volume = 0;
    s_player_handle.mute = 0;

#ifdef CONFIG_VSP_ENABLE_UART_WAV_PLAYER
    if (config != NULL) {
        _FlowControl = config->Uart_Player_Flow_Control;
        s_flow_control_priv = config->priv;
    }

    UART_MSG_INIT_CONFIG uart_msg_init_config = {
        .port = CONFIG_VSP_VOICE_PLAYER_UART_PORT,
        .baudrate = CONFIG_VSP_VOICE_PLAYER_UART_BAUDRATE,
        .magic = CONFIG_VSP_VOICE_PLAYER_UART_MAGIC
    };

    UartMessageAsyncInit(&uart_msg_init_config);

    UART_MSG_REGIST msg_regist_1, msg_regist_2, msg_regist_3;
    msg_regist_1.msg_id             = MSG_NTF_PCM_DATA;
    msg_regist_1.port               = uart_msg_init_config.port;
    msg_regist_1.msg_buffer         = s_wav_buff;
    msg_regist_1.msg_buffer_length  = sizeof(s_wav_buff);
    msg_regist_1.msg_buffer_offset  = 0;
    msg_regist_1.msg_pack_callback  = _MsgPackCallback_UartPlayPcm;
    msg_regist_1.priv               = "MSG_NTF_PCM_DATA";
    UartMessageAsyncRegist(&msg_regist_1);
    msg_regist_2.msg_id             = MSG_NTF_PCM_START;
    msg_regist_2.port               = uart_msg_init_config.port;
    msg_regist_2.msg_buffer         = s_wav_buff;
    msg_regist_2.msg_buffer_length  = sizeof(s_wav_buff);
    msg_regist_2.msg_buffer_offset  = 0;
    msg_regist_2.msg_pack_callback  = _MsgPackCallback_UartPlayPcm;
    msg_regist_2.priv               = "MSG_NTF_PCM_START";
    UartMessageAsyncRegist(&msg_regist_2);
    msg_regist_3.msg_id             = MSG_NTF_PCM_DONE;
    msg_regist_3.port               = uart_msg_init_config.port;
    msg_regist_3.msg_buffer         = s_wav_buff;
    msg_regist_3.msg_buffer_length  = sizeof(s_wav_buff);
    msg_regist_3.msg_buffer_offset  = 0;
    msg_regist_3.msg_pack_callback  = _MsgPackCallback_UartPlayPcm;
    msg_regist_3.priv               = "MSG_NTF_PCM_DONE";
    UartMessageAsyncRegist(&msg_regist_3);
#endif

    return 0;
}

int VspVoicePlayerDone(VSP_VOICE_PLAYER_INIT_CONFIG *config)
{
#ifdef CONFIG_VSP_ENABLE_UART_WAV_PLAYER
    _FlowControl = NULL;
    s_flow_control_priv = NULL;


    UART_MSG_REGIST msg_regist_1, msg_regist_2, msg_regist_3;
    msg_regist_1.msg_id             = MSG_NTF_PCM_DATA;
    msg_regist_1.port               = CONFIG_VSP_VOICE_PLAYER_UART_PORT;
    msg_regist_1.msg_buffer         = s_wav_buff;
    msg_regist_1.msg_buffer_length  = sizeof(s_wav_buff);
    msg_regist_1.msg_buffer_offset  = 0;
    msg_regist_1.msg_pack_callback  = _MsgPackCallback_UartPlayPcm;
    msg_regist_1.priv               = "MSG_NTF_PCM_DATA";
    UartMessageAsyncLogout(&msg_regist_1);
    msg_regist_2.msg_id             = MSG_NTF_PCM_START;
    msg_regist_2.port               = CONFIG_VSP_VOICE_PLAYER_UART_PORT;
    msg_regist_2.msg_buffer         = s_wav_buff;
    msg_regist_2.msg_buffer_length  = sizeof(s_wav_buff);
    msg_regist_2.msg_buffer_offset  = 0;
    msg_regist_2.msg_pack_callback  = _MsgPackCallback_UartPlayPcm;
    msg_regist_2.priv               = "MSG_NTF_PCM_START";
    UartMessageAsyncLogout(&msg_regist_2);
    msg_regist_3.msg_id             = MSG_NTF_PCM_DONE;
    msg_regist_3.port               = CONFIG_VSP_VOICE_PLAYER_UART_PORT;
    msg_regist_3.msg_buffer         = s_wav_buff;
    msg_regist_3.msg_buffer_length  = sizeof(s_wav_buff);
    msg_regist_3.msg_buffer_offset  = 0;
    msg_regist_3.msg_pack_callback  = _MsgPackCallback_UartPlayPcm;
    msg_regist_3.priv               = "MSG_NTF_PCM_DONE";
    UartMessageAsyncLogout(&msg_regist_3);
#endif

    return 0;
}

int VspVoicePlayerSetVolume(int volume)
{
    s_player_handle.volume = volume;
    return volume;
}

int VspVoicePlayerGetVolume(void)
{
    return s_player_handle.volume;
}

int VspVoicePlayerMute(unsigned int mute)
{
    s_player_handle.mute = mute;
    return mute;
}

int VspVoicePlayerPlay(int offset, unsigned int size, PLC_RESURCE_TYPE type)
{
    if (type == PRT_WAV) {
#if (defined CONFIG_VSP_ENABLE_WAV_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
        return _VspVoicePlayerProcWav(offset, size);
#else
        return 0;
#endif
    } else if (type == PRT_MP3) {
#if (defined CONFIG_VSP_ENABLE_MP3_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
        return _VspVoicePlayerProcMp3(offset, size);
#else
        return 0;
#endif
    } else {
        return -1;
    }
}

int VspVoicePlayerTick(void *priv)
{
#if (defined CONFIG_VSP_ENABLE_WAV_VOICE_PLAYER || defined CONFIG_VSP_ENABLE_ALL_TYPES_VOICE_PLAYER)
    // Play audio by frame
    _VspVoicePlayerPlayWavByFrame();
#endif

#ifdef CONFIG_VSP_ENABLE_UART_WAV_PLAYER
    UartMessageAsyncTick();

    if (s_uart_playing == 1 && s_uart_data_ready == 0 && s_flow_control_ready == 0) {
        if (_FlowControl != NULL) {
            s_flow_control_ready = _FlowControl(s_flow_control_priv);
        } else {
            MSG_PACK msg_pack;
            msg_pack.msg_header.magic     = CONFIG_VSP_VOICE_PLAYER_UART_MAGIC;
            msg_pack.msg_header.cmd       = MSG_NTF_PCM_READY;
            msg_pack.msg_header.flags     = 0;
            msg_pack.msg_header.length    = 0;
            msg_pack.body_addr            = NULL;
            msg_pack.port                 = CONFIG_VSP_VOICE_PLAYER_UART_PORT;
            msg_pack.len                  = 0;
            UartMessageAsyncSend(&msg_pack);
            s_flow_control_ready = 1;
        }
    }
#endif
    return 0;
}



