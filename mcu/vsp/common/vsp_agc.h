/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_agc.h:
 */


#ifndef __VSP_AGC_H__
#define __VSP_AGC_H__


typedef struct {
    unsigned char mic_gain;
    unsigned char ref_gain;
} AGC_CTRL;

typedef struct {
    AGC_CTRL curr_agc_ctrl;
    AGC_CTRL prev_agc_ctrl;
    struct {
        unsigned int smooth_threshold; // 增大增益平滑门限：本意不想立即增大增益，增加一个 count 来做个平滑后再提升 
        unsigned int smooth_count;     // 计数器
    } agc_smooth;
    unsigned int gain_width;
} VSP_AGC_HANDLE;

unsigned char VspAgcGetMicGain(void);
unsigned char VspAgcGetRefGain(void);

int VspAudioInInitAgcConfig(AUDIO_IN_AGC_CONFIG *agc_handle);
int VspInitGainControl(VSP_AGC_HANDLE *acg_config);
int VspAudioInAgcAdjustGain(void);
int VspAudionInSetAgcSmoothParam(unsigned int threshold);


#endif //__VSP_AGC_H__
