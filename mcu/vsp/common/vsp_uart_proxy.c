/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_uart_proxy.c :
 *
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>
#include <common.h>

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/audio_mic.h>
#include <driver/spi_flash.h>
#include <driver/delay.h>
#include <driver/uart.h>
#include <driver/watchdog.h>
#include <driver/dsp.h>
#include <driver/timer.h>

#include "uart_message_v2.h"

#include "vsp_uart_proxy.h"

#ifdef CONFIG_PROXY_SERIAL_BAUD_RATE
#define PROXY_BAUDRATE CONFIG_PROXY_SERIAL_BAUD_RATE
#else
#define PROXY_BAUDRATE CONFIG_SERIAL_BAUD_RATE
#endif

#define LOG_TAG "[VSP_DOWNLOD_FROM_UART]"

#define PROXY_DTAT_END_MAGIC ("yxorp")
#define PROXY_DTAT_END_MAGIC_LENGTH sizeof(PROXY_DTAT_END_MAGIC)

typedef enum {
    VPP_PART_1 = 1,  // user.json
    VPP_PART_2 = 2,  // vpa.bin
    VPP_PART_3 = 3,  // behavior.bin
    VPP_PART_4 = 4,  // resource.bin
} VSP_PROXY_PART;

typedef struct {
    unsigned char part;
    unsigned char rw;
    unsigned reserved:16;
    unsigned int offset;
    unsigned int size;
} VSP_PROXY_INFO;

typedef struct {
    unsigned int serial_baudRate;
} VSP_SET_BAUDRATE;

typedef enum {
    VPO_REBOOT = 0x1,
    VPO_ASK_BAUDRATE = 0x2,
    VPO_HELLO = 0x3,
} VSP_PROXY_OPERATION;

static VSP_PROXY_OPERATION s_operation;

union {
    VSP_PROXY_INFO proxy_info;
    unsigned char pack_ready_start_data_body_buffer[sizeof(VSP_PROXY_INFO) + 4];
} s_pack_ready_start_body;

static unsigned int s_start_ms = 0;

static MSG_PACK send_pack_ready = {
    .port = CONFIG_SERIAL_PORT,
    .msg_header.magic = MSG_HOST_MAGIC,
    .msg_header.cmd = MSG_NTF_UART_PROXY_READY,
    .msg_header.flags = 1,
    .body_addr = (unsigned char *)&s_pack_ready_start_body.proxy_info,
    .len = sizeof(VSP_PROXY_INFO)
};

static unsigned char send_pack_data_buffer[512] = {0};
static MSG_PACK send_pack_data = {
    .port = CONFIG_SERIAL_PORT,
    .msg_header.magic = MSG_HOST_MAGIC,
    .msg_header.cmd = MSG_NTF_UART_PROXY_DATA,
    .msg_header.flags = 1,
    .body_addr = (unsigned char *)send_pack_data_buffer,
    .len = sizeof(send_pack_data_buffer)
};

static unsigned int s_work_timeout = 3000;
static unsigned int s_timeout_ms = 100;
static unsigned int s_high_baudreta_timeout_ms = 50;
static unsigned int s_get_size = 0;
static unsigned int s_send_size = 0;
static unsigned int need_recv_size = 0;

static unsigned int s_timer_timeout_ms = 1000;
static int _TimeCallback(void *private_data) {
    return UartMessageAsyncSend((MSG_PACK *)private_data);
}

static int _uartRecvCallback(MSG_PACK * pack, void *priv)
{
    TimerUnregisterCallback(_TimeCallback);

    s_high_baudreta_timeout_ms = s_work_timeout;

    if (pack->msg_header.cmd == MSG_NTF_UART_PROXY_OPERATION) {

        static int proxy_done_count = 0;
        proxy_done_count++;

        switch(s_operation) {
            case VPO_REBOOT: {
                WatchdogInit(1, 1, NULL);
                break;
            }
            case VPO_ASK_BAUDRATE: {
                VSP_SET_BAUDRATE set_baudrate = {
                    .serial_baudRate = PROXY_BAUDRATE,
                };

                MSG_PACK send_pack = {
                    .port = CONFIG_SERIAL_PORT,
                    .msg_header.magic = MSG_HOST_MAGIC,
                    .msg_header.cmd = MSG_NTF_SET_BAUDRATE,
                    .msg_header.flags = 1,
                    .body_addr = (unsigned char *)&set_baudrate,
                    .len = sizeof(set_baudrate)
                };
                TimerRegisterCallback(_TimeCallback, (void*)&send_pack, s_timer_timeout_ms);

                mdelay(5);

                UART_MSG_INIT_CONFIG init_config = {
                    .magic = MSG_HOST_MAGIC,
                    .port = CONFIG_SERIAL_PORT,
                    .baudrate = PROXY_BAUDRATE,
                    .reinit_flag = 1
                };
                UartMessageAsyncInit(&init_config);

                WatchdogInit(10000, 1, NULL);
                break;
            }
            case VPO_HELLO: {
                WatchdogDone();
                s_pack_ready_start_body.proxy_info.offset = 0xffffff;
                TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);
                break;
            }
            default :
                break;
        }

        return 0;
    }

    if (pack->msg_header.cmd == MSG_NTF_UART_PROXY_START &&
        pack->len == sizeof(VSP_PROXY_INFO)) {

        s_get_size = 0;
        s_send_size = 0;

        if (SpiFlashInitPure(NULL, 0) != 0) {
            printf(LOG_TAG"Flash init error!\n");
            return 1;
        }

        if (s_pack_ready_start_body.proxy_info.rw == 0) { // write
            s_pack_ready_start_body.proxy_info.size = s_pack_ready_start_body.proxy_info.part == VPP_PART_1 ? (s_pack_ready_start_body.proxy_info.size <= VPS_PART_1 ? s_pack_ready_start_body.proxy_info.size : 0) : \
                                s_pack_ready_start_body.proxy_info.part == VPP_PART_2 ? (s_pack_ready_start_body.proxy_info.size <= VPS_PART_2 ? s_pack_ready_start_body.proxy_info.size : 0) : \
                                s_pack_ready_start_body.proxy_info.part == VPP_PART_3 ? (s_pack_ready_start_body.proxy_info.size <= VPS_PART_3 ? s_pack_ready_start_body.proxy_info.size : 0) : \
                                s_pack_ready_start_body.proxy_info.part == VPP_PART_4 ? (s_pack_ready_start_body.proxy_info.size <= VPS_PART_4 ? s_pack_ready_start_body.proxy_info.size : 0) : 0;
            need_recv_size = s_pack_ready_start_body.proxy_info.size;
            if (s_pack_ready_start_body.proxy_info.size != 0 && s_pack_ready_start_body.proxy_info.part == VPP_PART_1 )
                 SpiFlashErase(PROXY_OFFSET, FLASH_TOTAL_SIZE - FLASH_BLOCK_SIZE - PROXY_OFFSET);
        } else { //read
            s_pack_ready_start_body.proxy_info.size = s_pack_ready_start_body.proxy_info.part == VPP_PART_1 ? VPS_PART_1 : \
                                s_pack_ready_start_body.proxy_info.part == VPP_PART_2 ? VPS_PART_2 : \
                                s_pack_ready_start_body.proxy_info.part == VPP_PART_3 ? VPS_PART_3 : \
                                s_pack_ready_start_body.proxy_info.part == VPP_PART_4 ? VPS_PART_4 : 0;
            s_pack_ready_start_body.proxy_info.offset = 0;
        }

        TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);
        s_timeout_ms = s_work_timeout;
        s_start_ms = get_time_ms();
        return 0;
    }

    if (pack->msg_header.cmd == MSG_NTF_UART_PROXY_DATA) {

        unsigned int write_offset = s_pack_ready_start_body.proxy_info.part == VPP_PART_1 ? (s_pack_ready_start_body.proxy_info.offset + pack->len <= VPS_PART_1 - PROXY_DTAT_END_MAGIC_LENGTH ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_1 : 0) : \
                                    s_pack_ready_start_body.proxy_info.part == VPP_PART_2 ? (s_pack_ready_start_body.proxy_info.offset + pack->len <= VPS_PART_2 - PROXY_DTAT_END_MAGIC_LENGTH ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_2 : 0) : \
                                    s_pack_ready_start_body.proxy_info.part == VPP_PART_3 ? (s_pack_ready_start_body.proxy_info.offset + pack->len <= VPS_PART_3 - PROXY_DTAT_END_MAGIC_LENGTH ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_3 : 0) : \
                                    s_pack_ready_start_body.proxy_info.part == VPP_PART_4 ? (s_pack_ready_start_body.proxy_info.offset + pack->len <= VPS_PART_4 - PROXY_DTAT_END_MAGIC_LENGTH ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_4 : 0) : 0;

        if (write_offset == 0 || s_pack_ready_start_body.proxy_info.offset != s_get_size) {
            s_pack_ready_start_body.proxy_info.offset = 0xffffff;
            TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);
            return -1;
        }
        if (SpiFlashInitPure(NULL, 0) != 0) {
            printf(LOG_TAG"Flash init error!\n");
            return 1;
        }

        SpiFlashWrite(write_offset, (unsigned char *)(pack->body_addr), pack->len);
        s_pack_ready_start_body.proxy_info.offset += pack->len;
        s_get_size += pack->len;

        if (s_pack_ready_start_body.proxy_info.offset >= need_recv_size) {
            SpiFlashWrite(write_offset + pack->len, (unsigned char *)(PROXY_DTAT_END_MAGIC), PROXY_DTAT_END_MAGIC_LENGTH);
        }

        TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);
        s_timeout_ms = s_work_timeout;
        s_start_ms = get_time_ms();
        return 0;
    }

    if (pack->msg_header.cmd == MSG_NTF_UART_PROXY_READY) {
        unsigned int read_offset = s_pack_ready_start_body.proxy_info.part == VPP_PART_1 ? (s_pack_ready_start_body.proxy_info.offset < VPS_PART_1 ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_1 : 0) : \
                                   s_pack_ready_start_body.proxy_info.part == VPP_PART_2 ? (s_pack_ready_start_body.proxy_info.offset < VPS_PART_2 ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_2 : 0) : \
                                   s_pack_ready_start_body.proxy_info.part == VPP_PART_3 ? (s_pack_ready_start_body.proxy_info.offset < VPS_PART_3 ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_3 : 0) : \
                                   s_pack_ready_start_body.proxy_info.part == VPP_PART_4 ? (s_pack_ready_start_body.proxy_info.offset < VPS_PART_4 ? s_pack_ready_start_body.proxy_info.offset + VPB_PART_4 : 0) : 0;

        if (read_offset == 0 || s_pack_ready_start_body.proxy_info.offset != s_send_size) {
            s_pack_ready_start_body.proxy_info.offset = 0xffffff;
            s_send_size = 0;
            TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);
            return -1;
        }
        if (SpiFlashInitPure(NULL, 0) != 0) {
            printf(LOG_TAG"Flash init error!\n");
            return 1;
        }

        unsigned int leftover_size = s_pack_ready_start_body.proxy_info.part == VPP_PART_1 ? VPS_PART_1 - read_offset : \
                                 s_pack_ready_start_body.proxy_info.part == VPP_PART_2 ? VPS_PART_2 - read_offset : \
                                 s_pack_ready_start_body.proxy_info.part == VPP_PART_3 ? VPS_PART_3 - read_offset : \
                                 s_pack_ready_start_body.proxy_info.part == VPP_PART_4 ? VPS_PART_4 - read_offset : 0;
        unsigned int read_size = leftover_size > sizeof(send_pack_data_buffer) ? sizeof(send_pack_data_buffer) : leftover_size;

        SpiFlashReadPure(read_offset, send_pack_data.body_addr, read_size);
        send_pack_data.len = read_size;
        s_send_size += read_size;
        TimerRegisterCallback(_TimeCallback, (void*)&send_pack_data, s_timer_timeout_ms);

        s_timeout_ms = s_work_timeout;
        s_start_ms = get_time_ms();
        return 0;
    }

    return 0;
}


int VspUartProxy(void)
{
    UartMessageAsyncDone();
    UART_MSG_INIT_CONFIG init_config = {
        .magic = MSG_HOST_MAGIC,
        .port = CONFIG_SERIAL_PORT,
        .baudrate = PROXY_BAUDRATE,
        .reinit_flag = 1
    };
    UartMessageAsyncInit(&init_config);


    UART_MSG_REGIST regist_info_1 = {
        .msg_id = MSG_NTF_UART_PROXY_START,
        .port = init_config.port,
        .msg_buffer = (unsigned char *)&s_pack_ready_start_body.proxy_info,
        .msg_buffer_length = sizeof(s_pack_ready_start_body),
        .msg_pack_callback = _uartRecvCallback,
        .priv = "start"

    };
    UartMessageAsyncRegist(&regist_info_1);


    unsigned char buffer[512 + 4] = {0};
    UART_MSG_REGIST regist_info_2 = {
        .msg_id = MSG_NTF_UART_PROXY_DATA,
        .port = init_config.port,
        .msg_buffer = (unsigned char *)&buffer[0],
        .msg_buffer_length = sizeof(buffer),
        .msg_pack_callback = _uartRecvCallback,
        .priv = "data"

    };
    UartMessageAsyncRegist(&regist_info_2);

    UART_MSG_REGIST regist_info_3 = {
        .msg_id = MSG_NTF_UART_PROXY_READY,
        .port = init_config.port,
        .msg_buffer = (unsigned char *)&s_pack_ready_start_body.proxy_info,
        .msg_buffer_length = sizeof(s_pack_ready_start_body),
        .msg_pack_callback = _uartRecvCallback,
        .priv = "ready"

    };
    UartMessageAsyncRegist(&regist_info_3);

    UART_MSG_REGIST regist_info_4 = {
        .msg_id = MSG_NTF_UART_PROXY_OPERATION,
        .port = init_config.port,
        .msg_buffer = (unsigned char *)&s_operation,
        .msg_buffer_length = sizeof(s_operation),
        .msg_pack_callback = _uartRecvCallback,
        .priv = "done"

    };
    UartMessageAsyncRegist(&regist_info_4);

    if (SpiFlashInitPure(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return 1;
    }

    s_pack_ready_start_body.proxy_info.offset = 0xffffff;
    TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);

    s_start_ms =  get_time_ms();

    while(UartMessageAsyncTick() != -233) {
        if ((get_time_ms() - s_start_ms) > s_high_baudreta_timeout_ms) {
            static int once = 1;
            if (once) {
                once = 0;
                UART_MSG_INIT_CONFIG init_config = {
                    .magic = MSG_HOST_MAGIC,
                    .port = CONFIG_SERIAL_PORT,
                    .baudrate = CONFIG_SERIAL_BAUD_RATE,
                    .reinit_flag = 1
                };
                UartMessageAsyncInit(&init_config);

                TimerRegisterCallback(_TimeCallback, (void*)&send_pack_ready, s_timer_timeout_ms);
            }
        }

        if ((get_time_ms() - s_start_ms) > s_timeout_ms) {
            UART_MSG_INIT_CONFIG init_config = {
                .magic = MSG_HOST_MAGIC,
                .port = CONFIG_SERIAL_PORT,
                .baudrate = CONFIG_SERIAL_BAUD_RATE,
                .reinit_flag = 1
            };
            UartMessageAsyncInit(&init_config);
            TimerUnregisterCallback(_TimeCallback);
            break;
        }
    };

    return 0;
}




