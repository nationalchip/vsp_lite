/* Voice Signal Preprocess
 * Copyright (C) 1991-2019 Nationalchip Co., Ltd
 *
 * vsp_queue.h: a Circular Queue using array
 *
 */

#ifndef __VSP_QUEUE_H__
#define __VSP_QUEUE_H__

#define __VSP_QUEUE_SUPPORT_BUFFER__

typedef struct {
    int tail;      // next write index
    int head;      // next read index, if back == front, the queue is empty
    unsigned char *buffer;
    int size;
    int member_size;
} VSP_QUEUE;

void VspQueueInit(VSP_QUEUE *queue, unsigned char *buffer, const int size, int member_size);
int VspQueuePut(VSP_QUEUE *queue, const unsigned char *value);
int VspQueueGet(VSP_QUEUE *queue, unsigned char *value);
int VspQueueGetCapacity(VSP_QUEUE *queue);
int VspQueueIsEmpty(VSP_QUEUE *queue);
int VspQueueIsFull(VSP_QUEUE *queue);

#ifdef __VSP_QUEUE_SUPPORT_BUFFER__

int VspQueuePutBuffer(VSP_QUEUE *queue, const unsigned int *buffer, const int size);
int VspQueueGetBuffer(VSP_QUEUE *queue, unsigned int *buffer, const int size);
int VspQueuePeekBuffer(VSP_QUEUE *queue, unsigned int *buffer, const int size);

int VspQueueGetFreeSize(VSP_QUEUE *queue);
int VspQueueGetDataSize(VSP_QUEUE *queue);

#endif

#endif /* __VSP_QUEUE_H__ */
