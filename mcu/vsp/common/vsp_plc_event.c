/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_plc_event.c:
 */

#include <autoconf.h>

#include <stdio.h>
#include <string.h>

#include <board_config.h>

#include <driver/audio_out.h>
#include <driver/dsp.h>
#include <driver/delay.h>
#include <driver/spi_flash.h>
#include <driver/misc.h>
#include <driver/gpio.h>
#include <driver/irr.h>
#include <driver/clock.h>
#include <driver/uart.h>

#include "vsp_voice_player.h"
#include "vsp_queue.h"
#include "vsp_plc_event.h"

#include <vsp_context.h>

#include "vsp_uart_proxy.h"
#include "../hook/vsp_hook.h"

#define LOG_TAG "[PLC_EVENT]"

//=================================================================================================


#define VSP_PLC_MISC_QUEUE_LEN 16
static unsigned char s_plc_misc_event_queue_buffer[VSP_PLC_MISC_QUEUE_LEN * sizeof(PLC_EVENT)] = {0};
VSP_QUEUE s_plc_misc_event_queue;

#ifdef CONFIG_VSP_PLC_VOICE_PROCESS_ENABLE
#define VSP_PLC_VOICE_PLAYER_QUEUE_LEN 8
static unsigned char s_plc_voice_player_event_queue_buffer[VSP_PLC_VOICE_PLAYER_QUEUE_LEN * sizeof(PLC_EVENT)] = {0};
VSP_QUEUE s_plc_voice_player_event_queue;
#endif

#ifdef CONFIG_VSP_PLC_LOAD_EVENT_BEHAVIOR_CONFIG_ENABLE
#define VSP_PLC_LOAD_EVENT_BEHAVIOR_CONFIG_FROM VPB_PART_3
#define VSP_PLC_RESOURCE_OFFSET_BASE VPB_PART_4


typedef struct {
    PLC_RESURCE_TYPE    resource_tpye;
    unsigned int        resource_size;
    unsigned int        resource_offset;
} PLC_RESURCE_INFO;

typedef struct {
    unsigned int        event_id;
    unsigned int        behavior_type;
    PLC_RESURCE_INFO    resource[10];
    unsigned int        start_event_id;
    unsigned int        end_event_id;
} PLC_EVENT_BEHAVIOR;

PLC_EVENT_BEHAVIOR s_plc_event_behavior[CONFIG_VSP_PLC_EVENT_REGIST_NUM];

//=================================================================================================
//VoicePlayer
static int _PlcLoadConfig(void)
{
    unsigned int offset = VSP_PLC_LOAD_EVENT_BEHAVIOR_CONFIG_FROM;
    if (SpiFlashInit(NULL, 0) != 0) {
        printf(LOG_TAG"Flash init error!\n");
        return -1;
    }
    int result = SpiFlashRead(offset, (unsigned char *)(s_plc_event_behavior), sizeof(s_plc_event_behavior));
    return result;
}
#endif

#ifdef CONFIG_VSP_PLC_VOICE_PROCESS_ENABLE
int _PlcEventProcVoicePlayer(unsigned int event_id)
{
    for (int i = 0; i < CONFIG_VSP_PLC_EVENT_REGIST_NUM; i++) {
        if (s_plc_event_behavior[i].event_id == event_id) {
            if (s_plc_event_behavior[i].start_event_id != 0) {
                PLC_EVENT event = {.event_id = s_plc_event_behavior[i].start_event_id};
                VspTriggerPlcEvent(event);
            }

            int resource_count = 0;
            for (;resource_count < 10; resource_count++) {
                if (s_plc_event_behavior[i].resource[resource_count].resource_tpye == PRT_UNDEFINE) break;
            }

            if (resource_count == 0) break;

            unsigned int random = get_time_us() % resource_count;
            VspVoicePlayerPlay(VSP_PLC_RESOURCE_OFFSET_BASE + s_plc_event_behavior[i].resource[random].resource_offset, \
                               s_plc_event_behavior[i].resource[random].resource_size,
                               s_plc_event_behavior[i].resource[random].resource_tpye);

            break;
        }
    }

    return 0;
}
#endif

//=================================================================================================
static PLC_STATUS s_plc_status;

int VspGetPlcStatus(PLC_STATUS_TYPE status_type, PLC_STATUS *plc_status_result)
{
    if(status_type & PST_SWITCH_VPA_MODE) {
        plc_status_result->vpa_mode = s_plc_status.vpa_mode;
    }
    if(status_type & PST_NEW_VOICE_PLAY_TASK) {
        plc_status_result->new_voice_play_task = s_plc_status.new_voice_play_task;
    }

    return 0;
}

int VspSetPlcStatus(PLC_STATUS_TYPE status_type, PLC_STATUS *plc_status_result)
{
    if(status_type & PST_SWITCH_VPA_MODE) {
        s_plc_status.vpa_mode = plc_status_result->vpa_mode;
    }
    if(status_type & PST_NEW_VOICE_PLAY_TASK) {
        s_plc_status.new_voice_play_task = plc_status_result->new_voice_play_task;
    }

    return 0;
}

//=================================================================================================
int VspTriggerPlcEvent(PLC_EVENT plc_event)
{
//    if (plc_event.event_id > 99)
//        printf(LOG_TAG"event_id = %d\n", plc_event.event_id);

#ifndef CONFIG_VSP_PLC_ENABLE_SYS_EVENT
    if (plc_event.event_id < 100) {
        return -1;
    }
#endif

#ifdef CONFIG_VSP_PLC_LOAD_EVENT_BEHAVIOR_CONFIG_ENABLE
    for (int i = 0; i < CONFIG_VSP_PLC_EVENT_REGIST_NUM; i++) {
        if (s_plc_event_behavior[i].event_id == plc_event.event_id) {
# ifdef CONFIG_VSP_PLC_VOICE_PROCESS_ENABLE
            if (s_plc_event_behavior[i].behavior_type & PBT_VOICE_PLAY) {
                VspQueuePut(&s_plc_voice_player_event_queue, (const unsigned char *)&plc_event);
                PLC_STATUS plc_status_result = {.new_voice_play_task = 1};
                VspSetPlcStatus(PST_NEW_VOICE_PLAY_TASK, &plc_status_result);
            }
# endif

            if (s_plc_event_behavior[i].behavior_type & PBT_OTHER)
                VspQueuePut(&s_plc_misc_event_queue, (const unsigned char *)&plc_event);

            break;
        }

        if (i == CONFIG_VSP_PLC_EVENT_REGIST_NUM - 1)
            VspQueuePut(&s_plc_misc_event_queue, (const unsigned char *)&plc_event);
    }
#else
    VspQueuePut(&s_plc_misc_event_queue, (const unsigned char *)&plc_event);
#endif

    return 0;
}

int VspInitializePlcEvent(void)
{
#ifdef CONFIG_VSP_PLC_LOAD_EVENT_BEHAVIOR_CONFIG_ENABLE
    _PlcLoadConfig();
#endif

#ifdef CONFIG_VSP_PLC_VOICE_PROCESS_ENABLE
    VspQueueInit(&s_plc_voice_player_event_queue, s_plc_voice_player_event_queue_buffer, VSP_PLC_VOICE_PLAYER_QUEUE_LEN * sizeof(PLC_EVENT), sizeof(PLC_EVENT));
    VspVoicePlayerInit(NULL);
#endif

    VspQueueInit(&s_plc_misc_event_queue, s_plc_misc_event_queue_buffer, VSP_PLC_MISC_QUEUE_LEN * sizeof(PLC_EVENT), sizeof(PLC_EVENT));
    HookProcessInit();

    return 0;
}

void VspPlcEventTick(void)
{
#ifdef CONFIG_VSP_PLC_VOICE_PROCESS_ENABLE
    // Play audio by frame
    VspVoicePlayerTick(NULL);
#endif

    HookProcessTick();

    PLC_EVENT plc_event = {.event_id = 0};
#ifdef CONFIG_VSP_PLC_VOICE_PROCESS_ENABLE
    if (VspQueueGet(&s_plc_voice_player_event_queue, (unsigned char *)&plc_event))
        _PlcEventProcVoicePlayer(plc_event.event_id);
#endif

    if (VspQueueGet(&s_plc_misc_event_queue, (unsigned char *)&plc_event))
        HookEventResponse(plc_event);
}

