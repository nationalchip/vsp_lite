/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_uart_proxy.h:
 *
 */

#ifndef __VSP_UART_PROXYT_H__
#define __VSP_UART_PROXYT_H__

#ifdef CONFIG_VSP_UART_PROXY_ENABLE

#include "uart_message_v2.h"

#ifndef FLASH_BLOCK_SIZE
#define FLASH_BLOCK_SIZE (64 * 1024)
#endif

#ifndef FLASH_TOTAL_SIZE
#define FLASH_TOTAL_SIZE 0x200000
#endif

typedef enum {
    VPS_PART_1 = 0x5000,  // user.json
    VPS_PART_2 = 0x5000,  // vpa.bin
    VPS_PART_3 = 0x5000,  // behavior.bin
    VPS_PART_4 = FLASH_TOTAL_SIZE - FLASH_BLOCK_SIZE - PROXY_OFFSET - VPS_PART_1 - VPS_PART_2 - VPS_PART_3, // resource.bin
} VSP_PROXY_SIZE;

typedef enum {
    VPB_PART_1 = PROXY_OFFSET,               // user.json
    VPB_PART_2 = VPB_PART_1 + VPS_PART_1,   // vpa.bin
    VPB_PART_3 = VPB_PART_2 + VPS_PART_2,   // behavior.bin
    VPB_PART_4 = VPB_PART_3 + VPS_PART_3,   // resource.bin
} VSP_PROXY_BASE; // offset of flash

int VspUartProxy(void);
#endif

#endif /* __VSP_DOWNLOD_FROM_UART_H__ */
