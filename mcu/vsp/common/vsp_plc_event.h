/* Voice Signal Preprocess
 * Copyright (C) 2001-2020 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED!
 *
 * vsp_plc_event.h:
 */
#ifndef __VSP_PLC_EVENT_H__
#define __VSP_PLC_EVENT_H__

typedef struct {
    unsigned int event_id;
    union {
        unsigned int ctx_index;
        unsigned int uac_volume;
    };
} PLC_EVENT;

// Private Event ID [1~99] for PLC
#define VSP_WAKE_UP_EVENT_ID            (90)
#define ESR_GOODBYE_EVENT_ID            (99)  // It defined in the plc_json
#define DSP_PROCESS_DONE_EVENT_ID       (98)  // Used to notify board to send wav to bluetooth
#define AUDIO_IN_RECORD_DONE_EVENT_ID   (97)

#define UAC_DOWN_STREAM_OFF             (80)
#define UAC_DOWN_STREAM_ON              (81)
#define UAC_DOWN_SET_VOLUME             (82)
#define UAC_UP_STREAM_OFF               (83)

//=============================================================================//
typedef enum {
    PBT_UNDEFINE        = 0x0,
    PBT_OTHER           = 0x1,
    PBT_VOICE_PLAY      = 0x2,
    PBT_UART_ASYNC_SEND = 0x4,
    PBT_LED_CONTRL      = 0x8,
    PBT_ENABLE_UART_SEND_WAV = 0xa,
} PLC_BEHAVIOR_TYPE;

typedef enum {
    PRT_UNDEFINE,
    PRT_WAV,
    PRT_MP3,
    PRT_BIN,
} PLC_RESURCE_TYPE;

//=============================================================================//
typedef enum {
    PST_SWITCH_VPA_MODE = 1,
    PST_NEW_VOICE_PLAY_TASK = 2,
} PLC_STATUS_TYPE;

typedef struct {
    enum {
        PLC_STATUS_VPA_ACTIVE,
        PLC_STATUS_VPA_TALKING,
        PLC_STATUS_VPA_BYPASS,
        PLC_STATUS_VPA_IDLE,
        PLC_STATUS_VPA_TEST
    } vpa_mode;
    unsigned char new_voice_play_task;
} PLC_STATUS;


int VspInitializePlcEvent(void);
int VspTriggerPlcEvent(PLC_EVENT plc_event);
void VspPlcEventTick(void);
int VspGetPlcStatus(PLC_STATUS_TYPE status_type, PLC_STATUS *plc_status_data);
int VspSetPlcStatus(PLC_STATUS_TYPE status_type, PLC_STATUS *plc_status_data);

#endif /* __VSP_PLC_EVENT_H__ */
