/* Voice Signal Preprocess
 * Copyright (C) 1991-2017 Nationalchip Co., Ltd
 * All Rights Reserved!
 *
 * vsp_led_player.h: VSP LED Player
 *
 */

#ifndef __VSP_LED_PLAYER_H__
#define __VSP_LED_PLAYER_H__

#include <vsp_led.h>

typedef enum {
    VSP_LED_MODE_MASTER,                    // play with a buffer with speicifed parameters
    VSP_LED_MODE_SLAVE,                     // play with a queue from CPU
} VSP_LED_MODE;

typedef int (*VSP_LED_FRAME_DONE_CB)(unsigned int index, void *priv_data);
typedef int (*VSP_LED_TASK_DONE_CB)(void *priv_data);

typedef struct {
    VSP_LED_MODE            mode;
    VSP_LED_FRAME_DONE_CB   frame_done;
    VSP_LED_TASK_DONE_CB    task_done;
    void                   *priv_data;
    // for MASTER mode
    void                   *frames;
    unsigned                origin:8;       // the origin point
    unsigned                loop_times:8;   // loop times (1 - 255), if 0 infinite loop
    unsigned                frame_num:8;    // frame number (1 - 255)
    unsigned int            pixel_num:8;    // pixel number (1 - 255)
} VSP_LED_TASK;

/* the function accepts the MCU address */
int VspLedSetCpuQueue(VSP_LED_QUEUE *queue, void *frame);

/* Initialize the LED ring device */
int VspLedInit(void);

/* Play a task */
int VspLedPlay(const VSP_LED_TASK *task);
int VspLedStop(void);

int VspLedClear(void);

/* Flush LED data to device */
int VspLedFlush(void);

#endif  /* __VSP_LED_PLAYER_H__ */
