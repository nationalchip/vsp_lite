/* Voice Signal Preprocess
 * Copyright (C) 2001-2019 NationalChip Co., Ltd
 * ALL RIGHTS RESERVED
 *
 * vsp_uac_fifo_manager: UAC FIFO Manager
 *
 */

#include <stdio.h>
#include <common.h>
#include <string.h>

#include <driver/vsp_uac_fifo_manager.h>

#include <driver/delay.h>

#define LOG_TAG "[UAC/FIFO] "
#define PRINT(format, ...) printf(LOG_TAG format, ##__VA_ARGS__)
#define AOUT_MAX_READ_LEN 3072

#ifdef CONFIG_VSP_UAC_CAPTURE_ADVANCED_CONTROL
#define START_GATE CONFIG_VSP_UAC_CAPTURE_START_CONTEXT
#else
#define START_GATE (AIN_BUFFER_NUM / 2)
#endif

typedef struct {
    unsigned int addr;
    unsigned int out_buff_addr;
    unsigned int out_buff_size;
    unsigned int total_size;
} CONTEXT_INFO;

typedef struct {
    CONTEXT_INFO ctx[AIN_BUFFER_NUM];
    unsigned int total_buff_size;

    unsigned int rd_seg;
    unsigned int wr_seg;
    unsigned int virt_wr_seg;
    unsigned int used_seg;

    unsigned int wr_ptr;
    unsigned int rd_ptr;

    bool ain_start;
    bool ain_first_read;
    bool need_recover;
} BUFF_MANAGE;

static BUFF_MANAGE bm;
static unsigned int ain_buff_used_size;
static unsigned int uac_us_fifo_overflow, uac_us_fifo_underflow; // us : upstream

static inline unsigned int num_ones(unsigned int num)
{
    unsigned int ones = 0;
    while (num) {
        ones += (num & 1);
        num >>= 1;
    }
    return ones;
}

static inline unsigned int produce_n_one(unsigned int n)
{
    unsigned int num = 0;
    unsigned int i;
    for (i = 0;i < n;i++) {
        num += (1<<i);
    }
    return num;
}

unsigned int AudioBuffMngrInit(void *aud_buff,unsigned int aud_buff_size,bool aout,unsigned int call_interval)
{
    unsigned int i;

    if (!aout) {
        for (i = 0; i < AIN_BUFFER_NUM; i++) {
            bm.ctx[i].out_buff_addr = (unsigned int)aud_buff + call_interval * i;
            bm.ctx[i].out_buff_size = aud_buff_size;
            bm.ctx[i].total_size    = call_interval;
        }

        bm.rd_seg = bm.wr_seg = bm.virt_wr_seg = bm.used_seg = 0;
        bm.wr_ptr = bm.rd_ptr = (unsigned int)aud_buff;
        bm.ain_start          = false;
        bm.ain_first_read     = false;
        bm.need_recover       = false;

        bm.total_buff_size    = AIN_BUFFER_NUM * aud_buff_size;

        return (unsigned int)&bm;
    }

    return 0;
}

void AudioBuffMngrDone(unsigned int handle)
{
    unsigned int i;

    if (handle == (unsigned int)&bm)
    {
        for (i = 0; i < AIN_BUFFER_NUM; i++) {
            bm.ctx[i].addr          = 0;
            bm.ctx[i].out_buff_addr = 0;
            bm.ctx[i].out_buff_size = 0;
            bm.ctx[i].total_size    = 0;
        }

        bm.rd_seg = bm.wr_seg = bm.virt_wr_seg = bm.used_seg = 0;
        bm.wr_ptr = bm.rd_ptr = 0;
        bm.ain_start      = false;
        bm.ain_first_read = false;
        bm.need_recover   = false;
    }
}

/*
 * do_check : 确认 buff 实际可用空间
 */
void AudioBuffProduceNotify(unsigned int handle, int do_check)
{
    unsigned int cur_seg;
    unsigned int distance;
    unsigned int left_size;

    if (handle == (unsigned int)&bm) {
        if (bm.ain_start && !bm.ain_first_read) {
            bm.virt_wr_seg++;

            if (bm.virt_wr_seg == AIN_BUFFER_NUM)
                bm.virt_wr_seg = 0;

            if (do_check) {
                cur_seg = bm.virt_wr_seg;
                left_size = bm.ctx[bm.rd_seg].out_buff_addr + bm.ctx[bm.rd_seg].out_buff_size - bm.rd_ptr;
                if (cur_seg >= bm.rd_seg)
                    distance = (cur_seg - bm.rd_seg) * bm.ctx[0].out_buff_size + left_size;
                else
                    distance = (cur_seg + AIN_BUFFER_NUM - bm.rd_seg) * bm.ctx[0].out_buff_size + left_size;

                ain_buff_used_size = distance;
            }
        }
    }

    return ;
}

void * AudioBuffProduce(unsigned int handle,void *buff,unsigned int size)
{
    unsigned int cur_seg;
    unsigned int i;

    if (handle == (unsigned int)&bm)
    {
        for  (i = 0; i < AIN_BUFFER_NUM; i++) {
            if (bm.ctx[i].out_buff_addr == (unsigned int)buff)
                break;
        }

        if (i == AIN_BUFFER_NUM) {
            PRINT ("%s, Invalid buff addr : %p\n", __func__, buff);
            return NULL;
        }

        cur_seg = i;
        bm.wr_seg = cur_seg;
        bm.wr_ptr = (unsigned int)buff + size;

        if (bm.used_seg & (1 << cur_seg)) {
            if (bm.ain_start && !bm.ain_first_read) {
                //PRINT("ain overflow, used_seg : %x, wr_seg : %d, rd_seg : %d\n", bm.used_seg, bm.wr_seg, bm.rd_seg);
                uac_us_fifo_overflow = 1;
                bm.need_recover = true;
            }
        }

        bm.used_seg |= (1 << cur_seg);
        return (void *)bm.wr_ptr;
    }

    return NULL;
}

void * AudioBuffConsume(unsigned int handle,unsigned int size)
{
    int is_first = 0;

    if (handle == (unsigned int)&bm)
    {
        if(bm.ain_first_read || bm.need_recover)
        {
            /*
             * 思路就是 usb 开始工作时，使 ain 的写领先 usb 读一定距离，当前设定为总距离的一半
             */
            unsigned int avalible_context_number;
            unsigned int start_gate;
            unsigned int cur_segment;

            avalible_context_number = num_ones(bm.used_seg);
            start_gate     = START_GATE;
            cur_segment    = bm.wr_seg;
            bm.virt_wr_seg = cur_segment;

            if (avalible_context_number >= start_gate) {
                bm.used_seg = 0;

                for (;start_gate;start_gate--) {
                    bm.used_seg |= (1 << cur_segment);
                    if (cur_segment == 0) {
                        cur_segment = AIN_BUFFER_NUM - 1;
                    } else {
                        cur_segment--;
                    }
                }

                ++cur_segment;
                if (cur_segment == AIN_BUFFER_NUM)
                    cur_segment = 0;

                bm.rd_seg = cur_segment;
                bm.rd_ptr = bm.ctx[bm.rd_seg].out_buff_addr;

                bm.ain_first_read = false;
                bm.need_recover   = false;

                is_first = 1;
            } else
                return NULL;
        }

        if (bm.used_seg & (1 << bm.rd_seg)) {
            // 第一次从 context 中取数据时,从头开始取。后面再取数据时，根据前一次取的数据量更新指针
            if (!is_first)
                bm.rd_ptr += size;

            if (bm.rd_ptr == (bm.ctx[bm.rd_seg].out_buff_addr + bm.ctx[bm.rd_seg].out_buff_size)) {
                bm.used_seg &= ~(1 << bm.rd_seg);
                bm.rd_seg++;
                if (bm.rd_seg == AIN_BUFFER_NUM)
                    bm.rd_seg = 0;

                bm.rd_ptr = bm.ctx[bm.rd_seg].out_buff_addr;
            }
            return (void *)bm.rd_ptr;
        } else {
            //PRINT("ain underflow, used_seg : %x, rd_seg: %d, wr_seg : %d\n", bm.used_seg, bm.rd_seg, bm.wr_seg);
            uac_us_fifo_underflow = 1;
            bm.need_recover = true;
        }
    }
    return NULL;
}

int AudioBuffConsumeCheck(unsigned int handle, unsigned int want_len)
{
    unsigned int left_size;

    left_size = bm.ctx[bm.rd_seg].out_buff_addr + bm.ctx[bm.rd_seg].out_buff_size - bm.rd_ptr;

    if (left_size >= want_len)
        return 0;
    else
        return -1;
}

int AudioBuffConsumerRegister(unsigned int handle,RD_CALLBACK aud_consumer)
{
    return 0;
}

void AudioBuffConsumeStart(unsigned int handle)
{
    if (handle == (unsigned int)&bm) {
        bm.ain_start      = true;
        bm.ain_first_read = true;
        bm.need_recover   = false;

        ain_buff_used_size = 0;
    }
}

void AudioBuffConsumeStop(unsigned int handle)
{
    if (handle == (unsigned int)&bm)
        bm.ain_start = false;
}

/*
 * only for audio in now
 */
unsigned int AudioBuffTotalLen(unsigned int handle)
{
    return bm.total_buff_size;
}

/*
 * only for audio in now
 */
unsigned int AudioBuffUsedLen(unsigned int handle)
{
    return ain_buff_used_size;
}

/*
 * 用于监测 uac fifo 的状态，可放在 tick 中调用
 */
void AudioBuffPrintStatus(unsigned int handle)
{
    if (uac_us_fifo_overflow) {
        PRINT ("uac fifo overflowed\n");
        uac_us_fifo_overflow = 0;
    }

    if (uac_us_fifo_underflow) {
        PRINT ("uac fifo underflowed\n");
        uac_us_fifo_underflow = 0;
    }

    return ;
}
